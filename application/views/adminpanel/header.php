<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Panel de Control | RODI</title>
	<!-- CSS -->
	<?php echo link_tag("css/custom.css"); ?>
	<?php echo link_tag("css/sb-admin-2.min.css"); ?>
	<!-- DataTables -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- FullCalendar -->
	<link href='<?php echo base_url(); ?>calendar/css/fullcalendar.css' rel='stylesheet' >
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>img/favicon.jpg" />
	<!-- Select Bootstrap -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<!-- Sweetalert 2 -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.12.7/dist/sweetalert2.min.css">

</head>
<body id="page-top">
	<!-- Page Wrapper -->
  <div id="wrapper">
		<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
					<img width="40px" src="<?php echo base_url(); ?>img/rodi_icon.png">
        </div>
        <div class="sidebar-brand-text mx-3">RODI</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

			<!-- Dashboard -->
			<?php 
			if(in_array(1, $acceso)){ ?>
        <li class="nav-item active">
					<a class="nav-link" href="<?php echo site_url('Dashboard/index') ?>">
						<i class="fas fa-fw fa-tachometer-alt"></i>
						<span>Dashboard</span></a>
				</li>
			<?php
			}?>

      <!-- Divider -->
			<hr class="sidebar-divider">

			<!-- Clientes -->
			<?php 
			if(in_array(2, $acceso)){ ?>
				<li class="nav-item">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseClientes" aria-expanded="true" aria-controls="collapseClientes">
						<i class="fas fa-fw fa-users"></i>
						<span>Clientes</span>
					</a>
					<div id="collapseClientes" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          	<div class="bg-white py-2 collapse-inner rounded">
							<?php
									if($permisos){
										foreach($permisos as $p){ 
											echo "<a class='collapse-item contraer'data-toggle='tooltip' data-placement='right' title='".$p->nombreCliente."' href='".site_url("$p->url")."'>".$p->nombreCliente."</a>";
										} 
									} 
							?>  
						</div>
        	</div>
				</li>
			<?php       
			} ?>

			<!-- Doping -->
			<?php       
			if(in_array(5, $acceso)){ ?>
				<li class="nav-item">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDoping" aria-expanded="true" aria-controls="collapseDoping">
						<i class="fas fa-fw fa-eye-dropper"></i>
						<span>Doping</span>
					</a>
					<div id="collapseDoping" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          	<div class="bg-white py-2 collapse-inner rounded">
							<?php 
							if(in_array(6, $acceso)){ ?>
								<a class="collapse-item" href="<?php echo site_url('Doping/index') ?>">Tabla de control</a>
							<?php
							} ?>
						</div>
					</div>
				</li>
			<?php 
			} ?>

			<!-- Médico -->
			<?php       
			if(in_array(24, $acceso)){ ?>
				<li class="nav-item">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMedico" aria-expanded="true" aria-controls="collapseMedico">
						<i class="fas fa-notes-medical"></i>
						<span>Médico</span>
					</a>
					<div id="collapseMedico" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          	<div class="bg-white py-2 collapse-inner rounded">
							<?php 
							if(in_array(25, $acceso)){ ?>
								<a class="collapse-item" href="<?php echo site_url('Medico/index') ?>">Tabla de control</a>
							<?php
							} ?>
						</div>
					</div>
				</li>
			<?php 
			} ?>

			<!-- Catalogos -->
			<?php
			if(in_array(10, $acceso)){ ?>
				<li class="nav-item">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCatalogos" aria-expanded="true" aria-controls="collapseCatalogos">
						<i class="fas fa-fw fa-folder"></i>
						<span>Catálogos</span>
					</a>
					<div id="collapseCatalogos" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          	<div class="bg-white py-2 collapse-inner rounded">
							<?php 
							if(in_array(11, $acceso)){ ?>
								<a class="collapse-item" href="<?php echo site_url('Cat_Clientes/index') ?>">Clientes</a>
							<?php
							} ?>
							<?php 
							if(in_array(15, $acceso)){ ?>
								<a class="collapse-item" href="<?php echo site_url('Cat_Subclientes/index') ?>">Subclientes</a>
							<?php
							} ?>
							<?php 
							if(in_array(19, $acceso)){ ?>
								<a class="collapse-item" href="<?php echo site_url('Cat_Puestos/index') ?>">Puestos</a>
							<?php
							} ?>
						</div>
					</div>
				</li>
			<?php 
			}
			?>

			<!-- Reportes -->
			<?php
			if(in_array(23, $acceso)){ ?>
				<li class="nav-item">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReportes" aria-expanded="true" aria-controls="collapseReportes">
						<i class="fas fa-fw fa-medkit"></i>
						<span>Reportes</span>
					</a>
					<div id="collapseReportes" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          	<div class="bg-white py-2 collapse-inner rounded">
							<a class="collapse-item" href="<?php echo site_url('Reporte/index') ?>">Reportes</a>
						</div>
					</div>
				</li>
			<?php 
			}
			?>

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
		<!-- End of Sidebar -->
		<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno'); ?></span>
                <img class="img-profile rounded-circle" src="<?php echo base_url(); ?>img/user.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <!--a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div-->
                <a class="dropdown-item" href="<?php echo base_url(); ?>Login/logout">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Cerrar sesión
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->