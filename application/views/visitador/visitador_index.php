<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Visitador | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/visitador.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-datetimepicker.min.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"-->
	<!--link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"-->

</head>
<body>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<p id="texto_msj"></p>
  	</div>
	<div class="modal fade" id="formCandidatoModal" role="dialog" aria-labelledby="largeModal" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">Candidato: <span id="formNombre"></span></h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<form id="datos">
		        		<div class="alert alert-warning text-center">
			      			<p>Los campos con asterico (*) son obligatorios</p>
			      		</div>
		        		<div class="alert alert-primary text-center">
			      			<p>Documentos comprobatorios</p>
			      		</div>
			      		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Acta de nacimiento *</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_acta">Fecha de expedición *</label>
                				<input type="text" class="form-control solo_lectura p_obligado tipo_fecha" name="fecha_acta" id="fecha_acta" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_acta">Número y/o vigencia *</label>
			        			<input type="text" class="form-control p_obligado" name="numero_acta" id="numero_acta">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Comprobante de Domicilio *</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_domicilio">Fecha de expedición *</label>
                				<input type="text" class="form-control solo_lectura p_obligado tipo_fecha" name="fecha_domicilio" id="fecha_domicilio" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_domicilio">Número y/o vigencia *</label>
			        			<input type="text" class="form-control p_obligado" name="numero_domicilio" id="numero_domicilio">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Credencial de elector</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_ine">Fecha de expedición </label>
                				<input type="text" class="form-control solo_numeros" name="fecha_ine" id="fecha_ine" maxlength="4">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_ine">Número y/o vigencia</label>
			        			<input type="text" class="form-control" name="numero_ine" id="numero_ine">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">CURP *</p>
            			</div>   		
                			<div class="col-12">
                				<label for="fecha_curp">Fecha de expedición *</label>
                				<input type="text" class="form-control solo_lectura p_obligado tipo_fecha" name="fecha_curp" id="fecha_curp" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_curp">Número y/o vigencia *</label>
			        			<input type="text" class="form-control p_obligado" name="numero_curp" id="numero_curp" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Afiliación al IMSS</p>
            			</div>  
                			<div class="col-12">
                				<label for="fecha_imss">Fecha de expedición </label>
                				<input type="text" class="form-control solo_lectura tipo_fecha" name="fecha_imss" id="fecha_imss" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_imss">Número y/o vigencia </label>
			        			<input type="text" class="form-control" name="numero_imss" id="numero_imss">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Comprobante retención de impuestos</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_retencion">Fecha de expedición </label>
                				<input type="text" class="form-control solo_lectura tipo_fecha" name="fecha_retencion" id="fecha_retencion" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_retencion">Número y/o vigencia</label>
			        			<input type="text" class="form-control" name="numero_retencion" id="numero_retencion">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">RFC *</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_rfc">Fecha de expedición *</label>
                				<input type="text" class="form-control solo_lectura p_obligado tipo_fecha" name="fecha_rfc" id="fecha_rfc" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_rfc">Número y/o vigencia *</label>
			        			<input type="text" class="form-control p_obligado" name="numero_rfc" id="numero_rfc" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Licencia para conducir</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_licencia">Fecha de expedición </label>
                				<input type="text" class="form-control solo_lectura tipo_fecha" name="fecha_licencia" id="fecha_licencia" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_licencia">Número y/o vigencia</label>
			        			<input type="text" class="form-control" name="numero_licencia" id="numero_licencia">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Vigencia migratoria (extranjeros)</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_migra">Fecha de expedición </label>
                				<input type="text" class="form-control solo_lectura tipo_fecha" name="fecha_migra" id="fecha_migra" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_migra">Número y/o vigencia</label>
			        			<input type="text" class="form-control" name="numero_migra" id="numero_migra">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">VISA Norteamericana</p>
            			</div>
                			<div class="col-12">
                				<label for="fecha_visa">Fecha de expedición </label>
                				<input type="text" class="form-control solo_lectura tipo_fecha" name="fecha_visa" id="fecha_visa" readonly>
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="numero_visa">Número y/o vigencia</label>
			        			<input type="text" class="form-control" name="numero_visa" id="numero_visa">
			        			<br><br>
                			</div>
			      		<div class="alert alert-primary text-center">
			      			<p>Datos del Grupo Familiar</p>
			      		</div>
			        	<div class="alert alert-secondary text-center">
			      			<p>Persona #1</p>
			      		</div>
			      		<div class="row">
							<div class="col-12">
								<label for="p1_nombre">Nombre completo *</label>
			        			<input type="text" class="form-control es_persona p_obligado" name="p1_nombre" id="p1_nombre">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_parentesco">Parentesco *</label>
			        			<select name="p1_parentesco" id="p1_parentesco" class="form-control es_persona p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($parentescos as $par) {?>
						                <option value="<?php echo $par->id; ?>"><?php echo $par->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="p1_edad">Edad *</label>
			        			<input type="text" class="form-control solo_numeros es_persona p_obligado" name="p1_edad" id="p1_edad" maxlength="2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_escolaridad">Escolaridad *</label>
			        			<select name="p1_escolaridad" id="p1_escolaridad" class="form-control es_persona p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($escolaridades as $esc) {?>
						                <option value="<?php echo $esc->id; ?>"><?php echo $esc->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="p1_vive">¿Vive con usted? *</label>
			        			<select name="p1_vive" id="p1_vive" class="form-control es_persona p_obligado">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_civil">Estado civil *</label>
			        			<select name="p1_civil" id="p1_civil" class="form-control es_persona p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($civiles as $civ) {?>
						                <option value="<?php echo $civ->id; ?>"><?php echo $civ->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="p1_empresa">Empresa *</label>
			        			<input type="text" class="form-control es_persona p_obligado" name="p1_empresa" id="p1_empresa">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_puesto">Puesto *</label>
			        			<input type="text" class="form-control es_persona p_obligado" name="p1_puesto" id="p1_puesto">
			        			<br>
							</div>
							<div class="col-6">
								<label for="p1_antiguedad">Antigüedad *</label>
			        			<input type="text" class="form-control es_persona p_obligado" name="p1_antiguedad" id="p1_antiguedad">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_sueldo">Sueldo *</label>
			        			<input type="text" class="form-control solo_numeros es_persona p_obligado" name="p1_sueldo" id="p1_sueldo" maxlength="8">
			        			<br>
							</div>
							<div class="col-6">
								<label for="p1_aportacion">Aportación *</label>
			        			<input type="text" class="form-control solo_numeros es_persona p_obligado" name="p1_aportacion" id="p1_aportacion" maxlength="8">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_muebles">Muebles e inmuebles *</label>
			        			<input type="text" class="form-control es_persona p_obligado" name="p1_muebles" id="p1_muebles">
			        			<br>
							</div>
							<div class="col-6">
								<label for="p1_adeudo">Adeudo *</label>
			        			<select name="p1_adeudo" id="p1_adeudo" class="form-control es_persona p_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
							</div>
						</div>
						<br>
						<div id="div_personas"></div>
						<div class="row">
							<div class="col-md-4 offset-md-4">
								<a href="javascript:void(0)" class="btn btn-primary" onclick="generarPersona()">Agregar otra persona</a><br><br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label for="candidato_ingresos">Ingresos del candidato *</label>
			        			<input type="text" class="form-control p_obligado" name="candidato_ingresos" id="candidato_ingresos">
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="candidato_muebles">Muebles e inmuebles del candidato *</label>
			        			<input type="text" class="form-control p_obligado" name="candidato_muebles" id="candidato_muebles">
			        			<br>
							</div>
							<div class="col-md-3">
								<label for="candidato_adeudo">Adeudo *</label>
			        			<select name="candidato_adeudo" id="candidato_adeudo" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="notas">Notas *</label>
			        			<textarea class="form-control p_obligado" name="notas" id="notas" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="alert alert-secondary text-center">
			      			<p>Egresos mensuales</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="renta">Renta *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado" name="renta" id="renta" maxlength="8">
			        			<br>
							</div>
							<div class="col-6">
								<label for="alimentos">Alimentos *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado" name="alimentos" id="alimentos" maxlength="8">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="servicios">Servicios *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado" name="servicios" id="servicios">
			        			<br>
							</div>
							<div class="col-6">
								<label for="transportes">Transportes *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado" name="transportes" id="transportes">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="otros_gastos">Otros *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado" name="otros_gastos" id="otros_gastos">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<label for="solvencia">Cuando los egresos son mayores a los ingresos, ¿cómo los solventa? *</label>
			        			<textarea class="form-control p_obligado" name="solvencia" id="solvencia" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="alert alert-primary text-center">
			      			<p>Habitación y Medio Ambiente</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="tiempo_residencia">Tiempo de residencia en el domicilio actual *</label>
			        			<input type="text" class="form-control p_obligado" name="tiempo_residencia" id="tiempo_residencia">
			        			<br>
							</div>
							<div class="col-6">
								<label for="nivel_zona">Nivel de la zona *</label>
			        			<select name="nivel_zona" id="nivel_zona" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($zonas as $z) {?>
						                <option value="<?php echo $z->id; ?>"><?php echo $z->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="tipo_vivienda">Tipo de vivienda *</label>
			        			<select name="tipo_vivienda" id="tipo_vivienda" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($viviendas as $viv) {?>
						                <option value="<?php echo $viv->id; ?>"><?php echo $viv->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="recamaras">Recámaras *</label>
			        			<input type="number" class="form-control p_obligado" name="recamaras" id="recamaras">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="banios">Baños *</label>
			        			<input type="text" class="form-control p_obligado" name="banios" id="banios">
			        			<br>
							</div>
							<div class="col-6">
								<label for="distribucion">Distribución *</label>
			        			<textarea class="form-control p_obligado" name="distribucion" id="distribucion" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="calidad_mobiliario">Calidad mobiliario *</label>
			        			<select name="calidad_mobiliario" id="calidad_mobiliario" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <option value="1">Buena</option>
						            <option value="2">Regular</option>
						            <option value="3">Deficiente</option>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="mobiliario">Mobiliario *</label>
			        			<select name="mobiliario" id="mobiliario" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">Incompleto</option>
						            <option value="1">Completo</option>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="tamanio_vivienda">Tamaño vivienda *</label>
			        			<select name="tamanio_vivienda" id="tamanio_vivienda" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <option value="1">Amplia</option>
						            <option value="2">Media</option>
						            <option value="3">Reducida</option>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="condiciones_vivienda">Condiciones de la vivienda *</label>
			        			<select name="condiciones_vivienda" id="condiciones_vivienda" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($condiciones as $cond) {?>
						                <option value="<?php echo $cond->id; ?>"><?php echo $cond->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="alert alert-primary text-center">
			      			<p>Referencias vecinales</p>
			      		</div>
			      		<div class="alert alert-secondary text-center">
			      			<p>Referencia #1</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="vecino1_nombre">Nombre *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_nombre" id="vecino1_nombre">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_domicilio">Domicilio *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_domicilio" id="vecino1_domicilio">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_tel">Teléfono *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_tel" id="vecino1_tel">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_concepto">¿Qué concepto tiene del aspirante?  *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_concepto" id="vecino1_concepto">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_familia" id="vecino1_familia">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_civil" id="vecino1_civil">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_hijos">¿Tiene hijos?  *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_hijos" id="vecino1_hijos">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_sabetrabaja">¿Sabe en dónde trabaja? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_sabetrabaja" id="vecino1_sabetrabaja">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_notas">Notas *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino1_notas" id="vecino1_notas">
			        			<br>
							</div>
						</div>
						<div class="alert alert-secondary text-center">
			      			<p>Referencia #2</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="vecino2_nombre">Nombre *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_nombre" id="vecino2_nombre">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino2_domicilio">Domicilio *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_domicilio" id="vecino2_domicilio">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino2_tel">Teléfono *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_tel" id="vecino2_tel">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino2_concepto">¿Qué concepto tiene del aspirante?  *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_concepto" id="vecino2_concepto">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino2_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_familia" id="vecino2_familia">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino2_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_civil" id="vecino2_civil">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino2_hijos">¿Tiene hijos?  *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_hijos" id="vecino2_hijos">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino2_sabetrabaja">¿Sabe en dónde trabaja? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_sabetrabaja" id="vecino2_sabetrabaja">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino2_notas">Notas *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino2_notas" id="vecino2_notas">
			        			<br>
							</div>
						</div>
						<div class="alert alert-secondary text-center">
			      			<p>Referencia #3</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="vecino3_nombre">Nombre *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_nombre" id="vecino3_nombre">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino3_domicilio">Domicilio *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_domicilio" id="vecino3_domicilio">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino3_tel">Teléfono *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_tel" id="vecino3_tel">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino3_concepto">¿Qué concepto tiene del aspirante?  *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_concepto" id="vecino3_concepto">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino3_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_familia" id="vecino3_familia">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino3_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_civil" id="vecino3_civil">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino3_hijos">¿Tiene hijos?  *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_hijos" id="vecino3_hijos">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino3_sabetrabaja">¿Sabe en dónde trabaja? *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_sabetrabaja" id="vecino3_sabetrabaja">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino3_notas">Notas *</label>
			        			<input type="text" class="form-control p_obligado" name="vecino3_notas" id="vecino3_notas">
			        			<br>
							</div>
						</div>
						<div class="alert alert-danger text-center">
			      			<p>Recuerde tomar las 2 fotos del candidato en su domicilio por favor</p>
			      		</div>
			        </form>
			        <div id="campos_vacios">
		      			<p class="msj_error">Todos los campos obligatorios son requeridos</p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" id="guardarVisita">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="formCandidato2Modal" role="dialog" aria-labelledby="largeModal" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">Candidato: <span id="formNombre"></span></h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<form id="datostipo2">
		        		<div class="alert alert-warning text-center">
			      			<p>Los campos con asterico (*) son obligatorios</p>
			      		</div>
		        		<div class="alert alert-primary text-center">
			      			<p>Documentos comprobatorios</p>
			      		</div>
			      		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Número de Seguridad Social (NSS) *</p>
            			</div>  
                			<div class="col-12">
                				<label for="imss1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="imss1" id="imss1">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="imss2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="imss2" id="imss2" value="Instituto Mexicano del Seguro Social">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Comprobante de Domicilio *</p>
            			</div>
                			<div class="col-12">
                				<label for="dom1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="dom1" id="dom1">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="dom2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="dom2" id="dom2">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">INE (IFE o pasaporte) *</p>
            			</div>
                			<div class="col-12">
                				<label for="ine1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="ine1" id="ine1">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="ine2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="ine2" id="ine2" value="Instituto Federal Electoral">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">CURP *</p>
            			</div>   		
                			<div class="col-12">
                				<label for="curp1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="curp1" id="curp1">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="curp2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="curp2" id="curp2" value="Clave Única de Registro de Población ">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">RFC con homoclave*</p>
            			</div>
                			<div class="col-12">
                				<label for="rfc1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="rfc1" id="rfc1">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="rfc2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="rfc2" id="rfc2" value="Registro Federal de Contribuyentes">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Licencia para conducir *</p>
            			</div>
                			<div class="col-12">
                				<label for="lic1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="lic1" id="lic1">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="lic2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="lic2" id="lic2">
			        			<br>
                			</div>
                		<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Cartas de recomendación *</p>
            			</div>
                			<div class="col-12">
                				<label for="carta1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="carta1" id="carta1">
			        			<br>
                			</div>
                			<div class="col-12">
                				<label for="carta2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="carta2" id="carta2">
			        			<br><br>
                			</div>
            			<div class="alert alert-secondary text-center">
            				<p class="label_documentos">Comentarios de la documentación *</p>
            			</div>
                			<div class="col-12">
                				<label for="comentarios_documentos">Comentarios *</label>
                				<textarea class="form-control p_obligado_2" name="comentarios_documentos" id="comentarios_documentos" rows="2"></textarea>
			        			<br>
                			</div>
			      		<div class="alert alert-primary text-center">
			      			<p>Datos del Grupo Familiar</p>
			      		</div>
			        	<div class="alert alert-secondary text-center">
			      			<p>Persona #1</p>
			      		</div>
			      		<div class="row">
							<div class="col-12">
								<label for="p1_nombre">Nombre completo *</label>
			        			<input type="text" class="form-control es_persona_2 p_obligado_2" name="p1_nombre_2" id="p1_nombre_2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_parentesco">Parentesco *</label>
			        			<select name="p1_parentesco_2" id="p1_parentesco_2" class="form-control es_persona_2 p_obligado_2">
						            <option value="">Selecciona</option>
						            <?php foreach ($parentescos as $par) {?>
						                <option value="<?php echo $par->id; ?>"><?php echo $par->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="p1_edad">Edad *</label>
			        			<input type="text" class="form-control solo_numeros es_persona_2 p_obligado_2" name="p1_edad_2" id="p1_edad_2" maxlength="2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_escolaridad">Escolaridad *</label>
			        			<select name="p1_escolaridad_2" id="p1_escolaridad_2" class="form-control es_persona_2 p_obligado_2">
						            <option value="">Selecciona</option>
						            <?php foreach ($escolaridades as $esc) {?>
						                <option value="<?php echo $esc->id; ?>"><?php echo $esc->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="p1_vive">¿Vive con usted? *</label>
			        			<select name="p1_vive_2" id="p1_vive_2" class="form-control es_persona_2 p_obligado_2">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_civil">Estado civil *</label>
			        			<select name="p1_civil_2" id="p1_civil_2" class="form-control es_persona_2 p_obligado_2">
						            <option value="">Selecciona</option>
						            <?php foreach ($civiles as $civ) {?>
						                <option value="<?php echo $civ->id; ?>"><?php echo $civ->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="p1_empresa">Empresa *</label>
			        			<input type="text" class="form-control es_persona_2 p_obligado_2" name="p1_empresa_2" id="p1_empresa_2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_puesto">Puesto *</label>
			        			<input type="text" class="form-control es_persona_2 p_obligado_2" name="p1_puesto_2" id="p1_puesto_2">
			        			<br>
							</div>
							<div class="col-6">
								<label for="p1_antiguedad">Antigüedad *</label>
			        			<input type="text" class="form-control es_persona_2 p_obligado_2" name="p1_antiguedad_2" id="p1_antiguedad_2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_sueldo">Sueldo *</label>
			        			<input type="text" class="form-control solo_numeros es_persona_2 p_obligado_2" name="p1_sueldo_2" id="p1_sueldo_2" maxlength="8">
			        			<br>
							</div>
							<div class="col-6">
								<label for="p1_aportacion">Aportación *</label>
			        			<input type="text" class="form-control solo_numeros es_persona_2 p_obligado_2" name="p1_aportacion_2" id="p1_aportacion_2" maxlength="8">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="p1_muebles">Muebles e inmuebles *</label>
			        			<input type="text" class="form-control es_persona_2 p_obligado_2" name="p1_muebles_2" id="p1_muebles_2">
			        			<br>
							</div>
							<div class="col-6">
								<label for="p1_adeudo">Adeudo *</label>
			        			<select name="p1_adeudo_2" id="p1_adeudo_2" class="form-control es_persona_2 p_obligado_2">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
							</div>
						</div>
						<br>
						<div id="div_personas_2"></div>
						<div class="row">
							<div class="col-md-4 offset-md-4">
								<a href="javascript:void(0)" class="btn btn-primary" onclick="generarPersonaTipo2()">Agregar otra persona</a><br><br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 offset-md-1">
								<label for="candidato_ingresos">Ingresos del candidato *</label>
			        			<input type="text" class="form-control p_obligado_2" name="candidato_ingresos_2" id="candidato_ingresos_2">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="candidato_aporte">Aporte del candidato *</label>
			        			<input type="text" class="form-control p_obligado_2" name="candidato_aporte_2" id="candidato_aporte_2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="candidato_muebles">Muebles e inmuebles del candidato *</label>
			        			<input type="text" class="form-control p_obligado_2" name="candidato_muebles_2" id="candidato_muebles_2">
			        			<br>
							</div>
							<div class="col-md-3">
								<label for="candidato_adeudo">Adeudo *</label>
			        			<select name="candidato_adeudo_2" id="candidato_adeudo_2" class="form-control p_obligado_2">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="notas">Notas *</label>
			        			<textarea class="form-control p_obligado_2" name="notas_2" id="notas_2" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="alert alert-secondary text-center">
			      			<p>Egresos mensuales</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="renta">Renta *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado_2" name="renta_2" id="renta_2" maxlength="8">
			        			<br>
							</div>
							<div class="col-6">
								<label for="alimentos">Alimentos *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado_2" name="alimentos_2" id="alimentos_2" maxlength="8">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="servicios">Servicios *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado_2" name="servicios_2" id="servicios_2">
			        			<br>
							</div>
							<div class="col-6">
								<label for="transportes">Transportes *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado_2" name="transportes_2" id="transportes_2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="otros_gastos">Otros *</label>
			        			<input type="text" class="form-control solo_numeros p_obligado_2" name="otros_gastos_2" id="otros_gastos_2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<label for="solvencia">Cuando los egresos son mayores a los ingresos, ¿cómo los solventa? *</label>
			        			<textarea class="form-control p_obligado_2" name="solvencia_2" id="solvencia_2" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="alert alert-primary text-center">
			      			<p>Habitación y Medio Ambiente</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="tiempo_residencia">Tiempo de residencia en el domicilio actual *</label>
			        			<input type="text" class="form-control p_obligado_2" name="tiempo_residencia_2" id="tiempo_residencia_2">
			        			<br>
							</div>
							<div class="col-6">
								<label for="nivel_zona">Nivel de la zona *</label>
			        			<select name="nivel_zona_2" id="nivel_zona_2" class="form-control p_obligado_2">
						            <option value="">Selecciona</option>
						            <?php foreach ($zonas as $z) {?>
						                <option value="<?php echo $z->id; ?>"><?php echo $z->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="tipo_vivienda">Tipo de vivienda *</label>
			        			<select name="tipo_vivienda_2" id="tipo_vivienda_2" class="form-control p_obligado_2">
						            <option value="">Selecciona</option>
						            <?php foreach ($viviendas as $viv) {?>
						                <option value="<?php echo $viv->id; ?>"><?php echo $viv->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="recamaras">Recámaras *</label>
			        			<input type="number" class="form-control p_obligado_2" name="recamaras_2" id="recamaras_2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="banios">Baños *</label>
			        			<input type="text" class="form-control p_obligado_2" name="banios_2" id="banios_2">
			        			<br>
							</div>
							<div class="col-6">
								<label for="distribucion">Distribución *</label>
			        			<textarea class="form-control p_obligado_2" name="distribucion_2" id="distribucion_2" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="calidad_mobiliario">Calidad mobiliario *</label>
			        			<select name="calidad_mobiliario_2" id="calidad_mobiliario_2" class="form-control p_obligado_2">
						            <option value="">Selecciona</option>
						            <option value="1">Buena</option>
						            <option value="2">Regular</option>
						            <option value="3">Deficiente</option>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="mobiliario">Mobiliario *</label>
			        			<select name="mobiliario_2" id="mobiliario_2" class="form-control p_obligado_2">
						            <option value="">Selecciona</option>
						            <option value="0">Incompleto</option>
						            <option value="1">Completo</option>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="tamanio_vivienda">Tamaño vivienda *</label>
			        			<select name="tamanio_vivienda_2" id="tamanio_vivienda_2" class="form-control p_obligado_2">
						            <option value="">Selecciona</option>
						            <option value="1">Amplia</option>
						            <option value="2">Media</option>
						            <option value="3">Reducida</option>
					          	</select>
					          	<br>
							</div>
							<div class="col-6">
								<label for="condiciones_vivienda">Condiciones de la vivienda *</label>
			        			<select name="condiciones_vivienda_2" id="condiciones_vivienda_2" class="form-control p_obligado_2">
						            <option value="">Selecciona</option>
						            <?php foreach ($condiciones as $cond) {?>
						                <option value="<?php echo $cond->id; ?>"><?php echo $cond->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="alert alert-primary text-center">
			      			<p>¿El candidato tiene familiares, amigos o conocidos trabajando en Monex? (En caso de no tener no escribir en los siguientes campos)</p>
			      		</div>
			      		<div class="alert alert-secondary text-center">
			      			<p>Persona #1</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="mismo_trabajo_nombre1">Nombre</label>
			        			<input type="text" class="form-control" name="mismo_trabajo_nombre1" id="mismo_trabajo_nombre1">
			        			<br>
							</div>
							<div class="col-6">
								<label for="mismo_trabajo_puesto1">Puesto</label>
			        			<input type="text" class="form-control" name="mismo_trabajo_puesto1" id="mismo_trabajo_puesto1">
			        			<br>
							</div>
						</div>
						<div class="alert alert-secondary text-center">
			      			<p>Persona #2</p>
			      		</div>
			      		<div class="row">
							<div class="col-6">
								<label for="mismo_trabajo_nombre2">Nombre</label>
			        			<input type="text" class="form-control" name="mismo_trabajo_nombre2" id="mismo_trabajo_nombre2">
			        			<br>
							</div>
							<div class="col-6">
								<label for="mismo_trabajo_puesto2">Puesto</label>
			        			<input type="text" class="form-control" name="mismo_trabajo_puesto2" id="mismo_trabajo_puesto2">
			        			<br>
							</div>
						</div>
						<div class="alert alert-primary text-center">
			      			<p>Referencia vecinal</p>
			      		</div>
			      		<!--div class="alert alert-secondary text-center">
			      			<p>Referencia #1</p>
			      		</div-->
			      		<div class="row">
							<div class="col-6">
								<label for="vecino1_nombre">Nombre *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_nombre" id="vecino1_nombre">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_domicilio">Domicilio *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_domicilio" id="vecino1_domicilio">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_tel">Teléfono *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_tel" id="vecino1_tel">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_concepto">¿Qué concepto tiene del aspirante?  *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_concepto" id="vecino1_concepto">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_familia" id="vecino1_familia">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_civil" id="vecino1_civil">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_hijos">¿Tiene hijos?  *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_hijos" id="vecino1_hijos">
			        			<br>
							</div>
							<div class="col-6">
								<label for="vecino1_sabetrabaja">¿Sabe en dónde trabaja? *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_sabetrabaja" id="vecino1_sabetrabaja">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<label for="vecino1_notas">Notas *</label>
			        			<input type="text" class="form-control p_obligado_2" name="vecino1_notas" id="vecino1_notas">
			        			<br>
							</div>
						</div>
						<div class="alert alert-primary text-center">
			      			<p>Conclusión del visitador</p>
			      		</div>
			      		<div class="row">
							<div class="col-12">
								<label for="comentario_visitador">¿Cómo fue la actitud o comportamiento del candidato durante la visita? *</label>
			        			<input type="text" class="form-control p_obligado_2" name="comentario_visitador_2" id="comentario_visitador_2">
			        			<br>
							</div>
						</div>
						<div class="alert alert-danger text-center">
			      			<p>Recuerde tomar 3 fotos para este candidato, 1 de frente que abarque solo su rostro, y las 2 fotos del candidato en su domicilio por favor</p>
			      		</div>
			        </form>
			        <div id="campos_vacios">
		      			<p class="msj_error">Todos los campos obligatorios son requeridos</p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" id="guardarVisitaTipo2">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-rodi">
		  	<a class="navbar-brand" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		<?php echo strtoupper($this->session->userdata('cliente')); ?>
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno'); ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="<?php echo base_url(); ?>Login/logout">Cerrar sesión</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
		<input type="hidden" id="idCandidato">
		<input type="hidden" class="correo">
		<?php 
			if($visitas != "" && $visitas != null){
				foreach($visitas as $v){
				$sub = ($v->subcliente == "" || $v->subcliente == null)? "N/A":$v->subcliente;
				$cel = ($v->celular == "" || $v->celular == null)? "Sin registro":$v->celular;
				$aux = explode(' ', $v->fecha_alta);
	            $f = explode('-', $aux[0]);
	            $fecha = $f[2]."/".$f[1]."/".$f[0];
	            $h = explode(':', $aux[1]);
	            $hora = $h[0].":".$h[1];
	            $tiempo = $fecha." ".$hora;
				 ?>
				<div class="div_item_contenedor">
					<div class="col-md-12">
						<div class="div_item">
							<div class="img_item">
								<img src="<?php echo base_url() ?>/img/user.png" width="60px">
							</div>
							<div class="div_texto_item">
								<p><b>Candidato: </b><?php echo $v->candidato; ?><br><b>Cliente: </b><?php echo $v->cliente; ?><br><b>Subcliente: </b><?php echo $sub; ?></p>
							</div>
							<div class="div_texto_item">
								<p><b>Tel. Celular: </b><?php echo $cel; ?><br><b>Fecha de alta: </b><?php echo $tiempo; ?></p>
							</div>
							<?php 
							if($v->id_cliente != 16){ ?>
								<div class="formulario_item">
									<a href="javascript:void(0)" data-toggle="tooltip" onclick="abrirFormulario(<?php echo $v->id ?>,'<?php echo $v->candidato; ?>')" class="fa-tooltip a-acciones"><i class="far fa-file-alt"></i></a>
								</div>
						<?php 
							}else{ ?>
								<div class="formulario_item">
									<a href="javascript:void(0)" data-toggle="tooltip" onclick="abrirFormularioTipo2(<?php echo $v->id ?>,'<?php echo $v->candidato; ?>')" class="fa-tooltip a-acciones"><i class="far fa-file-alt"></i></a>
								</div>
						<?php
							} ?>
						</div>
					</div>
				</div>
		<?php 
				}
			}
			else{ ?>
				<div class="div_item_contenedor">
					<div class="col-md-12">
						<div class="div_item">
							<p class="text-center">Sin visitas pendientes</p>
						</div>
					</div>
				</div>
		<?php 
			}
		?>
	<script src="<?php echo base_url() ?>js/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script src="<?php echo base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
	<script>
    $(document).ready(function(){
		var msj = localStorage.getItem("success");
  		if(msj == 1){
  			$("#mensaje").css('display','block');
  			$("#texto_msj").text("Se ha guardado la información del candidato correctamente");
  			setTimeout(function(){
          		$('#mensaje').fadeOut();
        	},6000);
        	localStorage.removeItem("success");
  		}
		$("#guardarVisita").click(function(){
			var data = $("form#datos").serialize();
			var id_candidato = $("#idCandidato").val();
			data += "&id_candidato="+id_candidato;
			/*var nombre = $("#nombre").val();
			var paterno = $("#paterno").val();
			var materno = $("#materno").val();*/

			var persona = "";
	        var total_persona = $(".es_persona").length;
	      	if(total_persona > 0){
	        	for(var i = 1; i <= total_persona / 13; i++){
					persona += $("#p"+i+"_nombre").val()+",,";
					persona += $("#p"+i+"_parentesco").val()+",,";
					persona += $("#p"+i+"_edad").val()+",,";
					persona += $("#p"+i+"_escolaridad").val()+",,";
					persona += $("#p"+i+"_vive").val()+",,";
					persona += $("#p"+i+"_civil").val()+",,";
					persona += $("#p"+i+"_empresa").val()+",,";
					persona += $("#p"+i+"_puesto").val()+",,";
					persona += $("#p"+i+"_antiguedad").val()+",,";
					persona += $("#p"+i+"_sueldo").val()+",,";
					persona += $("#p"+i+"_aportacion").val()+",,";
					persona += $("#p"+i+"_muebles").val()+",,";
					persona += $("#p"+i+"_adeudo").val()+"@@";
				}
	        }
	        //console.log(persona)

			var totalVacios = $('.p_obligado').filter(function(){
	        	return !$(this).val();
	      	}).length;
	      
	      	if(totalVacios > 0){
	        	$(".p_obligado").each(function() {
	          		var element = $(this);
	          		if (element.val() == "") {
	            		element.addClass("requerido");
	            		$("#campos_vacios").css('display','block');
			            setTimeout(function(){
			              $('#campos_vacios').fadeOut();
			            },4000);
	          		}
	          		else{
	            		element.removeClass("requerido");
	          		}
	        	});
	      	}
	      	else{
	      		formdata = $('#datos').serialize();
	      		var info = formdata += "_(personas)_"+persona;
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(info, 'informacion_visita-'+idCandidato+'-'+fecha_txt);
    			$.ajax({
	              	url: '<?php echo base_url('Candidato/completarCandidato'); ?>',
	              	type: 'post',
	              	data: {'data':data, 'personas':persona},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
              			setTimeout(function(){
		              		$('.loader').fadeOut();
		            	},400);
		            	localStorage.setItem("success", 1);
		            	location.reload();
	              	}
            	});
	      	}
		});
		$("#guardarVisitaTipo2").click(function(){
			var data = $("form#datostipo2").serialize();
			var id_candidato = $("#idCandidato").val();
			data += "&id_candidato="+id_candidato;

			var persona = "";
	        var total_persona = $(".es_persona_2").length;
	      	if(total_persona > 0){
	        	for(var i = 1; i <= total_persona / 13; i++){
					persona += $("#p"+i+"_nombre_2").val()+",,";
					persona += $("#p"+i+"_parentesco_2").val()+",,";
					persona += $("#p"+i+"_edad_2").val()+",,";
					persona += $("#p"+i+"_escolaridad_2").val()+",,";
					persona += $("#p"+i+"_vive_2").val()+",,";
					persona += $("#p"+i+"_civil_2").val()+",,";
					persona += $("#p"+i+"_empresa_2").val()+",,";
					persona += $("#p"+i+"_puesto_2").val()+",,";
					persona += $("#p"+i+"_antiguedad_2").val()+",,";
					persona += $("#p"+i+"_sueldo_2").val()+",,";
					persona += $("#p"+i+"_aportacion_2").val()+",,";
					persona += $("#p"+i+"_muebles_2").val()+",,";
					persona += $("#p"+i+"_adeudo_2").val()+"@@";
				}
	        }

			var totalVacios = $('.p_obligado_2').filter(function(){
	        	return !$(this).val();
	      	}).length;
	      
	      	if(totalVacios > 0){
	        	$(".p_obligado_2").each(function() {
	          		var element = $(this);
	          		if (element.val() == "") {
	            		element.addClass("requerido");
	            		$("#campos_vacios").css('display','block');
			            setTimeout(function(){
			              $('#campos_vacios').fadeOut();
			            },4000);
	          		}
	          		else{
	            		element.removeClass("requerido");
	          		}
	        	});
	      	}
	      	else{
	      		formdata = $('#datostipo2').serialize();
	      		var info = formdata += "_(personas)_"+persona;
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(info, 'informacion_visita-'+idCandidato+'-'+fecha_txt);
    			$.ajax({
	              	url: '<?php echo base_url('Candidato/completarCandidatoTipo2'); ?>',
	              	type: 'post',
	              	data: {'data':data, 'personas':persona},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
              			setTimeout(function(){
		              		$('.loader').fadeOut();
		            	},400);
		            	localStorage.setItem("success", 1);
		            	location.reload();
	              	}
            	});
	      	}
		});
		$(".solo_numeros").on("input", function(){
		    var valor = $(this).val();
		    $(this).val(valor.replace(/[^0-9]/g, ''));
		});
    });
	function abrirFormulario(id, candidato){
		$("#idCandidato").val(id);
		$("#formNombre").text(candidato);
		$.ajax({
      		url: '<?php echo base_url('Candidato/getDocumentacionCandidato'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id,'opcion':1},
      		beforeSend: function(){
                $('.loader').css("display","block");
            },
      		success: function(res)
      		{
      			if(res != 0){
	        		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
	            	dato = res.split('@@');
	            	$('#fecha_acta').val(dato[0]);
	            	$('#numero_acta').val(dato[1]);
	            	$('#fecha_domicilio').val(dato[2]);
	            	$('#numero_domicilio').val(dato[3]);
	            	$('#fecha_ine').val(dato[4]);
	            	$('#numero_ine').val(dato[5]);
	            	$('#fecha_curp').val(dato[6]);
	            	$('#numero_curp').val(dato[7]);
	            	$('#fecha_imss').val(dato[8]);
	            	$('#numero_imss').val(dato[9]);
	            	$('#fecha_retencion').val(dato[10]);
	            	$('#numero_retencion').val(dato[11]);
	            	$('#fecha_rfc').val(dato[12]);
	            	$('#numero_rfc').val(dato[13]);
	            	$('#fecha_licencia').val(dato[14]);
	            	$('#numero_licencia').val(dato[15]);
	            	$('#fecha_migra').val(dato[16]);
	            	$('#numero_migra').val(dato[17]);
	            	$('#fecha_visa').val(dato[18]);
	            	$('#numero_visa').val(dato[19]);
	            }
	            else{
	            	setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
	            }
      		}
    	});
		$("#formCandidatoModal").modal("show");
	}
	function abrirFormularioTipo2(id, candidato){
		$("#idCandidato").val(id);
		$("#formtipo2Nombre").text(candidato);
		$.ajax({
      		url: '<?php echo base_url('Candidato/getDocumentacionCandidato'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id,'opcion':2},
      		beforeSend: function(){
                $('.loader').css("display","block");
            },
      		success: function(res)
      		{
        		if(res != 0){
	        		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	dato = res.split('@@');
	            	$('#lic1').val(dato[0]);
	            	$('#lic2').val(dato[1]);
	            	$('#ine1').val(dato[2]);
	            	$('#ine2').val(dato[3]);
	            	$('#dom1').val(dato[4]);
	            	$('#dom2').val(dato[5]);
	            	$('#imss1').val(dato[6]);
	            	$('#imss2').val(dato[7]);
	            	$('#rfc1').val(dato[8]);
	            	$('#rfc2').val(dato[9]);
	            	$('#curp1').val(dato[10]);
	            	$('#curp2').val(dato[11]);
	            	$('#carta1').val(dato[12]);
	            	$('#carta2').val(dato[13]);
	            	$('#comentarios_documentos').val(dato[14]);
	            }
	            else{
	            	setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
	            }
      		}
    	});
		$("#formCandidato2Modal").modal("show");
	}
	var num = 1;
	function generarPersona(){
		num++;
		var item = "";
		item += '<div class="alert alert-secondary text-center p'+num+'"><p>Persona #'+num+'</p></div><div class="row p'+num+'"><div class="col-12"><label for="p'+num+'_nombre">Nombre completo *</label><input type="text" class="form-control es_persona p_obligado" name="p'+num+'_nombre" id="p'+num+'_nombre"><br></div></div><div class="row p'+num+'"><div class="col-6"><label for="p'+num+'_parentesco">Parentesco *</label><select name="p'+num+'_parentesco" id="p'+num+'_parentesco" class="form-control es_persona p_obligado"><option value="">Selecciona</option><?php foreach ($parentescos as $par) {?><option value="<?php echo $par->id; ?>"><?php echo $par->nombre; ?></option><?php } ?></select><br></div><div class="col-6"><label for="p'+num+'_edad">Edad *</label><input type="text" class="form-control solo_numeros es_persona p_obligado" name="p'+num+'_edad" id="p'+num+'_edad" maxlength="2"><br></div></div><div class="row p'+num+'"><div class="col-6"><label for="p'+num+'_escolaridad">Escolaridad *</label><select name="p'+num+'_escolaridad" id="p'+num+'_escolaridad" class="form-control es_persona p_obligado"><option value="">Selecciona</option><?php foreach ($escolaridades as $esc) {?><option value="<?php echo $esc->id; ?>"><?php echo $esc->nombre; ?></option><?php } ?></select><br></div><div class="col-6"><label for="p'+num+'_vive">¿Vive con usted? *</label><select name="p'+num+'_vive" id="p'+num+'_vive" class="form-control es_persona p_obligado"><option value="">Selecciona</option><option value="0">No</option><option value="1">Sí</option></select><br></div></div><div class="row p'+num+'"><div class="col-6"><label for="p'+num+'_civil">Estado civil *</label><select name="p'+num+'_civil" id="p'+num+'_civil" class="form-control es_persona p_obligado"><option value="">Selecciona</option><?php foreach ($civiles as $civ) {?><option value="<?php echo $civ->id; ?>"><?php echo $civ->nombre; ?></option><?php } ?></select><br></div><div class="col-6"><label for="p'+num+'_empresa">Empresa *</label><input type="text" class="form-control es_persona p_obligado" name="p'+num+'_empresa" id="p'+num+'_empresa"><br></div></div><div class="row p'+num+'"><div class="col-6"><label for="p'+num+'_puesto">Puesto *</label><input type="text" class="form-control es_persona p_obligado" name="p'+num+'_puesto" id="p'+num+'_puesto"><br></div><div class="col-6"><label for="p'+num+'_antiguedad">Antigüedad *</label><input type="text" class="form-control es_persona p_obligado" name="p'+num+'_antiguedad" id="p'+num+'_antiguedad"><br></div></div><div class="row p'+num+'"><div class="col-6"><label for="p'+num+'_sueldo">Sueldo *</label><input type="text" class="form-control solo_numeros es_persona p_obligado" name="p'+num+'_sueldo" id="p'+num+'_sueldo" maxlength="8"><br></div><div class="col-6"><label for="p'+num+'_aportacion">Aportación *</label><input type="text" class="form-control solo_numeros es_persona p_obligado" name="p'+num+'_aportacion" id="p'+num+'_aportacion" maxlength="8"><br></div></div><div class="row p'+num+'"><div class="col-6"><label for="p'+num+'_muebles">Muebles e inmuebles *</label><input type="text" class="form-control es_persona p_obligado" name="p'+num+'_muebles" id="p'+num+'_muebles"><br></div><div class="col-6"><label for="p'+num+'_adeudo">Adeudo *</label><select name="p'+num+'_adeudo" id="p'+num+'_adeudo" class="form-control es_persona p_obligado"><option value="">Selecciona</option><option value="0">No</option><option value="1">Sí</option></select><br></div></div><br><div class="row p'+num+'"><div class="col-md-4 offset-md-4"><a href="javascript:void(0)" class="btn btn-danger" onclick="borrarPersona('+num+')">Borrar esta persona</a><br><br><br><br></div></div>';
		$("#div_personas").append(item);
	}
	num2 = 1;
	function generarPersonaTipo2(){
		num2++;
		var item2 = "";
		item2 += '<div class="alert alert-secondary text-center p2_'+num2+'"><p>Persona #'+num2+'</p></div><div class="row p2_'+num2+'"><div class="col-12"><label for="p'+num2+'_nombre">Nombre completo *</label><input type="text" class="form-control es_persona_2 p_obligado_2" name="p'+num2+'_nombre_2" id="p'+num2+'_nombre_2"><br></div></div><div class="row p2_'+num2+'"><div class="col-6"><label for="p'+num2+'_parentesco">Parentesco *</label><select name="p'+num2+'_parentesco_2" id="p'+num2+'_parentesco_2" class="form-control es_persona_2 p_obligado_2"><option value="">Selecciona</option><?php foreach ($parentescos as $par) {?><option value="<?php echo $par->id; ?>"><?php echo $par->nombre; ?></option><?php } ?></select><br></div><div class="col-6"><label for="p'+num2+'_edad">Edad *</label><input type="text" class="form-control solo_numeros es_persona_2 p_obligado_2" name="p'+num2+'_edad_2" id="p'+num2+'_edad_2" maxlength="2"><br></div></div><div class="row p2_'+num2+'"><div class="col-6"><label for="p'+num2+'_escolaridad">Escolaridad *</label><select name="p'+num2+'_escolaridad_2" id="p'+num2+'_escolaridad_2" class="form-control es_persona_2 p_obligado_2"><option value="">Selecciona</option><?php foreach ($escolaridades as $esc) {?><option value="<?php echo $esc->id; ?>"><?php echo $esc->nombre; ?></option><?php } ?></select><br></div><div class="col-6"><label for="p'+num2+'_vive">¿Vive con usted? *</label><select name="p'+num2+'_vive_2" id="p'+num2+'_vive_2" class="form-control es_persona_2 p_obligado_2"><option value="">Selecciona</option><option value="0">No</option><option value="1">Sí</option></select><br></div></div><div class="row p2_'+num2+'"><div class="col-6"><label for="p'+num2+'_civil">Estado civil *</label><select name="p'+num2+'_civil_2" id="p'+num2+'_civil_2" class="form-control es_persona_2 p_obligado_2"><option value="">Selecciona</option><?php foreach ($civiles as $civ) {?><option value="<?php echo $civ->id; ?>"><?php echo $civ->nombre; ?></option><?php } ?></select><br></div><div class="col-6"><label for="p'+num2+'_empresa">Empresa *</label><input type="text" class="form-control es_persona_2 p_obligado_2" name="p'+num2+'_empresa_2" id="p'+num2+'_empresa_2"><br></div></div><div class="row p2_'+num2+'"><div class="col-6"><label for="p'+num2+'_puesto">Puesto *</label><input type="text" class="form-control es_persona_2 p_obligado_2" name="p'+num2+'_puesto_2" id="p'+num2+'_puesto_2"><br></div><div class="col-6"><label for="p'+num2+'_antiguedad">Antigüedad *</label><input type="text" class="form-control es_persona_2 p_obligado_2" name="p'+num2+'_antiguedad_2" id="p'+num2+'_antiguedad_2"><br></div></div><div class="row p2_'+num2+'"><div class="col-6"><label for="p'+num2+'_sueldo">Sueldo *</label><input type="text" class="form-control solo_numeros es_persona_2 p_obligado_2" name="p'+num2+'_sueldo_2" id="p'+num2+'_sueldo_2" maxlength="8"><br></div><div class="col-6"><label for="p'+num2+'_aportacion">Aportación *</label><input type="text" class="form-control solo_numeros es_persona_2 p_obligado_2" name="p'+num2+'_aportacion_2" id="p'+num2+'_aportacion_2" maxlength="8"><br></div></div><div class="row p2_'+num2+'"><div class="col-6"><label for="p'+num2+'_muebles">Muebles e inmuebles *</label><input type="text" class="form-control es_persona_2 p_obligado_2" name="p'+num2+'_muebles_2" id="p'+num2+'_muebles_2"><br></div><div class="col-6"><label for="p'+num2+'_adeudo">Adeudo *</label><select name="p'+num2+'_adeudo_2" id="p'+num2+'_adeudo_2" class="form-control es_persona_2 p_obligado_2"><option value="">Selecciona</option><option value="0">No</option><option value="1">Sí</option></select><br></div></div><br><div class="row p2_'+num2+'"><div class="col-md-4 offset-md-4"><a href="javascript:void(0)" class="btn btn-danger" onclick="borrarPersona('+num2+')">Borrar esta persona</a><br><br><br><br></div></div>';
		$("#div_personas_2").append(item2);
	}
	function borrarPersona(numero){
		$(".p"+numero).empty();
		$(".p"+numero).removeClass('alert','alert-secondary');
		num--;
	}
	function borrarPersonaTipo2(numero){
		$(".p2_"+numero).empty();
		$(".p2_"+numero).removeClass('alert','alert-secondary');
		num--;
	}
	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	function respaldoTxt(formdata,nombreArchivo){      
	    var textFileAsBlob = new Blob([formdata], {type:'text/plain'});
	    var fileNameToSaveAs = nombreArchivo+".txt";
	    var downloadLink = document.createElement("a");
	    downloadLink.download = fileNameToSaveAs;
	    downloadLink.innerHTML = "My Hidden Link";
	    window.URL = window.URL || window.webkitURL;
	    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	    downloadLink.onclick = destroyClickedElement;
	    downloadLink.style.display = "none";
	    document.body.appendChild(downloadLink);
	    downloadLink.click();
	}
	function destroyClickedElement(event){
	    document.body.removeChild(event.target);
	}
	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
	//Se crea la variable para establecer la fecha actual
  	var hoy = new Date();
  	var dd = hoy.getDate();
  	var mm = hoy.getMonth()+1;
  	var yyyy = hoy.getFullYear();
  	var hora = hoy.getHours()+":"+hoy.getMinutes();

  	if(dd<10) {
      	dd='0'+dd;
  	} 

  	if(mm<10) {
      	mm='0'+mm;
  	}

  	$(".tipo_fecha").datetimepicker({
  		minView: 2,
    	format: "dd/mm/yyyy",
    	startView: 4,
    	autoclose: true,
    	todayHighlight: true,
    	pickerPosition: "bottom-left",
    	forceParse: false
  	});
  	//Se establece que el calendario comienzo en la fecha actual
  	$('.tipo_fecha').datetimepicker('setEndDate', hoy);
	</script>
</body>
</html>