<div class="box-header tituloSubseccion">
                  			<p class="box-title" id="titulo_personal"><strong>  Personal data</strong><hr></p>
                		</div>
                		<form id="d_personal">
	                		<div class="row">
				        		<div class="col-md-4">
				        			<label for="nombre">Name *</label>
				        			<input type="text" class="form-control personal_obligado" name="nombre" id="nombre" readonly>
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="paterno">First lastname *</label>
				        			<input type="text" class="form-control personal_obligado" name="paterno" id="paterno" readonly>
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="materno">Second lastname *</label>
				        			<input type="text" class="form-control personal_obligado" name="materno" id="materno" readonly>
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
				        		<div class="col-md-2">
				        			<label for="fecha_nacimiento">Birthdate *</label>
				        			<input type="text" class="form-control solo_lectura personal_obligado" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="mm/dd/yyyy" readonly>
				        			<br>
				        		</div>
				        		<div class="col-md-1">
				        			<label for="edad">Age *</label>
				        			<input type="text" class="form-control" id="edad" placeholder="Age" disabled>
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="puesto">Job Position Requested *</label>
				        			<input type="text" class="form-control personal_obligado" name="puesto" id="puesto" placeholder="Job Position Requested">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="nacionalidad">Nationality *</label>
				        			<input type="text" class="form-control personal_obligado" name="nacionalidad" id="nacionalidad" placeholder="Nationality">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="genero">Gender: *</label>
				        			<select name="genero" id="genero" class="form-control personal_obligado">
							            <option value="-1">Select</option>
							            <option value="1">Male</option>
							            <option value="2">Female</option>
						          	</select>
						          	<br>
				        		</div>		
					        </div>
				        	<div class="row">
					        	<div class="col-md-4">
				        			<label for="calle">Address *</label>
				        			<input type="text" class="form-control personal_obligado" name="calle" id="calle" placeholder="Address">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="exterior">Ext. Num. *</label>
				        			<input type="text" class="form-control personal_obligado" name="exterior" id="exterior" placeholder="Ext. Num" maxlength="6">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="interior">Int. Num. </label>
				        			<input type="text" class="form-control" name="interior" id="interior" placeholder="Int. Num." maxlength="5">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="colonia">Neighborhood *</label>
				        			<input type="text" class="form-control personal_obligado" name="colonia" id="colonia" placeholder="Neighborhood">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-4">
				        			<label for="estado">State *</label>
				        			<select name="estado" id="estado" class="form-control personal_obligado">
							            <option value="-1">Select</option>
							            <?php foreach ($estados as $e) {?>
							                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="municipio">City *</label>
				        			<select name="municipio" id="municipio" class="form-control personal_obligado" disabled>
							            <option value="-1">Select</option>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="cp">Zip Code *</label>
				        			<input type="text" class="form-control solo_numeros personal_obligado" name="cp" id="cp" placeholder="Zip Code" maxlength="5">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-3">
				        			<label for="civil">Marital Status *</label>
				        			<select name="civil" id="civil" class="form-control personal_obligado">
							            <option value="-1">Select</option>
							            <option value="1">Married</option>
							            <option value="2">Single</option>
							            <option value="3">Divorced</option>
							            <option value="4">Union free</option>
							            <option value="5">Widowed</option>
							            <option value="6">Separated</option>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-3">
				        			<label for="telefono">Mobile Number *</label>
				        			<input type="text" class="form-control solo_numeros personal_obligado" name="telefono" id="telefono" placeholder="Mobile Number" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
				        			<label for="tel_casa">Home Number </label>
				        			<input type="text" class="form-control solo_numeros" name="tel_casa" id="tel_casa" placeholder="Home Number" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
				        			<label for="tel_otro">Number to leave Messages </label>
				        			<input type="text" class="form-control solo_numeros" name="tel_otro" id="tel_otro" placeholder="Number to leave Messages" maxlength="10">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-3">
				        			<label for="correo">Email *</label>
				        			<input type="text" class="form-control personal_obligado" name="correo" id="correo" readonly>
				        			<br>
				        		</div>
					        </div>
						    <div class="row">
					        	<div class="col-md-3 col-md-offset-5">
					        		<button type="submit" class="btn btn-primary" id="actualizarDatos">Actualizar Datos Personales</button>
					        		<br><br>
					        	</div>
					        </div>
					   	</form>