<div class="modal fade" id="nuevoModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrar examen antidoping</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="nuevo">
          <div class="row">
            <!--div class="col-12 text-center">
              <h5 id="clave"><b>Clave a registrar: Pendiente</b></h5>
            </div-->
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Nombre(s) *</label>
              <input type="text" class="form-control nuevo_obligado" name="nombre" id="nombre" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Primer apellido *</label>
              <input type="text" class="form-control nuevo_obligado" name="paterno" id="paterno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
            <div class="col-md-6">
              <label>Segundo apellido</label>
              <input type="text" class="form-control" name="materno" id="materno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Cliente *</label>
              <select name="cliente" id="cliente" class="form-control nuevo_obligado">
                <option value="">Selecciona</option>
                <?php
                foreach ($clientes as $cl) { ?>
                    <option value="<?php echo $cl->id; ?>"><?php echo $cl->nombre; ?></option>
                <?php
                }
                ?>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Subcliente *</label>
              <select name="subcliente" id="subcliente" class="form-control nuevo_obligado" disabled>
                <option value="0">Selecciona</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Proyecto *</label>
              <select name="proyecto" id="proyecto" class="form-control nuevo_obligado" disabled>
                <option value="0">Selecciona</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Parámetros a aplicar *</label>
              <select name="paquete" id="paquete" class="form-control nuevo_obligado" disabled>
                <option value="">Selecciona</option>
                <?php
                foreach ($paquetes as $paq) { ?>
                  <option value="<?php echo $paq->id; ?>"><?php echo $paq->nombre; ?></option>
                <?php }
                ?>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Fecha de nacimiento *</label>
              <input type="text" class="form-control nuevo_obligado" name="nuevo_fecha_nacimiento" id="nuevo_fecha_nacimiento">
              <br>
            </div>
            <div class="col-md-4">
              <label>Tipo identificación *</label>
              <select name="nuevo_identificacion" id="nuevo_identificacion" class="form-control nuevo_obligado">
                <option value="">Selecciona</option>
                <?php
                foreach ($identificaciones as $ide) { ?>
                  <option value="<?php echo $ide->id; ?>"><?php echo $ide->nombre; ?></option>
                <?php }
                ?>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Número, licencia o código *</label>
              <input type="text" class="form-control nuevo_obligado" name="nuevo_ine" id="nuevo_ine">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Razón *</label>
              <select name="nuevo_razon" id="nuevo_razon" class="form-control nuevo_obligado">
                <option value="">Selecciona</option>
                <option value="1">Nuevo ingreso</option>
                <option value="2">Aleatorio</option>
              </select>
              <br>
            </div>
            <div class="col-md-6">
              <label>Medicamentos recetados * </label>
              <textarea name="nuevo_medicamentos" id="nuevo_medicamentos" class="form-control nuevo_obligado" rows="2"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Fecha de doping *</label>
              <input type="text" class="form-control nuevo_obligado" name="nuevo_fecha_doping" id="nuevo_fecha_doping">
              <br>
            </div>
            <div class="col-md-6">
              <label>Foto</label><span id="previa_foto"></span>
              <input type="file" id="nuevo_foto" name="nuevo_foto" class="form-control" accept=".jpg, .jpeg, .png"><br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Comentarios </label>
              <textarea name="nuevo_comentarios" id="nuevo_comentarios" class="form-control" rows="2"></textarea>
              <br>
            </div>
          </div>
          <div id="msj_error" class="alert alert-danger hidden"></div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" id="btnRegistro" onclick="guardarDoping()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="pendientesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Solicitudes de doping pendientes</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row" id="row_candidato">
          <div class="col-md-12">
            <label>Candidato *</label>
            <select name="candidato" id="candidato" class="form-control solo_lectura selectpicker" data-live-search="true" data-size="10">
              <option value="">Selecciona</option>
              <?php
              if ($candidatos) {
                foreach ($candidatos as $c) {
                  $cliente = ($c->cliente != "" && $c->cliente != null) ? ' - ' . $c->cliente : '';
                  $subcliente = ($c->subcliente != "" && $c->subcliente != null) ? ' - ' . $c->subcliente : '';
                  $proyecto = ($c->proyecto != "" && $c->proyecto != null) ? ' - ' . $c->proyecto : ''; ?>
                  <option value="<?php echo $c->idCandidato; ?>"><?php echo $c->nombre . ' ' . $c->paterno . ' ' . $c->materno . $cliente . $subcliente . $proyecto; ?></option>
              <?php }
              } ?>
            </select>
            <br><br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-11 text-center" id="parametros">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <label>Fecha de nacimiento *</label>
            <input type="text" class="form-control dop_obligado" name="fecha_nacimiento" id="fecha_nacimiento">
            <br>
          </div>
          <div class="col-md-4">
            <label>Tipo identificación *</label>
            <select name="identificacion" id="identificacion" class="form-control dop_obligado">
              <option value="">Selecciona</option>
              <?php
              foreach ($identificaciones as $ide) { ?>
                <option value="<?php echo $ide->id; ?>"><?php echo $ide->nombre; ?></option>
              <?php }
              ?>
            </select>
            <br>
          </div>
          <div class="col-md-4">
            <label>Número, licencia o código *</label>
            <input type="text" class="form-control dop_obligado" name="ine" id="ine">
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <label>Razón *</label>
            <select name="razon" id="razon" class="form-control dop_obligado">
              <option value="">Selecciona</option>
              <option value="1">Nuevo ingreso</option>
              <option value="2">Aleatorio</option>
            </select>
            <br>
          </div>
          <div class="col-md-4">
            <label>Fecha de doping *</label>
            <input type="text" class="form-control dop_obligado" name="fecha_doping" id="fecha_doping">
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Medicamentos recetados * </label>
            <textarea name="medicamentos" id="medicamentos" class="form-control dop_obligado" rows="2"></textarea>
            <br>
          </div>
          <div class="col-md-6">
            <label>Foto</label>
            <input type="file" id="foto" name="foto" class="form-control" accept=".jpg, .jpeg, .png"><br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <label>Comentarios </label>
            <textarea name="comentarios" id="comentarios" class="form-control" rows="2"></textarea>
            <br>
          </div>
        </div>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="registrarPendiente()">Registrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="editarpendienteModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edición de doping pendiente</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row" id="row_candidato">
          <div class="col-md-8 col-md-offset-3">
            <div id="editarCandidato"></div>
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <label>Tipo identificación *</label>
            <select name="edicion_identificacion" id="edicion_identificacion" class="form-control edicion_obligado">
              <option value="">Selecciona</option>
              <?php
              foreach ($identificaciones as $ide) { ?>
                <option value="<?php echo $ide->id; ?>"><?php echo $ide->nombre; ?></option>
              <?php }
              ?>
            </select>
            <br>
          </div>
          <div class="col-md-4">
            <label>Número, licencia o código *</label>
            <input type="text" class="form-control edicion_obligado" name="edicion_ine" id="edicion_ine">
            <br>
          </div>
          <div class="col-md-4">
            <label>Razón *</label>
            <select name="edicion_razon" id="edicion_razon" class="form-control edicion_obligado">
              <option value="">Selecciona</option>
              <option value="1">Nuevo ingreso</option>
              <option value="2">Aleatorio</option>
            </select>
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <label>Fecha de doping *</label>
            <input type="text" class="form-control edicion_obligado" name="edicion_fecha_doping" id="edicion_fecha_doping">
            <br>
          </div>
          <div class="col-md-4">
            <label>Medicamentos recetados * </label>
            <textarea name="edicion_medicamentos" id="edicion_medicamentos" class="form-control edicion_obligado" rows="2"></textarea>
            <br>
          </div>
          <div class="col-md-4">
            <label>Foto</label><span id="edicion_previa_foto"></span>
            <input type="file" id="edicion_foto" name="edicion_foto" class="form-control" accept=".jpg, .jpeg, .png"><br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <label>Comentarios </label>
            <textarea name="edicion_comentarios" id="edicion_comentarios" class="form-control" rows="2"></textarea>
            <br>
          </div>
        </div>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="editarPendiente()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="titulo_accion"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 id="nombre_candidato"></h4><br>
        <p class="" id="detalles"></p><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="resultadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Resultados del examen antidoping</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="row">
          <div class="col-md-12 text-center">
            <b><span id="titulo_candidato"></span></b><br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <b><span id="titulo_prueba"></span></b><br><br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <label>Fecha resultado *</label>
            <input type="text" class="form-control sust_obligado" name="nuevo_fecha_resultado" id="nuevo_fecha_resultado"><br><br>
          </div>
        </div>
        <form id="results">
          <div id="div_resultados"></div>
        </form>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="registrarResultados()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="titulo_accion"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="" id="texto_confirmacion"></p><br>
        <div class="row" id="div_commentario">
          <div class="col-md-12">
            <label for="motivo">Motivo *</label>
            <textarea name="motivo" id="motivo" class="form-control" rows="3"></textarea>
            <br>
          </div>
        </div>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" id="btnGuardar" onclick="ejecutarAccion()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="eliminadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Examenes antidoping eliminados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="div_eliminados">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="labModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Laboratorio del examen de antidoping</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="text-left" id="lab_nombre_candidato"></p><br>
        <select name="opcion_laboratorio" id="opcion_laboratorio" class="form-control">
          <option value="GUADALAJARA">GUADALAJARA</option>
          <option value="LAPI">LAPI</option>
        </select>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="actualizarLab()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="finalizadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Exámenes antidoping finalizados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <label>Busca al candidato *</label>
            <select name="candidato_finalizado" id="candidato_finalizado" class="form-control solo_lectura selectpicker" data-live-search="true">
              <option value="">Selecciona</option>
              <?php
              if ($finalizados) {
                foreach ($finalizados as $f) {
                  $sub = ($f->subcliente != '' && $f->subcliente != null) ? " - " . $f->subcliente : "";
                  $proyecto = ($f->proyecto != '' && $f->proyecto != null) ? " - " . $f->proyecto : ""; ?>
                  <option value="<?php echo $f->id; ?>"><?php echo $f->id . ' - ' . $f->candidato . ' - ' . $f->cliente . $sub . $proyecto; ?></option>
              <?php }
              } ?>
            </select>
            <br><br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" id="detalle_finalizado">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script>
  $("#nuevoModal").on("hidden.bs.modal", function() {
    $("#nuevoModal input, #nuevoModal select, textarea").val("");
    $("#nuevoModal #msj_error").css('display', 'none');
    $("#previa_foto").empty();
    $("#subcliente, #paquete, #proyecto").prop('disabled', true);
  });
  $("#pendientesModal").on("hidden.bs.modal", function() {
    $("#pendientesModal input, #pendientesModal select, #pendientesModal textarea").val("");
    $("#candidato").val('').trigger('change');
    $("#parametros").empty();
    $("#pendientesModal #msj_error").css('display', 'none');
  });
  $("#quitarModal").on("hidden.bs.modal", function() {
    $("#quitarModal #msj_error").css('display', 'none');
    $("#quitarModal textarea").val('');
  });
  $("#resultadosModal").on("hidden.bs.modal", function() {
    $("#resultadosModal input").val('');
    $("#div_resultados").empty();
    $("#resultadosModal #msj_error").css('display', 'none');
  });
  $("#finalizadosModal").on("hidden.bs.modal", function() {
    $("#candidato_finalizado").val("").trigger('change');
    $("#detalle_finalizado").empty();
  });
</script>