<div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Registrar candidato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="nuevoRegistroForm">
          <div class="row">
            <div class="col-md-4">
              <label>Subcliente (Proveedor) *</label>
              <select name="subcliente" id="subcliente" class="form-control obligado">
                <option value="">Selecciona</option>
                <?php
                if ($subclientes) {
                  foreach ($subclientes as $sub) { ?>
                    <option value="<?php echo $sub->id; ?>"><?php echo $sub->nombre; ?></option>
                  <?php   }
                  echo '<option value="0">N/A</option>';
                } else { ?>
                  <option value="0">N/A</option>

                <?php } ?>
              </select>
              <br><br>
            </div>
            <div class="col-md-4">
              <label>Puesto *</label>
              <select name="puesto" id="puesto" class="form-control obligado">
                <option value="">Selecciona</option>
                <?php
                foreach ($puestos as $p) { ?>
                  <option value="<?php echo $p->id; ?>"><?php echo $p->nombre; ?></option>
                <?php
                } ?>
              </select>
              <br><br>
            </div>
            <div class="col-md-4">
              <label>Tipo de ESE *</label>
              <select name="proceso" id="proceso" class="form-control obligado" disabled>
                <option value="">Selecciona</option>
              </select>
              <br><br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Nombre(s) *</label>
              <input type="text" class="form-control obligado" name="nombre" id="nombre" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
            <div class="col-md-4">
              <label>Apellido paterno *</label>
              <input type="text" class="form-control obligado" name="paterno" id="paterno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
            <div class="col-md-4">
              <label>Apellido materno</label>
              <input type="text" class="form-control" name="materno" id="materno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Correo electrónico</label>
              <input type="email" class="form-control" name="correo" id="correo" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
              <br>
            </div>
            <div class="col-md-4">
              <label>Num. Tel. Celular *</label>
              <input type="text" class="form-control obligado" name="celular" id="celular" maxlength="16">
              <br>
            </div>
            <div class="col-md-4">
              <label>Num. Tel. Casa </label>
              <input type="text" class="form-control" name="fijo" id="fijo" maxlength="16">
              <br>
            </div>
          </div>
          <h4 class="text-center">Selecciona los estudios que requiere el candidato:</h4><br><br>
          <div class="row">
            <div class="col-sm-12 col-md-4">
              <label>Socioeconómico *</label>
              <select name="socio" id="socio" class="form-control">
                <option value="" selected>Selecciona</option>
                <option value="0">No</option>
                <option value="1">Si</option>
              </select>
              <br>
            </div>
            <div class="col-sm-12 col-md-4">
              <label>Antidoping *</label>
              <select name="antidoping" id="antidoping" class="form-control">
                <option value="" selected>Selecciona</option>
                <option value="1">Si</option>
                <option value="0">No</option>
              </select>
              <br>
            </div>
            <div class="col-sm-12 col-md-4">
              <label>Psicométrico *</label>
              <select name="psicometrico" id="psicometrico" class="form-control">
                <option value="" selected>Selecciona</option>
                <option value="1">Si</option>
                <option value="0">No</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-4">
              <label>Médico *</label>
              <select name="medico" id="medico" class="form-control">
                <option value="" selected>Selecciona</option>
                <option value="1">Si</option>
                <option value="0">No</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <label>Examen antidoping *</label>
              <select name="examen" id="examen" class="form-control" disabled>
                <option value="" selected>Selecciona</option>
                <?php
                foreach ($drogas as $d) { ?>
                  <option value="<?php echo $d->id; ?>"><?php echo $d->nombre . " (" . $d->conjunto . ")"; ?></option>
                <?php
                } ?>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <label>¿Requiere algo más para el candidato?</label>
              <textarea class="form-control" name="otro_requisito" id="otro_requisito" rows="3"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <label>Cargar CV o solicitud de empleo del candidato</label>
              <input type="file" id="cv" name="cv" class="form-control" accept=".pdf, .jpg, .jpeg, .png" multiple><br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="registrar()">Registrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="asignarCandidatoModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reasignar candidato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formAsignacion">
          <div class="row">
            <div class="col-md-12">
              <label>Candidatos *</label>
              <select name="asignar_candidato" id="asignar_candidato" class="form-control asignar_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($cands as $cand) { ?>
                  <option value="<?php echo $cand->id; ?>"><?php echo $cand->candidato; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Usuario *</label>
              <select name="asignar_usuario" id="asignar_usuario" class="form-control asignar_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($usuarios as $user) { ?>
                  <option value="<?php echo $user->id; ?>"><?php echo $user->usuario; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="AsignarCandidato()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mensajes de avances</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-header tituloSubseccion">
          <p class="box-title"><strong> Anteriores:</strong></p>
        </div>
        <div class="row" id="div_crearEstatusAvances"></div>
        <hr>
        <div class="margen" id="div_estatus_avances">
          <div class="box-header tituloSubseccion">
            <p class="box-title"><strong> Nuevos:</strong></p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="avances_estatus_comentario">Comentario / Estatus</label>
              <textarea class="form-control" name="avances_estatus_comentario" id="avances_estatus_comentario" rows="3"></textarea>
              <br>
            </div>
            <div class="col-md-6">
              <p class="text-center"><b>Fecha</b></p>
              <p class="text-center" id="fecha_estatus_avances"></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="adjunto">Adjuntar imagen</label>
              <input type="file" id="adjunto" name="adjunto" class="form-control" accept=".jpg, .jpeg, .png"><br>
            </div>
          </div>
          <div id="msj_error" class="alert alert-danger hidden"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="generarEstatusAvance()">Actualizar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="generalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Datos generales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_generales">
          <div class="row">
            <div class="col-md-4">
              <label>Nombre(s) *</label>
              <input type="text" class="form-control personal_obligado" name="nombre_general" id="nombre_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
            <div class="col-md-4">
              <label>Apellido paterno *</label>
              <input type="text" class="form-control personal_obligado" name="paterno_general" id="paterno_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
            <div class="col-md-4">
              <label>Apellido materno</label>
              <input type="text" class="form-control" name="materno_general" id="materno_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Fecha de nacimiento *</label>
              <input type="text" class="form-control solo_lectura personal_obligado" name="fecha_nacimiento" id="fecha_nacimiento">
              <br>
            </div>
            <div class="col-md-4">
              <label>Lugar de nacimiento *</label>
              <input type="text" class="form-control personal_obligado" name="lugar" id="lugar">
              <br>
            </div>
            <div class="col-md-4">
              <label>Puesto *</label>
              <select name="puesto_general" id="puesto_general" class="form-control personal_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($puestos as $pu) { ?>
                  <option value="<?php echo $pu->id; ?>"><?php echo $pu->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Género: *</label>
              <select name="genero" id="genero" class="form-control personal_obligado">
                <option value="">Selecciona</option>
                <option value="Masculino">Masculino</option>
                <option value="Femenino">Femenino</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Calle *</label>
              <input type="text" class="form-control personal_obligado" name="calle" id="calle">
              <br>
            </div>
            <div class="col-md-4">
              <label>No. Exterior *</label>
              <input type="text" class="form-control personal_obligado" name="exterior" id="exterior" maxlength="8">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>No. Interior </label>
              <input type="text" class="form-control" name="interior" id="interior" maxlength="8">
              <br>
            </div>
            <div class="col-md-4">
              <label>Entre calles</label>
              <input type="text" class="form-control" name="calles" id="calles">
              <br>
            </div>
            <div class="col-md-4">
              <label>Colonia *</label>
              <input type="text" class="form-control personal_obligado" name="colonia" id="colonia">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Estado *</label>
              <select name="estado" id="estado" class="form-control personal_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($estados as $e) { ?>
                  <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Municipio (o Delegación) *</label>
              <select name="municipio" id="municipio" class="form-control personal_obligado" disabled>
                <option value="">Selecciona</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Código postal *</label>
              <input type="text" class="form-control solo_numeros personal_obligado" name="cp" id="cp" maxlength="5">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Estado civil *</label>
              <select name="civil" id="civil" class="form-control personal_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($civiles as $civ) { ?>
                  <option value="<?php echo $civ->id; ?>"><?php echo $civ->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Teléfono celular *</label>
              <input type="text" class="form-control solo_numeros personal_obligado" name="celular_general" id="celular_general" maxlength="10">
              <br>
            </div>
            <div class="col-md-4">
              <label>Teléfono local </label>
              <input type="text" class="form-control solo_numeros" name="tel_casa" id="tel_casa" maxlength="10">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Grado máximo de estudios: *</label>
              <select name="grado" id="grado" class="form-control personal_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($grados as $gr) { ?>
                  <option value="<?php echo $gr->id; ?>"><?php echo $gr->nombre; ?></option>
                <?php } ?>
              </select>
              <br><br>
            </div>
            <div class="col-md-4">
              <label>Correo electrónico </label>
              <input type="text" class="form-control " name="correo_general" id="correo_general">
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="guardarGenerales()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="academicosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Historial académico del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_estudios">
          <div class="alert alert-info">
            <p class="text-center">Primaria </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Periodo</label>
              <input type="text" class="form-control estudios_obligado" name="prim_periodo" id="prim_periodo">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela</label>
              <input type="text" class="form-control estudios_obligado" name="prim_escuela" id="prim_escuela">
              <br>
            </div>
            <div class="col-md-4">
              <label>Ciudad</label>
              <input type="text" class="form-control estudios_obligado" name="prim_ciudad" id="prim_ciudad">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Certificado</label>
              <select name="prim_certificado" id="prim_certificado" class="form-control estudios_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Promedio</label>
              <input type="text" class="form-control estudios_obligado" name="prim_promedio" id="prim_promedio">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Secundaria </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Periodo</label>
              <input type="text" class="form-control estudios_obligado" name="sec_periodo" id="sec_periodo">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela</label>
              <input type="text" class="form-control estudios_obligado" name="sec_escuela" id="sec_escuela">
              <br>
            </div>
            <div class="col-md-4">
              <label>Ciudad</label>
              <input type="text" class="form-control estudios_obligado" name="sec_ciudad" id="sec_ciudad">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Certificado</label>
              <select name="sec_certificado" id="sec_certificado" class="form-control estudios_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Promedio</label>
              <input type="text" class="form-control estudios_obligado" name="sec_promedio" id="sec_promedio">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Carrera comercial </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Periodo</label>
              <input type="text" class="form-control estudios_obligado" name="comercial_periodo" id="comercial_periodo">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela</label>
              <input type="text" class="form-control estudios_obligado" name="comercial_escuela" id="comercial_escuela">
              <br>
            </div>
            <div class="col-md-4">
              <label>Ciudad</label>
              <input type="text" class="form-control estudios_obligado" name="comercial_ciudad" id="comercial_ciudad">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Certificado</label>
              <select name="comercial_certificado" id="comercial_certificado" class="form-control estudios_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Promedio</label>
              <input type="text" class="form-control estudios_obligado" name="comercial_promedio" id="comercial_promedio">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Bachillerato </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Periodo</label>
              <input type="text" class="form-control estudios_obligado" name="prep_periodo" id="prep_periodo">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela</label>
              <input type="text" class="form-control estudios_obligado" name="prep_escuela" id="prep_escuela">
              <br>
            </div>
            <div class="col-md-4">
              <label>Ciudad</label>
              <input type="text" class="form-control estudios_obligado" name="prep_ciudad" id="prep_ciudad">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Certificado</label>
              <select name="prep_certificado" id="prep_certificado" class="form-control estudios_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Promedio</label>
              <input type="text" class="form-control estudios_obligado" name="prep_promedio" id="prep_promedio">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Licenciatura </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Periodo</label>
              <input type="text" class="form-control estudios_obligado" name="lic_periodo" id="lic_periodo">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela</label>
              <input type="text" class="form-control estudios_obligado" name="lic_escuela" id="lic_escuela">
              <br>
            </div>
            <div class="col-md-4">
              <label>Ciudad</label>
              <input type="text" class="form-control estudios_obligado" name="lic_ciudad" id="lic_ciudad">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Certificado</label>
              <select name="lic_certificado" id="lic_certificado" class="form-control estudios_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Promedio</label>
              <input type="text" class="form-control estudios_obligado" name="lic_promedio" id="lic_promedio">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Estudios actuales </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Periodo</label>
              <input type="text" class="form-control estudios_obligado" name="actual_periodo" id="actual_periodo">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela</label>
              <input type="text" class="form-control estudios_obligado" name="actual_escuela" id="actual_escuela">
              <br>
            </div>
            <div class="col-md-4">
              <label>Ciudad</label>
              <input type="text" class="form-control estudios_obligado" name="actual_ciudad" id="actual_ciudad">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Certificado</label>
              <select name="actual_certificado" id="actual_certificado" class="form-control estudios_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Promedio</label>
              <input type="text" class="form-control estudios_obligado" name="actual_promedio" id="actual_promedio">
              <br>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <label>Cédula profesional (Número y fecha de emisión) *</label>
              <textarea class="form-control estudios_obligado" name="cedula" id="cedula" rows="3"></textarea>
              <br>
            </div>
            <div class="col-md-6">
              <label>Otros estudios *</label>
              <textarea class="form-control estudios_obligado" name="otro_certificado" id="otro_certificado" rows="3"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Periodos inactivos (fechas y motivos) *</label>
              <textarea class="form-control estudios_obligado" name="carrera_inactivo" id="carrera_inactivo" rows="3"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Comentarios *</label>
              <textarea class="form-control estudios_obligado" name="estudios_comentarios" id="estudios_comentarios" rows="3"></textarea>
              <br><br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger hidden"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="guardarEstudios()">Guardar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="docsModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Documentación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" class="idCandidato">
        <input type="hidden" class="prefijo">
        <div class="row">
          <div class="col-md-4 margen">
            <label>Selecciona el documento</label><br>
            <input type="file" id="documento" class="doc_obligado" name="documento" accept=".pdf, .jpg, .png, .jpeg"><br>
            <button class="btn btn-primary" onclick="subirDoc()">Subir</button>
          </div>
          <div id="tablaDocs" class="col-md-8 borde">

          </div>

        </div>
        <div class="row margen">
          <div id="campos_vacios">
            <p class="msj_error">No se ha seleccionado el tipo de archivo</p>
          </div>
          <div id="tipos_iguales">
            <p class="msj_error">Los documentos deben ser de diferente tipo</p>
          </div>
          <div id="sin_documento">
            <p class="msj_error">Seleccione un documento para subir</p>
          </div>
          <div id="subido">
            <p class="msj_error">El documento se ha subido con éxito</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" onclick="actualizarDocs()">Actualizar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="completarModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Finalizar proceso del candidato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="div_incompleto" style="display: none;">
          <h4 class="text-center" id="msjIncompleto"></h4>
        </div>
        <div id="div_completo" style="display: none;">
          <form id="formConclusiones">
            <div class="alert alert-info text-center">
              <p>Conclusión Personal</p>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label for="check_identidad">Descripción de datos personales *</label>
                <textarea class="form-control conclusion_obligado" name="personal1" id="personal1" rows="6"></textarea>
                <br>
              </div>
              <div class="col-md-6">
                <label for="check_identidad">Descripción de hábitos y referencias personales *</label>
                <textarea class="form-control conclusion_obligado" name="personal2" id="personal2" rows="6"></textarea>
                <br>
              </div>
            </div>
            <div class="alert alert-info text-center">
              <p>Conclusión Laboral</p>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label for="check_identidad">Número de referencias laborales señaladas *</label>
                <textarea class="form-control conclusion_obligado" name="laboral1" id="laboral1" rows="2"></textarea>
                <br>
              </div>
              <div class="col-md-6">
                <label for="check_identidad">Descripción de las referencias laborales *</label>
                <textarea class="form-control conclusion_obligado" name="laboral2" id="laboral2" rows="6"></textarea>
                <br>
              </div>
            </div>
            <div class="alert alert-info text-center">
              <p>Conclusión Socioeconómica</p>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label for="check_identidad">Descripción de la vivienda, zona y condiciones *</label>
                <textarea class="form-control conclusion_obligado" name="socio1" id="socio1" rows="6"></textarea>
                <br>
              </div>
              <div class="col-md-6">
                <label for="check_identidad">Descripción de gastos y referencias vecinales *</label>
                <textarea class="form-control conclusion_obligado" name="socio2" id="socio2" rows="6"></textarea>
                <br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 col-md-offset-4">
                <label>De acuerdo a lo anterior, la persona investigada es considerada: *</label>
                <select name="recomendable" id="recomendable" class="form-control conclusion_obligado">
                  <option value="">Selecciona</option>
                  <option value="2">No recomendable</option>
                  <option value="1">Recomendable</option>
                  <option value="3">Con reservas</option>
                </select>
                <br>
              </div>
            </div>
          </form>
          <div id="campos_vacios_check" class="alert alert-danger">
            <p class="text-center">Hay campos vacíos</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="finalizarProceso()">Guardar</button>
        <input type="hidden" id="idFinalizado">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="titulo_accion"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="" id="texto_confirmacion"></p><br>
        <div class="row" id="div_motivo">
          <div class="col-md-12">
            <label for="motivo">Motivo *</label>
            <textarea name="motivo" id="motivo" class="form-control" rows="3"></textarea>
            <br>
          </div>
        </div>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="ejecutarAccion()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="eliminadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Procesos cancelados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="div_listado"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="psicometriaModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Carga de archivo de psicometria</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 class="text-left" id="psicometria_candidato"></h4><br>
        <input type="hidden" name="idPsicometrico" id="idPsicometrico">
        <input id="doc_psicometria" name="doc_psicometria" class="psico_obligado" type="file" accept=".pdf"><br><br>
        <br>
        <div id="campos_vacios" class="alert alert-danger">
          <p class="msj_error text-white">No se ha seleccionado el archivo</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="subirPsicometria()">Guardar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="socialesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Antecedentes sociales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_sociales">
          <div class="row">
            <div class="col-md-4">
              <label for="sindical">¿Perteneció algún puesto sindical? *</label>
              <select name="sindical" id="sindical" class="form-control social_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label for="sindical_nombre">¿A cuál? *</label>
              <input type="text" class="form-control social_obligado" name="sindical_nombre" id="sindical_nombre">
              <br>
            </div>
            <div class="col-md-4">
              <label for="sindical_cargo">¿Cargo? *</label>
              <input type="text" class="form-control social_obligado" name="sindical_cargo" id="sindical_cargo">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="partido">¿Pertenece algún partido político? *</label>
              <select name="partido" id="partido" class="form-control social_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label for="partido_nombre">¿A cuál? *</label>
              <input type="text" class="form-control social_obligado" name="partido_nombre" id="partido_nombre">
              <br>
            </div>
            <div class="col-md-4">
              <label for="partido_cargo">¿Cargo? *</label>
              <input type="text" class="form-control social_obligado" name="partido_cargo" id="partido_cargo">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="club">¿Pertenece algún club deportivo? *</label>
              <select name="club" id="club" class="form-control social_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-6">
              <label for="deporte">¿Qué deporte practica? *</label>
              <input type="text" class="form-control social_obligado" name="deporte" id="deporte">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="religion">¿Qué religión profesa? *</label>
              <input type="text" class="form-control social_obligado" name="religion" id="religion">
              <br>
            </div>
            <div class="col-md-6">
              <label for="religion_frecuencia">¿Con qué frecuencia? *</label>
              <input type="text" class="form-control social_obligado" name="religion_frecuencia" id="religion_frecuencia">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <label for="bebidas">¿Ingiere bebidas alcohólicas? *</label>
              <select name="bebidas" id="bebidas" class="form-control social_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-3">
              <label for="bebidas_frecuencia">¿Con qué frecuencia? *</label>
              <input type="text" class="form-control social_obligado" name="bebidas_frecuencia" id="bebidas_frecuencia">
              <br>
            </div>
            <div class="col-md-3">
              <label for="fumar">¿Acostumbra fumar? *</label>
              <select name="fumar" id="fumar" class="form-control social_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
              </select>
              <br>
            </div>
            <div class="col-md-3">
              <label for="fumar_frecuencia">¿Con qué frecuencia? *</label>
              <input type="text" class="form-control social_obligado" name="fumar_frecuencia" id="fumar_frecuencia">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="cirugia">¿Ha tenido alguna intervención quirúrgica? *</label>
              <input type="text" class="form-control social_obligado" name="cirugia" id="cirugia">
              <br>
            </div>
            <div class="col-md-6">
              <label for="enfermedades">¿Antecedentes de enfermedades en su familia directa? *</label>
              <input type="text" class="form-control social_obligado" name="enfermedades" id="enfermedades">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="corto_plazo">¿Cuáles son sus planes a corto plazo? *</label>
              <input type="text" class="form-control social_obligado" name="corto_plazo" id="corto_plazo">
              <br>
            </div>
            <div class="col-md-6">
              <label for="mediano_plazo">¿Cuáles son sus planes a mediano plazo? *</label>
              <input type="text" class="form-control social_obligado" name="mediano_plazo" id="mediano_plazo">
              <br><br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="guardarSociales">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="refPersonalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Referencias personales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_refpersonal1">
          <div class="alert alert-info">
            <p class="text-center">Primer referencia </p>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="refPer1_nombre">Nombre *</label>
              <input type="text" class="form-control refPer1_obligado" name="refPer1_nombre" id="refPer1_nombre">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="refPer1_tiempo">Tiempo de conocerlo *</label>
              <input type="text" class="form-control refPer1_obligado" name="refPer1_tiempo" id="refPer1_tiempo">
              <br>
            </div>
            <div class="col-md-4">
              <label for="refPer1_telefono">Teléfono *</label>
              <input type="text" class="form-control solo_numeros refPer1_obligado" name="refPer1_telefono" id="refPer1_telefono" maxlength="10">
              <br>
            </div>
            <div class="col-md-4">
              <label for="refPer1_recomienda">¿La recomienda? *</label>
              <select name="refPer1_recomienda" id="refPer1_recomienda" class="form-control refPer1_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
                <option value="2">N/A</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="hidden" id="refPer1_id">
              <label for="refPer1_comentario">Comentarios *</label>
              <textarea class="form-control refPer1_obligado" name="refPer1_comentario" id="refPer1_comentario" rows="2"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-md-offset-4">
              <button type="button" class="btn btn-primary" onclick="guardarPersonales(1)">Actualizar Referencia Personal #1</button>
              <br><br>
            </div>
          </div>
          <br>
        </form>
        <form action="" id="d_refpersonal2">
          <div class="alert alert-info">
            <p class="text-center">Segunda referencia </p>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="refPer2_nombre">Nombre *</label>
              <input type="text" class="form-control refPer2_obligado" name="refPer2_nombre" id="refPer2_nombre">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="refPer2_tiempo">Tiempo de conocerlo *</label>
              <input type="text" class="form-control refPer2_obligado" name="refPer2_tiempo" id="refPer2_tiempo">
              <br>
            </div>
            <div class="col-md-4">
              <label for="refPer2_telefono">Teléfono *</label>
              <input type="text" class="form-control solo_numeros refPer2_obligado" name="refPer2_telefono" id="refPer2_telefono" maxlength="10">
              <br>
            </div>
            <div class="col-md-4">
              <label for="refPer2_recomienda">¿La recomienda? *</label>
              <select name="refPer2_recomienda" id="refPer2_recomienda" class="form-control refPer2_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
                <option value="2">N/A</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="hidden" id="refPer2_id">
              <label for="refPer2_comentario">Comentarios *</label>
              <textarea class="form-control refPer2_obligado" name="refPer2_comentario" id="refPer2_comentario" rows="2"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-md-offset-4">
              <button type="button" class="btn btn-primary" onclick="guardarPersonales(2)">Actualizar Referencia Personal #2</button>
              <br><br>
            </div>
          </div>
          <br>
        </form>
        <form action="" id="d_refpersonal3">
          <div class="alert alert-info">
            <p class="text-center">Tercer referencia </p>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="refPer3_nombre">Nombre *</label>
              <input type="text" class="form-control refPer3_obligado" name="refPer3_nombre" id="refPer3_nombre">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="refPer3_tiempo">Tiempo de conocerlo *</label>
              <input type="text" class="form-control refPer3_obligado" name="refPer3_tiempo" id="refPer3_tiempo">
              <br>
            </div>
            <div class="col-md-4">
              <label for="refPer3_telefono">Teléfono *</label>
              <input type="text" class="form-control solo_numeros refPer3_obligado" name="refPer3_telefono" id="refPer3_telefono" maxlength="10">
              <br>
            </div>
            <div class="col-md-4">
              <label for="refPer3_recomienda">¿La recomienda? *</label>
              <select name="refPer3_recomienda" id="refPer3_recomienda" class="form-control refPer3_obligado">
                <option value="">Selecciona</option>
                <option value="0">No</option>
                <option value="1">Sí</option>
                <option value="2">N/A</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="hidden" id="refPer3_id">
              <label for="refPer3_comentario">Comentarios *</label>
              <textarea class="form-control refPer3_obligado" name="refPer3_comentario" id="refPer3_comentario" rows="2"></textarea>
              <br>
            </div>
          </div>
        </form>
        <div class="row">
          <div class="col-md-3 col-md-offset-4">
            <button type="button" class="btn btn-primary" onclick="guardarPersonales(3)">Actualizar Referencia Personal #3</button>
            <br><br>
          </div>
        </div>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <!--button type="button" class="btn btn-success" id="guardarSociales">Guardar</button-->
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="legalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Investigación legal del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_investigacion">
          <div class="row">
            <div class="col-md-6">
              <label>Penal *</label>
              <input type="text" class="form-control inv_obligado" name="inv_penal" id="inv_penal">
              <br>
            </div>
            <div class="col-md-6">
              <label>Notas *</label>
              <textarea cols="2" class="form-control inv_obligado" name="inv_penal_notas" id="inv_penal_notas"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Civil *</label>
              <input type="text" class="form-control inv_obligado" name="inv_civil" id="inv_civil">
              <br>
            </div>
            <div class="col-md-6">
              <label>Notas *</label>
              <textarea cols="2" class="form-control inv_obligado" name="inv_civil_notas" id="inv_civil_notas"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Laboral *</label>
              <input type="text" class="form-control inv_obligado" name="inv_laboral" id="inv_laboral">
              <br>
            </div>
            <div class="col-md-6">
              <label>Notas *</label>
              <textarea cols="2" class="form-control inv_obligado" name="inv_laboral_notas" id="inv_laboral_notas"></textarea>
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="actualizarInvestigacion()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="noMencionadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Trabajos no mencionados del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_no_mencionados">
          <div class="row">
            <div class="col-md-6">
              <label>Trabajos no mencionados *</label>
              <textarea cols="2" class="form-control nomen_obligado" name="no_mencionados" id="no_mencionados"></textarea>
              <br>
            </div>
            <div class="col-md-6">
              <label>Resultado *</label>
              <textarea cols="2" class="form-control nomen_obligado" name="resultado_no_mencionados" id="resultado_no_mencionados"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Notas *</label>
              <textarea cols="2" class="form-control nomen_obligado" name="notas_no_mencionados" id="notas_no_mencionados"></textarea>
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="actualizarNoMencionados()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="visitaDocumentosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Documentos de la visita del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_documentos">
          <div class="alert alert-info">
            <p class="text-center">Acta de nacimiento * </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_acta">Fecha de expedición *</label>
              <input type="text" class="form-control docs_obligado tipo_fecha" name="fecha_acta" id="fecha_acta">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_acta">Número y/o vigencia *</label>
              <input type="text" class="form-control docs_obligado" name="numero_acta" id="numero_acta">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Comprobante de Domicilio * </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_domicilio">Fecha de expedición *</label>
              <input type="text" class="form-control docs_obligado tipo_fecha" name="fecha_domicilio" id="fecha_domicilio">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_domicilio">Número y/o vigencia *</label>
              <input type="text" class="form-control docs_obligado" name="numero_domicilio" id="numero_domicilio">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Credencial de elector </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_ine">Fecha de expedición </label>
              <input type="text" class="form-control tipo_fecha" name="fecha_ine" id="fecha_ine" maxlength="4">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_ine">Número y/o vigencia </label>
              <input type="text" class="form-control" name="numero_ine" id="numero_ine">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">CURP * </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_curp">Fecha de expedición *</label>
              <input type="text" class="form-control docs_obligado tipo_fecha" name="fecha_curp" id="fecha_curp">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_curp">Número y/o vigencia *</label>
              <input type="text" class="form-control docs_obligado" name="numero_curp" id="numero_curp" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Afiliación al IMSS </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_imss">Fecha de expedición </label>
              <input type="text" class="form-control tipo_fecha" name="fecha_imss" id="fecha_imss">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_imss">Número y/o vigencia </label>
              <input type="text" class="form-control" name="numero_imss" id="numero_imss">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Comprobante retención de impuestos </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_retencion">Fecha de expedición </label>
              <input type="text" class="form-control tipo_fecha" name="fecha_retencion" id="fecha_retencion">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_retencion">Número y/o vigencia </label>
              <input type="text" class="form-control" name="numero_retencion" id="numero_retencion">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">RFC * </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_rfc">Fecha de expedición *</label>
              <input type="text" class="form-control docs_obligado tipo_fecha" name="fecha_rfc" id="fecha_rfc">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_rfc">Número y/o vigencia *</label>
              <input type="text" class="form-control docs_obligado" name="numero_rfc" id="numero_rfc" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Licencia para conducir </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_licencia">Fecha de expedición </label>
              <input type="text" class="form-control tipo_fecha" name="fecha_licencia" id="fecha_licencia">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_licencia">Número y/o vigencia </label>
              <input type="text" class="form-control" name="numero_licencia" id="numero_licencia">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Vigencia migratoria (extranjeros) </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_migra">Fecha de expedición </label>
              <input type="text" class="form-control tipo_fecha" name="fecha_migra" id="fecha_migra">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_migra">Número y/o vigencia </label>
              <input type="text" class="form-control" name="numero_migra" id="numero_migra">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">VISA Norteamericana </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="fecha_visa">Fecha de expedición </label>
              <input type="text" class="form-control tipo_fecha" name="fecha_visa" id="fecha_visa">
              <br>
            </div>
            <div class="col-md-6">
              <label for="numero_visa">Número y/o vigencia </label>
              <input type="text" class="form-control" name="numero_visa" id="numero_visa">
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="guardarDocumentacion">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="familiaresModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Datos del grupo familiar del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_familia">
          <div id="div_familiares">

          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="egresosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Egresos mensuales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_egresos">
          <div class="row">
            <div class="col-md-4">
              <label for="renta">Renta *</label>
              <input type="text" class="form-control solo_numeros e_obligado" name="renta" id="renta" maxlength="8">
              <br>
            </div>
            <div class="col-md-4">
              <label for="alimentos">Alimentos *</label>
              <input type="text" class="form-control solo_numeros e_obligado" name="alimentos" id="alimentos" maxlength="8">
              <br>
            </div>
            <div class="col-md-4">
              <label for="servicios">Servicios *</label>
              <input type="text" class="form-control solo_numeros e_obligado" name="servicios" id="servicios">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="transportes">Transportes *</label>
              <input type="text" class="form-control solo_numeros e_obligado" name="transportes" id="transportes">
              <br>
            </div>
            <div class="col-md-4">
              <label for="otros_gastos">Otros *</label>
              <input type="text" class="form-control solo_numeros e_obligado" name="otros_gastos" id="otros_gastos">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="solvencia">Cuando los egresos son mayores a los ingresos, ¿cómo los solventa? *</label>
              <textarea class="form-control e_obligado" name="solvencia" id="solvencia" rows="2"></textarea>
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="actualizarEgresos()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="visitaHabitacionModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Habitacion y medio ambiente del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_habitacion">
          <div class="row">
            <div class="col-md-6">
              <label for="tiempo_residencia">Tiempo de residencia en el domicilio actual *</label>
              <input type="text" class="form-control h_obligado" name="tiempo_residencia" id="tiempo_residencia">
              <br>
            </div>
            <div class="col-md-6">
              <label for="nivel_zona">Nivel de la zona *</label>
              <select name="nivel_zona" id="nivel_zona" class="form-control h_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($zonas as $z) { ?>
                  <option value="<?php echo $z->id; ?>"><?php echo $z->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="tipo_vivienda">Tipo de vivienda *</label>
              <select name="tipo_vivienda" id="tipo_vivienda" class="form-control h_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($viviendas as $viv) { ?>
                  <option value="<?php echo $viv->id; ?>"><?php echo $viv->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
            <div class="col-md-6">
              <label for="recamaras">Recámaras *</label>
              <input type="number" class="form-control h_obligado" name="recamaras" id="recamaras">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="banios">Baños *</label>
              <input type="text" class="form-control h_obligado" name="banios" id="banios">
              <br>
            </div>
            <div class="col-md-6">
              <label for="distribucion">Distribución *</label>
              <textarea class="form-control h_obligado" name="distribucion" id="distribucion" rows="2"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="calidad_mobiliario">Calidad mobiliario *</label>
              <select name="calidad_mobiliario" id="calidad_mobiliario" class="form-control h_obligado">
                <option value="">Selecciona</option>
                <option value="1">Buena</option>
                <option value="2">Regular</option>
                <option value="3">Deficiente</option>
              </select>
              <br>
            </div>
            <div class="col-md-6">
              <label for="mobiliario">Mobiliario *</label>
              <select name="mobiliario" id="mobiliario" class="form-control h_obligado">
                <option value="">Selecciona</option>
                <option value="0">Incompleto</option>
                <option value="1">Completo</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="tamanio_vivienda">Tamaño vivienda *</label>
              <select name="tamanio_vivienda" id="tamanio_vivienda" class="form-control h_obligado">
                <option value="">Selecciona</option>
                <option value="1">Amplia</option>
                <option value="2">Media</option>
                <option value="3">Reducida</option>
              </select>
              <br>
            </div>
            <div class="col-md-6">
              <label for="condiciones_vivienda">Condiciones de la vivienda *</label>
              <select name="condiciones_vivienda" id="condiciones_vivienda" class="form-control h_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($condiciones as $cond) { ?>
                  <option value="<?php echo $cond->id; ?>"><?php echo $cond->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="actualizarHabitacion()">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="refVecinalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Referencias vecinales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_refVecinal1">
          <div class="alert alert-info">
            <p class="text-center">Primer referencia </p>
            <input type="hidden" id="idrefvec1">
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="vecino1_nombre">Nombre *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_nombre" id="vecino1_nombre">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino1_domicilio">Domicilio *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_domicilio" id="vecino1_domicilio">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino1_tel">Teléfono *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_tel" id="vecino1_tel">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino1_concepto">¿Qué concepto tiene del aspirante? *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_concepto" id="vecino1_concepto">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino1_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_familia" id="vecino1_familia">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino1_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_civil" id="vecino1_civil">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino1_hijos">¿Tiene hijos? *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_hijos" id="vecino1_hijos">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino1_sabetrabaja">¿Sabe en dónde trabaja? *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_sabetrabaja" id="vecino1_sabetrabaja">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino1_notas">Notas *</label>
              <input type="text" class="form-control vec1_obligado" name="vecino1_notas" id="vecino1_notas">
              <br>
            </div>
          </div>

        </form>
        <div class="row">
          <div class="col-md-3 col-md-offset-4">
            <button type="button" class="btn btn-primary" onclick="guardarVecinales(1)">Actualizar Referencia Vecinal #1</button>
            <br><br>
          </div>
        </div>
        <br>
        <form id="d_refVecinal2">
          <div class="alert alert-info">
            <p class="text-center">Segunda referencia </p>
            <input type="hidden" id="idrefvec2">
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="vecino2_nombre">Nombre *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_nombre" id="vecino2_nombre">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino2_domicilio">Domicilio *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_domicilio" id="vecino2_domicilio">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino2_tel">Teléfono *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_tel" id="vecino2_tel">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino2_concepto">¿Qué concepto tiene del aspirante? *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_concepto" id="vecino2_concepto">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino2_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_familia" id="vecino2_familia">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino2_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_civil" id="vecino2_civil">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino2_hijos">¿Tiene hijos? *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_hijos" id="vecino2_hijos">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino2_sabetrabaja">¿Sabe en dónde trabaja? *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_sabetrabaja" id="vecino2_sabetrabaja">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino2_notas">Notas *</label>
              <input type="text" class="form-control vec2_obligado" name="vecino2_notas" id="vecino2_notas">
              <br><br>
            </div>
          </div>
        </form>
        <div class="row">
          <div class="col-md-3 col-md-offset-4">
            <button type="button" class="btn btn-primary" onclick="guardarVecinales(2)">Actualizar Referencia Vecinal #2</button>
            <br><br>
          </div>
        </div>
        <br>
        <form id="d_refVecinal3">
          <div class="alert alert-info">
            <p class="text-center">Tercera referencia </p>
            <input type="hidden" id="idrefvec3">
          </div>
          <div class="row">
            <div class="col-md-12">
              <label for="vecino2_nombre">Nombre *</label>
              <input type="text" class="form-control vec3_obligado" name="vecino3_nombre" id="vecino3_nombre">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino3_domicilio">Domicilio *</label>
              <input type="text" class="form-control vec3_obligado" name="vecino3_domicilio" id="vecino3_domicilio">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino3_tel">Teléfono *</label>
              <input type="text" class="form-control vec3_obligado" name="vecino3_tel" id="vecino3_tel">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino3_concepto">¿Qué concepto tiene del aspirante? *</label>
              <input type="text" class="form-control vec_obligado" name="vecino3_concepto" id="vecino3_concepto">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino3_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
              <input type="text" class="form-control vec_obligado" name="vecino3_familia" id="vecino3_familia">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino3_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
              <input type="text" class="form-control vec_obligado" name="vecino3_civil" id="vecino3_civil">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino3_hijos">¿Tiene hijos? *</label>
              <input type="text" class="form-control vec3_obligado" name="vecino3_hijos" id="vecino3_hijos">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="vecino3_sabetrabaja">¿Sabe en dónde trabaja? *</label>
              <input type="text" class="form-control vec3_obligado" name="vecino3_sabetrabaja" id="vecino3_sabetrabaja">
              <br>
            </div>
            <div class="col-md-6">
              <label for="vecino3_notas">Notas *</label>
              <input type="text" class="form-control vec3_obligado" name="vecino3_notas" id="vecino3_notas">
              <br><br>
            </div>
          </div>
        </form>
        <div class="row">
          <div class="col-md-3 col-md-offset-4">
            <button type="button" class="btn btn-primary" onclick="guardarVecinales(3)">Actualizar Referencia Vecinal #3</button>
            <br><br>
          </div>
        </div>
        <br>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <!--button type="button" class="btn btn-success" id="">Guardar</button-->
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="generales2Modal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Datos generales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_generales2">
          <div class="row">
            <div class="col-md-4">
              <label for="nombre_general">Nombre(s) *</label>
              <input type="text" class="form-control personal2_obligado" name="nombre_ingles" id="nombre_ingles" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
            <div class="col-md-4">
              <label for="paterno_general">Apellido paterno *</label>
              <input type="text" class="form-control personal2_obligado" name="paterno_ingles" id="paterno_ingles" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
            <div class="col-md-4">
              <label for="materno_general">Apellido materno</label>
              <input type="text" class="form-control" name="materno_ingles" id="materno_ingles" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="fecha_nacimiento">Fecha de nacimiento *</label>
              <input type="text" class="form-control solo_lectura personal2_obligado" name="fecha_nacimiento_ingles" id="fecha_nacimiento_ingles">
              <br>
            </div>
            <div class="col-md-4">
              <label for="edad">Edad *</label>
              <input type="text" class="form-control" id="edad_ingles" disabled>
              <br>
            </div>
            <div class="col-md-4">
              <label for="lugar">Nacionalidad *</label>
              <input type="text" class="form-control personal2_obligado" name="nacionalidad_ingles" id="nacionalidad_ingles">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="puesto_general">Puesto *</label>
              <input type="text" class="form-control personal2_obligado" name="puesto_ingles" id="puesto_ingles">
              <br>
            </div>
            <div class="col-md-4">
              <label for="genero">Género *</label>
              <select name="genero_ingles" id="genero_ingles" class="form-control personal2_obligado">
                <option value="">Selecciona</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label for="calle">Calle *</label>
              <input type="text" class="form-control personal2_obligado" name="calle_ingles" id="calle_ingles">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="exterior">No. Exterior *</label>
              <input type="text" class="form-control personal2_obligado" name="exterior_ingles" id="exterior_ingles" maxlength="8">
              <br>
            </div>
            <div class="col-md-4">
              <label for="interior">No. Interior </label>
              <input type="text" class="form-control" name="interior_ingles" id="interior_ingles" maxlength="8">
              <br>
            </div>
            <div class="col-md-4">
              <label for="calles">Entre calles</label>
              <input type="text" class="form-control" name="calles_ingles" id="calles_ingles">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="colonia">Colonia *</label>
              <input type="text" class="form-control personal2_obligado" name="colonia_ingles" id="colonia_ingles">
              <br>
            </div>
            <div class="col-md-4">
              <label for="estado">Estado *</label>
              <select name="estado_ingles" id="estado_ingles" class="form-control personal2_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($estados as $e) { ?>
                  <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label for="municipio">Municipio (o Delegación) *</label>
              <select name="municipio_ingles" id="municipio_ingles" class="form-control personal2_obligado" disabled>
                <option value="">Selecciona</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="cp">Código postal *</label>
              <input type="text" class="form-control solo_numeros personal2_obligado" name="cp_ingles" id="cp_ingles" maxlength="5">
              <br>
            </div>
            <div class="col-md-4">
              <label for="civil">Estado civil *</label>
              <select name="civil_ingles" id="civil_ingles" class="form-control personal2_obligado">
                <option value="">Selecciona</option>
                <option value="1">Married</option>
                <option value="2">Single</option>
                <option value="3">Divorced</option>
                <option value="4">Free union</option>
                <option value="5">Widowed</option>
                <option value="6">Separated</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label for="celular_general">Teléfono celular *</label>
              <input type="text" class="form-control solo_numeros personal2_obligado" name="celular_general_ingles" id="celular_general_ingles" maxlength="10">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label for="tel_casa">Teléfono local </label>
              <input type="text" class="form-control solo_numeros" name="tel_casa_ingles" id="tel_casa_ingles" maxlength="10">
              <br>
            </div>
            <div class="col-md-4">
              <label for="correo">Correo </label>
              <input type="text" class="form-control" name="personales_correo_ingles" id="personales_correo_ingles">
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="guardarGenerales2">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="globalesGeneralModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Búsquedas globales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_globales_general">
          <div class="row">
            <div class="col-md-4">
              <label>Law enforcement *</label>
              <input type="text" class="form-control global_general_obligado" name="law_enforcement" id="law_enforcement">
              <br>
            </div>
            <div class="col-md-4">
              <label>Regulatory *</label>
              <input type="text" class="form-control global_general_obligado" name="regulatory" id="regulatory">
              <br>
            </div>
            <div class="col-md-4">
              <label>Sanctions *</label>
              <input type="text" class="form-control global_general_obligado" name="sanctions" id="sanctions">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Other bodies *</label>
              <input type="text" class="form-control global_general_obligado" name="other_bodies" id="other_bodies">
              <br>
            </div>
            <div class="col-md-4">
              <label>Web and media searches *</label>
              <input type="text" class="form-control global_general_obligado" name="media_searches" id="media_searches">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Comentarios *</label>
              <input type="text" class="form-control global_general_obligado" name="global_comentarios" id="global_comentarios">
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="guardarGlobalesGeneral">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="identidadModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Verificación de identidad del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_identidad">
          <div class="alert alert-info">
            <p class="text-center">INE (IFE) </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>ID</label>
              <input type="text" class="form-control" name="ine" id="ine">
              <br>
            </div>
            <div class="col-md-6">
              <label>Año de registro</label>
              <input type="text" class="form-control" name="ine_ano" id="ine_ano">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Número vertical</label>
              <input type="text" class="form-control" name="ine_vertical" id="ine_vertical">
              <br>
            </div>
            <div class="col-md-6">
              <label>Fecha / Institución</label>
              <input type="text" class="form-control" name="ine_institucion" id="ine_institucion">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Pasaporte </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>ID</label>
              <input type="text" class="form-control" name="pasaporte" id="pasaporte">
              <br>
            </div>
            <div class="col-md-6">
              <label>Fecha / Institución</label>
              <input type="text" class="form-control" name="pasaporte_fecha" id="pasaporte_fecha">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Carta de Antecedentes No Penales (Carta policía)</p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>ID</label>
              <input type="text" class="form-control" name="penales_id" id="penales_id">
              <br>
            </div>
            <div class="col-md-6">
              <label>Fecha / Institución</label>
              <input type="text" class="form-control" name="penales_institucion" id="penales_institucion">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Forma migratoria </p>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>ID</label>
              <input type="text" class="form-control" name="forma_migratoria" id="forma_migratoria">
              <br>
            </div>
            <div class="col-md-6">
              <label>Fecha / Institución</label>
              <input type="text" class="form-control" name="forma_migratoria_fecha" id="forma_migratoria_fecha">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Comentarios *</label>
              <input type="text" class="form-control identidad_obligado" name="identidad_comentarios" id="identidad_comentarios">
              <br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="guardarVerificacionIdentidad">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="mayoresEstudiosModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mayores estudios del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="d_mayor_estudios">
          <div class="alert alert-info">
            <p class="text-center">Candidato </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Nivel escolar *</label>
              <select name="mayor_estudios" id="mayor_estudios" class="form-control mayor_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($studies as $st) { ?>
                  <option value="<?php echo $st->id; ?>"><?php echo $st->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Periodo *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_periodo" id="estudios_periodo">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_escuela" id="estudios_escuela">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Ciudad *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_ciudad" id="estudios_ciudad">
              <br>
            </div>
            <div class="col-md-4">
              <label>Certificado obtenido *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_certificado" id="estudios_certificado">
              <br>
            </div>
          </div>
          <div class="alert alert-info">
            <p class="text-center">Analista </p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Nivel escolar *</label>
              <select name="mayor_estudios2" id="mayor_estudios2" class="form-control mayor_obligado">
                <option value="">Selecciona</option>
                <?php foreach ($studies as $st) { ?>
                  <option value="<?php echo $st->id; ?>"><?php echo $st->nombre; ?></option>
                <?php } ?>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Periodo *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_periodo2" id="estudios_periodo2">
              <br>
            </div>
            <div class="col-md-4">
              <label>Escuela *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_escuela2" id="estudios_escuela2">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Ciudad *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_ciudad2" id="estudios_ciudad2">
              <br>
            </div>
            <div class="col-md-4">
              <label>Certificado obtenido *</label>
              <input type="text" class="form-control mayor_obligado" name="estudios_certificado2" id="estudios_certificado2">
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Comentarios *</label>
              <textarea class="form-control mayor_obligado" name="mayor_estudios_comentarios" id="mayor_estudios_comentarios" rows="3"></textarea>
              <input type="hidden" id="idMayoresEstudios">
              <br><br>
            </div>
          </div>
        </form>
        <div id="msj_error" class="alert alert-danger">
          <p id="msj_texto" class="text-white"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="actualizarMayoresEstudios">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="verificacionPenalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Verificación de antecedentes no penales del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="idVerificacionPenales">
        <div class="box-header tituloSubseccion">
          <p class="box-title"><strong> Anteriores:</strong></p>
        </div>
        <div class="row" id="div_crearEstatusPenales">
          <p class="text-center">Sin registros </p>
        </div>
        <hr>
        <div class="margen" id="div_estatus_penales">
          <div class="box-header tituloSubseccion">
            <p class="box-title"><strong> Nuevos:</strong></p>
          </div>
          <div class="row">
            <div class="col-md-3">
              <p class="text-center"><b>Fecha</b></p>
              <p class="text-center" id="fecha_estatus_penales"></p>
            </div>
            <div class="col-md-9">
              <label for="penales_estatus_comentario">Comentario / Estatus</label>
              <textarea class="form-control" name="penales_estatus_comentario" id="penales_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
              <br>
            </div>

          </div>
          <div class="row">
            <div class="col-md-3 col-md-offset-5">
              <a class="btn btn-app btn_verificacion" onclick="generarEstatusPenales()">Actualizar estatus</a>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnTerminarVerificacionPenales" class="btn btn-danger" data-toggle="modal" data-target="#confirmarPenalesModal">Terminar Verificación</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirmarPenalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirmación de antecedentes no penales</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4>¿Estás segura de terminar la verificación de antecedentes no penales de <b><span class="nombreCandidato"></span></b>?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" onclick="terminarPenales()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="finalizarInglesModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Finalizar proceso del candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formChecks">
          <div class="row">
            <div class="col-md-4">
              <label>Verificación de Identidad *</label>
              <select name="check_identidad" id="check_identidad" class="form-control check_obligado">
                <option value="">Selecciona</option>
                <option value="1">Aprobado</option>
                <option value="0">No Aprobado</option>
                <option value="2">A consideración</option>
                <option value="3">NA</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Verificación de Global Data Searches *</label>
              <select name="check_global" id="check_global" class="form-control check_obligado">
                <option value="">Selecciona</option>
                <option value="1">Aprobado</option>
                <option value="0">No Aprobado</option>
                <option value="2">A consideración</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Verificación de Antecedentes No Penales *</label>
              <select name="check_penales" id="check_penales" class="form-control check_obligado">
                <option value="">Selecciona</option>
                <option value="1">Aprobado</option>
                <option value="0">No Aprobado</option>
                <option value="2">A consideración</option>
                <option value="3">NA</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label>Employment History Check *</label>
              <select name="check_laboral" id="check_laboral" class="form-control check_obligado">
                <option value="">Selecciona</option>
                <option value="1">Aprobado</option>
                <option value="0">No Aprobado</option>
                <option value="2">A consideración</option>
                <option value="3">NA</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Academic History Check *</label>
              <select name="check_estudios" id="check_estudios" class="form-control check_obligado">
                <option value="">Selecciona</option>
                <option value="1">Aprobado</option>
                <option value="0">No Aprobado</option>
                <option value="2">A consideración</option>
                <option value="3">NA</option>
              </select>
              <br>
            </div>
            <div class="col-md-4">
              <label>Verificación OFAC *</label>
              <select name="check_ofac" id="check_ofac" class="form-control check_obligado">
                <option value="">Selecciona</option>
                <option value="1">Aprobado</option>
                <option value="0">No Aprobado</option>
                <option value="2">A consideración</option>
              </select>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Declaración final *</label>
              <textarea class="form-control check_obligado" name="comentario_final" id="comentario_final" rows="3"></textarea>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <label>Estatus final del Estudio *</label>
              <select name="bgc_status" id="bgc_status" class="form-control check_obligado">
                <option value="">Selecciona</option>
                <option value="1">Recomendable</option>
                <option value="2">No recomendable</option>
                <option value="3">A consideración</option>
              </select>
              <br>
            </div>
          </div>
        </form>
        <div id="campos_vacios_check">
          <p class="msj_error text-center">Hay campos vacíos</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="finalizarProcesoIngles()">Terminar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="passModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Password Generated</h5>

      </div>
      <div class="modal-body">
        <h4>Data login on form</h4><br>
        <p><b>User: </b><span id="user"></span></p>
        <p><b>Password: </b><span id="pass"></span></p><br>
        <p id="respuesta_mail"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="actualizarCandidatoModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Actualizar candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="">
          <p class="text-center"><b>IMPORTANTE:</b> Para actualizar al candidato considera los siguientes puntos: <br><br>
          <ul>
            <li>Se reanudarán los resultados (estatus) finales del estudio actual</li>
            <li>La información recabada en la visita será eliminada para que se vuelva a realizar</li>
            <li>El archivo PDF del estudio finalizado actual se respaldará.</li>
            <li>La mayoría de la información del candidato (que no entra en la visita) se matendrá para su actualización</li>
            <li>El examen antidoping se va requerir nuevamente</li>
            <li>La psicometría y/o el examen médico (en caso de tener) se podrán actualizar </li>
            <li>Los puntos anteriores se aplicarán en las siguientes actualizaciones (a reserva de que haya cambios posteriores)</li>
          </ul><br>
          </p>
          <p class="text-center">¿Desea continuar?</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="actualizarCandidato()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="revisionModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Última revisión al candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Antes de finalizar tu estudio, realiza una ultima revisión de la ortografía, redacción y asegúrate que la información registrada sea la correcta. Para este estudio, se contemplan las siguientes secciones:</p><br>
        <ul>
          <li>Datos generales</li>
          <li>Historial académico</li>
          <li>Antecedentes sociales</li>
          <li>Referencias personales</li>
          <li>Antecedentes laborales</li>
          <li>Investigación legal</li>
          <li>Trabajos no mencionados</li>
          <li>Documentos en la visita</li>
          <li>Grupo familiar en la visita</li>
          <li>Egresos mensuales</li>
          <li>Habitación y medio ambiente</li>
          <li>Referencias vecinales</li>
        </ul><br>
        <p>En caso de que se presente un error o alguna imformación no se pueda cambiar, favor de avisar a TI en cuanto antes del cambio a solicitar </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar y regresar a revisar</button>
        <button type="button" class="btn btn-danger" onclick="aceptarRevision()">Acepto que he revisado el estudio</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="revision2Modal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Última revisión al candidato: <span class="nombreCandidato"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Antes de finalizar tu estudio, realiza una ultima revisión de la ortografía, redacción y asegúrate que la información registrada sea la correcta. Para este estudio, se contemplan las siguientes secciones:</p><br>
        <ul>
          <li>Datos generales</li>
          <li>Verificación de identidad</li>
          <li>Búsquedas globales</li>
        </ul><br>
        <p>En caso de que se presente un error o alguna imformación no se pueda cambiar, favor de avisar a TI en cuanto antes del cambio a solicitar </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar y regresar a revisar</button>
        <button type="button" class="btn btn-danger" onclick="aceptarRevision2()">Acepto que he revisado el estudio</button>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    $("#newModal").on("hidden.bs.modal", function() {
			$("#newModal input, #newModal select, #newModal textarea").val('');
      $("#newModal #subcliente").val('').change()
			$("#newModal #msj_error").css('display', 'none');
		})
    $("#asignarCandidatoModal").on("hidden.bs.modal", function() {
			$("#asignarCandidatoModal select").val('');
			$("#asignarCandidatoModal #msj_error").css('display', 'none');
		})
    $('#avancesModal').on('hidden.bs.modal', function(e) {
      $("#avancesModal #msj_error").css('display', 'none');
      $("#avancesModal input, #avancesModal textarea").val('');
    });/*
    $('#generalesModal').on('hidden.bs.modal', function(e) {
      $("#generalesModal #msj_error").css('display', 'none');
      $("#generalesModal input, #avancesModal select").val('');
    });*/
  })
</script>