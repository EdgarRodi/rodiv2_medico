<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Formulario de Requisición</title>
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<!-- Select Bootstrap -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<!-- Sweetalert 2 -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.12.7/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>img/favicon.jpg" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/custom.css">
</head>
<body>
	<div id="exito" class="alert alert-success mensaje" style='display:none;'>
      	<strong>¡Success!</strong> The form has been send succesfully.
  	</div>
  	<div id="empty" class="alert alert-danger mensaje" style='display:none;'>
      	<strong>¡Attention!</strong> There are empty required fields.
  	</div>
  	<div id="no_file" class="alert alert-danger mensaje" style='display:none;'>
      	<strong>¡Attention!</strong> Please select files.
  	</div>
  	<div id="no_check" class="alert alert-danger mensaje" style='display:none;'>
      	<strong>¡Attention!</strong> Please check the privacy notice.
  	</div>
	<div class="modal fade" id="errorModal" role="dialog">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal">&times;</button>
	        		<h4 class="modal-title">¡Something is wrong!</h4>
	      		</div>
	      		<div class="modal-body">
	        		<p class="text-red">Contact the admin please.</p>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="mensajeModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Form completed</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<p>The next step is to upload your documents. When you have them all. You must access to this link with your same credentials in order to upload your documents in pdf, jpg or png formats (max. 2MB each)</p>
	    		</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="avisoModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-xl">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Privacy notice</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<p>En relación con su empleo o solicitud de empleo, es necesario que RO & DI GLOBAL MARKETING S DE RL DE CV (en lo sucesivo La Empresa) recopile, reciba y administre ciertos datos personales incluyendo, de manera enunciativa más no limitativa: <br>a) Nombre, domicilio, número telefónico, dirección de correo electrónico, fotografías/video de su domicilio actual y otra información de contacto; <br>b) Nacionalidad, ciudadanía, registro federal de contribuyentes, número de seguro social y estado civil. <br>c) Información relativa a su empleo actual y empleos anteriores, puesto que ocupa actualmente y los puestos que ocupo anteriormente, permiso de trabajo, su experiencia laboral y sueldo, historial de INFONAVIT, IMMS y de otras instituciones públicas. <br>d) Habilidades y aptitudes, incluido su dominio de idiomas, su educación, incluidos los títulos obtenidos y las instituciones a las que asistió, y <br>e) Datos de familiares, cónyuge e hijos, según sea el caso, y cualquier otra información que en relación a la presente resulte necesaria, entre otros. Dichos datos personales podrán ser recopilados en cualquier momento por la empresa o cualquieras otras entidades subsidiarias o afiliadas a La Empresa. Por medio de la presente se hace de su conocimiento que algunos de los datos personales que serán recabados pudieran ser considerados como Datos Personales Sensibles conforme a la <b>Ley de Protección de Datos Personales en posesión de Particulares.</b></p>
	      			<p>La empresa podrá utilizar su información y datos personales, para diversos fines y para actividades de negocios como son las siguientes: <br>a) Para prestarle servicios de colocación de personal, tramitar empleos o asignaciones temporales para Usted, o para que lo enviemos a usted con un contratante. b) Para evaluar si está Usted calificado para un puesto o una función.<br>c) Para comunicarnos con Usted en relación con los puestos o servicios disponibles que ofrecemos. <br>d) Para informar a nuestros clientes y socios de egocios acerca de nuestros servicios. <br>e) Para llevar a cabo investigaciones y análisis, así como para generar perfiles y estructuras laborales que permitan incrementar y mejorar la productividad, así como para cualesquier otros fines que La Empresa considere necesarios para el mejoramiento de las condiciones de trabajo, y para resolver o defender quejas y demandas legales.</p>
	      			<p>La empresa podrá compartir y/o transferir su información y datos personales a sus compañías filiales y subsidiarias nacionales y a terceros, incluyendo de manera enunciativa más no limitativa, clientes, proveedores, asesores, consultores y socios de negocios, nacionales. También podemos compartir información: <br>a) Con clientes que puedan contar con oportunidades de empleo disponibles o que tengan interés en contratar nuestros candidatos o empleados. <br>b) Cuando alguna autoridad gubernamental o diverso funcionario gubernamental responsable de hacer cumplir la ley solicite o requiera razonablemente dicha información, o cuando lo exija la ley o en respuesta a algún proceso legal. <br>c) Cuando sea razonablemente necesario para llevar a cabo una investigación en relación con actividades ilegales sospechosas o reales. <br> <b>La empresa tratara su información y datos personales como confidenciales y mantendrá medidas preventivas dirigidas a protegerla contra perdida, mal uso, acceso no autorizado, alteraciones o destrucción, no divulgarla para otro propósito que no sea el establecido con el presente Aviso de Privacidad.</b></p>
	      			<p>La Empresa comunica sus directrices y normas en materia de privacidad y seguridad a nuestros empleados, clientes y proveedores. Al firmar el presente Aviso de Privacidad, Usted reconoce que entiende y acepta la recopilación y transmisión de su información y datos personales por parte de La Empresa según se señala en el presente Aviso de Privacidad. En cumplimiento a lo dispuesto en <b>la Ley Federal para la Protección de Datos Personales en Posesión de Particulares</b> se le informa lo siguiente: <br> 	      				Domicilio de La Empresa: Benito Juárez # 5693, Col. Santa María del Pueblito, Zapopan, Jalisco <br>Datos de Contacto: VISITADOR Tel. (33)14044054 <br>Derechos ARCO: Conforme a la<b> Ley Federal para la Protección de Datos Personales en Posesión de Particulares</b> Usted tiene el derecho de ejercer en cualquier momento sus derechos de acceso, rectificación, cancelación y oposición (los  Derechos ARCO”) de su información, mediante una solicitud por escrito dirigido a el VISITADOR, quién podrá solicitarle para su protección y beneficio, documentación que acredite correcciones a los datos en caso de que solicite rectificación de los mismos.</p>
	      			<p>Usted tiene derecho de acceder a sus datos personales que poseemos y a los detalles del tratamiento de los mismos siempre y cuando nos contacte en el periodo de tiempo que la información permanezca en la empresa comprendido hasta que se envía dicha información  al contratante (que a partir de ese momento es eliminada de nuestra base de datos) o si no es enviada tendrá 10 días hábiles después de otorgar la información, ya que después de éste periodo de tiempo se borra toda información relacionada. Sin embargo, la empresa no se hace responsable del uso que haga el contratante del estudio socioeconómico una vez que recibe la información, por lo que es necesario contactar directamente a la contratante. También puede cancelarlos cuando considere que no se requieren para alguna de las finalidades señalados en el presente aviso de privacidad, estén siendo utilizados para finalidades no consentidas o haya finalizado la relación contractual o de servicio, o bien, oponerse al tratamiento de los mismos para fines específicos. Se hace de su conocimiento que no podrá limitar el uso de sus datos personales cuando dicho uso se requiera para el cumplimiento de cualquier obligación a cargo de La Empresas.</p><br>
	      			<p>Atentamente <br>RO & DI GLOBAL MARKETING S DE RL DE CV <br>VISITADOR</p>
	      			<div class="row">
	      				<div class="col-md-4 offset-md-5">
	      					<button type="button" class="btn btn-primary" data-dismiss="modal">Acepto</button>
	      				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="nondisclosureModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-xl">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Privacy notice</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<p>In connection with your employment or application for employment, RO & DI GLOBAL MARKETING S DE RL DE CV (hereinafter The Company) is required to collect, receive and administer certain personal data including, but not limited to:<br>a) Name, address, telephone number, e-mail address, photographs/video of your current address and other contact information;<br>b) Nationality, citizenship, federal taxpayer registry, social security number and marital status.<br>c) Information regarding your current and previous employment, current and previous positions, work permit, work experience and salary, INFONAVIT, IMMS and other public institution history.<br>d) Skills and abilities, including language proficiency, education, including degrees obtained and institutions attended; and <br>e) Data on family members, spouse and children, as the case may be, and any other information that may be necessary in relation to the present, among others. Such personal data may be collected at any time by the Company or any other subsidiary or affiliated entities of The Company. You are hereby informed that some of the personal data that will be collected may be considered as Sensitive Personal Data in accordance with the Law on the Protection of Personal Data in the possession of Individuals.</b></p>
	      			<p>The company may use your information and personal data for various purposes and for business activities such as the following: <br>a) To provide you with personnel placement services, to process jobs or temporary assignments for you, or for us to send you with a contractor. B) To evaluate whether you are qualified for a position or function.<br>c) To contact you regarding available positions or services we offer.<br>d) To inform our customers and business partners about our services. <br>e) To carry out research and analysis, as well as to generate job profiles and structures to increase and improve productivity, as well as for any other purposes that The Company deems necessary for the improvement of working conditions, and to resolve or defend complaints and legal claims.</p>
	      			<p>The Company may share and/or transfer your personal information and data to its affiliated companies and national subsidiaries and to third parties, including, but not limited to, customers, suppliers, advisors, consultants and national business partners. We may also share information: <br>a) With customers who may have available employment opportunities or who have an interest in hiring our candidates or employees.<br>b) When such information is requested or reasonably required by any governmental authority or other governmental official responsible for law enforcement, or when required by law or in response to legal process. <br>c) When reasonably necessary to conduct an investigation into suspected or actual illegal activities. <br> <b>The company will treat your personal information and data as confidential and will maintain preventive measures aimed at protecting it against loss, misuse, unauthorized access, alteration or destruction, not to disclose it for any purpose other than that established with this Privacy Notice.</b></p>
	      			<p>The Company communicates its guidelines and rules on privacy and security to our employees, customers and suppliers. <br> By signing this Privacy Notice, you acknowledge that you understand and accept the collection and transmission of your information and personal data by The Company as outlined in this Privacy Notice.  In compliance with the provisions of the <b>Federal Law for the Protection of Personal Data in Possession of Individuals </b> we inform you of the following: <br> Company Address: Benito Juárez # 5693, Col. Santa María del Pueblito, Zapopan, Jalisco <br>Contact Information: VISITOR Tel. (33)14044054 <br>ARCO Rights: According to the <b> Federal Law for the Protection of Personal Data in Possession of Individuals</b> You have the right to exercise at any time your rights of access, rectification, cancellation and opposition (the "ARCO Rights") of your information, through a written request addressed to the VISITOR, who may request for your protection and benefit, documentation that accredits corrections to the data in case you request rectification of them.</p>
	      			<p>You have the right to access to your personal data that we have and to the details of the treatment of the same as long as you contact us in the period of time that the information stays in the company understood until the information is sent to the contracting party (that from that moment is eliminated of our data base) or if it is not sent you will have 10 working days after granting the information, since after this period of time all related information is erased. However, the company is not responsible for the use made by the contractor of the socioeconomic study once it receives the information, so it is necessary to contact the contractor directly. You can also cancel them when you consider that they are not required for any of the purposes indicated in this privacy notice, are being used for non-consented purposes or have terminated the contractual relationship or service, or oppose the treatment of them for specific purposes. You are hereby informed that you may not limit the use of your personal data when such use is required for the fulfilment of any obligation on the part of The Companies.</p><br>
	      			<p>Sincerely <br>RO & DI GLOBAL MARKETING S DE RL DE CV <br>VISITOR</p>
	      			<div class="row">
	      				<div class="col-md-4 offset-md-5">
	      					<button type="button" class="btn btn-primary" data-dismiss="modal">I agree</button>
	      				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="checkAvisoModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-xl modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Important notice</h4>
	      			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<p>Before starting the process, it is important that you download, read, sign and send us the Non-disclosure agreement (image or PDF format).</p><br>
	      			<p>You can download the Non-disclosure agreement  <a class="btn btn-info" href="<?php echo base_url()."privacy_notice/non-disclosure.pdf" ?>" target="_blank">Here</a> Once you have uploaded the file, the form will be enabled to start the process.</p><br><br>
	      			<div class="row">
	      				<div class="col-12">
	      					<label>Select the file *</label><br>
	      					<input id="doc_aviso" type="file" name="doc_aviso" accept=".pdf, .jpg, .jpeg, .png" multiple><br><br>
	      				</div>
	      			</div>
	      			<div id="msj_error" class="alert alert-danger hidden"></div>
	    		</div>
	    		<div class="modal-footer">
			        <button type="button" class="btn btn-success" onclick="subirAviso()">Upload the Non-disclosure agreement</button>
		      	</div>
	  		</div>
		</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light" id="menu">
		  	<a class="navbar-brand text-light" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		(<?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno')." ".$this->session->userdata('materno'); ?>)
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $this->session->userdata('correo'); ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" id="closeSession" href="<?php echo base_url(); ?>index.php/Login/logout">Sign out</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
	<div class="alert alert-info">
  	<h5 class="text-center">* All fields are required</h5>
  </div>
	<?php 
	if($tiene_aviso > 0){ ?>
			<div class="formulario contenedor mt-5">
				<form method="post" id="datos" enctype="multipart/form-data">
					<div class="card my-5">
					  	<h5 class="card-header text-center seccion">Personal Data</h5>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Name *</label>
				        			<div class="input-group mb-3">
									  <div class="input-group-prepend">
									    <span class="input-group-text"><i class="fas fa-user"></i></span>
									  </div>
				        				<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $this->session->userdata('nombre'); ?>" disabled>
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>First lastname *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-user"></i></span>
										</div>
				        			<input type="text" class="form-control" name="paterno" id="paterno" value="<?php echo $this->session->userdata('paterno'); ?>" disabled>
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Second lastname *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-user"></i></span>
										</div>
				        				<input type="text" class="form-control" name="materno" id="materno" value="<?php echo $this->session->userdata('materno'); ?>" disabled>
			        				</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Email *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="far fa-envelope"></i></span>
										</div>
				        				<input type="text" class="form-control" name="correo" id="correo" value="<?php echo $this->session->userdata('correo'); ?>" disabled>
			        				</div>
				        		</div>
							</div>
							<br>
							<div class="row">
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Birthdate *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-birthday-cake"></i></span>
										</div>
				        				<input type="text" class="form-control obligado" name="fecha_nacimiento" id="fecha_nacimiento">
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Job Position Requested: *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
										</div>
				        				<input type="text" class="form-control obligado" name="puesto" id="puesto">
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Nationality: *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-globe-americas"></i></span>
										</div>
				        				<input type="text" class="form-control obligado" name="nacionalidad" id="nacionalidad">
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Gender: *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-venus-mars"></i></span>
										</div>
					        			<select name="genero" id="genero" class="form-control obligado">
								            <option value="-1">Select</option>
								            <option value="Male">Male</option>
								            <option value="Female">Female</option>
							          	</select>
						          	</div>
				        		</div>		
					        </div>
					        <br>
					        <div class="row">
					        	<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Marital Status *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-user-friends"></i></span>
										</div>
					        			<select name="civil" id="civil" class="form-control obligado">
								            <option value="-1">Select</option>
								            <option value="1">Married</option>
								            <option value="2">Single</option>
								            <option value="3">Divorced</option>
								            <option value="4">Free Union</option>
								            <option value="5">Widowed</option>
								            <option value="6">Separated</option>
							          	</select>
							        </div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Mobile Number *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
										</div>
				        				<input type="text" class="form-control solo_numeros obligado" name="telefono" id="telefono" maxlength="10" value="<?php echo $this->session->userdata('celular'); ?>" disabled>
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Home Number </label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
										</div>
				        				<input type="text" class="form-control solo_numeros" name="tel_casa" id="tel_casa" maxlength="10">
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Number to leave Messages </label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-sms"></i></span>
										</div>
				        				<input type="text" class="form-control solo_numeros" name="tel_otro" id="tel_otro" maxlength="10">
				        			</div>
				        		</div>
					        </div>
					        <br>
					        <div class="row">
					        	<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Address *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-home"></i></span>
										</div>
				        				<input type="text" class="form-control obligado" name="calle" id="calle">
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Ext. Num. *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-home"></i></span>
										</div>
				        				<input type="text" class="form-control obligado" name="exterior" id="exterior" maxlength="6">
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Int. Num. </label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-home"></i></span>
										</div>
				        				<input type="text" class="form-control" name="interior" id="interior" maxlength="5">
				        			</div>
				        		</div>
				        		<div class="col-sm-12 col-md-3 col-lg-3">
				        			<label>Neighborhood *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
										</div>
				        				<input type="text" class="form-control obligado" name="colonia" id="colonia">
				        			</div>
				        		</div>
					        </div>
					        <br>
					        <div class="row">
					        	<div class="col-sm-12 col-md-4 col-lg-4">
				        			<label>State *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-university"></i></span>
										</div>
					        			<select name="estado" id="estado" class="form-control obligado">
								            <option value="-1">Select</option>
								            <?php foreach ($estados as $e) {?>
								                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
								            <?php } ?>
							          	</select>
							        </div>
				        		</div>
				        		<div class="col-sm-12 col-md-4 col-lg-4">
				        			<label>City *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-city"></i></span>
										</div>
					        			<select name="municipio" id="municipio" class="form-control obligado" disabled>
								            <option value="-1">Select</option>
							          	</select>
							        </div>
				        		</div>
				        		<div class="col-sm-12 col-md-4 col-lg-4">
				        			<label>Zip Code *</label>
				        			<div class="input-group mb-3">
										<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
										</div>
				        				<input type="text" class="form-control solo_numeros obligado" name="cp" id="cp" maxlength="5">
				        			</div>
				        		</div>
					        </div>
					        
					    </div>
					</div>

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Family Environment</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">TYPE THE COMPLETE NAMES OF YOUR PARENTS AND SIBLINGS INCLUDING THOSE WHO DO NOT LIVE IN YOUR SAME ADDRESS</h6>
							<div class="card my-5">
								<p class="card-header text-center"><b>Wife/Husband</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Name </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-user"></i></span>
												</div>
						        				<input type="text" class="form-control casado" name="nombre_conyuge" id="nombre_conyuge">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
						        			<label>Age </label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
												<span class="input-group-text"><i class="far fa-id-card"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros casado" name="edad_conyuge" id="edad_conyuge" maxlength="2">
					        				</div>
						        		</div>
						        		<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Ocupation </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
												</div>
						        				<input type="text" class="form-control casado" name="puesto_conyuge" id="puesto_conyuge">
					        				</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>City</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-city"></i></span>
												</div>
						        				<input type="text" class="form-control casado" name="ciudad_conyuge" id="ciudad_conyuge">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Company</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-building"></i></span>
												</div>
						        				<input type="text" class="form-control casado" name="empresa_conyuge" id="empresa_conyuge">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Live with her/him?</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-house-user"></i></span>
												</div>
							        			<select name="con_conyuge" id="con_conyuge" class="form-control casado">
										            <option value="">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
									        </div>
							        	</div>
									</div>
								</div>
							</div>
							</form>
								<div class="card my-5">
									<p class="card-header text-center"><b>Children</b></p>
									<div class="card-body">
										<div class="row">
								        	<div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4">
								        		<label>How many children do you have? <br>(If not type zero) *</label>
								        		<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-baby"></i></span>
													</div>
							        				<input type="text" class="form-control solo_numeros obligado" name="num_hijos" id="num_hijos" maxlength="2" oninput="generarHijos()">
						        				</div>
								        	</div>
								        </div>
								        <div id="children">
							
										</div>
									</div>
								</div>
							<form id="datos2">
								<div class="card my-5">
									<p class="card-header text-center"><b>Father</b></p>
									<div class="card-body">
										<div class="row">
											<div class="col-sm-12 col-md-4 col-lg-4">
												<label>Name *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-user"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="nombre_padre" id="nombre_padre">
							        			</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-4">
							        			<label>Age *</label>
							        			<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="far fa-id-card"></i></span>
													</div>
							        				<input type="text" class="form-control solo_numeros obligado" name="edad_padre" id="edad_padre" maxlength="2">
							        			</div>
							        		</div>
							        		<div class="col-sm-12 col-md-4 col-lg-4">
												<label>Ocupation *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="puesto_padre" id="puesto_padre">
							        			</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-4 col-lg-4">
												<label>City *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-city"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="ciudad_padre" id="ciudad_padre">
							        			</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-4">
												<label>Company *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-building"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="empresa_padre" id="empresa_padre">
							        			</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-4">
								        		<label>Live with her/him? *</label>
								        		<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-house-user"></i></span>
													</div>
								        			<select name="con_padre" id="con_padre" class="form-control obligado">
											            <option value="-1">Select</option>
											            <option value="0">No</option>
											            <option value="1">Yes</option>
										          	</select>
									          	</div>
								        	</div>
										</div>	
									</div>
								</div>
								<div class="card my-5">
									<p class="card-header text-center"><b>Mother</b></p>
									<div class="card-body">
										<div class="row">
											<div class="col-sm-12 col-md-4 col-lg-4">
												<label>Name *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-user"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="nombre_madre" id="nombre_madre">
							        			</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-4">
							        			<label>Age *</label>
							        			<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="far fa-id-card"></i></span>
													</div>
							        				<input type="text" class="form-control solo_numeros obligado" name="edad_madre" id="edad_madre" maxlength="2">
							        			</div>
							        		</div>
							        		<div class="col-sm-12 col-md-4 col-lg-4">
												<label>Ocupation *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="puesto_madre" id="puesto_madre">
							        			</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-4 col-lg-4">
												<label>City *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-city"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="ciudad_madre" id="ciudad_madre">
							        			</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-4">
												<label>Company *</label>
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-building"></i></span>
													</div>
							        				<input type="text" class="form-control obligado" name="empresa_madre" id="empresa_madre">
							        			</div>
											</div>
											<div class="col-sm-12 col-md-4 col-lg-4">
								        		<label>Live with her/him? *</label>
								        		<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-house-user"></i></span>
													</div>
								        			<select name="con_madre" id="con_madre" class="form-control obligado">
											            <option value="-1">Select</option>
											            <option value="0">No</option>
											            <option value="1">Yes</option>
										          	</select>
									          	</div>
								        	</div>
										</div>
									</div>
								</div>
							</form>
							<div class="card">
								<p class="card-header text-center"><b>Sister/Brother</b></p>
								<div class="card-body">
									<div class="row">
						        	<div class="col-sm-12 col-md-4 col-lg-4 offset-md-4 offset-lg-4">
						        		<label>How many siblings do you have? (If not type zero)*</label>
						        		<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-users"></i></span>
											</div>
					        			<input type="text" class="form-control solo_numeros obligado" name="num_hermanos" id="num_hermanos" maxlength="2" oninput="generarHermanos()">
					        			</div>
						        	</div>
						        </div>
								<div id="siblings">
									
								</div>
								</div>
							</div>	
						</div>
					</div>


				<form id="datos3">

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Studies Record</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">TYPE BY PERIOD, FOR EXAMPLE: 2000-2003</h6>
							<div class="card my-5">
								<p class="card-header text-center"><b>Elementary School</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Period *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prim_periodo" id="prim_periodo">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Institute *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-university"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prim_escuela" id="prim_escuela">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>City *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-city"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prim_ciudad" id="prim_ciudad">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Certificate Obtained *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prim_certificado" id="prim_certificado">
					        				</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card my-5">
								<p class="card-header text-center"><b>Middle School</b></p>
								<div class=" card-body">
									<div class="row">
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Period *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="sec_periodo" id="sec_periodo">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Institute *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-university"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="sec_escuela" id="sec_escuela">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>City *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-city"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="sec_ciudad" id="sec_ciudad">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Certificate Obtained *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="sec_certificado" id="sec_certificado">
						        			</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card my-5">
								<p class="card-header text-center"><b>High School</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Period *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prep_periodo" id="prep_periodo">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Institute *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-university"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prep_escuela" id="prep_escuela">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>City *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-city"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prep_ciudad" id="prep_ciudad">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Certificate Obtained *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="prep_certificado" id="prep_certificado">
						        			</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card my-5">
								<p class=" card-header text-center"><b>Collage</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Period *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="lic_periodo" id="lic_periodo">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Institute *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-university"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="lic_escuela" id="lic_escuela">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>City *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-city"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="lic_ciudad" id="lic_ciudad">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Certificate Obtained *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="lic_certificado" id="lic_certificado">
						        			</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-12 col-md-12 col-lg-12">
											<label>Seminaries/Courses Certificates *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
												</div>
						        			<textarea class="form-control obligado" name="otro_certificado" id="otro_certificado" rows="3"></textarea>
						        			</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Break(s) in Studies</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">IF YOU HAD A BREAK DURING YOUR STUDIES, PLEASE EXPLAIN THE REASON, INDICATE PERIOD AND WHICH ACTIVITIES DID YOU DO MEANWHILE. IF NOT TYPE NA</h6>
								<div class="row">
								<div class="col-sm-12 col-md-12 col-lg-12">
									<label>Period, Reason and Activities *</label>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-hourglass-half"></i></span>
										</div>
				        			<textarea class="form-control obligado" name="carrera_inactivo" id="carrera_inactivo" rows="3"></textarea>
				        			</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Labor References</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">TYPE YOUR LAST TWO JOBS STARTING WITH THE LAST ONE. PROVIDE THE TELEPHONE NUMBER OF THE COMPANY AND THE EMAIL OF YOUR IMMEDIATE BOSS</h6>
							<div class="card my-5">
								<p class="card-header text-center"><b>First reference</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Company </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-building"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_empresa" id="refLab1_empresa">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Address </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_direccion" id="refLab1_direccion">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-2 col-lg-2">
						        			<label>Entry Date </label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_entrada" id="refLab1_entrada">
					        				</div>
						        		</div>
						        		<div class="col-sm-12 col-md-2 col-lg-2">
						        			<label>Exit Date </label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_salida" id="refLab1_salida" >
					        				</div>
						        		</div>
									</div>
									<br>
									<div class="row">
						        		<div class="col-sm-12 col-md-4 col-lg-4">
						        			<label>Phone </label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros reflab1" name="refLab1_telefono" id="refLab1_telefono" maxlength="10">
					        				</div>
						        		</div>
						        		<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Initial Job Position </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_puesto1" id="refLab1_puesto1">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Last Job Position </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-people-arrows"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_puesto2" id="refLab1_puesto2">
					        				</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-12 col-md-3 col-lg-3">
							        		<label>Initial Salary </label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-money-bill-alt"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros reflab1" name="refLab1_salario1" id="refLab1_salario1" maxlength="8">
					        				</div>
							        	</div>
							        	<div class="col-sm-12 col-md-3 col-lg-3">
							        		<label>Last Salary </label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-hand-holding-usd"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros reflab1" name="refLab1_salario2" id="refLab1_salario2" maxlength="8">
					        				</div>
							        	</div>
							        	<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Immediate Boss Name </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_bossnombre" id="refLab1_bossnombre">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Immediate Boss Email </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-envelope"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_bosscorreo" id="refLab1_bosscorreo">
					        				</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-12 col-md-6 col-lg-6">
											<label>Boss's Job Position </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fab fa-black-tie"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_bosspuesto" id="refLab1_bosspuesto">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-6 col-lg-6">
											<label>Cause of Separation </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-handshake"></i></span>
												</div>
						        				<input type="text" class="form-control reflab1" name="refLab1_separacion" id="refLab1_separacion">
						        			</div>
										</div>
									</div>
								</div>
							</div>
							<div class=" card my-5">
								<p class="card-header text-center"><b>Second Reference</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Company </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-building"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_empresa" id="refLab2_empresa">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Address </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_direccion" id="refLab2_direccion">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-2 col-lg-2">
						        			<label>Entry Date </label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_entrada" id="refLab2_entrada">
					        				</div>
						        		</div>
						        		<div class="col-sm-12 col-md-2 col-lg-2">
						        			<label>Exit Date </label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_salida" id="refLab2_salida">
						        			</div>
						        		</div>
									</div>
									<br>
									<div class="row">
						        		<div class="col-sm-12 col-md-4 col-lg-4">
						        			<label>Phone </label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros reflab2" name="refLab2_telefono" id="refLab2_telefono" maxlength="10">
					        				</div>
						        		</div>
						        		<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Initial Job Position </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_puesto1" id="refLab2_puesto1">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Last Job Position </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-people-arrows"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_puesto2" id="refLab2_puesto2">
						        			</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-12 col-md-3 col-lg-3">
							        		<label>Initial Salary </label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-money-bill-alt"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros reflab2" name="refLab2_salario1" id="refLab2_salario1" maxlength="8">
						        			</div>
							        	</div>
							        	<div class="col-sm-12 col-md-3 col-lg-3">
							        		<label>Last Salary </label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-hand-holding-usd"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros reflab2" name="refLab2_salario2" id="refLab2_salario2" maxlength="8">
					        				</div>
							        	</div>
							        	<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Immediate Boss Name </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-user-tie"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_bossnombre" id="refLab2_bossnombre">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-3 col-lg-3">
											<label>Immediate Boss Email </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-envelope"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_bosscorreo" id="refLab2_bosscorreo">
						        			</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-12 col-md-6 col-lg-6">
											<label>Boss's Job Position </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fab fa-black-tie"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_bosspuesto" id="refLab2_bosspuesto">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-6 col-lg-6">
											<label>Cause of Separation </label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-handshake"></i></span>
												</div>
						        				<input type="text" class="form-control reflab2" name="refLab2_separacion" id="refLab2_separacion">
						        			</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Break(s) in Employment</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">IF YOU HAD A BREAK DURING EMPLOYMENTS, PLEASE EXPLAIN THE REASON, INDICATE PERIOD AND WHICH ACTIVITIES DID YOU DO MEANWHILE. IF NOT TYPE NA</h6>
							<div class="row">
								<div class="col-sm-12 col-md-12 col-lg-12">
									<label>Period, Reason and Activities *</label>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-hourglass-half"></i></span>
										</div>
				        			<textarea class="form-control obligado" name="trabajo_inactivo" id="trabajo_inactivo" rows="3"></textarea>
				        			</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Personal references</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">THESE REFERENCES MUST BE YOUR FRIENDS ONLY, NOT FAMILY</h6>
							<div class="card my-5">
								<p class="card-header text-center"><b>First reference</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Name *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-user"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="refPer1_nombre" id="refPer1_nombre">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
						        			<label>Phone *</label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros obligado" name="refPer1_telefono" id="refPer1_telefono" maxlength="10">
						        			</div>
						        		</div>
						        		<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Time to know her/him (years) *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-clock"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros obligado" name="refPer1_tiempo" id="refPer1_tiempo" maxlength="2">
					        				</div>
							        	</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>How did you meet her/him? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-comments"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="refPer1_conocido" id="refPer1_conocido">
					        				</div>
							        	</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Does she/he know where do you work/worked? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-question"></i></span>
												</div>
							        			<select name="refPer1_sabetrabajo" id="refPer1_sabetrabajo" class="form-control obligado">
										            <option value="-1">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
									        </div>
							        	</div>
							        	<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Does she/he know where do you live? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-question"></i></span>
												</div>
							        			<select name="refPer1_sabevive" id="refPer1_sabevive" class="form-control obligado">
										            <option value="-1">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
									        </div>
							        	</div>
									</div>
								</div>
							</div>
							<div class="card my-5">
								<p class="card-header text-center"><b>Second reference</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Name *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-user"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="refPer2_nombre" id="refPer2_nombre">
						        			</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
						        			<label>Phone *</label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros obligado" name="refPer2_telefono" id="refPer2_telefono" maxlength="10">
						        			</div>
						        		</div>
						        		<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Time to know her/him (years) *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-clock"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros obligado" name="refPer2_tiempo" id="refPer2_tiempo" maxlength="2">
						        			</div>
							        	</div>
									</div>
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>How did you meet her/him? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-comments"></i></span>
												</div>
						        			<input type="text" class="form-control obligado" name="refPer2_conocido" id="refPer2_conocido">
						        			</div>
							        	</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Does she/he know where do you work/worked? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-question"></i></span>
												</div>
							        			<select name="refPer2_sabetrabajo" id="refPer2_sabetrabajo" class="form-control obligado">
										            <option value="-1">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
								          	</div>
							        	</div>
							        	<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Does she/he know where do you live? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-question"></i></span>
												</div>
							        			<select name="refPer2_sabevive" id="refPer2_sabevive" class="form-control obligado">
										            <option value="-1">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
								          	</div>
							        	</div>
									</div>
								</div>
							</div>
							<div class="card">
								<p class="card-header text-center"><b>Third reference</b></p>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label>Name *</label>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-user"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="refPer3_nombre" id="refPer3_nombre">
					        				</div>
										</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
						        			<label>Phone *</label>
						        			<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros obligado" name="refPer3_telefono" id="refPer3_telefono" maxlength="10">
						        			</div>
						        		</div>
						        		<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Time to know her/him (years) *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-clock"></i></span>
												</div>
						        				<input type="text" class="form-control solo_numeros obligado" name="refPer3_tiempo" id="refPer3_tiempo" maxlength="2">
						        			</div>
							        	</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>How did you meet her/him? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="far fa-comments"></i></span>
												</div>
						        				<input type="text" class="form-control obligado" name="refPer3_conocido" id="refPer3_conocido">
						        			</div>
							        	</div>
										<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Does she/he know where do you work/worked? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-question"></i></span>
												</div>
							        			<select name="refPer3_sabetrabajo" id="refPer3_sabetrabajo" class="form-control obligado">
										            <option value="-1">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
									        </div>
							        	</div>
							        	<div class="col-sm-12 col-md-4 col-lg-4">
							        		<label>Does she/he know where do you live? *</label>
							        		<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fas fa-question"></i></span>
												</div>
							        			<select name="refPer3_sabevive" id="refPer3_sabevive" class="form-control obligado">
										            <option value="-1">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
									        </div>
							        	</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Have you worked in any government entity, Politic Party or NGO? *</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">IF YOUR ANSWER IS AFIRMATIVE, INDICATE DEPENDENCY NAME, PERIOD, SALARY AND SEPARATION MOTIVE</h6>
							<div class="row">
								<div class="col-sm-12 col-lg-12">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-university"></i></span>
										</div>
				        				<input type="text" class="form-control obligado" name="trabajo_gobierno" id="trabajo_gobierno">
				        			</div>
								</div>
							</div>
						</div>
					</div>

					<div class="card my-5">
						<h5 class="card-header text-center seccion">Your Comments</h5>
						<div class="card-body">
							<h6 class="alert alert-warning card-subtitle mb-2 text-muted text-center">IF YOU HAVE ANY COMMENT ABOUT YOUR DOCUMENTS OR IF YOU WANT TO ADD ANY INFORMATION WHICH COULD HELP US DURING THE PROCESS, PLEASE LET US KNOW</h6>
							<div class="row">
								<div class="col-sm-12 col-lg-12">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-comments"></i></span>
										</div>
										<textarea name="obs" id="obs" class="form-control obligado" rows="5"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row alert alert-primary">
						<div class="col-sm-12 col-md-12">
							<label>Please read this <a href="javascript:void(0)" data-toggle="modal" data-target="#nondisclosureModal">Non-disclosure agreement</a> / <a href="javascript:void(0)" data-toggle="modal" data-target="#avisoModal">Aviso de privacidad</a></label>
                        </div>
                        <div class="col-sm-12 col-md-12">
                        	<label class="contenedor_check">I declare that I have read and fully agree with everything in this Non-disclosure agreement
                            <input type="checkbox" name="acepto" id="acepto" value="1">
                            </label>
                        </div>
					</div>
					<div class="row my-5">
						<div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">
							<!--a id="sendForm" href="javascript:void(0)">Send form and finish</a-->
							<button id="sendForm" class="btn btn-success form-control">Send form and continue with the upload of documents</button>
						</div>
					</div>
				</form>
			</div>
	<?php 
	} ?>
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
	<!-- Sweetalert 2 -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.12.7/dist/sweetalert2.js"></script>
	<!-- Bootstrap Select -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
	<!-- InputMask -->
	<script src="<?php echo base_url(); ?>js/input-mask/jquery.inputmask.js"></script>
	<script src="<?php echo base_url(); ?>js/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="<?php echo base_url(); ?>js/input-mask/jquery.inputmask.extensions.js"></script>
	<script>
    $(document).ready(function(){
    	var check_aviso = <?php echo $tiene_aviso; ?>;
    	if(check_aviso == 0){
    		$('#checkAvisoModal').modal('show');
    	}
  		$('#fecha_nacimiento').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
  		$('#refLab1_entrada').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
  		$('#refLab1_salida').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
  		$('#refLab2_entrada').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
  		$('#refLab2_salida').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });

    	var fnacimiento = '<?php echo $this->session->userdata('fecha') ?>';
    	if(fnacimiento != "" && fnacimiento != null && fnacimiento != "0000-00-00" && fnacimiento != "00/00/0000"){
    		$("#fecha_nacimiento").val(fnacimiento).trigger('change');
    	}
    	//Limpiar inputs
		$(".obligado").focus(function(){
			$(this).removeClass("requerido");
		});
    	//Obtiene los municipios dependiendo del id estado
	  	$("#estado").change(function(){
	      	var id_estado = $(this).val();
	      	if(id_estado != -1){
	        	$.ajax({
	          		url: '<?php echo base_url('Funciones/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#municipio').prop('disabled', false);
	            		$('#municipio').html(res);
	          		},error:function(res)
	          		{
	            		//$('#errorModal').modal('show');
	          		}
	        	});
	      	}
	      	else{
        		$('#municipio').prop('disabled', true);
	        	$('#municipio').append($("<option selected></option>").attr("value",-1).text("Selecciona"));
	      	}
	    });
		//Crear registro
		$('#datos3').on('submit', function(e){  
           	e.preventDefault();
           	var totalVacios = $('.obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;

		    if(totalVacios > 0){
		      	$(".obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "" || element.val() == -1) {
			          	element.addClass("requerido");
			          	$('#empty').css("display", "block");
			          	setTimeout(function(){
			            	$('#empty').fadeOut();
			          	},5000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	if($('#acepto').is(':checked')){
		           	var data = $("#datos").serialize();
		           	var data2 = $("#datos2").serialize();
		           	var data3 = $(this).serialize();
		           	var num_hijos = $("#num_hijos").val();
					var hijos_data = "";
					if(num_hijos > 0){
		            	for(var i = 1; i <= num_hijos; i++){
							hijos_data += $("#nombre_hijo"+i).val()+",";
							hijos_data += $("#edad_hijo"+i).val()+",";
							hijos_data += $("#puesto_hijo"+i).val()+",";
							hijos_data += $("#ciudad_hijo"+i).val()+",";
							hijos_data += $("#empresa_hijo"+i).val()+",";
							hijos_data += $("#con_hijo"+i).val()+"@@";
						}
		            }
		            else{
		            	hijos_data = "";
		            }
					var num_hermanos = $("#num_hermanos").val();
					var hermanos_data = "";
					if(num_hermanos > 0){
		            	for(var i = 1; i <= num_hermanos; i++){
							hermanos_data += $("#nombre_hermano"+i).val()+",";
							hermanos_data += $("#edad_hermano"+i).val()+",";
							hermanos_data += $("#puesto_hermano"+i).val()+",";
							hermanos_data += $("#ciudad_hermano"+i).val()+",";
							hermanos_data += $("#empresa_hermano"+i).val()+",";
							hermanos_data += $("#con_hermano"+i).val()+"@@";
						}
		            }
		            else{
		            	hermanos_data = "";
		            }
					/*var rp1_sabetrabajo = $("input:checkbox[id='refPer1_sabetrabajo']:checked").val();
					var rp1_sabevive = $("input:checkbox[id='refPer1_sabevive']:checked").val();
					var rp2_sabetrabajo = $("input:checkbox[id='refPer2_sabetrabajo']:checked").val();
					var rp2_sabevive = $("input:checkbox[id='refPer2_sabevive']:checked").val();         
					var rp3_sabetrabajo = $("input:checkbox[id='refPer3_sabetrabajo']:checked").val();
					var rp3_sabevive = $("input:checkbox[id='refPer3_sabevive']:checked").val();*/	           
		            $.ajax({  
		                url:"<?php echo base_url('Candidato/storeCandidato'); ?>",   
		                method: "POST",  
		                data: {'datos':data,'hijos':hijos_data,'padres':data2,'hermanos':hermanos_data,'complementos':data3},
		                dataType: "text",
		                beforeSend: function(){
		                	$('.loader').css("display","block");
		              	},
		                success:function(res){
		                	//$("#exito").css('display','block');
		                	$("#mensajeModal").modal("show");
	                		setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  		$("#mensajeModal").modal("hide");
		                  		//$("#exito").fadeOut();
		                    	//$("#closeSession")[0].click();
		                    	window.location.href = "<?php echo base_url(); ?>index.php/Login/logout";
		                	},15000);  
		                }  
		            }); 
		        }
		        else{
		          	$("#no_check").css("display","block");
		          	setTimeout(function(){
		            	$("#no_check").fadeOut();
		          	}, 4000);
		        }   
		    }
		});
        $("#civil").change(function(){
	    	var civil = $(this).val();
	    	if(civil == 1){
	    		$(".casado").addClass('obligado');
	    		$(".casado").addClass('requerido');
	    	}
	    	else{
	    		$(".casado").removeClass('obligado');
	    		$(".casado").removeClass('requerido');
	    	}
	    });
	    $(".casado").change(function(){
	    	var vaciosConyuge = $('.casado').filter(function(){
	      		return !$(this).val();
	    	}).length;

		    if(vaciosConyuge > 0 && vaciosConyuge < 6){
		      	$(".casado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("obligado");
			        }
		      	});
		    }
		    else{
		    	$(".casado").removeClass("obligado");
		    	$(".casado").removeClass("requerido");
		    }
	    });
	    $(".reflab1").change(function(){
	    	var vaciosReflab1 = $('.reflab1').filter(function(){
	      		return !$(this).val();
	    	}).length;

		    if(vaciosReflab1 > 0 && vaciosReflab1 < 13){
		      	$(".reflab1").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("obligado");
			        }
		      	});
		    }
		    else{
		    	$(".reflab1").removeClass("obligado");
		    	$(".reflab1").removeClass("requerido");
		    }
	    });
	    $(".reflab2").change(function(){
	    	var vaciosReflab2 = $('.reflab2').filter(function(){
	      		return !$(this).val();
	    	}).length;

		    if(vaciosReflab2 > 0 && vaciosReflab2 < 13){
		      	$(".reflab2").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("obligado");
			        }
		      	});
		    }
		    else{
		    	$(".reflab2").removeClass("obligado");
		    	$(".reflab2").removeClass("requerido");
		    }
	    });
	    //Obtiene la fecha de nacimiento
		$("#fecha_nacimiento").change(function(){
			var fecha = $(this).val();
			var today = new Date();
		    var birthDate = new Date(fecha);
		    var age = today.getFullYear() - birthDate.getFullYear();
		    var m = today.getMonth() - birthDate.getMonth();
		    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		        age = age - 1;
		    }
		    $("#edad").val(age);
		});
	    //Acepta solo numeros en los input
	  	$(".solo_numeros").on("input", function(){
		    var valor = $(this).val();
		    $(this).val(valor.replace(/[^0-9]/g, ''));
		}); 	
    });
    //Genera los campos dependiendo del número que se escriba
	function generarHijos(){
		var num = $("#num_hijos").val();
		var bloque = "";
		if(num >= 1){
			for(i = 1; i <= num; i++){
				bloque += "<div class='card'><p class='card-header text-center'><b>Child "+i+"</b></p><div class='card-body'><div class='row'><div class='col-sm-12 col-md-4 col-lg-4'><label>Name *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-user'></i></span></div><input type='text' class='form-control obligado' name='nombre_hijo"+i+"' id='nombre_hijo"+i+"'></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>Age *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-birthday-cake'></i></span></div><input type='text' class='form-control solo_numeros obligado' name='edad_hijo"+i+"' id='edad_hijo"+i+"'  maxlength='2' ></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>Ocupation *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-user-tie'></i></span></div><input type='text' class='form-control obligado' name='puesto_hijo"+i+"' id='puesto_hijo"+i+"' ></div></div></div><div class='row'><div class='col-sm-12 col-md-4 col-lg-4'><label>City *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-city'></i></span></div><input type='text' class='form-control obligado' name='ciudad_hijo"+i+"' id='ciudad_hijo"+i+"' ></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>School/Company *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-school'></i></span></div><input type='text' class='form-control obligado' name='empresa_hijo"+i+"' id='empresa_hijo"+i+"' ></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>Live with her/him? *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-house-user'></i></span></div><select name='con_hijo"+i+"' id='con_hijo"+i+"' class='form-control obligado'><option value='-1'>Select</option><option value='0'>No</option><option value='1'>Yes</option></select></div></div></div></div></div>";
			}
			bloque += "<script>";
			bloque += '$(".solo_numeros").on("input",function(){var valor = $(this).val();$(this).val(valor.replace(/[^0-9]/g,""));});';
			bloque += "<\/script>";
			$("#children").html(bloque);
		}
		else{
			$("#children").empty();
		}
	}
	//Genera los campos dependiendo del número que se escriba
	function generarHermanos(){
		var num = $("#num_hermanos").val();
		var bloque = "";
		if(num >= 1){
			for(i = 1; i <= num; i++){
				bloque += "<div class='card my-5'><p class='card-header text-center'><b>Sibling "+i+"</b></p><div class='card-body'><div class='row'><div class='col-sm-12 col-md-4 col-lg-4'><label>Name *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-user'></i></span></div><input type='text' class='form-control obligado' name='nombre_hermano"+i+"' id='nombre_hermano"+i+"'></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>Age *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-id-card'></i></span></div><input type='text' class='form-control solo_numeros obligado' name='edad_hermano"+i+"' id='edad_hermano"+i+"' maxlength='2'></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>Ocupation *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-user-tie'></i></span></div><input type='text' class='form-control obligado' name='puesto_hermano"+i+"' id='puesto_hermano"+i+"'></div></div></div><div class='row'><div class='col-sm-12 col-md-4 col-lg-4'><label>City *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-city'></i></span></div><input type='text' class='form-control obligado' name='ciudad_hermano"+i+"' id='ciudad_hermano"+i+"'></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>School/Company *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-building'></i></span></div><input type='text' class='form-control obligado' name='empresa_hermano"+i+"' id='empresa_hermano"+i+"'></div></div><div class='col-sm-12 col-md-4 col-lg-4'><label>Live with her/him? *</label><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'><i class='fas fa-house-user'></i></span></div><select name='con_hermano"+i+"' id='con_hermano"+i+"' class='form-control obligado'><option value='-1'>Select</option><option value='0'>No</option><option value='1'>Yes</option></select></div></div></div></div></div>";
			}
			bloque += "<script>";
			bloque += '$(".solo_numeros").on("input",function(){var valor = $(this).val();$(this).val(valor.replace(/[^0-9]/g,""));});';
			bloque += "<\/script>";
			$("#siblings").html(bloque);
		}
		else{
			$("#siblings").empty();
		}
	}
	var id_candidato = "<?php echo $this->session->userdata('id'); ?>";
	var nombre = "<?php echo $this->session->userdata('nombre'); ?>";
    var paterno = "<?php echo $this->session->userdata('paterno'); ?>";
    var prefijo = id_candidato+"_"+nombre+"_"+paterno;
	function subirAviso(){
    	if($("#doc_aviso").val() == ""){
    		$("#msj_error").text("Select your non-disclosure agreement");
          	$("#msj_error").css("display","block");
          	setTimeout(function(){
          		$("#msj_error").fadeOut();
          	},4000)
	    }
	    else{
		   	var docs = new FormData();
	    	var num_files = document.getElementById('doc_aviso').files.length;
            for(var x = 0; x < num_files; x++) {
                docs.append("avisos[]", document.getElementById('doc_aviso').files[x]);
            }
            docs.append("id_candidato", id_candidato);
            docs.append("prefijo", prefijo);
            $.ajax({  
                url:"<?php echo base_url('Candidato/subirNonDisclosure'); ?>",   
                method: "POST",  
                data: docs,  
                contentType: false,  
                cache: false,  
                processData:false,  
                beforeSend: function(){
                	$('.loader').css("display","block");
              	},
                success:function(res){
                	location.reload();
                }  
            });    
	    }
    }
	</script>
	

</body>
</html>