<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Cliente: <small><?php echo $cliente; ?></small></h1><br>
		<a href="#" class="btn btn-primary btn-icon-split" id="btn_nuevo" data-toggle="modal" data-target="#newModal">
			<span class="icon text-white-50">
				<i class="fas fa-plus-circle"></i>
			</span>
			<span class="text">Registrar candidato</span>
		</a>
		<a href="#" class="btn btn-primary btn-icon-split" id="btn_nuevo" data-toggle="modal" data-target="#asignarCandidatoModal">
			<span class="icon text-white-50">
				<i class="fas fa-plus-circle"></i>
			</span>
			<span class="text">Asignacion de candidato</span>
		</a>
		<a href="#" class="btn btn-primary btn-icon-split hidden" id="btn_regresar" onclick="regresarListado()" style="display: none;">
			<span class="icon text-white-50">
				<i class="fas fa-arrow-left"></i>
			</span>
			<span class="text">Regresar al listado</span>
		</a>
	</div>

	<?php echo $modals; ?>
	<div class="loader" style="display: none;"></div>
	<input type="hidden" id="idCandidato">
	<input type="hidden" id="idCliente">
	<input type="hidden" id="idDoping">


	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary"></h6>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table id="tabla" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				</table>
			</div>
		</div>
	</div>


	<section class="content" id="formulario" style="display: none;">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<form id="data_reflaboral1">
							<div class="box-header tituloSubseccion">
								<p class="box-title"><strong> Antecedentes Laborales </strong></p>
							</div>
							<div class="box-header text-center tituloSubseccion">
								<p class="box-title " id="titulo_reflab1"><strong> 1. Último trabajo </strong>
									<hr>
								</p>
								<input type="hidden" id="idreflab1">
							</div>
							<p class="tituloSubseccion text-center"></p>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre de la empresa * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_empresa" id="reflab1_empresa">
									<br>
								</div>
								<div class="col-md-3">
									<label>Área o Departamento * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_area" id="reflab1_area">
									<br>
								</div>
								<div class="col-md-3">
									<label>Domicilio, calle y número * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_domicilio" id="reflab1_domicilio">
									<br>
								</div>
								<div class="col-md-3">
									<label>Colonia * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_colonia" id="reflab1_colonia">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2">
									<label>Código postal * </label>
									<input type="text" class="form-control solo_numeros reflab1_obligado" name="reflab1_cp" id="reflab1_cp" maxlength="5">
									<br>
								</div>
								<div class="col-md-2">
									<label>Teléfono * </label>
									<input type="text" class="form-control solo_numeros reflab1_obligado" name="reflab1_telefono" id="reflab1_telefono" maxlength="12">
									<br>
								</div>
								<div class="col-md-2">
									<label>Tipo de empresa * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_tipo" id="reflab1_tipo">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto desempeñado * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_puesto" id="reflab1_puesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Periodo trabajado, mes y año * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_periodo" id="reflab1_periodo">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre del último jefe * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_jefenombre" id="reflab1_jefenombre">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto del último jefe * </label>
									<input type="text" class="form-control reflab1_obligado" name="reflab1_jefepuesto" id="reflab1_jefepuesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual inicial * </label>
									<input type="text" class="form-control solo_numeros reflab1_obligado" name="reflab1_sueldo1" id="reflab1_sueldo1" maxlength="10">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual final * </label>
									<input type="text" class="form-control solo_numeros reflab1_obligado" name="reflab1_sueldo2" id="reflab1_sueldo2" maxlength="10">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>¿En qué consistía su trabajo? *</label>
									<textarea class="form-control reflab1_obligado" cols="2" name="reflab1_actividades" id="reflab1_actividades"></textarea>
									<br>
								</div>
								<div class="col-md-6">
									<label>Causa de separación *</label>
									<textarea class="form-control reflab1_obligado" cols="2" name="reflab1_razon" id="reflab1_razon"></textarea>
									<br>
								</div>
							</div>
							<br>
							<p class="tituloSubseccion text-center">Características analizadas</p><br>
							<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<label for="aplicar_todo">Aplicar a todo</label>
									<select id="aplicar_todo" class="form-control">
										<option value="-1">Selecciona</option>
										<option value="0">No proporciona</option>
										<option value="1">Excelente</option>
										<option value="2">Bueno</option>
										<option value="3">Regular</option>
										<option value="4">Insuficiente</option>
										<option value="5">Muy mal</option>
									</select>
									<br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab1_calidad">Calidad en el trabajo *</label>
									<select name="reflab1_calidad" id="reflab1_calidad" class="form-control performance reflab1_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab1_puntualidad">Puntualidad y asistencia *</label>
									<select name="reflab1_puntualidad" id="reflab1_puntualidad" class="form-control performance reflab1_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab1_honesto">Honradez e integridad *</label>
									<select name="reflab1_honesto" id="reflab1_honesto" class="form-control performance reflab1_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab1_responsabilidad">Responsabilidad *</label>
									<select name="reflab1_responsabilidad" id="reflab1_responsabilidad" class="form-control performance reflab1_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab1_adaptacion">Adaptación al trabajo *</label>
									<select name="reflab1_adaptacion" id="reflab1_adaptacion" class="form-control performance reflab1_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab1_actitud_jefes">Actitud hacia sus jefes *</label>
									<select name="reflab1_actitud_jefes" id="reflab1_actitud_jefes" class="form-control performance reflab1_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab1_actitud_comp">Actitud hacia sus compañeros *</label>
									<select name="reflab1_actitud_comp" id="reflab1_actitud_comp" class="form-control performance reflab1_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Comentarios y observaciones *</label>
									<textarea class="form-control reflab1_obligado" name="reflab1_comentarios" id="reflab1_comentarios" rows="2"></textarea><br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-md-offset-3">
									<button type="button" class="btn btn-primary" onclick="actualizarLaboral(1)">Guardar los cambios del Último Trabajo</button>
									<br><br>
								</div>
								<div class="col-md-3">
									<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
									<br><br><br><br>
								</div>
							</div>
						</form>
						<form id="data_reflaboral2">
							<div class="box-header text-center tituloSubseccion">
								<p class="box-title " id="titulo_reflab1"><strong> 2.Segundo trabajo </strong>
									<hr>
								</p>
								<input type="hidden" id="idreflab2">
							</div>
							<p class="tituloSubseccion text-center"></p>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre de la empresa * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_empresa" id="reflab2_empresa">
									<br>
								</div>
								<div class="col-md-3">
									<label>Área o Departamento * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_area" id="reflab2_area">
									<br>
								</div>
								<div class="col-md-3">
									<label>Domicilio, calle y número * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_domicilio" id="reflab2_domicilio">
									<br>
								</div>
								<div class="col-md-3">
									<label>Colonia * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_colonia" id="reflab2_colonia">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2">
									<label>Código postal * </label>
									<input type="text" class="form-control solo_numeros reflab2_obligado" name="reflab2_cp" id="reflab2_cp" maxlength="5">
									<br>
								</div>
								<div class="col-md-2">
									<label>Teléfono * </label>
									<input type="text" class="form-control solo_numeros reflab2_obligado" name="reflab2_telefono" id="reflab2_telefono" maxlength="12">
									<br>
								</div>
								<div class="col-md-2">
									<label>Tipo de empresa * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_tipo" id="reflab2_tipo">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto desempeñado * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_puesto" id="reflab2_puesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Periodo trabajado, mes y año * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_periodo" id="reflab2_periodo">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre del último jefe * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_jefenombre" id="reflab2_jefenombre">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto del último jefe * </label>
									<input type="text" class="form-control reflab2_obligado" name="reflab2_jefepuesto" id="reflab2_jefepuesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual inicial * </label>
									<input type="text" class="form-control solo_numeros reflab2_obligado" name="reflab2_sueldo1" id="reflab2_sueldo1" maxlength="10">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual final * </label>
									<input type="text" class="form-control solo_numeros reflab2_obligado" name="reflab2_sueldo2" id="reflab2_sueldo2" maxlength="10">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>¿En qué consistía su trabajo? *</label>
									<textarea class="form-control reflab2_obligado" cols="2" name="reflab2_actividades" id="reflab2_actividades"></textarea>
									<br>
								</div>
								<div class="col-md-6">
									<label>Causa de separación *</label>
									<textarea class="form-control reflab2_obligado" cols="2" name="reflab2_razon" id="reflab2_razon"></textarea>
									<br>
								</div>
							</div>
							<br>
							<p class="tituloSubseccion text-center">Características analizadas</p><br>
							<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<label for="aplicar_todo2">Aplicar a todo</label>
									<select id="aplicar_todo2" class="form-control">
										<option value="-1">Selecciona</option>
										<option value="0">No proporciona</option>
										<option value="1">Excelente</option>
										<option value="2">Bueno</option>
										<option value="3">Regular</option>
										<option value="4">Insuficiente</option>
										<option value="5">Muy mal</option>
									</select>
									<br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab2_calidad">Calidad en el trabajo *</label>
									<select name="reflab2_calidad" id="reflab2_calidad" class="form-control performance2 reflab2_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab2_puntualidad">Puntualidad y asistencia *</label>
									<select name="reflab2_puntualidad" id="reflab2_puntualidad" class="form-control performance2 reflab2_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab2_honesto">Honradez e integridad *</label>
									<select name="reflab2_honesto" id="reflab2_honesto" class="form-control performance2 reflab2_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab2_responsabilidad">Responsabilidad *</label>
									<select name="reflab2_responsabilidad" id="reflab2_responsabilidad" class="form-control performance2 reflab2_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab2_adaptacion">Adaptación al trabajo *</label>
									<select name="reflab2_adaptacion" id="reflab2_adaptacion" class="form-control performance2 reflab2_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab2_actitud_jefes">Actitud hacia sus jefes *</label>
									<select name="reflab2_actitud_jefes" id="reflab2_actitud_jefes" class="form-control performance2 reflab2_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab2_actitud_comp">Actitud hacia sus compañeros *</label>
									<select name="reflab2_actitud_comp" id="reflab2_actitud_comp" class="form-control performance2 reflab2_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Comentarios y observaciones *</label>
									<textarea class="form-control reflab2_obligado" name="reflab2_comentarios" id="reflab2_comentarios" rows="2"></textarea><br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-md-offset-3">
									<button type="button" class="btn btn-primary" onclick="actualizarLaboral(2)">Guardar los cambios del Trabajo #2</button>
									<br><br><br><br>
								</div>
								<div class="col-md-3">
									<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
									<br><br><br><br>
								</div>
							</div>
						</form>
						<form id="data_reflaboral3">
							<div class="box-header text-center tituloSubseccion">
								<p class="box-title " id="titulo_reflab1"><strong> 3.Tercer trabajo </strong>
									<hr>
								</p>
								<input type="hidden" id="idreflab3">
							</div>
							<p class="tituloSubseccion text-center"></p>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre de la empresa * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_empresa" id="reflab3_empresa">
									<br>
								</div>
								<div class="col-md-3">
									<label>Área o Departamento * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_area" id="reflab3_area">
									<br>
								</div>
								<div class="col-md-3">
									<label>Domicilio, calle y número * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_domicilio" id="reflab3_domicilio">
									<br>
								</div>
								<div class="col-md-3">
									<label>Colonia * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_colonia" id="reflab3_colonia">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2">
									<label>Código postal * </label>
									<input type="text" class="form-control solo_numeros reflab3_obligado" name="reflab3_cp" id="reflab3_cp" maxlength="5">
									<br>
								</div>
								<div class="col-md-2">
									<label>Teléfono * </label>
									<input type="text" class="form-control solo_numeros reflab3_obligado" name="reflab3_telefono" id="reflab3_telefono" maxlength="12">
									<br>
								</div>
								<div class="col-md-2">
									<label>Tipo de empresa * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_tipo" id="reflab3_tipo">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto desempeñado * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_puesto" id="reflab3_puesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Periodo trabajado, mes y año * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_periodo" id="reflab3_periodo">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre del último jefe * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_jefenombre" id="reflab3_jefenombre">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto del último jefe * </label>
									<input type="text" class="form-control reflab3_obligado" name="reflab3_jefepuesto" id="reflab3_jefepuesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual inicial * </label>
									<input type="text" class="form-control solo_numeros reflab3_obligado" name="reflab3_sueldo1" id="reflab3_sueldo1" maxlength="10">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual final * </label>
									<input type="text" class="form-control solo_numeros reflab3_obligado" name="reflab3_sueldo2" id="reflab3_sueldo2" maxlength="10">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>¿En qué consistía su trabajo? *</label>
									<textarea class="form-control reflab3_obligado" cols="2" name="reflab3_actividades" id="reflab3_actividades"></textarea>
									<br>
								</div>
								<div class="col-md-6">
									<label>Causa de separación *</label>
									<textarea class="form-control reflab3_obligado" cols="2" name="reflab3_razon" id="reflab3_razon"></textarea>
									<br>
								</div>
							</div>
							<br>
							<p class="tituloSubseccion text-center">Características analizadas</p><br>
							<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<label for="aplicar_todo3">Aplicar a todo</label>
									<select id="aplicar_todo3" class="form-control">
										<option value="-1">Selecciona</option>
										<option value="0">No proporciona</option>
										<option value="1">Excelente</option>
										<option value="2">Bueno</option>
										<option value="3">Regular</option>
										<option value="4">Insuficiente</option>
										<option value="5">Muy mal</option>
									</select>
									<br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab3_calidad">Calidad en el trabajo *</label>
									<select name="reflab3_calidad" id="reflab3_calidad" class="form-control performance3 reflab3_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab3_puntualidad">Puntualidad y asistencia *</label>
									<select name="reflab3_puntualidad" id="reflab3_puntualidad" class="form-control performance3 reflab3_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab3_honesto">Honradez e integridad *</label>
									<select name="reflab3_honesto" id="reflab3_honesto" class="form-control performance3 reflab3_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab3_responsabilidad">Responsabilidad *</label>
									<select name="reflab3_responsabilidad" id="reflab3_responsabilidad" class="form-control performance3 reflab3_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab3_adaptacion">Adaptación al trabajo *</label>
									<select name="reflab3_adaptacion" id="reflab3_adaptacion" class="form-control performance3 reflab3_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab3_actitud_jefes">Actitud hacia sus jefes *</label>
									<select name="reflab3_actitud_jefes" id="reflab3_actitud_jefes" class="form-control performance3 reflab3_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab3_actitud_comp">Actitud hacia sus compañeros *</label>
									<select name="reflab3_actitud_comp" id="reflab3_actitud_comp" class="form-control performance3 reflab3_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Comentarios y observaciones *</label>
									<textarea class="form-control reflab3_obligado" name="reflab3_comentarios" id="reflab3_comentarios" rows="2"></textarea><br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-md-offset-3">
									<button type="button" class="btn btn-primary" onclick="actualizarLaboral(3)">Guardar los cambios del Trabajo #3</button>
									<br><br><br><br>
								</div>
								<div class="col-md-3">
									<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
									<br><br><br><br>
								</div>
							</div>
						</form>
						<form id="data_reflaboral4">
							<div class="box-header text-center tituloSubseccion">
								<p class="box-title " id="titulo_reflab1"><strong> 4.Cuarto trabajo </strong>
									<hr>
								</p>
								<input type="hidden" id="idreflab4">
							</div>
							<p class="tituloSubseccion text-center"></p>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre de la empresa * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_empresa" id="reflab4_empresa">
									<br>
								</div>
								<div class="col-md-3">
									<label>Área o Departamento * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_area" id="reflab4_area">
									<br>
								</div>
								<div class="col-md-3">
									<label>Domicilio, calle y número * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_domicilio" id="reflab4_domicilio">
									<br>
								</div>
								<div class="col-md-3">
									<label>Colonia * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_colonia" id="reflab4_colonia">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2">
									<label>Código postal * </label>
									<input type="text" class="form-control solo_numeros reflab4_obligado" name="reflab4_cp" id="reflab4_cp" maxlength="5">
									<br>
								</div>
								<div class="col-md-2">
									<label>Teléfono * </label>
									<input type="text" class="form-control solo_numeros reflab4_obligado" name="reflab4_telefono" id="reflab4_telefono" maxlength="12">
									<br>
								</div>
								<div class="col-md-2">
									<label>Tipo de empresa * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_tipo" id="reflab4_tipo">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto desempeñado * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_puesto" id="reflab4_puesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Periodo trabajado, mes y año * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_periodo" id="reflab4_periodo">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label>Nombre del último jefe * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_jefenombre" id="reflab4_jefenombre">
									<br>
								</div>
								<div class="col-md-3">
									<label>Puesto del último jefe * </label>
									<input type="text" class="form-control reflab4_obligado" name="reflab4_jefepuesto" id="reflab4_jefepuesto">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual inicial * </label>
									<input type="text" class="form-control solo_numeros reflab4_obligado" name="reflab4_sueldo1" id="reflab4_sueldo1" maxlength="10">
									<br>
								</div>
								<div class="col-md-3">
									<label>Sueldo mensual final * </label>
									<input type="text" class="form-control solo_numeros reflab4_obligado" name="reflab4_sueldo2" id="reflab4_sueldo2" maxlength="10">
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>¿En qué consistía su trabajo? *</label>
									<textarea class="form-control reflab4_obligado" cols="2" name="reflab4_actividades" id="reflab4_actividades"></textarea>
									<br>
								</div>
								<div class="col-md-6">
									<label>Causa de separación *</label>
									<textarea class="form-control reflab4_obligado" cols="2" name="reflab4_razon" id="reflab4_razon"></textarea>
									<br>
								</div>
							</div>
							<br>
							<p class="tituloSubseccion text-center">Características analizadas</p><br>
							<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<label for="aplicar_todo4">Aplicar a todo</label>
									<select id="aplicar_todo4" class="form-control">
										<option value="-1">Selecciona</option>
										<option value="0">No proporciona</option>
										<option value="1">Excelente</option>
										<option value="2">Bueno</option>
										<option value="3">Regular</option>
										<option value="4">Insuficiente</option>
										<option value="5">Muy mal</option>
									</select>
									<br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab4_calidad">Calidad en el trabajo *</label>
									<select name="reflab4_calidad" id="reflab4_calidad" class="form-control performance4 reflab4_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab4_puntualidad">Puntualidad y asistencia *</label>
									<select name="reflab4_puntualidad" id="reflab4_puntualidad" class="form-control performance4 reflab4_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab4_honesto">Honradez e integridad *</label>
									<select name="reflab4_honesto" id="reflab4_honesto" class="form-control performance4 reflab4_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab4_responsabilidad">Responsabilidad *</label>
									<select name="reflab4_responsabilidad" id="reflab4_responsabilidad" class="form-control performance4 reflab4_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="reflab4_adaptacion">Adaptación al trabajo *</label>
									<select name="reflab4_adaptacion" id="reflab4_adaptacion" class="form-control performance4 reflab4_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab4_actitud_jefes">Actitud hacia sus jefes *</label>
									<select name="reflab4_actitud_jefes" id="reflab4_actitud_jefes" class="form-control performance4 reflab4_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
								<div class="col-md-3">
									<label for="reflab4_actitud_comp">Actitud hacia sus compañeros *</label>
									<select name="reflab4_actitud_comp" id="reflab4_actitud_comp" class="form-control performance4 reflab4_obligado">
										<option value="No proporciona">No proporciona</option>
										<option value="Excelente">Excelente</option>
										<option value="Bueno">Bueno</option>
										<option value="Regular">Regular</option>
										<option value="Insuficiente">Insuficiente</option>
										<option value="Muy mal">Muy mal</option>
									</select>
									<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label>Comentarios y observaciones *</label>
									<textarea class="form-control reflab4_obligado" name="reflab4_comentarios" id="reflab4_comentarios" rows="2"></textarea><br><br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-md-offset-3">
									<button type="button" class="btn btn-primary" onclick="actualizarLaboral(4)">Guardar los cambios del Trabajo #4</button>
									<br><br><br><br>
								</div>
								<div class="col-md-3">
									<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
									<br><br><br><br>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content" id="formulario_ref_laborales" style="display: none;">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<div class="box-header tituloSubseccion">
							<p class="box-title"><strong> Referencias Laborales </strong></p>
						</div>
						<?php
						for ($i = 1; $i <= 10; $i++) {
							echo '<div class="box-header text-center tituloSubseccion">
	                  				<p class="box-title " id="titulo_reflab' . $i . '"><strong> Trabajo #' . $i . ' </strong><hr></p>
            					</div>';
							echo '<div class="row">
            						<form id="candidato_reflaboral' . $i . '">
	                					<div class="col-md-6">
	                						<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                						<div class="row">
	                							<div class="col-md-3">
					                				<label>Compañía: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
		                							<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_empresa_ingles" id="reflab' . $i . '_empresa_ingles" >
	        										<br>
		                						</div>
	                						</div>
	                						<div class="row">
				                				<div class="col-md-3">
					                				<label>Dirección: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_direccion_ingles" id="reflab' . $i . '_direccion_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Fecha de entrada: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_entrada_ingles" id="reflab' . $i . '_entrada_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Fecha de salida: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_salida_ingles" id="reflab' . $i . '_salida_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Teléfono: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control solo_numeros reflabingles' . $i . '_obligado" name="reflab' . $i . '_telefono_ingles" id="reflab' . $i . '_telefono_ingles" maxlength="10">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Puesto inicial: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_puesto1_ingles" id="reflab' . $i . '_puesto1_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Puesto final: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_puesto2_ingles" id="reflab' . $i . '_puesto2_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Salario inicial: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_salario1_ingles" id="reflab' . $i . '_salario1_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Salario final: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_salario2_ingles" id="reflab' . $i . '_salario2_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Jefe inmediato: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_jefenombre_ingles" id="reflab' . $i . '_jefenombre_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Correo del jefe inmediato: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_jefecorreo_ingles" id="reflab' . $i . '_jefecorreo_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Puesto del jefe inmediato:</label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_jefepuesto_ingles" id="reflab' . $i . '_jefepuesto_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Causa de separación: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_separacion_ingles" id="reflab' . $i . '_separacion_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3 col-md-offset-4">
				                					<button type="button" class="btn btn-info" onclick="actualizarReferenciaLaboral(' . $i . ')">Guardar referencia laboral</button><br><br><br><br>
				                					<input type="hidden" id="idreflabingles' . $i . '">
				                				</div>
				                			</div>
				                		</div>
				                	</form>
				                	<form id="analista_reflaboral' . $i . '">
	                					<div class="col-md-6">
                							<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                							<div class="row">
					                				<div class="col-md-3">
						                				<label>Compañía: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
									        			<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_empresa" id="an_reflab' . $i . '_empresa" >
									        			<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Dirección: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_direccion" id="an_reflab' . $i . '_direccion">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Fecha de entrada: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_entrada" id="an_reflab' . $i . '_entrada">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Fecha de salida: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_salida" id="an_reflab' . $i . '_salida">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Teléfono: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control solo_numeros ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_telefono" id="an_reflab' . $i . '_telefono" maxlength="10">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Puesto inicial: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_puesto1" id="an_reflab' . $i . '_puesto1">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Puesto final: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_puesto2" id="an_reflab' . $i . '_puesto2">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Salario inicial: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_salario1" id="an_reflab' . $i . '_salario1">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Salario final: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_salario2" id="an_reflab' . $i . '_salario2">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Jefe inmediato: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_jefenombre" id="an_reflab' . $i . '_jefenombre">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Correo del jefe inmediato: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_jefecorreo" id="an_reflab' . $i . '_jefecorreo">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Puesto del jefe inmediato: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_jefepuesto" id="an_reflab' . $i . '_jefepuesto">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Causa de separación: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_separacion" id="an_reflab' . $i . '_separacion">
						        						<br>
						                			</div>
					                			</div>
					                		</div>
	                					</div>
				                		<div class="row">
				                			<div class="col-md-6">
				                				<label>Fortalezas o cualidades del candidato *</label>
				                				<textarea class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_cualidades" id="an_reflab' . $i . '_cualidades" rows="2"></textarea><br><br>
				                			</div>
				                			<div class="col-md-6">
				                				<label>Áreas a mejorar del candidato *</label>
				                				<textarea class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_mejoras" id="an_reflab' . $i . '_mejoras" rows="2"></textarea><br><br>
				                			</div>
				                		</div>
				                		<div class="row">
				                			<div class="col-md-12">
				                				<label>Comentarios *</label>
				                				<textarea class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_comentarios" id="an_reflab' . $i . '_comentarios" rows="2"></textarea><br><br>
				                			</div>
				                		</div>
            						</form>
			            			<div class="row">
							        	<div class="col-md-3 col-md-offset-3">
							        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(' . $i . ')">Guardar la verificación del trabajo #' . $i . '</button>
							        		<br><br><input type="hidden" id="idverlab' . $i . '">
							        	</div>
							        	<div class="col-md-3">
							        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
							        		<br><br><br><br>
							        	</div>
							        </div>';
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<!-- /.content-wrapper -->
<script>
	var id = '<?php echo $this->uri->segment(3) ?>';
	var url = '<?php echo base_url('Cliente/getCandidatos?id='); ?>' + id;
	var psico = '<?php echo base_url(); ?>_psicometria/';
	$(document).ready(function() {
		//inputmask
		$('#fecha_nacimiento').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_nacimiento_ingles').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_acta').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_domicilio').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_imss').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_curp').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_retencion').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_rfc').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_licencia').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_migra').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_visa').inputmask('dd/mm/yyyy', {
			'placeholder': 'dd/mm/yyyy'
		});
		$('#fecha_ine').inputmask('yyyy', {
			'placeholder': 'yyyy'
		});
		var msj = localStorage.getItem("success");
		var msj2 = localStorage.getItem("finished");
		var msj3 = localStorage.getItem("deleted");
		if (msj == 1) {
			$("#texto_msj").text(" Los datos se han guardado correctamente");
			$("#mensaje").css('display', 'block');
			setTimeout(function() {
				$('#mensaje').fadeOut();
			}, 4000);
			localStorage.removeItem("success");
		}
		if (msj2 == 1) {
			$("#exitoFinalizado").css('display', 'block');
			setTimeout(function() {
				$('#exitoFinalizado').fadeOut();
			}, 4000);
			localStorage.removeItem("finished");
		}
		if (msj3 == 1) {
			$("#texto_msj").text(" Candidato eliminado correctamente");
			$("#mensaje").css('display', 'block');
			setTimeout(function() {
				$('#mensaje').fadeOut();
			}, 4000);
			localStorage.removeItem("deleted");
		}
		$('#tabla').DataTable({
			"pageLength": 25,
			//"pagingType": "simple",
			"order": [0, "desc"],
			"stateSave": true,
			"serverSide": false,
			"ajax": url,
			"columns": [{
					title: 'id',
					data: 'id',
					visible: false
				},
				{
					title: 'Candidato',
					data: 'candidato',
					"width": "15%",
					mRender: function(data, type, full) {
						var analista = (full.usuario == null | full.usuario == '') ? '<small><b>(Sin definir)</b></small>' : '<small><b>('+full.usuario+')</b></small>';
						return '<a data-toggle="tooltip" class="sin_vinculo" title="' + full.id + '">' + data + '</a><br>'+analista;
					}
				},
				{
					title: 'Subcliente',
					data: 'cliente',
					"width": "10%",
					mRender: function(data, type, full) {
						var subcliente = (full.subcliente == null || full.subcliente == "") ? "-" : full.subcliente;
						return subcliente;
					}
				},
				{
					title: 'Fechas',
					data: 'fecha_alta',
					"width": "13%",
					mRender: function(data, type, full) {
						if (full.id_subcliente != 180) {
							var f = data.split(' ');
							var h = f[1];
							var aux = h.split(':');
							var hora = aux[0] + ':' + aux[1];
							var aux = f[0].split('-');
							var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
							var f_inicio = fecha + ' ' + hora;
							if (full.fecha_final != null) {
								var f = full.fecha_final.split(' ');
								var h = f[1];
								var aux = h.split(':');
								var hora = aux[0] + ':' + aux[1];
								var aux = f[0].split('-');
								var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
								var f_fin = "Final: " + fecha + ' ' + hora;
								return "Alta: " + f_inicio + '<br>' + f_fin;
							} else {
								var f = new Date();
								var hoy = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
								return "Alta: " + f_inicio + '<br>Final: -';
							}
						} else {
							var f = data.split(' ');
							var h = f[1];
							var aux = h.split(':');
							var hora = aux[0] + ':' + aux[1];
							var aux = f[0].split('-');
							var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
							var f_inicio = fecha + ' ' + hora;
							if (full.fecha_final_ingles != null) {
								var f = full.fecha_final_ingles.split(' ');
								var h = f[1];
								var aux = h.split(':');
								var hora = aux[0] + ':' + aux[1];
								var aux = f[0].split('-');
								var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
								var f_fin = "Final: " + fecha + ' ' + hora;
								return "Alta: " + f_inicio + '<br>' + f_fin;
							} else {
								var f = new Date();
								var hoy = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
								return "Alta: " + f_inicio + '<br>Final: -';
							}
						}
					}
				},
				{
					title: 'SLA',
					data: 'tiempo',
					"width": "8%",
					mRender: function(data, type, full) {
						if (full.id_tipo_proceso == 1) {
							if (data != null) {
								if (data != -1) {
									if (data >= 0 && data <= 2) {
										return res = '<div class="formato_dias dias_verde">' + data + ' días</div>';
									}
									if (data > 2 && data <= 4) {
										return res = '<div class="formato_dias dias_amarillo">' + data + ' días</div>';
									}
									if (data >= 5) {
										return res = '<div class="formato_dias dias_rojo">' + data + ' días</div>';
									}
								} else {
									return "Actualizando...";
								}
							} else {
								if (full.tiempo_parcial != 0) {
									var parcial = full.tiempo_parcial;
									if (parcial >= 0 && parcial < 2) {
										return res = '<div class="formato_dias dias_verde">' + parcial + ' días</div>';
									}
									if (parcial >= 2 && parcial <= 4) {
										return res = '<div class="formato_dias dias_amarillo">' + parcial + ' días</div>';
									}
									if (parcial >= 5) {
										return res = '<div class="formato_dias dias_rojo">' + parcial + ' días</div>';
									}
								} else {
									return "Actualizando...";
								}
							}
						} else {
							if (full.tiempo_ingles != null) {
								if (full.tiempo_ingles != -1) {
									if (full.tiempo_ingles >= 0 && full.tiempo_ingles <= 2) {
										return res = '<div class="formato_dias dias_verde">' + full.tiempo_ingles + ' días</div>';
									}
									if (full.tiempo_ingles > 2 && full.tiempo_ingles <= 4) {
										return res = '<div class="formato_dias dias_amarillo">' + full.tiempo_ingles + ' días</div>';
									}
									if (full.tiempo_ingles >= 5) {
										return res = '<div class="formato_dias dias_rojo">' + full.tiempo_ingles + ' días</div>';
									}
								} else {
									return "Actualizando...";
								}
							} else {
								if (full.tiempo_parcial != 0) {
									var parcial = full.tiempo_parcial;
									if (parcial >= 0 && parcial < 2) {
										return res = '<div class="formato_dias dias_verde">' + parcial + ' días</div>';
									}
									if (parcial >= 2 && parcial <= 4) {
										return res = '<div class="formato_dias dias_amarillo">' + parcial + ' días</div>';
									}
									if (parcial >= 5) {
										return res = '<div class="formato_dias dias_rojo">' + parcial + ' días</div>';
									}
								} else {
									return "Actualizando...";
								}
							}
						}

					}
				},
				{
					title: 'Mensajes',
					data: 'id',
					bSortable: false,
					"width": "3%",
					mRender: function(data, type, full) {
						return '<a href="javascript:void(0)" data-toggle="tooltip"  title="Mensajes de avances" id="msj_avances" class="fa-tooltip icono_datatable"><i class="fas fa-comment-dots"></i></a>';
					}
				},
				{
					title: 'Estudio',
					data: 'id',
					bSortable: false,
					"width": "13%",
					mRender: function(data, type, full) {
						if (full.id_subcliente != 180) {
							if (full.idFinalizado != null) {
								return '<div class="btn-group"  style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:void(0)" id="datos_generales">Datos generales</a></div></div> ';
							} else {
								return '<div class="btn-group"  style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:void(0)" id="datos_generales">Datos generales</a><a class="dropdown-item" href="javascript:void(0)" id="datos_academicos">Historial académico</a></div></div> ';
							}
						} else {
							if (full.id_tipo_proceso == 3) {
								if (full.idBGC != null) {
									return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales2">Personal data</a></li><li><a href="#" id="datos_mayores_estudios">Highest studies</a></li><li><a href="#" id="datos_laborales_ingles">Labor references</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="verificacion_penales">Criminal verification (Mexico)</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="conclusiones_ingles">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
								} else {
									return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales2">Personal data</a></li><li><a href="#" id="datos_mayores_estudios">Highest studies</a></li><li><a href="#" id="datos_laborales_ingles">Labor references</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="verificacion_penales">Criminal verification (Mexico)</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
								}
							}
							if (full.id_tipo_proceso == 4) {
								if (full.idBGC != null) {
									return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales2">Personal data</a></li><li><a href="#" id="datos_mayores_estudios">Highest studies</a></li><li><a href="#" id="datos_identidad">Identity check</a></li><li><a href="#" id="datos_laborales_ingles">Labor references</a></li><li><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="verificacion_penales">Criminal verification (Mexico)</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="conclusiones_ingles">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
								} else {
									return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales2">Personal data</a></li><li><a href="#" id="datos_mayores_estudios">Highest studies</a></li><li><a href="#" id="datos_identidad">Identity check</a></li><li><a href="#" id="datos_laborales_ingles">Labor references</a></li><li><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="verificacion_penales">Criminal verification (Mexico)</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
								}
							}
							if (full.id_tipo_proceso == 5) {
								if (full.idBGC != null) {
									return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales2">Personal data</a></li><li><a href="#" id="datos_identidad">Identity check</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="conclusiones_ingles">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
								} else {
									return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales2">Personal data</a></li><li><a href="#" id="datos_identidad">Identity check</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
								}
							}
						}
					}
				},
				{
					title: 'Visita',
					data: 'visitador',
					"width": "10%",
					mRender: function(data, type, full) {
						if (full.id_subcliente != 180) {
							if (data == 1) {
								return '<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Datos de la visita <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="visita_documentos">Documentos</a></li><li><a href="#" id="visita_familiares">Grupo familiar</a></li><li><a href="#" id="visita_egresos">Egresos mensuales</a></li><li><a href="#" id="visita_habitacion">Habitacion y medio ambiente</a></li><li><a href="#" id="visita_vecinales">Referencias vecinales</a></li></ul></div>';
							} else {
								return "<i class='fas fa-circle estatus0'></i>";
							}
						} else {
							return "N/A";
						}
					}
				},
				{
					title: 'Médico',
					data: 'id',
					width: '6%',
					mRender: function(data, type, full) {
						if (full.medico == 1) {
							if (full.imagen != null && full.conclusion != null) {
								return '<div style="display: inline-block;"><form id="med' + full.idMedico + '" action="<?php echo base_url('Medico/crearPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfMedico" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idMedico" id="idMedico' + full.idMedico + '" value="' + full.idMedico + '"></form></div>';
							} else {
								return "<i class='fas fa-circle status_bgc0'></i> En proceso";
							}
						} else {
							return "N/A";
						}
					}
				},
				{
					title: 'Psicometrico',
					data: 'id',
					bSortable: false,
					"width": "6%",
					mRender: function(data, type, full) {
						if (full.psicometrico == 1) {
							if (full.archivo != null && full.archivo != "") {
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Subir psicometria" id="psicometria" class="fa-tooltip a-acciones"><i class="fas fa-brain"></i></a> <a href="' + psico + full.archivo + '" target="_blank" data-toggle="tooltip" title="Ver psicometria" id="descarga_psicometrico" class="fa-tooltip a-acciones"><i class="fas fa-file-powerpoint"></i></a>';
							} else {
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Subir psicometria" id="psicometria" class="fa-tooltip a-acciones"><i class="fas fa-brain"></i></a>';
							}
						} else {
							return "N/A";
						}
					}
				},
				{
					title: 'Antidoping',
					data: 'id',
					bSortable: false,
					"width": "6%",
					mRender: function(data, type, full) {
						if (full.tipo_antidoping == 1) {
							if(full.doping_hecho == 1){
								if (full.fecha_resultado != null && full.fecha_resultado != "" && full.statusDoping == 1) {
									if (full.resultado_doping == 1) {
										return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;margin-left:3px;"><form id="pdfForm' + full.idDoping + '" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop' + full.idDoping + '" value="' + full.idDoping + '"></form></div>';
									} else {
										return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;margin-left:3px;"><form id="pdfForm' + full.idDoping + '" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop' + full.idDoping + '" value="' + full.idDoping + '"></form></div>';
									}

								} else {
									return "Pendiente";
								}
							}else {
								return "Pendiente";
							}
						}
						if (full.tipo_antidoping == 0 || full.tipo_antidoping == "" || full.tipo_antidoping == null) {
							return "N/A";
						}
					}
				},
				{
					title: 'Resultado',
					data: 'id',
					bSortable: false,
					"width": "6%",
					mRender: function(data, type, full) {
						/*var previo = '<div style="display: inline-block;"><form id="previopdf'+data+'" action="<?php //echo base_url('Candidato/crearPrevioPDF'); 
																																																			?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfPrevio" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPrevio" id="idPrevio'+data+'" value="'+data+'"></form></div>';*/
						var previo = '';
						if (full.status == 0) {
							if (full.id_subcliente != 180) {
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a> ' + previo + ' ';
							} else {
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final_ingles" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a> ' + previo + ' ';
							}
						} else {
							if (full.id_subcliente != 180) {
								if (full.status_bgc == 1) {
									return '<i class="fas fa-circle status_bgc1"></i><div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Candidato/crearEstudioPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
								if (full.status_bgc == 2) {
									return '<i class="fas fa-circle status_bgc2"></i><div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Candidato/crearEstudioPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
								if (full.status_bgc == 3) {
									return '<i class="fas fa-circle status_bgc3"></i><div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Candidato/crearEstudioPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
							} else {
								if (full.status_bgc == 0) {
									return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final_ingles" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
								}
								if (full.status_bgc == 1) {
									return '<i class="fas fa-circle status_bgc1"></i><div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Candidato/crearEstudioSubclienteInglesPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
								if (full.status_bgc == 2) {
									return '<i class="fas fa-circle status_bgc2"></i><div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Candidato/crearEstudioSubclienteInglesPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
								if (full.status_bgc == 3) {
									return '<i class="fas fa-circle status_bgc3"></i><div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Candidato/crearEstudioSubclienteInglesPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
							}
						}
					}
				}
			],
			fnDrawCallback: function(oSettings) {
				$('a[data-toggle="tooltip"]').tooltip({
					trigger: "hover"
				});
			},
			rowCallback: function(row, data) {
				$('a[id^=pdfPrevio]', row).bind('click', () => {
					var id = data.id;
					$('#previopdf' + id).submit();
				});
				$('a#actualizacion_candidato', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#idDoping").val(data.idDoping);

					$("#actualizarCandidatoModal").modal('show');
				});
				$('a#datos_generales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#nombre_general").val(data.nombre);
					$("#paterno_general").val(data.paterno);
					$("#materno_general").val(data.materno);
					if (data.fecha_nacimiento != "" && data.fecha_nacimiento != null) {
						var f_nacimiento = convertirDate(data.fecha_nacimiento);
						$("#fecha_nacimiento").val(f_nacimiento);
					} else {
						$("#fecha_nacimiento").val("");
					}
					$("#puesto_general").val(data.id_puesto);
					$("#lugar").val(data.lugar_nacimiento);
					$("#genero").val(data.genero);
					$("#calle").val(data.calle);
					$("#exterior").val(data.exterior);
					$("#interior").val(data.interior);
					$("#colonia").val(data.colonia);
					$("#calles").val(data.entre_calles);
					$("#estado").val(data.id_estado);
					if (data.id_estado != "" && data.id_estado != null && data.id_estado != 0) {
						getMunicipio(data.id_estado, data.id_municipio);
					} else {
						$('#municipio').prop('disabled', true);
						$('#municipio').val('');
					}
					$("#cp").val(data.cp);
					$("#civil").val(data.id_estado_civil);
					$("#celular_general").val(data.celular);
					$("#tel_casa").val(data.telefono_casa);
					$("#grado").val(data.id_grado_estudio);
					$("#generalesModal").modal('show');
				});
				$('a#datos_generales2', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#nombre_ingles").val(data.nombre);
					$("#paterno_ingles").val(data.paterno);
					$("#materno_ingles").val(data.materno);
					if (data.fecha_nacimiento != "" && data.fecha_nacimiento != null) {
						var f_nacimiento = convertirDate(data.fecha_nacimiento);
						$("#fecha_nacimiento_ingles").val(f_nacimiento);
						$("#edad_ingles").val(data.edad);
					} else {
						$("#fecha_nacimiento_ingles").val("");
						$("#edad_ingles").val("");
					}
					$("#puesto_ingles").val(data.puesto_ingles);
					$("#nacionalidad_ingles").val(data.nacionalidad);
					$("#genero_ingles").val(data.genero);
					$("#calle_ingles").val(data.calle);
					$("#exterior_ingles").val(data.exterior);
					$("#interior_ingles").val(data.interior);
					$("#colonia_ingles").val(data.colonia);
					$("#calles_ingles").val(data.entre_calles);
					$("#estado_ingles").val(data.id_estado);
					if (data.id_estado != "" && data.id_estado != null && data.id_estado != 0) {
						getMunicipioingles(data.id_estado, data.id_municipio);
					} else {
						$('#municipio_ingles').prop('disabled', true);
					}
					$("#cp_ingles").val(data.cp);
					$("#civil_ingles").val(data.id_estado_civil);
					$("#celular_general_ingles").val(data.celular);
					$("#tel_casa_ingles").val(data.telefono_casa);
					$("#personales_correo_ingles").val(data.correo);
					$("#generales2Modal").modal('show');
				});
				$('a#datos_identidad', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#ine").val(data.ver_ine);
					$("#ine_ano").val(data.ine_ano);
					$("#ine_vertical").val(data.ine_vertical);
					$("#ine_institucion").val(data.ine_institucion);
					$("#pasaporte").val(data.pasaporte);
					$("#pasaporte_fecha").val(data.pasaporte_fecha);
					$("#penales_id").val(data.ver_penales);
					$("#penales_institucion").val(data.penales_institucion);
					$("#forma_migratoria").val(data.forma_migratoria);
					$("#forma_migratoria_fecha").val(data.forma_migratoria_fecha);
					$("#identidad_comentarios").val(data.ver_comentarios);
					$("#identidadModal").modal('show');
				});
				$('a#datos_globales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#law_enforcement").val(data.law_enforcement);
					$("#regulatory").val(data.regulatory);
					$("#sanctions").val(data.sanctions);
					$("#other_bodies").val(data.other_bodies);
					$("#media_searches").val(data.media_searches);
					$("#global_comentarios").val(data.global_comentarios);
					$("#globalesGeneralModal").modal('show');
				});
				$('a#datos_academicos', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#prim_periodo").val(data.primaria_periodo);
					$("#prim_escuela").val(data.primaria_escuela);
					$("#prim_ciudad").val(data.primaria_ciudad);
					$("#prim_certificado").val(data.primaria_certificado);
					$("#prim_promedio").val(data.primaria_promedio);
					$("#sec_periodo").val(data.secundaria_periodo);
					$("#sec_escuela").val(data.secundaria_escuela);
					$("#sec_ciudad").val(data.secundaria_ciudad);
					$("#sec_certificado").val(data.secundaria_certificado);
					$("#sec_promedio").val(data.secundaria_promedio);
					$("#prep_periodo").val(data.preparatoria_periodo);
					$("#prep_escuela").val(data.preparatoria_escuela);
					$("#prep_ciudad").val(data.preparatoria_ciudad);
					$("#prep_certificado").val(data.preparatoria_certificado);
					$("#prep_promedio").val(data.preparatoria_promedio);
					$("#lic_periodo").val(data.licenciatura_periodo);
					$("#lic_escuela").val(data.licenciatura_escuela);
					$("#lic_ciudad").val(data.licenciatura_ciudad);
					$("#lic_certificado").val(data.licenciatura_certificado);
					$("#lic_promedio").val(data.licenciatura_promedio);
					$("#comercial_promedio").val(data.comercial_promedio);
					$("#comercial_periodo").val(data.comercial_periodo);
					$("#comercial_escuela").val(data.comercial_escuela);
					$("#comercial_ciudad").val(data.comercial_ciudad);
					$("#comercial_certificado").val(data.comercial_certificado);
					$("#comercial_promedio").val(data.comercial_promedio);
					$("#actual_periodo").val(data.actual_periodo);
					$("#actual_escuela").val(data.actual_escuela);
					$("#actual_ciudad").val(data.actual_ciudad);
					$("#actual_certificado").val(data.actual_certificado);
					$("#actual_promedio").val(data.actual_promedio);
					$("#cedula").val(data.cedula_profesional);
					$("#otro_certificado").val(data.otros_certificados);
					$("#estudios_comentarios").val(data.comentarios);
					$("#carrera_inactivo").val(data.carrera_inactivo);
					$("#academicosModal").modal('show');
				});
				$('a#datos_mayores_estudios', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#idMayoresEstudios").val(data.idMayores);
					$("#mayor_estudios").val(data.id_grado_estudio);
					$("#estudios_periodo").val(data.estudios_periodo);
					$("#estudios_escuela").val(data.estudios_escuela);
					$("#estudios_ciudad").val(data.estudios_ciudad);
					$("#estudios_certificado").val(data.estudios_certificado);
					$("#mayor_estudios2").val(data.id_tipo_studies);
					$("#estudios_periodo2").val(data.periodo);
					$("#estudios_escuela2").val(data.escuela);
					$("#estudios_ciudad2").val(data.ciudad);
					$("#estudios_certificado2").val(data.certificado);
					$("#mayor_estudios_comentarios").val(data.estudios_comentarios);
					$("#mayoresEstudiosModal").modal('show');
				});
				$('a#datos_sociales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#sindical").val(data.sindical);
					$("#sindical_nombre").val(data.sindical_nombre);
					$("#sindical_cargo").val(data.sindical_cargo);
					$("#partido").val(data.partido);
					$("#partido_nombre").val(data.partido_nombre);
					$("#partido_cargo").val(data.partido_cargo);
					$("#club").val(data.club);
					$("#deporte").val(data.deporte);
					$("#religion").val(data.religion);
					$("#religion_frecuencia").val(data.religion_frecuencia);
					$("#bebidas").val(data.bebidas);
					$("#bebidas_frecuencia").val(data.bebidas_frecuencia);
					$("#fumar").val(data.fumar);
					$("#fumar_frecuencia").val(data.fumar_frecuencia);
					$("#cirugia").val(data.cirugia);
					$("#enfermedades").val(data.enfermedades);
					$("#corto_plazo").val(data.corto_plazo);
					$("#mediano_plazo").val(data.mediano_plazo);
					$("#socialesModal").modal('show');
				});
				$('a#datos_ref_personales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_refpersonal1")[0].reset();
					$("#d_refpersonal2")[0].reset();
					$("#d_refpersonal3")[0].reset();
					$.ajax({
						url: '<?php echo base_url('Candidato/getPersonalesOperador'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						dataType: "text",
						success: function(res) {
							if (res != 0) {
								var rows = res.split('###');
								for (var i = 0; i < rows.length; i++) {
									if (rows[i] != "") {
										var dato = rows[i].split('@@');
										$("#refPer" + (i + 1) + "_nombre").val(dato[0]);
										$("#refPer" + (i + 1) + "_telefono").val(dato[1]);
										$("#refPer" + (i + 1) + "_tiempo").val(dato[2]);
										$("#refPer" + (i + 1) + "_id").val(dato[3]);
										$("#refPer" + (i + 1) + "_recomienda").val(dato[4]);
										$("#refPer" + (i + 1) + "_comentario").val(dato[5]);
									}
								}
							}
						}
					});
					$("#refPersonalesModal").modal('show');
				});
				$('a#datos_laborales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#data_reflaboral1")[0].reset();
					$("#data_reflaboral2")[0].reset();
					$("#data_reflaboral3")[0].reset();
					$("#data_reflaboral4")[0].reset();
					$.ajax({
						url: '<?php echo base_url('Candidato/getAntecedentesLaborales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						dataType: "text",
						success: function(res) {
							if (res != "") {
								var rows = res.split('###');
								//Primera referencia laboral
								for ($i = 0; $i < rows.length; $i++) {
									if (rows[$i] != "") {
										var dato = rows[$i].split('@@');
										$("#reflab" + ($i + 1) + "_empresa").val(dato[0]);
										$("#reflab" + ($i + 1) + "_area").val(dato[1]);
										$("#reflab" + ($i + 1) + "_domicilio").val(dato[2]);
										$("#reflab" + ($i + 1) + "_colonia").val(dato[3]);
										$("#reflab" + ($i + 1) + "_cp").val(dato[4]);
										$("#reflab" + ($i + 1) + "_telefono").val(dato[5]);
										$("#reflab" + ($i + 1) + "_tipo").val(dato[6]);
										$("#reflab" + ($i + 1) + "_puesto").val(dato[7]);
										$("#reflab" + ($i + 1) + "_periodo").val(dato[8]);
										$("#reflab" + ($i + 1) + "_jefenombre").val(dato[9]);
										$("#reflab" + ($i + 1) + "_jefepuesto").val(dato[10]);
										$("#reflab" + ($i + 1) + "_sueldo1").val(dato[11]);
										$("#reflab" + ($i + 1) + "_sueldo2").val(dato[12]);
										$("#reflab" + ($i + 1) + "_actividades").val(dato[13]);
										$("#reflab" + ($i + 1) + "_razon").val(dato[14]);
										$("#reflab" + ($i + 1) + "_calidad").val(dato[15]);
										$("#reflab" + ($i + 1) + "_puntualidad").val(dato[16]);
										$("#reflab" + ($i + 1) + "_honesto").val(dato[17]);
										$("#reflab" + ($i + 1) + "_responsabilidad").val(dato[18]);
										$("#reflab" + ($i + 1) + "_adaptacion").val(dato[19]);
										$("#reflab" + ($i + 1) + "_actitud_jefes").val(dato[20]);
										$("#reflab" + ($i + 1) + "_actitud_comp").val(dato[21]);
										$("#reflab" + ($i + 1) + "_comentarios").val(dato[22]);
										$("#idreflab" + ($i + 1)).val(dato[23]);
									}
								}
							}

						}
					});
					$("#listado").css('display', 'none');
					$("#btn_nuevo").css('display', 'none');
					$("#formulario").css('display', 'block');
					$("#btn_regresar").css('display', 'block');
				});
				$('a#datos_laborales_ingles', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					//$("#candidato_reflaboral1")[0].reset();
					$.ajax({
						async: false,
						url: '<?php echo base_url('Candidato/getRefLaborales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						dataType: "text",
						success: function(res) {
							if (res != "") {
								var rows = res.split('###');
								for ($i = 0; $i < rows.length; $i++) {
									if (rows[$i] != "") {
										var dato = rows[$i].split('@@');
										$("#reflab" + ($i + 1) + "_empresa_ingles").val(dato[0]);
										$("#reflab" + ($i + 1) + "_direccion_ingles").val(dato[1]);
										$("#reflab" + ($i + 1) + "_entrada_ingles").val(dato[2]);
										$("#reflab" + ($i + 1) + "_salida_ingles").val(dato[3]);
										$("#reflab" + ($i + 1) + "_telefono_ingles").val(dato[4]);
										$("#reflab" + ($i + 1) + "_puesto1_ingles").val(dato[5]);
										$("#reflab" + ($i + 1) + "_puesto2_ingles").val(dato[6]);
										$("#reflab" + ($i + 1) + "_salario1_ingles").val(dato[7]);
										$("#reflab" + ($i + 1) + "_salario2_ingles").val(dato[8]);
										$("#reflab" + ($i + 1) + "_jefenombre_ingles").val(dato[9]);
										$("#reflab" + ($i + 1) + "_jefecorreo_ingles").val(dato[10]);
										$("#reflab" + ($i + 1) + "_jefepuesto_ingles").val(dato[11]);
										$("#reflab" + ($i + 1) + "_separacion_ingles").val(dato[12]);
										$("#idreflabingles" + ($i + 1)).val(dato[13]);
									}
								}
							}

						}
					});
					$.ajax({
						async: false,
						url: '<?php echo base_url('Candidato/getVerificacionRefLaborales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						dataType: "text",
						success: function(res) {
							if (res != "") {
								var rows = res.split('###');
								for ($i = 0; $i < rows.length; $i++) {
									if (rows[$i] != "") {
										var dato = rows[$i].split('@@');
										$("#an_reflab" + dato[17] + "_empresa").val(dato[0]);
										$("#an_reflab" + dato[17] + "_direccion").val(dato[1]);
										$("#an_reflab" + dato[17] + "_entrada").val(dato[2]);
										$("#an_reflab" + dato[17] + "_salida").val(dato[3]);
										$("#an_reflab" + dato[17] + "_telefono").val(dato[4]);
										$("#an_reflab" + dato[17] + "_puesto1").val(dato[5]);
										$("#an_reflab" + dato[17] + "_puesto2").val(dato[6]);
										$("#an_reflab" + dato[17] + "_salario1").val(dato[7]);
										$("#an_reflab" + dato[17] + "_salario2").val(dato[8]);
										$("#an_reflab" + dato[17] + "_jefenombre").val(dato[9]);
										$("#an_reflab" + dato[17] + "_jefecorreo").val(dato[10]);
										$("#an_reflab" + dato[17] + "_jefepuesto").val(dato[11]);
										$("#an_reflab" + dato[17] + "_separacion").val(dato[12]);
										$("#an_reflab" + dato[17] + "_comentarios").val(dato[13]);
										$("#an_reflab" + dato[17] + "_cualidades").val(dato[14]);
										$("#an_reflab" + dato[17] + "_mejoras").val(dato[15]);
										$("#idverlab" + dato[17]).val(dato[16]);
									}
								}
							}

						}
					});
					$("#nombreCliente").text(data.cliente);
					$("#listado").css('display', 'none');
					$("#btn_nuevo").css('display', 'none');
					$("#formulario_ref_laborales").css('display', 'block');
					$("#btn_regresar").css('display', 'block');
				});
				$('a#datos_legales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_investigacion")[0].reset();
					$("#inv_penal").val(data.penal);
					$("#inv_penal_notas").val(data.penal_notas);
					$("#inv_civil").val(data.inv_civil);
					$("#inv_civil_notas").val(data.civil_notas);
					$("#inv_laboral").val(data.inv_laboral);
					$("#inv_laboral_notas").val(data.laboral_notas);
					$("#legalesModal").modal('show');
				});
				$('a#datos_no_mencionados', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_no_mencionados")[0].reset();
					$("#no_mencionados").val(data.no_mencionados);
					$("#resultado_no_mencionados").val(data.resultado_no_mencionados);
					$("#notas_no_mencionados").val(data.notas_no_mencionados);
					$("#noMencionadosModal").modal('show');
				});
				$('a#visita_documentos', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_documentos")[0].reset();

					var f_acta = (data.fecha_acta == null || data.fecha_acta == "0000-00-00") ? '' : convertirDateTime(data.fecha_acta);
					var f_domicilio = (data.fecha_domicilio == null || data.fecha_domicilio == "0000-00-00") ? '' : convertirDate(data.fecha_domicilio);
					var f_curp = (data.emision_curp == null || data.emision_curp == "0000-00-00") ? '' : convertirDate(data.emision_curp);
					var f_imss = (data.emision_nss == null || data.emision_nss == "0000-00-00") ? '' : convertirDate(data.emision_nss);
					var f_retencion = (data.fecha_retencion_impuestos == null || data.fecha_retencion_impuestos == "0000-00-00") ? '' : convertirDate(data.fecha_retencion_impuestos);
					var f_rfc = (data.emision_rfc == null || data.emision_rfc == "0000-00-00") ? '' : convertirDate(data.emision_rfc);
					var f_licencia = (data.fecha_licencia == null || data.fecha_licencia == "0000-00-00") ? '' : convertirDateTime(data.fecha_licencia);
					var f_migra = (data.vigencia_migratoria == null || data.vigencia_migratoria == "0000-00-00") ? '' : convertirDate(data.vigencia_migratoria);
					var f_visa = (data.fecha_visa == null || data.fecha_visa == "0000-00-00") ? '' : convertirDate(data.fecha_visa);
					$("#fecha_acta").val(f_acta);
					$("#numero_acta").val(data.acta);
					$("#fecha_domicilio").val(f_domicilio);
					$("#numero_domicilio").val(data.cuenta_domicilio);
					$("#fecha_ine").val(data.emision_ine);
					$("#numero_ine").val(data.ine);
					$("#fecha_curp").val(f_curp);
					$("#numero_curp").val(data.curp);
					$("#fecha_imss").val(f_imss);
					$("#numero_imss").val(data.nss);
					$("#fecha_retencion").val(f_retencion);
					$("#numero_retencion").val(data.retencion_impuestos);
					$("#fecha_rfc").val(f_rfc);
					$("#numero_rfc").val(data.rfc);
					$("#fecha_licencia").val(f_licencia);
					$("#numero_licencia").val(data.licencia);
					$("#fecha_migra").val(f_migra);
					$("#numero_migra").val(data.numero_migratorio);
					$("#fecha_visa").val(f_visa);
					$("#numero_visa").val(data.visa);
					$("#visitaDocumentosModal").modal('show');
				});
				$('a#visita_familiares', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#div_familiares").empty();
					$.ajax({
						url: '<?php echo base_url('Candidato/getGrupoFamiliar'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						dataType: "text",
						success: function(res) {
							if (res != 0) {
								$("#div_familiares").empty();
								$("#div_familiares").append(res)

							} else {
								$("#div_familiares").empty();
								$("#div_familiares").append('<p class="text-center">Sin registros</p>');
							}
						}
					});
					$("#familiaresModal").modal('show');
				});
				$('a#visita_egresos', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_egresos")[0].reset();
					$.ajax({
						url: '<?php echo base_url('Candidato/getEgresos'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						dataType: "text",
						success: function(res) {
							if (res != 0) {
								var dato = res.split('@@');
								$("#renta").val(dato[0]);
								$("#alimentos").val(dato[1]);
								$("#servicios").val(dato[2]);
								$("#transportes").val(dato[3]);
								$("#otros_gastos").val(dato[4]);
								$("#solvencia").val(dato[5]);
							}
						}
					});
					$("#egresosModal").modal('show');
				});
				$('a#visita_habitacion', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_habitacion")[0].reset();

					$("#tiempo_residencia").val(data.tiempo_residencia);
					$("#nivel_zona").val(data.id_tipo_nivel_zona);
					$("#tipo_vivienda").val(data.id_tipo_vivienda);
					$("#recamaras").val(data.recamaras);
					$("#banios").val(data.banios);
					$("#distribucion").val(data.distribucion);
					$("#calidad_mobiliario").val(data.calidad_mobiliario);
					$("#mobiliario").val(data.mobiliario);
					$("#tamanio_vivienda").val(data.tamanio_vivienda);
					$("#condiciones_vivienda").val(data.id_tipo_condiciones);
					$("#visitaHabitacionModal").modal('show');
				});
				$('a#visita_vecinales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_refVecinal1")[0].reset();
					$("#d_refVecinal2")[0].reset();
					$("#d_refVecinal3")[0].reset();
					$.ajax({
						url: '<?php echo base_url('Candidato/getVecinales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						dataType: "text",
						success: function(res) {
							if (res != 0) {
								var rows = res.split('###');
								for (var i = 0; i < rows.length; i++) {
									if (rows[i] != "") {
										var dato = rows[i].split('@@');
										$("#vecino" + (i + 1) + "_nombre").val(dato[0]);
										$("#vecino" + (i + 1) + "_tel").val(dato[1]);
										$("#vecino" + (i + 1) + "_domicilio").val(dato[2]);
										$("#vecino" + (i + 1) + "_concepto").val(dato[3]);
										$("#vecino" + (i + 1) + "_familia").val(dato[4]);
										$("#vecino" + (i + 1) + "_civil").val(dato[5]);
										$("#vecino" + (i + 1) + "_hijos").val(dato[6]);
										$("#vecino" + (i + 1) + "_sabetrabaja").val(dato[7]);
										$("#vecino" + (i + 1) + "_notas").val(dato[8]);
										$("#idrefvec" + (i + 1)).val(dato[9]);
									}
								}
							}
						}
					});
					$("#refVecinalesModal").modal('show');
				});
				$('a#conclusiones', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#idFinalizado").val(data.idFinalizado);
					$("#div_incompleto").css('display', 'none');
					$("#div_completo").css('display', 'block');
					$("#btnTerminar").css('display', 'initial');
					$("#personal1").val(data.descripcion_personal1);
					$("#personal2").val(data.descripcion_personal2);
					$("#laboral1").val(data.descripcion_laboral1);
					$("#laboral2").val(data.descripcion_laboral2);
					$("#socio1").val(data.descripcion_socio1);
					$("#socio2").val(data.descripcion_socio2);
					$("#recomendable").val(data.recomendable);
					$("#completarModal").modal('show');
				});
				$('a#verificacion_penales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					var f = new Date();
					var dia = f.getDate();
					var mes = (f.getMonth() + 1);
					var dia = (dia < 10) ? '0' + dia : dia;
					var mes = (mes < 10) ? '0' + mes : mes;
					$("#fecha_estatus_penales").text(dia + "/" + mes + "/" + f.getFullYear());
					$.ajax({
						url: '<?php echo base_url('Candidato/checkEstatusPenales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != 0) {
								var aux = res.split('@@');
								var finalizado = aux[2];
								if (finalizado == 1) {
									$("#div_estatus_penales").css('display', 'none');
									$("#btnTerminarVerificacionPenales").css('display', 'none');
								}
								$("#div_crearEstatusPenales").empty();
								$("#idVerificacionPenales").val(aux[1]);
								$("#div_crearEstatusPenales").append(aux[0]);
							} else {
								$("#div_crearEstatusPenales").empty();
								$("#div_crearEstatusPenales").append('<p class="text-center">Sin registros </p>');
								$("#div_estatus_penales").css('display', 'block');
								$("#btnTerminarVerificacionPenales").css('display', 'initial');
								$("#idVerificacionPenales").val(0);
							}

						}
					});
					$("#verificacionPenalesModal").modal("show");
				});
				$('a#conclusiones_ingles', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#idFinalizado").val(data.idFinalizado);
					if (data.id_tipo_proceso == 3) {
						$("#check_identidad").prop('disabled', true);
						$("#check_identidad").val(data.identidad_check);
						$("#check_penales").prop('disabled', false);
						$("#check_penales").val(data.penales_check);
						$("#check_estudios").prop('disabled', false);
						$("#check_estudios").val(data.estudios_check);
						$("#check_laboral").prop('disabled', false);
						$("#check_laboral").val(data.empleo_check);
						$("#check_global").val(data.global_searches_check);
						$("#check_ofac").val(data.ofac_check);
						$("#comentario_final").val(data.comentario_final);
						$("#bgc_status").val(data.status_bgc);
						$("#finalizarInglesModal").modal('show');

					}
					if (data.id_tipo_proceso == 4) {
						$("#check_penales").prop('disabled', false);
						$("#check_penales").val(data.penales_check);
						$("#check_identidad").prop('disabled', false);
						$("#check_identidad").val(data.identidad_check);
						$("#check_estudios").prop('disabled', false);
						$("#check_estudios").val(data.estudios_check);
						$("#check_laboral").prop('disabled', false);
						$("#check_laboral").val(data.empleo_check);
						$("#check_global").val(data.global_searches_check);
						$("#check_ofac").val(data.ofac_check);
						$("#comentario_final").val(data.comentario_final);
						$("#bgc_status").val(data.status_bgc);
						$("#finalizarInglesModal").modal('show');

					}
					if (data.id_tipo_proceso == 5) {
						$("#check_identidad").prop('disabled', false);
						$("#check_identidad").val(data.identidad_check);
						$("#check_penales").prop('disabled', true);
						$("#check_penales").val(data.penales_check);
						$("#check_estudios").prop('disabled', true);
						$("#check_estudios").val(data.estudios_check);
						$("#check_laboral").prop('disabled', true);
						$("#check_laboral").val(data.empleo_check);
						$("#check_global").val(data.global_searches_check);
						$("#check_ofac").val(data.ofac_check);
						$("#comentario_final").val(data.comentario_final);
						$("#bgc_status").val(data.status_bgc);
						$("#finalizarInglesModal").modal('show');
					}
				});
				$('a#solicitudes', row).bind('click', () => {
					$.ajax({
						url: '<?php echo base_url('Candidato/viewSolicitudesCandidato'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != "") {
								$("#titulo_accion").text("Requisición del candidato");
								$("#nombre_candidato").html("<b>" + data.nombre + ' ' + data.paterno + ' ' + data.materno + '</b>');
								$("#motivo").html(res);
								$("#verModal").modal('show');
							} else {
								$("#titulo_accion").text("Requisiciones del candidato");
								$("#motivo").html("No hay registro de requisición");
								$("#verModal").modal('show');
							}


						},
						error: function(res) {

						}
					});
				});
				$("a#subirDocs", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".prefijo").val(data.id + "_" + data.nombre + "" + data.paterno);
					$.ajax({
						url: '<?php echo base_url('Candidato/getDocs'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id,
							'prefijo': data.id + "_" + data.nombre + "" + data.paterno
						},
						success: function(res) {
							$("#tablaDocs").html(res);

						},
						error: function(res) {

						}
					});
					$("#docsModal").modal("show");
				});
				$("a#msj_avances", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$("#avances_nombrecandidato").text(data.nombre + " " + data.paterno + " " + data.materno);
					estatusAvances();
				});
				$("a#final", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					var refs_comentarios = $.ajax({
						url: '<?php echo base_url('Candidato/getComentariosRefPersonales'); ?>',
						type: 'post',
						async: false,
						data: {
							'id_candidato': data.id
						},
						success: function(res) {}
					}).responseText;
					var trabajos = $.ajax({
						url: '<?php echo base_url('Candidato/countAntecedentesLaborales'); ?>',
						type: 'post',
						async: false,
						data: {
							'id_candidato': data.id
						},
						success: function(res) {}
					}).responseText;
					var bebidas = (data.bebidas == 1) ? "ingerir" : "no ingerir";
					var fuma = (data.fumar == 1) ? "Fuma " + data.fumar_frecuencia + "." : "No fuma.";
					switch (data.calidad_mobiliario) {
						case '1':
							var calidad = "Buena";
							break;
						case '2':
							var calidad = "Regular";
							break;
						case '3':
							var calidad = "Mala";
							break;
					}
					switch (data.tamanio_vivienda) {
						case '1':
							var tamano = "Amplia";
							break;
						case '2':
							var tamano = "Suficiente";
							break;
						case '3':
							var tamano = "Reducidad";
							break;
					}
					var vecinales = $.ajax({
						url: '<?php echo base_url('Candidato/getComentariosRefVecinales'); ?>',
						type: 'post',
						async: false,
						data: {
							'id_candidato': data.id
						},
						success: function(res) {}
					}).responseText;
					var adeudo = (data.adeudo_muebles == 1) ? "con adeudo" : "sin adeudo";
					//checkCandidato(data.candidato,data.edad,data.lugar_nacimiento,data.grado,data.religion,data.corto_plazo,data.mediano_plazo,bebidas,data.bebidas_frecuencia,fuma,refs_comentarios,trabajos,data.vivienda,data.tiempo_residencia,data.zona,calidad,tamano,data.condiciones,vecinales,data.muebles, adeudo);
					$("#div_incompleto").css('display', 'none');
					$("#div_completo").css('display', 'block');
					$("#btnTerminar").css('display', 'initial');
					$("#personal1").val(data.candidato + ", de " + data.edad + " años, originario(a) de " + data.lugar_nacimiento + ", cuenta con un grado máximo de estudios de " + data.grado + ". Refiere ser " + data.religion + ", y sus planes a corto plazo son " + data.corto_plazo + " y a mediano plazo " + data.mediano_plazo);
					$("#personal2").val("Refiere " + bebidas + " bebidas alcohólicas " + data.bebidas_frecuencia + ". " + fuma + " Sus referencias personales lo describen como " + refs_comentarios + ".");
					$("#laboral1").val("Señaló " + trabajos + " referencias laborales");
					$("#laboral2").val(" cuyo(a) propietario(a)/dueño(a) es quien nos valida referencia laboral.");
					$("#socio1").val(data.candidato + ", actualmente vive en un/una " + data.vivienda + ", con un tiempo de residencia de " + data.tiempo_residencia + ". El nivel de la zona es " + data.zona + ", el mobiliario es de calidad " + calidad + ", la vivienda es " + tamano + " y en condiciones " + data.condiciones);
					$("#socio2").val("Los gastos generados en el hogar son solventados por _____. Sus referencias vecinales describen que es " + vecinales + ". El candidato cuenta con " + data.muebles + " " + adeudo);
					$("#revisionModal").modal('show');

				});
				$("a#final_ingles", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					if (data.id_tipo_proceso == 3) {
						$("#check_identidad").prop('disabled', true);
						$("#check_identidad").val(3);
						$("#check_penales").prop('disabled', false);
						$("#check_penales").val('');
						$("#check_estudios").prop('disabled', false);
						$("#check_estudios").val('');
						$("#check_laboral").prop('disabled', false);
						$("#check_laboral").val('');
					}
					if (data.id_tipo_proceso == 4) {
						$("#check_penales").prop('disabled', false);
						$("#check_penales").val('');
						$("#check_identidad").prop('disabled', false);
						$("#check_identidad").val('');
						$("#check_estudios").prop('disabled', false);
						$("#check_estudios").val('');
						$("#check_laboral").prop('disabled', false);
						$("#check_laboral").val('');
					}
					if (data.id_tipo_proceso == 5) {
						$("#check_identidad").prop('disabled', false);
						$("#check_identidad").val('');
						$("#check_penales").prop('disabled', true);
						$("#check_penales").val(3);
						$("#check_estudios").prop('disabled', true);
						$("#check_estudios").val(3);
						$("#check_laboral").prop('disabled', true);
						$("#check_laboral").val(3);
					}
					$("#revision2Modal").modal('show');
					//$("#finalizarInglesModal").modal('show');
				});
				$('a[id^=pdfFinal]', row).bind('click', () => {
					var id = data.id;
					$('#pdf' + id).submit();
				});
				$('a[id^=pdfDoping]', row).bind('click', () => {
					var id = data.idDoping;
					$('#pdfForm' + id).submit();
				});
				$('a[id^=pdfMedico]', row).bind('click', () => {
					var id = data.idMedico;
					$('#med' + id).submit();
				});
				$("a#eliminar", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$("#idCliente").val(data.id_cliente);
					$("#titulo_accion").text("Cancelar proceso");
					$("#texto_confirmacion").html("¿Estás seguro de cancelar el proceso de <b>" + data.nombre + "</b>?");
					$("#div_motivo").css('display', 'block');
					$("#quitarModal").modal("show");
				});
				$('a#verCancelado', row).bind('click', () => {
					$.ajax({
						url: '<?php echo base_url('Candidato/getCancelacion'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							datos = res.split('##');
							var fecha = new Date(datos[0]);
							var options = {
								year: 'numeric',
								month: 'long',
								day: 'numeric'
							};
							$("#titulo_accion").text("Cancelado");
							$("#nombre_candidato").html("El candidato <b>" + data.nombre + ' ' + data.paterno + ' ' + data.materno + '</b> fue cancelado el ' + fecha.toLocaleDateString("es-ES", options));
							$("#motivo").html("Comentario: <br>" + datos[1]);
							$("#verModal").modal('show');

						},
						error: function(res) {

						}
					});
				});
				$('a#verEliminado', row).bind('click', () => {
					$.ajax({
						url: '<?php echo base_url('Candidato/getEliminacion'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							datos = res.split('##');
							var fecha = new Date(datos[0]);
							var options = {
								year: 'numeric',
								month: 'long',
								day: 'numeric'
							};
							$("#titulo_accion").text("Eliminado");
							$("#nombre_candidato").html("El candidato <b>" + data.nombre + ' ' + data.paterno + ' ' + data.materno + '</b> fue eliminado el ' + fecha.toLocaleDateString("es-ES", options));
							$("#motivo").html("Comentario: <br>" + datos[1]);
							$("#verModal").modal('show');

						},
						error: function(res) {

						}
					});
				});
				$('a#comentario', row).bind('click', () => {
					$.ajax({
						url: '<?php echo base_url('Candidato/viewComentario'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != 0) {
								$("#titulo_accion").text("Comentario del candidato");
								//$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue cancelado el '+fecha.toLocaleDateString("es-ES", options));
								$("#motivo").html(res);
								$("#verModal").modal('show');
							} else {
								$("#titulo_accion").text("Comentarios del candidato");
								$("#motivo").html("No hay registro de comentarios");
								$("#verModal").modal('show');
							}


						},
						error: function(res) {

						}
					});
				});
				$("a#psicometria", row).bind('click', () => {
					$("#idPsicometrico").val(data.idPsicometrico);
					$("#idCandidato").val(data.id);
					$("#psicometria_candidato").html("Candidato <b>" + data.candidato + "</b>");
					$("#psicometriaModal").modal("show");
				});
			},
			"language": {
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"zeroRecords": "No se encontraron registros",
				"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"infoEmpty": "Sin registros disponibles",
				"infoFiltered": "(Filtrado _MAX_ registros totales)",
				"sSearch": "Buscar:",
				"oPaginate": {
					"sLast": "Última página",
					"sFirst": "Primera",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				}
			}
		});
		//$('#tabla').DataTable().search(" ");
		$("#btn_eliminados").click(function() {
			var id_cliente = '<?php echo $this->uri->segment(3) ?>';
			$.ajax({
				url: '<?php echo base_url('Candidato/getCandidatosEliminados'); ?>',
				type: 'POST',
				data: {
					'id_cliente': id_cliente
				},
				success: function(res) {
					$("#div_listado").html(res);
					$("#eliminadosModal").modal("show");
				}
			});
		});
		
		$("#guardarGenerales2").click(function() {
			var d_generales = $("#d_generales2").serialize();
			d_generales += "&id_candidato=" + $("#idCandidato").val();
			var totalPer = $('.personal2_obligado').filter(function() {
				return !$(this).val();
			}).length;
			if (totalPer > 0) {
				$(".personal2_obligado").each(function() {
					var element = $(this);
					if (element.val() == "") {
						element.addClass("requerido");
						$("#txt_vacios").text(" Hay campos obligatorios vacíos");
						$('#vacios').css("display", "block");
						setTimeout(function() {
							$('#vacios').fadeOut();
						}, 4000);
					} else {
						element.removeClass("requerido");
					}
				});
			} else {
				formdata = $('#d_generales2').serialize();
				var idCandidato = $("#idCandidato").val();
				var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'datos_generales-' + idCandidato + '-' + fecha_txt);
				$.ajax({
					url: '<?php echo base_url('Candidato/actualizarDatosGeneralesIngles'); ?>',
					method: "POST",
					data: {
						'd_generales': d_generales
					},
					beforeSend: function() {
						$('.loader').css("display", "block");
					},
					success: function(res) {
						if (res == 1) {
							setTimeout(function() {
								$('.loader').fadeOut();
							}, 200);
							$("#generales2Modal").modal('hide');
							localStorage.setItem("success", 1);
							location.reload();
						}
					}
				});
			}
		});
		$("#guardarDocumentacion").click(function(e) {
			e.preventDefault();
			var d_documentos = $("#d_documentos").serialize();
			d_documentos += "&id_candidato=" + $("#idCandidato").val();
			var totalPer = $('.docs_obligado').filter(function() {
				return !$(this).val();
			}).length;
			if (totalPer > 0) {
				$(".docs_obligado").each(function() {
					var element = $(this);
					if (element.val() == "") {
						element.addClass("requerido");
						$("#txt_vacios").text(" Hay campos obligatorios vacíos");
						$('#vacios').css("display", "block");
						setTimeout(function() {
							$('#vacios').fadeOut();
						}, 4000);
					} else {
						element.removeClass("requerido");
					}
				});
			} else {
				formdata = $('#d_documentos').serialize();
				var idCandidato = $("#idCandidato").val();
				var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'visita_documentacion-' + idCandidato + '-' + fecha_txt);
				$.ajax({
					url: '<?php echo base_url('Candidato/actualizarDocumentacionVisita'); ?>',
					method: "POST",
					data: {
						'd_documentos': d_documentos
					},
					beforeSend: function() {
						$('.loader').css("display", "block");
					},
					success: function(res) {
						if (res == 1) {
							setTimeout(function() {
								$('.loader').fadeOut();
							}, 200);
							$("#visitaDocumentosModal").modal('hide');
							localStorage.setItem("success", 1);
							location.reload();
						}
					}
				});
			}
		});
		
		$("#actualizarMayoresEstudios").click(function(e) {
			e.preventDefault();
			var d_estudios = $("#d_mayor_estudios").serialize();
			d_estudios += "&id_candidato=" + $("#idCandidato").val();
			d_estudios += "&id_ver_estudios=" + $("#idMayoresEstudios").val();
			var total = $('.mayor_obligado').filter(function() {
				return !$(this).val();
			}).length;
			if (total > 0) {
				$(".mayor_obligado").each(function() {
					var element = $(this);
					if (element.val() == "") {
						element.addClass("requerido");
						$("#txt_vacios").text(" Hay campos obligatorios vacíos");
						$('#vacios').css("display", "block");
						setTimeout(function() {
							$('#vacios').fadeOut();
						}, 3000);
					} else {
						element.removeClass("requerido");
					}
				});
			} else {
				formdata = $('#d_mayor_estudios').serialize();
				var idCandidato = $("#idCandidato").val();
				var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'mayores_estudios-' + idCandidato + '-' + fecha_txt);
				$.ajax({
					url: '<?php echo base_url('Candidato/actualizarMayoresEstudios'); ?>',
					method: "POST",
					data: {
						'd_estudios': d_estudios
					},
					beforeSend: function() {
						$('.loader').css("display", "block");
					},
					success: function(res) {
						setTimeout(function() {
							$('.loader').fadeOut();
						}, 200);
						$("#mayoresEstudiosModal").modal('hide');
						localStorage.setItem("success", 1);
						location.reload();
					}
				});
			}
		});
		$("#guardarSociales").click(function() {
			var d_sociales = $("#d_sociales").serialize();
			d_sociales += "&id_candidato=" + $("#idCandidato").val();
			var totalPer = $('.social_obligado').filter(function() {
				return !$(this).val();
			}).length;
			if (totalPer > 0) {
				$(".social_obligado").each(function() {
					var element = $(this);
					if (element.val() == "") {
						element.addClass("requerido");
						$("#txt_vacios").text(" Hay campos obligatorios vacíos");
						$('#vacios').css("display", "block");
						setTimeout(function() {
							$('#vacios').fadeOut();
						}, 4000);
					} else {
						element.removeClass("requerido");
					}
				});
			} else {
				formdata = $('#d_sociales').serialize();
				var idCandidato = $("#idCandidato").val();
				var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'antecedentes_sociales-' + idCandidato + '-' + fecha_txt);
				$.ajax({
					url: '<?php echo base_url('Candidato/actualizarAntecendentesSociales'); ?>',
					method: "POST",
					data: {
						'd_sociales': d_sociales
					},
					beforeSend: function() {
						$('.loader').css("display", "block");
					},
					success: function(res) {
						if (res == 1) {
							setTimeout(function() {
								$('.loader').fadeOut();
							}, 200);
							$("#socialesModal").modal('hide');
							localStorage.setItem("success", 1);
							location.reload();
						}
					}
				});
			}
		});
		$("#guardarGlobalesGeneral").click(function() {
			var d_global = $("#d_globales_general").serialize();
			d_global += "&id_candidato=" + $("#idCandidato").val();
			var totalPer = $('.global_general_obligado').filter(function() {
				return !$(this).val();
			}).length;
			if (totalPer > 0) {
				$(".global_general_obligado").each(function() {
					var element = $(this);
					if (element.val() == "") {
						element.addClass("requerido");
						$("#txt_vacios").text(" Hay campos obligatorios vacíos");
						$('#vacios').css("display", "block");
						setTimeout(function() {
							$('#vacios').fadeOut();
						}, 4000);
					} else {
						element.removeClass("requerido");
					}
				});
			} else {
				formdata = $('#d_globales_general').serialize();
				var idCandidato = $("#idCandidato").val();
				var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'global_data_searches-' + idCandidato + '-' + fecha_txt);
				$.ajax({
					url: '<?php echo base_url('Candidato/updateGlobalSearches'); ?>',
					method: "POST",
					data: {
						'd_global': d_global
					},
					success: function(res) {
						if (res == 1) {
							setTimeout(function() {
								$('.loader').fadeOut();
							}, 200);
							$("#globalesGeneralModal").modal('hide');
							localStorage.setItem("success", 1);
							location.reload();
						}
					}
				});
			}
		});
		$("#guardarVerificacionIdentidad").click(function() {
			var d_identidad = $("#d_identidad").serialize();
			d_identidad += "&id_candidato=" + $("#idCandidato").val();
			var totalPer = $('.identidad_obligado').filter(function() {
				return !$(this).val();
			}).length;
			if (totalPer > 0) {
				$(".identidad_obligado").each(function() {
					var element = $(this);
					if (element.val() == "") {
						element.addClass("requerido");
						$("#txt_vacios").text(" Hay campos obligatorios vacíos");
						$('#vacios').css("display", "block");
						setTimeout(function() {
							$('#vacios').fadeOut();
						}, 4000);
					} else {
						element.removeClass("requerido");
					}
				});
			} else {
				formdata = $('#d_identidad').serialize();
				var idCandidato = $("#idCandidato").val();
				var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'verificacion_identidad-' + idCandidato + '-' + fecha_txt);
				$.ajax({
					url: '<?php echo base_url('Candidato/verificacionIdentidadCandidato'); ?>',
					method: "POST",
					data: {
						'd_identidad': d_identidad
					},
					beforeSend: function() {
						$('.loader').css("display", "block");
					},
					success: function(res) {
						if (res == 1) {
							setTimeout(function() {
								$('.loader').fadeOut();
							}, 200);
							$("#identidadModal").modal('hide');
							localStorage.setItem("success", 1);
							location.reload();
						}
					}
				});
			}
		});
		$("#subcliente").change(function() {
			var subcliente = $(this).val();
			if (subcliente != "") {
				if (subcliente == 0) {
					$('#proceso').prop('disabled', false);
					$('#proceso').empty();
					$('#proceso').append($("<option selected></option>").attr("value", 1).text("ESE Español"));
					$('#proceso').append($("<option></option>").attr("value", 5).text("ESE Simple"));
					$('#antidoping').val('');
					$("#examen").prop('disabled',true);
					$('#examen').val('');
				}
				if (subcliente == 180) {
					$('#proceso').prop('disabled', false);
					$('#proceso').empty();
					$('#proceso').append($("<option selected></option>").attr("value", "").text("Selecciona"));
					$('#proceso').append($("<option></option>").attr("value", 3).text("ESE General"));
					$('#proceso').append($("<option></option>").attr("value", 4).text("ESE General Plus"));
					$('#proceso').append($("<option></option>").attr("value", 5).text("ESE Simple"));
				}
			} 
			if (subcliente == "") {
				$('#proceso').empty();
				$('#proceso').append($("<option selected></option>").attr("value", "").text("Selecciona"));
				$('#antidoping').val('');
				$('#examen').val('');
				$('#proceso').prop('disabled', true);
				$("#examen").prop('disabled',true);
			}
		});
		$('#antidoping').change(function(){
			var opcion = $(this).val();
			var id_subcliente = $("#subcliente").val();
			var id_cliente = '<?php echo $this->uri->segment(3) ?>';
			subcliente = (id_subcliente == '')? 0 : id_subcliente;
			if(opcion == 1){
				$("#examen").prop('disabled',false);
				$.ajax({
					url: '<?php echo base_url('Doping/getPaqueteSubcliente'); ?>',
					method: 'POST',
					data: {
						'id_subcliente': subcliente,
						'id_cliente': id_cliente,
						'id_proyecto': 0
					},
					success: function(res) {
						if (res != "") {
							$('#examen').val(res);
							$("#examen").prop('disabled', false);
							$("#examen").addClass('obligado');
						} else {
							$('#examen').val('');
							$("#examen").prop('disabled', false);
							$("#examen").addClass('obligado');
						}
					}
				});
			}
			else{
				$("#examen").val('');
				$("#examen").prop('disabled',true);
			}
		})
		$("#estado").change(function() {
			var id_estado = $(this).val();
			if (id_estado != "") {
				$.ajax({
					url: '<?php echo base_url('Funciones/getMunicipios'); ?>',
					method: 'POST',
					data: {
						'id_estado': id_estado
					},
					dataType: "text",
					success: function(res) {
						$('#municipio').prop('disabled', false);
						$('#municipio').html(res);
					}
				});
			} else {
				$('#municipio').prop('disabled', true);
				$('#municipio').append($("<option selected></option>").attr("value", "").text("Selecciona"));
			}
		});
		$("#estado_ingles").change(function() {
			var id_estado = $(this).val();
			if (id_estado != "") {
				$.ajax({
					url: '<?php echo base_url('Funciones/getMunicipios'); ?>',
					method: 'POST',
					data: {
						'id_estado': id_estado
					},
					dataType: "text",
					success: function(res) {
						$('#municipio_ingles').prop('disabled', false);
						$('#municipio_ingles').html(res);
					}
				});
			} else {
				$('#municipio_ingles').prop('disabled', true);
				$('#municipio_ingles').append($("<option selected></option>").attr("value", "").text("Selecciona"));
			}
		});
		$("#aplicar_todo").change(function() {
			var valor = $(this).val();
			switch (valor) {
				case "-1":
					$(".performance").val("No proporciona");
					break;
				case "0":
					$(".performance").val("No proporciona");
					break;
				case "1":
					$(".performance").val("Excelente");
					break;
				case "2":
					$(".performance").val("Bueno");
					break;
				case "3":
					$(".performance").val("Regular");
					break;
				case "4":
					$(".performance").val("Insuficiente");
					break;
				case "5":
					$(".performance").val("Muy mal");
					break;
			}
		});
		$("#aplicar_todo2").change(function() {
			var valor = $(this).val();
			switch (valor) {
				case "-1":
					$(".performance2").val("No proporciona");
					break;
				case "0":
					$(".performance2").val("No proporciona");
					break;
				case "1":
					$(".performance2").val("Excelente");
					break;
				case "2":
					$(".performance2").val("Bueno");
					break;
				case "3":
					$(".performance2").val("Regular");
					break;
				case "4":
					$(".performance2").val("Insuficiente");
					break;
				case "5":
					$(".performance2").val("Muy mal");
					break;
			}
		});
		$("#aplicar_todo3").change(function() {
			var valor = $(this).val();
			switch (valor) {
				case "-1":
					$(".performance3").val("No proporciona");
					break;
				case "0":
					$(".performance3").val("No proporciona");
					break;
				case "1":
					$(".performance3").val("Excelente");
					break;
				case "2":
					$(".performance3").val("Bueno");
					break;
				case "3":
					$(".performance3").val("Regular");
					break;
				case "4":
					$(".performance3").val("Insuficiente");
					break;
				case "5":
					$(".performance3").val("Muy mal");
					break;
			}
		});
		$("#aplicar_todo4").change(function() {
			var valor = $(this).val();
			switch (valor) {
				case "-1":
					$(".performance4").val("No proporciona");
					break;
				case "0":
					$(".performance4").val("No proporciona");
					break;
				case "1":
					$(".performance4").val("Excelente");
					break;
				case "2":
					$(".performance4").val("Bueno");
					break;
				case "3":
					$(".performance4").val("Regular");
					break;
				case "4":
					$(".performance4").val("Insuficiente");
					break;
				case "5":
					$(".performance4").val("Muy mal");
					break;
			}
		});
		$("#completarModal").on("hidden.bs.modal", function() {
			$("textarea, select").val("");
			$("select, textarea").removeClass("requerido");
			$("#campos_vacios_check").css('display', 'none');
		});
		$("#psicometriaModal").on("hidden.bs.modal", function() {
			$("input").val("");
			$("input").removeClass("requerido");
			$("#psicometriaModal #campos_vacios").css('display', 'none');
		});
		$("#refLab1_entrada, #refLab1_salida, #refLab2_entrada, #refLab2_salida").datetimepicker({
			minView: 2,
			format: "dd/mm/yyyy",
			startView: 4,
			autoclose: true,
			todayHighlight: true,
			pickerPosition: "bottom-left",
			forceParse: false
		});
		$("#visitaModal").on("hidden.bs.modal", function() {
			$("input, select").val("");
		});
		$('[data-toggle="tooltip"]').tooltip();
		
	});

	function guardarGenerales() {
		var datos = new FormData();
		datos.append('nombre', $("#nombre_general").val());
		datos.append('paterno', $("#paterno_general").val());
		datos.append('materno', $("#materno_general").val());
		datos.append('fecha_nacimiento', $("#fecha_nacimiento").val());
		datos.append('lugar', $("#lugar").val());
		datos.append('puesto', $("#puesto_general").val());
		datos.append('genero', $("#genero").val());
		datos.append('calle', $("#calle").val());
		datos.append('exterior', $("#exterior").val());
		datos.append('interior', $("#interior").val());
		datos.append('entre_calles', $("#calles").val());
		datos.append('colonia', $("#colonia").val());
		datos.append('estado', $("#estado").val());
		datos.append('municipio', $("#municipio").val());
		datos.append('cp', $("#cp").val());
		datos.append('civil', $("#civil").val());
		datos.append('celular', $("#celular_general").val());
		datos.append('tel_casa', $("#tel_casa").val());
		datos.append('grado_estudios', $("#grado").val());
		datos.append('correo', $("#correo_general").val());
		datos.append('id_candidato', $("#idCandidato").val());
		//Respaldo txt
		formdata = $('#d_generales').serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'datos_generales-' + idCandidato + '-' + fecha_txt);
		$.ajax({
			url: '<?php echo base_url('Cliente_General/guardarDatosGenerales'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#generalesModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#generalesModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function guardarEstudios() {
		var datos = new FormData();
		datos.append('prim_periodo', $("#prim_periodo").val());
		datos.append('prim_escuela', $("#prim_escuela").val());
		datos.append('prim_ciudad', $("#prim_ciudad").val());
		datos.append('prim_certificado', $("#prim_certificado").val());
		datos.append('prim_promedio', $("#prim_promedio").val());
		datos.append('sec_periodo', $("#sec_periodo").val());
		datos.append('sec_escuela', $("#sec_escuela").val());
		datos.append('sec_ciudad', $("#sec_ciudad").val());
		datos.append('sec_certificado', $("#sec_certificado").val());
		datos.append('sec_promedio', $("#sec_promedio").val());
		datos.append('comercial_periodo', $("#comercial_periodo").val());
		datos.append('comercial_escuela', $("#comercial_escuela").val());
		datos.append('comercial_ciudad', $("#comercial_ciudad").val());
		datos.append('comercial_certificado', $("#comercial_certificado").val());
		datos.append('comercial_promedio', $("#comercial_promedio").val());
		datos.append('prep_periodo', $("#prep_periodo").val());
		datos.append('prep_escuela', $("#prep_escuela").val());
		datos.append('prep_ciudad', $("#prep_ciudad").val());
		datos.append('prep_certificado', $("#prep_certificado").val());
		datos.append('prep_promedio', $("#prep_promedio").val());
		datos.append('lic_periodo', $("#lic_periodo").val());
		datos.append('lic_escuela', $("#lic_escuela").val());
		datos.append('lic_ciudad', $("#lic_ciudad").val());
		datos.append('lic_certificado', $("#lic_certificado").val());
		datos.append('lic_promedio', $("#lic_promedio").val());
		datos.append('actual_periodo', $("#actual_periodo").val());
		datos.append('actual_escuela', $("#actual_escuela").val());
		datos.append('actual_ciudad', $("#actual_ciudad").val());
		datos.append('actual_certificado', $("#actual_certificado").val());
		datos.append('actual_promedio', $("#actual_promedio").val());
		datos.append('cedula', $("#cedula").val());
		datos.append('otro_certificado', $("#otro_certificado").val());
		datos.append('carrera_inactivo', $("#carrera_inactivo").val());
		datos.append('estudios_comentarios', $("#estudios_comentarios").val());
		datos.append('id_candidato', $("#idCandidato").val());
		//Respaldo txt
		formdata = $('#d_estudios').serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'historial_academico-' + idCandidato + '-' + fecha_txt);
		$.ajax({
			url: '<?php echo base_url('Cliente_General/guardarHistorialAcademico'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#academicosModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#academicosModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function aceptarRevision() {
		$("#revisionModal").modal('hide');
		$("#completarModal").modal('show');
	}

	function aceptarRevision2() {
		$("#revision2Modal").modal('hide');
		$("#finalizarInglesModal").modal('show');
	}
	
	function registrar() {
		var id_cliente = '<?php echo $this->uri->segment(3) ?>';
		var correo = $('#correo').val();
		var proceso = $("#proceso").val();
		var datos = new FormData();
		datos.append('usuario', 1);
		datos.append('subcliente', $("#subcliente").val());
		datos.append('nombre', $("#nombre").val());
		datos.append('paterno', $("#paterno").val());
		datos.append('materno', $("#materno").val());
		datos.append('correo', $("#correo").val());
		datos.append('celular', $("#celular").val());
		datos.append('fijo', $("#fijo").val());
		datos.append('puesto', $("#puesto").val());
		datos.append('socio', $("#socio").val());
		datos.append('antidoping', $("#antidoping").val());
		datos.append('psicometrico', $("#psicometrico").val());
		datos.append('medico', $("#medico").val());
		datos.append('examen', $("#examen").val());
		datos.append('proceso', proceso);
		datos.append('otro', $("#otro_requisito").val());
		datos.append('id_cliente', id_cliente);
		//datos.append('cv', $("#cv")[0].files[0]);
		var num_files = document.getElementById('cv').files.length;
		if (num_files > 0) {
			datos.append("hay_cvs", 1);
			for (var x = 0; x < num_files; x++) {
				datos.append("cvs[]", document.getElementById('cv').files[x]);
			}
		} else {
			datos.append("hay_cvs", 0);
		}
		$.ajax({
			url: '<?php echo base_url('Cliente_General/registrar'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#newModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha guardado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#newModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function AsignarCandidato() {
		var id_candidato = $("#asignar_candidato").val();
		var id_usuario = $("#asignar_usuario").val();

		var totalVacios = $('.asignar_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (totalVacios > 0) {
			$(".asignar_obligado").each(function() {
				var element = $(this);
				if (element.val() == "" || element.val() == -1) {
					element.addClass("requerido");
					$("#asignarCandidatoModal #msj_texto").text('Hay campos obligatorios vacios');
					$("#asignarCandidatoModal #msj_error").css('display', 'block');
					setTimeout(function() {
						$("#asignarCandidatoModal #msj_error").fadeOut();
					}, 4000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			$.ajax({
				url: '<?php echo base_url('Candidato/reasignarCandidatoAnalista'); ?>',
				method: 'POST',
				data: {
					'id_candidato': id_candidato,
					'id_usuario': id_usuario
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 200);
					var data = JSON.parse(res);
					if (data.codigo === 1) {
						$("#asignarCandidatoModal").modal('hide')
						recargarTable()
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: 'Se ha actualizado correctamente',
							showConfirmButton: false,
							timer: 2500
						})
					} else {
						$("#asignarCandidatoModal #msj_error").css('display', 'block').html(data.msg);
					}
				}
			});
		}
	}
	function estatusAvances() {
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_avances").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkAvances'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					if (res != 0) {
						var aux = res.split('@@');
						var finalizado = aux[2];
						if (finalizado == 1) {
							$("#div_estatus_avances").css('display', 'none');
						}
						$("#div_crearEstatusAvances").empty();
						$("#idAvances").val(aux[1]);
						$("#div_crearEstatusAvances").append(aux[0]);
					} else {
						$("#idAvances").val(0);
						$("#div_crearEstatusAvances").empty();
						$("#div_crearEstatusAvances").html('<div class="col-12"><p class="text-center">Sin registros</p></div>');
					}
					$('.loader').fadeOut();
				}, 200);
			}
		});
		$("#avancesModal").modal("show");
	}
	function generarEstatusAvance() {
		var datos = new FormData();
		datos.append('id_candidato', $("#idCandidato").val());
		datos.append('id_avance', $("#idAvances").val());
		datos.append('comentario', $("#avances_estatus_comentario").val());
		datos.append('adjunto', $("#adjunto")[0].files[0]);
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusAvance'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$("#adjunto").val("");
					var aux = res.split('@@');
					$("#idAvances").val(aux[1]);
					$("#avances_estatus_comentario").val("");
					$("#div_crearEstatusAvances").empty();
					$("#div_crearEstatusAvances").append(aux[0]);
					$('.loader').fadeOut();
				}, 200);
			}
		});
	}

	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}

	function eliminarArchivo(fila, idDoc, archivo) {
		$("#fila" + fila).remove();
		$.ajax({
			url: '<?php echo base_url('Candidato/eliminarDocCandidato'); ?>',
			method: 'POST',
			data: {
				'idDoc': idDoc,
				'archivo': archivo
			},
			dataType: "text",
			success: function(res) {

			}
		});
	}

	

	function guardarPersona(id_persona, num_persona, idCandidato) {
		var nombre = $("#p" + num_persona + "_nombre").val();
		var empresa = $("#p" + num_persona + "_empresa").val();
		var puesto = $("#p" + num_persona + "_puesto").val();
		var antiguedad = $("#p" + num_persona + "_antiguedad").val();
		var muebles = $("#p" + num_persona + "_muebles").val();

		var totalPer = $('.p_obligado').filter(function() {
			return !$(this).val();
		}).length;
		if (totalPer > 0) {
			$(".p_obligado").each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos obligatorios vacíos");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 4000);

				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $("#d_familiar" + num_persona).serialize();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'grupo_familiar_' + num_persona + '-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/actualizarFamiliar'); ?>',
				method: "POST",
				data: {
					'id_persona': id_persona,
					'nombre': nombre,
					'empresa': empresa,
					'puesto': puesto,
					'antiguedad': antiguedad,
					'muebles': muebles
				},
				success: function(res) {
					if (res == 1) {
						$("#texto_msj").text('Los datos de la persona se han actualizado correctamente');
						$("#mensaje").css('display', 'block');
						setTimeout(function() {
							$('#mensaje').fadeOut();
						}, 6000);
					}
				}
			});
		}
	}

	function guardarPersonales(num) {
		var d_refPersonal = $("#d_refpersonal" + num).serialize();
		d_refPersonal += "&id_candidato=" + $("#idCandidato").val();
		d_refPersonal += "&id_refper=" + $("#refPer" + num + "_id").val();
		var totalPer = $('.refPer' + num + '_obligado').filter(function() {
			return !$(this).val();
		}).length;
		if (totalPer > 0) {
			$('.refPer' + num + '_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos vacíos en la sección de referencia personal #" + num + ".");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 4000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $("#d_refpersonal" + num).serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'referencia_personal_' + num + '-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				async: false,
				url: '<?php echo base_url('Candidato/actualizarReferenciaPersonal'); ?>',
				method: "POST",
				data: {
					'd_refPersonal': d_refPersonal,
					'num': num
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
						$("#refPer" + num + "_id").val(res);
						$("#texto_msj").text('La referencia personal #' + num + ' se ha guardado correctamente');
						$("#mensaje").css('display', 'block');
						setTimeout(function() {
							$('#mensaje').fadeOut();
						}, 6000);
					}, 300);
				}
			});
		}
	}

	function guardarVecinales(num) {
		var d_refVecinal = $("#d_refVecinal" + num).serialize();
		d_refVecinal += "&id_candidato=" + $("#idCandidato").val();
		d_refVecinal += "&id_refvec=" + $("#idrefvec" + num).val();

		var totalPer = $(".vec" + num + "_obligado").filter(function() {
			return !$(this).val();
		}).length;
		if (totalPer > 0) {
			$(".vec" + num + "_obligado").each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos obligatorios vacíos");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 4000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $("#d_refVecinal" + num).serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'visita_referencia_vecinal_' + num + '-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/actualizarReferenciasVecinales'); ?>',
				method: "POST",
				data: {
					'd_refVecinal': d_refVecinal,
					'num': num
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
						$("#idrefvec" + num).val(res);
						$("#texto_msj").text('La referencia vecinal #' + num + ' se ha guardado correctamente');
						$("#mensaje").css('display', 'block');
						setTimeout(function() {
							$('#mensaje').fadeOut();
						}, 6000);
					}, 300);
				}
			});
		}
	}

	function actualizarLaboral(num) {
		var reflaboral = $("#data_reflaboral" + num).serialize();
		reflaboral += "&id_candidato=" + $("#idCandidato").val();
		reflaboral += "&id_reflaboral=" + $("#idreflab" + num).val();
		var total = $('.reflab' + num + '_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (total > 0) {
			$('.reflab' + num + '_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos vacíos en la sección de la referencia laboral #" + num + ".");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 6000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $("#data_reflaboral" + num).serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'antecedente_laboral_' + num + '-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				async: false,
				url: '<?php echo base_url('Candidato/actualizarAntecedenteLaboral'); ?>',
				method: "POST",
				data: {
					'reflaboral': reflaboral,
					'num': num
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					if (res == 1) {
						setTimeout(function() {
							$('.loader').fadeOut();
						}, 300);
						$("#idreflab" + num).val(res);
						$("#texto_msj").text('Los datos de la referencia laboral #' + num + ' se han actualizado correctamente');
						$("#mensaje").css('display', 'block');
						setTimeout(function() {
							$('#mensaje').fadeOut();
						}, 6000);
					}
				}
			});
		}
	}

	function actualizarReferenciaLaboral(num) {
		var reflaboral = $("#candidato_reflaboral" + num).serialize();
		reflaboral += "&id_candidato=" + $("#idCandidato").val();
		reflaboral += "&idref=" + $("#idreflabingles" + num).val();
		var total = $('.reflabingles' + num + '_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (total > 0) {
			$('.reflabingles' + num + '_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos vacíos en la sección de la referencia laboral #" + num + ".");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 6000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $("#candidato_reflaboral" + num).serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'referencia_laboral_' + num + '-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				async: false,
				url: '<?php echo base_url('Candidato/actualizarReferenciaLaboralIngles'); ?>',
				method: "POST",
				data: {
					'reflaboral': reflaboral,
					'num': num
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 300);
					$("#idreflab" + num).val(res);
					$("#texto_msj").text('Los datos de la referencia laboral #' + num + ' se han actualizado correctamente');
					$("#mensaje").css('display', 'block');
					setTimeout(function() {
						$('#mensaje').fadeOut();
					}, 6000);
				}
			});
		}
	}

	function verificarLaboral(num) {
		var reflaboral = $("#analista_reflaboral" + num).serialize();
		reflaboral += "&id_candidato=" + $("#idCandidato").val();
		reflaboral += "&idver=" + $("#idverlab" + num).val();
		var total = $('.ver_reflab' + num + '_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (total > 0) {
			$('.ver_reflab' + num + '_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos vacíos en la sección de la verificación laboral #" + num + ".");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 6000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $("#analista_reflaboral" + num).serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'verificacion_laboral_' + num + '-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				async: false,
				url: '<?php echo base_url('Candidato/actualizarVerificacionLaboral'); ?>',
				method: "POST",
				data: {
					'reflaboral': reflaboral,
					'num': num
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 300);
					$("#idverlab" + num).val(res);
					$("#texto_msj").text('Los datos de la verificación laboral #' + num + ' se han actualizado correctamente');
					$("#mensaje").css('display', 'block');
					setTimeout(function() {
						$('#mensaje').fadeOut();
					}, 6000);
				}
			});
		}
	}

	function actualizarInvestigacion() {
		var inv = $("#d_investigacion").serialize();
		inv += "&id_candidato=" + $("#idCandidato").val();
		var total = $('.inv_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (total > 0) {
			$('.inv_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos obligatorios vacíos");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 6000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $('#d_investigacion').serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'investigacion_legal-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/actualizarInvestigacionLegal'); ?>',
				method: "POST",
				data: {
					'inv': inv
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					if (res == 1) {
						setTimeout(function() {
							$('.loader').fadeOut();
						}, 200);
						$("#legalesModal").modal('hide');
						localStorage.setItem("success", 1);
						location.reload();
					}
				}
			});
		}
	}

	function actualizarNoMencionados() {
		var nomen = $("#d_no_mencionados").serialize();
		nomen += "&id_candidato=" + $("#idCandidato").val();
		var total = $('.nomen_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (total > 0) {
			$('.nomen_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos obligatorios vacíos");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 6000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $('#d_no_mencionados').serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'trabajos_no_mencionados-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/actualizarNoMencionados'); ?>',
				method: "POST",
				data: {
					'nomen': nomen
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					if (res == 1) {
						setTimeout(function() {
							$('.loader').fadeOut();
						}, 200);
						$("#legalesModal").modal('hide');
						localStorage.setItem("success", 1);
						location.reload();
					}
				}
			});
		}
	}

	function actualizarEgresos() {
		var egresos = $("#d_egresos").serialize();
		egresos += "&id_candidato=" + $("#idCandidato").val();
		var total = $('.e_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (total > 0) {
			$('.e_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos obligatorios vacíos");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 6000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $('#d_egresos').serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'visita_egresos-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/actualizarEgresos'); ?>',
				method: "POST",
				data: {
					'egresos': egresos
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					if (res == 1) {
						setTimeout(function() {
							$('.loader').fadeOut();
						}, 200);
						$("#egresosModal").modal('hide');
						localStorage.setItem("success", 1);
						location.reload();
					}
				}
			});
		}
	}

	function actualizarHabitacion() {
		var hab = $("#d_habitacion").serialize();
		hab += "&id_candidato=" + $("#idCandidato").val();
		var total = $('.h_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (total > 0) {
			$('.h_obligado').each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos obligatorios vacíos");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 6000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $('#d_habitacion').serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'visita_habitacion-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/actualizarHabitacion'); ?>',
				method: "POST",
				data: {
					'hab': hab
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					if (res == 1) {
						setTimeout(function() {
							$('.loader').fadeOut();
						}, 200);
						$("#visitaHabitacionModal").modal('hide');
						localStorage.setItem("success", 1);
						location.reload();
					}
				}
			});
		}
	}

	function guardarExtrasCandidato(id_candidato) {
		var muebles = $("#candidato_muebles").val();
		var notas = $("#notas").val();

		var total = $('.extra_candidato').filter(function() {
			return !$(this).val();
		}).length;
		if (total > 0) {
			$(".extra_candidato").each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#txt_vacios").text(" Hay campos vacíos en los muebles o inmuebles y notas del candidato.");
					$('#vacios').css("display", "block");
					setTimeout(function() {
						$('#vacios').fadeOut();
					}, 4000);

				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $("#d_familiar_candidato").serialize();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'grupo_familiar_candidato-' + id_candidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/updateExtrasCandidato'); ?>',
				method: "POST",
				data: {
					'id_candidato': id_candidato,
					'notas': notas,
					'muebles': muebles
				},
				success: function(res) {
					if (res == 1) {
						$("#texto_msj").text('Los datos del candidato se han actualizado correctamente');
						$("#mensaje").css('display', 'block');
						setTimeout(function() {
							$('#mensaje').fadeOut();
						}, 6000);
					}
				}
			});
		}
	}

	function recargarTable() {
		$("#tabla").DataTable().ajax.reload();
	}

	function confirmarProceso() {
		var id_candidato = $(".idCandidato").val();
		$.ajax({
			url: "<?php echo base_url('Candidato/confirmarProceso'); ?>",
			method: "POST",
			data: {
				'id_candidato': id_candidato
			},
			dataType: "text",
			success: function(res) {
				$("#confirmarCandidatoModal").modal('hide');
				$("#texto_msj").text('Se ha iniciado el proceso para el candidato ' + $("#visita_candidato").val() + ' correctamente');
				$("#mensaje").css('display', 'block');
				setTimeout(function() {
					$('#mensaje').fadeOut();
				}, 6000);
				recargarTable();
			}
		});
	}

	function subirDoc() {
		if ($("#documento").val() == "") {
			$("#sin_documento").css("display", "block");
			setTimeout(function() {
				$("#sin_documento").fadeOut();
			}, 4000);
		} else {
			var id_candidato = $("#idCandidato").val();
			var prefijo = $(".prefijo").val();
			//var form = document.getElementById("formSubirDoc");
			var data = new FormData();
			var doc = $("#documento")[0].files[0];
			data.append('id_candidato', id_candidato);
			data.append('prefijo', prefijo);
			data.append('documento', doc);
			$.ajax({
				url: "<?php echo base_url('Candidato/uploadDoc'); ?>",
				method: "POST",
				data: data,
				contentType: false,
				cache: false,
				processData: false,

				success: function(res) {
					$("#documento").val("");

					$("#subido").css('display', 'block');
					setTimeout(function() {
						$('#subido').fadeOut();
					}, 4000);
					$("#tablaDocs").empty();
					$("#tablaDocs").html(res);
				}
			});
		}
	}
	//Se actualizan los documentos referenciando por su tipo
	function actualizarDocs() {
		$("#sin_documento").css("display", "none");
		var total = $("select[id^='tipo']").length;

		var hayArchivo = $('.item').filter(function() {
			return $(this).val();
		}).length;

		var totalVacios = $('.item').filter(function() {
			return !$(this).val();
		}).length;

		//console.log(hayArchivo)
		if (totalVacios > 0) {
			$(".item").each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#tipos_iguales").css('display', 'none');
					$("#campos_vacios").css('display', 'block');
					setTimeout(function() {
						$("#campos_vacios").fadeOut();
					}, 4000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			if (hayArchivo <= 0) {
				$("#sin_documento").css('display', 'block');
				setTimeout(function() {
					$('#sin_documento').fadeOut();
				}, 4000);
			} else {
				var id_candidato = $("#idCandidato").val();
				var prefijo = $(".prefijo").val();
				var salida = "";
				var indicador = 0;
				var aux = new Array();
				for (var i = 0; i < total; i++) {
					var arch = $("#arch" + i).text();
					var tipo = $("#tipo" + i).val();
					//console.log("idc "+id_candidato+" pre: "+prefijo+" arch: "+arch+" tipo: "+tipo);
					/*if(aux.includes(tipo) && tipo != 13){
						indicador = 1;
					}
					else{
						salida += arch+","+tipo+"@@";
					}*/
					salida += arch + "," + tipo + "@@";
					aux[i] = tipo;
				}
				if (indicador == 1) {
					$("#tipos_iguales").css('display', 'block');
					setTimeout(function() {
						$("#tipos_iguales").fadeOut();
					}, 4000);
				} else {
					$.ajax({
						url: '<?php echo base_url('Candidato/saveDocs'); ?>',
						method: 'POST',
						data: {
							'docs': salida,
							'id_candidato': id_candidato,
							'prefijo': prefijo
						},
						dataType: "text",
						beforeSend: function() {
							$('.loader').css("display", "block");
						},
						success: function(res) {
							if (res == 1) {
								setTimeout(function() {
									$('.loader').fadeOut();
								}, 300);
								$("#exito").css('display', 'block');
								$("#docsModal").modal('hide');
								setTimeout(function() {
									$('#exito').fadeOut();
								}, 6000);
							}
						}
					});
				}
			}
		}

	}
	//Modal para generar estatus de verificación de documentos
	function verificacionEstudios() {
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		$("#fecha_estatus_estudio").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkEstatusEstudios'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			dataType: "text",
			success: function(res) {
				if (res != 0) {
					var aux = res.split('@@');
					var finalizado = aux[2];
					if (finalizado == 1) {
						$("#div_estatus_estudio").css('display', 'none');
						$("#btnTerminarVerificacionEstudio").css('display', 'none');
					}
					$("#div_crearEstatusEstudio").empty();
					$("#idVerificacionEstudio").val(aux[1]);
					$("#div_crearEstatusEstudio").append(aux[0]);
				} else {
					$("#idVerificacionEstudio").val(0);
				}

			},
			error: function(res) {
				//$('#errorModal').modal('show');
			}
		});
		$("#verificacionEstudiosModal").modal("show");
	}
	//Generar estatus de verificacion de estudio
	function generarEstatusEstudio() {
		var id_candidato = $(".idCandidato").val();
		var id_verificacion = $("#idVerificacionEstudio").val();
		var comentario = $("#estudio_estatus_comentario").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusEstudios'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'id_verificacion': id_verificacion,
				'comentario': comentario
			},
			dataType: "text",
			success: function(res) {
				var aux = res.split('@@');
				$("#idVerificacionEstudio").val(aux[1]);
				$("#estudio_estatus_comentario").val("");
				$("#div_crearEstatusEstudio").empty();
				$("#div_crearEstatusEstudio").append(aux[0]);
			},
			error: function(res) {
				//$('#errorModal').modal('show');
			}
		});
	}
	//Termina con la verificación de estudios
	function terminarEstudios() {
		var id_verificacion = $("#idVerificacionEstudio").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/finishEstatusEstudios'); ?>',
			method: 'POST',
			data: {
				'id_verificacion': id_verificacion
			},
			dataType: "text",
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				$("#confirmarEstudiosModal").modal('hide');
				$("#verificacionEstudiosModal").modal('hide');
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 300);
				$("#exitoEstudios").css('display', 'block');
				setTimeout(function() {
					$('#exitoEstudios').fadeOut();
				}, 6000);

			},
			error: function(res) {
				//$('#errorModal').modal('show');
			}
		});
	}
	//Modal para generar estatus de verificación de referencias laborales
	function verificacionLaborales() {
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		$("#fecha_estatus_laboral").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkEstatusLaborales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			dataType: "text",
			success: function(res) {
				if (res != 0) {
					var aux = res.split('@@');
					var finalizado = aux[2];
					if (finalizado == 1) {
						$("#div_estatus_laboral").css('display', 'none');
						$("#btnTerminarVerificacionLaboral").css('display', 'none');
					}
					$("#div_crearEstatusLaboral").empty();
					$("#idVerificacionLaboral").val(aux[1]);
					$("#div_crearEstatusLaboral").append(aux[0]);
				} else {
					$("#idVerificacionLaboral").val(0);
				}

			},
			error: function(res) {
				//$('#errorModal').modal('show');
			}
		});
		$("#verificacionLaboralesModal").modal("show");
	}
	//Generar estatus de verificacion de referencia laboral
	function generarEstatusLaboral() {
		var id_candidato = $(".idCandidato").val();
		var id_status = $("#idVerificacionLaboral").val();
		var comentario = $("#laboral_estatus_comentario").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusLaborales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'id_status': id_status,
				'comentario': comentario
			},
			dataType: "text",
			success: function(res) {
				var aux = res.split('@@');
				$("#idVerificacionLaboral").val(aux[1]);
				$("#laboral_estatus_comentario").val("");
				$("#div_crearEstatusLaboral").empty();
				$("#div_crearEstatusLaboral").append(aux[0]);
			},
			error: function(res) {
				//$('#errorModal').modal('show');
			}
		});
	}
	//Termina con la verificación de ref laborales
	function terminarLaborales() {
		var id_status = $("#idVerificacionLaboral").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/finishEstatusLaborales'); ?>',
			method: 'POST',
			data: {
				'id_status': id_status
			},
			dataType: "text",
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				$("#confirmarLaboralesModal").modal('hide');
				$("#verificacionLaboralesModal").modal('hide');
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 300);
				$("#exitoLaborales").css('display', 'block');
				setTimeout(function() {
					$('#exitoLaborales').fadeOut();
				}, 6000);

			},
			error: function(res) {
				//$('#errorModal').modal('show');
			}
		});
	}
	//Modal para generar estatus de verificación de antecedentes no penales
	function verificacionPenales() {
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		$("#fecha_estatus_penales").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkEstatusPenales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			dataType: "text",
			success: function(res) {
				if (res != 0) {
					var aux = res.split('@@');
					var finalizado = aux[2];
					if (finalizado == 1) {
						$("#div_estatus_penales").css('display', 'none');
						$("#btnTerminarVerificacionPenales").css('display', 'none');
					}
					$("#div_crearEstatusPenales").empty();
					$("#idVerificacionPenales").val(aux[1]);
					$("#div_crearEstatusPenales").append(aux[0]);
				} else {
					$("#idVerificacionPenales").val(0);
				}

			},
			error: function(res) {
				//$('#errorModal').modal('show');
			}
		});
		$("#verificacionPenalesModal").modal("show");
	}

	function generarEstatusPenales() {
		var id_candidato = $("#idCandidato").val();
		var id_status = $("#idVerificacionPenales").val();
		var comentario = $("#penales_estatus_comentario").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusPenales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'id_status': id_status,
				'comentario': comentario
			},
			dataType: "text",
			success: function(res) {
				var aux = res.split('@@');
				$("#idVerificacionPenales").val(aux[1]);
				$("#penales_estatus_comentario").val("");
				$("#div_crearEstatusPenales").empty();
				$("#div_crearEstatusPenales").append(aux[0]);
			}
		});
	}

	function terminarPenales() {
		var id_status = $("#idVerificacionPenales").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/finishEstatusPenales'); ?>',
			method: 'POST',
			data: {
				'id_status': id_status
			},
			dataType: "text",
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				$("#confirmarPenalesModal").modal('hide');
				$("#verificacionPenalesModal").modal('hide');
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 300);
				$("#exitoPenales").css('display', 'block');
				setTimeout(function() {
					$('#exitoPenales').fadeOut();
				}, 6000);

			}
		});
	}

	

	function getMunicipioingles(id_estado, id_municipio) {
		$.ajax({
			url: '<?php echo base_url('Funciones/getMunicipios'); ?>',
			method: 'POST',
			data: {
				'id_estado': id_estado
			},
			dataType: "text",
			success: function(res) {
				$('#municipio_ingles').prop('disabled', false);
				$('#municipio_ingles').html(res);
				$("#municipio_ingles").find('option').attr("selected", false);
				$('#municipio_ingles option[value="' + id_municipio + '"]').attr('selected', 'selected');
			}
		});
	}

	function ejecutarAccion() {
		var id = $("#idCandidato").val();
		var id_cliente = $("#idCliente").val();
		var motivo = $("#motivo").val();
		var usuario = 1;
		if (motivo == "") {
			$("#quitarModal #msj_texto").text("Se requiere el motivo");
			$("#quitarModal #msj_error").css('display', 'block');
			setTimeout(function() {
				$('#quitarModal #msj_error').fadeOut();
			}, 4000);
		} else {
			$.ajax({
				url: '<?php echo base_url('Candidato/accionCandidato'); ?>',
				type: 'post',
				data: {
					'id': id,
					'motivo': motivo,
					'usuario': usuario,
					'id_cliente': id_cliente
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 2000);
					localStorage.setItem("deleted", 1);
					location.reload();
				}
			});
		}
	}
	//Verificamos si el candidato cuenta con los datos completos para poder generar el archivo final
	function checkCandidato(candidato, edad, lugar_nacimiento, grado, religion, corto_plazo, mediano_plazo, bebidas, bebidas_frecuencia, fuma, refs_comentarios, trabajos, vivienda, tiempo_residencia, zona, calidad, tamano, condiciones, vecinales, muebles, adeudo) {
		var id_candidato = $("#idCandidato").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/revisionEstudioCandidato'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			dataType: "text",
			/*beforeSend: function() {
            	$('.loader').css("display","block");
          	},*/
			success: function(res) {
				if (res == "D1SE1AS1F1P1AL1IL1N1") { //Indica todo completo
					$("#div_incompleto").css('display', 'none');
					$("#div_completo").css('display', 'block');
					$("#btnTerminar").css('display', 'initial');
					$("#personal1").val(candidato + ", de " + edad + " años, originario(a) de " + lugar_nacimiento + ", cuenta con un grado máximo de estudios de " + grado + ". Refiere ser " + religion + ", y sus planes a corto plazo son " + corto_plazo + " y a mediano plazo " + mediano_plazo);
					$("#personal2").val("Refiere " + bebidas + " bebidas alcohólicas " + bebidas_frecuencia + ". " + fuma + " Sus referencias personales lo describen como " + refs_comentarios + ".");
					$("#laboral1").val("Señaló " + trabajos + " referencias laborales");
					$("#laboral2").val(" cuyo(a) propietario(a)/dueño(a) es quien nos valida referencia laboral.");
					$("#socio1").val(candidato + ", actualmente vive en un/una " + vivienda + ", con un tiempo de residencia de " + tiempo_residencia + ". El nivel de la zona es " + zona + ", el mobiliario es de calidad " + calidad + ", la vivienda es " + tamano + " y en condiciones " + condiciones);
					$("#socio2").val("Los gastos generados en el hogar son solventados por _____. Sus referencias vecinales describen que es " + vecinales + ". El candidato cuenta con " + muebles + " " + adeudo);
					$("#completarModal").modal('show');
				} else {
					if (res == "D0") { //Indica sin verificacion de documentos
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Faltan datos en la verificación de documentos");
						$("#completarModal").modal('show');
					}
					if (res == "D1SE0") { //Indica sin historial academico
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Faltan datos en el estudio académico");
						$("#completarModal").modal('show');
					}
					if (res == "D1SE1AS0") { //Indica sin antecedentes sociales
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Faltan datos en los antecedentes sociales");
						$("#completarModal").modal('show');
					}
					if (res == "D1SE1AS1F0") { //Indica sin datos familiares
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Faltan datos en los familiares");
						$("#completarModal").modal('show');
					}
					if (res == "D1SE1AS1F1P0") { //Indica sin verificacion de referencias personales
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Faltan las referencias personales");
						$("#completarModal").modal('show');
					}
					if (res == "D1SE1AS1F1P1AL0") { //Indica sin antecedentes laborales
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Faltan los antecedentes laborales");
						$("#completarModal").modal('show');
					}
					if (res == "D1SE1AS1F1P1AL1IL0") { //Indica sin investigacion legal
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Falta investigacion legal");
						$("#completarModal").modal('show');
					}
					if (res == "D1SE1AS1F1P1AL1IL1N0") { //Indica sin registro de trabajos no mencionados
						$("#div_completo").css('display', 'none');
						$("#div_incompleto").css('display', 'block');
						$("#btnTerminar").css('display', 'none');
						$("#msjIncompleto").text("Sin registro de trabajos no mencionados");
						$("#completarModal").modal('show');
					}
				}
			}
		});
	}

	function subirPsicometria() {
		if ($("#doc_psicometria").val() == "") {
			$("#doc_psicometria").addClass("requerido");
			$("#psicometriaModal #campos_vacios").css("display", "block");
			setTimeout(function() {
				$("#psicometriaModal #campos_vacios").fadeOut();
			}, 4000);
		} else {
			var docs = new FormData();
			var archivo = $("#doc_psicometria")[0].files[0];
			docs.append("id_candidato", $("#idCandidato").val());
			docs.append("id_psicometrico", $("#idPsicometrico").val());
			docs.append("archivo", archivo);
			$.ajax({
				url: "<?php echo base_url('Psicometrico/subirPsicometria'); ?>",
				method: "POST",
				data: docs,
				contentType: false,
				cache: false,
				processData: false,
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					if (res == 1) {
						setTimeout(function() {
							$('.loader').fadeOut();
						}, 300);
						$("#psicometriaModal").modal('hide');
						$("#texto_msj").text(" La psicometria se ha guardado correctamente");
						$("#mensaje").css('display', 'block');
						recargarTable();
						setTimeout(function() {
							$("#mensaje").fadeOut();
						}, 6000);
					}
				}
			});
		}
	}

	function finalizarProceso() {
		var id_candidato = $("#idCandidato").val();
		var conclusiones = $("#formConclusiones").serialize();
		conclusiones += "&id_finalizado=" + $("#idFinalizado").val();
		var totalVacios = $('.conclusion_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (totalVacios > 0) {
			$(".conclusion_obligado").each(function() {
				var element = $(this);
				if (element.val() == "" || element.val() == -1) {
					element.addClass("requerido");
					$("#campos_vacios_check").css('display', 'block');
					setTimeout(function() {
						$("#campos_vacios_check").fadeOut();
					}, 4000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			formdata = $('#formConclusiones').serialize();
			var idCandidato = $("#idCandidato").val();
			var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'conclusiones-' + idCandidato + '-' + fecha_txt);
			$.ajax({
				url: '<?php echo base_url('Candidato/terminarProcesoCandidato'); ?>',
				method: 'POST',
				data: {
					'id_candidato': id_candidato,
					'conclusiones': conclusiones
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 2000);
					localStorage.setItem("finished", 1);
					location.reload();
				}
			});
		}
	}

	function finalizarProcesoIngles() {
		var id_candidato = $("#idCandidato").val();
		var checks = $("#formChecks").serialize();
		//console.log(checks)
		var totalVacios = $('.check_obligado').filter(function() {
			return !$(this).val();
		}).length;

		if (totalVacios > 0) {
			$(".check_obligado").each(function() {
				var element = $(this);
				if (element.val() == "" || element.val() == -1) {
					element.addClass("requerido");
					$("#campos_vacios_check").css('display', 'block');
					setTimeout(function() {
						$("#campos_vacios_check").fadeOut();
					}, 4000);
				} else {
					element.removeClass("requerido");
				}
			});
		} else {
			$.ajax({
				url: '<?php echo base_url('Candidato/terminarProcesoCandidatoIngles'); ?>',
				method: 'POST',
				data: {
					'id_candidato': id_candidato,
					'checks': checks
				},
				dataType: "text",
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 2000);
					localStorage.setItem("finished", 1);
					location.reload();
				}
			});
		}
	}

	

	function actualizarCandidato() {
		var id_candidato = $("#idCandidato").val();
		var id_doping = $("#idDoping").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/actualizarProcesoCandidato'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'id_doping': id_doping
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				if (res == 1) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 2000);
					localStorage.setItem("success", 1);
					location.reload();
				}
			}
		});
	}

	function respaldoTxt(formdata, nombreArchivo) {
		var textFileAsBlob = new Blob([formdata], {
			type: 'text/plain'
		});
		var fileNameToSaveAs = nombreArchivo + ".txt";
		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.innerHTML = "My Hidden Link";
		window.URL = window.URL || window.webkitURL;
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
		downloadLink.click();
	}

	function destroyClickedElement(event) {
		document.body.removeChild(event.target);
	}
	//Regresa del formulario al listado
	function regresar() {
		$("#btn_regresar").css('display', 'none');
		$("#formulario").css('display', 'none');
		$("#listado").css('display', 'block');
		$("#btn_nuevo").css('display', 'block');
	}

	function convertirDate(fecha) {
		var aux = fecha.split('-');
		var f = aux[2] + '/' + aux[1] + '/' + aux[0];
		return f;
	}

	function convertirDateTime(fecha) {
		var f = fecha.split(' ');
		var aux = f[0].split('-');
		var date = aux[2] + '/' + aux[1] + '/' + aux[0];
		return date;
	}

	function regresarListado() {
		location.reload();
	}
	function getMunicipio(id_estado, id_municipio) {
		$.ajax({
			url: '<?php echo base_url('Funciones/getMunicipios'); ?>',
			method: 'POST',
			data: {
				'id_estado': id_estado
			},
			dataType: "text",
			success: function(res) {
				$('#municipio').prop('disabled', false);
				$('#municipio').html(res);
				$("#municipio").find('option').attr("selected", false);
				$('#municipio option[value="' + id_municipio + '"]').attr('selected', 'selected');
			}
		});
	}
	$("#fecha_nacimiento").change(function() {
		var aux = $(this).val().split('/');
		var f = aux[1] + '/' + aux[0] + '/' + aux[2];
		var fecha = f;
		var today = new Date();
		var birthDate = new Date(fecha);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age = age - 1;
		}
		$("#edad").val(age);
	});
	$("#fecha_nacimiento_ingles").change(function() {
		var aux = $(this).val().split('/');
		var f = aux[1] + '/' + aux[0] + '/' + aux[2];
		var fecha = f;
		var today = new Date();
		var birthDate = new Date(fecha);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age = age - 1;
		}
		$("#edad_ingles").val(age);
	});
	//Acepta solo numeros en los input
	$(".solo_numeros").on("input", function() {
		var valor = $(this).val();
		$(this).val(valor.replace(/[^0-9]/g, ''));
	});
	//Limpiar inputs
	$(".personal_obligado, .obligado, .conclusion_obligado, .familia_obligado, .estudios_obligado, .p_obligado, .refPer_obligado, .asignar_obligado").focus(function() {
		$(this).removeClass("requerido");
	});
</script>