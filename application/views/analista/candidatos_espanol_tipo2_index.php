<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div id="exito" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> Los documentos han sido actualizados correctamente.
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Éxito!</strong><p id="texto_msj"></p>
  	</div>
  	<div id="vacios" class="alert alert-danger fade in mensaje" style='display:none;'>
      	<strong>¡Atención!</strong><span id="txt_vacios"></span>
  	</div>
  	<div id="exitoFinalizado" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>EL estudio ha finalizado, favor de revisar el PDF generado</strong>
  	</div>
	<div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			        <h4 class="modal-title">Nuevo Registro</h4>
		      	</div>
		      	<div class="modal-body">
		      		<form id="nuevoRegistroForm">
					<div class="row">
						<div class="col-md-6">
							<label for="subcliente">Subcliente (Proveedor) *</label>
							<select name="subcliente" id="subcliente" class="form-control obligado">
								<option value="">Selecciona</option>
								<?php 
									if($subclientes){
										foreach ($subclientes as $sub) { ?>
											<option value="<?php echo $sub->id; ?>"><?php echo $sub->nombre; ?></option>
								<?php 	}
										echo '<option value="0">N/A</option>';
									}
									else{ ?>
										<option value="0">N/A</option>
									
								<?php } ?>
							</select>
							<br><br>
						</div>
						<div class="col-md-6"> 
							<label for="puesto">Puesto *</label>
							<select name="puesto" id="puesto" class="form-control obligado">
								<option value="">Selecciona</option>
								<?php 
								foreach ($puestos as $p) { ?>
									<option value="<?php echo $p->id; ?>"><?php echo $p->nombre; ?></option>
								<?php	
								} ?>
							</select>
							<br><br>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="nombre">Nombre(s) *</label>
							<input type="text" class="form-control obligado" name="nombre" id="nombre" placeholder="Nombre(s)" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
							<br>
						</div>
						<div class="col-md-4">
							<label for="paterno">Apellido paterno *</label>
							<input type="text" class="form-control obligado" name="paterno" id="paterno" placeholder="Apellido Paterno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
							<br>
						</div>
						<div class="col-md-4">
							<label for="materno">Apellido materno</label>
							<input type="text" class="form-control" name="materno" id="materno" placeholder="Apellido materno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
							<br>
						</div>			    
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="correo">Correo electrónico</label>
							<input type="email" class="form-control" name="correo" id="correo" placeholder="Email" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
							<br>
						</div>
						<div class="col-md-4">
							<label for="celular">Num. Tel. Celular *</label>
							<input type="text" class="form-control solo_numeros obligado" name="celular" id="celular" placeholder="Num. Tel. Celular" maxlength="10">
							<br>
						</div>
						<div class="col-md-4">
							<label for="fijo">Num. Tel. Casa </label>
							<input type="text" class="form-control solo_numeros" name="fijo" id="fijo" placeholder="Num. Tel. Casa" maxlength="10">
							<br>
						</div>
					</div>
					<h4 class="text-center">¿Qué estudios requiere el candidato?</h4><br><br>
					<div class="row">
						<div class="col-md-4 div_estudio">
							<label class="contenedor_check fuente-14">Socioeconómico
								<input type="checkbox" name="socio" id="socio" checked disabled>
								<span class="checkmark"></span>
							</label>
							<br>
						</div>
						<div class="col-md-4 div_estudio">
							<label class="contenedor_check fuente-14">Antidoping
								<input type="checkbox" name="antidoping" id="antidoping">
								<span class="checkmark"></span>
							</label>
							<br>
						</div>
						<div class="col-md-4 div_estudio">
							<label class="contenedor_check fuente-14">Psicométrico
								<input type="checkbox" name="psicometrico" id="psicometrico">
								<span class="checkmark"></span>
							</label>
							<br>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 div_estudio">
							<label class="contenedor_check fuente-14">Médico
								<input type="checkbox" name="medico" id="medico">
								<span class="checkmark"></span>
							</label>
							<br>
						</div>
						<div class="col-md-4 div_estudio">
							<label class="contenedor_check fuente-14">Buró de Crédito
								<input type="checkbox" name="buro" id="buro">
								<span class="checkmark"></span>
							</label>
							<br>
						</div>
						<div class="col-md-4 div_estudio">
							<label class="contenedor_check fuente-14">Sociolaboral
								<input type="checkbox" name="laboral" id="laboral">
								<span class="checkmark"></span>
							</label>
							<br><br>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label for="examen">Examen antidoping *</label>
							<select name="examen" id="examen" class="form-control" disabled>
								<option value="" selected>Selecciona</option>
								<?php 
								foreach ($drogas as $d) { ?>
									<option value="<?php echo $d->id; ?>"><?php echo $d->nombre." (".$d->conjunto.")"; ?></option>
								<?php	
								} ?>
							</select>
							<br><br>
						</div>
						<div class="col-md-6">
							<label for="otro_requisito">¿Requiere algo más para el candidato?</label>
							<textarea class="form-control" name="otro_requisito" id="otro_requisito" rows="3" placeholder="Escriba su mensaje"></textarea>
							<br>
						</div>
					</div>
					<div id="div_antidoping" class="padding-40">
						
					</div>
					<div id="div_psicometrico" class="padding-40">
						
					</div>
					<div id="div_buro" class="padding-40">
						
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<label for="cv">Cargar CV o solicitud de empleo del candidato</label>
							<input type="file" id="cv" name="cv" class="form-control" accept=".pdf, .jpg, .jpeg, .png" multiple><br>
						</div>
					</div>
					</form>
			        <?php if(!empty(validation_errors())): ?>
						<div class="alert alert-danger in mensaje">
						  	<?php echo validation_errors(); ?>
						</div>
					<?php endif; ?>
					<div id="alert-msg">
						
					</div>
			        <div id="campos_vacios" class="alert alert-danger">
		      			<p class="msj_error text-white">Hay campos vacíos</p>
		      		</div>
		      		<div id="repetido">
		      			<p class="msj_error">El nombre y/o el email del candidato ya existen</p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-danger" onclick="registrar()">Registrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Avances en el estudio del candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idAvances">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusAvances">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_avances">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_avances"></p>
                            </div>
                            <div class="col-md-5">
                            	<label for="avances_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="avances_estatus_comentario" id="avances_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>
							<div class="col-md-4">
			        			<label for="adjunto">Adjuntar imagen</label>
			        			<input type="file" id="adjunto" name="adjunto" class="form-control" accept=".jpg, .jpeg, .png"><br>
			        		</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusAvance()">Actualizar avances</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="llamadasModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Registro de llamadas al candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idLlamadas">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusLlamadas">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_llamadas">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_llamadas"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="llamadas_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="llamadas_estatus_comentario" id="llamadas_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusLlamada()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="emailsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Registro de correos enviados al candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idEmails">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusEmail">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_emails">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_emails"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="emails_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="emails_estatus_comentario" id="emails_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusEmail()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="docsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Documentación</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" class="idCandidato">
		      		<input type="hidden" class="prefijo">
		        	<div class="row">
		        		<div class="col-md-4 margen">
		        			<label>Selecciona el documento</label><br>
	        				<input type="file" id="documento" class="doc_obligado" name="documento" accept=".pdf, .jpg, .png, .jpeg"><br>
                        	<button class="btn btn-primary" onclick="subirDoc()">Subir</button>
		        		</div>
		        		<div id="tablaDocs" class="col-md-8 borde">
		        		
		        		</div>
		        		
		        	</div>
		        	<div class="row margen">
		        		<div id="campos_vacios">
			      			<p class="msj_error">No se ha seleccionado el tipo de archivo</p>
			      		</div>
			      		<div id="tipos_iguales">
			      			<p class="msj_error">Los documentos deben ser de diferente tipo</p>
			      		</div>
			      		<div id="sin_documento">
			      			<p class="msj_error">Seleccione un documento para subir</p>
			      		</div>
			      		<div id="subido">
			      			<p class="msj_error">El documento se ha subido con éxito</p>
			      		</div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-danger" onclick="actualizarDocs()">Actualizar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	        		<div class="row" id="div_motivo">
	        			<div class="col-md-12">
	        				<label for="motivo">Motivo *</label>
	        				<textarea name="motivo" id="motivo" class="form-control" rows="3"></textarea>
	        				<br>
	        			</div>
	        		</div>
	        		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      		<button type="button" class="btn btn-success" onclick="ejecutarAccion()">Aceptar</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="eliminadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Procesos cancelados</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_listado"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="psicometriaModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Carga de archivo de psicometria</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<h4 class="text-left" id="psicometria_candidato"></h4><br>
					<input type="hidden" name="idPsicometrico" id="idPsicometrico">
                    <input id="doc_psicometria" name="doc_psicometria" class="psico_obligado" type="file" accept=".pdf"><br><br>
                    <br>
                    <div id="campos_vacios" class="alert alert-danger">
		      			<p class="msj_error text-white">No se ha seleccionado el archivo</p>
		      		</div>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-success" onclick="subirPsicometria()">Guardar</button>
		    	</div>
	  		</div>
		</div>
    </div>
    <div class="modal fade" id="generalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Datos generales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_generales">
                		<div class="row">
			        		<div class="col-md-4">
			        			<label for="nombre_general">Nombre(s) *</label>
			        			<input type="text" class="form-control personal_obligado" name="nombre_general" id="nombre_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="paterno_general">Apellido paterno *</label>
			        			<input type="text" class="form-control personal_obligado" name="paterno_general" id="paterno_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="materno_general">Apellido materno</label>
			        			<input type="text" class="form-control" name="materno_general" id="materno_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="fecha_nacimiento">Fecha de nacimiento *</label>
			        			<input type="text" class="form-control solo_lectura personal_obligado" name="fecha_nacimiento" id="fecha_nacimiento">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="edad">Edad *</label>
			        			<input type="text" class="form-control" id="edad" disabled>
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="nacionalidad">Nacionalidad *</label>
			        			<input type="text" class="form-control personal_obligado" name="nacionalidad" id="nacionalidad">
			        			<br>
			        		</div>
			        	</div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="puesto_general">Puesto *</label>
			        			<select name="puesto_general" id="puesto_general" class="form-control personal_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($puestos as $pu) {?>
						                <option value="<?php echo $pu->id; ?>"><?php echo $pu->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="genero">Género: *</label>
			        			<select name="genero" id="genero" class="form-control personal_obligado">
						            <option value="">Selecciona</option>
						            <option value="Masculino">Masculino</option>
						            <option value="Femenino">Femenino</option>
					          	</select>
					          	<br>
			        		</div>	
			        		<div class="col-md-4">
			        			<label for="calle">Calle *</label>
			        			<input type="text" class="form-control personal_obligado" name="calle" id="calle">
			        			<br>
			        		</div>	
				        </div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="exterior">No. Exterior *</label>
			        			<input type="text" class="form-control personal_obligado" name="exterior" id="exterior"maxlength="8">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="interior">No. Interior </label>
			        			<input type="text" class="form-control" name="interior" id="interior" maxlength="8">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="calles">Entre calles</label>
			        			<input type="text" class="form-control" name="calles" id="calles">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="colonia">Colonia *</label>
			        			<input type="text" class="form-control personal_obligado" name="colonia" id="colonia">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="estado">Estado *</label>
			        			<select name="estado" id="estado" class="form-control personal_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($estados as $e) {?>
						                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="municipio">Municipio (Ciudad o Delegación) *</label>
			        			<select name="municipio" id="municipio" class="form-control personal_obligado" disabled>
						            <option value="">Selecciona</option>
					          	</select>
					          	<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="cp">Código postal *</label>
			        			<input type="text" class="form-control solo_numeros personal_obligado" name="cp" id="cp" maxlength="5">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="civil">Estado civil *</label>
			        			<select name="civil" id="civil" class="form-control personal_obligado">
						            <option value="">Selecciona</option>
					            <?php foreach ($civiles as $civ) {?>
					                <option value="<?php echo $civ->id; ?>"><?php echo $civ->nombre; ?></option>
					            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="celular_general">Teléfono celular *</label>
			        			<input type="text" class="form-control solo_numeros personal_obligado" name="celular_general" id="celular_general" maxlength="10">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="tel_casa">Teléfono de casa </label>
			        			<input type="text" class="form-control solo_numeros" name="tel_casa" id="tel_casa" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Teléfono de oficina </label>
			        			<input type="text" class="form-control solo_numeros" name="tel_oficina" id="tel_oficina">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Correo electrónico </label>
			        			<input type="text" class="form-control" name="personales_correo" id="personales_correo">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="grado">Grado máximo de estudios *</label>
			        			<select name="grado" id="grado" class="form-control personal_obligado">
						            <option value="">Selecciona</option>
					            <?php foreach ($grados as $gr) {?>
					                	<option value="<?php echo $gr->id; ?>"><?php echo $gr->nombre; ?></option>
					            <?php } ?>
					          	</select>
					          	<br>
			        		</div>	
			        		<div class="col-md-4">
			        			<label>Tiempo en el domicilio actual *</label>
			        			<input type="text" class="form-control personal_obligado" name="tiempo_dom_actual" id="tiempo_dom_actual">
					          	<br>
			        		</div>	
			        		<div class="col-md-4">
			        			<label>Tiempo de traslado a la oficina *</label>
			        			<input type="text" class="form-control personal_obligado" name="tiempo_traslado" id="tiempo_traslado">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Medio de transporte *</label>
			        			<select name="medio_transporte" id="medio_transporte" class="form-control personal_obligado">
					            	<option value="">Selecciona</option>
					            	<option value="Transporte público">Transporte público</option>
					            	<option value="Carro propio">Carro propio</option>
					            	<option value="Motocicleta">Motocicleta</option>
					            	<option value="Bicicleta">Bicicleta</option>
					            	<option value="Caminando">Caminando</option>
					            	<option value="Taxi">Taxi (o Taxi ejecutivo)</option>
					            	<option value="Carpool">Carpool (aventon)</option>
					            	<option value="Patineta">Patineta patines</option>
					          	</select>
					          	<br>
			        		</div>	
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGenerales">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="academicosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Historial académico del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_estudios">
		      			<div class="alert alert-info">
		      				<p class="text-center">Primaria </p>
		      			</div>
                		<div class="row">
							<div class="col-md-4">
								<label for="prim_periodo">Periodo *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prim_periodo" id="prim_periodo">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="prim_escuela">Escuela *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prim_escuela" id="prim_escuela">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="prim_ciudad">Ciudad *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prim_ciudad" id="prim_ciudad">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label for="prim_certificado">Certificado *</label>
			        			<select name="prim_certificado" id="prim_certificado" class="form-control estudios_obligado">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="prim_promedio">Promedio *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prim_promedio" id="prim_promedio">
			        			<br>
							</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Secundaria </p>
		      			</div>
						<div class="row">
							<div class="col-md-4">
								<label for="sec_periodo">Periodo *</label>
			        			<input type="text" class="form-control estudios_obligado" name="sec_periodo" id="sec_periodo">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="sec_escuela">Escuela *</label>
			        			<input type="text" class="form-control estudios_obligado" name="sec_escuela" id="sec_escuela">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="sec_ciudad">Ciudad *</label>
			        			<input type="text" class="form-control estudios_obligado" name="sec_ciudad" id="sec_ciudad">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label for="sec_certificado">Certificado *</label>
			        			<select name="sec_certificado" id="sec_certificado" class="form-control estudios_obligado">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="sec_promedio">Promedio *</label>
			        			<input type="text" class="form-control estudios_obligado" name="sec_promedio" id="sec_promedio">
			        			<br>
							</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Bachillerato </p>
		      			</div>
						<div class="row">
							<div class="col-md-4">
								<label for="prep_periodo">Periodo *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prep_periodo" id="prep_periodo">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="prep_escuela">Escuela *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prep_escuela" id="prep_escuela">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="prep_ciudad">Ciudad *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prep_ciudad" id="prep_ciudad">
			        			<br>
							</div>
						</div>
						<div class="row">		
							<div class="col-md-4">
								<label for="prep_certificado">Certificado *</label>
			        			<select name="prep_certificado" id="prep_certificado" class="form-control estudios_obligado">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="prep_promedio">Promedio *</label>
			        			<input type="text" class="form-control estudios_obligado" name="prep_promedio" id="prep_promedio">
			        			<br>
							</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Licenciatura </p>
		      			</div>
						<div class="row">
							<div class="col-md-4">
								<label for="lic_periodo">Periodo *</label>
			        			<input type="text" class="form-control estudios_obligado" name="lic_periodo" id="lic_periodo">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="lic_escuela">Escuela *</label>
			        			<input type="text" class="form-control estudios_obligado" name="lic_escuela" id="lic_escuela">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="lic_ciudad">Ciudad *</label>
			        			<input type="text" class="form-control estudios_obligado" name="lic_ciudad" id="lic_ciudad">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label for="lic_certificado">Certificado *</label>
			        			<select name="lic_certificado" id="lic_certificado" class="form-control estudios_obligado">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="lic_promedio">Promedio *</label>
			        			<input type="text" class="form-control estudios_obligado" name="lic_promedio" id="lic_promedio">
			        			<br>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<label for="otro_certificado">Seminarios o Cursos *</label>
			        			<textarea class="form-control estudios_obligado" name="otro_certificado" id="otro_certificado" rows="3"></textarea>
			        			<br>
							</div>
						</div>
                		<div class="row">
							<div class="col-md-12">
								<label for="carrera_inactivo">Periodos inactivos (fechas y motivos) *</label>
			        			<textarea class="form-control estudios_obligado" name="carrera_inactivo" id="carrera_inactivo" rows="3"></textarea>
	        					<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="estudios_comentarios">Comentarios *</label>
			        			<textarea class="form-control estudios_obligado" name="estudios_comentarios" id="estudios_comentarios" rows="3"></textarea>
			        			<br><br>
							</div>
						</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarEstudios">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="socialesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Antecedentes sociales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_sociales">
			        	<div class="row">
			        		<div class="col-md-6">
			        			<label for="religion">¿Qué religión profesa? *</label>
			        			<input type="text" class="form-control social_obligado" name="religion" id="religion">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label for="religion_frecuencia">¿Con qué frecuencia? *</label>
			        			<input type="text" class="form-control social_obligado" name="religion_frecuencia" id="religion_frecuencia">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-3">
			        			<label for="bebidas">¿Ingiere bebidas alcohólicas? *</label>
			        			<select name="bebidas" id="bebidas" class="form-control social_obligado">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
			        		</div>
			        		<div class="col-md-3">
			        			<label for="bebidas_frecuencia">¿Con qué frecuencia? *</label>
			        			<input type="text" class="form-control social_obligado" name="bebidas_frecuencia" id="bebidas_frecuencia">
			        			<br>
			        		</div>
			        		<div class="col-md-3">
			        			<label for="fumar">¿Acostumbra fumar? *</label>
			        			<select name="fumar" id="fumar" class="form-control social_obligado">
			        				<option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
			        			<br>
			        		</div>
			        		<div class="col-md-3">
			        			<label for="fumar_frecuencia">¿Con qué frecuencia? *</label>
			        			<input type="text" class="form-control social_obligado" name="fumar_frecuencia" id="fumar_frecuencia">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-6">
			        			<label for="cirugia">¿Ha tenido alguna intervención quirúrgica? *</label>
			        			<input type="text" class="form-control social_obligado" name="cirugia" id="cirugia">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label for="enfermedades">¿Antecedentes de enfermedades en su familia directa? *</label>
			        			<input type="text" class="form-control social_obligado" name="enfermedades" id="enfermedades">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-6">
			        			<label for="corto_plazo">¿Cuáles son sus planes a corto plazo? *</label>
			        			<input type="text" class="form-control social_obligado" name="corto_plazo" id="corto_plazo">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label for="mediano_plazo">¿Cuáles son sus planes a mediano plazo? *</label>
			        			<input type="text" class="form-control social_obligado" name="mediano_plazo" id="mediano_plazo">
			        			<br><br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarSociales">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="refPersonalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Referencias personales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_refpersonal1">
		      			<div class="alert alert-info">
		      				<p class="text-center">Primer referencia </p>
		      			</div>
                		<div class="row">
							<div class="col-md-12">
								<label for="refPer1_nombre">Nombre *</label>
			        			<input type="text" class="form-control refPer1_obligado" name="refPer1_nombre" id="refPer1_nombre">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
				        		<label for="refPer1_tiempo">Tiempo de conocerlo *</label>
			        			<input type="text" class="form-control refPer1_obligado" name="refPer1_tiempo" id="refPer1_tiempo">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="refPer1_lugar">¿De qué lugar conoce al candidato? *</label>
			        			<input type="text" class="form-control refPer1_obligado" name="refPer1_lugar" id="refPer1_lugar">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="refPer1_trabaja">¿Sabe dónde trabaja el candidato? *</label>
			        			<select name="refPer1_trabaja" id="refPer1_trabaja" class="form-control refPer1_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-4">
				        		<label for="refPer1_vive">¿Sabe dónde vive el candidato? *</label>
			        			<select name="refPer1_vive" id="refPer1_vive" class="form-control refPer1_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
				        	</div>
							<div class="col-md-4">
			        			<label for="refPer1_telefono">Teléfono *</label>
			        			<input type="text" class="form-control solo_numeros refPer1_obligado" name="refPer1_telefono" id="refPer1_telefono" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
				        		<label for="refPer1_recomienda">¿La recomienda? *</label>
			        			<select name="refPer1_recomienda" id="refPer1_recomienda" class="form-control refPer1_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
						            <option value="2">N/A</option>
					          	</select>
					          	<br>
				        	</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" id="refPer1_id">
								<label for="refPer1_comentario">Comentarios *</label>
			        			<textarea class="form-control refPer1_obligado" name="refPer1_comentario" id="refPer1_comentario" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="row">
				        	<div class="col-md-3 col-md-offset-4">
				        		<button type="button" class="btn btn-primary" onclick="guardarPersonales(1)">Actualizar Referencia Personal #1</button>
				        		<br><br>
				        	</div>
				        </div>
						<br>
					</form>
					<form action="" id="d_refpersonal2">
						<div class="alert alert-info">
		      				<p class="text-center">Segunda referencia </p>
		      			</div>
						<div class="row">
							<div class="col-md-12">
								<label for="refPer2_nombre">Nombre *</label>
			        			<input type="text" class="form-control refPer2_obligado" name="refPer2_nombre" id="refPer2_nombre">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
				        		<label for="refPer2_tiempo">Tiempo de conocerlo *</label>
			        			<input type="text" class="form-control refPer2_obligado" name="refPer2_tiempo" id="refPer2_tiempo">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="refPer2_lugar">¿De qué lugar conoce al candidato? *</label>
			        			<input type="text" class="form-control refPer2_obligado" name="refPer2_lugar" id="refPer2_lugar">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="refPer2_trabaja">¿Sabe dónde trabaja el candidato? *</label>
			        			<select name="refPer2_trabaja" id="refPer2_trabaja" class="form-control refPer2_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-4">
				        		<label for="refPer2_vive">¿Sabe dónde vive el candidato? *</label>
			        			<select name="refPer2_vive" id="refPer2_vive" class="form-control refPer2_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
				        	</div>
							<div class="col-md-4">
			        			<label for="refPer2_telefono">Teléfono *</label>
			        			<input type="text" class="form-control solo_numeros refPer2_obligado" name="refPer2_telefono" id="refPer2_telefono" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
				        		<label for="refPer2_recomienda">¿La recomienda? *</label>
			        			<select name="refPer2_recomienda" id="refPer2_recomienda" class="form-control refPer2_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
						            <option value="2">N/A</option>
					          	</select>
					          	<br>
				        	</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" id="refPer2_id">
								<label for="refPer2_comentario">Comentarios *</label>
			        			<textarea class="form-control refPer2_obligado" name="refPer2_comentario" id="refPer2_comentario" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="row">
				        	<div class="col-md-3 col-md-offset-4">
				        		<button type="button" class="btn btn-primary" onclick="guardarPersonales(2)">Actualizar Referencia Personal #2</button>
				        		<br><br>
				        	</div>
				        </div>
						<br>
					</form>
					<form action="" id="d_refpersonal3">
						<div class="alert alert-info">
		      				<p class="text-center">Tercera referencia </p>
		      			</div>
						<div class="row">
							<div class="col-md-12">
								<label for="refPer3_nombre">Nombre *</label>
			        			<input type="text" class="form-control refPer3_obligado" name="refPer3_nombre" id="refPer3_nombre">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
				        		<label for="refPer3_tiempo">Tiempo de conocerlo *</label>
			        			<input type="text" class="form-control refPer3_obligado" name="refPer3_tiempo" id="refPer3_tiempo">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="refPer3_lugar">¿De qué lugar conoce al candidato? *</label>
			        			<input type="text" class="form-control refPer3_obligado" name="refPer3_lugar" id="refPer3_lugar">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="refPer3_trabaja">¿Sabe dónde trabaja el candidato? *</label>
			        			<select name="refPer3_trabaja" id="refPer3_trabaja" class="form-control refPer3_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-4">
				        		<label for="refPer3_vive">¿Sabe dónde vive el candidato? *</label>
			        			<select name="refPer3_vive" id="refPer3_vive" class="form-control refPer3_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
					          	</select>
					          	<br>
				        	</div>
							<div class="col-md-4">
			        			<label for="refPer3_telefono">Teléfono *</label>
			        			<input type="text" class="form-control solo_numeros refPer3_obligado" name="refPer3_telefono" id="refPer3_telefono" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
				        		<label for="refPer3_recomienda">¿La recomienda? *</label>
			        			<select name="refPer3_recomienda" id="refPer3_recomienda" class="form-control refPer3_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">No</option>
						            <option value="1">Sí</option>
						            <option value="2">N/A</option>
					          	</select>
					          	<br>
				        	</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" id="refPer3_id">
								<label for="refPer3_comentario">Comentarios *</label>
			        			<textarea class="form-control refPer3_obligado" name="refPer3_comentario" id="refPer3_comentario" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="row">
				        	<div class="col-md-3 col-md-offset-4">
				        		<button type="button" class="btn btn-primary" onclick="guardarPersonales(3)">Actualizar Referencia Personal #3</button>
				        		<br><br>
				        	</div>
				        </div>
						<br>
					</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="noMencionadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Trabajos no mencionados del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_no_mencionados">
                		<div class="row">
            				<div class="col-md-6">
                				<label>Trabajos no mencionados *</label>
                				<textarea cols="2" class="form-control nomen_obligado" name="no_mencionados" id="no_mencionados"></textarea>
        						<br>
                			</div>
                			<div class="col-md-6">
                				<label>Resultado *</label>
                				<textarea cols="2" class="form-control nomen_obligado" name="resultado_no_mencionados" id="resultado_no_mencionados"></textarea>
        						<br>
                			</div>
            			</div>
            			<div class="row">
                			<div class="col-md-12">
                				<label>Notas *</label>
                				<textarea cols="2" class="form-control nomen_obligado" name="notas_no_mencionados" id="notas_no_mencionados"></textarea>
        						<br>
                			</div>
            			</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" onclick="actualizarNoMencionados()">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="visitaDocumentosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Documentos de la visita del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_documentos">
		      			<div class="alert alert-info">
		      				<p class="text-center">Afiliación al IMSS *</p>
		      			</div>
						<div class="row">
							<div class="col-md-6">
                				<label for="imss1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="imss1" id="imss1">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label for="imss2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="imss2" id="imss2">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Comprobante de Domicilio * </p>
		      			</div>
						<div class="row">
							<div class="col-md-6">
                				<label for="dom1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="dom1" id="dom1">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label for="dom2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="dom2" id="dom2">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">INE (IFE o pasaporte) *</p>
		      			</div>
						<div class="row">
							<div class="col-md-6">
                				<label for="ine1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="ine1" id="ine1">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label for="ine2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="ine2" id="ine2">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">CURP * </p>
		      			</div>
						<div class="row">
							<div class="col-md-6">
                				<label for="curp1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="curp1" id="curp1">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label for="curp2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="curp2" id="curp2">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">RFC * </p>
		      			</div>
						<div class="row">
							<div class="col-md-6">
                				<label for="rfc1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="rfc1" id="rfc1">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label for="rfc2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="rfc2" id="rfc2">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Licencia para conducir * </p>
		      			</div>
						<div class="row">
							<div class="col-md-6">
                				<label for="lic1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="lic1" id="lic1">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label for="lic2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="lic2" id="lic2">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Cartas de recomendación * </p>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-6">
                				<label for="carta1">Número de documento *</label>
                				<input type="text" class="form-control p_obligado_2" name="carta1" id="carta1">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label for="carta2">Dato / Institución *</label>
			        			<input type="text" class="form-control p_obligado_2" name="carta2" id="carta2">
			        			<br><br>
                			</div>
                		</div>
                		<div class="alert alert-info">
		      				<p class="text-center">Comentarios * </p>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-12">
                				<textarea class="form-control" name="comentarios_documentos" id="comentarios_documentos" rows="2"></textarea>
			        			<br>
                			</div>
                		</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarDocumentacion">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="familiaresModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Datos del grupo familiar del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_familia">
                		<div id="div_familiares">
	                			
                		</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="egresosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Egresos mensuales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_egresos">
                		<div class="row">
							<div class="col-md-4">
								<label for="renta">Renta *</label>
			        			<input type="text" class="form-control solo_numeros e_obligado" name="renta" id="renta" maxlength="8">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="alimentos">Alimentos *</label>
			        			<input type="text" class="form-control solo_numeros e_obligado" name="alimentos" id="alimentos" maxlength="8">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="servicios">Servicios *</label>
			        			<input type="text" class="form-control solo_numeros e_obligado" name="servicios" id="servicios">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label for="transportes">Transportes *</label>
			        			<input type="text" class="form-control solo_numeros e_obligado" name="transportes" id="transportes">
			        			<br>
							</div>
							<div class="col-md-4">
								<label for="otros_gastos">Otros *</label>
			        			<input type="text" class="form-control solo_numeros e_obligado" name="otros_gastos" id="otros_gastos">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="solvencia">Cuando los egresos son mayores a los ingresos, ¿cómo los solventa? *</label>
			        			<textarea class="form-control e_obligado" name="solvencia" id="solvencia" rows="2"></textarea>
			        			<br>
							</div>
						</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" onclick="actualizarEgresos()">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="visitaHabitacionModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Habitacion y medio ambiente del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_habitacion">
                		<div class="row">
							<div class="col-md-6">
								<label for="tiempo_residencia">Tiempo de residencia en el domicilio actual *</label>
			        			<input type="text" class="form-control h_obligado" name="tiempo_residencia" id="tiempo_residencia">
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="nivel_zona">Nivel de la zona *</label>
			        			<select name="nivel_zona" id="nivel_zona" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($zonas as $z) {?>
						                <option value="<?php echo $z->id; ?>"><?php echo $z->nombre; ?></option>
						            <?php } ?>
					          	</select>
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="tipo_vivienda">Tipo de vivienda *</label>
			        			<select name="tipo_vivienda" id="tipo_vivienda" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($viviendas as $viv) {?>
						                <option value="<?php echo $viv->id; ?>"><?php echo $viv->nombre; ?></option>
						            <?php } ?>
					          	</select>
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="recamaras">Recámaras *</label>
			        			<input type="number" class="form-control h_obligado" name="recamaras" id="recamaras">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="banios">Baños *</label>
			        			<input type="text" class="form-control h_obligado" name="banios" id="banios">
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="distribucion">Distribución *</label>
			        			<textarea class="form-control h_obligado" name="distribucion" id="distribucion" rows="2"></textarea>
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="calidad_mobiliario">Calidad mobiliario *</label>
			        			<select name="calidad_mobiliario" id="calidad_mobiliario" class="form-control h_obligado">
						            <option value="">Selecciona</option>
						            <option value="1">Buena</option>
						            <option value="2">Regular</option>
						            <option value="3">Deficiente</option>
					          	</select>
					          	<br>
							</div>
							<div class="col-md-6">
								<label for="mobiliario">Mobiliario *</label>
			        			<select name="mobiliario" id="mobiliario" class="form-control h_obligado">
						            <option value="">Selecciona</option>
						            <option value="0">Incompleto</option>
						            <option value="1">Completo</option>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="tamanio_vivienda">Tamaño vivienda *</label>
			        			<select name="tamanio_vivienda" id="tamanio_vivienda" class="form-control h_obligado">
						            <option value="">Selecciona</option>
						            <option value="1">Amplia</option>
						            <option value="2">Media</option>
						            <option value="3">Reducida</option>
					          	</select>
					          	<br>
							</div>
							<div class="col-md-6">
								<label for="condiciones_vivienda">Condiciones de la vivienda *</label>
			        			<select name="condiciones_vivienda" id="condiciones_vivienda" class="form-control p_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($condiciones as $cond) {?>
						                <option value="<?php echo $cond->id; ?>"><?php echo $cond->nombre; ?></option>
						            <?php } ?>
					          	</select>
			        			<br>
							</div>
						</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" onclick="actualizarHabitacion()">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="refVecinalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Referencias vecinales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_refVecinal1">
		      			<!--div class="alert alert-info">
		      				<p class="text-center">Primer referencia </p>
		      				<input type="hidden" id="idrefvec1">
		      			</div-->
                		<div class="row">
							<div class="col-md-12">
								<label for="vecino1_nombre">Nombre *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_nombre" id="vecino1_nombre">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="vecino1_domicilio">Domicilio *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_domicilio" id="vecino1_domicilio">
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="vecino1_tel">Teléfono *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_tel" id="vecino1_tel">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="vecino1_concepto">¿Qué concepto tiene del aspirante?  *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_concepto" id="vecino1_concepto">
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="vecino1_familia">¿En qué concepto tiene a la familia como vecinos? *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_familia" id="vecino1_familia">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="vecino1_civil">¿Conoce el estado civil del aspirante? ¿Cuál es? *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_civil" id="vecino1_civil">
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="vecino1_hijos">¿Tiene hijos?  *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_hijos" id="vecino1_hijos">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="vecino1_sabetrabaja">¿Sabe en dónde trabaja? *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_sabetrabaja" id="vecino1_sabetrabaja">
			        			<br>
							</div>
							<div class="col-md-6">
								<label for="vecino1_notas">Notas *</label>
			        			<input type="text" class="form-control vec1_obligado" name="vecino1_notas" id="vecino1_notas">
			        			<br>
							</div>
						</div>
						
					</form>
					<div class="row">
			        	<div class="col-md-3 col-md-offset-4">
			        		<button type="button" class="btn btn-primary" onclick="guardarVecinales(1)">Actualizar Referencia Vecinal #1</button>
			        		<br><br>
			        	</div>
			        </div>
					<br>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        		<!--button type="button" class="btn btn-success" id="">Guardar</button-->
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="completarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Finalizar proceso del candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
	      			<form id="formConclusiones">
	      				<div class="alert alert-info text-center">
			      			<p>Conclusión Personal</p>
			      		</div>
		      			<div class="row">
		      				<div class="col-md-6">
		      					<label for="check_identidad">Descripción de datos personales *</label>
		        				<textarea class="form-control conclusion_obligado" name="personal1" id="personal1" rows="6"></textarea>
				          		<br>
		      				</div>
		      				<div class="col-md-6">
		      					<label for="check_identidad">Descripción de hábitos y referencias personales *</label>
		        				<textarea class="form-control conclusion_obligado" name="personal2" id="personal2" rows="6"></textarea>
				          		<br>
		      				</div>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-6">
		      					<label for="check_identidad">Descripción de metas (corto y mediano plazo) *</label>
		        				<textarea class="form-control conclusion_obligado" name="personal3" id="personal3" rows="6"></textarea>
				          		<br>
		      				</div>
		      				<div class="col-md-6">
		      					<label for="check_identidad">Descripción de estudios *</label>
		        				<textarea class="form-control conclusion_obligado" name="personal4" id="personal4" rows="6"></textarea>
				          		<br>
		      				</div>
		      			</div>
		      			<div class="alert alert-info text-center">
			      			<p>Conclusión Laboral</p>
			      		</div>
		      			<div class="row">
		      				<div class="col-md-6">
		      					<label for="check_identidad">Número de referencias laborales señaladas *</label>
		        				<textarea class="form-control conclusion_obligado" name="laboral1" id="laboral1" rows="2"></textarea>
				          		<br>
		      				</div>
		      				<div class="col-md-6">
		      					<label for="check_identidad">Descripción de las referencias laborales *</label>
		        				<textarea class="form-control conclusion_obligado" name="laboral2" id="laboral2" rows="6"></textarea>
				          		<br>
		      				</div>
		      			</div>
		      			<div class="alert alert-info text-center">
			      			<p>Conclusión Socioeconómica</p>
			      		</div>
		      			<div class="row">
		      				<div class="col-md-6">
		      					<label for="check_identidad">Descripción de la vivienda, zona y condiciones *</label>
		        				<textarea class="form-control conclusion_obligado" name="socio1" id="socio1" rows="6"></textarea>
				          		<br>
		      				</div>
		      				<div class="col-md-6">
		      					<label for="check_identidad">Descripción de ingresos y gastos *</label>
		        				<textarea class="form-control conclusion_obligado" name="socio2" id="socio2" rows="6"></textarea>
				          		<br>
		      				</div>
		      			</div>
		      			<div class="alert alert-info text-center">
			      			<p>Conclusión Visita</p>
			      		</div>
		      			<div class="row">
		      				<div class="col-md-6">
		      					<label for="check_identidad">Conclusiones de la referencia vecinal *</label>
		        				<textarea class="form-control conclusion_obligado" name="visita2" id="visita2" rows="6"></textarea>
				          		<br><br>
		      				</div>
		      				<div class="col-md-6">
		      					<label for="check_identidad">Conclusiones del visitador *</label>
		        				<textarea class="form-control conclusion_obligado" name="visita1" id="visita1" rows="6"></textarea>
				          		<br><br>
		      				</div>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-5 col-md-offset-4">
		      					<label>De acuerdo a lo anterior, la persona investigada es considerada: *</label>
		      					<select name="recomendable" id="recomendable" class="form-control conclusion_obligado">
					            	<option value="">Selecciona</option>
					            	<option value="2">No recomendable</option>
					            	<option value="1">Recomendable</option>
					            	<option value="3">Con reservas</option>
				          		</select>
	                			<br>
		      				</div>
		      			</div>
		      		</form>
	      			<div id="campos_vacios_check" class="alert alert-danger">
		      			<p class="text-center">Hay campos vacíos</p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-success" onclick="finalizarProceso()">Guardar</button>
			        <input type="hidden" id="idFinalizado">
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="actualizarCandidatoModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Actualizar candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
	      			<div class="">
	      				<p class="text-center"><b>IMPORTANTE:</b> Para actualizar al candidato considera los siguientes puntos: <br><br>		<ul>
	      						<li>Se reanudarán los resultados (estatus) finales del estudio actual</li>
	      						<li>La información recabada en la visita será eliminada para que se vuelva a realizar</li>
			      				<li>El archivo PDF del estudio finalizado actual se respaldará.</li>
			      				<li>La mayoría de la información del candidato (que no entra en la visita) se matendrá para su actualización</li>
			      				<li>El examen antidoping se va requerir nuevamente</li>
			      				<li>La psicometría y/o el examen médico (en caso de tener) se podrán actualizar </li>
			      				<li>Los puntos anteriores se aplicarán en las siguientes actualizaciones (a reserva de que haya cambios posteriores)</li>
		      				</ul><br>
	      				</p>
	      				<p class="text-center">¿Desea continuar?</p>
	      			</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" onclick="actualizarCandidato()">Aceptar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<section class="content-header">
      	<h1 class="titulo_seccion">Cliente<small><?php echo $cliente; ?></small></h1>
      	<a class="btn btn-app" id="btn_eliminados" class="btn btn-app">
    		<i class="fas fa-trash"></i> <span>Ver cancelados</span>
  		</a>
    	<a class="btn btn-app btn_acciones" id="btn_nuevo" data-toggle="modal" data-target="#newModal">
    		<i class="fas fa-plus-circle"></i> <span>Nuevo candidato</span>
  		</a>
  		<a class="btn btn-app btn_acciones" id="btn_regresar" onclick="regresarListado()" style="display: none;">
    		<i class="far fa-arrow-alt-circle-left"></i> <span>Regresar al listado</span>
  		</a>
    </section>
    
    <div class="loader" style="display: none;"></div>
    <a href="#" class="scroll-to-top"><i class="fas fa-arrow-up"></i><span class="sr-only">Ir arriba</span></a>
	<section class="content" id="listado">
	  	<div class="row">
	        <div class="col-xs-12">
	          	<div class="box">
	            	<div class="box-header">
	              		<!--h3 class="box-title"><strong>  Propietarios</strong></h3-->
	            	</div>
	            	<!-- /.box-header -->
	            	<div class="box-body table-responsive">
	              		<table id="tabla" class="table stripe hover row-border cell-border"></table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
	          	<!-- /.box -->
	        </div>
	    </div>
	</section>
	<section class="content" id="formulario" style="display: none;">
		<input type="hidden" id="idCandidato">
		<input type="hidden" id="idCliente">
		<input type="hidden" id="idDoping">
		<div class="row">
    		<div class="col-xs-12">
      			<div class="box">
        			<div class="box-body">
                		<div class="box-header tituloSubseccion">
                  			<p class="box-title"><strong>  Referencias Laborales </strong></p>
                		</div>
                		<div class="row">
				        	<div class="col-md-12">
				        		<div class="alert alert-warning"><h4 class="text-center">¿Cómo se enteró del trabajo en <span id="nombreCliente"></span>?</h4></div>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-12">
                				<textarea class="form-control trabajo_gobierno" name="trabajo_enterado" id="trabajo_enterado" rows="2"></textarea><br><br>
				        	</div>
				        </div>
                		<div class="row">
				        	<div class="col-md-12">
				        		<div class="alert alert-warning"><h4 class="text-center">¿Has trabajado en alguna entidad de gobierno, partido político u ONG?</h4></div>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-12">
                				<textarea class="form-control trabajo_gobierno" name="trabajo_gobierno" id="trabajo_gobierno" rows="2"></textarea><br><br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="actualizarTrabajoGobierno()">Guardar respuestas anteriores</button>
				        		<br><br>
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
                		<div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  1. Último trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral1">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_empresa" id="reflab1_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_direccion" id="reflab1_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_entrada" id="reflab1_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_salida" id="reflab1_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab1_obligado" name="reflab1_telefono" id="reflab1_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_puesto1" id="reflab1_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_puesto2" id="reflab1_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_salario1" id="reflab1_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_salario2" id="reflab1_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_jefenombre" id="reflab1_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_jefecorreo" id="reflab1_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_jefepuesto" id="reflab1_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab1_obligado" name="reflab1_separacion" id="reflab1_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(1)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab1">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral1">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_empresa" id="an_reflab1_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_direccion" id="an_reflab1_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_entrada" id="an_reflab1_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_salida" id="an_reflab1_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab1_obligado" name="an_reflab1_telefono" id="an_reflab1_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_puesto1" id="an_reflab1_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_puesto2" id="an_reflab1_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_salario1" id="an_reflab1_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_salario2" id="an_reflab1_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_jefenombre" id="an_reflab1_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_jefecorreo" id="an_reflab1_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_jefepuesto" id="an_reflab1_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab1_obligado" name="an_reflab1_separacion" id="an_reflab1_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab1_obligado" name="an_reflab1_cualidades" id="an_reflab1_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab1_obligado" name="an_reflab1_mejoras" id="an_reflab1_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab1_obligado" name="an_reflab1_comentarios" id="an_reflab1_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(1)">Guardar la verificación del Último Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab1">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        <div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  2. Segundo trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral2">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_empresa" id="reflab2_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_direccion" id="reflab2_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_entrada" id="reflab2_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_salida" id="reflab2_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab2_obligado" name="reflab2_telefono" id="reflab2_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_puesto1" id="reflab2_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_puesto2" id="reflab2_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_salario1" id="reflab2_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_salario2" id="reflab2_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_jefenombre" id="reflab2_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_jefecorreo" id="reflab2_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_jefepuesto" id="reflab2_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab2_obligado" name="reflab2_separacion" id="reflab2_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(2)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab2">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral2">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_empresa" id="an_reflab2_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_direccion" id="an_reflab2_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_entrada" id="an_reflab2_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_salida" id="an_reflab2_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab2_obligado" name="an_reflab2_telefono" id="an_reflab2_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_puesto1" id="an_reflab2_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_puesto2" id="an_reflab2_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_salario1" id="an_reflab2_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_salario2" id="an_reflab2_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_jefenombre" id="an_reflab2_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_jefecorreo" id="an_reflab2_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_jefepuesto" id="an_reflab2_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab2_obligado" name="an_reflab2_separacion" id="an_reflab2_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab2_obligado" name="an_reflab2_cualidades" id="an_reflab2_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab2_obligado" name="an_reflab2_mejoras" id="an_reflab2_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab2_obligado" name="an_reflab2_comentarios" id="an_reflab2_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(2)">Guardar la verificación del Segundo Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab2">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        <div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  3. Tercer trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral3">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_empresa" id="reflab3_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_direccion" id="reflab3_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_entrada" id="reflab3_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_salida" id="reflab3_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab3_obligado" name="reflab3_telefono" id="reflab3_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_puesto1" id="reflab3_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_puesto2" id="reflab3_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_salario1" id="reflab3_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_salario2" id="reflab3_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_jefenombre" id="reflab3_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_jefecorreo" id="reflab3_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_jefepuesto" id="reflab3_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab3_obligado" name="reflab3_separacion" id="reflab3_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(3)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab3">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral3">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_empresa" id="an_reflab3_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_direccion" id="an_reflab3_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_entrada" id="an_reflab3_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_salida" id="an_reflab3_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab3_obligado" name="an_reflab3_telefono" id="an_reflab3_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_puesto1" id="an_reflab3_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_puesto2" id="an_reflab3_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_salario1" id="an_reflab3_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_salario2" id="an_reflab3_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_jefenombre" id="an_reflab3_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_jefecorreo" id="an_reflab3_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_jefepuesto" id="an_reflab3_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab3_obligado" name="an_reflab3_separacion" id="an_reflab3_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab3_obligado" name="an_reflab3_cualidades" id="an_reflab3_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab3_obligado" name="an_reflab3_mejoras" id="an_reflab3_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab3_obligado" name="an_reflab3_comentarios" id="an_reflab3_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(3)">Guardar la verificación del Tercer Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab3">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        <div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  4. Cuarto trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral4">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_empresa" id="reflab4_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_direccion" id="reflab4_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_entrada" id="reflab4_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_salida" id="reflab4_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab4_obligado" name="reflab4_telefono" id="reflab4_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_puesto1" id="reflab4_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_puesto2" id="reflab4_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_salario1" id="reflab4_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_salario2" id="reflab4_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_jefenombre" id="reflab4_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_jefecorreo" id="reflab4_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_jefepuesto" id="reflab4_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab4_obligado" name="reflab4_separacion" id="reflab4_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(4)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab4">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral4">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_empresa" id="an_reflab4_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_direccion" id="an_reflab4_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_entrada" id="an_reflab4_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_salida" id="an_reflab4_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab4_obligado" name="an_reflab4_telefono" id="an_reflab4_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_puesto1" id="an_reflab4_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_puesto2" id="an_reflab4_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_salario1" id="an_reflab4_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_salario2" id="an_reflab4_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_jefenombre" id="an_reflab4_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_jefecorreo" id="an_reflab4_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_jefepuesto" id="an_reflab4_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab4_obligado" name="an_reflab4_separacion" id="an_reflab4_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab4_obligado" name="an_reflab4_cualidades" id="an_reflab4_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab4_obligado" name="an_reflab4_mejoras" id="an_reflab4_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab4_obligado" name="an_reflab4_comentarios" id="an_reflab4_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(4)">Guardar la verificación del Cuarto Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab4">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        <div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  5. Quinto trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral5">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_empresa" id="reflab5_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_direccion" id="reflab5_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_entrada" id="reflab5_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_salida" id="reflab5_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab5_obligado" name="reflab5_telefono" id="reflab5_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_puesto1" id="reflab5_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_puesto2" id="reflab5_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_salario1" id="reflab5_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_salario2" id="reflab5_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_jefenombre" id="reflab5_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_jefecorreo" id="reflab5_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_jefepuesto" id="reflab5_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab5_obligado" name="reflab5_separacion" id="reflab5_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(5)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab5">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral5">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_empresa" id="an_reflab5_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_direccion" id="an_reflab5_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_entrada" id="an_reflab5_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_salida" id="an_reflab5_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab5_obligado" name="an_reflab5_telefono" id="an_reflab5_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_puesto1" id="an_reflab5_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_puesto2" id="an_reflab5_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_salario1" id="an_reflab5_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_salario2" id="an_reflab5_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_jefenombre" id="an_reflab5_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_jefecorreo" id="an_reflab5_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_jefepuesto" id="an_reflab5_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab5_obligado" name="an_reflab5_separacion" id="an_reflab5_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab5_obligado" name="an_reflab5_cualidades" id="an_reflab5_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab5_obligado" name="an_reflab5_mejoras" id="an_reflab5_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab5_obligado" name="an_reflab5_comentarios" id="an_reflab5_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(5)">Guardar la verificación del Cuarto Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab5">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        <div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  6. Sexto trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral6">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_empresa" id="reflab6_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_direccion" id="reflab6_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_entrada" id="reflab6_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_salida" id="reflab6_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab6_obligado" name="reflab6_telefono" id="reflab6_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_puesto1" id="reflab6_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_puesto2" id="reflab6_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_salario1" id="reflab6_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_salario2" id="reflab6_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_jefenombre" id="reflab6_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_jefecorreo" id="reflab6_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_jefepuesto" id="reflab6_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab6_obligado" name="reflab6_separacion" id="reflab6_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(6)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab6">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral6">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_empresa" id="an_reflab6_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_direccion" id="an_reflab6_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_entrada" id="an_reflab6_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_salida" id="an_reflab6_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab6_obligado" name="an_reflab6_telefono" id="an_reflab6_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_puesto1" id="an_reflab6_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_puesto2" id="an_reflab6_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_salario1" id="an_reflab6_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_salario2" id="an_reflab6_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_jefenombre" id="an_reflab6_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_jefecorreo" id="an_reflab6_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_jefepuesto" id="an_reflab6_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab6_obligado" name="an_reflab6_separacion" id="an_reflab6_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab6_obligado" name="an_reflab6_cualidades" id="an_reflab6_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab6_obligado" name="an_reflab6_mejoras" id="an_reflab6_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab6_obligado" name="an_reflab6_comentarios" id="an_reflab6_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(6)">Guardar la verificación del Sexto Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab6">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        <div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  7. Séptimo trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral7">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_empresa" id="reflab7_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_direccion" id="reflab7_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_entrada" id="reflab7_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_salida" id="reflab7_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab7_obligado" name="reflab7_telefono" id="reflab7_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_puesto1" id="reflab7_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_puesto2" id="reflab7_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control  reflab7_obligado" name="reflab7_salario1" id="reflab7_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_salario2" id="reflab7_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_jefenombre" id="reflab7_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_jefecorreo" id="reflab7_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_jefepuesto" id="reflab7_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab7_obligado" name="reflab7_separacion" id="reflab7_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(7)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab7">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral7">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_empresa" id="an_reflab7_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_direccion" id="an_reflab7_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_entrada" id="an_reflab7_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_salida" id="an_reflab7_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab7_obligado" name="an_reflab7_telefono" id="an_reflab7_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_puesto1" id="an_reflab7_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_puesto2" id="an_reflab7_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_salario1" id="an_reflab7_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_salario2" id="an_reflab7_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_jefenombre" id="an_reflab7_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_jefecorreo" id="an_reflab7_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_jefepuesto" id="an_reflab7_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab7_obligado" name="an_reflab7_separacion" id="an_reflab7_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab7_obligado" name="an_reflab7_cualidades" id="an_reflab7_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab7_obligado" name="an_reflab7_mejoras" id="an_reflab7_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab7_obligado" name="an_reflab7_comentarios" id="an_reflab7_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(7)">Guardar la verificación del Cuarto Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab7">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        <div class="box-header text-center tituloSubseccion">
                  			<p class="box-title " id="titulo_reflab1"><strong>  8. Octavo trabajo </strong><hr></p>
                		</div>
                		<div class="row">
                		<form id="candidato_reflaboral8">
	                		<div class="col-md-6">
	                			<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_empresa" id="reflab8_empresa" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_direccion" id="reflab8_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_entrada" id="reflab8_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_salida" id="reflab8_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros reflab8_obligado" name="reflab8_telefono" id="reflab8_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_puesto1" id="reflab8_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_puesto2" id="reflab8_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_salario1" id="reflab8_salario1" >
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_salario2" id="reflab8_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_jefenombre" id="reflab8_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_jefecorreo" id="reflab8_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato:</label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_jefepuesto" id="reflab8_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control reflab8_obligado" name="reflab8_separacion" id="reflab8_separacion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3 col-md-offset-4">
	                					<button type="button" class="btn btn-info" onclick="actualizarLaboral(8)">Guardar referencia laboral</button><br><br><br><br>
	                					<input type="hidden" id="idreflab8">
	                				</div>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="analista_reflaboral8">
	                		<div class="col-md-6">
	                			<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Compañía: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_empresa" id="an_reflab8_empresa" >
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Dirección: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_direccion" id="an_reflab8_direccion">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de entrada: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_entrada" id="an_reflab8_entrada">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Fecha de salida: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_salida" id="an_reflab8_salida">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Teléfono: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros ver_reflab8_obligado" name="an_reflab8_telefono" id="an_reflab8_telefono" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_puesto1" id="an_reflab8_puesto1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_puesto2" id="an_reflab8_puesto2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario inicial: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_salario1" id="an_reflab8_salario1">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Salario final: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_salario2" id="an_reflab8_salario2">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_jefenombre" id="an_reflab8_jefenombre">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Correo del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_jefecorreo" id="an_reflab8_jefecorreo">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Puesto del jefe inmediato: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_jefepuesto" id="an_reflab8_jefepuesto">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Causa de separación: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control ver_reflab8_obligado" name="an_reflab8_separacion" id="an_reflab8_separacion">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-6">
	                				<label>Fortalezas o cualidades del candidato *</label>
	                				<textarea class="form-control ver_reflab8_obligado" name="an_reflab8_cualidades" id="an_reflab8_cualidades" rows="2"></textarea><br><br>
	                			</div>
	                			<div class="col-md-6">
	                				<label>Áreas a mejorar del candidato *</label>
	                				<textarea class="form-control ver_reflab8_obligado" name="an_reflab8_mejoras" id="an_reflab8_mejoras" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label>Referencia *</label>
	                				<textarea class="form-control ver_reflab8_obligado" name="an_reflab8_comentarios" id="an_reflab8_comentarios" rows="2"></textarea><br><br>
	                			</div>
	                		</div>
            			</form>
            			<div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="verificarLaboral(8)">Guardar la verificación del Cuarto Trabajo</button>
				        		<br><br><input type="hidden" id="idverlab8">
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
				        
        			</div>
        		</div>
        	</div>
        </div>
	</section>
</div>
<!-- /.content-wrapper -->	
<script>
	var id = '<?php echo $this->uri->segment(3) ?>';
	var url = '<?php echo base_url('Cliente/getCandidatos?id='); ?>'+id;
	var psico = '<?php echo base_url(); ?>_psicometria/';
  	$(document).ready(function(){
  		//inputmask
  		$('#fecha_nacimiento').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_acta').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_domicilio').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_imss').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_curp').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_retencion').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_rfc').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_licencia').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_migra').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_visa').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$('#fecha_ine').inputmask('yyyy', { 'placeholder': 'yyyy' });
  		var msj = localStorage.getItem("success");
  		var msj2 = localStorage.getItem("finished");
  		var msj3 = localStorage.getItem("deleted");
  		if(msj == 1){
  			$("#texto_msj").text(" Los datos se han guardado correctamente");
  			$("#mensaje").css('display','block');
  			setTimeout(function(){
          		$('#mensaje').fadeOut();
        	},4000);
        	localStorage.removeItem("success");
  		}
  		if(msj2 == 1){
  			$("#exitoFinalizado").css('display','block');
  			setTimeout(function(){
          		$('#exitoFinalizado').fadeOut();
        	},4000);
        	localStorage.removeItem("finished");
  		}
  		if(msj3 == 1){
  			$("#texto_msj").text(" Candidato eliminado correctamente");
  			$("#mensaje").css('display','block');
  			setTimeout(function(){
          		$('#mensaje').fadeOut();
        	},4000);
        	localStorage.removeItem("deleted");
  		}
    	$('#tabla').DataTable({
      		"pageLength": 25,
	      	"pagingType": "simple",
	      	"order": [0, "desc"],
	      	"stateSave": true,
	      	"serverSide": false,
	      	"ajax": url,
	      	"columns":[ 
	      		{ title: 'id', data: 'id', visible: false },
	        	{ title: 'Candidato', data: 'candidato', "width": "15%",
	        		mRender: function(data, type, full){
	        			return '<a data-toggle="tooltip" class="sin_vinculo" title="'+full.id+'">'+data+'</a>';
	        		}
	        	},
	        	{ title: 'Subcliente', data: 'cliente', "width": "15%",
	        		mRender: function(data, type, full){
	        			var subcliente = (full.subcliente == null || full.subcliente == "")? "-":full.subcliente;
	        			return subcliente;
	        		}
	        	},
	        	{ title: 'Fechas', data: 'fecha_alta', "width": "13%",
	        		mRender: function(data, type, full){
	        			var f = data.split(' ');
            			var h = f[1];
            			var aux = h.split(':');
            			var hora = aux[0]+':'+aux[1];
            			var aux = f[0].split('-');
            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
            			var f_inicio = fecha+' '+hora;
            			if(full.fecha_final != null){
            				var f = full.fecha_final.split(' ');
            				var h = f[1];
	            			var aux = h.split(':');
	            			var hora = aux[0]+':'+aux[1];
	            			var aux = f[0].split('-');
	            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
	            			var f_fin = "Final: "+fecha+' '+hora;
	        				return "Alta: "+f_inicio+'<br>'+f_fin;
            			}
            			else{
            				var f = new Date();
							var hoy = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
							return "Alta: "+f_inicio+'<br>Final: -';
            			}
	        		}
	        	},
	        	{ title: 'Tiempo del proceso', data: 'tiempo', "width": "8%",
	        		mRender: function(data, type, full){
	        			if(data != null){
	        				if(data != -1){
	        					if(data >= 0 && data <= 2){
	            					return res = '<div class="formato_dias dias_verde">'+data+' días</div>';
	            				}
	            				if(data > 2 && data <= 4){
	            					return res = '<div class="formato_dias dias_amarillo">'+data+' días</div>';
	            				}
	            				if(data >= 5){
	            					return res = '<div class="formato_dias dias_rojo">'+data+' días</div>';
	            				}
	        				}
            				else{
            					return "Actualizando...";
            				}
            			}
            			else{
            				if(full.tiempo_parcial != 0){
            					var parcial = full.tiempo_parcial;
	            				if(parcial >= 0 && parcial < 2){
	            					return res = '<div class="formato_dias dias_verde">'+parcial+' días</div>';
	            				}
	            				if(parcial >= 2 && parcial <= 4){
	            					return res = '<div class="formato_dias dias_amarillo">'+parcial+' días</div>';
	            				}
	            				if(parcial >= 5){
	            					return res = '<div class="formato_dias dias_rojo">'+parcial+' días</div>';
	            				}
            				}
            				else{
            					return "Actualizando...";
            				}
            			}
          			}
	        	},
	        	{ title: 'Mensajes', data: 'id', bSortable: false, "width": "17%",
         			mRender: function(data, type, full) {
     					if(full.status == 0){
                			return '<a href="javascript:void(0)" data-toggle="tooltip"  title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip"  title="Llamadas al candidato" id="llamada" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Correos enviados al candidato" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
            			}
            			if(full.status == 1){
                			return '<a href="javascript:void(0)" data-toggle="tooltip"  title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a id="actualizar" href="javascript:void(0)" data-toggle="tooltip" title="Verificación de datos" class="fa-tooltip a-acciones"><i class="fas fa-edit"></i></a><a id="estudios" href="javascript:void(0)" data-toggle="tooltip" title="Verificación de estudios" class="fa-tooltip a-acciones"><i class="fas fa-graduation-cap"></i></a><a id="laborales" href="javascript:void(0)" data-toggle="tooltip" title="Verificación de referencias laborales" class="fa-tooltip a-acciones"><i class="fas fa-history"></i></a><a id="penales" href="javascript:void(0)" data-toggle="tooltip" title="Verificación de antecedentes no penales" class="fa-tooltip a-acciones"><i class="fas fa-gavel"></i></a>';
            			}
            			if(full.status == 2){
            				return '<a href="javascript:void(0)" data-toggle="tooltip"  title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip"  title="Llamadas al candidato" id="llamada" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Correos enviados al candidato" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
            			}
          			}
        		},
        		{ title: 'Analista', data: 'usuario', bSortable: false, "width": "8%",
         			mRender: function(data, type, full) {
         				if(data == null | data == ''){
         					return 'Sin definir';
         				}
         				else{
         					return data;
         				}
          			}
        		},
        		{ title: 'Estudio', data: 'id', bSortable: false, "width": "13%",
         			mRender: function(data, type, full) {
         				if(full.idFinalizado != null){
         					return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_academicos">Historial académico</a></li><li><a href="#" id="datos_sociales">Antecedentes sociales</a></li><li><a href="#" id="datos_ref_personales">Referencias personales</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li><a href="#" id="datos_no_mencionados">Trabajos no mencionados</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="conclusiones">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="actualizacion_candidato" style="background:khaki;">Actualizacion del candidato</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
         				}
         				else{
         					return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_academicos">Historial académico</a></li><li><a href="#" id="datos_sociales">Antecedentes sociales</a></li><li><a href="#" id="datos_ref_personales">Referencias personales</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li><a href="#" id="datos_no_mencionados">Trabajos no mencionados</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div> ';
         				}
          			}
        		},
        		{ title: 'Visita', data: 'visitador', "width": "10%",
	        		mRender: function(data, type, full){
	        			if(data == 1){
		        			return '<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Datos de la visita <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="visita_documentos">Documentos</a></li><li><a href="#" id="visita_familiares">Grupo familiar</a></li><li><a href="#" id="visita_egresos">Egresos mensuales</a></li><li><a href="#" id="visita_habitacion">Habitacion y medio ambiente</a></li><li><a href="#" id="visita_vecinales">Referencias vecinales</a></li></ul></div>';
		        		}
		        		else{
	        				return "<i class='fas fa-circle estatus0'></i>";
	        			}
          			}
				},
				{ title: 'Médico', data: 'id', width: '13%',
	        		mRender: function(data, type, full){
                        if(full.medico == 1){
                            if(full.imagen != null && full.conclusion != null){
								return '<div style="display: inline-block;"><form id="med'+full.idMedico+'" action="<?php echo base_url('Medico/crearPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfMedico" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idMedico" id="idMedico'+full.idMedico+'" value="'+full.idMedico+'"></form></div>';
                            }
                            else{
                                return "<i class='fas fa-circle status_bgc0'></i> En proceso";
                            }
                        }
                        else{
	        				return "N/A";
                        }
	        		}
				},
				{ title: 'Psicometrico', data: 'id', bSortable: false, "width": "13%",
         			mRender: function(data, type, full) {
         				if(full.psicometrico == 1){
         					if(full.archivo != null && full.archivo != ""){
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Subir psicometria" id="psicometria" class="fa-tooltip a-acciones"><i class="fas fa-brain"></i></a> <a href="'+psico+full.archivo+'" target="_blank" data-toggle="tooltip" title="Ver psicometria" id="descarga_psicometrico" class="fa-tooltip a-acciones"><i class="fas fa-file-powerpoint"></i></a>'; 
	         				}
	         				else{
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Subir psicometria" id="psicometria" class="fa-tooltip a-acciones"><i class="fas fa-brain"></i></a>';
	         				}
         				}
         				else{
         					return "N/A";
         				}
          			}
        		},
        		{ title: 'Antidoping', data: 'id', bSortable: false, "width": "13%",
         			mRender: function(data, type, full) {
         				if(full.tipo_antidoping == 1){
         					if(full.fecha_resultado != null && full.fecha_resultado != ""){
	         					if(full.resultado_doping == 1){
	         						return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;margin-left:10px;"><form id="pdfForm'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop'+full.idDoping+'" value="'+full.idDoping+'"></form></div>'; 
	         					}
	         					else{
	         						return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;margin-left:10px;"><form id="pdfForm'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop'+full.idDoping+'" value="'+full.idDoping+'"></form></div>';
	         					}
	         					
	         				}
	         				else{
	         					return "Pendiente";
	         				}
         				}
         				if(full.tipo_antidoping == 0 || full.tipo_antidoping == "" || full.tipo_antidoping == null){
         					return "N/A";
         				}
          			}
        		},
        		{ title: 'Resultado', data: 'id', bSortable: false, "width": "12%",
         			mRender: function(data, type, full) {
     					if(full.status == 0){
     						return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
     					}
     					else{
     						if(full.status_bgc == 0){
	         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
	         				}
	         				if(full.status_bgc == 1){
	         					return '<i class="fas fa-circle status_bgc1"></i><div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/crearEstudioTipo2PDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
	         				}
	         				if(full.status_bgc == 2){
	         					return '<i class="fas fa-circle status_bgc2"></i><div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/crearEstudioTipo2PDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
	         				}
	         				if(full.status_bgc == 3){
	         					return '<i class="fas fa-circle status_bgc3"></i><div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/crearEstudioTipo2PDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
	         				}
     					}
     				}
        		}   
	      	],
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$('a#datos_generales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
		        	$("#nombre_general").val(data.nombre);
		        	$("#paterno_general").val(data.paterno);
		        	$("#materno_general").val(data.materno);
		        	if(data.fecha_nacimiento != "" && data.fecha_nacimiento != null){
			        	var f_nacimiento = convertirDate(data.fecha_nacimiento);
			        	$("#fecha_nacimiento").val(f_nacimiento);
			        	$("#edad").val(data.edad);
		        	}
		        	else{
		        		$("#fecha_nacimiento").val("");
			        	$("#edad").val("");
		        	}
		        	$("#puesto_general").val(data.id_puesto);
		        	$("#nacionalidad").val(data.nacionalidad);
		        	$("#genero").val(data.genero);
		        	$("#calle").val(data.calle);
		        	$("#exterior").val(data.exterior);
		        	$("#interior").val(data.interior);
		        	$("#colonia").val(data.colonia);
		        	$("#calles").val(data.entre_calles);
		        	$("#estado").val(data.id_estado);
		        	if(data.id_estado != "" && data.id_estado != null && data.id_estado != 0){
		        		getMunicipio(data.id_estado, data.id_municipio);
		            }
		            else{
		              $('#municipio').prop('disabled', true);
		            }
		            $("#cp").val(data.cp);
		        	$("#civil").val(data.id_estado_civil);
		        	$("#celular_general").val(data.celular);
		        	$("#tel_casa").val(data.telefono_casa);
		        	$("#grado").val(data.id_grado_estudio);
		        	$("#tel_oficina").val(data.telefono_otro);
		        	$("#personales_correo").val(data.correo);
		        	$("#tiempo_dom_actual").val(data.tiempo_dom_actual);
		        	$("#tiempo_traslado").val(data.tiempo_traslado);
		        	$("#medio_transporte").val(data.tipo_transporte);
		        	$("#generalesModal").modal('show');
		        });
		        $('a#datos_academicos', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	              	$("#prim_periodo").val(data.primaria_periodo);
	              	$("#prim_escuela").val(data.primaria_escuela);
	              	$("#prim_ciudad").val(data.primaria_ciudad);
	              	$("#prim_certificado").val(data.primaria_certificado);
	              	$("#prim_promedio").val(data.primaria_promedio);
	              	$("#sec_periodo").val(data.secundaria_periodo);
	              	$("#sec_escuela").val(data.secundaria_escuela);
	              	$("#sec_ciudad").val(data.secundaria_ciudad);
	              	$("#sec_certificado").val(data.secundaria_certificado);
	              	$("#sec_promedio").val(data.secundaria_promedio);
	              	$("#prep_periodo").val(data.preparatoria_periodo);
	              	$("#prep_escuela").val(data.preparatoria_escuela);
	              	$("#prep_ciudad").val(data.preparatoria_ciudad);
	              	$("#prep_certificado").val(data.preparatoria_certificado);
	              	$("#prep_promedio").val(data.preparatoria_promedio);
	              	$("#lic_periodo").val(data.licenciatura_periodo);
	              	$("#lic_escuela").val(data.licenciatura_escuela);
	              	$("#lic_ciudad").val(data.licenciatura_ciudad);
	              	$("#lic_certificado").val(data.licenciatura_certificado);
	              	$("#lic_promedio").val(data.licenciatura_promedio);
	              	$("#otro_certificado").val(data.otros_certificados);
	              	$("#estudios_comentarios").val(data.comentarios);
	              	$("#carrera_inactivo").val(data.carrera_inactivo);
		        	$("#academicosModal").modal('show');
		        });
		        $('a#datos_sociales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	              	$("#religion").val(data.religion);
	              	$("#religion_frecuencia").val(data.religion_frecuencia);
	              	$("#bebidas").val(data.bebidas);
	              	$("#bebidas_frecuencia").val(data.bebidas_frecuencia);
	              	$("#fumar").val(data.fumar);
	              	$("#fumar_frecuencia").val(data.fumar_frecuencia);
	              	$("#cirugia").val(data.cirugia);
	              	$("#enfermedades").val(data.enfermedades);
	              	$("#corto_plazo").val(data.corto_plazo);
	              	$("#mediano_plazo").val(data.mediano_plazo);
		        	$("#socialesModal").modal('show');
		        });
		        $('a#datos_ref_personales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#d_refpersonal1")[0].reset();
	      			$("#d_refpersonal2")[0].reset();
	              	$.ajax({
	                	url: '<?php echo base_url('Candidato/getRefPersonales'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != 0){
	                			var rows = res.split('###');
	                			for(var i = 0; i < rows.length; i++){
	                				if(rows[i] != ""){
	                					var dato = rows[i].split('@@');
			                			$("#refPer"+(i+1)+"_nombre").val(dato[0]);
			                			$("#refPer"+(i+1)+"_telefono").val(dato[1]);
			                			$("#refPer"+(i+1)+"_tiempo").val(dato[2]);
			                			$("#refPer"+(i+1)+"_lugar").val(dato[3]);
			                			$("#refPer"+(i+1)+"_trabaja").val(dato[4]);
			                			$("#refPer"+(i+1)+"_vive").val(dato[5]);
			                			$("#refPer"+(i+1)+"_recomienda").val(dato[6]);
			                			$("#refPer"+(i+1)+"_comentario").val(dato[7]);
			                			$("#refPer"+(i+1)+"_id").val(dato[8]);
	                				}
	                			}
	                		}	                		
	                	}
	              	});
		        	$("#refPersonalesModal").modal('show');
		        });
		        $('a#datos_laborales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#candidato_reflaboral1")[0].reset();
	      			$("#analista_reflaboral1")[0].reset();
	      			$("#candidato_reflaboral2")[0].reset();
	      			$("#analista_reflaboral2")[0].reset();
	      			$("#candidato_reflaboral3")[0].reset();
	      			$("#analista_reflaboral3")[0].reset();
	      			$("#candidato_reflaboral4")[0].reset();
	      			$("#analista_reflaboral4")[0].reset();
		        	$("#trabajo_gobierno").val(data.trabajo_gobierno);
		        	$("#trabajo_enterado").val(data.trabajo_enterado);
		        	$.ajax({
		        		async: false,
	                	url: '<?php echo base_url('Candidato/getRefLaborales'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != ""){
	                			var rows = res.split('###');
		                		for($i = 0; $i < rows.length; $i++){
		                			if(rows[$i] != ""){
				                		var dato = rows[$i].split('@@');
				                  		$("#reflab"+($i+1)+"_empresa").val(dato[0]);
				                  		$("#reflab"+($i+1)+"_direccion").val(dato[1]);
				                  		$("#reflab"+($i+1)+"_entrada").val(dato[2]);
				                  		$("#reflab"+($i+1)+"_salida").val(dato[3]);
				                  		$("#reflab"+($i+1)+"_telefono").val(dato[4]);
				                  		$("#reflab"+($i+1)+"_puesto1").val(dato[5]);
				                  		$("#reflab"+($i+1)+"_puesto2").val(dato[6]);
				                  		$("#reflab"+($i+1)+"_salario1").val(dato[7]);
				                  		$("#reflab"+($i+1)+"_salario2").val(dato[8]);
				                  		$("#reflab"+($i+1)+"_jefenombre").val(dato[9]);
				                  		$("#reflab"+($i+1)+"_jefecorreo").val(dato[10]);
				                  		$("#reflab"+($i+1)+"_jefepuesto").val(dato[11]);
				                  		$("#reflab"+($i+1)+"_separacion").val(dato[12]);
				                  		$("#idreflab"+($i+1)).val(dato[13]);
			                		}
		                		}
	                		}
	                  		
	                	}
	              	});
	              	$.ajax({
		        		async: false,
	                	url: '<?php echo base_url('Candidato/getVerificacionRefLaborales'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != ""){
	                			var rows = res.split('###');
		                		for($i = 0; $i < rows.length; $i++){
		                			if(rows[$i] != ""){
				                		var dato = rows[$i].split('@@');
				                  		$("#an_reflab"+($i+1)+"_empresa").val(dato[0]);
				                  		$("#an_reflab"+($i+1)+"_direccion").val(dato[1]);
				                  		$("#an_reflab"+($i+1)+"_entrada").val(dato[2]);
				                  		$("#an_reflab"+($i+1)+"_salida").val(dato[3]);
				                  		$("#an_reflab"+($i+1)+"_telefono").val(dato[4]);
				                  		$("#an_reflab"+($i+1)+"_puesto1").val(dato[5]);
				                  		$("#an_reflab"+($i+1)+"_puesto2").val(dato[6]);
				                  		$("#an_reflab"+($i+1)+"_salario1").val(dato[7]);
				                  		$("#an_reflab"+($i+1)+"_salario2").val(dato[8]);
				                  		$("#an_reflab"+($i+1)+"_jefenombre").val(dato[9]);
				                  		$("#an_reflab"+($i+1)+"_jefecorreo").val(dato[10]);
				                  		$("#an_reflab"+($i+1)+"_jefepuesto").val(dato[11]);
				                  		$("#an_reflab"+($i+1)+"_separacion").val(dato[12]);
				                  		$("#an_reflab"+($i+1)+"_comentarios").val(dato[13]);
				                  		$("#an_reflab"+($i+1)+"_cualidades").val(dato[14]);
				                  		$("#an_reflab"+($i+1)+"_mejoras").val(dato[15]);
				                  		$("#idverlab"+($i+1)).val(dato[16]);
			                		}
		                		}
	                		}
	                  		
	                	}
	              	});
	              	$("#nombreCliente").text(data.cliente);
		        	$("#listado").css('display','none');
			        $("#btn_nuevo").css('display','none');
			        $("#formulario").css('display','block');
			        $("#btn_regresar").css('display','block'); 
		        });
		        $('a#datos_no_mencionados', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#d_no_mencionados")[0].reset();
	      			$("#no_mencionados").val(data.no_mencionados);
	              	$("#resultado_no_mencionados").val(data.resultado_no_mencionados);
	              	$("#notas_no_mencionados").val(data.notas_no_mencionados);
		        	$("#noMencionadosModal").modal('show');
		        });
		        $('a#visita_documentos', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#d_documentos")[0].reset();

		        	$("#imss1").val(data.ver_imss);
		        	$("#imss2").val(data.imss_institucion);
		        	$("#dom1").val(data.domicilio);
		        	$("#dom2").val(data.fecha_domicilio);
		        	$("#ine1").val(data.ver_ine);
		        	$("#ine2").val(data.ine_institucion);
		        	$("#curp1").val(data.ver_curp);
		        	$("#curp2").val(data.curp_institucion);
		        	$("#rfc1").val(data.ver_rfc);
		        	$("#rfc2").val(data.rfc_institucion);
		        	$("#lic1").val(data.ver_licencia);
		        	$("#lic2").val(data.licencia_institucion);
		        	$("#carta1").val(data.carta_recomendacion);
		        	$("#carta2").val(data.carta_recomendacion_institucion);
		        	$("#comentarios_documentos").val(data.ver_comentarios);
		        	$("#visitaDocumentosModal").modal('show');
		        });
		        $('a#visita_familiares', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#div_familiares").empty();
	              	$.ajax({
	                	url: '<?php echo base_url('Candidato/getGrupoFamiliar'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                  		if(res != 0){
	                  			$("#div_familiares").empty();
	                  			$("#div_familiares").append(res)

	                  		}
	                  		else{
	                  			$("#div_familiares").empty();
	                  			$("#div_familiares").append('<p class="text-center">Sin registros</p>');
	                  		}
	                	}
	              	});
		        	$("#familiaresModal").modal('show');
		        });
		        $('a#visita_egresos', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#d_egresos")[0].reset();
	      			$.ajax({
	                	url: '<?php echo base_url('Candidato/getEgresos'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != 0){
	                			var dato = res.split('@@');
	                			$("#renta").val(dato[0]);
	                			$("#alimentos").val(dato[1]);
	                			$("#servicios").val(dato[2]);
	                			$("#transportes").val(dato[3]);
	                			$("#otros_gastos").val(dato[4]);
	                			$("#solvencia").val(dato[5]);
	                		}	                		
	                	}
	              	});
	              	$("#egresosModal").modal('show');
		        });
		        $('a#visita_habitacion', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#d_habitacion")[0].reset();

		        	$("#tiempo_residencia").val(data.tiempo_residencia);
        			$("#nivel_zona").val(data.id_tipo_nivel_zona);
        			$("#tipo_vivienda").val(data.id_tipo_vivienda);
        			$("#recamaras").val(data.recamaras);
        			$("#banios").val(data.banios);
        			$("#distribucion").val(data.distribucion);
        			$("#calidad_mobiliario").val(data.calidad_mobiliario);
        			$("#mobiliario").val(data.mobiliario);
        			$("#tamanio_vivienda").val(data.tamanio_vivienda);
        			$("#condiciones_vivienda").val(data.id_tipo_condiciones);
		        	$("#visitaHabitacionModal").modal('show');
		        });
		        $('a#visita_vecinales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#d_refVecinal1")[0].reset();
	              	$.ajax({
	                	url: '<?php echo base_url('Candidato/getVecinales'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != 0){
	                			var rows = res.split('###');
	                			for(var i = 0; i < rows.length; i++){
	                				if(rows[i] != ""){
	                					var dato = rows[i].split('@@');
			                			$("#vecino"+(i+1)+"_nombre").val(dato[0]);
			                			$("#vecino"+(i+1)+"_tel").val(dato[1]);
			                			$("#vecino"+(i+1)+"_domicilio").val(dato[2]);
			                			$("#vecino"+(i+1)+"_concepto").val(dato[3]);
			                			$("#vecino"+(i+1)+"_familia").val(dato[4]);
			                			$("#vecino"+(i+1)+"_civil").val(dato[5]);
			                			$("#vecino"+(i+1)+"_hijos").val(dato[6]);
			                			$("#vecino"+(i+1)+"_sabetrabaja").val(dato[7]);
			                			$("#vecino"+(i+1)+"_notas").val(dato[8]);
			                			$("#idrefvec"+(i+1)).val(dato[9]);
	                				}
	                			}
	                		}	                   		
	                	}
	              	});
		        	$("#refVecinalesModal").modal('show');
		        });
		        $('a#conclusiones', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#idFinalizado").val(data.idFinalizado);
	              	$("#personal1").val(data.descripcion_personal1);
	      			$("#personal2").val(data.descripcion_personal2);
	      			$("#personal3").val(data.descripcion_personal3);
	      			$("#personal4").val(data.descripcion_personal4);
	      			$("#socio1").val(data.descripcion_socio1);
	      			$("#socio2").val(data.descripcion_socio2);
	      			$("#laboral1").val(data.descripcion_laboral1);
	      			$("#laboral2").val(data.descripcion_laboral2);
	      			$("#visita1").val(data.descripcion_visita1);
	      			$("#visita2").val(data.descripcion_visita2);
	      			$("#recomendable").val(data.recomendable);
	      			$("#completarModal").modal('show');
		        });
		        $('a#actualizacion_candidato', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$("#idDoping").val(data.idDoping);
		        	
		        	$("#actualizarCandidatoModal").modal('show');
		        });
	      		$('a#solicitudes', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/viewSolicitudesCandidato'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != ""){
			            		$("#titulo_accion").text("Requisición del candidato");
				            	$("#nombre_candidato").html("<b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b>');
				            	$("#motivo").html(res);
				            	$("#verModal").modal('show');
			            	}
			            	else{
			            		$("#titulo_accion").text("Requisiciones del candidato");
				            	$("#motivo").html("No hay registro de requisición");
				            	$("#verModal").modal('show');
			            	}
			            	

			            }
			        });
		        });
	      		$("a#subirDocs", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".prefijo").val(data.id+"_"+data.nombre+""+data.paterno);
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/getDocs'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'prefijo':data.id+"_"+data.nombre+""+data.paterno},
			            success : function(res)
			            { 
			            	$("#tablaDocs").html(res);

			            }
			        });
	      			$("#docsModal").modal("show");
	      		});		        
	      		$("a#llamada", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#llamada_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusLlamadas();
	      		});
	      		$("a#msj_avances", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#avances_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusAvances();
	      		});
	      		$("a#correoEnviado", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#email_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusEmails();
	      		});
	      		$("a#final", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			var conclusiones = $.ajax({
                            url: '<?php echo base_url('Candidato/checkConclusionesCandidato'); ?>',
                            type: 'post',
                            async: false, 
                            data: {'id_candidato':data.id},
                            success : function(res){ }
                          }).responseText;
	      			if(conclusiones != 1){
	      				var refs_comentarios = $.ajax({
                            url: '<?php echo base_url('Candidato/getComentariosRefPersonales'); ?>',
                            type: 'post',
                            async: false, 
                            data: {'id_candidato':data.id},
                            success : function(res){ }
                          }).responseText;
		      			var trabajos = $.ajax({
	                            url: '<?php echo base_url('Candidato/countReferenciasLaborales'); ?>',
	                            type: 'post',
	                            async: false, 
	                            data: {'id_candidato':data.id},
	                            success : function(res){ }
	                          }).responseText;
		      			var comentarios_laborales = $.ajax({
	                            url: '<?php echo base_url('Candidato/getComentariosRefLaborales'); ?>',
	                            type: 'post',
	                            async: false, 
	                            data: {'id_candidato':data.id},
	                            success : function(res){ }
	                          }).responseText;
		      			var vecinales = $.ajax({
	                            url: '<?php echo base_url('Candidato/getComentariosRefVecinales'); ?>',
	                            type: 'post',
	                            async: false, 
	                            data: {'id_candidato':data.id},
	                            success : function(res){ }
	                          }).responseText;
		      			var bebidas = (data.bebidas == 1)? "ingerir":"no ingerir";
		      			var fuma = (data.fumar == 1)? "Fuma "+data.fumar_frecuencia+".":"No fuma.";
		      			switch(data.calidad_mobiliario){
		      				case '1': var calidad = "Buena";break;
		      				case '2': var calidad = "Regular";break;
		      				case '3': var calidad = "Mala";break;
		      			}
		      			switch(data.tamanio_vivienda){
		      				case '1': var tamano = "Amplia";break;
		      				case '2': var tamano = "Suficiente";break;
		      				case '3': var tamano = "Reducidad";break;
		      			}
		      			if(data.religion != "" && data.religion != "Ninguna" && data.religion != "NINGUNA" && data.religion != "No" && data.religion != "NO" && data.religion != "NA" && data.religion != "N/A" && data.religion != "No aplica" && data.religion != "NO APLICA" && data.religion != "No Aplica"){
		      				var religion = "profesa la religion "+data.religion+".";
		      			}
		      			else{
		      				var religion = "no profesa alguna religión.";
		      			}
		      			if(data.cirugia != "" && data.cirugia != "Ninguna" && data.cirugia != "NINGUNA" && data.cirugia != "No" && data.cirugia != "NO" && data.cirugia != "NA" && data.cirugia != "N/A" && data.cirugia != "No aplica" && data.cirugia != "NO APLICA" && data.cirugia != "No Aplica" && data.cirugia != "0"){
		      				var cirugia = "Cuenta con cirugia(s) de "+data.cirugia+".";
		      			}
		      			else{
		      				var cirugia = "No cuenta con cirugias.";
		      			}
		      			if(data.enfermedades != "" && data.enfermedades != "Ninguna" && data.enfermedades != "NINGUNA" && data.enfermedades != "No" && data.enfermedades != "NO" && data.enfermedades != "NA" && data.enfermedades != "N/A" && data.enfermedades != "No aplica" && data.enfermedades != "NO APLICA" && data.enfermedades != "No Aplica" && data.enfermedades != "0"){
		      				var enfermedades = "Tiene alguna(s) enfermedad(es) con antecedente familiar como "+data.enfermedades+".";
		      			}
		      			else{
		      				var enfermedades = "No tiene antecedentes de enfermedadades en su familia.";
		      			}
		      			
		      			var adeudo = (data.adeudo_muebles == 1)? "con adeudo":"sin adeudo";

		      			$("#personal1").val(data.candidato+", de "+data.edad+" años, originario(a) de "+data.municipio+", "+data.estado+", es "+data.civil+" y "+religion);
		      			$("#personal2").val("Refiere "+bebidas+" bebidas alcohólicas. "+fuma+" "+cirugia+" "+enfermedades+" Sus referencias personales lo describen como "+refs_comentarios+".");
		      			$("#personal3").val("Su plan a corto plazo es "+data.corto_plazo+"; y su meta a mediano plazo es "+data.mediano_plazo);
		      			$("#personal4").val("Su grado máximo de estudios es "+data.grado);
		      			$("#socio1").val("Actualmente vive en un/una "+data.vivienda+", con un tiempo de residencia de "+data.tiempo_residencia+". El nivel de la zona es "+data.zona+", el mobiliario es de calidad "+calidad+", la vivienda es "+tamano+" y en condiciones "+data.condiciones+". La distribución de su "+data.vivienda+" es "+data.distribucion);
		      			$("#socio2").val(data.candidato+" declara en sus ingresos "+data.ingresos+". Los gastos generados en el hogar son solventados por _____. Cuenta con "+data.muebles+" "+adeudo+".");
		      			$("#laboral1").val("Señaló "+trabajos+" referencias laborales");
		      			$("#laboral2").val(comentarios_laborales);
		      			$("#visita1").val("El candidato durante la visita: "+data.visita_comentarios);
		      			$("#visita2").val("De acuerdo a la referencia vecinal, el candidato es considerado: "+vecinales);
		      			$("#completarModal").modal('show');
	      			}
	      		});
	      		$('a[id^=pdfFinal]', row).bind('click', () => {
		            var id = data.id;
		            $('#pdf'+id).submit();
		        });
		        $('a[id^=pdfDoping]', row).bind('click', () => {
		            var id = data.idDoping;
		            $('#pdfForm'+id).submit();
				});
				$('a[id^=pdfMedico]', row).bind('click', () => {
		            var id = data.idMedico;
		            $('#med'+id).submit();
		        });
		        $("a#eliminar", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#idCliente").val(data.id_cliente);
	      			$("#titulo_accion").text("Cancelar proceso");
	      			$("#texto_confirmacion").html("¿Estás seguro de cancelar el proceso de <b>"+data.nombre+"</b>?");
	      			$("#div_motivo").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
		        $('a#verCancelado', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/getCancelacion'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	datos = res.split('##');
			            	var fecha = new Date(datos[0]);
							var options = { year: 'numeric', month: 'long', day: 'numeric' };
			            	$("#titulo_accion").text("Cancelado");
			            	$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue cancelado el '+fecha.toLocaleDateString("es-ES", options));
			            	$("#motivo").html("Comentario: <br>"+datos[1]);
			            	$("#verModal").modal('show');

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
		        $('a#verEliminado', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/getEliminacion'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	datos = res.split('##');
			            	var fecha = new Date(datos[0]);
							var options = { year: 'numeric', month: 'long', day: 'numeric' };
			            	$("#titulo_accion").text("Eliminado");
			            	$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue eliminado el '+fecha.toLocaleDateString("es-ES", options));
			            	$("#motivo").html("Comentario: <br>"+datos[1]);
			            	$("#verModal").modal('show');

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
	      		$('a#comentario', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/viewComentario'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
			            		$("#titulo_accion").text("Comentario del candidato");
				            	//$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue cancelado el '+fecha.toLocaleDateString("es-ES", options));
				            	$("#motivo").html(res);
				            	$("#verModal").modal('show');
			            	}
			            	else{
			            		$("#titulo_accion").text("Comentarios del candidato");
				            	$("#motivo").html("No hay registro de comentarios");
				            	$("#verModal").modal('show');
			            	}
			            	

			            },error: function(res)
			            {
			              
			            }
			        });
				});
				$("a#psicometria", row).bind('click', () => {
					$("#idPsicometrico").val(data.idPsicometrico);
	      			$("#idCandidato").val(data.id);
                    $("#psicometria_candidato").html("Candidato <b>"+data.candidato+"</b>");
                    $("#psicometriaModal").modal("show");
				});
      		},
	      	"language": {
	        	"lengthMenu": "Mostrar _MENU_ registros por página",
	        	"zeroRecords": "No se encontraron registros",
	        	"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        	"infoEmpty": "Sin registros disponibles",
	        	"infoFiltered": "(Filtrado _MAX_ registros totales)",
	        	"sSearch": "Buscar:",
	        	"oPaginate": {
	          		"sLast": "Última página",
	          		"sFirst": "Primera",
	          		"sNext": "<i class='fa  fa-arrow-right'></i>",
	          		"sPrevious": "<i class='fa fa-arrow-left'></i>"
	        	}
	      	}
    	}); 
    	//$('#tabla').DataTable().search(" ");
    	$("#btn_eliminados").click(function(){
			var id_cliente = '<?php echo $this->uri->segment(3) ?>';
			$.ajax({
              	url: '<?php echo base_url('Candidato/getCandidatosEliminados'); ?>',
              	type: 'POST',
              	data: {'id_cliente':id_cliente},
              	success : function(res){ 
              		$("#div_listado").html(res);
              		$("#eliminadosModal").modal("show");
              	}
        	});
		});
		$("#guardarGenerales").click(function(){
			var d_generales = $("#d_generales").serialize();
			d_generales += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.personal_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".personal_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_generales').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'datos_generales-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/actualizarDatosGenerales'); ?>',
		        	method: "POST",  
		            data: {'d_generales':d_generales},
		            beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#generalesModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarDocumentacion").click(function(e){
   			e.preventDefault();
			var d_documentos = $("#d_documentos").serialize();
			d_documentos += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.docs_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".docs_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_documentos').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'visita_documentacion-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/actualizarDocumentacionVisitaTipo2'); ?>',
		        	method: "POST",  
		            data: {'d_documentos':d_documentos},
		            beforeSend: function(){
		                $('.loader').css("display","block");
		            },
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#visitaDocumentosModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarEstudios").click(function(){
			var d_estudios = $("#d_estudios").serialize();
			d_estudios += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.estudios_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".estudios_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_estudios').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'historial_academico-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/actualizarHistorialAcademico'); ?>',
		        	method: "POST",  
		            data: {'d_estudios':d_estudios},
		            beforeSend: function(){
		                $('.loader').css("display","block");
		            },
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#academicosModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarSociales").click(function(){
			var d_sociales = $("#d_sociales").serialize();
			d_sociales += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.social_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".social_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_sociales').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'antecedentes_sociales-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/actualizarAntecendentesSociales'); ?>',
		        	method: "POST",  
		            data: {'d_sociales':d_sociales},
		            beforeSend: function(){
		                $('.loader').css("display","block");
		            },
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#socialesModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#antidoping").change(function(){
			if(this.checked){
				var id_subcliente = $("#subcliente").val();
				var id_cliente = '<?php echo $this->uri->segment(3) ?>';
				if(id_subcliente != ""){
		        	$.ajax({
		          		url: '<?php echo base_url('Doping/getPaqueteSubcliente'); ?>',
		          		method: 'POST',
		          		data: {'id_subcliente':id_subcliente,'id_cliente':id_cliente},
		          		success: function(res)
		          		{
		            		if(res != ""){
		          				$('#examen').val(res);
		          				$("#examen").prop('disabled',false);
								$("#examen").addClass('obligado');
		          			}
		          			else{
		          				$('#examen').val('');
		          				$("#examen").prop('disabled',false);
								$("#examen").addClass('obligado');
		          			}
		          		}
		        	});
		      	}
			}
			else{
				$("#examen").prop('disabled',true);
				$("#examen").val("");
				$("#examen").removeClass('obligado');
			}
		});
	  	$("#estado").change(function(){
	      	var id_estado = $(this).val();
	      	if(id_estado != ""){
	        	$.ajax({
	          		url: '<?php echo base_url('Alumno/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#municipio').prop('disabled', false);
	            		$('#municipio').html(res);
	          		}
	        	});
	      	}
	      	else{
        		$('#municipio').prop('disabled', true);
	        	$('#municipio').append($("<option selected></option>").attr("value","").text("Selecciona"));
	      	}
	    });
	  	$("#completarModal").on("hidden.bs.modal", function(){
	    	$("select, textarea").removeClass("requerido");
	    	$("#campos_vacios_check").css('display','none');
	    });
	    $("#psicometriaModal").on("hidden.bs.modal", function(){
	    	$("input").val("");
	    	$("input").removeClass("requerido");
	    	$("#psicometriaModal #campos_vacios").css('display','none');
	    });
  		$("#visitaModal").on("hidden.bs.modal", function(){
	    	$("input, select").val("");
	    });
	    $('[data-toggle="tooltip"]').tooltip();
	    $("#newModal").on("hidden.bs.modal", function(){
	      	$("#newModal input, select, .div_estudio").removeClass("requerido");
	      	$("#newModal select, textarea").val("");
	      	$("#subcliente, #puesto, #nombre, #paterno, #materno, #correo, #celular, #fijo, #cv").val("");
	      	$("#antidoping").prop("checked",false);
	      	$("#psicometrico").prop("checked",false);
	      	$("#medico").prop("checked",false);
	      	$("#buro").prop("checked",false);
	      	$("#laboral").prop("checked",false);
	      	$("#newModal #campos_vacios").css('display','none');
	      	$("#newModal #repetido").css('display','none');
	      	$("#newModal #alert-msg").css('display','none');
	      	$("#newModal #correo_invalido").css('display','none');
	      	//$("#puesto").val(12);
	    });
	  });
	function registrar(){
		var id_cliente = '<?php echo $this->uri->segment(3) ?>';
		var correo = $('#correo').val();
		var datos = new FormData();
		datos.append('usuario', 1);
		datos.append('subcliente', $("#subcliente").val());
		datos.append('nombre', $("#nombre").val());
		datos.append('paterno', $("#paterno").val());
		datos.append('materno', $("#materno").val());
		datos.append('correo', $("#correo").val());
		datos.append('celular', $("#celular").val());
		datos.append('fijo', $("#fijo").val());
		datos.append('puesto', $("#puesto").val());
		datos.append('examen', $("#examen").val());
		datos.append('otro', $("#otro_requisito").val());
		datos.append('id_cliente', id_cliente);
		//datos.append('cv', $("#cv")[0].files[0]);
		var num_files = document.getElementById('cv').files.length;
		if(num_files > 0){
			datos.append("hay_cvs", 1);
			for(var x = 0; x < num_files; x++) {
				datos.append("cvs[]", document.getElementById('cv').files[x]);
			}
		}
		else{
			datos.append("hay_cvs", 0);
		}
		//datos.append('socio', $("input:checkbox[id='socio']:checked").val());
		datos.append('medico', $("input:checkbox[id='medico']:checked").val());
		datos.append('laboral', $("input:checkbox[id='laboral']:checked").val());
		datos.append('antidoping', $("input:checkbox[id='antidoping']:checked").val());
		datos.append('psicometrico', $("input:checkbox[id='psicometrico']:checked").val());
		datos.append('buro', $("input:checkbox[id='buro']:checked").val());

		var totalVacios = $('.obligado').filter(function(){
			return !$(this).val();
		}).length;

		if(totalVacios > 0){
			$(".obligado").each(function() {
				var element = $(this);
				if (element.val() == "") {
					element.addClass("requerido");
					$("#newModal #campos_vacios").css('display','block');
					setTimeout(function(){
						$('#newModal #campos_vacios').fadeOut();
					},4000);
				}
				else{
					element.removeClass("requerido");
				}
			});
		}
		else{
			$.ajax({
				url: '<?php echo base_url('Candidato/registrarCandidatoEspanol'); ?>',
				type: 'POST',
				data: datos,
				contentType: false,  
				cache: false,  
				processData:false,
				beforeSend: function() {
					$('.loader').css("display","block");
				},
				success : function(res){ 
					if(res == 1){
						setTimeout(function(){
							$('.loader').fadeOut();
						},300);
						$("#newModal").modal('hide');
						$('#newModal #correo_invalido').css("display", "none");
						$("#texto_msj").text(" El candidato ha sido registrado correctamente");
						$("#mensaje").css('display','block');
						recargarTable();
						setTimeout(function(){
							$("#mensaje").fadeOut();
						},6000);
						/*$('#repetido').css("display", "block");
						setTimeout(function(){
							$("#repetido").fadeOut();
						},4000);*/
					}
					if(res == 0){
						setTimeout(function(){
							$('.loader').fadeOut();
						},300);
						//$("#newModal").modal('hide');
						$('#newModal #campos_vacios').css("display", "none");
						$('#newModal #correo_invalido').css("display", "none");
						$("#newModal #repetido .msj_error").css('color','white');
						$("#newModal #repetido").css("display", "block");
						//recargarTable();
						setTimeout(function(){
							$("#newModal #repetido").fadeOut();
						},6000);
						/*$('#repetido').css("display", "block");
						setTimeout(function(){
							$("#repetido").fadeOut();
						},4000);*/
					}
					if(res != 0 && res != 1){
						setTimeout(function(){
							$('.loader').fadeOut();
						},200);
						$('#alert-msg').html('<div class="alert alert-danger">' + res + '</div>');              			
					}
					
				}
			});
		}
	}
	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	function eliminarArchivo(fila,idDoc,archivo){
		$("#fila"+fila).remove();
		$.ajax({
      		url: '<?php echo base_url('Candidato/eliminarDocCandidato'); ?>',
      		method: 'POST',
      		data: {'idDoc':idDoc,'archivo':archivo},
      		dataType: "text",
      		success: function(res)
      		{
      			
      		}
    	});
	}
	function estatusAvances(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_avances").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
			url: '<?php echo base_url('Candidato/checkAvances'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_avances").css('display','none');
        			}
        			$("#div_crearEstatusAvances").empty();
        			$("#idAvances").val(aux[1]);
        			$("#div_crearEstatusAvances").append(aux[0]);
        		}
        		else{
        			$("#idAvances").val(0);
        			$("#div_crearEstatusAvances").empty();
        			$("#div_crearEstatusAvances").html('<p class="text-center">Sin registros </p>');
        		}

      		}
    	});
		$("#avancesModal").modal("show");
	}
	function generarEstatusAvance(){
		var datos = new FormData();
		datos.append('id_candidato', $("#idCandidato").val());
		datos.append('id_avance', $("#idAvances").val());
		datos.append('comentario', $("#avances_estatus_comentario").val());
		datos.append('adjunto', $("#adjunto")[0].files[0]);
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusAvance'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,  
			cache: false,  
			processData:false,
      		success: function(res)
      		{
				$("#adjunto").val("");
      			var aux = res.split('@@');
      			$("#idAvances").val(aux[1]);
        		$("#avances_estatus_comentario").val("");
        		$("#div_crearEstatusAvances").empty();
        		$("#div_crearEstatusAvances").append(aux[0]);
      		}
    	});
	}
	function estatusLlamadas(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_llamadas").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkLlamadas'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_llamadas").css('display','none');
        			}
        			$("#div_crearEstatusLlamadas").empty();
        			$("#idLlamadas").val(aux[1]);
        			$("#div_crearEstatusLlamadas").append(aux[0]);
        		}
        		else{
        			$("#idLlamadas").val(0);
        			$("#div_crearEstatusLlamadas").empty();
        			$("#div_crearEstatusLlamadas").html('<p class="text-center">Sin registros </p>');
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#llamadasModal").modal("show");
	}
	function generarEstatusLlamada(){
		var id_candidato = $("#idCandidato").val();
		var id_llamada = $("#idLlamadas").val();
		var comentario = $("#llamadas_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createEstatusLlamada'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_llamada':id_llamada,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idLlamadas").val(aux[1]);
        		$("#llamadas_estatus_comentario").val("");
        		$("#div_crearEstatusLlamadas").empty();
        		$("#div_crearEstatusLlamadas").append(aux[0]);
      		}
    	});
	}
	function estatusEmails(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_emails").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkEmails'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_emails").css('display','none');
        			}
        			$("#div_crearEstatusEmail").empty();
        			$("#idEmails").val(aux[1]);
        			$("#div_crearEstatusEmail").append(aux[0]);
        		}
        		else{
        			$("#idEmails").val(0);
        			$("#div_crearEstatusEmail").empty();
        			$("#div_crearEstatusEmail").html('<p class="text-center">Sin registros </p>');
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#emailsModal").modal("show");
	}
	function generarEstatusEmail(){
		var id_candidato = $("#idCandidato").val();
		var id_email = $("#idEmails").val();
		var comentario = $("#emails_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createEstatusEmail'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_email':id_email,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idEmails").val(aux[1]);
        		$("#emails_estatus_comentario").val("");
        		$("#div_crearEstatusEmail").empty();
        		$("#div_crearEstatusEmail").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	function guardarPersona(id_persona,num_persona,idCandidato){
		var nombre = $("#p"+num_persona+"_nombre").val();
		var empresa = $("#p"+num_persona+"_empresa").val();
		var puesto = $("#p"+num_persona+"_puesto").val();
		var antiguedad = $("#p"+num_persona+"_antiguedad").val();
		var muebles = $("#p"+num_persona+"_muebles").val();

		var totalPer = $('.visita_p_obligado_'+num_persona).filter(function(){
      		return !$(this).val();
    	}).length;
    	if(totalPer > 0){
	      	$(".visita_p_obligado_"+num_persona).each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},4000);
		          	
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	   	else{
	   		formdata = $("#d_familiar"+num_persona).serialize();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'grupo_familiar_'+num_persona+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/actualizarFamiliar'); ?>',
	        	method: "POST",  
	            data: {'id_persona':id_persona,'nombre':nombre,'empresa':empresa,'puesto':puesto,'antiguedad':antiguedad,'muebles':muebles},
	        	success: function(res){
	        		if(res == 1){
	        			$("#texto_msj").text('Los datos de la persona se han actualizado correctamente');
		        		$("#mensaje").css('display','block');
		        		setTimeout(function(){
		              		$('#mensaje').fadeOut();
		            	},6000);
	        		}
	        	}
	      	});
	    }
	}
	function guardarPersonales(num){
		var d_refPersonal = $("#d_refpersonal"+num).serialize();
		d_refPersonal += "&id_candidato="+$("#idCandidato").val();
		d_refPersonal += "&id_refper="+$("#refPer"+num+"_id").val();
		var totalPer = $('.refPer'+num+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(totalPer > 0){
	      	$('.refPer'+num+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en la sección de referencia personal #"+num+".");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#d_refpersonal"+num).serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'referencia_personal_'+num+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarReferenciaPersonal'); ?>',
	        	method: "POST",  
	            data: {'d_refPersonal':d_refPersonal,'num':num},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
	        		setTimeout(function(){
						$('.loader').fadeOut();
		        		$("#refPer"+num+"_id").val(res);
	        			$("#texto_msj").text('La referencia personal #'+num+' se ha guardado correctamente');
		        		$("#mensaje").css('display','block');
		        		setTimeout(function(){
		              		$('#mensaje').fadeOut();
		            	},6000);
	            	},300);
	        	}
	      	});
	    }
	}
	function guardarVecinales(num){
		var d_refVecinal = $("#d_refVecinal"+num).serialize();
		d_refVecinal += "&id_candidato="+$("#idCandidato").val();
		d_refVecinal += "&id_refvec="+$("#idrefvec"+num).val();
		
		var totalPer = $(".vec"+num+"_obligado").filter(function(){
      		return !$(this).val();
    	}).length;
    	if(totalPer > 0){
	      	$(".vec"+num+"_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#d_refVecinal"+num).serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'visita_referencia_vecinal_'+num+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/actualizarReferenciasVecinales'); ?>',
	        	method: "POST",  
	            data: {'d_refVecinal':d_refVecinal,'num':num},
	            beforeSend: function() {
                	$('.loader').css("display","block");
              	},
	        	success: function(res){
	        		setTimeout(function(){
						$('.loader').fadeOut();
		        		$("#idrefvec"+num).val(res);
	        			$("#texto_msj").text('La referencia vecinal #'+num+' se ha guardado correctamente');
		        		$("#mensaje").css('display','block');
		        		setTimeout(function(){
		              		$('#mensaje').fadeOut();
		            	},6000);
	        		},300);
	        	}
	      	});
	    }
	}
	function actualizarLaboral(num){
		var reflaboral = $("#candidato_reflaboral"+num).serialize();
		reflaboral += "&id_candidato="+$("#idCandidato").val();
		reflaboral += "&idref="+$("#idreflab"+num).val();
		var total = $('.reflab'+num+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.reflab'+num+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en la sección de la referencia laboral #"+num+".");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#candidato_reflaboral"+num).serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'referencia_laboral_'+num+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarReferenciaLaboral'); ?>',
	        	method: "POST",  
	            data: {'reflaboral':reflaboral,'num':num},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
        			setTimeout(function(){
						$('.loader').fadeOut();
					},300);
	        		$("#idreflab"+num).val(res);
        			$("#texto_msj").text('Los datos de la referencia laboral #'+num+' se han actualizado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},6000);
	        	}
	      	});
	    }
	}
	function verificarLaboral(num){
		var reflaboral = $("#analista_reflaboral"+num).serialize();
		reflaboral += "&id_candidato="+$("#idCandidato").val();
		reflaboral += "&idver="+$("#idverlab"+num).val();
		var total = $('.ver_reflab'+num+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.ver_reflab'+num+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en la sección de la verificación laboral #"+num+".");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#analista_reflaboral"+num).serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'verificacion_laboral_'+num+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarVerificacionLaboral'); ?>',
	        	method: "POST",  
	            data: {'reflaboral':reflaboral,'num':num},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
        			setTimeout(function(){
						$('.loader').fadeOut();
					},300);
	        		$("#idverlab"+num).val(res);
        			$("#texto_msj").text('Los datos de la verificación laboral #'+num+' se han actualizado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},6000);
	        	}
	      	});
	    }
	}
	function actualizarTrabajoGobierno(){
		var trabajo = $("#trabajo_gobierno").val();
		var enterado = $("#trabajo_enterado").val();
		var id_candidato = $("#idCandidato").val();
		var total = $('.trabajo_gobierno').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.trabajo_gobierno').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#trabajo_gobierno").val();
	    	formdata += ','+enterado;
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'trabajo_gobierno-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarTrabajoGobierno'); ?>',
	        	method: "POST",  
	            data: {'trabajo':trabajo,'id_candidato':id_candidato,'enterado':enterado},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
        			setTimeout(function(){
						$('.loader').fadeOut();
					},300);
        			$("#texto_msj").text('La informacion se ha actualizado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},6000);
	        	}
	      	});
	    }
	}
	function actualizarInvestigacion(){
		var inv = $("#d_investigacion").serialize();
		inv += "&id_candidato="+$("#idCandidato").val();
		var total = $('.inv_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.inv_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $('#d_investigacion').serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'investigacion_legal-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/actualizarInvestigacionLegal'); ?>',
	        	method: "POST",  
	            data: {'inv':inv},
	            beforeSend: function(){
	                $('.loader').css("display","block");
	            },
	        	success: function(res){
	        		if(res == 1){
	        			setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  	},200);
	                  	$("#legalesModal").modal('hide');
	          			localStorage.setItem("success", 1);
	        			location.reload();
	        		}
	        	}
	      	});
	    }
	}
	function actualizarNoMencionados(){
		var nomen = $("#d_no_mencionados").serialize();
		nomen += "&id_candidato="+$("#idCandidato").val();
		var total = $('.nomen_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.nomen_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $('#d_no_mencionados').serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'trabajos_no_mencionados-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/actualizarNoMencionados'); ?>',
	        	method: "POST",  
	            data: {'nomen':nomen},
	            beforeSend: function(){
	                $('.loader').css("display","block");
	            },
	        	success: function(res){
	        		if(res == 1){
	        			setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  	},200);
	                  	$("#legalesModal").modal('hide');
	          			localStorage.setItem("success", 1);
	        			location.reload();
	        		}
	        	}
	      	});
	    }
	}
	function actualizarEgresos(){
		var egresos = $("#d_egresos").serialize();
		egresos += "&id_candidato="+$("#idCandidato").val();
		var total = $('.e_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.e_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $('#d_egresos').serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'visita_egresos-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/actualizarEgresos'); ?>',
	        	method: "POST",  
	            data: {'egresos':egresos},
	            beforeSend: function(){
	                $('.loader').css("display","block");
	            },
	        	success: function(res){
	        		if(res == 1){
	        			setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  	},200);
	                  	$("#egresosModal").modal('hide');
	          			localStorage.setItem("success", 1);
	        			location.reload();
	        		}
	        	}
	      	});
	    }
	}
	function actualizarHabitacion(){
		var hab = $("#d_habitacion").serialize();
		hab += "&id_candidato="+$("#idCandidato").val();
		var total = $('.h_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.h_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $('#d_habitacion').serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'visita_habitacion-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/actualizarHabitacion'); ?>',
	        	method: "POST",  
	            data: {'hab':hab},
	            beforeSend: function(){
	                $('.loader').css("display","block");
	            },
	        	success: function(res){
	        		if(res == 1){
	        			setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  	},200);
	                  	$("#visitaHabitacionModal").modal('hide');
	          			localStorage.setItem("success", 1);
	        			location.reload();
	        		}
	        	}
	      	});
	    }
	}
	function guardarExtrasCandidato(id_candidato){
		var muebles = $("#candidato_muebles").val();
		var notas = $("#notas").val();

		var total = $('.extra_candidato').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(total > 0){
	      	$(".extra_candidato").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en los muebles o inmuebles y notas del candidato.");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},4000);
		          	
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	   	else{
	   		formdata = $("#d_familiar_candidato").serialize();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'grupo_familiar_candidato-'+id_candidato+'-'+fecha_txt);
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/updateExtrasCandidato'); ?>',
	        	method: "POST",  
	            data: {'id_candidato':id_candidato,'notas':notas,'muebles':muebles},
	        	success: function(res){
	        		if(res == 1){
	        			$("#texto_msj").text('Los datos del candidato se han actualizado correctamente');
		        		$("#mensaje").css('display','block');
		        		setTimeout(function(){
		              		$('#mensaje').fadeOut();
		            	},6000);
	        		}
	        	}
	      	});
	    }
	}
	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
	function confirmarProceso(){
		var id_candidato = $(".idCandidato").val();
		$.ajax({
         	url:"<?php echo base_url('Candidato/confirmarProceso'); ?>",   
         	method:"POST",  
         	data: {'id_candidato':id_candidato},
      		dataType: "text",
         	success:function(res){
         		$("#confirmarCandidatoModal").modal('hide');
     			$("#texto_msj").text('Se ha iniciado el proceso para el candidato '+$("#visita_candidato").val()+' correctamente');
        		$("#mensaje").css('display','block');
        		setTimeout(function(){
              		$('#mensaje').fadeOut();
            	},6000);
            	recargarTable();
        	}  
      	});
	}
	function subirDoc(){
        if($("#documento").val() == ""){
          $("#sin_documento").css("display","block");
          setTimeout(function(){
            $("#sin_documento").fadeOut();
          }, 4000);
        }
        else{
        	var id_candidato = $("#idCandidato").val();
		    var prefijo = $(".prefijo").val();
		    //var form = document.getElementById("formSubirDoc");
	        var data = new FormData();
	        var doc = $("#documento")[0].files[0];
	        data.append('id_candidato', id_candidato);
	        data.append('prefijo', prefijo);
	        data.append('documento', doc);
          	$.ajax({
             	url:"<?php echo base_url('Candidato/uploadDoc'); ?>",   
             	method:"POST",  
             	data:data,  
             	contentType: false,  
             	cache: false,  
             	processData:false,
             	
             	success:function(res){
         			$("#documento").val("");
         			
                	$("#subido").css('display','block');
            		setTimeout(function(){
                  		$('#subido').fadeOut();
                	},4000);
            		$("#tablaDocs").empty();
            		$("#tablaDocs").html(res);
            	}  
          	});
        }
    }  
	//Se actualizan los documentos referenciando por su tipo
	function actualizarDocs(){
		$("#sin_documento").css("display","none");
		var total = $("select[id^='tipo']").length;

		var hayArchivo = $('.item').filter(function(){
      		return $(this).val();
    	}).length;

		var totalVacios = $('.item').filter(function(){
      		return !$(this).val();
    	}).length;

		//console.log(hayArchivo)
    	if(totalVacios > 0){
	      	$(".item").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#tipos_iguales").css('display','none');
					$("#campos_vacios").css('display','block');
					setTimeout(function(){
						$("#campos_vacios").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	if(hayArchivo <= 0){
	    		$("#sin_documento").css('display','block');
        		setTimeout(function(){
              		$('#sin_documento').fadeOut();
            	},4000);
	    	}
	    	else{
	    		var id_candidato = $("#idCandidato").val();
		    	var prefijo = $(".prefijo").val();
		    	var salida = "";
				var indicador = 0;
				var aux = new Array();
	    		for(var i = 0; i < total; i++){
	    			var arch = $("#arch"+i).text();
	    			var tipo = $("#tipo"+i).val();
	    			//console.log("idc "+id_candidato+" pre: "+prefijo+" arch: "+arch+" tipo: "+tipo);
	    			/*if(aux.includes(tipo) && tipo != 13){
	    				indicador = 1;
	    			}
	    			else{
	    				salida += arch+","+tipo+"@@";
	    			}*/
	    			salida += arch+","+tipo+"@@";
	    			aux[i] = tipo;
	    		}
	    		if(indicador == 1){
	    			$("#tipos_iguales").css('display','block');
					setTimeout(function(){
						$("#tipos_iguales").fadeOut();
					},4000);
	    		}
		    	else{
		    		$.ajax({
		          		url: '<?php echo base_url('Candidato/saveDocs'); ?>',
		          		method: 'POST',
		          		data: {'docs':salida,'id_candidato':id_candidato,'prefijo':prefijo},
		          		dataType: "text",
		          		beforeSend: function() {
		                	$('.loader').css("display","block");
		              	},
		          		success: function(res)
		          		{
		          			if(res == 1){
		          				setTimeout(function(){
			                  		$('.loader').fadeOut();
			                	},300);
			                	$("#exito").css('display','block');
			                	$("#docsModal").modal('hide');
		                		setTimeout(function(){
			                  		$('#exito').fadeOut();
			                	},6000);
		          			}
		          		}
		        	});
		    	}
	    	}
	    }
		
	}
	//Modal para generar estatus de verificación de documentos
	function verificacionEstudios(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_estudio").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_estudio").css('display','none');
        				$("#btnTerminarVerificacionEstudio").css('display','none');
        			}
        			$("#div_crearEstatusEstudio").empty();
        			$("#idVerificacionEstudio").val(aux[1]);
        			$("#div_crearEstatusEstudio").append(aux[0]);
        		}
        		else{
        			$("#idVerificacionEstudio").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionEstudiosModal").modal("show");
	}
	//Generar estatus de verificacion de estudio
	function generarEstatusEstudio(){
		var id_candidato = $(".idCandidato").val();
		var id_verificacion = $("#idVerificacionEstudio").val();
		var comentario = $("#estudio_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_verificacion':id_verificacion,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionEstudio").val(aux[1]);
        		$("#estudio_estatus_comentario").val("");
        		$("#div_crearEstatusEstudio").empty();
        		$("#div_crearEstatusEstudio").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de estudios
	function terminarEstudios(){
		var id_verificacion = $("#idVerificacionEstudio").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/finishEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_verificacion':id_verificacion},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarEstudiosModal").modal('hide');
      			$("#verificacionEstudiosModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoEstudios").css('display','block');
            	setTimeout(function(){
              		$('#exitoEstudios').fadeOut();
            	},6000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar estatus de verificación de referencias laborales
	function verificacionLaborales(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_laboral").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_laboral").css('display','none');
        				$("#btnTerminarVerificacionLaboral").css('display','none');
        			}
        			$("#div_crearEstatusLaboral").empty();
        			$("#idVerificacionLaboral").val(aux[1]);
        			$("#div_crearEstatusLaboral").append(aux[0]);
        		}
        		else{
        			$("#idVerificacionLaboral").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionLaboralesModal").modal("show");
	}
	//Generar estatus de verificacion de referencia laboral
	function generarEstatusLaboral(){
		var id_candidato = $(".idCandidato").val();
		var id_status = $("#idVerificacionLaboral").val();
		var comentario = $("#laboral_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_status':id_status,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionLaboral").val(aux[1]);
        		$("#laboral_estatus_comentario").val("");
        		$("#div_crearEstatusLaboral").empty();
        		$("#div_crearEstatusLaboral").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de ref laborales
	function terminarLaborales(){
		var id_status = $("#idVerificacionLaboral").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/finishEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_status':id_status},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarLaboralesModal").modal('hide');
      			$("#verificacionLaboralesModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoLaborales").css('display','block');
            	setTimeout(function(){
              		$('#exitoLaborales').fadeOut();
            	},6000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar estatus de verificación de antecedentes no penales
	function verificacionPenales(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_penales").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_penales").css('display','none');
        				$("#btnTerminarVerificacionPenales").css('display','none');
        			}
        			$("#div_crearEstatusPenales").empty();
        			$("#idVerificacionPenales").val(aux[1]);
        			$("#div_crearEstatusPenales").append(aux[0]);
        		}
        		else{
        			$("#idVerificacionPenales").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionPenalesModal").modal("show");
	}
	//Generar estatus de verificacion de antecdentes no penales
	function generarEstatusPenales(){
		var id_candidato = $(".idCandidato").val();
		var id_status = $("#idVerificacionPenales").val();
		var comentario = $("#penales_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_status':id_status,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionPenales").val(aux[1]);
        		$("#penales_estatus_comentario").val("");
        		$("#div_crearEstatusPenales").empty();
        		$("#div_crearEstatusPenales").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de antecedentes no penales
	function terminarPenales(){
		var id_status = $("#idVerificacionPenales").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/finishEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_status':id_status},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarPenalesModal").modal('hide');
      			$("#verificacionPenalesModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoPenales").css('display','block');
            	setTimeout(function(){
              		$('#exitoPenales').fadeOut();
            	},6000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Obtiene el municipio al verificar datos
	function getMunicipio(id_estado, id_municipio){
		$.ajax({
        	url: '<?php echo base_url('Candidato/getMunicipios'); ?>',
        	method: 'POST',
        	data: {'id_estado':id_estado},
        	dataType: "text",
        	success: function(res){
          		$('#municipio').prop('disabled', false);
          		$('#municipio').html(res);
          		$("#municipio").find('option').attr("selected",false) ;
          		$('#municipio option[value="'+id_municipio+'"]').attr('selected', 'selected');
        	},error:function(res){
          		$('#errorModal').modal('show');
        	}
      	});
	}
	function ejecutarAccion(){
		var id = $("#idCandidato").val();
		var id_cliente = $("#idCliente").val();
		var motivo = $("#motivo").val();
		var usuario = 1;
		if(motivo == ""){
			$("#quitarModal #msj_texto").text("Se requiere el motivo");
    		$("#quitarModal #msj_error").css('display','block');
            setTimeout(function(){
              $('#quitarModal #msj_error').fadeOut();
            },4000);
		}
		else{
			$.ajax({
              	url: '<?php echo base_url('Candidato/accionCandidato'); ?>',
              	type: 'post',
              	data: {'id':id,'motivo':motivo,'usuario':usuario,'id_cliente':id_cliente},
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("deleted", 1);
	            	location.reload();
              	}
        	});
		}		
	}
	function subirPsicometria(){
        if($("#doc_psicometria").val() == ""){
            $("#doc_psicometria").addClass("requerido");
            $("#psicometriaModal #campos_vacios").css("display","block");
            setTimeout(function(){
                $("#psicometriaModal #campos_vacios").fadeOut();
            }, 4000);
        }
        else{
            var docs = new FormData();
            var archivo = $("#doc_psicometria")[0].files[0];
            docs.append("id_candidato", $("#idCandidato").val());
            docs.append("id_psicometrico", $("#idPsicometrico").val());
            docs.append("archivo", archivo);
            $.ajax({  
                url:"<?php echo base_url('Psicometrico/subirPsicometria'); ?>",   
                method: "POST",  
                data: docs,  
                contentType: false,  
                cache: false,  
                processData:false,  
                beforeSend: function(){
                    $('.loader').css("display","block");
                },
                success:function(res){
                    if(res == 1){
                        setTimeout(function(){
                            $('.loader').fadeOut();
                        },300);
                        $("#psicometriaModal").modal('hide');
                        $("#texto_msj").text(" La psicometria se ha guardado correctamente");
                        $("#mensaje").css('display','block');
                        recargarTable();
                        setTimeout(function(){
                            $("#mensaje").fadeOut();
                        },6000);
                    }
                }  
            });    
        }
    }
	function finalizarProceso(){
		var id_candidato = $("#idCandidato").val();
		var conclusiones = $("#formConclusiones").serialize();
		conclusiones += "&id_finalizado="+$("#idFinalizado").val();
		var totalVacios = $('.conclusion_obligado').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".conclusion_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
					$("#campos_vacios_check").css('display','block');
					setTimeout(function(){
						$("#campos_vacios_check").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $('#formConclusiones').serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'conclusiones-'+idCandidato+'-'+fecha_txt);
	    	$.ajax({
          		url: '<?php echo base_url('Candidato/terminarProcesoCandidato'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'conclusiones':conclusiones},
          		beforeSend: function(){
	                $('.loader').css("display","block");
	            },
          		success: function(res)
          		{
            		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("finished", 1);
	            	location.reload();
          		}
        	});
	    }
	}
	function actualizarCandidato(){
		var id_candidato = $("#idCandidato").val();
		var id_doping = $("#idDoping").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/actualizarProcesoCandidato'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_doping':id_doping},
      		beforeSend: function(){
                $('.loader').css("display","block");
            },
      		success: function(res)
      		{
      			if(res == 1){
      				setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("success", 1);
	            	location.reload();
      			}
      		}
    	});
	}
	function respaldoTxt(formdata,nombreArchivo){      
	    var textFileAsBlob = new Blob([formdata], {type:'text/plain'});
	    var fileNameToSaveAs = nombreArchivo+".txt";
	    var downloadLink = document.createElement("a");
	    downloadLink.download = fileNameToSaveAs;
	    downloadLink.innerHTML = "My Hidden Link";
	    window.URL = window.URL || window.webkitURL;
	    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	    downloadLink.onclick = destroyClickedElement;
	    downloadLink.style.display = "none";
	    document.body.appendChild(downloadLink);
	    downloadLink.click();
	}
	function destroyClickedElement(event){
	    document.body.removeChild(event.target);
	}
  	//Regresa del formulario al listado
  	function regresar(){
  		$("#btn_regresar").css('display','none'); 
  		$("#formulario").css('display','none');
  		$("#listado").css('display','block');
  		$("#btn_nuevo").css('display','block');
  	}
  	function convertirDate(fecha){
  		var aux = fecha.split('-');
    	var f = aux[2]+'/'+aux[1]+'/'+aux[0];
    	return f;
  	}
  	function convertirDateTime(fecha){
  		var f = fecha.split(' ');
    	var aux = f[0].split('-');
    	var date = aux[2]+'/'+aux[1]+'/'+aux[0];
    	return date;
  	}
  	function checkDecimales(el){
 		var ex = /^[0-9]+\.?[0-9]*$/;
 		if(ex.test(el.value)==false){
   			el.value = el.value.substring(0,el.value.length - 1);
  		}
	}
	function regresarListado(){
		location.reload();
	}
  	//Obtiene la fecha de nacimiento
	$("#fecha_nacimiento").change(function(){
		var aux = $(this).val().split('/');
		var f = aux[1]+'/'+aux[0]+'/'+aux[2];
		var fecha = f;
		var today = new Date();
	    var birthDate = new Date(fecha);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age = age - 1;
	    }
	    $("#edad").val(age);
	});
  	//Acepta solo numeros en los input
  	$(".solo_numeros").on("input", function(){
	    var valor = $(this).val();
	    $(this).val(valor.replace(/[^0-9]/g, ''));
	});
	//Limpiar inputs
	$(".personal_obligado, .obligado, .conclusion_obligado, .familia_obligado, .estudios_obligado, .p_obligado, .refPer_obligado").focus(function(){
		$(this).removeClass("requerido");
	});

</script>