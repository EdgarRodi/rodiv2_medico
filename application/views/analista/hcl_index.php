<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div id="exito" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> Los documentos han sido actualizados correctamente.
  	</div>
  	<div id="exitoEstudios" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de estudios ha sido finalizada correctamente.
  	</div>
  	<div id="exitoLaborales" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de las referencias laborales ha sido finalizada correctamente.
  	</div>
  	<div id="exitoPenales" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de los antecedentes no penales ha sido finalizada correctamente.
  	</div>
  	<div id="exitoCandidato" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> Los datos del candidato han sido actualizados correctamente.
  	</div>
  	<div id="vacios" class="alert alert-danger fade in mensaje" style='display:none;'>
      	<strong>¡Atención!</strong><span id="txt_vacios"></span>
  	</div>
  	<div id="exitoFinalizado" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> El proceso del candidato ha finalizado correctamente.
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<p id="texto_msj"></p>
  	</div>
	<section class="content-header">
      	<h1 class="titulo_seccion">Cliente<small><?php echo $cliente; ?></small></h1>
    	<a class="btn btn-app" id="btn_regresar" onclick="regresarListado()" style="display: none;">
    		<i class="far fa-arrow-alt-circle-left"></i> <span>Regresar</span>
  		</a>
  		<a class="btn btn-app btn_acciones" data-toggle="modal" data-target="#newModal"><i class="fas fa-user-plus icon"></i> Add Candidate</a>
    </section>
    <div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">New Register</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<form id="datos">
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Name *</label>
			        			<input type="text" class="form-control registro_obligado" name="nombre_registro" id="nombre_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>First lastname *</label>
			        			<input type="text" class="form-control registro_obligado" name="paterno_registro" id="paterno_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Second lastname </label>
			        			<input type="text" class="form-control" name="materno_registro" id="materno_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>			    
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Email *</label>
			        			<input type="email" class="form-control registro_obligado" name="correo_registro" id="correo_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Cell phone number</label>
			        			<input type="text" class="form-control" name="celular_registro" id="celular_registro" maxlength="16">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Home Number </label>
			        			<input type="text" class="form-control" name="fijo_registro" id="fijo_registro" maxlength="18">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Birthdate</label>
			        			<input type="text" class="form-control solo_lectura" name="fecha_nacimiento_registro" id="fecha_nacimiento_registro" placeholder="mm/dd/yyyy" readonly>
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Project *</label>
			        			<select name="proyecto_registro" id="proyecto_registro" class="form-control registro_obligado">
						            <option value="">Select</option>
						            <?php 
						            	foreach ($proyectos as $pro) { ?>
						                	<option value="<?php echo $pro->id; ?>"><?php echo $pro->nombre; ?></option>
						            <?php 	
					        			}?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Drug test *</label>
			        			<select name="examen_registro" id="examen_registro" class="form-control registro_obligado">
						            <option value="">Select</option>
					          	</select>
					          	<br>
			        		</div>			        		
			        	</div>
			        </form>
			        <div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-success" id="registrar">Create</button>
		      	</div>
		    </div>
	 	</div>
	</div>
    <div class="modal fade" id="docsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Documentación</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" class="idCandidato">
		      		<input type="hidden" class="prefijo">
		        	<div class="row">
		        		<div class="col-md-4 margen">
		        			<label>Selecciona el documento</label><br>
	        				<input type="file" id="documento" class="doc_obligado" name="documento" accept=".pdf, .jpg, .png, .jpeg"><br>
                        	<button class="btn btn-primary" onclick="subirDoc()">Subir</button>
		        		</div>
		        		<div id="tablaDocs" class="col-md-8 borde">
		        		
		        		</div>
		        		
		        	</div>
		        	<div class="row margen">
		        		<div id="campos_vacios">
			      			<p class="msj_error">No se ha seleccionado el tipo de archivo</p>
			      		</div>
			      		<div id="tipos_iguales">
			      			<p class="msj_error">Los documentos deben ser de diferente tipo</p>
			      		</div>
			      		<div id="sin_documento">
			      			<p class="msj_error">Seleccione un documento para subir</p>
			      		</div>
			      		<div id="subido">
			      			<p class="msj_error">El documento se ha subido con éxito</p>
			      		</div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-danger" onclick="actualizarDocs()">Actualizar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verificacionEstudiosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Verificación de estudios académicos</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idVerificacionEstudio">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusEstudio">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_estudio">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_estudio"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="estudio_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="estudio_estatus_comentario" id="estudio_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusEstudio()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" id="btnTerminarVerificacionEstudio" class="btn btn-danger" data-toggle="modal" data-target="#confirmarEstudiosModal">Terminar Verificación</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="confirmarEstudiosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Confirmación de estudios académicos</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<h4>¿Estás segura de terminar la verificación de estudios de <b><span id="estudios_nombrecandidato"></span></b>?</h4>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			        <button type="button" class="btn btn-danger" onclick="terminarEstudios()">Aceptar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verificacionLaboralesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Verificación de referencias laborales</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idVerificacionLaboral">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusLaboral">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_laboral">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_laboral"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="laboral_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="laboral_estatus_comentario" id="laboral_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusLaboral()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" id="btnTerminarVerificacionLaboral" class="btn btn-danger" data-toggle="modal" data-target="#confirmarLaboralesModal">Terminar Verificación</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="confirmarLaboralesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Confirmación de referencias laborales</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<h4>¿Estás segura de terminar la verificación de referencias laborales de <b><span id="laborales_nombrecandidato"></span></b>?</h4>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			        <button type="button" class="btn btn-danger" onclick="terminarLaborales()">Aceptar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verificacionPenalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Verificación de antecedentes no penales</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idVerificacionPenales">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusPenales">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_penales">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_penales"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="penales_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="penales_estatus_comentario" id="penales_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusPenales()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" id="btnTerminarVerificacionPenales" class="btn btn-danger" data-toggle="modal" data-target="#confirmarPenalesModal">Terminar Verificación</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="confirmarPenalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Confirmación de antecedentes no penales</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<h4>¿Estás segura de terminar la verificación de antecedentes no penales de <b><span id="penales_nombrecandidato"></span></b>?</h4>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			        <button type="button" class="btn btn-danger" onclick="terminarPenales()">Aceptar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Avances en el estudio del candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idAvances">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusAvances">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_avances">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_avances"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="avances_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="avances_estatus_comentario" id="avances_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusAvance()">Actualizar avances</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="llamadasModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Registro de llamadas al candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idLlamadas">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusLlamadas">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_llamadas">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_llamadas"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="llamadas_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="llamadas_estatus_comentario" id="llamadas_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusLlamada()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="emailsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Registro de correos enviados al candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idEmails">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusEmail">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_emails">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_emails"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="emails_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="emails_estatus_comentario" id="emails_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusEmail()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="ofacModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Verificación OFAC y OIG</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div class="row">
		      			<div class="col-md-4 col-md-offset-2">
		      				<p class="text-center"><b>Nombre del candidato</b></p>
		      				<p class="text-center" id="ofac_nombrecandidato"></p>
		      			</div>
            			<div class="col-md-4">
                        	<p class="text-center" id="fecha_titulo_ofac"><b></b></p>
                            <p class="text-center" id="fecha_estatus_ofac"></p>
                        </div>
            		</div>
        			<div class="row">
                        <div class="col-md-10 col-md-offset-1">
                        	<label for="estatus_ofac">Estatus OFAC *</label>
                            <textarea class="form-control ofac" name="estatus_ofac" id="estatus_ofac" rows="3" placeholder="Estatus OFAC"></textarea>
                            <input type="hidden" id="idCliente">
                            <br>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-4 col-md-offset-4">
                    		<label for="res_ofac">Resultado *</label>
	        				<select name="res_ofac" id="res_ofac" class="form-control ofac">
				            	<option value="">Select</option>
				            	<option value="1">Positivo</option>
				            	<option value="0">Negativo</option>
			          		</select>
			          		<br>
                    	</div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                        	<label for="estatus_oig">Estatus OIG *</label>
                            <textarea class="form-control ofac" name="estatus_oig" id="estatus_oig" rows="3" placeholder="Estatus OIG"></textarea>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-4 col-md-offset-4">
	                    	<label for="res_oig">Resultado *</label>
	        				<select name="res_oig" id="res_oig" class="form-control ofac">
				            	<option value="">Select</option>
				            	<option value="1">Positivo</option>
				            	<option value="0">Negativo</option>
			          		</select>
			          		<br><br><br>
			          	</div>
                    </div>
                    <div class="row">
                    	<div class="col-md-4">
                    		<div id="campos_vacios_ofac">
			      			<p class="msj_error text-center">Hay campos vacíos</p>
			      		</div>
                    	</div>
                    	<div class="col-md-3 col-md-offset-1">
                    		<a class="btn btn-app btn_verificacion" onclick="actualizarOfac()">Actualizar verificación</a>
                    	</div>
                    	<br>
                    </div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="completarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Finalizar proceso del candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_incompleto" style="display: none;">
		      			<h4 class="text-center" id="msjIncompleto"></h4>
		      		</div>
		      		<div id="div_completo" style="display: none;">
		      			<form id="formChecks">
			      			<div class="row">
			      				<div class="col-md-4" id="final_identity">
			      					<label for="check_identidad">Identity Check *</label>
			        				<select name="check_identidad" id="check_identidad" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      				<div class="col-md-4" id="final_employment">
			      					<label for="check_laboral">Employment History Check *</label>
			        				<select name="check_laboral" id="check_laboral" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
					          	</div>
			      				<div class="col-md-4" id="final_education">
			      					<label for="check_estudios">Highest Education Check *</label>
			        				<select name="check_estudios" id="check_estudios" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      			</div>
			      			<div class="row">
			      				<div class="col-md-4" id="final_visit">
			      					<label for="check_visita">Home Visit *</label>
			        				<select name="check_visita" id="check_visita" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      				<div class="col-md-4" id="final_criminal">
			      					<label for="check_penales">Criminal Records – Mexican *</label>
			        				<select name="check_penales" id="check_penales" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      				<div class="col-md-4" id="final_ofacs">
			      					<label for="check_ofac">Criminal Records – OFAC *</label>
			        				<select name="check_ofac" id="check_ofac" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      			</div>
			      			<div class="row">
			      				<div class="col-md-4" id="final_doping">
			      					<label for="check_laboratorio">Laboratory Test *</label>
			        				<select name="check_laboratorio" id="check_laboratorio" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Not approved</option>
						            	<option value="1">Approved</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      				<div class="col-md-4" id="final_medical">
			      					<label for="check_medico">Medical Check Up *</label>
			        				<select name="check_medico" id="check_medico" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      				<div class="col-md-4" id="final_global">
			      					<label for="check_global">Global data searches *</label>
			        				<select name="check_global" id="check_global" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      			</div>
			      			<div class="row">
			      				<div class="col-md-4" id="final_domicilios">
			      					<label for="check_domicilio">Addresses History Check *</label>
			        				<select name="check_domicilio" id="check_domicilio" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      				<div class="col-md-4" id="final_domicilios">
			      					<label for="check_domicilio">Credit History Check *</label>
			        				<select name="check_credito" id="check_credito" class="form-control">
						            	<option value="-1">Select</option>
						            	<option value="0">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="2">For Consideration (FC)</option>
						            	<option value="3">NA</option>
					          		</select>
					          		<br>
			      				</div>
			      			</div>
			      			<div class="row">
			      				<div class="col-md-12">
			      					<label>Final Statement *</label>
			      					<textarea class="form-control check_obligado" name="comentario_final" id="comentario_final" rows="3"></textarea>
		                			<br>
			      				</div>
			      			</div>
			      			<div class="row">
			      				<div class="col-md-4 col-md-offset-4">
			      					<label>Final BGC Status *</label>
			      					<select name="bgc_status" id="bgc_status" class="form-control check_obligado">
						            	<option value="-1">Select</option>
						            	<option value="2">Negative</option>
						            	<option value="1">Positive</option>
						            	<option value="3">For Consideration (FC)</option>
					          		</select>
		                			<br>
			      				</div>
			      			</div>
			      		</form>
		      			<div id="campos_vacios_check" class="alert alert-danger">
			      			<p class="text-center">Hay campos vacíos</p>
			      		</div>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" id="btnTerminar" class="btn btn-danger" style="display: none;" onclick="finalizarProceso()">Terminar</button>
			        <input type="hidden" id="idBGC">
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="completarOfacModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Finalizar proceso del candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="ofac_incompleto" style="display: none;">
		      			<h4 class="text-center" id="ofac_msjIncompleto"></h4>
		      		</div>
		      		<div id="ofac_completo" style="display: none;">
		      			<form id="ofacChecks">
			      			<div class="row">
			      				<div class="col-md-12">
			      					<label>Informe final *</label>
			      					<textarea class="form-control fin_ofac_obligado" name="ofac_comentario_final" id="ofac_comentario_final" rows="3"></textarea>
		                			<br>
			      				</div>
			      			</div>
			      			<div class="row">
			      				<div class="col-md-4 col-md-offset-4">
			      					<label>Estatus final *</label>
			      					<select name="ofac_estatus_final" id="ofac_estatus_final" class="form-control fin_ofac_obligado">
						            	<option value="">Select</option>
						            	<option value="1">Positivo</option>
						            	<option value="2">Negativo</option>
						            	<option value="3">A consideración</option>
					          		</select>
		                			<br>
			      				</div>
			      			</div>
			      		</form>
		      			<div id="campos_vacios_ofac_final">
			      			<p class="msj_error text-center">Hay campos vacíos</p>
			      		</div>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" id="btnOfac" class="btn btn-danger" style="display: none;" onclick="finalizarOfac()">Terminar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<h4 id="nombre_candidato"></h4><br>
	        		<p class="" id="motivo"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      		<button type="button" class="btn btn-danger" id="btnGuardar" onclick="ejecutarAccion()">Accept</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="passModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">Password Generated</h5>
			        
		      	</div>
		      	<div class="modal-body">
		        	<h4>Data login on form</h4><br>
		        	<p><b>User: </b><span id="user"></span></p>
		        	<p><b>Password: </b><span id="pass"></span></p><br>
		        	<p id="respuesta_mail"></p>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="gapsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Gaps laborales</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_antesgap">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_gaps">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha inicio</b></p>
                                <input type="text" class="form-control" name="fecha_inicio" id="fecha_inicio">
                            </div>
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha fin</b></p>
                                <input type="text" class="form-control" name="fecha_fin" id="fecha_fin">
                            </div>
                            <div class="col-md-6">
                            	<label>Razón</label>
                                <textarea class="form-control" name="razon" id="razon" rows="3"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarGap()">Guardar</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="creditoModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Historial crediticio</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_antescredit">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_credit">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha inicio</b></p>
                                <input type="text" class="form-control" name="credito_fecha_inicio" id="credito_fecha_inicio">
                            </div>
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha fin</b></p>
                                <input type="text" class="form-control" name="credito_fecha_fin" id="credito_fecha_fin">
                            </div>
                            <div class="col-md-6">
                            	<label>Comentario</label>
                                <textarea class="form-control" name="credito_comentario" id="credito_comentario" rows="3"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarHistorialCredito()">Guardar</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="customDatabaseModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Búsquedas globales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_custom_database">
                		<div class="row">
                			<div class="col-md-4">
			        			<label>Law enforcement *</label>
			        			<input type="text" class="form-control custom_database_obligado" name="custom_law_enforcement" id="custom_law_enforcement">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Regulatory *</label>
			        			<input type="text" class="form-control custom_database_obligado" name="custom_regulatory" id="custom_regulatory">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Sanctions  *</label>
			        			<input type="text" class="form-control custom_database_obligado" name="custom_sanctions" id="custom_sanctions">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Other bodies  *</label>
			        			<input type="text" class="form-control custom_database_obligado" name="custom_other_bodies" id="custom_other_bodies">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Web and media searches *</label>
			        			<input type="text" class="form-control custom_database_obligado" name="custom_media_searches" id="custom_media_searches">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>SDN *</label>
			        			<input type="text" class="form-control custom_database_obligado" name="custom_sdn" id="custom_sdn">
			        			<br>
			        		</div>
				        </div>
				       	<div class="row">
			        		<div class="col-md-12">
			        			<label>Comentarios *</label>
			        			<input type="text" class="form-control custom_database_obligado" name="custom_comentarios" id="custom_comentarios">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarCustomDatabase">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="customIdentidadModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Verificación de identidad del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_custom_identidad">
		      			<div class="alert alert-info">
		      				<p class="text-center">INE (IFE) *</p>
		      			</div>
                		<div class="row">
			        		<div class="col-md-6">
			        			<label>ID *</label>
			        			<input type="text" class="form-control custom_identidad_obligado" name="custom_ine" id="custom_ine">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label>Año de registro *</label>
			        			<input type="text" class="form-control custom_identidad_obligado" name="custom_ine_ano" id="custom_ine_ano">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-6">
			        			<label>Número vertical *</label>
			        			<input type="text" class="form-control custom_identidad_obligado" name="custom_ine_vertical" id="custom_ine_vertical">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label>Fecha / Institución *</label>
			        			<input type="text" class="form-control custom_identidad_obligado" name="custom_ine_institucion" id="custom_ine_institucion">
			        			<br>
			        		</div>
				        </div>
				        <div class="alert alert-info">
		      				<p class="text-center">Pasaporte</p>
		      			</div>
		      			<div class="row">
			        		<div class="col-md-6">
			        			<label>ID</label>
			        			<input type="text" class="form-control" name="custom_pasaporte" id="custom_pasaporte">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="custom_pasaporte_fecha" id="custom_pasaporte_fecha">
			        			<br>
			        		</div>
				        </div>
				        <div class="alert alert-info">
		      				<p class="text-center">Cartilla militar</p>
		      			</div>
		      			<div class="row">
			        		<div class="col-md-6">
			        			<label>ID</label>
			        			<input type="text" class="form-control" name="custom_militar" id="custom_militar">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="custom_militar_fecha" id="custom_militar_fecha">
			        			<br>
			        		</div>
				        </div>
				       	<div class="row">
			        		<div class="col-md-12">
			        			<label>Comentarios *</label>
			        			<input type="text" class="form-control custom_identidad_obligado" name="custom_identidad_comentarios" id="custom_identidad_comentarios">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarVerificacionIdentidad">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="customAddressModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Historial de domicilios del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<?php 
		      		for($i = 1; $i <= 8; $i++){ ?>
		      			<form id="d_custom_address<?php echo $i; ?>">
			      			<div class="alert alert-info">
			      				<p class="text-center"><?php echo 'Direccion #'.$i; ?></p>
			      			</div>
			      			<div class="row">
				        		<div class="col-md-4">
				        			<label>Periodo</label>
				        			<input type="text" class="form-control custom_address_obligado<?php echo $i; ?>" name="custom_periodo<?php echo $i; ?>" id="custom_periodo<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Causa de salida *</label>
				        			<input type="text" class="form-control custom_address_obligado<?php echo $i; ?>" name="custom_causa<?php echo $i; ?>" id="custom_causa<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Calle *</label>
				        			<input type="text" class="form-control custom_address_obligado<?php echo $i; ?>" name="custom_calle<?php echo $i; ?>" id="custom_calle<?php echo $i; ?>">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-4">
				        			<label>No. Exterior *</label>
				        			<input type="text" class="form-control custom_address_obligado<?php echo $i; ?>" name="custom_exterior<?php echo $i; ?>" id="custom_exterior<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>No. Interior *</label>
				        			<input type="text" class="form-control custom_address_obligado<?php echo $i; ?>" name="custom_interior<?php echo $i; ?>" id="custom_interior<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Colonia *</label>
				        			<input type="text" class="form-control custom_address_obligado<?php echo $i; ?>" name="custom_colonia<?php echo $i; ?>" id="custom_colonia<?php echo $i; ?>">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-4">
				        			<label>Estado *</label>
				        			<select name="custom_estado<?php echo $i; ?>" id="custom_estado<?php echo $i; ?>" class="form-control custom_address_obligado<?php echo $i; ?> custom_estado">
							            <option value="">Select</option>
							            <?php foreach ($estados as $e) {?>
							                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Municipio *</label>
				        			<select name="custom_municipio<?php echo $i; ?>" id="custom_municipio<?php echo $i; ?>" class="form-control custom_address_obligado<?php echo $i; ?>" disabled>
							            <option value="-1">Select</option>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>CP *</label>
				        			<input type="text" class="form-control custom_address_obligado<?php echo $i; ?>" name="custom_cp<?php echo $i; ?>" id="custom_cp<?php echo $i; ?>" maxlength="5">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-4 col-md-offset-5">
				        			<button type="button" class="btn btn-primary" onclick="guardarDomicilio(<?php echo $i; ?>)">Guardar</button>
				        			<input type="hidden" id="idDomicilio<?php echo $i; ?>">
				        			<br><br>
				        		</div>
				        	</div>
				        	<div id="msj_error<?php echo $i; ?>" class="alert alert-danger" style="display: none;">
				      			<p id="msj_texto<?php echo $i; ?>" class="text-white"></p>
				      		</div>
				        </form>
		      		<?php
		      		}
		      		?>
		      		<div class="row">
		        		<div class="col-md-12">
		        			<label>Comentarios *</label>
		        			<input type="text" class="form-control custom_comentario_obligado" name="custom_address_comentarios" id="custom_address_comentarios">
		        			<br>
		        		</div>
			        </div>
			        <div class="row">
		        		<div class="col-md-4 col-md-offset-5">
		        			<button type="button" class="btn btn-primary" onclick="guardarComentarioDomicilio(<?php echo $i; ?>)">Guardar comentario</button><br><br>
		        		</div>
		        	</div>
			   		<div id="msj_error_comentario" class="alert alert-danger" style="display: none;">
		      			<p id="msj_texto_comentario" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="generalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Datos generales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_generales">
                		<div class="row">
			        		<div class="col-md-4">
			        			<label>Name *</label>
			        			<input type="text" class="form-control personal_obligado" name="nombre_general" id="nombre_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>First lastname *</label>
			        			<input type="text" class="form-control personal_obligado" name="paterno_general" id="paterno_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Second lastname </label>
			        			<input type="text" class="form-control" name="materno_general" id="materno_general" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Birthdate *</label>
			        			<input type="text" class="form-control personal_obligado" name="fecha_nacimiento" id="fecha_nacimiento">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Age *</label>
			        			<input type="text" class="form-control" id="edad" disabled>
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Nationality *</label>
			        			<input type="text" class="form-control personal_obligado" name="nacionalidad" id="nacionalidad">
			        			<br>
			        		</div>
			        	</div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Job Position Requested *</label>
			        			<input type="text" class="form-control personal_obligado" name="puesto_general" id="puesto_general">
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Gender: *</label>
			        			<select name="genero" id="genero" class="form-control personal_obligado">
						            <option value="">Select</option>
						            <option value="Male">Male</option>
						            <option value="Female">Female</option>
					          	</select>
					          	<br>
			        		</div>	
			        		<div class="col-md-4">
			        			<label>Address *</label>
			        			<input type="text" class="form-control personal_obligado" name="calle" id="calle">
			        			<br>
			        		</div>	
				        </div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Ext. Number *</label>
			        			<input type="text" class="form-control personal_obligado" name="exterior" id="exterior" maxlength="8">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Int. Number </label>
			        			<input type="text" class="form-control" name="interior" id="interior" maxlength="8">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Neighborhood *</label>
			        			<input type="text" class="form-control personal_obligado" name="colonia" id="colonia">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>State *</label>
			        			<select name="estado" id="estado" class="form-control personal_obligado">
						            <option value="">Select</option>
						            <?php foreach ($estados as $e) {?>
						                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>City *</label>
			        			<select name="municipio" id="municipio" class="form-control personal_obligado" disabled>
						            <option value="">Select</option>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Zip Code *</label>
			        			<input type="text" class="form-control solo_numeros personal_obligado" name="cp" id="cp" maxlength="5">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Marital Status *</label>
			        			<select name="civil" id="civil" class="form-control personal_obligado">
						            <option value="">Select</option>
					            	<option value="1">Married</option>
						            <option value="2">Single</option>
						            <option value="3">Divorced</option>
						            <option value="4">Free union</option>
						            <option value="5">Widowed</option>
						            <option value="6">Separated</option>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Mobile Number *</label>
			        			<input type="text" class="form-control solo_numeros personal_obligado" name="celular_general" id="celular_general" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Home Number </label>
			        			<input type="text" class="form-control solo_numeros" name="tel_casa" id="tel_casa" maxlength="10">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Number to leave Messages </label>
			        			<input type="text" class="form-control solo_numeros" name="tel_oficina" id="tel_oficina" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Email *</label>
			        			<input type="text" class="form-control personal_obligado" name="personales_correo" id="personales_correo">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGenerales">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="mayoresEstudiosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Mayores estudios del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_mayor_estudios">
		      			<div class="alert alert-info">
		      				<p class="text-center">Candidato </p>
		      			</div>
                		<div class="row">
							<div class="col-md-4">
								<label>Nivel escolar *</label>
			        			<select name="mayor_estudios" id="mayor_estudios" class="form-control mayor_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($studies as $st) {?>
						                <option value="<?php echo $st->id; ?>"><?php echo $st->nombre; ?></option>
						            <?php } ?>
					          	</select>
			        			<br>
							</div>
							<div class="col-md-4">
								<label>Periodo *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_periodo" id="estudios_periodo">
			        			<br>
							</div>
							<div class="col-md-4">
								<label>Escuela *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_escuela" id="estudios_escuela">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label>Ciudad *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_ciudad" id="estudios_ciudad">
			        			<br>
							</div>
							<div class="col-md-4">
								<label>Certificado obtenido *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_certificado" id="estudios_certificado">
			        			<br>
							</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Analista </p>
		      			</div>
		      			<div class="row">
							<div class="col-md-4">
								<label>Nivel escolar *</label>
			        			<select name="mayor_estudios2" id="mayor_estudios2" class="form-control mayor_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($studies as $st) {?>
						                <option value="<?php echo $st->id; ?>"><?php echo $st->nombre; ?></option>
						            <?php } ?>
					          	</select>
			        			<br>
							</div>
							<div class="col-md-4">
								<label>Periodo *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_periodo2" id="estudios_periodo2">
			        			<br>
							</div>
							<div class="col-md-4">
								<label>Escuela *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_escuela2" id="estudios_escuela2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label>Ciudad *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_ciudad2" id="estudios_ciudad2">
			        			<br>
							</div>
							<div class="col-md-4">
								<label>Certificado obtenido *</label>
			        			<input type="text" class="form-control mayor_obligado" name="estudios_certificado2" id="estudios_certificado2">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Comentarios *</label>
			        			<textarea class="form-control mayor_obligado" name="mayor_estudios_comentarios" id="mayor_estudios_comentarios" rows="3"></textarea>
			        			<input type="hidden" id="idMayoresEstudios">
			        			<br><br>
							</div>
						</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="actualizarMayoresEstudios">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="globalesGeneralModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Búsquedas globales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_globales_general">
                		<div class="row">
                			<div class="col-md-4">
			        			<label>Law enforcement *</label>
			        			<input type="text" class="form-control global_general_obligado" name="law_enforcement" id="law_enforcement">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Regulatory *</label>
			        			<input type="text" class="form-control global_general_obligado" name="regulatory" id="regulatory">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Sanctions  *</label>
			        			<input type="text" class="form-control global_general_obligado" name="sanctions" id="sanctions">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Other bodies  *</label>
			        			<input type="text" class="form-control global_general_obligado" name="other_bodies" id="other_bodies">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Web and media searches *</label>
			        			<input type="text" class="form-control global_general_obligado" name="media_searches" id="media_searches">
			        			<br>
			        		</div>
				        </div>
				       	<div class="row">
			        		<div class="col-md-12">
			        			<label>Comentarios *</label>
			        			<input type="text" class="form-control global_general_obligado" name="global_general_comentarios" id="global_general_comentarios">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGlobalesGeneral">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="globalesEspecialesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Búsquedas globales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_globales_especiales">
                		<div class="row">
                			<div class="col-md-4">
			        			<label>Global compliance & Sanctions database  *</label>
			        			<input type="text" class="form-control global_especial_obligado" name="e_sanctions" id="e_sanctions">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Global media searches *</label>
			        			<input type="text" class="form-control global_especial_obligado" name="e_media_searches" id="e_media_searches">
			        			<br>
			        		</div>
                			<div class="col-md-4">
			        			<label>Office of the Inspector General *</label>
			        			<input type="text" class="form-control global_especial_obligado" name="e_oig" id="e_oig">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Interpol check  *</label>
			        			<input type="text" class="form-control global_especial_obligado" name="e_interpol" id="e_interpol">
			        			<br>
			        		</div>
				        </div>
				       	<div class="row">
			        		<div class="col-md-12">
			        			<label>Comentarios *</label>
			        			<input type="text" class="form-control global_especial_obligado" name="global_especial_comentarios" id="global_especial_comentarios">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGlobalesEspecial">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="globalesEspecificosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Búsquedas globales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_globales_especifico">
                		<div class="row">
                			<div class="col-md-4">
			        			<label>Office of the Inspector General  *</label>
			        			<input type="text" class="form-control global_especifico_obligado" name="x_oig" id="x_oig">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>General Services Administration Sanction *</label>
			        			<input type="text" class="form-control global_especifico_obligado" name="x_sanctions" id="x_sanctions">
			        			<br>
			        		</div>
                			<div class="col-md-4">
			        			<label>FACIS Sanction Search *</label>
			        			<input type="text" class="form-control global_especifico_obligado" name="x_facis" id="x_facis">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Bureau of Industry and Security List of Denied Persons  *</label>
			        			<input type="text" class="form-control global_especifico_obligado" name="x_bureau" id="x_bureau">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>EU Freeze List Maintained by the European Union (financial sanctions)  *</label>
			        			<input type="text" class="form-control global_especifico_obligado" name="x_european_financial" id="x_european_financial">
			        			<br>
			        		</div>
				        </div>
				       	<div class="row">
			        		<div class="col-md-12">
			        			<label>Comentarios *</label>
			        			<input type="text" class="form-control global_especifico_obligado" name="global_especifico_comentarios" id="global_especifico_comentarios">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGlobalesEspecifico">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="globalesSanctionsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Búsquedas globales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_globales_sanctions">
                		<div class="row">
                			<div class="col-md-6">
			        			<label>USA sanctions  *</label>
			        			<input type="text" class="form-control global_sanctions_obligado" name="usa_sanctions" id="usa_sanctions">
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label>Global sanctions *</label>
			        			<input type="text" class="form-control global_sanctions_obligado" name="global_sanctions" id="global_sanctions">
			        			<br>
			        		</div>
                			
			        	</div>
				       	<div class="row">
			        		<div class="col-md-12">
			        			<label>Comentarios *</label>
			        			<input type="text" class="form-control global_sanctions_obligado" name="global_sanctions_comentarios" id="global_sanctions_comentarios">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGlobalesSanctions">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="globalesInternalModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Búsquedas globales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_globales_internal">
                		<div class="row">
                			<div class="col-md-4">
			        			<label>Law enforcement *</label>
			        			<input type="text" class="form-control global_internal_obligado" name="law_enforcement_internal" id="law_enforcement_internal">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Regulatory *</label>
			        			<input type="text" class="form-control global_internal_obligado" name="regulatory_internal" id="regulatory_internal">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Sanctions  *</label>
			        			<input type="text" class="form-control global_internal_obligado" name="sanctions_internal" id="sanctions_internal">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Other bodies  *</label>
			        			<input type="text" class="form-control global_internal_obligado" name="other_bodies_internal" id="other_bodies_internal">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Web and media searches *</label>
			        			<input type="text" class="form-control global_internal_obligado" name="media_searches_internal" id="media_searches_internal">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>FDA department search *</label>
			        			<input type="text" class="form-control global_internal_obligado" name="global_fda" id="global_fda">
			        			<br>
			        		</div>
				        </div>
				       	<div class="row">
			        		<div class="col-md-12">
			        			<label>Comentarios *</label>
			        			<input type="text" class="form-control global_internal_obligado" name="global_internal_comentarios" id="global_internal_comentarios">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGlobalesInternal">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="AddressModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Historial de domicilios del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<?php 
		      		for($i = 1; $i <= 8; $i++){ ?>
		      			<form id="d_address<?php echo $i; ?>">
			      			<div class="alert alert-info">
			      				<p class="text-center"><?php echo 'Direccion #'.$i; ?></p>
			      			</div>
			      			<div class="row">
				        		<div class="col-md-4">
				        			<label>Periodo</label>
				        			<input type="text" class="form-control address_obligado<?php echo $i; ?>" name="address_periodo<?php echo $i; ?>" id="address_periodo<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Causa de salida *</label>
				        			<input type="text" class="form-control address_obligado<?php echo $i; ?>" name="address_causa<?php echo $i; ?>" id="address_causa<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Calle *</label>
				        			<input type="text" class="form-control address_obligado<?php echo $i; ?>" name="address_calle<?php echo $i; ?>" id="address_calle<?php echo $i; ?>">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-4">
				        			<label>No. Exterior *</label>
				        			<input type="text" class="form-control address_obligado<?php echo $i; ?>" name="address_exterior<?php echo $i; ?>" id="address_exterior<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>No. Interior *</label>
				        			<input type="text" class="form-control address_obligado<?php echo $i; ?>" name="address_interior<?php echo $i; ?>" id="address_interior<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Colonia *</label>
				        			<input type="text" class="form-control address_obligado<?php echo $i; ?>" name="address_colonia<?php echo $i; ?>" id="address_colonia<?php echo $i; ?>">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-4">
				        			<label>Estado *</label>
				        			<select name="address_estado<?php echo $i; ?>" id="address_estado<?php echo $i; ?>" class="form-control address_obligado<?php echo $i; ?> address_estado">
							            <option value="">Select</option>
							            <?php foreach ($estados as $e) {?>
							                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>Municipio *</label>
				        			<select name="address_municipio<?php echo $i; ?>" id="address_municipio<?php echo $i; ?>" class="form-control address_obligado<?php echo $i; ?>" disabled>
							            <option value="-1">Select</option>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label>CP *</label>
				        			<input type="text" class="form-control address_obligado<?php echo $i; ?>" name="address_cp<?php echo $i; ?>" id="address_cp<?php echo $i; ?>" maxlength="5">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-4 col-md-offset-5">
				        			<button type="button" class="btn btn-primary" onclick="guardarAddress(<?php echo $i; ?>)">Guardar</button>
				        			<input type="hidden" id="idAddress<?php echo $i; ?>">
				        			<br><br>
				        		</div>
				        	</div>
				        	<div id="msj_error<?php echo $i; ?>" class="alert alert-danger" style="display: none;">
				      			<p id="msj_texto<?php echo $i; ?>" class="text-white"></p>
				      		</div>
				        </form>
		      		<?php
		      		}
		      		?>
		      		<div class="row">
		        		<div class="col-md-12">
		        			<label>Comentarios *</label>
		        			<input type="text" class="form-control address_comentario_obligado" name="address_comentarios" id="address_comentarios">
		        			<br>
		        		</div>
			        </div>
			        <div class="row">
		        		<div class="col-md-4 col-md-offset-5">
		        			<button type="button" class="btn btn-primary" onclick="guardarComentarioAddress(<?php echo $i; ?>)">Guardar comentario</button><br><br>
		        		</div>
		        	</div>
			   		<div id="msj_error_comentario2" class="alert alert-danger" style="display: none;">
		      			<p id="msj_texto_comentario2" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="documentosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Documentos del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_documentos">
		      			<div class="alert alert-info">
		      				<p class="text-center">Comprobante de estudios (cedula, titulo o constancia) <br><div class="text-center" id="doc_estudios"></div></p>
		      			</div>
						<div class="row">
							<div class="col-md-6">
                				<label>Número de documento</label>
                				<input type="text" class="form-control" name="lic_profesional" id="lic_profesional">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="lic_institucion" id="lic_institucion">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">INE (o ID) <br><div class="text-center" id="doc_ine"></div></p>
		      			</div>
						<div class="row">
							<div class="col-md-4">
                				<label>ID o clave</label>
                				<input type="text" class="form-control" name="ine_clave" id="ine_clave" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" maxlength="18">
			        			<br>
                			</div>
							<div class="col-md-4">
                				<label>Año de registro</label>
                				<input type="text" class="form-control solo_numeros" name="ine_registro" id="ine_registro" maxlength="4">
			        			<br>
                			</div>
                			<div class="col-md-4">
                				<label>Número vertical</label>
                				<input type="text" class="form-control solo_numeros" name="ine_vertical" id="ine_vertical" maxlength="13">
			        			<br>
                			</div>
                		</div>
                		<div class="row">
                			<div class="col-md-4">
                				<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="ine_institucion" id="ine_institucion">
			        			<br>
                			</div>
						</div>
						<div class="alert alert-info">
		      				<p class="text-center">Pasaporte <br><div class="text-center" id="doc_pasaporte"></div></p>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-6">
                				<label>Número de documento</label>
                				<input type="text" class="form-control" name="pasaporte_numero" id="pasaporte_numero">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="pasaporte_institucion" id="pasaporte_institucion">
			        			<br><br>
                			</div>
                		</div>
						<div class="alert alert-info">
		      				<p class="text-center">Carta de no antecedentes penales (carta policía) <br><div class="text-center" id="doc_penales"></div></p>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-6">
                				<label>Número de documento</label>
                				<input type="text" class="form-control" name="penales_numero" id="penales_numero">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="penales_institucion" id="penales_institucion">
			        			<br><br>
                			</div>
                		</div>
                		<div class="alert alert-info">
		      				<p class="text-center">Comprobante de domicilio (servicio de electricidad o agua) <br><div class="text-center" id="doc_domicilio"></div></p>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-6">
                				<label>Número de documento</label>
                				<input type="text" class="form-control" name="domicilio_numero" id="domicilio_numero">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="domicilio_fecha" id="domicilio_fecha">
			        			<br><br>
                			</div>
                		</div>
                		<div class="alert alert-info">
		      				<p class="text-center">Cartilla o carta militar <br><div class="text-center" id="doc_militar"></div></p>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-6">
                				<label>Número de documento</label>
                				<input type="text" class="form-control" name="militar_numero" id="militar_numero">
			        			<br>
                			</div>
                			<div class="col-md-6">
                				<label>Fecha / Institución</label>
			        			<input type="text" class="form-control" name="militar_fecha" id="militar_fecha">
			        			<br><br>
                			</div>
                		</div>
                		<div class="alert alert-info">
		      				<p class="text-center">Comentarios * </p>
		      			</div>
		      			<div class="row">
		      				<div class="col-md-12">
                				<textarea class="form-control documento_obligado" name="doc_comentarios" id="doc_comentarios" rows="2"></textarea>
			        			<br>
                			</div>
                		</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarDocumentacion">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="ScopeModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Alcance de las verificaciones del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_checklist">
                		<div class="row">
                			<div class="col-md-4">
								<label>Education *</label>
								<textarea class="form-control list_check" name="check_education" id="check_education" rows="2"></textarea>
								<br>
							</div>
			        		<div class="col-md-4">
								<label>Employment *</label>
								<textarea class="form-control list_check" name="check_employment" id="check_employment" rows="2"></textarea>
								<br>
							</div>
			        		<div class="col-md-4">
								<label>Address *</label>
								<textarea class="form-control list_check" name="check_address" id="check_address" rows="2"></textarea>
								<br>
							</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
								<label>Criminal *</label>
								<textarea class="form-control list_check" name="check_criminal" id="check_criminal" rows="2"></textarea>
								<br>
							</div>
			        		<div class="col-md-4">
								<label>Database *</label>
								<textarea class="form-control list_check" name="check_database" id="check_database" rows="2"></textarea>
								<br>
							</div>
			        		<div class="col-md-4">
								<label>Identity *</label>
								<textarea class="form-control list_check" name="check_identity" id="check_identity" rows="2"></textarea>
								<br>
							</div>
				        </div>
				        <div class="row">
							<div class="col-md-4">
								<label>Military service *</label>
								<textarea class="form-control list_check" name="check_military" id="check_military" rows="2"></textarea>
								<br>
							</div>
							<div class="col-md-4">
								<label>Other checks *</label>
								<textarea class="form-control list_check" name="check_other" id="check_other" rows="2"></textarea>
								<br>
							</div>
						</div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" onclick="actualizarChecklist()">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="generalesInternacionalModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Datos generales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_generales_internacional">
                		<div class="row">
			        		<div class="col-md-4">
			        			<label>Name *</label>
			        			<input type="text" class="form-control inter_personal_obligado" name="nombre_internacional" id="nombre_internacional" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>First lastname *</label>
			        			<input type="text" class="form-control inter_personal_obligado" name="paterno_internacional" id="paterno_internacional" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Second lastname </label>
			        			<input type="text" class="form-control" name="materno_internacional" id="materno_internacional" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Birthdate *</label>
			        			<input type="text" class="form-control inter_personal_obligado" name="fecha_nacimiento_internacional" id="fecha_nacimiento_internacional">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Age *</label>
			        			<input type="text" class="form-control" id="edad_internacional" disabled>
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Nationality *</label>
			        			<input type="text" class="form-control inter_personal_obligado" name="nacionalidad_internacional" id="nacionalidad_internacional">
			        			<br>
			        		</div>
			        	</div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Job Position Requested *</label>
			        			<input type="text" class="form-control inter_personal_obligado" name="puesto_internacional" id="puesto_internacional">
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Gender: *</label>
			        			<select name="genero_internacional" id="genero_internacional" class="form-control inter_personal_obligado">
						            <option value="">Select</option>
						            <option value="Male">Male</option>
						            <option value="Female">Female</option>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Marital Status *</label>
			        			<select name="civil_internacional" id="civil_internacional" class="form-control inter_personal_obligado">
						            <option value="">Select</option>
					            	<option value="1">Married</option>
						            <option value="2">Single</option>
						            <option value="3">Divorced</option>
						            <option value="4">Free union</option>
						            <option value="5">Widowed</option>
						            <option value="6">Separated</option>
					          	</select>
					          	<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-12">
			        			<label>Address *</label>
			        			<input type="text" class="form-control inter_personal_obligado" name="domicilio_internacional" id="domicilio_internacional">
			        			<br>
			        		</div>
			        	</div>	
				        <div class="row">
				        	<div class="col-md-4">
			        			<label>Country *</label>
			        			<select name="pais_internacional" id="pais_internacional" class="form-control inter_personal_obligado">
						            <option value="">Select</option>
						            <?php foreach ($paises as $pais) {?>
						                <option value="<?php echo $pais->nombre; ?>"><?php echo $pais->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Mobile Number *</label>
			        			<input type="text" class="form-control solo_numeros inter_personal_obligado" name="celular_internacional" id="celular_internacional" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Home Number </label>
			        			<input type="text" class="form-control solo_numeros" name="tel_casa_internacional" id="tel_casa_internacional" maxlength="10">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label>Number to leave Messages </label>
			        			<input type="text" class="form-control solo_numeros" name="tel_oficina_internacional" id="tel_oficina_internacional" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Email *</label>
			        			<input type="text" class="form-control inter_personal_obligado" name="personales_correo_internacional" id="personales_correo_internacional">
			        			<br>
			        		</div>
				        </div>
				   	</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        		<button type="button" class="btn btn-success" id="guardarGeneralesInternacional">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="AddressInternacionalModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Historial de domicilios del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<?php 
		      		for($i = 1; $i <= 8; $i++){ ?>
		      			<form id="d_address_internacional<?php echo $i; ?>">
			      			<div class="alert alert-info">
			      				<p class="text-center"><?php echo 'Direccion #'.$i; ?></p>
			      			</div>
			      			<div class="row">
				        		<div class="col-md-6">
				        			<label>Periodo</label>
				        			<input type="text" class="form-control address_obligado_internacional<?php echo $i; ?>" name="address_periodo_internacional<?php echo $i; ?>" id="address_periodo_internacional<?php echo $i; ?>">
				        			<br>
				        		</div>
				        		<div class="col-md-6">
				        			<label>Causa de salida *</label>
				        			<input type="text" class="form-control address_obligado_internacional<?php echo $i; ?>" name="address_causa_internacional<?php echo $i; ?>" id="address_causa_internacional<?php echo $i; ?>">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-12">
				        			<label>Domicilio *</label>
				        			<input type="text" class="form-control address_obligado_internacional<?php echo $i; ?>" name="address_domicilio_internacional<?php echo $i; ?>" id="address_domicilio_internacional<?php echo $i; ?>">
				        			<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-6">
				        			<label>País *</label>
				        			<select name="address_pais_internacional<?php echo $i; ?>" id="address_pais_internacional<?php echo $i; ?>" class="form-control address_obligado_internacional<?php echo $i; ?>">
							            <option value="">Select</option>
							            <?php foreach ($paises as $p) {?>
							                <option value="<?php echo $p->nombre; ?>"><?php echo $p->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
				        		</div>
				        	</div>
				        	<div class="row">
				        		<div class="col-md-4 col-md-offset-5">
				        			<button type="button" class="btn btn-primary" onclick="guardarAddressInternacional(<?php echo $i; ?>)">Guardar</button>
				        			<input type="hidden" id="idAddressInternacional<?php echo $i; ?>">
				        			<br><br>
				        		</div>
				        	</div>
				        	<div id="msj_error<?php echo $i; ?>" class="alert alert-danger" style="display: none;">
				      			<p id="msj_texto<?php echo $i; ?>" class="text-white"></p>
				      		</div>
				        </form>
		      		<?php
		      		}
		      		?>
		      		<div class="row">
		        		<div class="col-md-12">
		        			<label>Comentarios *</label>
		        			<input type="text" class="form-control address_internacional_comentario_obligado" name="address_comentarios_internacional" id="address_comentarios_internacional">
		        			<br>
		        		</div>
			        </div>
			        <div class="row">
		        		<div class="col-md-4 col-md-offset-5">
		        			<button type="button" class="btn btn-primary" onclick="guardarComentarioAddressInternacional(<?php echo $i; ?>)">Guardar comentario</button><br><br>
		        		</div>
		        	</div>
			   		<div id="msj_error_comentario2" class="alert alert-danger" style="display: none;">
		      			<p id="msj_texto_comentario2" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="refProfesionalesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Referencias profesionales del candidato: <span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<form id="d_refprofesional1">
		      			<div class="alert alert-info">
		      				<p class="text-center">Primer referencia </p>
		      			</div>
                		<div class="row">
							<div class="col-md-12">
								<label>Nombre *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_nombre" id="refpro1_nombre">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
			        			<label>Teléfono *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_telefono" id="refpro1_telefono" maxlength="16">
			        			<br>
			        		</div>
							<div class="col-md-6">
				        		<label>Tiempo de conocerlo(a) *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_tiempo" id="refpro1_tiempo">
			        			<br>
				        	</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-6">
				        		<label>Dónde lo(a) conoció *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_conocido" id="refpro1_conocido">
			        			<br>
				        	</div>
							<div class="col-md-6">
			        			<label>Qué puesto desempeñaba *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_puesto" id="refpro1_puesto">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="alert alert-warning">
		      				<p class="text-center">Los siguientes campos deben ser completados por parte de la analista para verificar la referencia profesional </p>
		      			</div>
			        	<div class="row">
			        		<div class="col-md-6">
				        		<label>Tiempo de conocer al candidato *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_tiempo2" id="refpro1_tiempo2">
			        			<br>
				        	</div>
				        	<div class="col-md-6">
				        		<label>Dónde conoció al candidato *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_conocido2" id="refpro1_conocido2">
			        			<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-6">
			        			<label>Qué puesto desempeñaba *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_puesto2" id="refpro1_puesto2">
			        			<br>
			        		</div>
				        	<div class="col-md-6">
								<label>Cualidades del candidato *</label>
			        			<input type="text" class="form-control refpro1_obligado" name="refpro1_cualidades" id="refpro1_cualidades">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
				        		<label>¿Cómo fue el desempeño del candidato? *</label>
			        			<select name="refpro1_desempeno" id="refpro1_desempeno" class="form-control refpro1_obligado">
						            <option value="">Selecciona</option>
						            <option value="Not provided">Not provided</option>
						            <option value="Excellent">Excellent</option>
						            <option value="Good">Good</option>
						            <option value="Regular">Regular</option>
						            <option value="Bad">Bad</option>
						            <option value="Very Bad">Very Bad</option>
					          	</select>
					          	<br>
				        	</div>
			        		<div class="col-md-6">
				        		<label>¿Recomienda al candidato? *</label>
			        			<select name="refpro1_recomienda" id="refpro1_recomienda" class="form-control refpro1_obligado">
						            <option value="">Selecciona</option>
						            <option value="No">No</option>
						            <option value="Yes">Yes</option>
						            <option value="N/A">N/A</option>
					          	</select>
					          	<br>
				        	</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Comentarios de la referencia *</label>
			        			<textarea class="form-control refpro1_obligado" name="refpro1_comentario" id="refpro1_comentario" rows="3"></textarea>
			        			<br>
							</div>
						</div>
						<div id="msj_error1" class="alert alert-danger" style="display: none;">
			      			<p id="msj_texto1" class="text-white"></p>
			      		</div>
						<div class="row">
				        	<div class="col-md-3 col-md-offset-4">
				        		<button type="button" class="btn btn-primary" onclick="guardarRefProfesional(1)">Actualizar Referencia Profesional #1</button>
								<input type="hidden" id="id_refpro1">
				        		<br><br>
				        	</div>
				        </div>
						<br>
					</form>
					<form id="d_refprofesional2">
		      			<div class="alert alert-info">
		      				<p class="text-center">Segunda referencia </p>
		      			</div>
                		<div class="row">
							<div class="col-md-12">
								<label>Nombre *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_nombre" id="refpro2_nombre">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
			        			<label>Teléfono *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_telefono" id="refpro2_telefono" maxlength="16">
			        			<br>
			        		</div>
							<div class="col-md-6">
				        		<label>Tiempo de conocerlo(a) *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_tiempo" id="refpro2_tiempo">
			        			<br>
				        	</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-6">
				        		<label>Dónde lo(a) conoció *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_conocido" id="refpro2_conocido">
			        			<br>
				        	</div>
							<div class="col-md-6">
			        			<label>Qué puesto desempeñaba *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_puesto" id="refpro2_puesto">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="alert alert-warning">
		      				<p class="text-center">Los siguientes campos deben ser completados por parte de la analista para verificar la referencia profesional </p>
		      			</div>
			        	<div class="row">
			        		<div class="col-md-6">
				        		<label>Tiempo de conocer al candidato *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_tiempo2" id="refpro2_tiempo2">
			        			<br>
				        	</div>
				        	<div class="col-md-6">
				        		<label>Dónde conoció al candidato *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_conocido2" id="refpro2_conocido2">
			        			<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-6">
			        			<label>Qué puesto desempeñaba *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_puesto2" id="refpro2_puesto2">
			        			<br>
			        		</div>
				        	<div class="col-md-6">
								<label>Cualidades del candidato *</label>
			        			<input type="text" class="form-control refpro2_obligado" name="refpro2_cualidades" id="refpro2_cualidades">
			        			<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
				        		<label>¿Cómo fue el desempeño del candidato? *</label>
			        			<select name="refpro2_desempeno" id="refpro2_desempeno" class="form-control refpro2_obligado">
						            <option value="">Selecciona</option>
						            <option value="Not provided">Not provided</option>
						            <option value="Excellent">Excellent</option>
						            <option value="Good">Good</option>
						            <option value="Regular">Regular</option>
						            <option value="Bad">Bad</option>
						            <option value="Very Bad">Very Bad</option>
					          	</select>
					          	<br>
				        	</div>
			        		<div class="col-md-6">
				        		<label>¿Recomienda al candidato? *</label>
			        			<select name="refpro2_recomienda" id="refpro2_recomienda" class="form-control refpro2_obligado">
						            <option value="">Selecciona</option>
						            <option value="No">No</option>
						            <option value="Yes">Yes</option>
						            <option value="N/A">N/A</option>
					          	</select>
					          	<br>
				        	</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Comentarios de la referencia *</label>
			        			<textarea class="form-control refpro2_obligado" name="refpro2_comentario" id="refpro2_comentario" rows="3"></textarea>
			        			<br>
							</div>
						</div>
						<div id="msj_error2" class="alert alert-danger" style="display: none;">
			      			<p id="msj_texto2" class="text-white"></p>
			      		</div>
						<div class="row">
				        	<div class="col-md-3 col-md-offset-4">
				        		<button type="button" class="btn btn-primary" onclick="guardarRefProfesional(2)">Actualizar Referencia Profesional #2</button>
								<input type="hidden" id="id_refpro2">
				        		<br><br>
				        	</div>
				        </div>
						<br>
					</form>
			   		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        		<!--button type="button" class="btn btn-success" id="guardarSociales">Guardar</button-->
		      	</div>
		    </div>
	 	</div>
	</div>
    <div class="loader" style="display: none;"></div>
    <a href="#" class="scroll-to-top"><i class="fas fa-arrow-up"></i><span class="sr-only">Ir arriba</span></a>
	<section class="content" id="listado">
	  	<div class="row">
	        <div class="col-xs-12">
	          	<div class="box">
	            	<div class="box-header">
	              		<!--h3 class="box-title"><strong>  Propietarios</strong></h3-->
	            	</div>
	            	<!-- /.box-header -->
	            	<div class="box-body table-responsive">
	              		<table id="tabla" class="table stripe hover row-border cell-border"></table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
	          	<!-- /.box -->
	        </div>
	    </div>
	</section>
	<section class="content" id="formulario" style="display: none;">
		<input type="hidden" id="idCandidato">
		<input type="hidden" class="correo">
		<div class="row">
    		<div class="col-xs-12">
      			<div class="box">
        			<div class="box-body">
                		<div class="box-header tituloSubseccion">
                  			<p class="box-title"><strong>  Referencias Laborales </strong></p>
                		</div>
				        <div class="row">
				        	<div class="col-md-12">
				        		<div class="alert alert-warning"><h4 class="text-center">Break(s) in Employment</h4></div>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-12">
                				<textarea class="form-control trabajo_gobierno" name="trabajo_inactivo" id="trabajo_inactivo" rows="2"></textarea><br><br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-3 col-md-offset-3">
				        		<button type="button" class="btn btn-warning" onclick="actualizarTrabajoGobierno()">Guardar respuestas anteriores</button>
				        		<br><br>
				        	</div>
				        	<div class="col-md-3">
				        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
				        		<br><br><br><br>
				        	</div>
				        </div>
                		<?php 
                		for($i = 1; $i <= 8; $i++){ 
                			echo '<div class="box-header text-center tituloSubseccion">
	                  				<p class="box-title " id="titulo_reflab'.$i.'"><strong> Trabajo #'.$i.' </strong><hr></p>
            					</div>';
            				echo '<div class="row">
            						<form id="candidato_reflaboral'.$i.'">
	                					<div class="col-md-6">
	                						<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
	                						<div class="row">
	                							<div class="col-md-3">
					                				<label>Compañía: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
		                							<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_empresa_ingles" id="reflab'.$i.'_empresa_ingles" >
	        										<br>
		                						</div>
	                						</div>
	                						<div class="row">
				                				<div class="col-md-3">
					                				<label>Dirección: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_direccion_ingles" id="reflab'.$i.'_direccion_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Fecha de entrada: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control fecha_laboral reflabingles'.$i.'_obligado" name="reflab'.$i.'_entrada_ingles" id="reflab'.$i.'_entrada_ingles" placeholder="mm/dd/yyyy">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Fecha de salida: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control fecha_laboral reflabingles'.$i.'_obligado" name="reflab'.$i.'_salida_ingles" id="reflab'.$i.'_salida_ingles" placeholder="mm/dd/yyyy">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Teléfono: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control solo_numeros reflabingles'.$i.'_obligado" name="reflab'.$i.'_telefono_ingles" id="reflab'.$i.'_telefono_ingles" maxlength="10">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Puesto inicial: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_puesto1_ingles" id="reflab'.$i.'_puesto1_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Puesto final: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_puesto2_ingles" id="reflab'.$i.'_puesto2_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Salario inicial: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control solo_numeros reflabingles'.$i.'_obligado" name="reflab'.$i.'_salario1_ingles" id="reflab'.$i.'_salario1_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Salario final: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control solo_numeros reflabingles'.$i.'_obligado" name="reflab'.$i.'_salario2_ingles" id="reflab'.$i.'_salario2_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Jefe inmediato: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_jefenombre_ingles" id="reflab'.$i.'_jefenombre_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Correo del jefe inmediato: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_jefecorreo_ingles" id="reflab'.$i.'_jefecorreo_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Puesto del jefe inmediato:</label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_jefepuesto_ingles" id="reflab'.$i.'_jefepuesto_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3">
					                				<label>Causa de separación: </label>
					                				<br>
					                			</div>
					                			<div class="col-md-9">
					                				<input type="text" class="form-control reflabingles'.$i.'_obligado" name="reflab'.$i.'_separacion_ingles" id="reflab'.$i.'_separacion_ingles">
					        						<br>
					                			</div>
				                			</div>
				                			<div class="row">
				                				<div class="col-md-3 col-md-offset-4">
				                					<button type="button" class="btn btn-info" onclick="actualizarReferenciaLaboral('.$i.')">Guardar referencia laboral</button><br><br><br><br>
				                					<input type="hidden" id="idreflabingles'.$i.'">
				                				</div>
				                			</div>
				                		</div>
				                	</form>
				                	<form id="analista_reflaboral'.$i.'">
	                					<div class="col-md-6">
                							<div class="alert alert-warning"><h4 class="text-center">Analista</h4></div>
	                							<div class="row">
					                				<div class="col-md-3">
						                				<label>Compañía: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
									        			<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_empresa" id="an_reflab'.$i.'_empresa" >
									        			<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Dirección: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_direccion" id="an_reflab'.$i.'_direccion">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Fecha de entrada: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control fecha_laboral ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_entrada" id="an_reflab'.$i.'_entrada" placeholder="mm/dd/yyyy">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Fecha de salida: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control fecha_laboral ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_salida" id="an_reflab'.$i.'_salida" placeholder="mm/dd/yyyy">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Teléfono: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control solo_numeros ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_telefono" id="an_reflab'.$i.'_telefono" maxlength="10">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Puesto inicial: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_puesto1" id="an_reflab'.$i.'_puesto1">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Puesto final: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_puesto2" id="an_reflab'.$i.'_puesto2">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Salario inicial: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control solo_numeros ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_salario1" id="an_reflab'.$i.'_salario1">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Salario final: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control solo_numeros ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_salario2" id="an_reflab'.$i.'_salario2">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Jefe inmediato: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_jefenombre" id="an_reflab'.$i.'_jefenombre">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Correo del jefe inmediato: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_jefecorreo" id="an_reflab'.$i.'_jefecorreo">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Puesto del jefe inmediato: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_jefepuesto" id="an_reflab'.$i.'_jefepuesto">
						        						<br>
						                			</div>
					                			</div>
					                			<div class="row">
					                				<div class="col-md-3">
						                				<label>Causa de separación: </label>
						                				<br>
						                			</div>
						                			<div class="col-md-9">
						                				<input type="text" class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_separacion" id="an_reflab'.$i.'_separacion">
						        						<br>
						                			</div>
					                			</div>
					                		</div>
	                					</div>
	                					<div class="row">
				                			<div class="col-md-4 col-md-offset-4">
				                				<label>Notes *</label>
				                				<textarea class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_notas" id="an_reflab'.$i.'_notas" rows="3"></textarea><br>
				                			</div>
				                		</div>
				                		<div class="row">
				                			<div class="col-md-4 col-md-offset-4">
				                				<label>Apply to all</label>
							        			<select id="aplicar_todo'.$i.'" class="form-control aplicar_todo">
										            <option value="-1">Select</option>
										            <option value="0">Not provided</option>
										            <option value="1">Excellent</option>
										            <option value="2">Good</option>
										            <option value="3">Regular</option>
										            <option value="4">Bad</option>
										            <option value="5">Very Bad</option>
									          	</select>
							        			<br><br>
				                			</div>
				                		</div>
				                		<div class="row">
				                			<div class="col-md-3">
				                				<label>Responsability *</label>
							        			<select name="an_reflab'.$i.'_responsabilidad" id="an_reflab'.$i.'_responsabilidad" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-3">
				                				<label>Initiative *</label>
							        			<select name="an_reflab'.$i.'_iniciativa" id="an_reflab'.$i.'_iniciativa" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-3">
				                				<label>Work efficiency *</label>
							        			<select name="an_reflab'.$i.'_eficiencia" id="an_reflab'.$i.'_eficiencia" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-3">
				                				<label>Discipline *</label>
							        			<select name="an_reflab'.$i.'_disciplina" id="an_reflab'.$i.'_disciplina" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                		</div>
				                		<div class="row">
				                			<div class="col-md-3">
				                				<label>Punctuality and assistance *</label>
							        			<select name="an_reflab'.$i.'_puntualidad" id="an_reflab'.$i.'_puntualidad" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-3">
				                				<label>Cleanliness and order *</label>
							        			<select name="an_reflab'.$i.'_limpieza" id="an_reflab'.$i.'_limpieza" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-3">
				                				<label>Stability *</label>
							        			<select name="an_reflab'.$i.'_estabilidad" id="an_reflab'.$i.'_estabilidad" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-3">
				                				<label>Emotional Stability *</label>
							        			<select name="an_reflab'.$i.'_emocional" id="an_reflab'.$i.'_emocional" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                		</div>
				                		<div class="row">
				                			<div class="col-md-3">
				                				<label>Honesty *</label>
							        			<select name="an_reflab'.$i.'_honesto" id="an_reflab'.$i.'_honesto" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-3">
				                				<label>Performance *</label>
							        			<select name="an_reflab'.$i.'_rendimiento" id="an_reflab'.$i.'_rendimiento" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                			<div class="col-md-4">
				                				<label>Attitude with coworkers, bosses and subordinates *</label>
							        			<select name="an_reflab'.$i.'_actitud" id="an_reflab'.$i.'_actitud" class="form-control performance'.$i.' ver_reflab'.$i.'_obligado">
										            <option value="Not provided">Not provided</option>
			                                        <option value="Excellent">Excellent</option>
			                                        <option value="Good">Good</option>
			                                        <option value="Regular">Regular</option>
			                                        <option value="Bad">Bad</option>
			                                        <option value="Very Bad">Very Bad</option>
									          	</select>
							        			<br>
				                			</div>
				                		</div>
				                		<div class="row">
			                				<div class="col-md-4">
			                					<label>In case of vacancy would you hire her/him again? *</label>
							        			<select name="an_reflab'.$i.'_recontratacion" id="an_reflab'.$i.'_recontratacion" class="form-control ver_reflab'.$i.'_obligado">
										            <option value="">Select</option>
										            <option value="0">No</option>
										            <option value="1">Yes</option>
									          	</select>
							        			<br><br><br>
			                				</div>
			                				<div class="col-md-8">
			                					<label>Why? *</label>
			                					<textarea class="form-control ver_reflab'.$i.'_obligado" name="an_reflab'.$i.'_motivo" id="an_reflab'.$i.'_motivo" rows="3"></textarea>
			                					<br><br><br>
			                				</div>
			                			</div>
            						</form>
			            			<div class="row">
							        	<div class="col-md-3 col-md-offset-3">
							        		<button type="button" class="btn btn-warning" onclick="verificarLaboral('.$i.')">Guardar la verificación del trabajo #'.$i.'</button>
							        		<br><br><input type="hidden" id="idverlab'.$i.'">
							        	</div>
							        	<div class="col-md-3">
							        		<button type="button" class="btn btn-default" onclick="regresarListado()">Regresar al listado</button>
							        		<br><br><br><br>
							        	</div>
							        </div>';
                		}
                		?>
        			</div>
        		</div>
        	</div>
        </div>
	</section>
	
</div>
<!-- /.content-wrapper -->	
<script>
	var url = '<?php echo base_url('Cliente/HclGet'); ?>';
  	$(document).ready(function(){
  		$('#fecha_nacimiento').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
  		$('.fecha_laboral').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
  		var msj = localStorage.getItem("success");
  		var msj2 = localStorage.getItem("finished");
  		if(msj == 1){
  			$("#exitoCandidato").css('display','block');
  			setTimeout(function(){
          		$('#exitoCandidato').fadeOut();
        	},4000);
        	localStorage.removeItem("success");
  		}
  		if(msj2 == 1){
  			$("#exitoFinalizado").css('display','block');
  			setTimeout(function(){
          		$('#exitoFinalizado').fadeOut();
        	},4000);
        	localStorage.removeItem("finished");
  		}
    	$('#tabla').DataTable({
      		"pageLength": 25,
	      	"pagingType": "simple",
	      	"order": [0, "desc"],
	      	"stateSave": true,
	      	"serverSide": false,
	      	"ajax": url,
	      	"columns":[ 
	      		{ title: 'id', data: 'id', visible: false },
	        	{ title: 'Candidato', data: 'candidato', "width": "15%" },
	        	{ title: 'Proyecto', data: 'proyecto' },
	        	{ title: 'Fechas', data: 'fecha_alta', "width": "10%",
	        		mRender: function(data, type, full){
	        			var f = data.split(' ');
            			var h = f[1];
            			var aux = h.split(':');
            			var hora = aux[0]+':'+aux[1];
            			var aux = f[0].split('-');
            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
            			var f_inicio = fecha+' '+hora;
            			if(full.fecha_final != null){
            				var f = full.fecha_final.split(' ');
            				var h = f[1];
	            			var aux = h.split(':');
	            			var hora = aux[0]+':'+aux[1];
	            			var aux = f[0].split('-');
	            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
	            			var f_fin = "Final: "+fecha+' '+hora;
	        				return "Alta: "+f_inicio+'<br>'+f_fin;
            			}
            			else{
            				var f = new Date();
							var hoy = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
							return "Alta: "+f_inicio+'<br>Final: -';
            			}
	        		}
	        	},
	        	{ title: 'Tiempo del proceso', data: 'tiempo', "width": "8%",
	        		mRender: function(data, type, full){
	        			if(data != null){
	        				if(data != -1){
	        					if(data >= 0 && data <= 2){
	            					return res = '<div class="formato_dias dias_verde">'+data+' días</div>';
	            				}
	            				if(data > 2 && data <= 4){
	            					return res = '<div class="formato_dias dias_amarillo">'+data+' días</div>';
	            				}
	            				if(data >= 5){
	            					return res = '<div class="formato_dias dias_rojo">'+data+' días</div>';
	            				}
	        				}
            				else{
            					return "Actualizando...";
            				}
            			}
            			else{
            				if(full.tiempo_parcial != 0){
            					var parcial = full.tiempo_parcial;
	            				if(parcial >= 0 && parcial <= 2){
	            					return res = '<div class="formato_dias dias_verde">'+parcial+' días</div>';
	            				}
	            				if(parcial >= 2 && parcial <= 4){
	            					return res = '<div class="formato_dias dias_amarillo">'+parcial+' días</div>';
	            				}
	            				if(parcial >= 5){
	            					return res = '<div class="formato_dias dias_rojo">'+parcial+' días</div>';
	            				}
            				}
            				else{
            					return "Actualizando...";
            				}
            			}
          			}
	        	},
	        	{ title: 'Fecha formulario', data: 'fecha_contestado', "width": "12%",
	        		mRender: function(data, type, full){
	        			if(full.id_proyecto != 25 && full.id_proyecto != 135 && full.id_proyecto != 136 && full.id_proyecto != 137 && full.id_proyecto != 138 && full.id_proyecto != 140 && full.id_proyecto != 147 && full.id_proyecto != 148){
		        			if(data == "" || data == null){
		        				return "<i class='fas fa-circle estatus0'></i>Sin contestar";
		        			}
		        			else{
		        				var f = data.split(' ');
	            				var h = f[1];
	            				var aux = h.split(':');
	            				var hora = aux[0]+':'+aux[1];
	            				var aux = f[0].split('-');
	            				var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
	            				var tiempo = fecha+' '+hora;
	        					return "<i class='fas fa-circle estatus1'></i>"+tiempo;
		        			}
		        		}
		        		else{
		        			return "<i class='fas fa-circle status_bgc0'></i> N/A";
		        		}
          			}
	        	},
	        	{ title: 'Fecha documentos', data: 'fecha_documentos', "width": "12%",
	        		mRender: function(data, type, full){
	        			if(full.id_proyecto != 25 && full.id_proyecto != 135 && full.id_proyecto != 136 && full.id_proyecto != 137 && full.id_proyecto != 138 && full.id_proyecto != 140 && full.id_proyecto != 147 && full.id_proyecto != 148){
		        			if(data == "" || data == null){
		        				return "<i class='fas fa-circle estatus0'></i>En espera";
		        			}
		        			else{
		        				var f = data.split(' ');
	            				var h = f[1];
	            				var aux = h.split(':');
	            				var hora = aux[0]+':'+aux[1];
	            				var aux = f[0].split('-');
	            				var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
	            				var tiempo = fecha+' '+hora;
	        					return "<i class='fas fa-circle estatus1'></i>"+tiempo;
		        			}
		        		}
		        		else{
		        			return "<i class='fas fa-circle status_bgc0'></i> N/A";
		        		}
          			}
	        	},
	        	{ title: 'Mensajes', data: 'id', bSortable: false, "width": "8%",
         			mRender: function(data, type, full) {
         				if(full.id_proyecto != 25 && full.id_proyecto != 128 && full.id_proyecto != 135 && full.id_proyecto != 136 && full.id_proyecto != 137 && full.id_proyecto != 138 && full.id_proyecto != 140 && full.id_proyecto != 147 && full.id_proyecto != 148){
	         				if(full.status == 0){
	                			return '<a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Generar password" id="generar" class="fa-tooltip a-acciones"><i class="fas fa-key"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Registrar llamada al candidato" id="llamada" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Emails to candidate" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
	            			}
	            			if(full.status > 0){
	                			return '<a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Registrar llamada al candidato" id="llamada" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Emails to candidate" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
	            			}
	            		}
	            		if(full.id_proyecto == 128 || full.id_proyecto == 135 || full.id_proyecto == 136 || full.id_proyecto == 137 || full.id_proyecto == 138 || full.id_proyecto == 140 || full.id_proyecto == 147 || full.id_proyecto == 148){
	            			return '<a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Registrar llamada al candidato" id="llamada" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Emails to candidate" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
	            		}
	            		if(full.id_proyecto == 25){
	            			return "<i class='fas fa-circle status_bgc0'></i> NA";
	            		}
          			}
        		},
	        	{ title: 'Estudio', data: 'id', bSortable: false, "width": "15%",
         			mRender: function(data, type, full) {
         				if(full.id_proyecto != 25){
         					if(full.cancelado == 1){
	         					return "<i class='fas fa-circle status_bgc0'></i> NA";
	         				}
	         				if(full.cancelado == 0){
	         					if(full.id_proyecto == 20){
         							if(full.status == 0){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
         						}
         						if(full.id_proyecto == 21){
         							if(full.status == 0){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
	         					}
         						if(full.id_proyecto == 23){
         							if(full.status == 0){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
         						}
         						if(full.id_proyecto == 24){
         							if(full.status == 0){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
	         					}
         						if(full.id_proyecto == 26){
         							if(full.status == 0){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
         						}
         						if(full.id_proyecto == 27){
         							if(full.status == 0){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
         						}
         						if(full.id_proyecto == 28){
         							if(full.status == 0){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
	         					}
	         					if(full.id_proyecto == 35){
	         							if(full.status == 0){
				                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
				            			if(full.status == 1){
				                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
				            			if(full.status == 2){
				            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
         						}
         						if(full.id_proyecto == 128){
         							if(full.status == 0 || full.status == 1){
			                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
			            			if(full.status == 2){
			            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_globales">Global data search</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
			            			}
	         					}
	         					if(full.id_proyecto == 135 || full.id_proyecto == 137){
         							return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="custom_database_check">Database check</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li></ul></div>';
	         					}
	         					if(full.id_proyecto == 136){
         							return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="custom_database_check">Database check</a></li><li><a href="#" id="penales">Verificacion de Antecedentes No Penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li></ul></div>';
	         					}
	         					if(full.id_proyecto == 138){
         							return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="custom_database_check">Database check</a></li><li><a href="#" id="custom_identidad">ID check</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li></ul></div>';
	         					}
	         					if(full.id_proyecto == 140){
         							return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="custom_identidad">ID check</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li></ul></div>';
	         					}
	         					if(full.id_proyecto == 147){
         							return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="custom_identidad">ID check</a></li><li><a href="#" id="custom_database_check">Database check</a></li><li><a href="#" id="custom_address_check">Address check</a></li><li><a href="#" id="penales">Verificacion de Antecedentes No Penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li></ul></div>';
	         					}
	         					if(full.id_proyecto == 148){
         							return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="custom_identidad">ID check</a></li><li><a href="#" id="custom_address_check">Address check</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li></ul></div>';
	         					}
	         					if(full.id_proyecto == 150 || full.id_proyecto == 151 || full.id_proyecto == 152 || full.id_proyecto == 153){
	         							if(full.status == 0){
				                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales_internacionales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
				            			if(full.status == 1){
				                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales_internacionales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li><a href="#" id="datos_ref_profesionales">Referencias profesionales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
				            			if(full.status == 2){
				            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales_internacionales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li><a href="#" id="datos_ref_profesionales">Referencias profesionales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
         						}
         						if(full.id_proyecto == 154){
	         							if(full.status == 0){
				                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales_internacionales">Datos generales</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
				            			if(full.status == 1){
				                			return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales_internacionales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li><a href="#" id="datos_credito">Historial crediticio</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
				            			if(full.status == 2){
				            				return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato <span class="caret"></span></button><ul class="dropdown-menu menu_estudio"><li><a href="#" id="datos_generales_internacionales">Datos generales</a></li><li><a href="#" id="datos_mayores_estudios">Mayores estudios</a></li><li><a href="#" id="datos_domicilios">Historial de domicilios</a></li><li><a href="#" id="datos_globales">Global data search</a></li><li><a href="#" id="datos_verificacion_docs">Verificación documentos</a></li><li><a href="#" id="datos_laborales">Referencias laborales</a></li><li><a href="#" id="datos_credito">Historial crediticio</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_gaps">GAPS</a></li><li role="separator" class="divider"></li><li><a href="#" id="estudios">Verificación de estudios</a></li><li><a href="#" id="laborales">Verificación de referencias laborales</a></li><li><a href="#" id="penales">Verificación de antecedentes no penales</a></li><li role="separator" class="divider"></li><li><a href="#" id="datos_checklist">Scope of Verification</a></li><li role="separator" class="divider"></li><li><a href="#" id="subirDocs">Documentación</a></li><li role="separator" class="divider"></li><li><a href="#" id="editar_final">Conclusiones</a></li><li role="separator" class="divider"></li><li><a href="#" id="eliminar">Cancelar proceso</a></li></ul></div>';
				            			}
         						}
		            		}
         				}
         				else{
         					return "<i class='fas fa-circle status_bgc0'></i> N/A";
         				}
          			}
        		},
        		{ title: 'Antidoping', data: 'id', bSortable: false, "width": "10%",
         			mRender: function(data, type, full) {
         				if(full.id_proyecto != 21 && full.id_proyecto != 135 && full.id_proyecto != 136  && full.id_proyecto != 138 && full.id_proyecto != 140){
         					if(full.fecha_resultado != null && full.fecha_resultado != ""){
	         					if(full.resultado_doping == 1){
	         						return "<i class='fas fa-circle status_bgc2'></i>No aprobado"; 
	         					}
	         					else{
	         						return "<i class='fas fa-circle status_bgc1'></i>Aprobado";
	         					}
	         					
	         				}
	         				else{
	         					return "<i class='fas fa-circle status_bgc0'></i>Pendiente";
	         				}
         				}
         				else{
         					return "N/A";
         				}
          			}
        		},
        		{ title: 'Proceso', data: 'id', bSortable: false, "width": "15%",
         			mRender: function(data, type, full) {
         				if(full.id_proyecto != 25){
         					if(full.id_proyecto != 128 && full.id_proyecto != 135 && full.id_proyecto != 136 && full.id_proyecto != 137 && full.id_proyecto != 138 && full.id_proyecto != 140 && full.id_proyecto != 147 && full.id_proyecto != 148 && full.id_proyecto != 150 && full.id_proyecto != 151 && full.id_proyecto != 152 && full.id_proyecto != 153 && full.id_proyecto != 154){
         						if(full.status == 0){
		     						return "<i class='fas fa-circle status_bgc0'></i>En espera";
		     					}
		     					else{
		     						if(full.status_bgc == 0){
			         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
			         				}
			         				if(full.status_bgc == 1){
			         					return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLGeneral'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 2){
			         					return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLGeneral'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 3){
			         					return '<i class="fas fa-circle status_bgc3"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLGeneral'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
		     					}
         					}
         					if(full.id_proyecto == 128){
         						if(full.status == 0){
		     						return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
		     					}
		     					else{
		     						if(full.status_bgc == 0){
			         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
			         				}
			         				if(full.status_bgc == 1){
			         					return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLInternalPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 2){
			         					return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLInternalPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 3){
			         					return '<i class="fas fa-circle status_bgc3"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLInternalPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
		     					}
         					}
         					if(full.id_proyecto == 135 || full.id_proyecto == 136 || full.id_proyecto == 137 || full.id_proyecto == 138 || full.id_proyecto == 140 || full.id_proyecto == 147 || full.id_proyecto == 148){
         						if(full.status == 0){
		     						return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
		     					}
		     					else{
		     						if(full.status_bgc == 0){
			         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
			         				}
			         				if(full.status_bgc == 1){
			         					return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLCustom'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 2){
			         					return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLCustom'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 3){
			         					return '<i class="fas fa-circle status_bgc3"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLCustom'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
		     					}
         					}
         					if(full.id_proyecto == 150 || full.id_proyecto == 151 || full.id_proyecto == 152 || full.id_proyecto == 153 || full.id_proyecto == 154){
         						if(full.status == 0){
		     						return "<i class='fas fa-circle status_bgc0'></i>En espera";
		     					}
		     					else{
		     						if(full.status_bgc == 0){
			         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
			         				}
			         				if(full.status_bgc == 1){
			         					return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLInternacional'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 2){
			         					return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLInternacional'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
			         				if(full.status_bgc == 3){
			         					return '<i class="fas fa-circle status_bgc3"></i> <div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLInternacional'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			         				}
		     					}
         					}
	     				}
	     				else{
	     					if(full.fecha_resultado != null && full.fecha_resultado != ""){
	         					if(full.resultado_doping == 1){
	         						return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;margin-left:10px;"><form id="pdf'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+full.idDoping+'" value="'+full.idDoping+'"></form></div>'; 
	         					}
	         					else{
	         						return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;margin-left:10px;"><form id="pdf'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+full.idDoping+'" value="'+full.idDoping+'"></form></div>'; 
	         					}
	         					
	         				}
	         				else{
	         					return "<i class='fas fa-circle status_bgc0'></i> Pendiente examen antidoping";
	         				}
	     				}
          			}
        		}     
	      	],
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$('a#datos_generales', row).bind('click', () => {
	      			$("#d_generales")[0].reset();
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$('.personal_obligado').removeClass('requerido');
		        	$("#nombre_general").val(data.nombre);
		        	$("#paterno_general").val(data.paterno);
		        	$("#materno_general").val(data.materno);
		        	if(data.fecha_nacimiento != "0000-00-00" && data.fecha_nacimiento != null){
			        	var f_nacimiento = convertirDate(data.fecha_nacimiento);
			        	$("#fecha_nacimiento").val(f_nacimiento);
			        	$("#edad").val(data.edad);
		        	}
		        	else{
		        		$("#fecha_nacimiento").val("");
			        	$("#edad").val("");
		        	}
		        	$("#puesto_general").val(data.puesto);
		        	$("#nacionalidad").val(data.nacionalidad);
		        	$("#genero").val(data.genero);
		        	$("#calle").val(data.calle);
		        	$("#exterior").val(data.exterior);
		        	$("#interior").val(data.interior);
		        	$("#colonia").val(data.colonia);
		        	$("#estado").val(data.id_estado);
		        	if(data.id_estado != "" && data.id_estado != null && data.id_estado != 0){
		        		getMunicipio(data.id_estado, data.id_municipio);
		            }
		            else{
		              $('#municipio').prop('disabled', true);
		            }
		            $("#cp").val(data.cp);
		        	$("#civil").val(data.id_estado_civil);
		        	$("#celular_general").val(data.celular);
		        	$("#tel_casa").val(data.telefono_casa);
		        	$("#tel_oficina").val(data.telefono_otro);
		        	$("#personales_correo").val(data.correo);
		        	$("#generalesModal").modal('show');
		        });
		        $('a#datos_generales_internacionales', row).bind('click', () => {
	      			$("#d_generales_internacional")[0].reset();
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			$('.personal_obligado').removeClass('requerido');
		        	$("#nombre_internacional").val(data.nombre);
		        	$("#paterno_internacional").val(data.paterno);
		        	$("#materno_internacional").val(data.materno);
		        	if(data.fecha_nacimiento != "0000-00-00" && data.fecha_nacimiento != null){
			        	var f_nacimiento = convertirFechaEspanol(data.fecha_nacimiento);
			        	$("#fecha_nacimiento_internacional").val(f_nacimiento);
			        	$("#edad_internacional").val(data.edad);
		        	}
		        	else{
		        		$("#fecha_nacimiento_internacional").val("");
			        	$("#edad_internacional").val("");
		        	}
		        	$("#puesto_internacional").val(data.puesto);
		        	$("#nacionalidad_internacional").val(data.nacionalidad);
		        	$("#genero_internacional").val(data.genero);
		        	$("#domicilio_internacional").val(data.domicilio_internacional);
		        	$("#pais_internacional").val(data.pais);
		        	$("#civil_internacional").val(data.id_estado_civil);
		        	$("#celular_internacional").val(data.celular);
		        	$("#tel_casa_internacional").val(data.telefono_casa);
		        	$("#tel_oficina_internacional").val(data.telefono_otro);
		        	$("#personales_correo_internacional").val(data.correo);
		        	$("#generalesInternacionalModal").modal('show');
		        });
		        $('a#datos_mayores_estudios', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	              	$("#idMayoresEstudios").val(data.idMayores);
	              	$("#mayor_estudios").val(data.id_grado_estudio);
	              	$("#estudios_periodo").val(data.estudios_periodo);
	              	$("#estudios_escuela").val(data.estudios_escuela);
	              	$("#estudios_ciudad").val(data.estudios_ciudad);
	              	$("#estudios_certificado").val(data.estudios_certificado);
	              	$("#mayor_estudios2").val(data.id_tipo_studies);
	              	$("#estudios_periodo2").val(data.periodo);
	              	$("#estudios_escuela2").val(data.escuela);
	              	$("#estudios_ciudad2").val(data.ciudad);
	              	$("#estudios_certificado2").val(data.certificado);
	              	$("#mayor_estudios_comentarios").val(data.estudios_comentarios);
		        	$("#mayoresEstudiosModal").modal('show');
		        });
		        $('a#datos_ref_profesionales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	              	$.ajax({
			            url: '<?php echo base_url('Candidato/getReferenciasProfesionales'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            beforeSend: function(){
			                $('.loader').css("display","block");
			            },
			            success : function(res)
			            { 
			            	setTimeout(function(){
				            	var data = res.split('###');
				            	for(var i = 0; i < data.length; i++){
				            		if(data[i] != ''){
				            			var num = i + 1;
				            			var d = data[i].split('@@');
						            	$("#id_refpro"+num).val(d[0]);
						            	$("#refpro"+num+"_nombre").val(d[1]);
						            	$("#refpro"+num+"_telefono").val(d[2]);
						            	$("#refpro"+num+"_tiempo").val(d[3]);
						            	$("#refpro"+num+"_conocido").val(d[4]);
						            	$("#refpro"+num+"_puesto").val(d[5]);
						            	$("#refpro"+num+"_tiempo2").val(d[6]);
						            	$("#refpro"+num+"_conocido2").val(d[7]);
						            	$("#refpro"+num+"_puesto2").val(d[8]);
						            	$("#refpro"+num+"_cualidades").val(d[9]);
						            	$("#refpro"+num+"_desempeno").val(d[10]);
						            	$("#refpro"+num+"_recomienda").val(d[11]);
						            	$("#refpro"+num+"_comentario").val(d[12]);
				            		}
				            	}
				            	$('.loader').fadeOut();
			            	},200);
			            }
			        });
		        	$("#refProfesionalesModal").modal('show');
		        });
		        $('a#datos_globales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			if(data.id_proyecto == 20){
	      				$("#e_oig").val(data.oig);
			        	$("#e_interpol").val(data.interpol);
			        	$("#e_sanctions").val(data.sanctions);
			        	$("#e_media_searches").val(data.media_searches);
			        	$("#global_especial_comentarios").val(data.global_comentarios);
			        	$("#globalesEspecialesModal").modal('show');
	      			}
	      			if(data.id_proyecto == 23){
	      				$("#x_oig").val(data.oig);
			        	$("#x_sanctions").val(data.sanctions);
			        	$("#x_facis").val(data.facis);
			        	$("#x_bureau").val(data.bureau);
			        	$("#x_european_financial").val(data.european_financial);
			        	$("#global_especifico_comentarios").val(data.global_comentarios);
			        	$("#globalesEspecificosModal").modal('show');
	      			}
	      			if(data.id_proyecto == 26 || data.id_proyecto == 27 || data.id_proyecto == 28 || data.id_proyecto == 150 || data.id_proyecto == 151 || data.id_proyecto == 152 || data.id_proyecto == 153 || data.id_proyecto == 154){
	      				$("#law_enforcement").val(data.law_enforcement);
			        	$("#regulatory").val(data.regulatory);
			        	$("#sanctions").val(data.sanctions);
			        	$("#other_bodies").val(data.other_bodies);
			        	$("#media_searches").val(data.media_searches);
			        	$("#global_general_comentarios").val(data.global_comentarios);
			        	$("#globalesGeneralModal").modal('show');
	      			}
	      			if(data.id_proyecto == 35){
	      				$("#usa_sanctions").val(data.usa_sanctions);
			        	$("#global_sanctions").val(data.sanctions);
			        	$("#global_sanctions_comentarios").val(data.global_comentarios);
			        	$("#globalesSanctionsModal").modal('show');
	      			}
	      			if(data.id_proyecto == 128){
	      				$("#law_enforcement_internal").val(data.law_enforcement);
			        	$("#regulatory_internal").val(data.regulatory);
			        	$("#sanctions_internal").val(data.sanctions);
			        	$("#other_bodies_internal").val(data.other_bodies);
			        	$("#media_searches_internal").val(data.media_searches);
			        	$("#global_fda").val(data.fda);
			        	$("#global_internal_comentarios").val(data.global_comentarios);
			        	$("#globalesInternalModal").modal('show');
	      			}
		        	
		        });
		        $('a#datos_domicilios', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			if(data.id_proyecto == 21 || data.id_proyecto == 24 || data.id_proyecto == 27){
		      			for(var i = 0; i <= 8; i++){
		      				$(".address_obligado"+i).val('');
		      			}
		      			$.ajax({
				            url: '<?php echo base_url('Candidato/getDomiciliosCandidato'); ?>',
				            type: 'post',
				            data: {'id_candidato':data.id},
				            beforeSend: function(){
				                $('.loader').css("display","block");
				            },
				            success : function(res)
				            { 
				            	setTimeout(function(){
					            	var data = res.split('###');
					            	for(var i = 0; i < data.length; i++){
					            		if(data[i] != ''){
					            			var num = i + 1;
					            			var d = data[i].split('@@');
							            	$("#address_periodo"+num).val(d[0]);
								        	$("#address_causa"+num).val(d[1]);
								        	$("#address_calle"+num).val(d[2]);
								        	$("#address_exterior"+num).val(d[3]);
								        	$("#address_interior"+num).val(d[4]);
								        	$("#address_colonia"+num).val(d[5]);
								        	$("#address_cp"+num).val(d[8]);
								        	$("#address_estado"+num).val(d[6]);
							            	$("#idAddress"+num).val(d[9]);
								        	var num = i + 1;
								        	getMunicipioDomicilio(d[6], d[7], num, 'general');
					            		}
					            	}
					            	$('.loader').fadeOut();
				            	},200);
				            }
				        });
				        $.ajax({
				            url: '<?php echo base_url('Candidato/getVerificacionDomiciliosCandidato'); ?>',
				            type: 'post',
				            data: {'id_candidato':data.id},
				            success : function(res)
				            { 
				            	$('#address_comentarios').val(res);
				            }
				        });
			        	$("#AddressModal").modal('show');
			        }
			        if(data.id_proyecto == 150 || data.id_proyecto == 151 || data.id_proyecto == 152 || data.id_proyecto == 153 || data.id_proyecto == 154){
		      			for(var i = 0; i <= 8; i++){
		      				$(".address_obligado_internacional"+i).val('');
		      			}
		      			$.ajax({
				            url: '<?php echo base_url('Candidato/getDomiciliosCandidato'); ?>',
				            type: 'post',
				            data: {'id_candidato':data.id},
				            beforeSend: function(){
				                $('.loader').css("display","block");
				            },
				            success : function(res)
				            { 
				            	setTimeout(function(){
					            	var data = res.split('###');
					            	for(var i = 0; i < data.length; i++){
					            		if(data[i] != ''){
					            			var num = i + 1;
					            			var d = data[i].split('@@');
							            	$("#address_periodo_internacional"+num).val(d[0]);
								        	$("#address_causa_internacional"+num).val(d[1]);
								        	$("#address_domicilio_internacional"+num).val(d[10]);
								        	$("#address_pais_internacional"+num).val(d[11]);
							            	$("#idAddressInternacional"+num).val(d[9]);
								        	var num = i + 1;
					            		}
					            	}
					            	$('.loader').fadeOut();
				            	},200);
				            }
				        });
				        $.ajax({
				            url: '<?php echo base_url('Candidato/getVerificacionDomiciliosCandidato'); ?>',
				            type: 'post',
				            data: {'id_candidato':data.id},
				            success : function(res)
				            { 
				            	$('#address_comentarios_internacional').val(res);
				            }
				        });
			        	$("#AddressInternacionalModal").modal('show');
			        }
		        });
		        $('a#datos_verificacion_docs', row).bind('click', () => {
		        	$("#d_documentos")[0].reset();
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			var href = '<?php echo base_url()."_docs/"; ?>';
	      			if(data.id_proyecto == 20){
	      				//Habilitados
	      				$("#lic_profesional").prop('readonly', false);
              			$("#lic_institucion").prop('readonly', false);
              			$("#ine_clave").prop('readonly', false);
              			$("#ine_registro").prop('readonly', false);
              			$("#ine_vertical").prop('readonly', false);
              			$("#ine_institucion").prop('readonly', false);
              			$("#pasaporte_numero").prop('readonly', false);
              			$("#pasaporte_institucion").prop('readonly', false);
              			$("#penales_numero").prop('readonly', true);
              			$("#penales_institucion").prop('readonly', true);
              			$("#domicilio_numero").prop('readonly', true);
              			$("#domicilio_fecha").prop('readonly', true);
              			$("#militar_numero").prop('readonly', true);
              			$("#militar_fecha").prop('readonly', true);
              			//Obligatorios
              			$("#lic_profesional").addClass('documento_obligado');
              			$("#lic_institucion").addClass('documento_obligado');
              			$("#ine_clave").addClass('documento_obligado');
              			$("#ine_registro").addClass('documento_obligado');
              			$("#ine_vertical").addClass('documento_obligado');
              			$("#ine_institucion").addClass('documento_obligado');
              			$("#pasaporte_numero").removeClass('documento_obligado');
              			$("#pasaporte_institucion").removeClass('documento_obligado');
              			$("#penales_numero").removeClass('documento_obligado');
              			$("#penales_institucion").removeClass('documento_obligado');
              			$("#domicilio_numero").removeClass('documento_obligado');
              			$("#domicilio_fecha").removeClass('documento_obligado');
              			$("#militar_numero").removeClass('documento_obligado');
              			$("#militar_fecha").removeClass('documento_obligado');
	      				$.ajax({
		                	url: '<?php echo base_url('Candidato/checkDocumentos'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	success: function(res){
		                  		if(res != 0){
		                  			$("#doc_estudios").empty();
		                  			$("#doc_ine").empty();
		                  			$("#doc_pasaporte").empty();
		                  			$("#doc_penales").empty();
		                  			$("#doc_domicilio").empty();
		                  			$("#doc_militar").empty();
		                  			var salida = res.split('@@');
		                  			for(var i = 0; i < salida.length; i++){
		                  				var aux = salida[i].split(',');
		                  				if(aux[0] == 7 || aux[0] == 10){
		                  					$("#doc_estudios").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  				if(aux[0] == 3){
		                  					$("#doc_ine").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 14){
		                  					$("#doc_pasaporte").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  			}
		                  		}
		                	}
		              	});
		              	$.ajax({
		                	url: '<?php echo base_url('Candidato/checkVerificacionDocumentosHCL'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	beforeSend: function(){
				                $('.loader').css("display","block");
				            },
		                	success: function(res){
		                		setTimeout(function(){
			                  		if(res != 0){
			                  			var data = res.split('@@');
			                  			$("#lic_profesional").val(data[0]);
			                  			$("#lic_institucion").val(data[1]);
			                  			$("#ine_clave").val(data[2]);
			                  			$("#ine_registro").val(data[3]);
			                  			$("#ine_vertical").val(data[4]);
			                  			$("#ine_institucion").val(data[5]);
			                  			$("#pasaporte_numero").val(data[12]);
			                  			$("#pasaporte_institucion").val(data[13]);
			                  			$("#doc_comentarios").val(data[16]);
			                  		}
		                  			$('.loader').fadeOut();
		            			},200);
		                	}
		              	});
	      			}
	      			if(data.id_proyecto == 28 || data.id_proyecto == 35 || data.id_proyecto == 128){
	      				//Habilitados
	      				$("#lic_profesional").prop('readonly', false);
              			$("#lic_institucion").prop('readonly', false);
              			$("#ine_clave").prop('readonly', false);
              			$("#ine_registro").prop('readonly', false);
              			$("#ine_vertical").prop('readonly', false);
              			$("#ine_institucion").prop('readonly', false);
              			$("#pasaporte_numero").prop('readonly', false);
              			$("#pasaporte_institucion").prop('readonly', false);
              			$("#penales_numero").prop('readonly', false);
              			$("#penales_institucion").prop('readonly', false);
              			$("#domicilio_numero").prop('readonly', true);
              			$("#domicilio_fecha").prop('readonly', true);
              			$("#militar_numero").prop('readonly', true);
              			$("#militar_fecha").prop('readonly', true);
              			//Obligatorios
              			$("#lic_profesional").addClass('documento_obligado');
              			$("#lic_institucion").addClass('documento_obligado');
              			$("#ine_clave").addClass('documento_obligado');
              			$("#ine_registro").addClass('documento_obligado');
              			$("#ine_vertical").addClass('documento_obligado');
              			$("#ine_institucion").addClass('documento_obligado');
              			$("#pasaporte_numero").removeClass('documento_obligado');
              			$("#pasaporte_institucion").removeClass('documento_obligado');
              			$("#penales_numero").addClass('documento_obligado');
              			$("#penales_institucion").addClass('documento_obligado');
              			$("#domicilio_numero").removeClass('documento_obligado');
              			$("#domicilio_fecha").removeClass('documento_obligado');
              			$("#militar_numero").removeClass('documento_obligado');
              			$("#militar_fecha").removeClass('documento_obligado');
	      				$.ajax({
		                	url: '<?php echo base_url('Candidato/checkDocumentos'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	success: function(res){
		                  		if(res != 0){
		                  			$("#doc_estudios").empty();
		                  			$("#doc_ine").empty();
		                  			$("#doc_pasaporte").empty();
		                  			$("#doc_penales").empty();
		                  			$("#doc_domicilio").empty();
		                  			$("#doc_militar").empty();
		                  			var salida = res.split('@@');
		                  			for(var i = 0; i < salida.length; i++){
		                  				var aux = salida[i].split(',');
		                  				if(aux[0] == 7 || aux[0] == 10){
		                  					$("#doc_estudios").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  				if(aux[0] == 3){
		                  					$("#doc_ine").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 12){
		                  					$("#doc_penales").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 14){
		                  					$("#doc_pasaporte").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  			}
		                  		}
		                	}
		              	});
		              	$.ajax({
		                	url: '<?php echo base_url('Candidato/checkVerificacionDocumentosHCL'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	beforeSend: function(){
				                $('.loader').css("display","block");
				            },
		                	success: function(res){
		                		setTimeout(function(){
			                  		if(res != 0){
			                  			var data = res.split('@@');
			                  			$("#lic_profesional").val(data[0]);
			                  			$("#lic_institucion").val(data[1]);
			                  			$("#ine_clave").val(data[2]);
			                  			$("#ine_registro").val(data[3]);
			                  			$("#ine_vertical").val(data[4]);
			                  			$("#ine_institucion").val(data[5]);
			                  			$("#penales_numero").val(data[6]);
			                  			$("#penales_institucion").val(data[7]);
			                  			$("#pasaporte_numero").val(data[12]);
			                  			$("#pasaporte_institucion").val(data[13]);
			                  			$("#doc_comentarios").val(data[16]);
			                  		}
		                  			$('.loader').fadeOut();
		            			},200);
		                	}
		              	});
	      			}
	      			if(data.id_proyecto == 21 || data.id_proyecto == 24 || data.id_proyecto == 27){
	      				//Habilitados
	      				$("#lic_profesional").prop('readonly', false);
              			$("#lic_institucion").prop('readonly', false);
              			$("#ine_clave").prop('readonly', false);
              			$("#ine_registro").prop('readonly', false);
              			$("#ine_vertical").prop('readonly', false);
              			$("#ine_institucion").prop('readonly', false);
              			$("#pasaporte_numero").prop('readonly', false);
              			$("#pasaporte_institucion").prop('readonly', false);
              			$("#penales_numero").prop('readonly', false);
              			$("#penales_institucion").prop('readonly', false);
              			$("#domicilio_numero").prop('readonly', false);
              			$("#domicilio_fecha").prop('readonly', false);
              			$("#militar_numero").prop('readonly', true);
              			$("#militar_fecha").prop('readonly', true);
              			//Obligatorios
              			$("#lic_profesional").addClass('documento_obligado');
              			$("#lic_institucion").addClass('documento_obligado');
              			$("#ine_clave").addClass('documento_obligado');
              			$("#ine_registro").addClass('documento_obligado');
              			$("#ine_vertical").addClass('documento_obligado');
              			$("#ine_institucion").addClass('documento_obligado');
              			$("#pasaporte_numero").removeClass('documento_obligado');
              			$("#pasaporte_institucion").removeClass('documento_obligado');
              			$("#penales_numero").removeClass('documento_obligado');
              			$("#penales_institucion").removeClass('documento_obligado');
              			$("#domicilio_numero").addClass('documento_obligado');
              			$("#domicilio_fecha").addClass('documento_obligado');
              			$("#militar_numero").removeClass('documento_obligado');
              			$("#militar_fecha").removeClass('documento_obligado');
	      				$.ajax({
		                	url: '<?php echo base_url('Candidato/checkDocumentos'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	success: function(res){
		                  		if(res != 0){
		                  			$("#doc_estudios").empty();
		                  			$("#doc_ine").empty();
		                  			$("#doc_pasaporte").empty();
		                  			$("#doc_penales").empty();
		                  			$("#doc_domicilio").empty();
		                  			$("#doc_militar").empty();
		                  			var salida = res.split('@@');
		                  			for(var i = 0; i < salida.length; i++){
		                  				var aux = salida[i].split(',');
		                  				if(aux[0] == 7 || aux[0] == 10){
		                  					$("#doc_estudios").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  				if(aux[0] == 3){
		                  					$("#doc_ine").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 12){
		                  					$("#doc_penales").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 2){
		                  					$("#doc_domicilio").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 14){
		                  					$("#doc_pasaporte").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  			}
		                  		}
		                	}
		              	});
		              	$.ajax({
		                	url: '<?php echo base_url('Candidato/checkVerificacionDocumentosHCL'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	beforeSend: function(){
				                $('.loader').css("display","block");
				            },
		                	success: function(res){
		                		setTimeout(function(){
			                  		if(res != 0){
			                  			var data = res.split('@@');
			                  			$("#lic_profesional").val(data[0]);
			                  			$("#lic_institucion").val(data[1]);
			                  			$("#ine_clave").val(data[2]);
			                  			$("#ine_registro").val(data[3]);
			                  			$("#ine_vertical").val(data[4]);
			                  			$("#ine_institucion").val(data[5]);
			                  			$("#penales_numero").val(data[6]);
			                  			$("#penales_institucion").val(data[7]);
			                  			$("#domicilio_numero").val(data[8]);
			                  			$("#domicilio_fecha").val(data[9]);
			                  			$("#pasaporte_numero").val(data[12]);
			                  			$("#pasaporte_institucion").val(data[13]);
			                  			$("#doc_comentarios").val(data[16]);
			                  		}
		                  			$('.loader').fadeOut();
		            			},200);
		                	}
		              	});
	      			}
	      			if(data.id_proyecto == 23){
	      				//Habilitados
	      				$("#lic_profesional").prop('readonly', false);
              			$("#lic_institucion").prop('readonly', false);
              			$("#ine_clave").prop('readonly', false);
              			$("#ine_registro").prop('readonly', false);
              			$("#ine_vertical").prop('readonly', false);
              			$("#ine_institucion").prop('readonly', false);
              			$("#pasaporte_numero").prop('readonly', false);
              			$("#pasaporte_institucion").prop('readonly', false);
              			$("#penales_numero").prop('readonly', false);
              			$("#penales_institucion").prop('readonly', false);
              			$("#domicilio_numero").prop('readonly', true);
              			$("#domicilio_fecha").prop('readonly', true);
              			$("#militar_numero").prop('readonly', false);
              			$("#militar_fecha").prop('readonly', false);
              			//Obligatorios
              			$("#lic_profesional").addClass('documento_obligado');
              			$("#lic_institucion").addClass('documento_obligado');
              			$("#ine_clave").removeClass('documento_obligado');
              			$("#ine_registro").removeClass('documento_obligado');
              			$("#ine_vertical").removeClass('documento_obligado');
              			$("#ine_institucion").removeClass('documento_obligado');
              			$("#pasaporte_numero").removeClass('documento_obligado');
              			$("#pasaporte_institucion").removeClass('documento_obligado');
              			$("#penales_numero").addClass('documento_obligado');
              			$("#penales_institucion").addClass('documento_obligado');
              			$("#domicilio_numero").removeClass('documento_obligado');
              			$("#domicilio_fecha").removeClass('documento_obligado');
              			$("#militar_numero").addClass('documento_obligado');
              			$("#militar_fecha").addClass('documento_obligado');
	      				$.ajax({
		                	url: '<?php echo base_url('Candidato/checkDocumentos'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	success: function(res){
		                  		if(res != 0){
		                  			$("#doc_estudios").empty();
		                  			$("#doc_ine").empty();
		                  			$("#doc_pasaporte").empty();
		                  			$("#doc_penales").empty();
		                  			$("#doc_domicilio").empty();
		                  			$("#doc_militar").empty();
		                  			var salida = res.split('@@');
		                  			for(var i = 0; i < salida.length; i++){
		                  				var aux = salida[i].split(',');
		                  				if(aux[0] == 7 || aux[0] == 10){
		                  					$("#doc_estudios").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  				if(aux[0] == 3){
		                  					$("#doc_ine").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 12){
		                  					$("#doc_penales").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 15){
		                  					$("#doc_militar").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 14){
		                  					$("#doc_pasaporte").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  			}
		                  		}
		                	}
		              	});
		              	$.ajax({
		                	url: '<?php echo base_url('Candidato/checkVerificacionDocumentosHCL'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	beforeSend: function(){
				                $('.loader').css("display","block");
				            },
		                	success: function(res){
		                		setTimeout(function(){
			                  		if(res != 0){
			                  			var data = res.split('@@');
			                  			$("#lic_profesional").val(data[0]);
			                  			$("#lic_institucion").val(data[1]);
			                  			$("#ine_clave").val(data[2]);
			                  			$("#ine_registro").val(data[3]);
			                  			$("#ine_vertical").val(data[4]);
			                  			$("#ine_institucion").val(data[5]);
			                  			$("#penales_numero").val(data[6]);
			                  			$("#penales_institucion").val(data[7]);
			                  			$("#militar_numero").val(data[10]);
			                  			$("#militar_fecha").val(data[11]);
			                  			$("#pasaporte_numero").val(data[12]);
			                  			$("#pasaporte_institucion").val(data[13]);
			                  			$("#doc_comentarios").val(data[16]);
			                  		}
		                  			$('.loader').fadeOut();
		            			},200);
		                	}
		              	});
	      			}
	      			if(data.id_proyecto == 26){
	      				//Habilitados
	      				$("#lic_profesional").prop('readonly', false);
              			$("#lic_institucion").prop('readonly', false);
              			$("#ine_clave").prop('readonly', false);
              			$("#ine_registro").prop('readonly', false);
              			$("#ine_vertical").prop('readonly', false);
              			$("#ine_institucion").prop('readonly', false);
              			$("#pasaporte_numero").prop('readonly', false);
              			$("#pasaporte_institucion").prop('readonly', false);
              			$("#penales_numero").prop('readonly', false);
              			$("#penales_institucion").prop('readonly', false);
              			$("#domicilio_numero").prop('readonly', true);
              			$("#domicilio_fecha").prop('readonly', true);
              			$("#militar_numero").prop('readonly', true);
              			$("#militar_fecha").prop('readonly', true);
              			//Obligatorios
              			$("#lic_profesional").addClass('documento_obligado');
              			$("#lic_institucion").addClass('documento_obligado');
              			$("#ine_clave").removeClass('documento_obligado');
              			$("#ine_registro").removeClass('documento_obligado');
              			$("#ine_vertical").removeClass('documento_obligado');
              			$("#ine_institucion").removeClass('documento_obligado');
              			$("#pasaporte_numero").removeClass('documento_obligado');
              			$("#pasaporte_institucion").removeClass('documento_obligado');
              			$("#penales_numero").addClass('documento_obligado');
              			$("#penales_institucion").addClass('documento_obligado');
              			$("#domicilio_numero").removeClass('documento_obligado');
              			$("#domicilio_fecha").removeClass('documento_obligado');
              			$("#militar_numero").removeClass('documento_obligado');
              			$("#militar_fecha").removeClass('documento_obligado');
	      				$.ajax({
		                	url: '<?php echo base_url('Candidato/checkDocumentos'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	success: function(res){
		                  		if(res != 0){
		                  			$("#doc_estudios").empty();
		                  			$("#doc_ine").empty();
		                  			$("#doc_pasaporte").empty();
		                  			$("#doc_penales").empty();
		                  			$("#doc_domicilio").empty();
		                  			$("#doc_militar").empty();
		                  			var salida = res.split('@@');
		                  			for(var i = 0; i < salida.length; i++){
		                  				var aux = salida[i].split(',');
		                  				if(aux[0] == 7 || aux[0] == 10){
		                  					$("#doc_estudios").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  				if(aux[0] == 3){
		                  					$("#doc_ine").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 12){
		                  					$("#doc_penales").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 14){
		                  					$("#doc_pasaporte").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  			}
		                  		}
		                	}
		              	});
		              	$.ajax({
		                	url: '<?php echo base_url('Candidato/checkVerificacionDocumentosHCL'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	beforeSend: function(){
				                $('.loader').css("display","block");
				            },
		                	success: function(res){
		                		setTimeout(function(){
			                  		if(res != 0){
			                  			var data = res.split('@@');
			                  			$("#lic_profesional").val(data[0]);
			                  			$("#lic_institucion").val(data[1]);
			                  			$("#ine_clave").val(data[2]);
			                  			$("#ine_registro").val(data[3]);
			                  			$("#ine_vertical").val(data[4]);
			                  			$("#ine_institucion").val(data[5]);
			                  			$("#penales_numero").val(data[6]);
			                  			$("#penales_institucion").val(data[7]);
			                  			$("#pasaporte_numero").val(data[12]);
			                  			$("#pasaporte_institucion").val(data[13]);
			                  			$("#doc_comentarios").val(data[16]);
			                  		}
		                  			$('.loader').fadeOut();
		            			},200);
		                	}
		              	});
	      			}
	      			if(data.id_proyecto == 150 || data.id_proyecto == 151 || data.id_proyecto == 152 || data.id_proyecto == 153 || data.id_proyecto == 154){
	      				//Habilitados
	      				$("#lic_profesional").prop('readonly', false);
              			$("#lic_institucion").prop('readonly', false);
              			$("#ine_clave").prop('readonly', false);
              			$("#ine_registro").prop('readonly', true);
              			$("#ine_vertical").prop('readonly', true);
              			$("#ine_institucion").prop('readonly', false);
              			$("#pasaporte_numero").prop('readonly', false);
              			$("#pasaporte_institucion").prop('readonly', false);
              			$("#penales_numero").prop('readonly', false);
              			$("#penales_institucion").prop('readonly', false);
              			$("#domicilio_numero").prop('readonly', false);
              			$("#domicilio_fecha").prop('readonly', false);
              			$("#militar_numero").prop('readonly', true);
              			$("#militar_fecha").prop('readonly', true);
              			//Obligatorios
              			$("#lic_profesional").addClass('documento_obligado');
              			$("#lic_institucion").addClass('documento_obligado');
              			$("#ine_clave").addClass('documento_obligado');
              			$("#ine_registro").removeClass('documento_obligado');
              			$("#ine_vertical").removeClass('documento_obligado');
              			$("#ine_institucion").addClass('documento_obligado');
              			$("#pasaporte_numero").removeClass('documento_obligado');
              			$("#pasaporte_institucion").removeClass('documento_obligado');
              			$("#penales_numero").removeClass('documento_obligado');
              			$("#penales_institucion").removeClass('documento_obligado');
              			$("#domicilio_numero").addClass('documento_obligado');
              			$("#domicilio_fecha").addClass('documento_obligado');
              			$("#militar_numero").removeClass('documento_obligado');
              			$("#militar_fecha").removeClass('documento_obligado');
	      				$.ajax({
		                	url: '<?php echo base_url('Candidato/checkDocumentos'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	success: function(res){
		                  		if(res != 0){
		                  			$("#doc_estudios").empty();
		                  			$("#doc_ine").empty();
		                  			$("#doc_pasaporte").empty();
		                  			$("#doc_penales").empty();
		                  			$("#doc_domicilio").empty();
		                  			$("#doc_militar").empty();
		                  			var salida = res.split('@@');
		                  			for(var i = 0; i < salida.length; i++){
		                  				var aux = salida[i].split(',');
		                  				if(aux[0] == 7 || aux[0] == 10){
		                  					$("#doc_estudios").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  				if(aux[0] == 3){
		                  					$("#doc_ine").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 12){
		                  					$("#doc_penales").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 2){
		                  					$("#doc_domicilio").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a>");
		                  				}
		                  				if(aux[0] == 14){
		                  					$("#doc_pasaporte").append("<a href='"+href+aux[1]+"' target='_blank' style='color: black;'>"+aux[1]+"</a><br><br>");
		                  				}
		                  			}
		                  		}
		                	}
		              	});
		              	$.ajax({
		                	url: '<?php echo base_url('Candidato/checkVerificacionDocumentosHCL'); ?>',
		                	method: 'POST',
		                	data: {'id_candidato':data.id},
		                	beforeSend: function(){
				                $('.loader').css("display","block");
				            },
		                	success: function(res){
		                		setTimeout(function(){
			                  		if(res != 0){
			                  			var data = res.split('@@');
			                  			$("#lic_profesional").val(data[0]);
			                  			$("#lic_institucion").val(data[1]);
			                  			$("#ine_clave").val(data[2]);
			                  			$("#ine_institucion").val(data[5]);
			                  			$("#penales_numero").val(data[6]);
			                  			$("#penales_institucion").val(data[7]);
			                  			$("#domicilio_numero").val(data[8]);
			                  			$("#domicilio_fecha").val(data[9]);
			                  			$("#pasaporte_numero").val(data[12]);
			                  			$("#pasaporte_institucion").val(data[13]);
			                  			$("#doc_comentarios").val(data[16]);
			                  		}
		                  			$('.loader').fadeOut();
		            			},200);
		                	}
		              	});
	      			}
		        	$("#documentosModal").modal('show');
		        });
				$('a#datos_checklist', row).bind('click', () =>{
					$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
					$("#check_education").val(data.education);
	              	$("#check_employment").val(data.employment);
	              	$("#check_address").val(data.address);
	              	$("#check_criminal").val(data.criminal);
	              	$("#check_database").val(data.global_database);
	              	$("#check_identity").val(data.identity);
	              	$("#check_military").val(data.military);
	              	$("#check_other").val(data.other);
			        $("#ScopeModal").modal('show');
				});
	      		$('a#custom_database_check', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
		        	$("#custom_law_enforcement").val(data.law_enforcement);
		        	$("#custom_regulatory").val(data.regulatory);
		        	$("#custom_sanctions").val(data.sanctions);
		        	$("#custom_other_bodies").val(data.other_bodies);
		        	$("#custom_media_searches").val(data.media_searches);
		        	$("#custom_sdn").val(data.sdn);
		        	$("#custom_comentarios").val(data.global_comentarios);
		        	$("#customDatabaseModal").modal('show');
		        });
		        $('a#custom_identidad', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
		        	$("#custom_ine").val(data.custom_ine);
		        	$("#custom_ine_ano").val(data.custom_ine_ano);
		        	$("#custom_ine_vertical").val(data.custom_ine_vertical);
		        	$("#custom_ine_institucion").val(data.custom_ine_institucion);
		        	$("#custom_pasaporte").val(data.custom_pasaporte);
		        	$("#custom_pasaporte_fecha").val(data.custom_pasaporte_fecha);
		        	$("#custom_militar").val(data.custom_militar);
		        	$("#custom_militar_fecha").val(data.custom_militar_fecha);
		        	$("#custom_identidad_comentarios").val(data.custom_comentarios);
		        	$("#customIdentidadModal").modal('show');
		        });
		        $('a#custom_address_check', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			for(var i = 0; i <= 8; i++){
	      				$(".custom_address_obligado"+i).val('');
	      			}
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/getDomiciliosCandidato'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            beforeSend: function(){
			                $('.loader').css("display","block");
			            },
			            success : function(res)
			            { 
			            	setTimeout(function(){
				            	var data = res.split('###');
				            	for(var i = 0; i < data.length; i++){
				            		if(data[i] != ''){
				            			var num = i + 1;
				            			var d = data[i].split('@@');
						            	$("#custom_periodo"+num).val(d[0]);
							        	$("#custom_causa"+num).val(d[1]);
							        	$("#custom_calle"+num).val(d[2]);
							        	$("#custom_exterior"+num).val(d[3]);
							        	$("#custom_interior"+num).val(d[4]);
							        	$("#custom_colonia"+num).val(d[5]);
							        	$("#custom_cp"+num).val(d[8]);
							        	$("#custom_estado"+num).val(d[6]);
						            	$("#idDomicilio"+num).val(d[9]);
							        	var num = i + 1;
							        	getMunicipioDomicilio(d[6], d[7], num, 'custom');
				            		}
				            	}
				            	$('.loader').fadeOut();
			            	},200);
			            }
			        });
			        $.ajax({
			            url: '<?php echo base_url('Candidato/getVerificacionDomiciliosCandidato'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	$('#custom_address_comentarios').val(res);
			            }
			        });
		        	$("#customAddressModal").modal('show');
		        });
	      		$("a#subirDocs", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".prefijo").val(data.id+"_"+data.nombre+""+data.paterno);
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/getDocs'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'prefijo':data.id+"_"+data.nombre+""+data.paterno},
			            success : function(res)
			            { 
			            	$("#tablaDocs").html(res);

			            },error: function(res)
			            {
			              
			            }
			        });
	      			$("#docsModal").modal("show");
	      		});
		        $('a#datos_laborales', row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".nombreCandidato").text(data.candidato);
	      			for(var i = 1; i <= 8; i++){
	      				$("#candidato_reflaboral"+i)[0].reset();
	      				$("#analista_reflaboral"+i)[0].reset();
	      			}
	              	$("#trabajo_inactivo").val(data.trabajo_inactivo);
	              	$.ajax({
	              		async: false,
	                	url: '<?php echo base_url('Candidato/getReferencias'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	success: function(res){
	                		var rows = res.split('###');
	                		for(var i = 0; i < rows.length; i++){
	                			if(rows[i] != ""){
			                		var dato = rows[i].split('@@');
			                		var entrada = convertirDateTime(dato[2]);
			                		var salida = convertirDateTime(dato[3]);
			                  		$("#reflab"+(i+1)+"_empresa_ingles").val(dato[0]);
			                  		$("#reflab"+(i+1)+"_direccion_ingles").val(dato[1]);
			                  		$("#reflab"+(i+1)+"_entrada_ingles").val(entrada);
			                  		$("#reflab"+(i+1)+"_salida_ingles").val(salida);
			                  		$("#reflab"+(i+1)+"_telefono_ingles").val(dato[4]);
			                  		$("#reflab"+(i+1)+"_puesto1_ingles").val(dato[5]);
			                  		$("#reflab"+(i+1)+"_puesto2_ingles").val(dato[6]);
			                  		$("#reflab"+(i+1)+"_salario1_ingles").val(dato[7]);
			                  		$("#reflab"+(i+1)+"_salario2_ingles").val(dato[8]);
			                  		$("#reflab"+(i+1)+"_jefenombre_ingles").val(dato[9]);
			                  		$("#reflab"+(i+1)+"_jefecorreo_ingles").val(dato[10]);
			                  		$("#reflab"+(i+1)+"_jefepuesto_ingles").val(dato[11]);
			                  		$("#reflab"+(i+1)+"_separacion_ingles").val(dato[12]);
			                  		$("#idreflabingles"+(i+1)).val(dato[13]);
		                		}
	                		}
	                	}
	              	});
					$.ajax({
		        		async: false,
	                	url: '<?php echo base_url('Candidato/getVerificacionRefLaboralesAnalista'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	success: function(res){
	                		if(res != ""){
	                			var rows = res.split('###');
		                		for(var i = 0; i < rows.length; i++){
		                			if(rows[i] != ""){
				                		var dato = rows[i].split('@@');
			                			var entrada = convertirDateTime(dato[2]);
			                			var salida = convertirDateTime(dato[3]);
				                  		$("#an_reflab"+dato[29]+"_empresa").val(dato[0]);
				                  		$("#an_reflab"+dato[29]+"_direccion").val(dato[1]);
				                  		$("#an_reflab"+dato[29]+"_entrada").val(entrada);
				                  		$("#an_reflab"+dato[29]+"_salida").val(salida);
				                  		$("#an_reflab"+dato[29]+"_telefono").val(dato[4]);
				                  		$("#an_reflab"+dato[29]+"_puesto1").val(dato[5]);
				                  		$("#an_reflab"+dato[29]+"_puesto2").val(dato[6]);
				                  		$("#an_reflab"+dato[29]+"_salario1").val(dato[7]);
				                  		$("#an_reflab"+dato[29]+"_salario2").val(dato[8]);
				                  		$("#an_reflab"+dato[29]+"_jefenombre").val(dato[9]);
				                  		$("#an_reflab"+dato[29]+"_jefecorreo").val(dato[10]);
				                  		$("#an_reflab"+dato[29]+"_jefepuesto").val(dato[11]);
				                  		$("#an_reflab"+dato[29]+"_separacion").val(dato[12]);
				                  		$("#an_reflab"+dato[29]+"_notas").val(dato[13]);
				                  		$("#an_reflab"+dato[29]+"_responsabilidad").val(dato[15]);
				                  		$("#an_reflab"+dato[29]+"_iniciativa").val(dato[16]);
				                  		$("#an_reflab"+dato[29]+"_eficiencia").val(dato[17]);
				                  		$("#an_reflab"+dato[29]+"_disciplina").val(dato[18]);
				                  		$("#an_reflab"+dato[29]+"_puntualidad").val(dato[19]);
				                  		$("#an_reflab"+dato[29]+"_limpieza").val(dato[20]);
				                  		$("#an_reflab"+dato[29]+"_estabilidad").val(dato[21]);
				                  		$("#an_reflab"+dato[29]+"_emocional").val(dato[22]);
				                  		$("#an_reflab"+dato[29]+"_honesto").val(dato[23]);
				                  		$("#an_reflab"+dato[29]+"_rendimiento").val(dato[24]);
				                  		$("#an_reflab"+dato[29]+"_actitud").val(dato[25]);
				                  		$("#an_reflab"+dato[29]+"_recontratacion").val(dato[26]);
				                  		$("#an_reflab"+dato[29]+"_motivo").val(dato[27]);
				                  		$("#idverlab"+dato[29]).val(dato[28]);
			                		}
		                		}
	                		}
	                  		
	                	}
	              	});
	              	$("#nombreCliente").text(data.cliente);
		        	$("#listado").css('display','none');
			        $("#btn_nuevo").css('display','none');
			        $("#opcion_proceso").css('display','none');
			        $("#formulario").css('display','block');
			        $("#btn_regresar").css('display','block');
		        });
				$("a#estudios", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#estudios_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			verificacionEstudios();
	      		});
	      		$("a#laborales", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#laborales_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			verificacionLaborales();
	      		});
	      		$("a#penales", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#penales_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			verificacionPenales();
	      		});
	      		$("a#msj_avances", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#avances_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusAvances();
	      		});
	      		$("a#llamada", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#llamada_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusLlamadas();
	      		});
	      		$("a#correoEnviado", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#email_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusEmails();
	      		});
	      		$("a#ofac", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#idCliente").val(data.id_cliente);
	      			$("#ofac_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusOFAC();
	      		});
	      		$("a#datos_gaps", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$.ajax({
			      		url: '<?php echo base_url('Candidato/checkGaps'); ?>',
			      		method: 'POST',
			      		data: {'id_candidato':data.id},
			      		dataType: "text",
			      		success: function(res)
			      		{
			      			if(res != 0){
			        			$("#div_antesgap").empty();
			        			$("#div_antesgap").append(res);
			        			$("#gapsModal").modal('show');
			        		}
			        		else{
			        			$("#div_antesgap").empty();
			        			$("#div_antesgap").html('<p class="text-center">Sin registros</p>');
			        			$("#gapsModal").modal('show');
			        		}
			      		}
			      	});
    			});
    			$("a#datos_credito", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$.ajax({
			      		url: '<?php echo base_url('Candidato/checkCredito'); ?>',
			      		method: 'POST',
			      		data: {'id_candidato':data.id},
			      		dataType: "text",
			      		success: function(res)
			      		{
			      			if(res != 0){
			        			$("#div_antescredit").empty();
			        			$("#div_antescredit").append(res);
			        			$("#creditoModal").modal('show');
			        		}
			        		else{
			        			$("#div_antescredit").empty();
			        			$("#div_antescredit").html('<p class="text-center">Sin registros</p>');
			        			$("#creditoModal").modal('show');
			        		}
			      		}
			      	});
    			});
	      		$("a#final", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			if(data.id_proyecto == 20){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',true);
	      				$("#check_ofac").prop('disabled',true);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").removeClass('check_obligado');
	      				$("#check_ofac").removeClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if( data.id_proyecto == 28 || data.id_proyecto == 23){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 21 || data.id_proyecto == 24){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',true);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").removeClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 26){
	      				$("#check_identidad").prop('disabled',true);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',true);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").removeClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").removeClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 27 || data.id_proyecto == 35 || data.id_proyecto == 150 || data.id_proyecto == 151 || data.id_proyecto == 152 || data.id_proyecto == 153){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 128 || data.id_proyecto == 135 || data.id_proyecto == 137){
	      				$("#check_identidad").prop('disabled',true);
	      				$("#check_laboral").prop('disabled',true);
	      				$("#check_estudios").prop('disabled',true);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',true);
	      				$("#check_ofac").prop('disabled',true);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").removeClass('check_obligado');
	      				$("#check_laboral").removeClass('check_obligado');
	      				$("#check_estudios").removeClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").removeClass('check_obligado');
	      				$("#check_ofac").removeClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');


	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 136){
	      				$("#check_identidad").prop('disabled',true);
	      				$("#check_laboral").prop('disabled',true);
	      				$("#check_estudios").prop('disabled',true);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").removeClass('check_obligado');
	      				$("#check_laboral").removeClass('check_obligado');
	      				$("#check_estudios").removeClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 138){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',true);
	      				$("#check_estudios").prop('disabled',true);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',true);
	      				$("#check_ofac").prop('disabled',true);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").removeClass('check_obligado');
	      				$("#check_estudios").removeClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").removeClass('check_obligado');
	      				$("#check_ofac").removeClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 140){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',true);
	      				$("#check_estudios").prop('disabled',true);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',true);
	      				$("#check_ofac").prop('disabled',true);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',true);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").removeClass('check_obligado');
	      				$("#check_estudios").removeClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").removeClass('check_obligado');
	      				$("#check_ofac").removeClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").removeClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 147){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',true);
	      				$("#check_estudios").prop('disabled',true);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").removeClass('check_obligado');
	      				$("#check_estudios").removeClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 148){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',true);
	      				$("#check_estudios").prop('disabled',true);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',true);
	      				$("#check_ofac").prop('disabled',true);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',true);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").removeClass('check_obligado');
	      				$("#check_estudios").removeClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").removeClass('check_obligado');
	      				$("#check_ofac").removeClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").removeClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 154){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',false);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").addClass('check_obligado');
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			
	      		});
				$("a#editar_final", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			if(data.id_proyecto == 20){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',true);
	      				$("#check_ofac").prop('disabled',true);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").removeClass('check_obligado');
	      				$("#check_ofac").removeClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#check_identidad").val(data.identidad_check);
	      				$("#check_laboral").val(data.empleo_check);
	      				$("#check_estudios").val(data.estudios_check);
	      				$("#check_visita").val(data.visita_check);
	      				$("#check_penales").val(data.penales_check);
	      				$("#check_ofac").val(data.ofac_check);
	      				$("#check_laboratorio").val(data.laboratorio_check);
	      				$("#check_medico").val(data.medico_check);
	      				$("#check_global").val(data.global_searches_check);
	      				$("#check_domicilio").val(data.domicilios_check);
	      				$("#check_credito").val(data.credito_check);

	      				$("#comentario_final").val(data.comentario_final);
	      				$("#bgc_status").val(data.status_bgc);
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 28 || data.id_proyecto == 23){
	      				$("#idBGC").val(data.idBGC);
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');
	      				
	      				$("#check_identidad").val(data.identidad_check);
	      				$("#check_laboral").val(data.empleo_check);
	      				$("#check_estudios").val(data.estudios_check);
	      				$("#check_visita").val(data.visita_check);
	      				$("#check_penales").val(data.penales_check);
	      				$("#check_ofac").val(data.ofac_check);
	      				$("#check_laboratorio").val(data.laboratorio_check);
	      				$("#check_medico").val(data.medico_check);
	      				$("#check_global").val(data.global_searches_check);
	      				$("#check_domicilio").val(data.domicilios_check);
	      				$("#check_credito").val(data.credito_check);

	      				$("#comentario_final").val(data.comentario_final);
	      				$("#bgc_status").val(data.status_bgc);

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 21 || data.id_proyecto == 24){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',true);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").removeClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#check_identidad").val(data.identidad_check);
	      				$("#check_laboral").val(data.empleo_check);
	      				$("#check_estudios").val(data.estudios_check);
	      				$("#check_visita").val(data.visita_check);
	      				$("#check_penales").val(data.penales_check);
	      				$("#check_ofac").val(data.ofac_check);
	      				$("#check_laboratorio").val(data.laboratorio_check);
	      				$("#check_medico").val(data.medico_check);
	      				$("#check_global").val(data.global_searches_check);
	      				$("#check_domicilio").val(data.domicilios_check);
	      				$("#check_credito").val(data.credito_check);

	      				$("#comentario_final").val(data.comentario_final);
	      				$("#bgc_status").val(data.status_bgc);
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 26){
	      				$("#check_identidad").prop('disabled',true);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',true);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").removeClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").removeClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#check_identidad").val(data.identidad_check);
	      				$("#check_laboral").val(data.empleo_check);
	      				$("#check_estudios").val(data.estudios_check);
	      				$("#check_visita").val(data.visita_check);
	      				$("#check_penales").val(data.penales_check);
	      				$("#check_ofac").val(data.ofac_check);
	      				$("#check_laboratorio").val(data.laboratorio_check);
	      				$("#check_medico").val(data.medico_check);
	      				$("#check_global").val(data.global_searches_check);
	      				$("#check_domicilio").val(data.domicilios_check);
	      				$("#check_credito").val(data.credito_check);

	      				$("#comentario_final").val(data.comentario_final);
	      				$("#bgc_status").val(data.status_bgc);

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 27 || data.id_proyecto == 35 || data.id_proyecto == 150 || data.id_proyecto == 151 || data.id_proyecto == 152 || data.id_proyecto == 153){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#check_identidad").val(data.identidad_check);
	      				$("#check_laboral").val(data.empleo_check);
	      				$("#check_estudios").val(data.estudios_check);
	      				$("#check_visita").val(data.visita_check);
	      				$("#check_penales").val(data.penales_check);
	      				$("#check_ofac").val(data.ofac_check);
	      				$("#check_laboratorio").val(data.laboratorio_check);
	      				$("#check_medico").val(data.medico_check);
	      				$("#check_global").val(data.global_searches_check);
	      				$("#check_domicilio").val(data.domicilios_check);
	      				$("#check_credito").val(data.credito_check);

	      				$("#comentario_final").val(data.comentario_final);
	      				$("#bgc_status").val(data.status_bgc);

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 128 || data.id_proyecto == 135 || data.id_proyecto == 137){
	      				$("#check_identidad").prop('disabled',true);
	      				$("#check_laboral").prop('disabled',true);
	      				$("#check_estudios").prop('disabled',true);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',true);
	      				$("#check_ofac").prop('disabled',true);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',true);
	      				$("#check_credito").prop('disabled',true);

	      				$("#check_identidad").removeClass('check_obligado');
	      				$("#check_laboral").removeClass('check_obligado');
	      				$("#check_estudios").removeClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").removeClass('check_obligado');
	      				$("#check_ofac").removeClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").removeClass('check_obligado');
	      				$("#check_credito").removeClass('check_obligado');

	      				$("#check_identidad").val(data.identidad_check);
	      				$("#check_laboral").val(data.empleo_check);
	      				$("#check_estudios").val(data.estudios_check);
	      				$("#check_visita").val(data.visita_check);
	      				$("#check_penales").val(data.penales_check);
	      				$("#check_ofac").val(data.ofac_check);
	      				$("#check_laboratorio").val(data.laboratorio_check);
	      				$("#check_medico").val(data.medico_check);
	      				$("#check_global").val(data.global_searches_check);
	      				$("#check_domicilio").val(data.domicilios_check);
	      				$("#check_credito").val(data.credito_check);

	      				$("#comentario_final").val(data.comentario_final);
	      				$("#bgc_status").val(data.status_bgc);

	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			if(data.id_proyecto == 154){
	      				$("#check_identidad").prop('disabled',false);
	      				$("#check_laboral").prop('disabled',false);
	      				$("#check_estudios").prop('disabled',false);
	      				$("#check_visita").prop('disabled',true);
	      				$("#check_penales").prop('disabled',false);
	      				$("#check_ofac").prop('disabled',false);
	      				$("#check_laboratorio").prop('disabled',true);
	      				$("#check_medico").prop('disabled',true);
	      				$("#check_global").prop('disabled',false);
	      				$("#check_domicilio").prop('disabled',false);
	      				$("#check_credito").prop('disabled',false);

	      				$("#check_identidad").addClass('check_obligado');
	      				$("#check_laboral").addClass('check_obligado');
	      				$("#check_estudios").addClass('check_obligado');
	      				$("#check_visita").removeClass('check_obligado');
	      				$("#check_penales").addClass('check_obligado');
	      				$("#check_ofac").addClass('check_obligado');
	      				$("#check_laboratorio").removeClass('check_obligado');
	      				$("#check_medico").removeClass('check_obligado');
	      				$("#check_global").addClass('check_obligado');
	      				$("#check_domicilio").addClass('check_obligado');
	      				$("#check_credito").addClass('check_obligado');

	      				$("#check_identidad").val(data.identidad_check);
	      				$("#check_laboral").val(data.empleo_check);
	      				$("#check_estudios").val(data.estudios_check);
	      				$("#check_visita").val(data.visita_check);
	      				$("#check_penales").val(data.penales_check);
	      				$("#check_ofac").val(data.ofac_check);
	      				$("#check_laboratorio").val(data.laboratorio_check);
	      				$("#check_medico").val(data.medico_check);
	      				$("#check_global").val(data.global_searches_check);
	      				$("#check_domicilio").val(data.domicilios_check);
	      				$("#check_credito").val(data.credito_check);
	      				
	      				$("#div_incompleto").css('display','none');
	  					$("#div_completo").css('display','block');
	  					$("#btnTerminar").css('display','initial');
	  					$("#completarModal").modal('show');
	      			}
	      			
	      		});
	      		$("a#final_ofac", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			checkOfac();
	      		});
	      		$('a[id^=pdfFinal]', row).bind('click', () => {
		            var id = data.id;
		            $('#pdf'+id).submit();
		        });
		        $('a[id^=pdfDoping]', row).bind('click', () => {
		            var id = data.idDoping;
		            $('#pdf'+id).submit();
		        });
		        $('a[id^=pdfOfac]', row).bind('click', () => {
		            var id = data.id;
		            $('#ofacpdf'+id).submit();
		        });
		        $('a#verCancelado', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('index.php/Candidato/getCancelacion'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	datos = res.split('##');
			            	var fecha = new Date(datos[0]);
							var options = { year: 'numeric', month: 'long', day: 'numeric' };
			            	$("#titulo_accion").text("Cancelado");
			            	$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue cancelado el '+fecha.toLocaleDateString("es-ES", options));
			            	$("#motivo").html("Comentario: <br>"+datos[1]);
			            	$("#verModal").modal('show');

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
		        $('a#verEliminado', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('index.php/Candidato/getEliminacion'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	datos = res.split('##');
			            	var fecha = new Date(datos[0]);
							var options = { year: 'numeric', month: 'long', day: 'numeric' };
			            	$("#titulo_accion").text("Eliminado");
			            	$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue eliminado el '+fecha.toLocaleDateString("es-ES", options));
			            	$("#motivo").html("Comentario: <br>"+datos[1]);
			            	$("#verModal").modal('show');

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
		        $("a#generar", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$(".correo").val(data.correo);
	      			$("#titulo_accion").text("Generar password");
	      			$("#texto_confirmacion").html("Estás seguro de generar un nuevo password para <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','generate');
	      			$("#quitarModal").modal("show");
	      		});
	      		$('a#comentario', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('index.php/Candidato/viewComentario'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
			            		$("#titulo_accion").text("Comentario del candidato");
				            	//$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue cancelado el '+fecha.toLocaleDateString("es-ES", options));
				            	$("#motivo").html(res);
				            	$("#verModal").modal('show');
			            	}
			            	else{
			            		$("#titulo_accion").text("Comentarios del candidato");
				            	$("#motivo").html("No hay registro de comentarios");
				            	$("#verModal").modal('show');
			            	}
			            	

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
      		},
	      	"language": {
	        	"lengthMenu": "Mostrar _MENU_ registros por página",
	        	"zeroRecords": "No se encontraron registros",
	        	"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        	"infoEmpty": "Sin registros disponibles",
	        	"infoFiltered": "(Filtrado _MAX_ registros totales)",
	        	"sSearch": "Buscar:",
	        	"oPaginate": {
	          		"sLast": "Última página",
	          		"sFirst": "Primera",
	          		"sNext": "<i class='fa  fa-arrow-right'></i>",
	          		"sPrevious": "<i class='fa fa-arrow-left'></i>"
	        	}
	      	}
    	}); 
    	$('#tabla').DataTable().search(" ");
    	$("#proyecto_registro").change(function(){
	      	var id_proyecto = $(this).val();
	      	if(id_proyecto != "" && id_proyecto != 0){
	        	$.ajax({
	          		url: '<?php echo base_url('Cliente/getPaqueteSubclienteProyecto'); ?>',
	          		method: 'POST',
	          		data: {'id_proyecto':id_proyecto},
	          		dataType: "text",
	          		success: function(res)
	          		{
	          			if(res != ""){
	            			$('#examen_registro').html(res);
	          			}
	          		}
	        	});
	      	}
	      	else{
	      		$('#examen').empty();
	      		$('#examen').append($("<option selected></option>").attr("value","").text("Select"));
	      	}	      	
	    });	
	  	$("#estado").change(function(){
	      	var id_estado = $(this).val();
	      	if(id_estado != -1){
	        	$.ajax({
	          		url: '<?php echo base_url('Candidato/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#municipio').prop('disabled', false);
	            		$('#municipio').html(res);
	          		}
	        	});
	      	}
	      	else{
        		$('#municipio').prop('disabled', true);
	        	$('#municipio').append($("<option selected></option>").attr("value",-1).text("Selecciona"));
	      	}
	    });
	    $(".custom_estado").change(function(){
	      	var id_estado = $(this).val();
	      	var seleccionado = $(this).attr("id");
	      	var aux = seleccionado.split('custom_estado');
	      	var num = aux[1];
	      	if(id_estado != ''){
	        	$.ajax({
	          		url: '<?php echo base_url('Candidato/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#custom_municipio'+num).prop('disabled', false);
	            		$('#custom_municipio'+num).html(res);
	          		}
	        	});
	      	}
	      	else{
        		$('#custom_municipio'+num).prop('disabled', true);
	        	$('#custom_municipio'+num).append($("<option selected></option>").attr("value",-1).text("Selecciona"));
	      	}
	    });
	    $(".address_estado").change(function(){
	      	var id_estado = $(this).val();
	      	var seleccionado = $(this).attr("id");
	      	var aux = seleccionado.split('address_estado');
	      	var num = aux[1];
	      	if(id_estado != ''){
	        	$.ajax({
	          		url: '<?php echo base_url('Candidato/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#address_municipio'+num).prop('disabled', false);
	            		$('#address_municipio'+num).html(res);
	          		}
	        	});
	      	}
	      	else{
        		$('#address_municipio'+num).prop('disabled', true);
	        	$('#address_municipio'+num).append($("<option selected></option>").attr("value",-1).text("Selecciona"));
	      	}
	    });
	    $("#fecha_nacimiento_registro, #refLab1_entrada, #refLab1_salida, #refLab2_entrada, #refLab2_salida").datetimepicker({
	  		minView: 2,
	    	format: "mm/dd/yyyy",
	    	startView: 4,
	    	autoclose: true,
	    	todayHighlight: true,
	    	pickerPosition: "bottom-left",
	    	forceParse: false
  		});
  		$("#docsModal").on("hidden.bs.modal", function(){
	    	$("#documento").val("");
	    });
	    $("#ofacModal").on("hidden.bs.modal", function(){
	    	$(".ofac").removeClass("requerido");
	    	$("#campos_vacios_ofac").css('display','none');
	    });
	    $("#completarModal").on("hidden.bs.modal", function(){
	    	$("#completarModal select").val(-1);
	    	$("#completarModal textarea").val("");
	    	$("#completarModal select, #completarModal textarea").removeClass("requerido");
	    	$("#completarModal #campos_vacios_check").css('display','none');
	    });
	    $("#guardarCustomDatabase").click(function(){
			var database = $("#d_custom_database").serialize();
			database += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.custom_database_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".custom_database_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#customDatabaseModal #msj_texto").text(" Hay campos obligatorios vacíos");
			          	$('#customDatabaseModal #msj_error').css("display", "block");
			          	setTimeout(function(){
			            	$('#customDatabaseModal #msj_error').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_custom_database').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'custom_database_check-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateDatabaseCheck'); ?>',
		        	method: "POST",  
		            data: {'database':database},
		            beforeSend: function(){
		                $('.loader').css("display","block");
		            },
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#customDatabaseModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarVerificacionIdentidad").click(function(){
			var d_identidad = $("#d_custom_identidad").serialize();
			d_identidad += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.custom_identidad_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".custom_identidad_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_custom_identidad').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'verificacion_identidad-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateIdentidadCandidato'); ?>',
		        	method: "POST",  
		            data: {'d_identidad':d_identidad},
		            beforeSend: function(){
		                $('.loader').css("display","block");
		            },
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#identidadModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
	    $("#registrar").click(function(){
			var datos = new FormData();
			var correo = $("#correo_registro").val();
			var proyecto = $("#proyecto_registro").val();
			datos.append('correo', $("#correo_registro").val());
			datos.append('nombre', $("#nombre_registro").val());
			datos.append('paterno', $("#paterno_registro").val());
			datos.append('materno', $("#materno_registro").val());
			datos.append('celular', $("#celular_registro").val());
			datos.append('fijo', $("#fijo_registro").val());
			datos.append('fecha_nacimiento', $("#fecha_nacimiento_registro").val());
			datos.append('proyecto', proyecto);
			datos.append('id_cliente', 2);
			datos.append('examen', $("#examen_registro").val());
			//console.log($("#ine")[0].files[0])
			var totalVacios = $('.registro_obligado').filter(function(){
	        	return !$(this).val();
	      	}).length;
	      
	      	if(totalVacios > 0){
	        	$(".registro_obligado").each(function() {
	          		var element = $(this);
	          		if (element.val() == "") {
	            		element.addClass("requerido");
	            		$("#newModal #msj_texto").text('Hay campos obligatorios vacios');
	            		$("#newModal #msj_error").css('display','block');
			            setTimeout(function(){
			              $('#newModal #msj_error').fadeOut();
			            },4000);
	          		}
	          		else{
	            		element.removeClass("requerido");
	          		}
	        	});
	      	}
	      	else{
	      		if(!isEmail(correo) || correo == ""){
          			$("#newModal #msj_texto").text('El correo no es valido');
            		$("#newModal #msj_error").css('display','block');
		            setTimeout(function(){
		              $('#newModal #msj_error').fadeOut();
		            },4000);
        		}
        		else{
        			if(proyecto != 128){
        				$.ajax({
			              	url: '<?php echo base_url('Cliente/addCandidate'); ?>',
			              	type: 'POST',
			              	data: datos,
			              	contentType: false,  
			     			cache: false,  
			     			processData:false,
			              	beforeSend: function() {
			                	$('.loader').css("display","block");
			              	},
			              	success : function(res){ 
			              		if(res == 0){
				                  	$('.loader').fadeOut();
			              			$("#newModal #msj_texto").text('El candidato ya existe');
				            		$("#newModal #msj_error").css('display','block');
						            setTimeout(function(){
						              $('#newModal #msj_error').fadeOut();
						            },4000);
			              		}
			              		if(res.substring(0,4) == "Sent"){
			              			dato = res.split('@@');
		              				setTimeout(function(){
				                  		$('.loader').fadeOut();
				                	},300);
			                		$("#newModal").modal('hide');
			                		recargarTable();
			                		$("#exito").css('display','block');
			                		$("#user").text(correo);
			                		$("#pass").text(dato[1]);
			                		$("#respuesta_mail").text("* A email has been sent with this credentials to candidate. This email could take a few minutes to be delivered.");
			                		$("#passModal").modal('show');
			                		setTimeout(function(){
				                  		$('#exito').fadeOut();
				                	},3000);
			              		}
			              		if(res == "creado"){
		              				setTimeout(function(){
				                  		$('.loader').fadeOut();
				                	},300);
			                		$("#newModal").modal('hide');
			                		recargarTable();
			                		$("#texto_msj").text('El candidato se ha registrado correctamente');
			                		$("#mensaje").css('display','block');
			                		setTimeout(function(){
				                  		$('#mensaje').fadeOut();
				                	},3000);
			              		}
			              		if(res != 0 && res.substring(0,4) != "Sent" && res != "creado"){
			              			//var dato = res.split('@@');
			              			setTimeout(function(){
				                  		$('.loader').fadeOut();
				                  	},200);
				                  	recargarTable();
				                  	$("#alert-msg").css('display','block');
			              			$('#alert-msg').html('<div class="alert alert-danger">' + res + '</div>');
			              		}
			              	}
		            	});	
        			}
        			else{
        				$.ajax({
			              	url: '<?php echo base_url('Cliente/addCandidate'); ?>',
			              	type: 'POST',
			              	data: datos,
			              	contentType: false,  
			     			cache: false,  
			     			processData:false,
			              	beforeSend: function() {
			                	$('.loader').css("display","block");
			              	},
			              	success : function(res){ 
			              		if(res == 0){
				                  	$('.loader').fadeOut();
			              			$('#campos_vacios').css("display", "none");
				          			$('#correo_invalido').css("display", "none");
				          			$('#repetido').css("display", "block");
				          			setTimeout(function(){
				            			$("#repetido").fadeOut();
				          			},4000);
			              		}
			              		if(res == "creado"){
		              				setTimeout(function(){
				                  		$('.loader').fadeOut();
				                	},300);
			                		$("#newModal").modal('hide');
			                		recargarTable();
			                		$("#texto_msj").text('El candidato se ha registrado correctamente');
			                		$("#mensaje").css('display','block');
			                		setTimeout(function(){
				                  		$('#mensaje').fadeOut();
				                	},3000);
			              		}
			              	}
		            	});
        			}
        		}
	      	}
		});
	    $("#guardarGenerales").click(function(){
			var d_generales = $("#d_generales").serialize();
			d_generales += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.personal_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".personal_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_generales').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'datos_generales-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/actualizarDatosGenerales'); ?>',
		        	method: "POST",  
		            data: {'d_generales':d_generales},
		            beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#generalesModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarGeneralesInternacional").click(function(){
			var d_generales = $("#d_generales_internacional").serialize();
			d_generales += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.inter_personal_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".inter_personal_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_generales_internacional').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'datos_generales-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/actualizarDatosGeneralesInternacionales'); ?>',
		        	method: "POST",  
		            data: {'d_generales':d_generales},
		            beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#generalesInternacionalModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#actualizarGlobalSearches").click(function(e){
   			e.preventDefault();
			var d_global = $("#d_global").serialize();
			d_global += "&id_candidato="+$("#idCandidato").val();
			var total = $('.global_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(total > 0){
		      	$(".global_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos vacíos en la sección de global searches.");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},3000);
			          	$('html, body').animate({
						    scrollTop: $("#titulo_global").offset().top
						}, 2000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateGlobalSearches'); ?>',
		        	method: "POST",  
		            data: {'d_global':d_global},
		        	success: function(res){
		        		if(res != 1){
		        			$("#msj_datospersonales").css('display','initial');
		        			$("#msj_datospersonales").html(res);
		        		}
		        		else{
		        			$("#msj_datospersonales").css('display','none');
		        			$("#msj_datospersonales").empty();
		        			$("#texto_msj").text('Los datos de global searches se han guardado correctamente');
			        		$("#mensaje").css('display','block');
			        		setTimeout(function(){
			              		$('#mensaje').fadeOut();
			            	},6000);
		        		}
		        	},error:function(res){
		          		$('#errorModal').modal('show');
		        	}
		      	});
		    }
   		});
   		$("#actualizarDomicilios").click(function(e){
			e.preventDefault();
			var d_doms = $("#d_domicilios").serialize();
			d_doms += "&id_candidato="+$("#idCandidato").val();
			var total = $('.dom_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(total > 0){
		      	$(".dom_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos vacíos en la sección del Historial de Domicilios.");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},3000);
			          	/*$('html, body').animate({
						    scrollTop: $("#titulo_estudios").offset().top
						}, 2000);*/
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	//console.log(d_personal)
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/verificarDomicilios'); ?>',
		        	method: "POST",  
		            data: {'d_doms':d_doms},
		        	success: function(res){
		        		if(res != 1){
		        			$("#msj_datospersonales").css('display','initial');
		        			$("#msj_datospersonales").html(res);
		        		}
		        		else{
		        			$("#msj_datospersonales").css('display','none');
		        			$("#msj_datospersonales").empty();
		        			$("#texto_msj").text('Los datos del Historial de Domicilios se han actualizado correctamente');
			        		$("#mensaje").css('display','block');
			        		setTimeout(function(){
			              		$('#mensaje').fadeOut();
			            	},6000);
		        		}
		        	},error:function(res){
		          		$('#errorModal').modal('show');
		        	}
		      	});
		    }
		});
   		$("#actualizarMayoresEstudios").click(function(e){
			e.preventDefault();
			var d_estudios = $("#d_mayor_estudios").serialize();
			d_estudios += "&id_candidato="+$("#idCandidato").val();
			d_estudios += "&id_ver_estudios="+$("#idMayoresEstudios").val();
			var total = $('.mayor_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(total > 0){
		      	$(".mayor_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},3000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_mayor_estudios').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'mayores_estudios-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/actualizarMayoresEstudios'); ?>',
		        	method: "POST",  
		            data: {'d_estudios':d_estudios},
		            beforeSend: function(){
		                $('.loader').css("display","block");
		            },
		        	success: function(res){
		        		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  	},200);
	                  	$("#mayoresEstudiosModal").modal('hide');
              			localStorage.setItem("success", 1);
            			location.reload();
		        	}
		      	});
		    }
		});
		$("#guardarGlobalesGeneral").click(function(){
			var d_global = $("#d_globales_general").serialize();
			d_global += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.global_general_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".global_general_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_globales_general').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'global_data_searches-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateGlobalSearches'); ?>',
		        	method: "POST",  
		            data: {'d_global':d_global},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#globalesGeneralModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarGlobalesEspecial").click(function(){
			var d_global = $("#d_globales_especiales").serialize();
			d_global += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.global_especial_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".global_especial_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_globales_especiales').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'global_data_searches-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateGlobalSearches'); ?>',
		        	method: "POST",  
		            data: {'d_global':d_global},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#globalesEspecialesModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarGlobalesEspecifico").click(function(){
			var d_global = $("#d_globales_especifico").serialize();
			d_global += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.global_especifico_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".global_especifico_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_globales_especifico').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'global_data_searches-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateGlobalSearches'); ?>',
		        	method: "POST",  
		            data: {'d_global':d_global},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#globalesEspecificosModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarGlobalesSanctions").click(function(){
			var d_global = $("#d_globales_sanctions").serialize();
			d_global += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.global_sanctions_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".global_sanctions_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_globales_sanctions').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'global_data_searches-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateGlobalSearches'); ?>',
		        	method: "POST",  
		            data: {'d_global':d_global},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#globalesSanctionsModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarGlobalesInternal").click(function(){
			var d_global = $("#d_globales_internal").serialize();
			d_global += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.global_internal_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".global_internal_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos obligatorios vacíos");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_globales_internal').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'global_data_searches-'+idCandidato+'-'+fecha_txt);
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateGlobalSearches'); ?>',
		        	method: "POST",  
		            data: {'d_global':d_global},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#globalesInternalModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
   		$("#guardarDocumentacion").click(function(e){
   			e.preventDefault();
			var d_documentos = $("#d_documentos").serialize();
			d_documentos += "&id_candidato="+$("#idCandidato").val();
			var totalPer = $('.documento_obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;
	    	if(totalPer > 0){
		      	$(".documento_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$("#documentosModal #msj_texto").text(" Hay campos obligatorios vacíos");
			          	$('#documentosModal #msj_error').css("display", "block");
			          	setTimeout(function(){
			            	$('#documentosModal #msj_error').fadeOut();
			          	},4000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	formdata = $('#d_documentos').serialize();
		    	var idCandidato = $("#idCandidato").val();
		    	var f = new Date();
				var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
				respaldoTxt(formdata, 'documentacion_verificacion-'+idCandidato+'-'+fecha_txt);
				$.ajax({
		        	url: '<?php echo base_url('Candidato/verificacionDocumentosCandidato'); ?>',
		        	method: "POST",  
		            data: {'d_documentos':d_documentos},
		            beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
		        	success: function(res){
		        		if(res == 1){
		        			setTimeout(function(){
		                  		$('.loader').fadeOut();
		                  	},200);
		                  	$("#documentosModal").modal('hide');
	              			localStorage.setItem("success", 1);
	            			location.reload();
		        		}
		        	}
		      	});
		    }
   		});
		$("#actualizarArchivos").click(function(e){
   			e.preventDefault();
			var d_images = $("#d_images").serialize();
			var id_candidato = $("#idCandidato").val();
			var agregados = $('input[id^=imagen]:checked').length;
			//alert(agregados)
    		if(agregados == 0){
    			$("#txt_vacios").text(" No hay archivos seleccionados.");
	          	$('#vacios').css("display", "block");
	          	setTimeout(function(){
	            	$('#vacios').fadeOut();
	          	},3000);
	          	$('html, body').animate({
				    scrollTop: $("#titulo_archivos").offset().top
				}, 2000);
    		}
		    else{
			    $.ajax({
		        	url: '<?php echo base_url('Candidato/updateArchivos1'); ?>',
		        	method: "POST",  
		            data: {'d_images':d_images,'id_candidato':id_candidato},
		        	success: function(res){
		        		if(res != 1){
		        			$("#msj_datospersonales").css('display','initial');
		        			$("#msj_datospersonales").html(res);
		        		}
		        		else{
		        			$("#msj_datospersonales").css('display','none');
		        			$("#msj_datospersonales").empty();
		        			$("#texto_msj").text('Los archivos se han agregado correctamente');
			        		$("#mensaje").css('display','block');
			        		setTimeout(function(){
			              		$('#mensaje').fadeOut();
			            	},6000);
		        		}
		        	},error:function(res){
		          		$('#errorModal').modal('show');
		        	}
		      	});
		    }
   		});
  	});
	function regresarListado(){
		location.reload();
	}
	//Sube un nuevo documento del candidato
	function subirDoc(){
        if($("#documento").val() == ""){
          $("#sin_documento").css("display","block");
          setTimeout(function(){
            $("#sin_documento").fadeOut();
          }, 4000);
        }
        else{
        	var id_candidato = $("#idCandidato").val();
		    var prefijo = $(".prefijo").val();
		    //var form = document.getElementById("formSubirDoc");
	        var data = new FormData();
	        var doc = $("#documento")[0].files[0];
	        data.append('id_candidato', id_candidato);
	        data.append('prefijo', prefijo);
	        data.append('documento', doc);
          	$.ajax({
             	url:"<?php echo base_url('Candidato/uploadDoc'); ?>",   
             	method:"POST",  
             	data:data,  
             	contentType: false,  
             	cache: false,  
             	processData:false,
             	
             	success:function(res){
         			$("#documento").val("");
         			
                	$("#subido").css('display','block');
            		setTimeout(function(){
                  		$('#subido').fadeOut();
                	},4000);
            		$("#tablaDocs").empty();
            		$("#tablaDocs").html(res);
            	}  
          	});
        }
    }  
	//Se actualizan los documentos referenciando por su tipo
	function actualizarDocs(){
		$("#sin_documento").css("display","none");
		var total = $("select[id^='tipo']").length;

		var totalVacios = $('.item').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".item").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#tipos_iguales").css('display','none');
					$("#campos_vacios").css('display','block');
					setTimeout(function(){
						$("#campos_vacios").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	var id_candidato = $("#idCandidato").val();
	    	var prefijo = $(".prefijo").val();
	    	var salida = "";
			var indicador = 0;
			var aux = new Array();
    		for(var i = 0; i < total; i++){
    			var arch = $("#arch"+i).text();
    			var tipo = $("#tipo"+i).val();
    			//console.log("idc "+id_candidato+" pre: "+prefijo+" arch: "+arch+" tipo: "+tipo);
    			/*if(aux.includes(tipo) && tipo != 13){
    				indicador = 1;
    			}
    			else{
    				salida += arch+","+tipo+"@@";
    			}*/
    			salida += arch+","+tipo+"@@";
    			aux[i] = tipo;
    		}
    		if(indicador == 1){
    			$("#tipos_iguales").css('display','block');
				setTimeout(function(){
					$("#tipos_iguales").fadeOut();
				},4000);
    		}
	    	else{
	    		$.ajax({
	          		url: '<?php echo base_url('Candidato/saveDocs'); ?>',
	          		method: 'POST',
	          		data: {'docs':salida,'id_candidato':id_candidato,'prefijo':prefijo},
	          		dataType: "text",
	          		beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	          		success: function(res)
	          		{
	          			if(res == 1){
	          				setTimeout(function(){
		                  		$('.loader').fadeOut();
		                	},300);
		                	$("#exito").css('display','block');
		                	$("#docsModal").modal('hide');
	                		setTimeout(function(){
		                  		$('#exito').fadeOut();
		                	},5000);
	          			}
	          		},error:function(res)
	          		{
	            		//$('#errorModal').modal('show');
	          		}
	        	});
	    	}
	    }
		
	}
	//Modal para generar estatus de verificación de documentos
	function verificacionEstudios(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_estudio").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_estudio").css('display','none');
        				$("#btnTerminarVerificacionEstudio").css('display','none');
        			}
        			$("#div_crearEstatusEstudio").empty();
        			$("#idVerificacionEstudio").val(aux[1]);
        			$("#div_crearEstatusEstudio").append(aux[0]);
        		}
        		else{
        			$("#div_crearEstatusEstudio").empty();
        			$("#div_crearEstatusEstudio").append('<p class="text-center">Sin registros </p>');
        			$("#div_estatus_estudio").css('display','block');
        			$("#btnTerminarVerificacionEstudio").css('display','initial');
        			$("#idVerificacionEstudio").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionEstudiosModal").modal("show");
	}
	//Generar estatus de verificacion de estudio
	function generarEstatusEstudio(){
		var id_candidato = $("#idCandidato").val();
		var id_verificacion = $("#idVerificacionEstudio").val();
		var comentario = $("#estudio_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_verificacion':id_verificacion,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionEstudio").val(aux[1]);
        		$("#estudio_estatus_comentario").val("");
        		$("#div_crearEstatusEstudio").empty();
        		$("#div_crearEstatusEstudio").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de estudios
	function terminarEstudios(){
		var id_verificacion = $("#idVerificacionEstudio").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/finishEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_verificacion':id_verificacion},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarEstudiosModal").modal('hide');
      			$("#verificacionEstudiosModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoEstudios").css('display','block');
            	setTimeout(function(){
              		$('#exitoEstudios').fadeOut();
            	},5000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar estatus de verificación de referencias laborales
	function verificacionLaborales(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_laboral").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_laboral").css('display','none');
        				$("#btnTerminarVerificacionLaboral").css('display','none');
        			}
        			$("#div_crearEstatusLaboral").empty();
        			$("#idVerificacionLaboral").val(aux[1]);
        			$("#div_crearEstatusLaboral").append(aux[0]);
        		}
        		else{
        			$("#div_crearEstatusLaboral").empty();
        			$("#div_crearEstatusLaboral").append('<p class="text-center">Sin registros </p>');
        			$("#div_estatus_laboral").css('display','block');
        			$("#btnTerminarVerificacionLaboral").css('display','initial');
        			$("#idVerificacionLaboral").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionLaboralesModal").modal("show");
	}
	//Generar estatus de verificacion de referencia laboral
	function generarEstatusLaboral(){
		var id_candidato = $("#idCandidato").val();
		var id_status = $("#idVerificacionLaboral").val();
		var comentario = $("#laboral_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_status':id_status,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionLaboral").val(aux[1]);
        		$("#laboral_estatus_comentario").val("");
        		$("#div_crearEstatusLaboral").empty();
        		$("#div_crearEstatusLaboral").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de ref laborales
	function terminarLaborales(){
		var id_status = $("#idVerificacionLaboral").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/finishEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_status':id_status},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarLaboralesModal").modal('hide');
      			$("#verificacionLaboralesModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoLaborales").css('display','block');
            	setTimeout(function(){
              		$('#exitoLaborales').fadeOut();
            	},5000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar estatus de verificación de antecedentes no penales
	function verificacionPenales(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_penales").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_penales").css('display','none');
        				$("#btnTerminarVerificacionPenales").css('display','none');
        			}
        			$("#div_crearEstatusPenales").empty();
        			$("#idVerificacionPenales").val(aux[1]);
        			$("#div_crearEstatusPenales").append(aux[0]);
        		}
        		else{
        			$("#div_crearEstatusPenales").empty();
        			$("#div_crearEstatusPenales").append('<p class="text-center">Sin registros </p>');
        			$("#div_estatus_penales").css('display','block');
        			$("#btnTerminarVerificacionPenales").css('display','initial');
        			$("#idVerificacionPenales").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionPenalesModal").modal("show");
	}
	//Generar estatus de verificacion de antecdentes no penales
	function generarEstatusPenales(){
		var id_candidato = $("#idCandidato").val();
		var id_status = $("#idVerificacionPenales").val();
		var comentario = $("#penales_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_status':id_status,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionPenales").val(aux[1]);
        		$("#penales_estatus_comentario").val("");
        		$("#div_crearEstatusPenales").empty();
        		$("#div_crearEstatusPenales").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de antecedentes no penales
	function terminarPenales(){
		var id_status = $("#idVerificacionPenales").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/finishEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_status':id_status},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarPenalesModal").modal('hide');
      			$("#verificacionPenalesModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoPenales").css('display','block');
            	setTimeout(function(){
              		$('#exitoPenales').fadeOut();
            	},5000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	function estatusAvances(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_avances").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkAvances'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_avances").css('display','none');
        			}
        			$("#div_crearEstatusAvances").empty();
        			$("#idAvances").val(aux[1]);
        			$("#div_crearEstatusAvances").append(aux[0]);
        		}
        		else{
        			$("#idAvances").val(0);
        			$("#div_crearEstatusAvances").empty();
        			$("#div_crearEstatusAvances").html('<p class="text-center">Sin registros </p>');
        		}

      		}
    	});
		$("#avancesModal").modal("show");
	}
	function generarEstatusAvance(){
		var id_candidato = $("#idCandidato").val();
		var id_avance = $("#idAvances").val();
		var comentario = $("#avances_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createEstatusAvance'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_avance':id_avance,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idAvances").val(aux[1]);
        		$("#avances_estatus_comentario").val("");
        		$("#div_crearEstatusAvances").empty();
        		$("#div_crearEstatusAvances").append(aux[0]);
      		}
    	});
	}
	function estatusLlamadas(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_llamadas").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkLlamadas'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_llamadas").css('display','none');
        			}
        			$("#div_crearEstatusLlamadas").empty();
        			$("#idLlamadas").val(aux[1]);
        			$("#div_crearEstatusLlamadas").append(aux[0]);
        		}
        		else{
        			$("#idLlamadas").val(0);
        			$("#div_crearEstatusLlamadas").empty();
        			$("#div_crearEstatusLlamadas").html('<p class="text-center">Sin registros </p>');
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#llamadasModal").modal("show");
	}
	function generarEstatusLlamada(){
		var id_candidato = $("#idCandidato").val();
		var id_llamada = $("#idLlamadas").val();
		var comentario = $("#llamadas_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusLlamada'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_llamada':id_llamada,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idLlamadas").val(aux[1]);
        		$("#llamadas_estatus_comentario").val("");
        		$("#div_crearEstatusLlamadas").empty();
        		$("#div_crearEstatusLlamadas").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar registros de las llamadas hechas al candidato
	function estatusEmails(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_emails").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkEmails'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_emails").css('display','none');
        			}
        			$("#div_crearEstatusEmail").empty();
        			$("#idEmails").val(aux[1]);
        			$("#div_crearEstatusEmail").append(aux[0]);
        		}
        		else{
        			$("#idEmails").val(0);
        			$("#div_crearEstatusEmail").empty();
        			$("#div_crearEstatusEmail").html('<p class="text-center">Sin registros </p>');
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#emailsModal").modal("show");
	}
	//Generar estatus de llamada candidato
	function generarEstatusEmail(){
		var id_candidato = $("#idCandidato").val();
		var id_email = $("#idEmails").val();
		var comentario = $("#emails_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusEmail'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_email':id_email,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idEmails").val(aux[1]);
        		$("#emails_estatus_comentario").val("");
        		$("#div_crearEstatusEmail").empty();
        		$("#div_crearEstatusEmail").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	function generarGap(){
		var id_candidato = $("#idCandidato").val();
		var razon = $("#razon").val();
		var fi = $("#fecha_inicio").val();
		var ff = $("#fecha_fin").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createGap'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'fi':fi,'ff':ff,'razon':razon},
      		dataType: "text",
      		success: function(res)
      		{
      			$("#fecha_inicio").val("");
      			$("#fecha_fin").val("");
        		$("#razon").val("");
        		$("#div_antesgap").empty();
        		$("#div_antesgap").append(res);
      		}
    	});
	}
	function generarHistorialCredito(){
		var id_candidato = $("#idCandidato").val();
		var comentario = $("#credito_comentario").val();
		var fi = $("#credito_fecha_inicio").val();
		var ff = $("#credito_fecha_fin").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/createHistorialCrediticio'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'fi':fi,'ff':ff,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			$("#credito_fecha_inicio").val("");
      			$("#credito_fecha_fin").val("");
        		$("#credito_comentario").val("");
        		$("#div_antescredit").empty();
        		$("#div_antescredit").append(res);
      		}
    	});
	}
	function estatusOFAC(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkOfac'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
  				var datos = res.split('@@');
  				if(datos[0] == 0){
  					$("#fecha_titulo_ofac").html("<b>Fecha</b>");
  					$("#fecha_estatus_ofac").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
  				}
  				else{
  					$("#fecha_titulo_ofac").html("<b>Última actualización</b>");
  					$("#fecha_estatus_ofac").text(datos[0]);
  				}
    			$("#estatus_ofac").val(datos[1]);
    			$("#res_ofac").val(datos[2]);
    			$("#estatus_oig").val(datos[3]);
    			$("#res_oig").val(datos[4]);

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#ofacModal").modal("show");
	}
	function actualizarOfac(){
		var id_cliente = $("#idCliente").val();
		var id_candidato = $("#idCandidato").val();
		var ofac = $("#estatus_ofac").val();
		var oig = $("#estatus_oig").val();
		var res_ofac = $("#res_ofac").val();
		var res_oig = $("#res_oig").val();

		var totalVacios = $('.ofac').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".ofac").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
					$("#campos_vacios_ofac").css('display','block');
					setTimeout(function(){
						$("#campos_vacios_ofac").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
	      		url: '<?php echo base_url('Candidato/updateEstatusOfac'); ?>',
	      		method: 'POST',
	      		data: {'id_candidato':id_candidato,'ofac':ofac,'oig':oig,'res_ofac':res_ofac,'res_oig':res_oig,'id_cliente':id_cliente},
	      		dataType: "text",
	      		success: function(res)
	      		{
	      			$("#ofacModal").modal('hide');
	      			$("#texto_msj").text('Se ha actualizado la verificación del OFAC y OIG correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},6000);
	        		
	      		},error:function(res)
	      		{
	        		//$('#errorModal').modal('show');
	      		}
	    	});
	    }
	}
	function getMunicipio(id_estado, id_municipio){
		$.ajax({
        	url: '<?php echo base_url('index.php/Candidato/getMunicipios'); ?>',
        	method: 'POST',
        	data: {'id_estado':id_estado},
        	dataType: "text",
        	success: function(res){
          		$('#municipio').prop('disabled', false);
          		$('#municipio').html(res);
          		$("#municipio").find('option').attr("selected",false) ;
          		$('#municipio option[value="'+id_municipio+'"]').attr('selected', 'selected');
        	},error:function(res){
          		$('#errorModal').modal('show');
        	}
      	});
	}
	function getMunicipioDomicilio(id_estado, id_municipio, num, tipo){
		$.ajax({
        	url: '<?php echo base_url('Candidato/getMunicipios'); ?>',
        	method: 'POST',
        	data: {'id_estado':id_estado},
        	dataType: "text",
        	success: function(res){
        		if(tipo == 'custom'){
        			$('#custom_municipio'+num).prop('disabled', false);
	          		$('#custom_municipio'+num).html(res);
	          		$("#custom_municipio"+num).find('option').attr("selected",false) ;
	          		$('#custom_municipio'+num+' option[value="'+id_municipio+'"]').attr('selected', 'selected');
        		}
          		if(tipo == 'general'){
        			$('#address_municipio'+num).prop('disabled', false);
	          		$('#address_municipio'+num).html(res);
	          		$("#address_municipio"+num).find('option').attr("selected",false) ;
	          		$('#address_municipio'+num+' option[value="'+id_municipio+'"]').attr('selected', 'selected');
        		}
        	}
      	});
	}
	function ejecutarAccion(){
		var accion = $("#btnGuardar").val();
		var id_candidato = $("#idCandidato").val();
		var correo = $(".correo").val();
		if(accion == 'generate'){
			$.ajax({
              	url: '<?php echo base_url('index.php/Candidato/generate'); ?>',
              	type: 'post',
              	data: {'id_candidato':id_candidato,'correo':correo},
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		setTimeout(function(){
                  		$('.loader').fadeOut();
                	},300);
            		$("#quitarModal").modal('hide');
            		$("#user").text(correo);
            		$("#pass").text(res);
            		$("#respuesta_mail").text("* An email has been sent with this credentials to the candidate. This email could take a few minutes to be delivered.");
            		$("#passModal").modal('show');
            		recargarTable();
            		$("#texto_msj").text('The password has been created succesfully');
            		$("#mensaje").css('display','block');
            		setTimeout(function(){
                  		$('#mensaje').fadeOut();
                	},5000);
              	},error: function(res){
                	$('#errorModal').modal('show');
              	}
        	});
		}
	}
	function checkOfac(){
		var id_candidato = $("#idCandidato").val();
		$.ajax({
      		url: '<?php echo base_url('Candidato/verificarOfacCandidato'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res == 0){
      				$("#ofac_incompleto").css('display','initial');
  					$("#ofac_completo").css('display','none');
  					$("#ofac_msjIncompleto").text("Faltan datos o los archivos de la verificación de OFAC y OIG");
  					$("#btnOfac").css('display','none');
  					$("#completarOfacModal").modal('show');
      			}
      			else{
  					$("#ofac_completo").css('display','block');
  					$("#ofac_incompleto").css('display','initial');
  					$("#btnOfac").css('display','initial');
  					$("#completarOfacModal").modal('show');
      			}
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	function saveVerificacionLaboral(cont) {
		var verlab = $("#data_vlaboral"+cont).serialize();
		verlab += "&id_candidato="+$("#idCandidato").val();
		//verlab += "&id_reflaboral="+$("#idreflab1").val();
		var total = $('.verlab'+cont+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(total > 0){
	      	$('.verlab'+cont+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en la Verificacion de Referencia Laboral #"+cont);
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		          	/*$('html, body').animate({
					    scrollTop: $("#titulo_reflab1").offset().top
					}, 2000);*/
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/insertVerificacionLaboral'); ?>',
	        	method: "POST",  
	            data: {'verlab':verlab,'cont':cont},
	        	success: function(res){
	        		if(res != 1){
	        			$("#msj_datospersonales").css('display','initial');
	        			$("#msj_datospersonales").html(res);
	        		}
	        		else{
	        			$("#msj_datospersonales").css('display','none');
	        			$("#msj_datospersonales").empty();
	        			$("#texto_msj").text('La verificación de la referencia laboral #'+cont+' se ha guardado correctamente');
		        		$("#mensaje").css('display','block');
		        		setTimeout(function(){
		              		$('#mensaje').fadeOut();
		            	},6000);
	        		}
	        	}
	      	});
	    }
	}
	function updateVerificacionLaboral(id,cont) {
		var verlab = $("#update_vlaboral"+cont).serialize();
		verlab += "&id_candidato="+$("#idCandidato").val();
		verlab += "&id="+$("#id").val();
		var total = $('.verlab'+cont+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(total > 0){
	      	$('.verlab'+cont+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en la Verificacion de Referencia Laboral #"+cont);
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		          	/*$('html, body').animate({
					    scrollTop: $("#titulo_reflab1").offset().top
					}, 2000);*/
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/updateVerificacionLaboral'); ?>',
	        	method: "POST",  
	            data: {'verlab':verlab,'cont':cont, 'id':id},
	        	success: function(res){
	        		if(res != 1){
	        			$("#msj_datospersonales").css('display','initial');
	        			$("#msj_datospersonales").html(res);
	        		}
	        		else{
	        			$("#msj_datospersonales").css('display','none');
	        			$("#msj_datospersonales").empty();
	        			$("#texto_msj").text('La verificación de la referencia laboral #'+cont+' se ha actualizado correctamente');
		        		$("#mensaje").css('display','block');
		        		setTimeout(function(){
		              		$('#mensaje').fadeOut();
		            	},6000);
	        		}
	        	}
	      	});
	    }
	}
	function finalizarOfac(){
		var id_candidato = $("#idCandidato").val();
		var checks = $("#ofacChecks").serialize();
		//console.log(checks)
		var totalVacios = $('.fin_ofac_obligado').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".fin_ofac_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "" || element.val() == -1) {
		          	element.addClass("requerido");
					$("#campos_vacios_ofac_final").css('display','block');
					setTimeout(function(){
						$("#campos_vacios_ofac_final").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
          		url: '<?php echo base_url('Candidato/finalizarOfacCandidato'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'checks':checks},
          		dataType: "text",
          		success: function(res)
          		{
            		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("finished", 1);
	            	location.reload();
          		},error:function(res)
          		{
            		//$('#errorModal').modal('show');
          		}
        	});
	    }
	}
	function actualizarTrabajoGobierno(){
		var inactivo = $("#trabajo_inactivo").val();
		var id_candidato = $("#idCandidato").val();
		var total = $('.trabajo_gobierno').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.trabajo_gobierno').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = inactivo;
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'trabajo_gobierno-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarTrabajoGobierno'); ?>',
	        	method: "POST",  
	            data: {'id_candidato':id_candidato,'inactivo':inactivo},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
        			setTimeout(function(){
						$('.loader').fadeOut();
					},300);
        			$("#texto_msj").text('La informacion se ha actualizado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},6000);
	        	}
	      	});
	    }
	}
	function actualizarReferenciaLaboral(num){
		var reflaboral = $("#candidato_reflaboral"+num).serialize();
		reflaboral += "&id_candidato="+$("#idCandidato").val();
		reflaboral += "&idref="+$("#idreflabingles"+num).val();
		var total = $('.reflabingles'+num+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.reflabingles'+num+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en la sección de la referencia laboral #"+num+".");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#candidato_reflaboral"+num).serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'referencia_laboral_'+num+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarReferenciaLaboralCandidato'); ?>',
	        	method: "POST",  
	            data: {'reflaboral':reflaboral,'num':num},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
        			setTimeout(function(){
						$('.loader').fadeOut();
					},300);
	        		$("#idreflab"+num).val(res);
        			$("#texto_msj").text('Los datos de la referencia laboral #'+num+' se han actualizado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},6000);
	        	}
	      	});
	    }
	}
	function verificarLaboral(num){
		var reflaboral = $("#analista_reflaboral"+num).serialize();
		reflaboral += "&id_candidato="+$("#idCandidato").val();
		reflaboral += "&idver="+$("#idverlab"+num).val();
		var total = $('.ver_reflab'+num+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
			
    	if(total > 0){
	      	$('.ver_reflab'+num+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text(" Hay campos vacíos en la sección de la verificación laboral #"+num+".");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},6000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#analista_reflaboral"+num).serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'verificacion_laboral_'+num+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarVerificacionLaboralCandidato'); ?>',
	        	method: "POST",  
	            data: {'reflaboral':reflaboral,'num':num},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
        			setTimeout(function(){
						$('.loader').fadeOut();
					},300);
	        		$("#idverlab"+num).val(res);
        			$("#texto_msj").text('Los datos de la verificación laboral #'+num+' se han actualizado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},6000);
	        	}
	      	});
	    }
	}
	function actualizarChecklist(){
		var id_candidato = $("#idCandidato").val();
		var checks = $("#d_checklist").serialize();

		var totalVacios = $('.list_check').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".list_check").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
					$("#ScopeModal #msj_texto").text(" Hay campos obligatorios vacíos");
		          	$('#ScopeModal #msj_error').css("display", "block");
		          	setTimeout(function(){
		            	$('#ScopeModal #msj_error').fadeOut();
		          	},3000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
          		url: '<?php echo base_url('Candidato/saveVerificacionChecklist'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'checks':checks},
          		beforeSend: function() {
                	$('.loader').css("display","block");
              	},
          		success: function(res)
          		{
            		if(res == 1){
	        			setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  	},200);
	                  	$("#ScopeModal").modal('hide');
              			localStorage.setItem("success", 1);
            			location.reload();
	        		}
          		}
        	});
	    }
	}
	function eliminarArchivo(fila,idDoc,archivo){
		$("#fila"+fila).remove();
		$.ajax({
      		url: '<?php echo base_url('Candidato/eliminarDocCandidato'); ?>',
      		method: 'POST',
      		data: {'idDoc':idDoc,'archivo':archivo},
      		dataType: "text",
      		success: function(res)
      		{
      			
      		}
    	});
	}
	function finalizarProceso(){
		var id_candidato = $("#idCandidato").val();
		var idBGC = $("#idBGC").val();
		var checks = $("#formChecks").serialize();
		//console.log(checks)
		var totalVacios = $('.check_obligado').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".check_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "" || element.val() == -1) {
		          	element.addClass("requerido");
					$("#campos_vacios_check").css('display','block');
					setTimeout(function(){
						$("#campos_vacios_check").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
          		url: '<?php echo base_url('Candidato/finishCandidateHCL'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'idBGC':idBGC,'checks':checks},
          		dataType: "text",
          		success: function(res)
          		{
            		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("finished", 1);
	            	location.reload();
          		}
        	});
	    }
	}
	function guardarDomicilio(num){
		var id_candidato = $("#idCandidato").val();
		var idDomicilio = $("#idDomicilio"+num).val();
		var doms = $("#d_custom_address"+num).serialize();

		var totalVacios = $('.custom_address_obligado'+num).filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".custom_address_obligado"+num).each(function() {
		        var element = $(this);
		        if (element.val() == "" || element.val() == -1) {
		          	element.addClass("requerido");
					$("#customAddressModal #msj_error"+num).css('display','block');
					$("#customAddressModal #msj_texto"+num).text(' Hay campos vacios en la Direccion #'+num);
					setTimeout(function(){
						$("#customAddressModal #msj_error"+num).fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
          		url: '<?php echo base_url('Candidato/actualizarDomicilioCandidato'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'idDomicilio':idDomicilio,'doms':doms,'num':num,'tipo':'custom'},
          		beforeSend: function(){
	                $('.loader').css("display","block");
	            },
          		success: function(res)
          		{
          			setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
            		$("#idDomicilio"+num).val(res);
        			$("#texto_msj").text('Los datos de Domicilio #'+num+' se han guardado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},4000);
          		}
        	});
	    }
	}
	function guardarComentarioDomicilio(){
		id_candidato = $("#idCandidato").val();
		comentario = $("#custom_address_comentarios").val();
		var total = $('.custom_comentario_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(total > 0){
	      	$(".custom_comentario_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#msj_texto_comentario").text(" El comentario esta vacio");
		          	$('#msj_error_comentario').css("display", "block");
		          	setTimeout(function(){
		            	$('#msj_error_comentario').fadeOut();
		          	},3000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/verificarDomiciliosCandidato'); ?>',
	        	method: "POST",  
	            data: {'id_candidato':id_candidato,'comentario':comentario},
	            beforeSend: function(){
	                $('.loader').css("display","block");
	            },
	        	success: function(res){
	        		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
        			$("#texto_msj").text('El comentario de los domicilios se ha guardado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},4000);
	        	}
	      	});
	    }
	}
	function guardarAddress(num){
		var id_candidato = $("#idCandidato").val();
		var idDomicilio = $("#idAddress"+num).val();
		var doms = $("#d_address"+num).serialize();

		var totalVacios = $('.address_obligado'+num).filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".address_obligado"+num).each(function() {
		        var element = $(this);
		        if (element.val() == "" || element.val() == -1) {
		          	element.addClass("requerido");
					$("#AddressModal #msj_error"+num).css('display','block');
					$("#AddressModal #msj_texto"+num).text(' Hay campos vacios en la Direccion #'+num);
					setTimeout(function(){
						$("#AddressModal #msj_error"+num).fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
          		url: '<?php echo base_url('Candidato/actualizarDomicilioCandidato'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'idDomicilio':idDomicilio,'doms':doms,'num':num,'tipo':'general'},
          		beforeSend: function(){
	                $('.loader').css("display","block");
	            },
          		success: function(res)
          		{
          			setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
            		$("#idAddress"+num).val(res);
        			$("#texto_msj").text('Los datos de Domicilio #'+num+' se han guardado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},4000);
          		}
        	});
	    }
	}
	function guardarAddressInternacional(num){
		var id_candidato = $("#idCandidato").val();
		var idDomicilio = $("#idAddressInternacional"+num).val();
		var doms = $("#d_address_internacional"+num).serialize();

		var totalVacios = $('.address_obligado_internacional'+num).filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".address_obligado_internacional"+num).each(function() {
		        var element = $(this);
		        if (element.val() == "" || element.val() == -1) {
		          	element.addClass("requerido");
					$("#AddressInternacionalModal #msj_error"+num).css('display','block');
					$("#AddressInternacionalModal #msj_texto"+num).text(' Hay campos vacios en la Direccion #'+num);
					setTimeout(function(){
						$("#AddressInternacionalModal #msj_error"+num).fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
          		url: '<?php echo base_url('Candidato/actualizarDomicilioCandidato'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'idDomicilio':idDomicilio,'doms':doms,'num':num,'tipo':'internacional'},
          		beforeSend: function(){
	                $('.loader').css("display","block");
	            },
          		success: function(res)
          		{
          			setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
            		$("#idAddress"+num).val(res);
        			$("#texto_msj").text('Los datos de Domicilio #'+num+' se han guardado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},4000);
          		}
        	});
	    }
	}
	function guardarComentarioAddress(){
		id_candidato = $("#idCandidato").val();
		comentario = $("#address_comentarios").val();
		var total = $('.address_comentario_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(total > 0){
	      	$(".address_comentario_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#msj_texto_comentario2").text(" El comentario esta vacio");
		          	$('#msj_error_comentario2').css("display", "block");
		          	setTimeout(function(){
		            	$('#msj_error_comentario2').fadeOut();
		          	},3000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/verificarDomiciliosCandidato'); ?>',
	        	method: "POST",  
	            data: {'id_candidato':id_candidato,'comentario':comentario},
	            beforeSend: function(){
	                $('.loader').css("display","block");
	            },
	        	success: function(res){
	        		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
        			$("#texto_msj").text('El comentario de los domicilios se ha guardado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},4000);
	        	}
	      	});
	    }
	}
	function guardarComentarioAddressInternacional(){
		id_candidato = $("#idCandidato").val();
		comentario = $("#address_comentarios_internacional").val();
		var total = $('.address_internacional_comentario_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(total > 0){
	      	$(".address_internacional_comentario_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#AddressInternacionalModal #msj_texto_comentario2").text(" El comentario esta vacio");
		          	$('#AddressInternacionalModal #msj_error_comentario2').css("display", "block");
		          	setTimeout(function(){
		            	$('#AddressInternacionalModal #msj_error_comentario2').fadeOut();
		          	},3000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
		    $.ajax({
	        	url: '<?php echo base_url('Candidato/verificarDomiciliosCandidato'); ?>',
	        	method: "POST",  
	            data: {'id_candidato':id_candidato,'comentario':comentario},
	            beforeSend: function(){
	                $('.loader').css("display","block");
	            },
	        	success: function(res){
	        		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},200);
        			$("#texto_msj").text('El comentario de los domicilios se ha guardado correctamente');
	        		$("#mensaje").css('display','block');
	        		setTimeout(function(){
	              		$('#mensaje').fadeOut();
	            	},4000);
	        	}
	      	});
	    }
	}
	function guardarRefProfesional(num){
		var d_refpro = $("#d_refprofesional"+num).serialize();
		var id_candidato = $("#idCandidato").val();
		d_refpro += "&id_refpro="+$("#id_refpro"+num).val();
		var totalPer = $('.refpro'+num+'_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	if(totalPer > 0){
	      	$('.refpro'+num+'_obligado').each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#refProfesionalesModal #msj_texto"+num).text(" Hay campos vacíos en la referencia profesional #"+num+".");
		          	$('#refProfesionalesModal #msj_error'+num).css("display", "block");
		          	setTimeout(function(){
		            	$('#refProfesionalesModal #msj_error'+num).fadeOut();
		          	},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	formdata = $("#d_refprofesional"+num).serialize();
	    	var idCandidato = $("#idCandidato").val();
	    	var f = new Date();
			var fecha_txt = f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
			respaldoTxt(formdata, 'referencia_profesional_'+num+'-'+idCandidato+'-'+fecha_txt);
		    $.ajax({
		    	async:false,
	        	url: '<?php echo base_url('Candidato/actualizarReferenciaProfesional'); ?>',
	        	method: "POST",  
	            data: {'d_refpro':d_refpro,'num':num,'id_candidato':id_candidato},
	            beforeSend: function(){
                    $('.loader').css("display","block");
                },
	        	success: function(res){
	        		setTimeout(function(){
						$('.loader').fadeOut();
		        		$("#id_refpro"+num).val(res);
	        			$("#texto_msj").text('La referencia profesional #'+num+' se ha guardado correctamente');
		        		$("#mensaje").css('display','block');
		        		setTimeout(function(){
		              		$('#mensaje').fadeOut();
		            	},6000);
	            	},300);
	        	}
	      	});
	    }
	}
	function respaldoTxt(formdata,nombreArchivo){      
	    var textFileAsBlob = new Blob([formdata], {type:'text/plain'});
	    var fileNameToSaveAs = nombreArchivo+".txt";
	    var downloadLink = document.createElement("a");
	    downloadLink.download = fileNameToSaveAs;
	    downloadLink.innerHTML = "My Hidden Link";
	    window.URL = window.URL || window.webkitURL;
	    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	    downloadLink.onclick = destroyClickedElement;
	    downloadLink.style.display = "none";
	    document.body.appendChild(downloadLink);
	    downloadLink.click();
	}
	function destroyClickedElement(event){
	    document.body.removeChild(event.target);
	}
	function convertirDate(fecha){
  		var aux = fecha.split('-');
    	var f = aux[1]+'/'+aux[2]+'/'+aux[0];
    	return f;
  	}
  	function convertirDateTime(fecha){
  		var aux = fecha.split(' ');
  		var time = aux[0].split('-');
    	var dia = time[1]+'/'+time[2]+'/'+time[0];
    	return dia;
  	}
  	function convertirFechaEspanol(fecha){
  		var aux = fecha.split(' ');
  		var time = aux[0].split('-');
    	var dia = time[2]+'/'+time[1]+'/'+time[0];
    	return dia;
  	}
  	//Regresa del formulario al listado
  	function regresar(){
  		$("#listado").css('display','block');
  		$("#btn_regresar").css('display','none'); 
  		$("#formulario").css('display','none');
  	}
  	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
  	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	$(".aplicar_todo").change(function(){
  		var id = $(this).attr('id');
  		var aux = id.split('aplicar_todo');
  		var num = aux[1];
  		var valor = $('#'+id).val();
  		switch(valor){
  			case "-1":
                $(".performance"+num).val("Not provided");
                break;
            case "0":
                $(".performance"+num).val("Not provided");
                break;
            case "1":
                $(".performance"+num).val("Excellent");
                break;
            case "2":
                $(".performance"+num).val("Good");
                break;
            case "3":
                $(".performance"+num).val("Regular");
                break;
            case "4":
                $(".performance"+num).val("Bad");
                break;
            case "5":
                $(".performance"+num).val("Very Bad");
                break;
  		}
  	});
  	
  	//Obtiene la fecha de nacimiento
	$("#fecha_nacimiento").change(function(){
		var fecha = $(this).val();2
		var today = new Date();
	    var birthDate = new Date(fecha);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age = age - 1;
	    }
	    $("#edad").val(age);
	});
  	//Acepta solo numeros en los input
  	$(".solo_numeros").on("input", function(){
	    var valor = $(this).val();
	    $(this).val(valor.replace(/[^0-9]/g, ''));
	});
	//Limpiar inputs
	$(".registro_obligado, .personal_obligado, .check_obligado, .familia_obligado, .global_obligado, dom_obligado").focus(function(){
		$(this).removeClass("requerido");
	});
	//Se cambia el idioma al español;
	$.fn.datetimepicker.dates['en'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
	    meridiem: '',
	    today: "Hoy"
	};

  	//Se crea la variable para establecer la fecha actual
  	var hoy = new Date();
  	var dd = hoy.getDate();
  	var mm = hoy.getMonth()+1;
  	var yyyy = hoy.getFullYear();
  	var hora = hoy.getHours()+":"+hoy.getMinutes();

  	if(dd<10) {
      	dd='0'+dd;
  	} 

  	if(mm<10) {
      	mm='0'+mm;
  	}
</script>