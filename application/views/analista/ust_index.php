<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Cliente: <small>UST GLOBAL</small></h1><br>
		<a href="#" class="btn btn-primary btn-icon-split" id="btn_nuevo" data-toggle="modal" data-target="#newModal">
			<span class="icon text-white-50">
				<i class="fas fa-plus-circle"></i>
			</span>
			<span class="text">Registrar candidato</span>
		</a>
		<a href="#" class="btn btn-primary btn-icon-split hidden" id="btn_regresar" onclick="regresarListado()" style="display: none;">
			<span class="icon text-white-50">
				<i class="fas fa-arrow-left"></i>
			</span>
			<span class="text">Regresar al listado</span>
		</a>
	</div>

	<div id="div_tipo_proceso" class="row">
		<div class="col-md-4">
			<label>Selecciona el tipo de Proceso: </label>
			<select class="form-control" name="tipo_proceso" id="tipo_proceso">
				<option value="1">Proceso ESE</option>
				<option value="2">Proceso FACIS</option>
			</select><br>
		</div>
	</div>

	<div id="opcion1">
		<div class="card shadow mb-4" id="opcion1">
			<div class="card-header py-3">
				<h4 class="m-0 font-weight-bold text-primary text-center">Proceso ESE</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table id="tabla" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					</table>
				</div>
			</div>
		</div>
	</div>

	<div id="opcion2" class="hidden">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h4 class="m-0 font-weight-bold text-primary  text-center">Proceso FACIS</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table id="tabla2" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					</table>
				</div>
			</div>
		</div>
	</div>

	<?php echo $modals; ?>
	<div class="loader" style="display: none;"></div>
	<input type="hidden" id="idCandidato">
	<input type="hidden" id="idCliente">
	<input type="hidden" id="idAvances">
	<input type="hidden" id="correo">
	<input type="hidden" class="prefijo">


	<section class="content" id="formulario" style="display: none;">
		<div class="row">
			<div class="col-12">
				<div class="text-center">
					<h4 class="text-primary"><strong> Referencias Laborales </strong></h4><br><br>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="alert alert-warning">
					<h4 class="text-center">Has the candidate worked in any government entity, political party or NGO?</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<textarea class="form-control trabajo_gobierno" name="trabajo_gobierno" id="trabajo_gobierno" rows="2"></textarea><br><br>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="alert alert-warning">
					<h4 class="text-center">Break(s) in Employment</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<textarea class="form-control trabajo_gobierno" name="trabajo_inactivo" id="trabajo_inactivo" rows="5"></textarea><br><br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 offset-md-3">
				<button type="button" class="btn btn-success" onclick="actualizarTrabajoGobierno()">Guardar respuestas anteriores</button>
				<br><br>
			</div>
			<div class="col-md-3">
				<button type="button" class="btn btn-secondary" onclick="regresarListado()">Regresar al listado</button>
				<br><br><br><br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="trabajos_msj_error" class="alert alert-danger hidden"></div>
			</div>
		</div>
		<?php
		for ($i = 1; $i <= 2; $i++) {
			echo '<div class="text-center">
							<h4 class="box-title " id="titulo_reflab' . $i . '"><strong> Trabajo #' . $i . ' </strong><hr></h4>
						</div>';
			echo '<div class="row">
							<div class="col-6">
								<form id="candidato_reflaboral' . $i . '">
									<div class="alert alert-info"><h4 class="text-center">Candidato</h4></div>
									<div class="row">
										<div class="col-md-3">
											<label>Compañía: </label>
											<br>
										</div>
										<div class="col-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_empresa_ingles" id="reflab' . $i . '_empresa_ingles" >
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Dirección: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_direccion_ingles" id="reflab' . $i . '_direccion_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Fecha de entrada: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control fecha_laboral reflabingles' . $i . '_obligado" name="reflab' . $i . '_entrada_ingles" id="reflab' . $i . '_entrada_ingles" placeholder="mm/dd/yyyy">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Fecha de salida: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control fecha_laboral reflabingles' . $i . '_obligado" name="reflab' . $i . '_salida_ingles" id="reflab' . $i . '_salida_ingles" placeholder="mm/dd/yyyy">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Teléfono: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control solo_numeros reflabingles' . $i . '_obligado" name="reflab' . $i . '_telefono_ingles" id="reflab' . $i . '_telefono_ingles" maxlength="10">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Puesto inicial: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_puesto1_ingles" id="reflab' . $i . '_puesto1_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Puesto final: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_puesto2_ingles" id="reflab' . $i . '_puesto2_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Salario inicial: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control solo_numeros reflabingles' . $i . '_obligado" name="reflab' . $i . '_salario1_ingles" id="reflab' . $i . '_salario1_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Salario final: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control solo_numeros reflabingles' . $i . '_obligado" name="reflab' . $i . '_salario2_ingles" id="reflab' . $i . '_salario2_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Jefe inmediato: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_jefenombre_ingles" id="reflab' . $i . '_jefenombre_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Correo del jefe inmediato: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_jefecorreo_ingles" id="reflab' . $i . '_jefecorreo_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Puesto del jefe inmediato:</label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_jefepuesto_ingles" id="reflab' . $i . '_jefepuesto_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<label>Causa de separación: </label>
											<br>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control reflabingles' . $i . '_obligado" name="reflab' . $i . '_separacion_ingles" id="reflab' . $i . '_separacion_ingles">
									<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4 offset-md-4">
											<button type="button" class="btn btn-success" onclick="actualizarReferenciaLaboral(' . $i . ')">Guardar referencia laboral</button><br><br><br><br>
											<input type="hidden" id="idreflabingles' . $i . '">
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div id="reflab_msj_error' . $i . '" class="alert alert-danger hidden"></div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-6">
  							<form id="analista_reflaboral' . $i . '">
								<div class="alert alert-warning">
									<h4 class="text-center">Analista</h4>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Compañía: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_empresa" id="an_reflab' . $i . '_empresa">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Dirección: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_direccion" id="an_reflab' . $i . '_direccion">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Fecha de entrada: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control fecha_laboral ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_entrada" id="an_reflab' . $i . '_entrada" placeholder="mm/dd/yyyy">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Fecha de salida: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control fecha_laboral ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_salida" id="an_reflab' . $i . '_salida" placeholder="mm/dd/yyyy">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Teléfono: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control solo_numeros ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_telefono" id="an_reflab' . $i . '_telefono" maxlength="10">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Puesto inicial: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_puesto1" id="an_reflab' . $i . '_puesto1">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Puesto final: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_puesto2" id="an_reflab' . $i . '_puesto2">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Salario inicial: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control solo_numeros ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_salario1" id="an_reflab' . $i . '_salario1">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Salario final: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control solo_numeros ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_salario2" id="an_reflab' . $i . '_salario2">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Jefe inmediato: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_jefenombre" id="an_reflab' . $i . '_jefenombre">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Correo del jefe inmediato: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_jefecorreo" id="an_reflab' . $i . '_jefecorreo">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Puesto del jefe inmediato: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_jefepuesto" id="an_reflab' . $i . '_jefepuesto">
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<label>Causa de separación: </label>
										<br>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_separacion" id="an_reflab' . $i . '_separacion">
										<br>
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<label>Notas *</label>
								<textarea class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_notas" id="an_reflab' . $i . '_notas" rows="3"></textarea><br>
							</div>
							<div class="col-md-3">
								<label>¿El candidato demandó a la empresa? *</label>
								<select name="an_reflab' . $i . '_demanda" id="an_reflab' . $i . '_demanda" class="form-control ver_reflab' . $i . '_obligado">
									<option value="">Select</option>
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
								<br>
							</div>
							<div class="col-md-6 offset-md-4">
								<label>Aplicar a todos</label>
								<select id="aplicar_todo' . $i . '" class="form-control">
									<option value="-1">Select</option>
									<option value="0">Not provided</option>
									<option value="1">Excellent</option>
									<option value="2">Good</option>
									<option value="3">Regular</option>
									<option value="4">Bad</option>
									<option value="5">Very Bad</option>
								</select>
								<br><br>
							</div>
							<div class="col-md-3">
								<label>Responsabilidad *</label>
								<select name="an_reflab' . $i . '_responsabilidad" id="an_reflab' . $i . '_responsabilidad" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Iniciativa *</label>
								<select name="an_reflab' . $i . '_iniciativa" id="an_reflab' . $i . '_iniciativa" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Eficiencia laboral *</label>
								<select name="an_reflab' . $i . '_eficiencia" id="an_reflab' . $i . '_eficiencia" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Disciplina *</label>
								<select name="an_reflab' . $i . '_disciplina" id="an_reflab' . $i . '_disciplina" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Puntualidad y asistencia *</label>
								<select name="an_reflab' . $i . '_puntualidad" id="an_reflab' . $i . '_puntualidad" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Limpieza y orden *</label>
								<select name="an_reflab' . $i . '_limpieza" id="an_reflab' . $i . '_limpieza" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Estabilidad laboral *</label>
								<select name="an_reflab' . $i . '_estabilidad" id="an_reflab' . $i . '_estabilidad" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Estabilidad emocional *</label>
								<select name="an_reflab' . $i . '_emocional" id="an_reflab' . $i . '_emocional" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Honestidad *</label>
								<select name="an_reflab' . $i . '_honesto" id="an_reflab' . $i . '_honesto" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-3">
								<label>Rendimiento *</label>
								<select name="an_reflab' . $i . '_rendimiento" id="an_reflab' . $i . '_rendimiento" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-4">
								<label>Actitud con colaboradores, jefes y subordinados *</label>
								<select name="an_reflab' . $i . '_actitud" id="an_reflab' . $i . '_actitud" class="form-control performance' . $i . ' ver_reflab' . $i . '_obligado">
									<option value="Not provided">Not provided</option>
									<option value="Excellent">Excellent</option>
									<option value="Good">Good</option>
									<option value="Regular">Regular</option>
									<option value="Bad">Bad</option>
									<option value="Very Bad">Very Bad</option>
								</select>
								<br>
							</div>
							<div class="col-md-4">
								<label>¿Lo(a) contrataría de nuevo? *</label>
								<select name="an_reflab' . $i . '_recontratacion" id="an_reflab' . $i . '_recontratacion" class="form-control ver_reflab' . $i . '_obligado">
									<option value="">Select</option>
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
								<br><br><br>
							</div>
							<div class="col-md-8">
								<label>¿Por qué? *</label>
								<textarea class="form-control ver_reflab' . $i . '_obligado" name="an_reflab' . $i . '_motivo" id="an_reflab' . $i . '_motivo" rows="3"></textarea>
								<br><br><br>
							</div>
							</form>
							<div class="col-md-3 offset-md-3">
								<button type="button" class="btn btn-success" onclick="verificarLaboral(' . $i . ')">Guardar la verificación del trabajo #' . $i . '</button>
								<br><br><input type="hidden" id="idverlab' . $i . '">
							</div>
							<div class="col-md-3">
								<button type="button" class="btn btn-secondary" onclick="regresarListado()">Regresar al listado</button>
								<br><br><br><br>
							</div>
							<div class="col-12">
								<div id="verlab_msj_error' . $i . '" class="alert alert-danger hidden"></div>
							</div>
						</div>';
			}
		?>
	</section>

</div>
<!-- /.content-wrapper -->
<script>
	var url = '<?php echo base_url('Cliente_Ust/geCandidatosESE'); ?>';
	var url2 = '<?php echo base_url('Cliente_Ust/getCandidatosFACIS'); ?>';
	$(document).ready(function() {
		$('#fecha_nacimiento, #fecha_nacimiento_nuevo, .fecha_laboral, #fecha_facis').inputmask('mm/dd/yyyy', {
			'placeholder': 'mm/dd/yyyy'
		});
		$("#tipo_proceso").change(function() {
			var opcion = $(this).val();
			if (opcion == 1) {
				$("#opcion2").css('display', 'none');
				$("#opcion1").css('display', 'block');
			} else {
				$("#opcion1").css('display', 'none');
				$("#opcion2").css('display', 'block');
			}
		});
		var msj = localStorage.getItem("success");
		var msj2 = localStorage.getItem("finished");
		if (msj == 1) {
			$("#exitoCandidato").css('display', 'block');
			setTimeout(function() {
				$('#exitoCandidato').fadeOut();
			}, 4000);
			localStorage.removeItem("success");
		}
		if (msj2 == 1) {
			$("#exitoFinalizado").css('display', 'block');
			setTimeout(function() {
				$('#exitoFinalizado').fadeOut();
			}, 4000);
			localStorage.removeItem("finished");
		}
		$('#tabla').DataTable({
			"pageLength": 25,
			//"pagingType": "simple",
			"order": [0, "desc"],
			"stateSave": true,
			"serverSide": false,
			"ajax": url,
			"columns": [{
					title: 'id',
					data: 'id',
					visible: false
				},
				{
					title: 'Candidato',
					data: 'candidato',
					"width": "15%",
					mRender: function(data, type, full) {
						return '<a data-toggle="tooltip" class="sin_vinculo" title="' + full.id + '">' + data + '</a>';
					}
				},
				{
					title: 'Fechas',
					data: 'fecha_alta',
					"width": "15%",
					mRender: function(data, type, full) {
						var f = data.split(' ');
						var h = f[1];
						var aux = h.split(':');
						var hora = aux[0] + ':' + aux[1];
						var aux = f[0].split('-');
						var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
						var f_inicio = fecha + ' ' + hora;
						if (full.fecha_final != null) {
							var f = full.fecha_final.split(' ');
							var h = f[1];
							var aux = h.split(':');
							var hora = aux[0] + ':' + aux[1];
							var aux = f[0].split('-');
							var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
							var f_fin = "<b>Final:</b> " + fecha + ' ' + hora;
							return "<b>Alta:</b> " + f_inicio + '<br>' + f_fin;
						} else {
							var f = new Date();
							var hoy = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
							return "<b>Alta:</b> " + f_inicio + '<br><b>Final:</b> -';
						}
					}
				},
				{
					title: 'Tiempo',
					data: 'tiempo',
					"width": "8%",
					mRender: function(data, type, full) {
						if(full.cancelado == 0){
							if (data != null) {
								if (data != -1) {
									if (data >= 0 && data <= 2) {
										return res = '<div class="formato_dias dias_verde">' + data + ' días</div>';
									}
									if (data > 2 && data <= 4) {
										return res = '<div class="formato_dias dias_amarillo">' + data + ' días</div>';
									}
									if (data >= 5) {
										return res = '<div class="formato_dias dias_rojo">' + data + ' días</div>';
									}
								} else {
									return "Actualizando...";
								}
							} 
							else {
								if (full.tiempo_parcial != 0) {
									var parcial = full.tiempo_parcial;
									if (parcial >= 0 && parcial < 2) {
										return res = '<div class="formato_dias dias_verde">' + parcial + ' días</div>';
									}
									if (parcial >= 2 && parcial <= 4) {
										return res = '<div class="formato_dias dias_amarillo">' + parcial + ' días</div>';
									}
									if (parcial >= 5) {
										return res = '<div class="formato_dias dias_rojo">' + parcial + ' días</div>';
									}
								} else {
									return "Actualizando...";
								}
							}
						}
						else{
							return 'N/A';
						}
					}
				},
				{
					title: 'Formulario',
					data: 'fecha_contestado',
					"width": "12%",
					mRender: function(data, type, full) {
						if(full.cancelado == 0){
							if (data == "" || data == null) {
								return "<i class='fas fa-circle estatus0'></i>Sin contestar";
							} 
							else {
								var f = data.split(' ');
								var h = f[1];
								var aux = h.split(':');
								var hora = aux[0] + ':' + aux[1];
								var aux = f[0].split('-');
								var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
								var tiempo = fecha + ' ' + hora;
								return "<i class='fas fa-circle estatus1'></i>" + tiempo;
							}
						}
						else{
							return 'N/A';
						}
					}
				},
				{
					title: 'Documentos',
					data: 'fecha_documentos',
					"width": "12%",
					mRender: function(data, type, full) {
						if(full.cancelado == 0){
							if (data == "" || data == null) {
								return "<i class='fas fa-circle estatus0'></i>Sin documentos";
							} 
							else {
								var f = data.split(' ');
								var h = f[1];
								var aux = h.split(':');
								var hora = aux[0] + ':' + aux[1];
								var aux = f[0].split('-');
								var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
								var tiempo = fecha + ' ' + hora;
								return "<i class='fas fa-circle estatus1'></i>" + tiempo;
							}
						}
						else{
							return 'N/A';
						}
					}
				},
				{
					title: 'Mensajes',
					data: 'id',
					bSortable: false,
					"width": "8%",
					mRender: function(data, type, full) {
						if(full.cancelado == 0){
							if (full.status == 0) {
								return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Generar password" id="generar" class="fa-tooltip icono_datatable"><i class="fas fa-key"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" id="msj_avances" class="fa-tooltip icono_datatable"><i class="fas fa-comment-dots"></i></a>';
							}
							if (full.status > 0) {
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" id="msj_avances" class="fa-tooltip icono_datatable"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Ver comentario del candidato" id="comentario" class="fa-tooltip icono_datatable"><i class="fas fa-comment"></i></a>';
							}
						}
						else{
							return 'N/A';
						}
					}
				},
				{
					title: 'Estudio',
					data: 'id',
					bSortable: false,
					"width": "15%",
					mRender: function(data, type, full) {
						if(full.cancelado == 0){
							if (full.status == 0) {
								return '<div class="btn-group" style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:void(0)" id="datos_generales">Datos generales</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="eliminar">Cancelar proceso</a></div></div>';
							}
							if (full.status == 1) {
								return '<div class="btn-group"  style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:void(0)" id="datos_generales">Datos generales</a><a class="dropdown-item" href="javascript:void(0)" id="datos_academicos">Historial académico</a><a class="dropdown-item" href="javascript:void(0)" id="datos_verificacion_docs">Verificación documentos</a><a class="dropdown-item" href="javascript:void(0)" id="datos_familiares">Información familiar</a><a class="dropdown-item" href="javascript:void(0)" id="datos_ref_personales">Referencias personales</a><a class="dropdown-item" href="javascript:void(0)" id="datos_laborales">Referencias laborales</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="estudios">Verificación de estudios</a><a class="dropdown-item" href="javascript:void(0)" id="laborales">Verificación de referencias laborales</a><a class="dropdown-item" href="javascript:void(0)" id="penales">Verificación penal</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="subirDocs">Documentación</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="eliminar">Cancelar proceso</a></div></div>';
							}
							if (full.status == 2) {
								return '<div class="btn-group"  style="margin-bottom: 10px;"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proceso del candidato</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:void(0)" id="datos_generales">Datos generales</a><a class="dropdown-item" href="javascript:void(0)" id="datos_academicos">Historial académico</a><a class="dropdown-item" href="javascript:void(0)" id="datos_verificacion_docs">Verificación documentos</a><a class="dropdown-item" href="javascript:void(0)" id="datos_familiares">Información familiar</a><a class="dropdown-item" href="javascript:void(0)" id="datos_ref_personales">Referencias personales</a><a class="dropdown-item" href="javascript:void(0)" id="datos_laborales">Referencias laborales</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="estudios">Verificación de estudios</a><a class="dropdown-item" href="javascript:void(0)" id="laborales">Verificación de referencias laborales</a><a class="dropdown-item" href="javascript:void(0)" id="penales">Verificación penal</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="subirDocs">Documentación</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="final_editar">Conclusiones</a><div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:void(0)" id="eliminar">Cancelar proceso</a></div></div>';
							}
						}
						else{
							return 'Cancelado';
						}
					}
				},
				{
					title: 'Proceso',
					data: 'id',
					bSortable: false,
					"width": "8%",
					mRender: function(data, type, full) {
						if(full.cancelado == 0){
							if (full.status == 0) {
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="En espera" class="fa-tooltip"><i class="fas fa-circle status_bgc0"></i></a>';
							} 
							else {
								if (full.status_bgc == 0) {
									return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip"><i class="fas fa-user-check icono_datatable"></i></a>';
								}
								if (full.status_bgc == 1) {
									return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Cliente_Ust/crearPDFProcesoESE'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
								if (full.status_bgc == 2) {
									return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Cliente_Ust/crearPDFProcesoESE'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
								if (full.status_bgc == 3) {
									return '<i class="fas fa-circle status_bgc3"></i> <div style="display: inline-block;"><form id="pdf' + data + '" action="<?php echo base_url('Cliente_Ust/crearPDFProcesoESE'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF' + data + '" value="' + data + '"></form></div>';
								}
							}
						}
						else{
							return 'N/A';
						}
					}
				}
			],
			fnDrawCallback: function(oSettings) {
				$('a[data-toggle="tooltip"]').tooltip({
					trigger: "hover"
				});
			},
			rowCallback: function(row, data) {
				$('a#datos_generales', row).bind('click', () => {
					$("#d_generales")[0].reset();
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#nombre_general").val(data.nombre);
					$("#paterno_general").val(data.paterno);
					$("#materno_general").val(data.materno);
					if (data.fecha_nacimiento != "0000-00-00" && data.fecha_nacimiento != null) {
						var f_nacimiento = convertirDate(data.fecha_nacimiento);
						$("#fecha_nacimiento").val(f_nacimiento);
					} else {
						$("#fecha_nacimiento").val("");
					}
					$("#puesto_general").val(data.puesto);
					$("#nacionalidad").val(data.nacionalidad);
					$("#genero").val(data.genero);
					$("#calle").val(data.calle);
					$("#exterior").val(data.exterior);
					$("#interior").val(data.interior);
					$("#colonia").val(data.colonia);
					$("#estado").val(data.id_estado);
					if (data.id_estado != "" && data.id_estado != null && data.id_estado != 0) {
						getMunicipio(data.id_estado, data.id_municipio);
					} else {
						$('#municipio').prop('disabled', true);
					}
					$("#cp").val(data.cp);
					$("#civil").val(data.id_estado_civil);
					$("#celular_general").val(data.celular);
					$("#tel_casa").val(data.telefono_casa);
					$("#tel_oficina").val(data.telefono_otro);
					$("#personales_correo").val(data.correo);
					$("#generalesModal").modal('show');
				});
				$('a#datos_academicos', row).bind('click', () => {
					$("#d_estudios")[0].reset();
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#prim_periodo").val(data.primaria_periodo);
					$("#prim_escuela").val(data.primaria_escuela);
					$("#prim_ciudad").val(data.primaria_ciudad);
					$("#prim_certificado").val(data.primaria_certificado);
					$("#prim_validado").val(data.primaria_validada);
					$("#sec_periodo").val(data.secundaria_periodo);
					$("#sec_escuela").val(data.secundaria_escuela);
					$("#sec_ciudad").val(data.secundaria_ciudad);
					$("#sec_certificado").val(data.secundaria_certificado);
					$("#sec_validado").val(data.secundaria_validada);
					$("#prep_periodo").val(data.preparatoria_periodo);
					$("#prep_escuela").val(data.preparatoria_escuela);
					$("#prep_ciudad").val(data.preparatoria_ciudad);
					$("#prep_certificado").val(data.preparatoria_certificado);
					$("#prep_validado").val(data.preparatoria_validada);
					$("#lic_periodo").val(data.licenciatura_periodo);
					$("#lic_escuela").val(data.licenciatura_escuela);
					$("#lic_ciudad").val(data.licenciatura_ciudad);
					$("#lic_certificado").val(data.licenciatura_certificado);
					$("#lic_validado").val(data.licenciatura_validada);
					$("#otro_certificado").val(data.otros_certificados);
					$("#estudios_comentarios").val(data.comentarios);
					$("#carrera_inactivo").val(data.carrera_inactivo);
					$("#academicosModal").modal('show');
				});
				$('a#datos_verificacion_docs', row).bind('click', () => {
					$("#d_documentos")[0].reset();
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					var href = '<?php echo base_url() . "_docs/"; ?>';
					$.ajax({
						url: '<?php echo base_url('Candidato/checkDocumentos'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != 0) {
								$("#doc_estudios").empty();
								$("#doc_ine").empty();
								$("#doc_penales").empty();
								var salida = res.split('@@');
								for (var i = 0; i < salida.length; i++) {
									var aux = salida[i].split(',');
									if (aux[0] == 7 || aux[0] == 10) {
										$("#doc_estudios").append("<a href='" + href + aux[1] + "' target='_blank'>" + aux[1] + "</a><br>");
									}
									if (aux[0] == 3 || aux[0] == 14 || aux[0] == 15) {
										$("#doc_ine").append("<a href='" + href + aux[1] + "' target='_blank'>" + aux[1] + "</a><br>");
									}
									if (aux[0] == 12) {
										$("#doc_penales").append("<a href='" + href + aux[1] + "' target='_blank'>" + aux[1] + "</a><br>");
									}
								}
							}
						}
					});
					$.ajax({
						url: '<?php echo base_url('Cliente_Ust/checkVerificacionDocumentos'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != 0) {
								var data = res.split('@@');
								$("#lic_profesional").val(data[0]);
								$("#lic_institucion").val(data[1]);
								$("#ine_clave").val(data[2]);
								$("#ine_registro").val(data[3]);
								$("#ine_vertical").val(data[4]);
								$("#ine_institucion").val(data[5]);
								$("#penales_numero").val(data[6]);
								$("#penales_institucion").val(data[7]);
								$("#doc_comentarios").val(data[8]);
							}
						}
					});
					$("#documentosModal").modal('show');
				});
				$('a#datos_familiares', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_familiares")[0].reset();
					var cont = 0;
					$.ajax({
						url: '<?php echo base_url('Cliente_Ust/getFamiliares'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != 0) {
								$('#div_familiares').empty();
								var rows = res.split('###');
								for (var i = 0; i < rows.length; i++) {
									var dato = rows[i].split('@@');
									if (rows[i] != "" && dato[1] != 'undefined') {
										var parentesco = '<div class="row"><div class="col-md-4"><label>Relationship *</label><select name="parentesco' + i + '" id="parentesco' + i + '" class="form-control familia_obligado"><option value="">Selecciona</option><option value="1">Padre</option><option value="2">Madre</option><option value="3">Hijo(a)</option><option value="4">Cónyuge</option><option value="6">Hermano(a)</option></select><br></div>';

										var nombre = '<div class="col-md-4"><label>Nombre</label><input type="text" class="form-control familia_obligado" name="nombre_familiar' + i + '" id="nombre_familiar' + i + '" value="' + dato[1] + '"><br></div>';

										var edad = '<div class="col-md-4"><label>Edad</label><input type="text" class="form-control solo_numeros familia_obligado" name="edad_familiar' + i + '" id="edad_familiar' + i + '" value="' + dato[2] + '" max_lenght="2"><br></div></div>';

										var ocupacion = '<div class="row"><div class="col-md-4"><label>Puesto/Ocupación</label><input type="text" class="form-control familia_obligado" name="puesto_familiar' + i + '" id="puesto_familiar' + i + '" value="' + dato[3] + '"><br></div>';

										var ciudad = '<div class="col-md-4"><label>Ciudad</label><input type="text" class="form-control familia_obligado" name="ciudad_familiar' + i + '" id="ciudad_familiar' + i + '" value="' + dato[4] + '"><br></div>';

										var empresa = '<div class="col-md-4"><label>Empresa</label><input type="text" class="form-control familia_obligado" name="empresa_familiar' + i + '" id="empresa_familiar' + i + '" value="' + dato[5] + '"><br></div></div>';

										var vivienda = '<div class="row"><div class="col-md-4"><label>¿Vive con ella/él? *</label><select name="misma_vivienda' + i + '" id="misma_vivienda' + i + '" class="form-control familia_obligado"><option value="">Selecciona</option><option value="1">Sí</option><option value="0">No</option></select><br></div></div>';

										var boton = '<div class="row"><div class="col-md-4 offset-md-5"><button type="button" class="btn btn-success" onclick="actualizarFamiliar(' + i + ',' + dato[7] + ')">Guardar</button><br><br></div></div>';

										var msj = '<div id="msj_error' + i + '" class="alert alert-danger hidden"></div>';

										$('#div_familiares').append(parentesco + nombre + edad + ocupacion + ciudad + empresa + vivienda + boton + msj);
										$('#parentesco' + i).val(dato[0]);
										$('#misma_vivienda' + i).val(dato[6]);
										cont++;
										$('#numFamiliares').val(cont);
									}
								}
							}
						}
					});
					$('#familiaresModal').modal('show');
				});
				$('a#datos_ref_personales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#d_refpersonal1")[0].reset();
					$("#d_refpersonal2")[0].reset();
					$("#d_refpersonal3")[0].reset();
					$.ajax({
						url: '<?php echo base_url('Cliente_Ust/getReferenciasPersonales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != 0) {
								var rows = res.split('###');
								for (var i = 0; i < rows.length; i++) {
									if (rows[i] != '') {
										var dato = rows[i].split('@@');
										$("#refper" + (i + 1) + "_nombre").val(dato[0]);
										$("#refper" + (i + 1) + "_telefono").val(dato[1]);
										$("#refper" + (i + 1) + "_tiempo").val(dato[2]);
										$("#refper" + (i + 1) + "_lugar").val(dato[3]);
										$("#refper" + (i + 1) + "_trabaja").val(dato[4]);
										$("#refper" + (i + 1) + "_vive").val(dato[5]);
										$("#refper" + (i + 1) + "_id").val(dato[6]);
										$("#refper" + (i + 1) + "_comentario").val(dato[7]);
									}
								}
							}
						}
					});
					$("#refPersonalesModal").modal('show');
				});
				$('a#datos_laborales', row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#candidato_reflaboral1")[0].reset();
					$("#candidato_reflaboral2")[0].reset();
					$("#analista_reflaboral1")[0].reset();
					$("#analista_reflaboral2")[0].reset();
					$("#trabajo_gobierno").val(data.trabajo_gobierno);
					$("#trabajo_inactivo").val(data.trabajo_inactivo);
					$.ajax({
						async: false,
						url: '<?php echo base_url('Cliente_Ust/getReferenciasLaborales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							var rows = res.split('###');
							for (var i = 0; i < rows.length; i++) {
								if (rows[i] != "") {
									var dato = rows[i].split('@@');
									var entrada = convertirDateTime(dato[2]);
									var salida = convertirDateTime(dato[3]);
									$("#reflab" + (i + 1) + "_empresa_ingles").val(dato[0]);
									$("#reflab" + (i + 1) + "_direccion_ingles").val(dato[1]);
									$("#reflab" + (i + 1) + "_entrada_ingles").val(entrada);
									$("#reflab" + (i + 1) + "_salida_ingles").val(salida);
									$("#reflab" + (i + 1) + "_telefono_ingles").val(dato[4]);
									$("#reflab" + (i + 1) + "_puesto1_ingles").val(dato[5]);
									$("#reflab" + (i + 1) + "_puesto2_ingles").val(dato[6]);
									$("#reflab" + (i + 1) + "_salario1_ingles").val(dato[7]);
									$("#reflab" + (i + 1) + "_salario2_ingles").val(dato[8]);
									$("#reflab" + (i + 1) + "_jefenombre_ingles").val(dato[9]);
									$("#reflab" + (i + 1) + "_jefecorreo_ingles").val(dato[10]);
									$("#reflab" + (i + 1) + "_jefepuesto_ingles").val(dato[11]);
									$("#reflab" + (i + 1) + "_separacion_ingles").val(dato[12]);
									$("#idreflabingles" + (i + 1)).val(dato[13]);
								}
							}
						}
					});
					$.ajax({
						async: false,
						url: '<?php echo base_url('Cliente_Ust/getVerificacionesLaborales'); ?>',
						method: 'POST',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != "") {
								var rows = res.split('###');
								for (var i = 0; i < rows.length; i++) {
									if (rows[i] != "") {
										var dato = rows[i].split('@@');
										var entrada = convertirDateTime(dato[2]);
										var salida = convertirDateTime(dato[3]);
										$("#an_reflab" + dato[29] + "_empresa").val(dato[0]);
										$("#an_reflab" + dato[29] + "_direccion").val(dato[1]);
										$("#an_reflab" + dato[29] + "_entrada").val(entrada);
										$("#an_reflab" + dato[29] + "_salida").val(salida);
										$("#an_reflab" + dato[29] + "_telefono").val(dato[4]);
										$("#an_reflab" + dato[29] + "_puesto1").val(dato[5]);
										$("#an_reflab" + dato[29] + "_puesto2").val(dato[6]);
										$("#an_reflab" + dato[29] + "_salario1").val(dato[7]);
										$("#an_reflab" + dato[29] + "_salario2").val(dato[8]);
										$("#an_reflab" + dato[29] + "_jefenombre").val(dato[9]);
										$("#an_reflab" + dato[29] + "_jefecorreo").val(dato[10]);
										$("#an_reflab" + dato[29] + "_jefepuesto").val(dato[11]);
										$("#an_reflab" + dato[29] + "_separacion").val(dato[12]);
										$("#an_reflab" + dato[29] + "_notas").val(dato[13]);
										$("#an_reflab" + dato[29] + "_demanda").val(dato[14]);
										$("#an_reflab" + dato[29] + "_responsabilidad").val(dato[15]);
										$("#an_reflab" + dato[29] + "_iniciativa").val(dato[16]);
										$("#an_reflab" + dato[29] + "_eficiencia").val(dato[17]);
										$("#an_reflab" + dato[29] + "_disciplina").val(dato[18]);
										$("#an_reflab" + dato[29] + "_puntualidad").val(dato[19]);
										$("#an_reflab" + dato[29] + "_limpieza").val(dato[20]);
										$("#an_reflab" + dato[29] + "_estabilidad").val(dato[21]);
										$("#an_reflab" + dato[29] + "_emocional").val(dato[22]);
										$("#an_reflab" + dato[29] + "_honesto").val(dato[23]);
										$("#an_reflab" + dato[29] + "_rendimiento").val(dato[24]);
										$("#an_reflab" + dato[29] + "_actitud").val(dato[25]);
										$("#an_reflab" + dato[29] + "_recontratacion").val(dato[26]);
										$("#an_reflab" + dato[29] + "_motivo").val(dato[27]);
										$("#idverlab" + dato[29]).val(dato[28]);
									}
								}
							}

						}
					});
					$("#div_tipo_proceso").css('display', 'none')
					$("#opcion1").css('display', 'none');
					$("#opcion2").css('display', 'none');
					$("#btn_nuevo").css('display', 'none');
					$("#formulario").css('display', 'block');
					$("#btn_regresar").css('display', 'block');
				});
				$("a#subirDocs", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$(".prefijo").val(data.id + "_" + data.nombre + "" + data.paterno);
					$.ajax({
						url: '<?php echo base_url('Candidato/getDocumentos'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id,
							'prefijo': data.id + "_" + data.nombre + "" + data.paterno
						},
						success: function(res) {
							$("#tablaDocs").html(res);

						}
					});
					$("#docsModal").modal("show");
				});
				$("a#estudios", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#estudios_nombrecandidato").text(data.nombre + " " + data.paterno + " " + data.materno);
					verificacionEstudios();
				});
				$("a#laborales", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#laborales_nombrecandidato").text(data.nombre + " " + data.paterno + " " + data.materno);
					verificacionLaborales();
				});
				$("a#penales", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#penales_nombrecandidato").text(data.nombre + " " + data.paterno + " " + data.materno);
					verificacionPenales();
				});
				$("a#eliminar", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$("#quitarModal #titulo_accion").text("Cancelar proceso");
					$("#quitarModal #texto_confirmacion").html("¿Estás seguro de cancelar el proceso de <b>"+data.candidato+"</b>?");
					$("#quitarModal #btnGuardar").attr('value', 'delete');
					$("#quitarModal").modal("show");
				});
				$("a#msj_avances", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$("#avances_nombrecandidato").text(data.nombre + " " + data.paterno + " " + data.materno);
					estatusAvances();
				});
				$("a#final", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#revisionModal").modal('show');
				});
				$("a#final_editar", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$('#check_identidad').val(data.identidad_check);
					$('#check_laboral').val(data.empleo_check);
					$('#check_estudios').val(data.estudios_check);
					$('#check_visita').val(data.visita_check);
					$('#check_penales').val(data.penales_check);
					$('#check_ofac').val(data.ofac_check);
					$('#check_laboratorio').val(data.laboratorio_check);
					$('#check_medico').val(data.medico_check);
					$('#comentario_final').val(data.comentario_final);
					$('#bgc_status').val(data.status_bgc);
					$("#completarModal").modal('show');
				});
				$('a[id^=pdfFinal]', row).bind('click', () => {
					var id = data.id;
					$('#pdf' + id).submit();
				});
				$("a#generar", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$("#correo").val(data.correo);
					$("#quitarModal #titulo_accion").text("Generar password");
					$("#quitarModal #texto_confirmacion").html("Estás seguro de generar un nuevo password para <b>" + data.nombre + " " + data.paterno + " " + data.materno + "</b>?");
					$("#quitarModal #btnGuardar").attr('value', 'generar');
					$("#quitarModal").modal("show");
				});
				$('a#comentario', row).bind('click', () => {
					$.ajax({
						url: '<?php echo base_url('Candidato/verComentarioCandidato'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id
						},
						success: function(res) {
							if (res != 0) {
								$("#titulo_accion").text("Comentario del candidato");
								$("#motivo").html(res);
								$("#verModal").modal('show');
							} else {
								$("#titulo_accion").text("Comentarios del candidato");
								$("#motivo").html("No hay registro de comentarios");
								$("#verModal").modal('show');
							}
						}
					});
				});
			},
			"language": {
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"zeroRecords": "No se encontraron registros",
				"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"infoEmpty": "Sin registros disponibles",
				"infoFiltered": "(Filtrado _MAX_ registros totales)",
				"sSearch": "Buscar:",
				"oPaginate": {
					"sLast": "Última página",
					"sFirst": "Primera",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				}
			}
		});
		$('#tabla2').DataTable({
			"pageLength": 25,
			//"pagingType": "simple",
			"order": [0, "desc"],
			"stateSave": true,
			"serverSide": false,
			"ajax": url2,
			"columns": [{
					title: 'id',
					data: 'id',
					visible: false
				},
				{
					title: 'Nombre del Candidato',
					data: 'candidato',
					"width": "35%",
					mRender: function(data, type, full) {
						return '<a data-toggle="tooltip" class="sin_vinculo" title="' + full.id + '">' + data + '</a>';
					}
				},
				{
					title: 'Fecha de alta',
					data: 'fecha_alta',
					"width": "5%",
					mRender: function(data, type, full) {
						var f = data.split(' ');
						var h = f[1];
						var aux = h.split(':');
						var hora = aux[0] + ':' + aux[1];
						var aux = f[0].split('-');
						var fecha = aux[2] + "/" + aux[1] + "/" + aux[0];
						var tiempo = fecha + ' ' + hora;
						return tiempo;
					}
				},
				{
					title: 'Acciones',
					data: 'id',
					bSortable: false,
					"width": "6%",
					mRender: function(data, type, full) {
						return '<a id="facis" href="javascript:void(0)" data-toggle="tooltip" title="Verificación FACIS" class="fa-tooltip icono_datatable"><i class="fas fa-list-alt"></i></a><a id="subirDocs" href="javascript:void(0)" data-toggle="tooltip" title="Documentacion" class="fa-tooltip icono_datatable"><i class="fas fa-folder"></i></a><a id="fecha" href="javascript:void(0)" data-toggle="tooltip" title="Cambiar fechas" class="fa-tooltip icono_datatable"><i class="far fa-clock"></i></a>';
					}
				},
				{
					title: 'Proceso',
					data: 'id',
					bSortable: false,
					"width": "10%",
					mRender: function(data, type, full) {
						if (full.status == 0) {
							if(full.ofac != null && full.oig != null && full.sam != null && full.data_juridica != null){
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar verificación del candidato" id="final_ofac" class="fa-tooltip icono_datatable"><i class="fas fa-user-check"></i></a>';
							}
							else{
								return '<a href="javascript:void(0)" data-toggle="tooltip" title="En espera de resultados" class="fa-tooltip"><i class="fas fa-circle status_bgc0"></i></a>';
							}
						} else {
							if (full.status_bgc == 1) {
								return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;"><form id="ofacpdf' + data + '" action="<?php echo base_url('Cliente_Ust/crearPDFProcesoFACIS'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfOfac" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf icono_datatable"></i></a><input type="hidden" name="idOfacPDF" id="idOfacPDF' + data + '" value="' + data + '"></form></div>';
							}
							if (full.status_bgc == 2) {
								return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;"><form id="ofacpdf' + data + '" action="<?php echo base_url('Cliente_Ust/crearPDFProcesoFACIS'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfOfac" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf icono_datatable"></i></a><input type="hidden" name="idOfacPDF" id="idOfacPDF' + data + '" value="' + data + '"></form></div>';
							}
							if (full.status_bgc == 3) {
								return '<i class="fas fa-circle status_bgc3"></i> <div style="display: inline-block;"><form id="ofacpdf' + data + '" action="<?php echo base_url('Cliente_Ust/crearPDFProcesoFACIS'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfOfac" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf icono_datatable"></i></a><input type="hidden" name="idOfacPDF" id="idOfacPDF' + data + '" value="' + data + '"></form></div>';
							}
						}
					}
				}
			],
			fnDrawCallback: function(oSettings) {
				$('a[data-toggle="tooltip"]').tooltip({
					trigger: "hover"
				});
			},
			rowCallback: function(row, data) {
				$("a#subirDocs", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$(".prefijo").val(data.id + "_" + data.nombre + "" + data.paterno);
					$.ajax({
						url: '<?php echo base_url('Candidato/getDocumentos'); ?>',
						type: 'post',
						data: {
							'id_candidato': data.id,
							'prefijo': data.id + "_" + data.nombre + "" + data.paterno
						},
						success: function(res) {
							$("#tablaDocs").html(res);

						}
					});
					$("#docsModal").modal("show");
				});
				$("#facis", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$("#idCliente").val(data.id_cliente);
					$(".nombreCandidato").text(data.candidato);
					estatusOFAC();
				});
				$("#fecha", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$('#fechaModal').modal('show');
				});
				$("a#final_ofac", row).bind('click', () => {
					$("#idCandidato").val(data.id);
					$(".nombreCandidato").text(data.candidato);
					$("#completarFacisModal").modal('show');
				});
				$('a[id^=pdfOfac]', row).bind('click', () => {
					var id = data.id;
					$('#ofacpdf' + id).submit();
				});
			},
			"language": {
				"lengthMenu": "Mostrar _MENU_ registros por página",
				"zeroRecords": "No se encontraron registros",
				"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"infoEmpty": "Sin registros disponibles",
				"infoFiltered": "(Filtrado _MAX_ registros totales)",
				"sSearch": "Buscar:",
				"oPaginate": {
					"sLast": "Última página",
					"sFirst": "Primera",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				}
			}
		});
		$("#estado").change(function() {
			var id_estado = $(this).val();
			if (id_estado != "") {
				$.ajax({
					url: '<?php echo base_url('Funciones/getMunicipios'); ?>',
					method: 'POST',
					data: {
						'id_estado': id_estado
					},
					dataType: "text",
					success: function(res) {
						$('#municipio').prop('disabled', false);
						$('#municipio').html(res);
					}
				});
			} else {
				$('#municipio').prop('disabled', true);
				$('#municipio').append($("<option selected></option>").attr("value", "").text("Select"));
			}
		});
		$("#ofacModal").on("hidden.bs.modal", function() {
			$(".ofac").removeClass("requerido");
			$("#campos_vacios_ofac").css('display', 'none');
		});
	});
	//Funciones Proceso # 1 ESE
	function registrarCandidato() {
		var datos = new FormData();
		var correo = $("#correo_nuevo").val();
		var proceso = $("#proceso_nuevo").val();
		datos.append('correo', $("#correo_nuevo").val());
		datos.append('nombre', $("#nombre_nuevo").val());
		datos.append('paterno', $("#paterno_nuevo").val());
		datos.append('materno', $("#materno_nuevo").val());
		datos.append('celular', $("#celular_nuevo").val());
		datos.append('fijo', $("#fijo_nuevo").val());
		datos.append('fecha_nacimiento', $("#fecha_nacimiento_nuevo").val());
		datos.append('proceso', proceso);
		datos.append('id_cliente', 1);
		if (proceso == 1) {
			$.ajax({
				url: '<?php echo base_url('Cliente_Ust/registrarCandidato'); ?>',
				type: 'POST',
				data: datos,
				contentType: false,
				cache: false,
				processData: false,
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 200);
					var data = JSON.parse(res);
					if (data.codigo === 0) {
						$("#newModal #msj_error").css('display', 'block').html(data.msg);
					}
					if (data.codigo === 1) {
						$("#newModal").modal('hide')
						recargarTable()
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: 'Se ha guardado correctamente',
							showConfirmButton: false,
							timer: 2500
						})
					}
					if (data.codigo === 2) {
						Swal.fire({
							position: 'center',
							icon: 'error',
							title: data.msg,
							showConfirmButton: false,
							timer: 2500
						})
					}
					if (data.codigo === 3) {
						$("#newModal").modal('hide')
						recargarTable()
						Swal.fire({
							position: 'center',
							icon: 'warning',
							title: 'Se ha guardado correctamente y' + data.msg,
							showConfirmButton: false,
							timer: 2500
						})
					}
					if (data.codigo === 4) {
						Swal.fire({
							position: 'center',
							icon: 'error',
							title: data.msg,
							showConfirmButton: false,
							timer: 2500
						})
					}
				}
			});
		}
		if (proceso == 2) {
			$.ajax({
				url: '<?php echo base_url('Cliente_Ust/registrarCandidato'); ?>',
				type: 'POST',
				data: datos,
				contentType: false,
				cache: false,
				processData: false,
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 200);
					var data = JSON.parse(res);
					if (data.codigo === 1) {
						$("#newModal").modal('hide')
						recargarTable2()
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: 'Se ha guardado correctamente',
							showConfirmButton: false,
							timer: 2500
						})
					} else {
						$("#newModal #msj_error").css('display', 'block').html(data.msg);
					}
				}
			});
		}
		if (proceso == '') {
			$("#newModal #msj_error").css('display', 'block').html('El campo Tipo de proceso es obligatorio');
		}
	}
	function guardarGenerales() {
		var datos = new FormData();
		datos.append('nombre', $("#nombre_general").val());
		datos.append('paterno', $("#paterno_general").val());
		datos.append('materno', $("#materno_general").val());
		datos.append('fecha_nacimiento', $("#fecha_nacimiento").val());
		datos.append('nacionalidad', $("#nacionalidad").val());
		datos.append('puesto', $("#puesto_general").val());
		datos.append('genero', $("#genero").val());
		datos.append('calle', $("#calle").val());
		datos.append('exterior', $("#exterior").val());
		datos.append('interior', $("#interior").val());
		datos.append('colonia', $("#colonia").val());
		datos.append('estado', $("#estado").val());
		datos.append('municipio', $("#municipio").val());
		datos.append('cp', $("#cp").val());
		datos.append('civil', $("#civil").val());
		datos.append('celular_general', $("#celular_general").val());
		datos.append('tel_casa', $("#tel_casa").val());
		datos.append('tel_oficina', $("#tel_oficina").val());
		datos.append('personales_correo', $("#personales_correo").val());
		datos.append('id_candidato', $("#idCandidato").val());
		//Respaldo txt
		var formdata = $('#d_generales').serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'datos_generales-' + idCandidato + '-' + fecha_txt);
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarDatosGenerales'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#generalesModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#generalesModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function guardarEstudios() {
		var datos = new FormData();
		datos.append('prim_periodo', $("#prim_periodo").val());
		datos.append('prim_escuela', $("#prim_escuela").val());
		datos.append('prim_ciudad', $("#prim_ciudad").val());
		datos.append('prim_certificado', $("#prim_certificado").val());
		datos.append('prim_validado', $("#prim_validado").val());
		datos.append('sec_periodo', $("#sec_periodo").val());
		datos.append('sec_escuela', $("#sec_escuela").val());
		datos.append('sec_ciudad', $("#sec_ciudad").val());
		datos.append('sec_certificado', $("#sec_certificado").val());
		datos.append('sec_validado', $("#sec_validado").val());
		datos.append('prep_periodo', $("#prep_periodo").val());
		datos.append('prep_escuela', $("#prep_escuela").val());
		datos.append('prep_ciudad', $("#prep_ciudad").val());
		datos.append('prep_certificado', $("#prep_certificado").val());
		datos.append('prep_validado', $("#prep_validado").val());
		datos.append('lic_periodo', $("#lic_periodo").val());
		datos.append('lic_escuela', $("#lic_escuela").val());
		datos.append('lic_ciudad', $("#lic_ciudad").val());
		datos.append('lic_certificado', $("#lic_certificado").val());
		datos.append('lic_validado', $("#lic_validado").val());
		datos.append('otro_certificado', $("#otro_certificado").val());
		datos.append('carrera_inactivo', $("#carrera_inactivo").val());
		datos.append('estudios_comentarios', $("#estudios_comentarios").val());
		datos.append('id_candidato', $("#idCandidato").val());

		//Respaldo txt
		formdata = $('#d_estudios').serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'historial_academico-' + idCandidato + '-' + fecha_txt);

		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarHistorialAcademico'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#academicosModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#academicosModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function guardarDocumentacion() {
		var datos = new FormData();
		datos.append('lic_profesional', $("#lic_profesional").val());
		datos.append('lic_institucion', $("#lic_institucion").val());
		datos.append('ine_clave', $("#ine_clave").val());
		datos.append('ine_registro', $("#ine_registro").val());
		datos.append('ine_vertical', $("#ine_vertical").val());
		datos.append('ine_institucion', $("#ine_institucion").val());
		datos.append('penales_numero', $("#penales_numero").val());
		datos.append('penales_institucion', $("#penales_institucion").val());
		datos.append('doc_comentarios', $("#doc_comentarios").val());
		datos.append('id_candidato', $("#idCandidato").val());

		//Respaldo txt
		formdata = $('#d_documentos').serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'documentacion_verificacion-' + idCandidato + '-' + fecha_txt);
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarVerificacionDocumentos'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#documentosModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#documentosModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function actualizarFamiliar(num, id) {
		var datos = new FormData();
		var id_familiar = $("#id").val();
		datos.append('parentesco', $('#parentesco' + num).val());
		datos.append('nombre', $('#nombre_familiar' + num).val());
		datos.append('edad', $('#edad_familiar' + num).val());
		datos.append('puesto', $('#puesto_familiar' + num).val());
		datos.append('ciudad', $('#ciudad_familiar' + num).val());
		datos.append('empresa', $('#empresa_familiar' + num).val());
		datos.append('misma_vivienda', $('#misma_vivienda' + num).val());
		datos.append('id_familiar', id);
		datos.append('id_candidato', $("#idCandidato").val());

		//Respaldo txt
		var formdata = $("#d_familiares").serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'informacion_familiar_' + idCandidato + '-' + fecha_txt);
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarInformacionFamiliar'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#familiaresModal #msj_error" + num).css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function guardarRefPersonal(num) {
		var datos = new FormData();
		datos.append('nombre', $('#refper' + num + '_nombre').val());
		datos.append('tiempo', $('#refper' + num + '_tiempo').val());
		datos.append('lugar', $('#refper' + num + '_lugar').val());
		datos.append('telefono', $('#refper' + num + '_telefono').val());
		datos.append('trabaja', $('#refper' + num + '_trabaja').val());
		datos.append('vive', $('#refper' + num + '_vive').val());
		datos.append('comentario', $('#refper' + num + '_comentario').val());
		datos.append('id_refper', $('#refper' + num + '_id').val());
		datos.append('num', num);
		datos.append('id_candidato', $("#idCandidato").val());

		formdata = $("#d_refpersonal" + num).serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'referencia_personal_' + num + '-' + idCandidato + '-' + fecha_txt);
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarReferenciaPersonal'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 0) {
					$("#refPersonalesModal #msj_error" + num).css('display', 'block').html(data.msg);
				}
				if (data.codigo === 1) {
					recargarTable()
					$("#refPersonalesModal #msj_error" + num).css('display', 'none');
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				}
				if (data.codigo === 2) {
					recargarTable()
					$("#refPersonalesModal #msj_error" + num).css('display', 'none');
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
					$('#refper' + num + '_id').val(data.msg);
				}
			}
		});
	}
	function actualizarTrabajoGobierno() {
		var datos = new FormData();
		datos.append('trabajo', $("#trabajo_gobierno").val());
		datos.append('inactivo', $("#trabajo_inactivo").val());
		datos.append('id_candidato', $("#idCandidato").val());
		datos.append('caso', 1);
		
		//Respaldo txt
		formdata = $("#trabajo_gobierno").val();
		formdata += ',' + $("#trabajo_inactivo").val();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'trabajo_gobierno-' + idCandidato + '-' + fecha_txt);
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarTrabajoGobierno'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					recargarTable()
					$("#trabajos_msj_error").css('display', 'none')
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#trabajos_msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function actualizarReferenciaLaboral(num) {
		var datos = new FormData();
		datos.append('empresa', $('#reflab' + num + '_empresa_ingles').val());
		datos.append('direccion', $('#reflab' + num + '_direccion_ingles').val());
		datos.append('entrada', $('#reflab' + num + '_entrada_ingles').val());
		datos.append('salida', $('#reflab' + num + '_salida_ingles').val());
		datos.append('telefono', $('#reflab' + num + '_telefono_ingles').val());
		datos.append('puesto1', $('#reflab' + num + '_puesto1_ingles').val());
		datos.append('puesto2', $('#reflab' + num + '_puesto2_ingles').val());
		datos.append('salario1', $('#reflab' + num + '_salario1_ingles').val());
		datos.append('salario2', $('#reflab' + num + '_salario2_ingles').val());
		datos.append('jefenombre', $('#reflab' + num + '_jefenombre_ingles').val());
		datos.append('jefecorreo', $('#reflab' + num + '_jefecorreo_ingles').val());
		datos.append('jefepuesto', $('#reflab' + num + '_jefepuesto_ingles').val());
		datos.append('separacion', $('#reflab' + num + '_separacion_ingles').val());
		datos.append('idref', $('#idreflabingles'+num).val());
		datos.append('num', num);
		datos.append('id_candidato', $("#idCandidato").val());

		formdata = $("#candidato_reflaboral" + num).serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'referencia_laboral_' + num + '-' + idCandidato + '-' + fecha_txt);

		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarReferenciaLaboralCandidato'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 0) {
					$("#reflab_msj_error" + num).css('display', 'block').html(data.msg);
				}
				if (data.codigo === 1) {
					recargarTable()
					$("#reflab_msj_error" + num).css('display', 'none');
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				}
				if (data.codigo === 2) {
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
					$('#idreflabingles' + num).val(data.msg);
				}
			}
		});
	}
	function verificarLaboral(num) {
		var datos = new FormData();
		datos.append('empresa', $('#an_reflab' + num + '_empresa').val());
		datos.append('direccion', $('#an_reflab' + num + '_direccion').val());
		datos.append('entrada', $('#an_reflab' + num + '_entrada').val());
		datos.append('salida', $('#an_reflab' + num + '_salida').val());
		datos.append('telefono', $('#an_reflab' + num + '_telefono').val());
		datos.append('puesto1', $('#an_reflab' + num + '_puesto1').val());
		datos.append('puesto2', $('#an_reflab' + num + '_puesto2').val());
		datos.append('salario1', $('#an_reflab' + num + '_salario1').val());
		datos.append('salario2', $('#an_reflab' + num + '_salario2').val());
		datos.append('jefenombre', $('#an_reflab' + num + '_jefenombre').val());
		datos.append('jefecorreo', $('#an_reflab' + num + '_jefecorreo').val());
		datos.append('jefepuesto', $('#an_reflab' + num + '_jefepuesto').val());
		datos.append('separacion', $('#an_reflab' + num + '_separacion').val());
		datos.append('notas', $('#an_reflab' + num + '_notas').val());
		datos.append('demanda', $('#an_reflab' + num + '_demanda').val());
		datos.append('responsabilidad', $('#an_reflab' + num + '_responsabilidad').val());
		datos.append('iniciativa', $('#an_reflab' + num + '_iniciativa').val());
		datos.append('eficiencia', $('#an_reflab' + num + '_eficiencia').val());
		datos.append('disciplina', $('#an_reflab' + num + '_disciplina').val());
		datos.append('puntualidad', $('#an_reflab' + num + '_puntualidad').val());
		datos.append('limpieza', $('#an_reflab' + num + '_limpieza').val());
		datos.append('estabilidad', $('#an_reflab' + num + '_estabilidad').val());
		datos.append('emocional', $('#an_reflab' + num + '_emocional').val());
		datos.append('honesto', $('#an_reflab' + num + '_honesto').val());
		datos.append('rendimiento', $('#an_reflab' + num + '_rendimiento').val());
		datos.append('actitud', $('#an_reflab' + num + '_actitud').val());
		datos.append('recontratacion', $('#an_reflab' + num + '_recontratacion').val());
		datos.append('motivo', $('#an_reflab' + num + '_motivo').val());
		datos.append('idverlab', $('#idverlab'+num).val());
		datos.append('num', num);
		datos.append('id_candidato', $("#idCandidato").val());
		//Respaldo txt
		formdata = $("#analista_reflaboral" + num).serialize();
		var idCandidato = $("#idCandidato").val();
		var f = new Date();
		var fecha_txt = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
		respaldoTxt(formdata, 'verificacion_laboral_' + num + '-' + idCandidato + '-' + fecha_txt);

		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarVerificacionLaboralCandidato'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					recargarTable()
					$("#verlab_msj_error" + num).css('display', 'none');
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				}
				else{
					$("#verlab_msj_error" + num).css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function verificacionEstudios() {
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		$("#fecha_estatus_estudio").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkEstatusEstudios'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				if (res != 0) {
					var aux = res.split('@@');
					var finalizado = aux[2];
					if (finalizado == 1) {
						$("#div_estatus_estudio").css('display', 'none');
						$("#btnTerminarVerificacionEstudio").css('display', 'none');
					}
					$("#div_crearEstatusEstudio").empty();
					$("#idVerificacionEstudio").val(aux[1]);
					$("#div_crearEstatusEstudio").append(aux[0]);
				} else {
					$("#div_crearEstatusEstudio").empty();
					$("#div_crearEstatusEstudio").append('<p>Sin registros</p>');
					$("#div_estatus_estudio").css('display', 'block');
					$("#btnTerminarVerificacionEstudio").css('display', 'initial');
					$("#idVerificacionEstudio").val(0);
				}

			}
		});
		$("#verificacionEstudiosModal").modal("show");
	}
	function generarEstatusEstudio() {
		var id_candidato = $("#idCandidato").val();
		var id_verificacion = $("#idVerificacionEstudio").val();
		var comentario = $("#estudio_estatus_comentario").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusEstudios'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'id_verificacion': id_verificacion,
				'comentario': comentario
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#verificacionEstudiosModal  #msj_error").css('display', 'none');
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
					var aux = data.msg.split('@@');
					$("#idVerificacionEstudio").val(aux[1]);
					$("#estudio_estatus_comentario").val("");
					$("#div_crearEstatusEstudio").empty();
					$("#div_crearEstatusEstudio").append(aux[0]);
				}
				else{
					$("#verificacionEstudiosModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function terminarEstudios() {
		var id_verificacion = $("#idVerificacionEstudio").val();
		var id_candidato = $("#idCandidato").val();

		$.ajax({
			url: '<?php echo base_url('Candidato/finishEstatusEstudios'); ?>',
			method: 'POST',
			data: {
				'id_verificacion': id_verificacion,
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				$("#confirmarEstudiosModal").modal('hide');
				$("#verificacionEstudiosModal").modal('hide');
				recargarTable()
				Swal.fire({
					position: 'center',
					icon: 'success',
					title: 'Se ha actualizado correctamente',
					showConfirmButton: false,
					timer: 2500
				})
			}
		});
	}
	function verificacionLaborales() {
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		$("#fecha_estatus_laboral").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkEstatusLaborales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				if (res != 0) {
					var aux = res.split('@@');
					var finalizado = aux[2];
					if (finalizado == 1) {
						$("#div_estatus_laboral").css('display', 'none');
						$("#btnTerminarVerificacionLaboral").css('display', 'none');
					}
					$("#div_crearEstatusLaboral").empty();
					$("#idVerificacionLaboral").val(aux[1]);
					$("#div_crearEstatusLaboral").append(aux[0]);
				} else {
					$("#div_crearEstatusLaboral").empty();
					$("#div_crearEstatusLaboral").append('<p>Sin registros </p>');
					$("#div_estatus_laboral").css('display', 'block');
					$("#btnTerminarVerificacionLaboral").css('display', 'initial');
					$("#idVerificacionLaboral").val(0);
				}
			}
		});
		$("#verificacionLaboralesModal").modal("show");
	}
	function generarEstatusLaboral() {
		var id_candidato = $("#idCandidato").val();
		var id_verificacion = $("#idVerificacionLaboral").val();
		var comentario = $("#laboral_estatus_comentario").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusLaborales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'id_verificacion': id_verificacion,
				'comentario': comentario
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#verificacionLaboralesModal  #msj_error").css('display', 'none');
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
					var aux = data.msg.split('@@');
					$("#idVerificacionLaboral").val(aux[1]);
					$("#laboral_estatus_comentario").val("");
					$("#div_crearEstatusLaboral").empty();
					$("#div_crearEstatusLaboral").append(aux[0]);
				}
				else{
					$("#verificacionLaboralesModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function terminarLaborales() {
		var id_verificacion = $("#idVerificacionLaboral").val();
		var id_candidato = $("#idCandidato").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/finishEstatusLaborales'); ?>',
			method: 'POST',
			data: {
				'id_verificacion': id_verificacion,
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				$("#confirmarLaboralesModal").modal('hide');
				$("#verificacionLaboralesModal").modal('hide');
				recargarTable()
				Swal.fire({
					position: 'center',
					icon: 'success',
					title: 'Se ha actualizado correctamente',
					showConfirmButton: false,
					timer: 2500
				})
			}
		});
	}
	function verificacionPenales() {
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		$("#fecha_estatus_penales").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkEstatusPenales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				if (res != 0) {
					var aux = res.split('@@');
					var finalizado = aux[2];
					if (finalizado == 1) {
						$("#div_estatus_penales").css('display', 'none');
						$("#btnTerminarVerificacionPenales").css('display', 'none');
					}
					$("#div_crearEstatusPenales").empty();
					$("#idVerificacionPenales").val(aux[1]);
					$("#div_crearEstatusPenales").append(aux[0]);
				} else {
					$("#div_crearEstatusPenales").empty();
					$("#div_crearEstatusPenales").append('<p>Sin registros </p>');
					$("#div_estatus_penales").css('display', 'block');
					$("#btnTerminarVerificacionPenales").css('display', 'initial');
					$("#idVerificacionPenales").val(0);
				}

			}
		});
		$("#verificacionPenalesModal").modal("show");
	}
	function generarEstatusPenales() {
		var id_candidato = $("#idCandidato").val();
		var id_verificacion = $("#idVerificacionPenales").val();
		var comentario = $("#penales_estatus_comentario").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusPenales'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'id_verificacion': id_verificacion,
				'comentario': comentario
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#verificacionPenalesModal  #msj_error").css('display', 'none');
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
					var aux = data.msg.split('@@');
					$("#idVerificacionPenales").val(aux[1]);
					$("#penales_estatus_comentario").val("");
					$("#div_crearEstatusPenales").empty();
					$("#div_crearEstatusPenales").append(aux[0]);
				}
				else{
					$("#verificacionPenalesModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function terminarPenales() {
		var id_verificacion = $("#idVerificacionPenales").val();
		var id_candidato = $("#idCandidato").val();
		$.ajax({
			url: '<?php echo base_url('Candidato/finishEstatusPenales'); ?>',
			method: 'POST',
			data: {
				'id_verificacion': id_verificacion,
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				$("#confirmarPenalesModal").modal('hide');
				$("#verificacionPenalesModal").modal('hide');
				recargarTable()
				Swal.fire({
					position: 'center',
					icon: 'success',
					title: 'Se ha actualizado correctamente',
					showConfirmButton: false,
					timer: 2500
				})
			}
		});
	}
	function subirDoc() {
		var data = new FormData();
		var doc = $("#documento")[0].files[0];
		data.append('id_candidato', $("#idCandidato").val());
		data.append('prefijo', $(".prefijo").val());
		data.append('tipo_doc', $("#tipo_archivo").val());
		data.append('documento', doc);
		$.ajax({
			url: "<?php echo base_url('Candidato/cargarDocumento'); ?>",
			method: "POST",
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#documento").val("");
					$("#tablaDocs").empty();
					$('#tipo_archivo').val('');
					$("#tablaDocs").html(data.msg);
					$("#docsModal #msj_error").css('display', 'none')
					recargarTable();
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				}
				if (data.codigo === 0) {
					$("#docsModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function eliminarArchivo(idDoc, archivo, id_candidato) {
		$("#fila" + idDoc).remove();
		$.ajax({
			url: '<?php echo base_url('Candidato/eliminarDocumento'); ?>',
			method: 'POST',
			data: {
				'idDoc': idDoc,
				'archivo': archivo,
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha eliminado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				}
				else{
					Swal.fire({
						position: 'center',
						icon: 'error',
						title: 'Hubo un problema al eliminar, intenta más tarde',
						showConfirmButton: false,
						timer: 2500
					})
				}
			}
		});
	}
	function ejecutarAccion() {
		var accion = $("#btnGuardar").val();
		var id_candidato = $("#idCandidato").val();
		var correo = $("#correo").val();
		if (accion == 'generar') {
			$.ajax({
				url: '<?php echo base_url('Cliente_Ust/regenerarPassword'); ?>',
				type: 'post',
				data: {
					'id_candidato': id_candidato,
					'correo': correo
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 200);
					var data = JSON.parse(res);
					if (data.codigo === 1) {
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: 'Se ha actualizado correctamente',
							showConfirmButton: false,
							timer: 2500
						})
					}
					if (data.codigo === 3) {
						$("#newModal").modal('hide')
						recargarTable()
						Swal.fire({
							position: 'center',
							icon: 'warning',
							title: 'Se ha guardado correctamente pero no se envió el correo',
							showConfirmButton: false,
							timer: 2500
						})
					}
					$("#quitarModal").modal('hide');
					$("#user").text(correo);
					$("#pass").text(data.msg);
					$("#respuesta_mail").text("* Un correo ha sido enviado al candidato con sus nuevas credenciales. Este correo puede demorar algunos minutos.");
					$("#passModal").modal('show');
				}
			});
		}
		if (accion == 'delete') {
			$.ajax({
				url: '<?php echo base_url('Candidato/cancelarCandidato'); ?>',
				type: 'post',
				data: {
					'id_candidato': id_candidato
				},
				beforeSend: function() {
					$('.loader').css("display", "block");
				},
				success: function(res) {
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 200);
					var data = JSON.parse(res);
					if (data.codigo === 1) {
						$("#quitarModal").modal('hide');
						recargarTable()
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: 'Se ha cancelado correctamente',
							showConfirmButton: false,
							timer: 2500
						})
					}
				}
			});
		}
	}
	function finalizarProceso() {
		var datos = new FormData();
		datos.append('check_identidad', $("#check_identidad").val());
		datos.append('check_laboral', $("#check_laboral").val());
		datos.append('check_estudios', $("#check_estudios").val());
		datos.append('check_visita', $("#check_visita").val());
		datos.append('check_penales', $("#check_penales").val());
		datos.append('check_ofac', $("#check_ofac").val());
		datos.append('check_laboratorio', $("#check_laboratorio").val());
		datos.append('check_medico', $("#check_medico").val());
		datos.append('comentario_final', $("#comentario_final").val());
		datos.append('bgc_status', $("#bgc_status").val());
		datos.append('id_candidato', $("#idCandidato").val());
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/finalizarProcesoESE'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#completarModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#completarModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function estatusAvances() {
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_avances").text(dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
			url: '<?php echo base_url('Candidato/checkAvances'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					if (res != 0) {
						var aux = res.split('@@');
						var finalizado = aux[2];
						if (finalizado == 1) {
							$("#div_estatus_avances").css('display', 'none');
						}
						$("#div_crearEstatusAvances").empty();
						$("#idAvances").val(aux[1]);
						$("#div_crearEstatusAvances").append(aux[0]);
					} else {
						$("#idAvances").val(0);
						$("#div_crearEstatusAvances").empty();
						$("#div_crearEstatusAvances").html('<div class="col-12"><p class="text-center">Sin registros</p></div>');
					}
					$('.loader').fadeOut();
				}, 200);
			}
		});
		$("#avancesModal").modal("show");
	}
	function generarEstatusAvance() {
		var datos = new FormData();
		datos.append('id_candidato', $("#idCandidato").val());
		datos.append('id_avance', $("#idAvances").val());
		datos.append('comentario', $("#avances_estatus_comentario").val());
		datos.append('adjunto', $("#adjunto")[0].files[0]);
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusAvance'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$("#adjunto").val("");
					var aux = res.split('@@');
					$("#idAvances").val(aux[1]);
					$("#avances_estatus_comentario").val("");
					$("#div_crearEstatusAvances").empty();
					$("#div_crearEstatusAvances").append(aux[0]);
					$('.loader').fadeOut();
				}, 200);
			}
		});
	}
	function regresarListado() {
		location.reload();
	}
	//Funciones Proceso # 2 FACIS
	function estatusOFAC() {
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() + 1);
		var dia = (dia < 10) ? '0' + dia : dia;
		var mes = (mes < 10) ? '0' + mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/getDatosFACIS'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					var datos = data.msg.split('@@');
					if (datos[0] == 0) {
						$("#fecha_titulo_ofac").html("<b>Fecha</b>");
						$("#fecha_estatus_ofac").text(dia + "/" + mes + "/" + f.getFullYear() + " " + h + ":" + m);
					} else {
						$("#fecha_titulo_ofac").html("<b>Última actualización</b>");
						$("#fecha_estatus_ofac").text(datos[0]);
					}
					$("#estatus_ofac").val(datos[1]);
					$("#res_ofac").val(datos[2]);
					$("#estatus_oig").val(datos[3]);
					$("#res_oig").val(datos[4]);
					$("#estatus_sam").val(datos[5]);
					$("#res_sam").val(datos[6]);
					$("#estatus_juridica").val(datos[7]);
					$("#res_juridica").val(datos[8]);
				} else {
					//$("#completarModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
		$("#ofacModal").modal("show");
	}
	function actualizarOfac() {
		var datos = new FormData();
		datos.append('id_candidato', $("#idCandidato").val());
		datos.append('id_cliente', $("#idCliente").val());
		datos.append('ofac', $("#estatus_ofac").val());
		datos.append('oig', $("#estatus_oig").val());
		datos.append('sam', $("#estatus_sam").val());
		datos.append('juridica', $("#estatus_juridica").val());
		datos.append('res_ofac', $("#res_ofac").val());
		datos.append('res_oig', $("#res_oig").val());
		datos.append('res_sam', $("#res_sam").val());
		datos.append('res_juridica', $("#res_juridica").val());

		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/guardarFACIS'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#ofacModal").modal('hide')
					recargarTable()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				} else {
					$("#ofacModal #msj_error").css('display', 'block').html(data.msg);
				}
			}
		});
	}
	function actualizarFecha(){
		var id_candidato = $("#idCandidato").val();
		var fecha = $("#fecha_facis").val();
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/cambiarFechaFACIS'); ?>',
			type: 'post',
			data: {
				'id_candidato': id_candidato,
				'fecha': fecha
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#fechaModal").modal('hide');
					recargarTable2()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha cancelado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				}
				else{
					$('#fechaModal #msj_error').css('display','block').html(data.msg);
				}
			}
		});
	}
	function finalizarFACIS() {
		var id_candidato = $("#idCandidato").val();
		var comentario = $("#ofac_comentario_final").val();
		var estatus = $("#ofac_estatus_final").val();
		//console.log(checks)
		$.ajax({
			url: '<?php echo base_url('Cliente_Ust/finalizarProcesoFACIS'); ?>',
			method: 'POST',
			data: {
				'id_candidato': id_candidato,
				'comentario': comentario,
				'estatus':estatus
			},
			beforeSend: function() {
				$('.loader').css("display", "block");
			},
			success: function(res) {
				setTimeout(function() {
					$('.loader').fadeOut();
				}, 200);
				var data = JSON.parse(res);
				if (data.codigo === 1) {
					$("#completarFacisModal").modal('hide');
					recargarTable2()
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Se ha actualizado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
				}
				else{
					$('#completarFacisModal #msj_error').css('display','block').html(data.msg);
				}
			}
		});
	}
	//Funciones de apoyo
	function getMunicipio(id_estado, id_municipio) {
		$.ajax({
			url: '<?php echo base_url('Funciones/getMunicipios'); ?>',
			method: 'POST',
			data: {
				'id_estado': id_estado
			},
			dataType: "text",
			success: function(res) {
				$('#municipio').prop('disabled', false);
				$('#municipio').html(res);
				$("#municipio").find('option').attr("selected", false);
				$('#municipio option[value="' + id_municipio + '"]').attr('selected', 'selected');
			}
		});
	}
	function recargarTable() {
		$("#tabla").DataTable().ajax.reload();
	}
	function recargarTable2() {
		$("#tabla2").DataTable().ajax.reload();
	}
	function aceptarRevision(){
		$("#revisionModal").modal('hide');
		$("#completarModal").modal('show');
	}
	function convertirDate(fecha) {
		var aux = fecha.split('-');
		var f = aux[1] + '/' + aux[2] + '/' + aux[0];
		return f;
	}
	function convertirDateTime(fecha) {
		var aux = fecha.split(' ');
		var time = aux[0].split('-');
		var dia = time[1] + '/' + time[2] + '/' + time[0];
		return dia;
	}
	function respaldoTxt(formdata, nombreArchivo) {
		var textFileAsBlob = new Blob([formdata], {
			type: 'text/plain'
		});
		var fileNameToSaveAs = nombreArchivo + ".txt";
		var downloadLink = document.createElement("a");
		downloadLink.download = fileNameToSaveAs;
		downloadLink.innerHTML = "My Hidden Link";
		window.URL = window.URL || window.webkitURL;
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
		downloadLink.click();
	}
	function destroyClickedElement(event) {
		document.body.removeChild(event.target);
	}
	//Aplica el valor del option a todos los select correspondientes
	$("#aplicar_todo1").change(function() {
		var valor = $(this).val();
		switch (valor) {
			case "-1":
				$(".performance1").val("Not provided");
				break;
			case "0":
				$(".performance1").val("Not provided");
				break;
			case "1":
				$(".performance1").val("Excellent");
				break;
			case "2":
				$(".performance1").val("Good");
				break;
			case "3":
				$(".performance1").val("Regular");
				break;
			case "4":
				$(".performance1").val("Bad");
				break;
			case "5":
				$(".performance1").val("Very Bad");
				break;
		}
	});
	$("#aplicar_todo2").change(function() {
		var valor = $(this).val();
		switch (valor) {
			case "-1":
				$(".performance2").val("Not provided");
				break;
			case "0":
				$(".performance2").val("Not provided");
				break;
			case "1":
				$(".performance2").val("Excellent");
				break;
			case "2":
				$(".performance2").val("Good");
				break;
			case "3":
				$(".performance2").val("Regular");
				break;
			case "4":
				$(".performance2").val("Bad");
				break;
			case "5":
				$(".performance2").val("Very Bad");
				break;
		}
	});
	$(".solo_numeros").on("input", function() {
		var valor = $(this).val();
		$(this).val(valor.replace(/[^0-9]/g, ''));
	});
</script>