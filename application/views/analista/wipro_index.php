<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div id="exito" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> El registro se ha guardado correctamente.
  	</div>
  	<div id="vacios" class="alert alert-danger fade in mensaje" style='display:none;'>
      	<strong>¡Atención!</strong><span id="txt_vacios"></span>
  	</div>
  	<div id="exitoFinalizado" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> El proceso del candidato ha finalizado correctamente.
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong><p id="texto_msj"></p></strong>
  	</div>
	<section class="content-header">
		<!--a class="btn btn-app" id="btn_regresar" class="btn btn-app" data-toggle="modal" data-target="#finalizadosModal">
    		<i class="fas fa-check-circle"></i> <span> Ver finalizados</span>
  		</a-->
      	<h1 class="titulo_seccion">Cliente<small><?php echo $cliente; ?></small></h1>
    	<a class="btn btn-app" id="btn_eliminados" class="btn btn-app">
    		<i class="fas fa-trash"></i> <span>Ver eliminados</span>
  		</a>
		<a class="btn btn-app" id="btn_nuevo" href="javascript:void(0)" onclick="registro()">
			<i class="fas fa-user-plus icon"></i> <span>Agregar candidato</span>
		</a>
    </section>
    <div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title" id="tituloNuevo"></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<form id="datos">
			        	<div class="row">
			        		<div class="col-md-12">
			        			<label>Nombre del candidato *</label>
			        			<input type="text" class="form-control registro_obligado" name="nombre" id="nombre" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label>Proyecto *</label>
			        			<select name="subcliente" id="subcliente" class="form-control registro_obligado">
						            <option value="">Selecciona</option>
					            <?php 
					            	foreach ($subclientes as $sub) {
					            		if($sub->id != 136 && $sub->id != 137){?>
					            			<option value="<?php echo $sub->id; ?>"><?php echo $sub->nombre; ?></option>
					            <?php		
					        			}  
									} ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label>Subproyecto *</label>
			        			<select name="proyecto" id="proyecto" class="form-control registro_obligado" disabled>
						            <option value="">Selecciona</option>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
                            	<label>ID empresa</label>
                                <input type="text" class="form-control" name="idempresa" id="idempresa">
                            </div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
                            	<label>Fecha y hora de alta</label>
                                <input type="text" class="form-control" name="fecha_actual" id="fecha_actual" disabled>
                            </div>
			        	</div>
			        </form>
		      		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-success" id="guardar" onclick="registrar()">Guardar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="concluirModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title" id="confirmar_titulo"></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<h4><span id="confirmar_pregunta"></span><b><span id="confirmar_nombreCandidato"></span></b></h4>
		      		<div class="row">
		      			<div class="col-md-6 col-md-offset-3">
	      					<label for="resultado">Se concluye el estudio del candidato como: *</label>
		        			<select name="resultado" id="resultado" class="form-control estatus_obligado">
					            <option value="">Selecciona</option>
					            <option value="1">Recomendable</option>
					            <option value="2">No recomendable</option>
					            <option value="3">A consideración</option>
				          	</select>
				          	<br>
		      			</div>
		      		</div>
		      		<div class="row">
		      			<div class="col-md-12">
		      				<label>Comentario</label>
		      				<textarea class="form-control" name="comentario_final" id="comentario_final" rows="2"></textarea>
		      			</div>
		      		</div>
		      		<div id="campo_vacio" class="alert alert-danger">
		      			<p class="msj_error text-white">Elige el estatus final del candidato</p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			        <button type="button" class="btn btn-success" onclick="finalizarProceso()">Aceptar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Avances en el estudio del candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idAvances">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusAvances">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_avances">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_avances"></p>
                            </div>
                            <div class="col-md-5">
                            	<label for="avances_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="avances_estatus_comentario" id="avances_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>
							<div class="col-md-4">
			        			<label for="adjunto">Adjuntar imagen</label>
			        			<input type="file" id="adjunto" name="adjunto" class="form-control" accept=".jpg, .jpeg, .png"><br>
			        		</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusAvance()">Actualizar avances</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	        		<div class="row" id="div_motivo">
	        			<div class="col-md-12">
	        				<label for="motivo">Motivo *</label>
	        				<textarea name="motivo" id="motivo" class="form-control" rows="3"></textarea>
	        				<br>
	        			</div>
	        		</div>
	        		<div id="msj_error" class="alert alert-danger">
		      			<p id="msj_texto" class="text-white"></p>
		      		</div>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      		<button type="button" class="btn btn-success" onclick="ejecutarAccion()">Aceptar</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="eliminadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Candidatos eliminados</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_listado"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="finalizadosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Estudios finalizados</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<div class="row">
						<div class="col-md-12">
							<label>Busca al candidato *</label>
		        			<select name="candidato_finalizado" id="candidato_finalizado" class="form-control solo_lectura selectpicker" data-live-search="true">
					            <option value="">Selecciona</option>
					           	<?php 
					           	if($finalizados){
				           			foreach ($finalizados as $f) { ?>
				                		<option value="<?php echo $f->id; ?>"><?php echo $f->candidato.' - '.$f->analista; ?></option>
				            	<?php } 
				        		}?>
				          	</select>
				          	<br><br>
						</div>
					</div>
					<div class="row"> 
		        		<div class="col-md-12" id="detalle_finalizado">
		        		</div>
		        	</div>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		    	</div>
	  		</div>
		</div>
	</div>
    <div class="loader" style="display: none;"></div>
	<section class="content" id="listado">
	  	<div class="row">
	        <div class="col-xs-12">
	          	<div class="box">
	            	<div class="box-header">
	              		<!--h3 class="box-title"><strong>  Propietarios</strong></h3-->
	            	</div>
	            	<!-- /.box-header -->
	            	<div class="box-body table-responsive">
	            		<input type="hidden" id="idCandidato">
	            		<input type="hidden" id="idCliente">
	              		<table id="tabla" class="table stripe hover row-border cell-border"></table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
	          	<!-- /.box -->
	        </div>
	    </div>
	</section>
	
	
</div>
<!-- /.content-wrapper -->	
<script>
	var url = '<?php echo base_url('Cliente/WiproGet'); ?>';
  	$(document).ready(function(){
        $('#fecha_actual').inputmask("datetime", { "placeholder": "dd-mm-yyyy hh:mm" });
  		var msj2 = localStorage.getItem("finished");
  		var msj3 = localStorage.getItem("deleted");
  		if(msj2 == 1){
  			$("#texto_msj").text(" Estudio finalizado correctamente");
  			$("#mensaje").css('display','block');
  			setTimeout(function(){
          		$('#mensaje').fadeOut();
        	},4000);
        	localStorage.removeItem("finished");
  		}
  		if(msj3 == 1){
  			$("#texto_msj").text(" Candidato eliminado correctamente");
  			$("#mensaje").css('display','block');
  			setTimeout(function(){
          		$('#mensaje').fadeOut();
        	},4000);
        	localStorage.removeItem("deleted");
  		}
    	$('#tabla').DataTable({
      		"pageLength": 25,
	      	"pagingType": "simple",
	      	"order": [0, "desc"],
	      	//"stateSave": true,
	      	"serverSide": false,
	      	"ajax": url,
	      	"columns":[ 
	      		{ 	title: 'id', data: 'id', visible: false },
	        	{ 	title: 'Candidato', data: 'candidato', "width": "20%" },
	        	{ 	title: 'Proyecto', data: 'proyecto', "width": "12%",
	        		mRender: function(data, type, full){
						return "<small>Proyecto: </small>"+data+"<br><small>Región: </small>"+full.subcliente;
	        		}
	        	},
	        	{ 	title: 'Analista', data: 'analista', "width": "8%" },
	        	{ 	title: 'Fechas', data: 'fecha_alta', "width": "8%",
	        		mRender: function(data, type, full){
	        			var f = data.split(' ');
            			var h = f[1];
            			var aux = h.split(':');
            			var hora = aux[0]+':'+aux[1];
            			var aux = f[0].split('-');
            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
            			var f_inicio = fecha+' '+hora;
						return "Alta: "+f_inicio;
	        		}
	        	},
	        	{ title: 'Tiempo del proceso', data: 'tiempo', "width": "8%",
	        		mRender: function(data, type, full){
	        			if(data != null){
	        				if(data != -1){
	        					if(data >= 0 && data <= 2){
	            					return res = '<div class="formato_dias dias_verde">'+data+' días</div>';
	            				}
	            				if(data > 2 && data <= 4){
	            					return res = '<div class="formato_dias dias_amarillo">'+data+' días</div>';
	            				}
	            				if(data >= 5){
	            					return res = '<div class="formato_dias dias_rojo">'+data+' días</div>';
	            				}
	        				}
            				else{
            					return "Se actualizará en 1 hora";
            				}
            			}
            			else{
            				if(full.tiempo_parcial != 0){
            					var parcial = full.tiempo_parcial;
	            				if(parcial >= 0 && parcial < 2){
	            					return res = '<div class="formato_dias dias_verde">'+parcial+' días</div>';
	            				}
	            				if(parcial >= 2 && parcial <= 4){
	            					return res = '<div class="formato_dias dias_amarillo">'+parcial+' días</div>';
	            				}
	            				if(parcial >= 5){
	            					return res = '<div class="formato_dias dias_rojo">'+parcial+' días</div>';
	            				}
            				}
            				else{
            					return "Se actualizará en 1 hora";
            				}
            			}
          			}
	        	},
	        	{ 	title: 'Acciones', data: 'id', bSortable: false, "width": "8%",
         			mRender: function(data, type, full) {
         				if(full.status == 0){
         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Editar candidato" id="editar" class="fa-tooltip a-acciones"><i class="fas fa-edit"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Eliminar candidato" id="eliminar" class="fa-tooltip a-acciones"><i class="fas fa-trash"></i></a>';
         				}
         				else{
         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Editar candidato" id="editar" class="fa-tooltip a-acciones"><i class="fas fa-edit"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Eliminar candidato" id="eliminar" class="fa-tooltip a-acciones"><i class="fas fa-trash"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Conclusiones" id="conclusiones" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
         				}
          			}
        		},
        		{ 	title: 'Finalizar', data: 'id', bSortable: false, "width": "5%",
         			mRender: function(data, type, full) {
 						if(full.status_bgc == 0){
         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
         				}
         				if(full.status_bgc == 1){
         					return "<i class='fas fa-circle status_bgc1'></i>Positivo";
         				}
         				if(full.status_bgc == 2){
         					return "<i class='fas fa-circle status_bgc2'></i>Negativo";
         				}
         				if(full.status_bgc == 3){
         					return "<i class='fas fa-circle status_bgc3'></i>A consideración";
         				}
          			}
        		} 
	      	],
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$("a#msj_avances", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#avances_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusAvances();
	      		});
	      		$('a[id^=pdfFinal]', row).bind('click', () => {
		            var id = data.idDoping;
		            $('#pdf'+id).submit();
				});
				$('a[id^=otropdfFinal]', row).bind('click', () => {
		            var id = data.idDoping;
		            $('#otropdf'+id).submit();
		        });
	      		$("a#final", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
			        $("#confirmar_nombreCandidato").text("");
	      			$("#confirmar_titulo").text("Finalizar proceso del candidato "+data.nombreCompleto);
	      			$("#confirmar_pregunta").text("");
	      			$("#concluirModal").modal("show");
	      		});
	      		$("a#conclusiones", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
			       	$("#comentario_final").val(data.comentario_final);
			       	$("#resultado").val(data.status_bgc);
	      			$("#concluirModal").modal("show");
	      		});
	      		$("a#editar", row).bind('click', () => {
	      			var f = data.fecha_alta.split(' ');
    				var h = f[1];
    				var aux = h.split(':');
    				var hora = aux[0]+':'+aux[1];
    				var aux = f[0].split('-');
    				var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
    				var tiempo = fecha+' '+hora;
	      			$("#idCandidato").val(data.id);
	      			$("#tituloNuevo").text("Editar candidato");
	      			$("#guardar").val('editar');
	      			$("#nombre").val(data.nombre);
	      			$("#subcliente").val(data.id_subcliente);
	      			getSubproyecto(data.id_subcliente, data.id_cliente, data.id_proyecto);
	      			$("#fecha_actual").prop('disabled',false);
	      			$("#fecha_actual").val(tiempo);
	      			$("#idempresa").val(data.idempresa);
	      			$("#newModal").modal("show");
	      		});
	      		$("a#eliminar", row).bind('click', () => {
	      			$("#idCandidato").val(data.id);
	      			$("#idCliente").val(data.id_cliente);
	      			$("#titulo_accion").text("Eliminar candidato");
	      			//$("#texto_confirmacion").html("¿Estás seguro de desactivar el puesto <b>"+data.nombre+"</b>?");
	      			$("#div_motivo").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
      		},
	      	"language": {
	        	"lengthMenu": "Mostrar _MENU_ registros por página",
	        	"zeroRecords": "No se encontraron registros",
	        	"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        	"infoEmpty": "Sin registros disponibles",
	        	"infoFiltered": "(Filtrado _MAX_ registros totales)",
	        	"sSearch": "Buscar:",
	        	"oPaginate": {
	          		"sLast": "Última página",
	          		"sFirst": "Primera",
	          		"sNext": "<i class='fa  fa-arrow-right'></i>",
	          		"sPrevious": "<i class='fa fa-arrow-left'></i>"
	        	}
	      	}
    	}); 
    	$('#tabla').DataTable().search(" ");
    	$("#candidato_finalizado").change(function(){
	      	var idCandidato = $(this).val();
	      	if(idCandidato != ""){
	        	$.ajax({
	          		url: '<?php echo base_url('Candidato/getDetalleCandidatoTata'); ?>',
	          		method: 'POST',
	          		data: {'idCandidato':idCandidato},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#detalle_finalizado').empty();
	            		$('#detalle_finalizado').html(res);
	          		}
	        	});
	      	}
	      	else{
        		$('#detalle_finalizado').empty();
	      	}
		});
    	$("#subcliente").change(function(){
	      	var id_subcliente = $(this).val();
	      	var id_cliente = 77;
	      	if(id_subcliente != ""){
	        	$.ajax({
	          		url: '<?php echo base_url('Candidato/getProyectosSubcliente'); ?>',
	          		method: 'POST',
	          		data: {'id_subcliente':id_subcliente,'id_cliente':id_cliente},
	          		dataType: "text",
	          		success: function(res)
	          		{
	          			if(res != ""){
	          				$('#proyecto').prop('disabled', false);
	            			$('#proyecto').html(res);
	          			}
	          		}
	        	});
	      	}
	      	else{
	      		$('#proyecto').empty();
	      		$('#proyecto').append($("<option selected></option>").attr("value","").text("Selecciona"));
	      	}	      	
	    });	
    	
		$("#btn_eliminados").click(function(){
			var id_cliente = 77;
			$.ajax({
              	url: '<?php echo base_url('Candidato/getCandidatosEliminados'); ?>',
              	type: 'POST',
              	data: {'id_cliente':id_cliente},
              	success : function(res){ 
              		$("#div_listado").html(res);
              		$("#eliminadosModal").modal("show");
              	}
        	});
		});
  		$('#newModal').on('hidden.bs.modal', function (e) {
		  	$("#msj_error").css('display','none');
		  	$(".registro_obligado").removeClass("requerido");
		});
		$("#finalizadosModal").on("hidden.bs.modal", function(){
	    	$("#candidato_finalizado").val("").trigger('change');
	    	$("#detalle_finalizado").empty();
	    });
		$('#concluirModal').on('hidden.bs.modal', function (e) {
			$("#alert-msg").empty();
		  	$("#alert-msg").css('display','none');
		  	$("#campo_vacio").css('display','none');
		  	$(".estatus_obligado").removeClass("requerido");
			$(this)
			    .find("input,select")
			       .val('')
			       .end();
		});
		$(".registro_obligado, .estatus_obligado").focus(function(){
			$(this).removeClass("requerido");
		});
  	});
	function getSubproyecto(id_subcliente, id_cliente, id_proyecto){
		$.ajax({
        	url: '<?php echo base_url('Candidato/getProyectosSubcliente'); ?>',
        	method: 'POST',
        	data: {'id_subcliente':id_subcliente,'id_cliente':id_cliente},
        	dataType: "text",
        	success: function(res){
          		$('#proyecto').prop('disabled', false);
          		$('#proyecto').html(res);
          		$("#proyecto").find('option').attr("selected",false) ;
          		$('#proyecto option[value="'+id_proyecto+'"]').prop('selected', true);
        	}
      	});
	}
	function registrar(){
		var datos = new FormData();
		datos.append('nombre', $("#nombre").val());
		datos.append('subcliente', $("#subcliente").val());
		datos.append('proyecto', $("#proyecto").val());
		datos.append('idempresa', $("#idempresa").val());
		datos.append('fecha_actual', $("#fecha_actual").val());
		datos.append('id_candidato', $("#idCandidato").val());
		datos.append('accion', $("#guardar").val());
		datos.append('id_cliente', 77);

		var totalVacios = $('.registro_obligado').filter(function(){
        	return !$(this).val();
      	}).length;
      
      	if(totalVacios > 0){
        	$(".registro_obligado").each(function() {
          		var element = $(this);
          		if (element.val() == "") {
            		element.addClass("requerido");
            		$("#newModal #msj_texto").text("Hay campo(s) vacío(s)");
            		$("#newModal #msj_error").css('display','block');
		            setTimeout(function(){
		              $('#newModal #msj_error').fadeOut();
		            },4000);
          		}
          		else{
            		element.removeClass("requerido");
          		}
        	});
      	}
  		else{
			$.ajax({
              	url: '<?php echo base_url('Cliente/Actualizando...'); ?>',
              	type: 'POST',
              	data: datos,
              	contentType: false,  
     			cache: false,  
     			processData:false,
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		if(res == 0){
	                  	$('.loader').fadeOut();
              			$("#newModal #msj_texto").text("El candidato esta activo");
	            		$("#newModal #msj_error").css('display','block');
			            setTimeout(function(){
			              $('#newModal #msj_error').fadeOut();
			            },4000);
              		}
              		if(res == "guardado"){
          				setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
                		$("#newModal").modal('hide');
                		recargarTable();
                		$("#exito").css('display','block');
                		setTimeout(function(){
	                  		$('#exito').fadeOut();
	                	},4000);
              		}
              	}
        	});
		}
	}
	function registro(){
		$("#tituloNuevo").text("Agregar candidato");
		$("#guardar").val('nuevo');
		$("input, select").val("");
		var hoy = new Date();
  		var fecha = hoy.getDate() + '/' + ( hoy.getMonth() + 1 ) + '/' + hoy.getFullYear();
  		var hora = hoy.getHours() + ':' + hoy.getMinutes();
  		var fecha_actual = fecha+' '+hora;
  		$("#fecha_actual").prop('disabled',true);
  		$("#fecha_actual").val(fecha_actual);
		$("#newModal").modal('show');
	}
	function eliminarEstudio(idCandidato, idCliente, candidato){
		$("#idCandidato").val(idCandidato);
		$("#idCliente").val(idCliente);
		$("#titulo_accion").text("Eliminar candidato");
		$("#div_motivo").css('display','block');
		$("#finalizadosModal").modal("hide");
		$("#quitarModal").modal("show");
	}
	function estatusAvances(){
		var id_candidato = $("#idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_avances").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkAvances'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_avances").css('display','none');
        			}
        			$("#div_crearEstatusAvances").empty();
        			$("#idAvances").val(aux[1]);
        			$("#div_crearEstatusAvances").append(aux[0]);
        		}
        		else{
        			$("#idAvances").val(0);
        			$("#div_crearEstatusAvances").empty();
        			$("#div_crearEstatusAvances").html('<p class="text-center">Sin registros </p>');
        		}

      		}
    	});
		$("#avancesModal").modal("show");
	}
	function generarEstatusAvance(){
		var datos = new FormData();
		datos.append('id_candidato', $("#idCandidato").val());
		datos.append('id_avance', $("#idAvances").val());
		datos.append('comentario', $("#avances_estatus_comentario").val());
		datos.append('adjunto', $("#adjunto")[0].files[0]);
		$.ajax({
			url: '<?php echo base_url('Candidato/createEstatusAvance'); ?>',
			type: 'POST',
			data: datos,
			contentType: false,  
			cache: false,  
			processData:false,
      		success: function(res)
      		{
				$("#adjunto").val("");
      			var aux = res.split('@@');
      			$("#idAvances").val(aux[1]);
        		$("#avances_estatus_comentario").val("");
        		$("#div_crearEstatusAvances").empty();
        		$("#div_crearEstatusAvances").append(aux[0]);
      		}
    	});
	}
	function ejecutarAccion(){
		var id = $("#idCandidato").val();
		var id_cliente = $("#idCliente").val();
		var motivo = $("#motivo").val();
		var usuario = 1;
		if(motivo == ""){
			$("#quitarModal #msj_texto").text("Se requiere el motivo");
    		$("#quitarModal #msj_error").css('display','block');
            setTimeout(function(){
              $('#quitarModal #msj_error').fadeOut();
            },4000);
		}
		else{
			$.ajax({
              	url: '<?php echo base_url('Candidato/accionCandidato'); ?>',
              	type: 'post',
              	data: {'id':id,'motivo':motivo,'usuario':usuario,'id_cliente':id_cliente},
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("deleted", 1);
	            	location.reload();
              	}
        	});
		}		
	}
	function finalizarProceso(){
		var id_candidato = $("#idCandidato").val();
		var estatus = $("#resultado").val();
		var comentario = $("#comentario_final").val();
		if(estatus == ""){
			$("#concluirModal #campo_vacio").css('display','block');
            setTimeout(function(){
              $('#concluirModal #campo_vacio').fadeOut();
            },4000);
		}
		else{
			$.ajax({
	      		url: '<?php echo base_url('Candidato/finalizarProcesoCandidato'); ?>',
	      		method: 'POST',
	      		data: {'id_candidato':id_candidato,'estatus':estatus,'comentario':comentario},
	      		dataType: "text",
	      		beforeSend: function() {
                	$('.loader').css("display","block");
              	},
	      		success: function(res)
	      		{
	        		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("finished", 1);
	            	location.reload();
	      		}
	    	});
		}
	}
  	function regresar(){
  		$("#listado").css('display','block');
  		$("#btn_regresar").css('display','none'); 
  		$("#formulario").css('display','none');
  	}
  	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
	
</script>