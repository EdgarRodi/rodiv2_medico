<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div id="exito" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> Los documentos han sido actualizados correctamente.
  	</div>
  	<div id="exitoEstudios" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de estudios ha sido finalizada correctamente.
  	</div>
  	<div id="exitoLaborales" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de las referencias laborales ha sido finalizada correctamente.
  	</div>
  	<div id="exitoPenales" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de los antecedentes no penales ha sido finalizada correctamente.
  	</div>
  	<div id="exitoCandidato" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> Los datos del candidato han sido actualizados correctamente.
  	</div>
  	<div id="vacios" class="alert alert-danger fade in mensaje" style='display:none;'>
      	<strong>¡Atención!</strong><span id="txt_vacios"></span>
  	</div>
  	<div id="exitoFinalizado" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> El proceso del candidato ha finalizado correctamente.
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong><p id="texto_msj"></p>
  	</div>
	<section class="content-header">
      	<h1 class="titulo_seccion">Reclutamiento</h1>
    	<a class="btn btn-app" id="btn_regresar" onclick="regresar()" style="display: none;">
    		<i class="far fa-arrow-alt-circle-left"></i> <span>Regresar</span>
  		</a>
    </section>
    <div class="modal fade" id="docsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Documentación</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" class="idCandidato">
		      		<input type="hidden" class="prefijo">
		        	<div class="row">
		        		<div class="col-md-4 margen">
		        			<label>Selecciona el documento</label><br>
		        			
		        				<input type="file" id="documento" class="doc_obligado" name="documento" accept=".pdf, .jpg, .png, .jpeg"><br>
                            	<button class="btn btn-primary" onclick="subirDoc()">Subir</button>
		        			
		        		</div>
		        		<div id="tablaDocs" class="col-md-8 borde">
		        		
		        		</div>
		        		
		        	</div>
		        	<div class="row margen">
		        		<div id="campos_vacios">
			      			<p class="msj_error">No se ha seleccionado el tipo de archivo</p>
			      		</div>
			      		<div id="tipos_iguales">
			      			<p class="msj_error">Los documentos deben ser de diferente tipo</p>
			      		</div>
			      		<div id="sin_documento">
			      			<p class="msj_error">Seleccione un documento para subir</p>
			      		</div>
			      		<div id="subido">
			      			<p class="msj_error">El documento se ha subido con éxito</p>
			      		</div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-danger" onclick="actualizarDocs()">Actualizar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="llamadasModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Registro de llamadas al candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idLlamadas">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusLlamadas">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_llamadas">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_llamadas"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="llamadas_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="llamadas_estatus_comentario" id="llamadas_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusLlamada()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="emailsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Registro de correos enviados al candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<input type="hidden" id="idEmails">
		      		<div class="box-header tituloSubseccion">
              			<p class="box-title"><strong>  Anteriores:</strong></p>
            		</div>
		      		<div class="row" id="div_crearEstatusEmail">
		      			<p class="text-center">Sin registros </p>
		      		</div>
		      		<hr>
		        	<div class="margen" id="div_estatus_emails">
		        		<div class="box-header tituloSubseccion">
	              			<p class="box-title"><strong>  Nuevos:</strong></p>
	            		</div>
	        			<div class="row">
                            <div class="col-md-3">
                            	<p class="text-center"><b>Fecha</b></p>
                                <p class="text-center" id="fecha_estatus_emails"></p>
                            </div>
                            <div class="col-md-9">
                            	<label for="emails_estatus_comentario">Comentario / Estatus</label>
                                <textarea class="form-control" name="emails_estatus_comentario" id="emails_estatus_comentario" rows="3" placeholder="Comentario / Estatus"></textarea>
                                <br>
                            </div>

                        </div>
                        <div class="row">
                        	<div class="col-md-3 col-md-offset-5">
                        		<a class="btn btn-app btn_verificacion" onclick="generarEstatusEmail()">Actualizar estatus</a>
                        	</div>
                        </div>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<h4 id="nombre_candidato"></h4><br>
	        		<p class="" id="motivo"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      		<button type="button" class="btn btn-danger" id="btnGuardar" onclick="ejecutarAccion()">Accept</button>
		    	</div>
	  		</div>
		</div>
	</div>
    <div class="loader" style="display: none;"></div>
    <a href="#" class="scroll-to-top"><i class="fas fa-arrow-up"></i><span class="sr-only">Ir arriba</span></a>
	<section class="content" id="listado">
	  	<div class="row">
	        <div class="col-xs-12">
	          	<div class="box">
	            	<div class="box-header">
	              		<!--h3 class="box-title"><strong>  Propietarios</strong></h3-->
	            	</div>
	            	<!-- /.box-header -->
	            	<div class="box-body table-responsive">
	              		<table id="tabla" class="table stripe hover row-border cell-border"></table>
	            	</div>
	            	<!-- /.box-body -->
	          	</div>
	          	<!-- /.box -->
	        </div>
	    </div>
	</section>
	<section class="content" id="formulario" style="display: none;">
		<input type="hidden" class="idCandidato">
		<input type="hidden" class="correo">
		<div class="row">
    		<div class="col-xs-12">
      			<div class="box">
        			<div class="box-body">
        				<div class="box-header tituloSubseccion">
                  			<p class="box-title" id="titulo_personal"><strong>  Personal data</strong><hr></p>
                		</div>
                		<form id="d_personal">
	                		<div class="row">
				        		<div class="col-md-4">
				        			<label for="nombre">Name *</label>
				        			<input type="text" class="form-control personal_obligado" name="nombre" id="nombre" readonly>
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="paterno">First lastname *</label>
				        			<input type="text" class="form-control personal_obligado" name="paterno" id="paterno" readonly>
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="materno">Second lastname *</label>
				        			<input type="text" class="form-control personal_obligado" name="materno" id="materno" readonly>
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
				        		<div class="col-md-2">
				        			<label for="fecha_nacimiento">Date of birth *</label>
				        			<input type="text" class="form-control solo_lectura personal_obligado" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="mm/dd/yyyy" readonly>
				        			<br>
				        		</div>
				        		<div class="col-md-1">
				        			<label for="edad">Age *</label>
				        			<input type="text" class="form-control personal_obligado" id="edad" placeholder="Age" disabled>
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="puesto">Job Position Requested *</label>
				        			<input type="text" class="form-control personal_obligado" name="puesto" id="puesto" placeholder="Job Position Requested">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="nacionalidad">Nationality *</label>
				        			<input type="text" class="form-control personal_obligado" name="nacionalidad" id="nacionalidad" placeholder="Nationality">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="genero">Gender: *</label>
				        			<select name="genero" id="genero" class="form-control personal_obligado">
							            <option value="-1">Select</option>
							            <option value="1">Male</option>
							            <option value="2">Female</option>
						          	</select>
						          	<br>
				        		</div>		
					        </div>
				        	<div class="row">
					        	<div class="col-md-4">
				        			<label for="calle">Address *</label>
				        			<input type="text" class="form-control personal_obligado" name="calle" id="calle" placeholder="Address">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="exterior">Ext. Num. *</label>
				        			<input type="text" class="form-control solo_numeros personal_obligado" name="exterior" id="exterior" placeholder="Ext. Num" maxlength="6">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="interior">Int. Num. </label>
				        			<input type="text" class="form-control" name="interior" id="interior" placeholder="Int. Num." maxlength="5">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="colonia">Neighborhood *</label>
				        			<input type="text" class="form-control personal_obligado" name="colonia" id="colonia" placeholder="Neighborhood">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-4">
				        			<label for="estado">State *</label>
				        			<select name="estado" id="estado" class="form-control personal_obligado">
							            <option value="-1">Select</option>
							            <?php foreach ($estados as $e) {?>
							                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="municipio">City *</label>
				        			<select name="municipio" id="municipio" class="form-control personal_obligado" disabled>
							            <option value="-1">Select</option>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="cp">Zip Code *</label>
				        			<input type="text" class="form-control solo_numeros personal_obligado" name="cp" id="cp" placeholder="Zip Code" maxlength="5">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-3">
				        			<label for="civil">Marital Status *</label>
				        			<select name="civil" id="civil" class="form-control personal_obligado">
							            <option value="-1">Select</option>
							            <option value="1">Married</option>
							            <option value="2">Single</option>
							            <option value="3">Divorced</option>
							            <option value="4">Union free</option>
							            <option value="5">Widowed</option>
							            <option value="6">Separated</option>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-3">
				        			<label for="telefono">Mobile Number *</label>
				        			<input type="text" class="form-control solo_numeros personal_obligado" name="telefono" id="telefono" placeholder="Mobile Number" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
				        			<label for="tel_casa">Home Number </label>
				        			<input type="text" class="form-control solo_numeros" name="tel_casa" id="tel_casa" placeholder="Home Number" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
				        			<label for="tel_otro">Number to leave Messages </label>
				        			<input type="text" class="form-control solo_numeros" name="tel_otro" id="tel_otro" placeholder="Number to leave Messages" maxlength="10">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-3">
					        		<label for="anos_direccion">Years in current address *</label>
				        			<input type="text" class="form-control solo_numeros" name="anos_direccion" id="anos_direccion"  maxlength="2" readonly>
				        			<br>
					        	</div>
					        	<div class="col-md-3">
					        		<label for="meses_direccion">Months in current address *</label>
				        			<select name="meses_direccion" id="meses_direccion" class="form-control" readonly>
							            <option value="-1">Select</option>
							            <?php for ($i = 0; $i <= 11; $i++) {?>
							                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-3">
					        		<label for="horas_traslado">Transit hours to office   *</label>
				        			<select name="horas_traslado" id="horas_traslado" class="form-control" readonly>
							            <option value="-1">Select</option>
							            <?php for ($i = 0; $i <= 5; $i++) {?>
							                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-3">
					        		<label for="minutos_traslado">Transit minutes to office   *</label>
				        			<select name="minutos_traslado" id="minutos_traslado" class="form-control" readonly>
							            <option value="-1">Select</option>
							            <?php for ($i = 0; $i <= 59; $i++) {?>
							                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-3">
				        			<label for="transporte">Transportation mode *</label>
				        			<select name="transporte" id="transporte" class="form-control" readonly>
							            <option value="-1">Select</option>
							            <option value="1">Public transport</option>
							            <option value="2">Own car</option>
							            <option value="3">Carpool</option>
							            <option value="4">Motorcycle</option>
							            <option value="5">Bike</option>
							            <option value="6">No vehicle</option>
						          	</select>
				        			<br>
				        		</div>
					        	<div class="col-md-3">
				        			<label for="correo">Email *</label>
				        			<input type="text" class="form-control personal_obligado" name="correo" id="correo" readonly>
				        			<br>
				        		</div>
					        </div>
					    </form>
					    <form id="d_documentos">
					        <div class="box-header tituloSubseccion">
	                  			<p class="box-title"><strong>  Documents</strong><hr></p>
	                		</div>
	                		<p class="tituloSubseccion">Professional licence</p>
	                		<div class="row">
	                			<div class="col-md-3" id="doc_estudios"></div>
	                			<div class="col-md-2 col-md-offset-2">
	                				<label for="lic_profesional">License Number </label>
				        			<input type="text" class="form-control solo_numeros" name="lic_profesional" id="lic_profesional" placeholder="License Number" maxlength="12" readonly>
				        			<br>
	                			</div>
	                			<div class="col-md-4">
	                				<label for="lic_institucion">Date / Institution </label>
	                				<textarea class="form-control" name="lic_institucion" id="lic_institucion" rows="2" readonly>Secretaría de Educación Pública </textarea>
	                				<br>
	                			</div>
	                		</div>
	                		<p class="tituloSubseccion">ID (IFE, INE or Passport)</p>
	                		<div class="row">
	                			<div class="col-md-3" id="doc_ine"></div>
	                			<div class="col-md-2 col-md-offset-2">
	                				<label for="ine_clave">ID  *</label>
				        			<input type="text" class="form-control docs_obligado" name="ine_clave" id="ine_clave" placeholder="ID" maxlength="18" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()" readonly>
				        			<br>
	                			</div>
	                			<div class="col-md-2">
	                				<label for="ine_registro">Register Year </label>
				        			<input type="text" class="form-control solo_numeros" name="ine_registro" id="ine_registro" placeholder="Register Year" maxlength="4" readonly>
				        			<br>
	                			</div>
	                			<div class="col-md-2">
	                				<label for="ine_vertical">Vertical number </label>
				        			<input type="text" class="form-control solo_numeros" name="ine_vertical" id="ine_vertical" placeholder="Vertical number" maxlength="13" readonly>
				        			<br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-4 col-md-offset-5">
	                				<label for="ine_institucion">Date / Institution  *</label>
	                				<textarea class="form-control docs_obligado" name="ine_institucion" id="ine_institucion" rows="2" placeholder="Date / Institution" readonly></textarea>
	                				<br>
	                			</div>
	                		</div>
	                		<p class="tituloSubseccion">Non-criminal background letter</p>     		
	                		<div class="row">
	                			<div class="col-md-3" id="doc_penales"></div>
	                			<div class="col-md-2 col-md-offset-2">
	                				<label for="penales_numero">Document number  *</label>
				        			<input type="text" class="form-control solo_numeros docs_obligado" name="penales_numero" id="penales_numero" placeholder="Document number" maxlength="12" readonly>
				        			<br>
	                			</div>
	                			<div class="col-md-4 ">
	                				<label for="penales_institucion">Date / Institution  *</label>
	                				<textarea class="form-control docs_obligado" name="penales_institucion" id="penales_institucion" rows="2" placeholder="Date / Institution" readonly></textarea>
	                				<br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-12">
	                				<label for="doc_comentarios">Comments  *</label>
	                				<textarea class="form-control docs_obligado" name="doc_comentarios" id="doc_comentarios" rows="2" placeholder="Comments"></textarea>
	                				<br>
	                			</div>
	                		</div>
	                	</form>
	                	<form id="d_familia">
	                		<div class="box-header tituloSubseccion">
	                  			<p class="box-title" id="titulo_familia"><strong>  Family Environment </strong><hr></p>
	                		</div>
	                		<div id="div_familiares">
	                			
	                		</div>
	                	</form>
	                	<form id="d_estudios">
	                		<div class="box-header tituloSubseccion">
	                  			<p class="box-title" id="titulo_estudios"><strong>  Studies Record </strong><hr></p>
	                		</div>
	                		<input type="hidden" id="idEstudios">
	                		<p class="tituloSubseccion">Elementary School</p>
							<div class="row">
								<div class="col-md-2">
									<label for="prim_periodo">Period *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prim_periodo" id="prim_periodo" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="prim_escuela">Institute *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prim_escuela" id="prim_escuela" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="prim_ciudad">City *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prim_ciudad" id="prim_ciudad" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="prim_certificado">Certificate Obtained *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prim_certificado" id="prim_certificado" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="prim_validado">Validated *</label>
				        			<select name="prim_validado" id="prim_validado" class="form-control">
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
								</div>
							</div>
							<p class="tituloSubseccion">Middle School</p>
							<div class="row">
								<div class="col-md-2">
									<label for="sec_periodo">Period *</label>
				        			<input type="text" class="form-control estudios_obligado" name="sec_periodo" id="sec_periodo" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="sec_escuela">Institute *</label>
				        			<input type="text" class="form-control estudios_obligado" name="sec_escuela" id="sec_escuela" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="sec_ciudad">City *</label>
				        			<input type="text" class="form-control estudios_obligado" name="sec_ciudad" id="sec_ciudad" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="sec_certificado">Certificate Obtained *</label>
				        			<input type="text" class="form-control estudios_obligado" name="sec_certificado" id="sec_certificado" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="sec_validado">Validated *</label>
				        			<select name="sec_validado" id="sec_validado" class="form-control">
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
								</div>
							</div>
							<p class="tituloSubseccion">High School</p>
							<div class="row">
								<div class="col-md-2">
									<label for="prep_periodo">Period *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prep_periodo" id="prep_periodo" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="prep_escuela">Institute *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prep_escuela" id="prep_escuela" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="prep_ciudad">City *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prep_ciudad" id="prep_ciudad" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="prep_certificado">Certificate Obtained *</label>
				        			<input type="text" class="form-control estudios_obligado" name="prep_certificado" id="prep_certificado" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="prep_validado">Validated *</label>
				        			<select name="prep_validado" id="prep_validado" class="form-control">
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
								</div>
							</div>
							<p class="tituloSubseccion">College</p>
							<div class="row">
								<div class="col-md-2">
									<label for="lic_periodo">Period *</label>
				        			<input type="text" class="form-control estudios_obligado" name="lic_periodo" id="lic_periodo" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="lic_escuela">Institute *</label>
				        			<input type="text" class="form-control estudios_obligado" name="lic_escuela" id="lic_escuela" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-3">
									<label for="lic_ciudad">City *</label>
				        			<input type="text" class="form-control estudios_obligado" name="lic_ciudad" id="lic_ciudad" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="lic_certificado">Certificate Obtained *</label>
				        			<input type="text" class="form-control estudios_obligado" name="lic_certificado" id="lic_certificado" placeholder="Name" >
				        			<br>
								</div>
								<div class="col-md-2">
									<label for="lic_validado">Validated *</label>
				        			<select name="lic_validado" id="lic_validado" class="form-control">
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="otro_certificado">Seminaries/Courses Certificates *</label>
				        			<textarea class="form-control estudios_obligado" name="otro_certificado" id="otro_certificado" rows="3" placeholder="Seminaries/Courses Certificates" ></textarea>
				        			<br>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="estudios_comentarios">Comments *</label>
				        			<textarea class="form-control" name="estudios_comentarios" id="estudios_comentarios" rows="3" placeholder="Comments"></textarea>
				        			<br>
								</div>
							</div>
							<div class="box-header tituloSubseccion">
	                  			<p class="box-title"><strong>Break(s) in Studies *</strong><hr></p>
	                		</div>
	                		<div class="row">
								<div class="col-md-12">
				        			<textarea class="form-control estudios_obligado" name="carrera_inactivo" id="carrera_inactivo" rows="3" placeholder="Period and motive"></textarea>
		        				<br>
								</div>
							</div>
	                	</form>
	                	<form id="d_refLaboral">
	                		<div class="box-header tituloSubseccion">
	                  			<p class="box-title"><strong>  Labor References </strong></p>
	                		</div>
	                		<div class="box-header text-center tituloSubseccion">
	                  			<p class="box-title "><strong>  First reference </strong><hr></p>
	                  			<input type="hidden" id="idreflab1">
	                		</div>
	                		<p class="tituloSubseccion text-center"></p>
	                		<div class="col-md-6">
	                			<p class="tituloSubseccion">Candidate</p>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Company: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_empresa"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Address: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_direccion"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Entry Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_entrada"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Exit Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_salida"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Phone: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_telefono"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_puesto1"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_puesto2"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_salario1"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_salario2"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Name </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_bossnombre"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Email </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_bosscorreo"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_bosspuesto"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Cause of Separation </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab1_separacion"></p>
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		<div class="col-md-6">
	                			<div class="row">
	                				<p class="tituloSubseccion text-center">Analist</p>
	                				<div class="col-md-3">
		                				<label>Company: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control" name="an_refLab1_empresa" id="an_refLab1_empresa" placeholder="Company">
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Address: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab1_direccion" id="an_refLab1_direccion" placeholder="Address">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Entry Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_lectura" name="an_refLab1_entrada" id="an_refLab1_entrada" placeholder="mm/dd/yyyy" readonly>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Exit Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_lectura" name="an_refLab1_salida" id="an_refLab1_salida" placeholder="mm/dd/yyyy" readonly>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Phone: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros" name="an_refLab1_telefono" id="an_refLab1_telefono" placeholder="Phone" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab1_puesto1" id="an_refLab1_puesto1" placeholder="Initial Job Position">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab1_puesto2" id="an_refLab1_puesto2" placeholder="Last Job Position">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros" name="an_refLab1_salario1" id="an_refLab1_salario1" placeholder="Initial Salary" maxlength="8">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros" name="an_refLab1_salario2" id="an_refLab1_salario2" placeholder="Initial Salary" maxlength="8">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Name </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab1_bossnombre" id="an_refLab1_bossnombre" placeholder="Immediate Boss Name">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Email </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab1_bosscorreo" id="an_refLab1_bosscorreo" placeholder="Immediate Boss Email">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab1_bosspuesto" id="an_refLab1_bosspuesto" placeholder="Boss's Job Position">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Cause of Separation </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab1_separacion" id="an_refLab1_separacion" placeholder="Cause of Separation">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-9">
	                				<label>Notes *</label>
	                				<textarea class="form-control obligado" name="an_refLab1_notas" id="an_refLab1_notas" rows="3" placeholder="Notes"></textarea>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_demanda">Did the candidate sue the company *</label>
				        			<select name="an_refLab1_demanda" id="an_refLab1_demanda" class="form-control obligado">
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<br><p class="tituloSubseccion text-center">Candidate Performance</p><br>
	                		<div class="row">
	                			<div class="col-md-4 col-md-offset-4">
	                				<label for="aplicar_todo">Apply to all</label>
				        			<select id="aplicar_todo" class="form-control">
							            <option value="-1">Select</option>
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-3">
	                				<label for="an_refLab1_responsabilidad">Responsability *</label>
				        			<select name="an_refLab1_responsabilidad" id="an_refLab1_responsabilidad" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_iniciativa">Initiative *</label>
				        			<select name="an_refLab1_iniciativa" id="an_refLab1_iniciativa" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_eficiencia">Work efficiency *</label>
				        			<select name="an_refLab1_eficiencia" id="an_refLab1_eficiencia" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_disciplina">Discipline *</label>
				        			<select name="an_refLab1_disciplina" id="an_refLab1_disciplina" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-3">
	                				<label for="an_refLab1_puntualidad">Punctuality and assistance *</label>
				        			<select name="an_refLab1_puntualidad" id="an_refLab1_puntualidad" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_limpieza">Cleanliness and order *</label>
				        			<select name="an_refLab1_limpieza" id="an_refLab1_limpieza" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_estabilidad">Stability *</label>
				        			<select name="an_refLab1_estabilidad" id="an_refLab1_estabilidad" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_emocional">Emotional Stability *</label>
				        			<select name="an_refLab1_emocional" id="an_refLab1_emocional" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-3">
	                				<label for="an_refLab1_honesto">Honesty *</label>
				        			<select name="an_refLab1_honesto" id="an_refLab1_honesto" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab1_rendimiento">Performance *</label>
				        			<select name="an_refLab1_rendimiento" id="an_refLab1_rendimiento" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-4">
	                				<label for="an_refLab1_actitud">Attitude with coworkers, bosses and subordinates *</label>
				        			<select name="an_refLab1_actitud" id="an_refLab1_actitud" class="form-control performance obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<div class="row">
                				<div class="col-md-4">
                					<label for="an_refLab1_recontratacion">In case of vacancy would you hire her/him again? *</label>
				        			<select name="an_refLab1_recontratacion" id="an_refLab1_recontratacion" class="form-control obligado">
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
                				</div>
                				<div class="col-md-8">
                					<label>Why? *</label>
                					<textarea class="form-control obligado" name="an_refLab1_motivo" id="an_refLab1_motivo" rows="3" placeholder="Why?" ></textarea>
                					<br>
                				</div>
                			</div>
	                		<div class="box-header text-center margen tituloSubseccion">
	                  			<p class="box-title "><strong>  Second reference </strong><hr></p>
	                  			<input type="hidden" id="idreflab2">
	                		</div>
	                		<p class="tituloSubseccion text-center"></p>
	                		<div class="col-md-6">
	                			<p class="tituloSubseccion">Candidate</p>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Company: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_empresa"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Address: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_direccion"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Entry Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_entrada"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Exit Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_salida"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Phone: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_telefono"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_puesto1"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_puesto2"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_salario1"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_salario2"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Name </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_bossnombre"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Email </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_bosscorreo"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_bosspuesto"></p>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Cause of Separation </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<p id="refLab2_separacion"></p>
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		<div class="col-md-6">
	                			<div class="row">
	                				<p class="tituloSubseccion text-center">Analist</p>
	                				<div class="col-md-3">
		                				<label>Company: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
					        			<input type="text" class="form-control" name="an_refLab2_empresa" id="an_refLab2_empresa" placeholder="Company">
					        			<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Address: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab2_direccion" id="an_refLab2_direccion" placeholder="Address">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Entry Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_lectura" name="an_refLab2_entrada" id="an_refLab2_entrada" placeholder="mm/dd/yyyy" readonly>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Exit Date: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_lectura" name="an_refLab2_salida" id="an_refLab2_salida" placeholder="mm/dd/yyyy" readonly>
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Phone: </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros" name="an_refLab2_telefono" id="an_refLab2_telefono" placeholder="Phone" maxlength="10">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab2_puesto1" id="an_refLab2_puesto1" placeholder="Initial Job Position">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Job Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab2_puesto2" id="an_refLab2_puesto2" placeholder="Last Job Position">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Initial Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros" name="an_refLab2_salario1" id="an_refLab2_salario1" placeholder="Initial Salary" maxlength="8">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Last Salary </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control solo_numeros" name="an_refLab2_salario2" id="an_refLab2_salario2" placeholder="Initial Salary" maxlength="8">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Name </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab2_bossnombre" id="an_refLab2_bossnombre" placeholder="Immediate Boss Name">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Email </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab2_bosscorreo" id="an_refLab2_bosscorreo" placeholder="Immediate Boss Email">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Immediate Boss Position </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab2_bosspuesto" id="an_refLab2_bosspuesto" placeholder="Boss's Job Position">
		        						<br>
		                			</div>
	                			</div>
	                			<div class="row">
	                				<div class="col-md-3">
		                				<label>Cause of Separation </label>
		                				<br>
		                			</div>
		                			<div class="col-md-9">
		                				<input type="text" class="form-control" name="an_refLab2_separacion" id="an_refLab2_separacion" placeholder="Cause of Separation">
		        						<br>
		                			</div>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-9">
	                				<label>Notes *</label>
	                				<textarea class="form-control obligado" name="an_refLab2_notas" id="an_refLab2_notas" rows="3" placeholder="Notes"></textarea>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_demanda">Did the candidate sue the company *</label>
				        			<select name="an_refLab2_demanda" id="an_refLab2_demanda" class="form-control obligado">
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<br><p class="tituloSubseccion text-center">Candidate Performance</p><br>
	                		<div class="row">
	                			<div class="col-md-4 col-md-offset-4">
	                				<label for="aplicar_todo2">Apply to all</label>
				        			<select id="aplicar_todo2" class="form-control">
							            <option value="-1">Select</option>
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br><br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-3">
	                				<label for="an_refLab2_responsabilidad">Responsability *</label>
				        			<select name="an_refLab2_responsabilidad" id="an_refLab2_responsabilidad" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_iniciativa">Initiative *</label>
				        			<select name="an_refLab2_iniciativa" id="an_refLab2_iniciativa" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_eficiencia">Work efficiency *</label>
				        			<select name="an_refLab2_eficiencia" id="an_refLab2_eficiencia" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_disciplina">Discipline *</label>
				        			<select name="an_refLab2_disciplina" id="an_refLab2_disciplina" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-3">
	                				<label for="an_refLab2_puntualidad">Punctuality and assistance *</label>
				        			<select name="an_refLab2_puntualidad" id="an_refLab2_puntualidad" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_limpieza">Cleanliness and order *</label>
				        			<select name="an_refLab2_limpieza" id="an_refLab2_limpieza" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_estabilidad">Stability *</label>
				        			<select name="an_refLab2_estabilidad" id="an_refLab2_estabilidad" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_emocional">Emotional Stability *</label>
				        			<select name="an_refLab2_emocional" id="an_refLab2_emocional" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-3">
	                				<label for="an_refLab2_honesto">Honesty *</label>
				        			<select name="an_refLab2_honesto" id="an_refLab2_honesto" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-3">
	                				<label for="an_refLab2_rendimiento">Performance *</label>
				        			<select name="an_refLab2_rendimiento" id="an_refLab2_rendimiento" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                			<div class="col-md-4">
	                				<label for="an_refLab2_actitud">Attitude with coworkers, bosses and subordinates *</label>
				        			<select name="an_refLab2_actitud" id="an_refLab2_actitud" class="form-control performance2 obligado">
							            <option value="0">Not provided</option>
							            <option value="1">Excellent</option>
							            <option value="2">Good</option>
							            <option value="3">Regular</option>
							            <option value="4">Bad</option>
							            <option value="5">Very Bad</option>
						          	</select>
				        			<br>
	                			</div>
	                		</div>
	                		<div class="row">
                				<div class="col-md-4">
                					<label for="an_refLab2_recontratacion">In case of vacancy would you hire her/him again? *</label>
				        			<select name="an_refLab2_recontratacion" id="an_refLab2_recontratacion" class="form-control obligado">
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
				        			<br>
                				</div>
                				<div class="col-md-8">
                					<label>Why? *</label>
                					<textarea class="form-control obligado" name="an_refLab2_motivo" id="an_refLab2_motivo" rows="3" placeholder="Why?" ></textarea>
                				</div>
                			</div>
	                		<div class="box-header margen tituloSubseccion">
	                  			<p class="box-title" id="titulo_break"><strong>Break(s) in Employment  *</strong><hr></p>
	                		</div>
	                		<div class="row">
								<div class="col-md-12">
				        			<textarea class="form-control obligado" name="trabajo_inactivo" id="trabajo_inactivo" rows="3" placeholder="Period, Reason and Activities"></textarea>
		        					<br>
								</div>
							</div>
						</form>
						<form id="d_refPersonal">
							<div class="box-header margen tituloSubseccion">
	                  			<p class="box-title" id="titulo_refpersonales"><strong>  Personal References  </strong><hr></p>
	                		</div>
	                		<p class="tituloSubseccion">First reference</p>
	                		<div class="row">
								<div class="col-md-4">
									<label for="refPer1_nombre">Name *</label>
				        			<input type="text" class="form-control refPer_obligado" name="refPer1_nombre" id="refPer1_nombre" placeholder="Name">
				        			<br>
								</div>
								<div class="col-md-2">
				        			<label for="refPer1_telefono">Phone *</label>
				        			<input type="text" class="form-control solo_numeros refPer_obligado" name="refPer1_telefono" id="refPer1_telefono" placeholder="Phone" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
					        		<label for="refPer1_tiempo">Time to know her/him *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="refPer1_tiempo" id="refPer1_tiempo" placeholder="Time to know her/him" disabled>
				        			<br>
					        	</div>
					        	<div class="col-md-3">
					        		<label for="refPer1_conocido">Why do you know her/him *</label>
				        			<input type="text" class="form-control refPer_obligado" name="refPer1_conocido" id="refPer1_conocido" placeholder="Why do you know her/him">
				        			<br>
					        	</div>
							</div>
							<div class="row">
								<div class="col-md-6">
					        		<label for="refPer1_sabetrabajo">Does she/he know where the candidate works/worked? *</label>
				        			<select name="refPer1_sabetrabajo" id="refPer1_sabetrabajo" class="form-control obligado" disabled>
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-6">
					        		<label for="refPer1_sabevive">Does she/he know where the candidate lives? *</label>
				        			<select name="refPer1_sabevive" id="refPer1_sabevive" class="form-control obligado" disabled>
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
						          	<br>
					        	</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="hidden" id="refPer1_id">
									<label for="refPer1_comentario">Comments *</label>
				        			<input type="text" class="form-control obligado" name="refPer1_comentario" id="refPer1_comentario">
				        			<br>
								</div>
							</div>
							<br>
							<p class="tituloSubseccion">Second reference</p>
							<div class="row">
								<div class="col-md-4">
									<label for="refPer2_nombre">Name *</label>
				        			<input type="text" class="form-control refPer_obligado" name="refPer2_nombre" id="refPer2_nombre" placeholder="Name">
				        			<br>
								</div>
								<div class="col-md-2">
				        			<label for="refPer2_telefono">Phone *</label>
				        			<input type="text" class="form-control solo_numeros refPer_obligado" name="refPer2_telefono" id="refPer2_telefono" placeholder="Phone" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
					        		<label for="refPer2_tiempo">Time to know her/him *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="refPer2_tiempo" id="refPer2_tiempo" placeholder="Time to know her/him" disabled>
				        			<br>
					        	</div>
					        	<div class="col-md-3">
					        		<label for="refPer2_conocido">Why do you know her/him *</label>
				        			<input type="text" class="form-control refPer_obligado" name="refPer2_conocido" id="refPer2_conocido" placeholder="Why do you know her/him">
				        			<br>
					        	</div>
							</div>
							<div class="row">
								<div class="col-md-6">
					        		<label for="refPer2_sabetrabajo">Does she/he know where the candidate works/worked? *</label>
				        			<select name="refPer2_sabetrabajo" id="refPer2_sabetrabajo" class="form-control obligado" disabled>
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-6">
					        		<label for="refPer2_sabevive">Does she/he know where the candidate lives? *</label>
				        			<select name="refPer2_sabevive" id="refPer2_sabevive" class="form-control obligado" disabled>
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
						          	<br>
					        	</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="hidden" id="refPer2_id">
									<label for="refPer2_comentario">Comments *</label>
				        			<input type="text" class="form-control obligado" name="refPer2_comentario" id="refPer2_comentario">
				        			<br>
								</div>
							</div>
							<br>
							<p class="tituloSubseccion">Third reference</p>
							<div class="row">
								<div class="col-md-4">
									<label for="refPer3_nombre">Name *</label>
				        			<input type="text" class="form-control refPer_obligado" name="refPer3_nombre" id="refPer3_nombre" placeholder="Name">
				        			<br>
								</div>
								<div class="col-md-2">
				        			<label for="refPer3_telefono">Phone *</label>
				        			<input type="text" class="form-control solo_numeros refPer_obligado" name="refPer3_telefono" id="refPer3_telefono" placeholder="Phone" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
					        		<label for="refPer3_tiempo">Time to know her/him *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="refPer3_tiempo" id="refPer3_tiempo" placeholder="Time to know her/him" disabled>
				        			<br>
					        	</div>
					        	<div class="col-md-3">
					        		<label for="refPer3_conocido">Why do you know her/him *</label>
				        			<input type="text" class="form-control refPer_obligado" name="refPer3_conocido" id="refPer3_conocido" placeholder="Why do you know her/him">
				        			<br>
					        	</div>
							</div>
							<div class="row">
								<div class="col-md-6">
					        		<label for="refPer3_sabetrabajo">Does she/he know where the candidate works/worked? *</label>
				        			<select name="refPer3_sabetrabajo" id="refPer3_sabetrabajo" class="form-control obligado" disabled>
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-6">
					        		<label for="refPer3_sabevive">Does she/he know where the candidate lives? *</label>
				        			<select name="refPer3_sabevive" id="refPer3_sabevive" class="form-control obligado" disabled>
							            <option value="-1">Select</option>
							            <option value="0">No</option>
							            <option value="1">Yes</option>
						          	</select>
						          	<br>
					        	</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="hidden" id="refPer3_id">
									<label for="refPer3_comentario">Comments *</label>
				        			<input type="text" class="form-control obligado" name="refPer3_comentario" id="refPer3_comentario">
				        			<br>
								</div>
							</div>
							<div class="box-header margen tituloSubseccion">
	                  			<p class="box-title"><strong>  Have you worked in any government entity, Politic Party or NGO? </strong></p>
	                		</div>
							<div class="row">
								<div class="col-lg-12">
				        			<input type="text" class="form-control refPer_obligado" name="trabajo_gobierno" id="trabajo_gobierno" placeholder="Have you worked in any government entity, Politic Party or NGO?">
				        			<br>
								</div>
							</div>
	                	</form>
	                	<form id="d_images">
	                		<div class="box-header margen tituloSubseccion">
	                  			<p class="box-title"><strong>  Add other verifications </strong></p>
	                		</div>
	                		<br>
	                		<div id="div_tipos_docs">
	                			
	                		</div>
	                	</form>
                		<div class="row">
                			<div class="col-md-4 col-md-offset-5 margen-top">
                				<a href="javascript:void(0)" class="btn btn-primary btnUpdate" onclick="actualizarCandidato()">Guardar Todo</a>
                			</div>
                		</div>
        			</div>
        		</div>
        	</div>
        </div>
	</section>
	
</div>
<!-- /.content-wrapper -->	
<script>
	var url = '<?php echo base_url('Reclutamiento/getRequisiciones'); ?>';
  	$(document).ready(function(){
  		var msj = localStorage.getItem("success");
  		var msj2 = localStorage.getItem("finished");
  		if(msj == 1){
  			$("#exitoCandidato").css('display','block');
  			setTimeout(function(){
          		$('#exitoCandidato').fadeOut();
        	},4000);
        	localStorage.removeItem("success");
  		}
  		if(msj2 == 1){
  			$("#exitoFinalizado").css('display','block');
  			setTimeout(function(){
          		$('#exitoFinalizado').fadeOut();
        	},4000);
        	localStorage.removeItem("finished");
  		}
    	$('#tabla').DataTable({
      		"pageLength": 25,
	      	"pagingType": "simple",
	      	//"stateSave": true,
	      	"ajax": url,
	      	"columns":[ 
	        	{ title: 'Cliente', data: 'cliente' },
	        	{ title: 'Fecha de alta', data: 'creacion', "width": "12%",
	        		mRender: function(data, type, full){
            			var f = data.split(' ');
            			var h = f[1];
            			var aux = h.split(':');
            			var hora = aux[0]+':'+aux[1];
            			var aux = f[0].split('-');
            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
            			var tiempo = fecha+' '+hora;
        				return tiempo;
          			}
	        	},
	        	{ title: 'Posición', data: 'puesto' },
	        	{ title: 'Vacantes', data: 'numero_vacantes',"width": "3%"},
	        	{ title: 'Causa vacante', data: 'causa',"width": "12%"},
	        	{ title: 'Jornada', data: 'jornada',"width": "10%"},
	        	{ title: 'Edad', data: 'edad_minima', "width": "10%",
	        		mRender: function(data, type, full){
        				return "De "+data+" a "+full.edad_maxima+" años";
          			}
	        	},
	        	{ title: 'Tipo sueldo', data: 'id_tipo_sueldo',"width": "12%",
	        		mRender: function(data, type, full){
	        			if(data == 3){
	        				return full.sueldo_variable+" de $"+full.variable_cantidad;
	        			}
	        			else{
	        				return full.sueldo;
	        			}
          			}
	        	},
	        	{ title: 'Sueldo min - max', data: 'sueldo_minimo',"width": "10%",
	        		mRender: function(data, type, full){
	        			return "$"+data+" a $"+full.sueldo_maximo;
          			}
	        	},
	        	//{ title: 'Sueldo', data: 'sueldo' },
	        	/*
	        	{ title: 'Estatus formulario', data: 'status',
	        		mRender: function(data){
	        			var res = (data == 0) ? "<i class='fas fa-circle estatus0'></i>Formulario sin contestar" : "<i class='fas fa-circle estatus1'></i>Formulario contestado";
	        			return res;
	        		} 
	        	},*/
	        	    
	      	],
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$("a#subirDocs", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$(".prefijo").val(data.id+"_"+data.nombre+""+data.paterno);
	      			$.ajax({
			            url: '<?php echo base_url('index.php/Candidato/getDocs'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'prefijo':data.id+"_"+data.nombre+""+data.paterno},
			            success : function(res)
			            { 
			            	$("#tablaDocs").html(res);

			            },error: function(res)
			            {
			              
			            }
			        });
	      			$("#docsModal").modal("show");
	      		});
		        $("a#verificar", row).bind('click', () => {
		        	$(".idCandidato").val(data.id);		        	
		        	//Datos personales
		        	$("#nombre").val(data.nombre);
		        	$("#paterno").val(data.paterno);
		        	$("#materno").val(data.materno);
		        	var f = data.fecha_nacimiento.split(' ');
		        	var aux = f[0].split('-');
		        	var f_nacimiento = aux[1]+'/'+aux[2]+'/'+aux[0];
		        	$("#fecha_nacimiento").val(f_nacimiento);
		        	$("#edad").val(data.edad);
		        	$("#puesto").val(data.puesto);
		        	$("#nacionalidad").val(data.nacionalidad);
		        	$("#genero").val(data.id_genero);
		        	$("#calle").val(data.calle);
		        	$("#exterior").val(data.exterior);
		        	$("#interior").val(data.interior);
		        	$("#colonia").val(data.colonia);
		        	$("#estado").val(data.id_estado);
		        	if(data.id_estado != "" && data.id_estado != null && data.id_estado != 0){
		        		getMunicipio(data.id_estado, data.id_municipio);
		            }
		            else{
		              $('#municipio').prop('disabled', true);
		            }
		            $("#cp").val(data.cp);
		        	$("#civil").val(data.id_estado_civil);
		        	$("#telefono").val(data.celular);
		        	$("#tel_casa").val(data.telefono_casa);
		        	$("#tel_otro").val(data.telefono_otro);
		        	$("#anos_direccion").val(data.anos_direccion);
		        	$("#meses_direccion").val(data.meses_direccion);
		        	$("#horas_traslado").val(data.horas_traslado);
		        	$("#minutos_traslado").val(data.minutos_traslado);
		        	$("#transporte").val(data.id_transporte);
		        	$("#correo").val(data.correo);
		        	//Documents
		        	var href = '<?php echo base_url()."_docs/"; ?>'
		        	$.ajax({
	                	url: '<?php echo base_url('index.php/Candidato/checkDocumentos'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                  		if(res != 0){
	                  			$("#doc_estudios").empty();
	                  			$("#doc_ine").empty();
	                  			var salida = res.split('@@');
	                  			for(var i = 0; i < salida.length; i++){
	                  				var aux = salida[i].split(',');
	                  				if(aux[0] == 7 || aux[0] == 10){
	                  					$("#doc_estudios").append("<a href='"+href+aux[1]+"' target='_blank'>"+aux[1]+"</a><br><br>");
	                  					$("#lic_profesional").prop('readonly', false);
	                  					$("#lic_institucion").prop('readonly', false);
	                  				}
	                  				
	                  				if(aux[0] == 3 || aux[0] == 14 || aux[0] == 15){
	                  					$("#doc_ine").append("<a href='"+href+aux[1]+"' target='_blank'>"+aux[1]+"</a>");
	                  					$("#ine_clave").prop('readonly', false);
	                  					$("#ine_registro").prop('readonly', false);
	                  					$("#ine_vertical").prop('readonly', false);
	                  					$("#ine_institucion").prop('readonly', false);
	                  				}
	                  				
	                  				if(aux[0] == 12){
	                  					$("#doc_penales").append("<a href='"+href+aux[1]+"' target='_blank'>"+aux[1]+"</a>");
	                  					$("#penales_numero").prop('readonly', false);
	                  					$("#penales_institucion").prop('readonly', false);
	                  				}
	                  				
	                  			}
	                  		}
	                	},error:function(res){
	                  		$('#errorModal').modal('show');
	                	}
	              	});
	              	$.ajax({
	                	url: '<?php echo base_url('index.php/Candidato/checkVerificacionDocumentos'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                  		if(res != 0){
	                  			var data = res.split('@@');
	                  			$("#lic_profesional").val(data[0]);
	                  			$("#lic_institucion").val(data[1]);
	                  			$("#ine_clave").val(data[2]);
	                  			$("#ine_registro").val(data[3]);
	                  			$("#ine_vertical").val(data[4]);
	                  			$("#ine_institucion").val(data[5]);
	                  			$("#penales_numero").val(data[6]);
	                  			$("#penales_institucion").val(data[7]);
	                  			$("#doc_comentarios").val(data[8]);
	                  		}
	                	},error:function(res){
	                  		//$('#errorModal').modal('show');
	                	}
	              	});
	              	//Familiares
	              	$.ajax({
	                	url: '<?php echo base_url('index.php/Candidato/checkFamiliares'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                  		if(res != 0){
	                  			$("#div_familiares").empty();
	                  			$("#div_familiares").append(res)

	                  		}
	                  		else{
	                  			$("#div_familiares").empty();
	                  			$("#div_familiares").append('<p class="tituloSubseccion">No data</p>');
	                  		}
	                	},error:function(res){
	                  		$('#errorModal').modal('show');
	                	}
	              	});
	              	//Estudios
	              	$("#idEstudios").val(data.idEstudios);
	              	$("#prim_periodo").val(data.primaria_periodo);
	              	$("#prim_escuela").val(data.primaria_escuela);
	              	$("#prim_ciudad").val(data.primaria_ciudad);
	              	$("#prim_certificado").val(data.primaria_certificado);
	              	if(data.primaria_validada == 0){
	              		$("#prim_validado").val(0).trigger('change');
	              	}
	              	else{
	              		$("#prim_validado").val(1).trigger('change');
	              	}
	              	$("#sec_periodo").val(data.secundaria_periodo);
	              	$("#sec_escuela").val(data.secundaria_escuela);
	              	$("#sec_ciudad").val(data.secundaria_ciudad);
	              	$("#sec_certificado").val(data.secundaria_certificado);
	              	if(data.secundaria_validada == 0){
	              		$("#sec_validado").val(0).trigger('change');
	              	}
	              	else{
	              		$("#sec_validado").val(1).trigger('change');
	              	}
	              	$("#prep_periodo").val(data.preparatoria_periodo);
	              	$("#prep_escuela").val(data.preparatoria_escuela);
	              	$("#prep_ciudad").val(data.preparatoria_ciudad);
	              	$("#prep_certificado").val(data.preparatoria_certificado);
	              	if(data.preparatoria_validada == 0){
	              		$("#prep_validado").val(0).trigger('change');
	              	}
	              	else{
	              		$("#prep_validado").val(1).trigger('change');
	              	}
	              	$("#lic_periodo").val(data.licenciatura_periodo);
	              	$("#lic_escuela").val(data.licenciatura_escuela);
	              	$("#lic_ciudad").val(data.licenciatura_ciudad);
	              	$("#lic_certificado").val(data.licenciatura_certificado);
	              	if(data.licenciatura_validada == 0){
	              		$("#lic_validado").val(0).trigger('change');
	              	}
	              	else{
	              		$("#lic_validado").val(1).trigger('change');
	              	}
	              	$("#otro_certificado").val(data.otros_certificados);
	              	$("#estudios_comentarios").val(data.comentarios);
	              	$("#carrera_inactivo").val(data.carrera_inactivo);
	              	//Referencias Laborales
	              	$.ajax({
	                	url: '<?php echo base_url('index.php/Candidato/getReferencias'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		var rows = res.split('###');
	                		//Primera referencia laboral
	                		var dato = rows[0].split('@@');
	                  		$("#refLab1_empresa").text(dato[0]);
	                  		$("#refLab1_direccion").text(dato[1]);
	                  		var f = dato[2].split(' ');
	                  		var aux = f[0].split('-');
	                  		var fi = aux[1]+"/"+aux[2]+"/"+aux[0];
	                  		$("#refLab1_entrada").text(fi);
	                  		var f = dato[3].split(' ');
	                  		var aux = f[0].split('-');
	                  		var ff = aux[1]+"/"+aux[2]+"/"+aux[0];
	                  		$("#refLab1_salida").text(ff);
	                  		$("#refLab1_telefono").text(dato[4]);
	                  		$("#refLab1_puesto1").text(dato[5]);
	                  		$("#refLab1_puesto2").text(dato[6]);
	                  		$("#refLab1_salario1").text("$"+dato[7]);
	                  		$("#refLab1_salario2").text("$"+dato[8]);
	                  		$("#refLab1_bossnombre").text(dato[9]);
	                  		$("#refLab1_bosscorreo").text(dato[10]);
	                  		$("#refLab1_bosspuesto").text(dato[11]);
	                  		$("#refLab1_separacion").text(dato[12]);
	                  		$("#idreflab1").val(dato[13]);
	                  		//Segunda referencia laboral
	                  		var dato2 = rows[1].split('@@');
	                  		$("#refLab2_empresa").text(dato2[0]);
	                  		$("#refLab2_direccion").text(dato2[1]);
	                  		var f = dato2[2].split(' ');
	                  		var aux = f[0].split('-');
	                  		var fi = aux[1]+"/"+aux[2]+"/"+aux[0];
	                  		$("#refLab2_entrada").text(fi);
	                  		var f = dato2[3].split(' ');
	                  		var aux = f[0].split('-');
	                  		var ff = aux[1]+"/"+aux[2]+"/"+aux[0];
	                  		$("#refLab2_salida").text(ff);
	                  		$("#refLab2_telefono").text(dato2[4]);
	                  		$("#refLab2_puesto1").text(dato2[5]);
	                  		$("#refLab2_puesto2").text(dato2[6]);
	                  		$("#refLab2_salario1").text("$"+dato2[7]);
	                  		$("#refLab2_salario2").text("$"+dato2[8]);
	                  		$("#refLab2_bossnombre").text(dato2[9]);
	                  		$("#refLab2_bosscorreo").text(dato2[10]);
	                  		$("#refLab2_bosspuesto").text(dato2[11]);
	                  		$("#refLab2_separacion").text(dato2[12]);
	                  		$("#idreflab2").val(dato[13]);
	                  		
	                	},error:function(res){
	                  		$('#errorModal').modal('show');
	                	}
	              	});
	              	$("#trabajo_inactivo").val(data.trabajo_inactivo);
	              	$.ajax({
	                	url: '<?php echo base_url('index.php/Candidato/getVerificacionReferencias'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != 0){
	                			var rows = res.split('###');
		                		//Primera referencia laboral
		                		var dato = rows[0].split('@@');
		                  		$("#an_refLab1_empresa").val(dato[0]);
		                  		$("#an_refLab1_direccion").val(dato[1]);
		                  		if(dato[2] != "0000-00-00"){
		                  			var aux = dato[2].split('-');
		                  			var fi = aux[1]+"/"+aux[2]+"/"+aux[0];
		                  			$("#an_refLab1_entrada").val(fi);
		                  		}
		                  		if(dato[3] != "0000-00-00"){
		                  			var aux = dato[3].split('-');
		                  			var ff = aux[1]+"/"+aux[2]+"/"+aux[0];
		                  			$("#an_refLab1_salida").val(ff);
		                  		}
		                  		$("#an_refLab1_telefono").val(dato[4]);
		                  		$("#an_refLab1_puesto1").val(dato[5]);
		                  		$("#an_refLab1_puesto2").val(dato[6]);
		                  		var s1 = (dato[7] == 0) ? "" : dato[7];
		                  		$("#an_refLab1_salario1").val(s1);
		                  		var s2 = (dato[8] == 0) ? "" : dato[8];
		                  		$("#an_refLab1_salario2").val(s2);
		                  		$("#an_refLab1_bossnombre").val(dato[9]);
		                  		$("#an_refLab1_bosscorreo").val(dato[10]);
		                  		$("#an_refLab1_bosspuesto").val(dato[11]);
		                  		$("#an_refLab1_separacion").val(dato[12]);
		                  		$("#an_refLab1_notas").val(dato[15]);
		                  		$("#an_refLab1_demanda").val(dato[16]);
		                  		$("#an_refLab1_responsabilidad").val(dato[17]);
		                  		$("#an_refLab1_iniciativa").val(dato[18]);
		                  		$("#an_refLab1_eficiencia").val(dato[19]);
		                  		$("#an_refLab1_disciplina").val(dato[20]);
		                  		$("#an_refLab1_puntualidad").val(dato[21]);
		                  		$("#an_refLab1_limpieza").val(dato[22]);
		                  		$("#an_refLab1_estabilidad").val(dato[23]);
		                  		$("#an_refLab1_emocional").val(dato[24]);
		                  		$("#an_refLab1_honesto").val(dato[25]);
		                  		$("#an_refLab1_rendimiento").val(dato[26]);
		                  		$("#an_refLab1_actitud").val(dato[27]);
		                  		$("#an_refLab1_recontratacion").val(dato[28]);
		                  		$("#an_refLab1_motivo").val(dato[29]);
		                  		//Segunda referencia laboral
		                  		var dato2 = rows[1].split('@@');
		                  		$("#an_refLab2_empresa").val(dato2[0]);
		                  		$("#an_refLab2_direccion").val(dato2[1]);
		                  		if(dato2[2] != "0000-00-00"){
		                  			var aux = dato2[2].split('-');
		                  			var fi = aux[1]+"/"+aux[2]+"/"+aux[0];
		                  			$("#an_refLab2_entrada").val(fi);
		                  		}
		                  		if(dato2[3] != "0000-00-00"){
		                  			var aux = dato2[3].split('-');
		                  			var ff = aux[1]+"/"+aux[2]+"/"+aux[0];
		                  			$("#an_refLab2_salida").val(ff);
		                  		}
		                  		$("#an_refLab2_telefono").val(dato2[4]);
		                  		$("#an_refLab2_puesto1").val(dato2[5]);
		                  		$("#an_refLab2_puesto2").val(dato2[6]);
		                  		var s1 = (dato2[7] == 0) ? "" : dato2[7];
		                  		$("#an_refLab2_salario1").val(s1);
		                  		var s2 = (dato2[8] == 0) ? "" : dato2[8];
		                  		$("#an_refLab2_salario2").val(s2);
		                  		$("#an_refLab2_bossnombre").val(dato2[9]);
		                  		$("#an_refLab2_bosscorreo").val(dato2[10]);
		                  		$("#an_refLab2_bosspuesto").val(dato2[11]);
		                  		$("#an_refLab2_separacion").val(dato2[12]);
		                  		$("#an_refLab2_notas").val(dato2[15]);
		                  		$("#an_refLab2_demanda").val(dato2[16]);
		                  		$("#an_refLab2_responsabilidad").val(dato2[17]);
		                  		$("#an_refLab2_iniciativa").val(dato2[18]);
		                  		$("#an_refLab2_eficiencia").val(dato2[19]);
		                  		$("#an_refLab2_disciplina").val(dato2[20]);
		                  		$("#an_refLab2_puntualidad").val(dato2[21]);
		                  		$("#an_refLab2_limpieza").val(dato2[22]);
		                  		$("#an_refLab2_estabilidad").val(dato2[23]);
		                  		$("#an_refLab2_emocional").val(dato2[24]);
		                  		$("#an_refLab2_honesto").val(dato2[25]);
		                  		$("#an_refLab2_rendimiento").val(dato2[26]);
		                  		$("#an_refLab2_actitud").val(dato2[27]);
		                  		$("#an_refLab2_recontratacion").val(dato2[28]);
		                  		$("#an_refLab2_motivo").val(dato2[29]);
	                		}
	                	},error:function(res){
	                  		$('#errorModal').modal('show');
	                	}
	              	});
	              	//Referencias Personales
	              	$.ajax({
	                	url: '<?php echo base_url('index.php/Candidato/getPersonales'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != 0){
	                			var rows = res.split('###');
	                			//Primera referencia personal
	                			var dato = rows[0].split('@@');
	                			$("#refPer1_nombre").val(dato[0]);
	                			$("#refPer1_telefono").val(dato[1]);
	                			$("#refPer1_tiempo").val(dato[2]);
	                			$("#refPer1_conocido").val(dato[3]);
	                			$("#refPer1_sabetrabajo").val(dato[4]);
	                			$("#refPer1_sabevive").val(dato[5]);
	                			$("#refPer1_id").val(dato[6]);
	                			$("#refPer1_comentario").val(dato[7]);
	                			//Segunda referencia personal
	                			var dato = rows[1].split('@@');
	                			$("#refPer2_nombre").val(dato[0]);
	                			$("#refPer2_telefono").val(dato[1]);
	                			$("#refPer2_tiempo").val(dato[2]);
	                			$("#refPer2_conocido").val(dato[3]);
	                			$("#refPer2_sabetrabajo").val(dato[4]);
	                			$("#refPer2_sabevive").val(dato[5]);
	                			$("#refPer2_id").val(dato[6]);
	                			$("#refPer2_comentario").val(dato[7]);
	                			//Tercera referencia personal
	                			var dato = rows[2].split('@@');
	                			$("#refPer3_nombre").val(dato[0]);
	                			$("#refPer3_telefono").val(dato[1]);
	                			$("#refPer3_tiempo").val(dato[2]);
	                			$("#refPer3_conocido").val(dato[3]);
	                			$("#refPer3_sabetrabajo").val(dato[4]);
	                			$("#refPer3_sabevive").val(dato[5]);
	                			$("#refPer3_id").val(dato[6]);
	                			$("#refPer3_comentario").val(dato[7]);
	                		}
	                   		
	                	},error:function(res){
	                  		$('#errorModal').modal('show');
	                	}
	              	});
	              	//Trabajo en gobierno
	              	$("#trabajo_gobierno").val(data.trabajo_gobierno);
	              	//Documentos para agregar al archivo final
	              	$.ajax({
	                	url: '<?php echo base_url('index.php/Candidato/getTiposDocumentos'); ?>',
	                	method: 'POST',
	                	data: {'id_candidato':data.id},
	                	dataType: "text",
	                	success: function(res){
	                		if(res != ""){
	                			$("#div_tipos_docs").empty();
	                			$("#div_tipos_docs").append(res);
	                		}
	                		else{
	                			$("#div_tipos_docs").empty();
	                			$("#div_tipos_docs").append('<p class="text-center">No hay documentos válidos (.jpg, .png) para añadir</p>');
	                		}	                   		
	                	},error:function(res){
	                  		//$('#errorModal').modal('show');
	                	}
	              	});
	              	
			        $("#listado").css('display','none');
			        $("#formulario").css('display','block');
			        $("#btn_regresar").css('display','block');            
		        });
				$("a#estudios", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#estudios_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			verificacionEstudios();
	      		});
	      		$("a#laborales", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#laborales_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			verificacionLaborales();
	      		});
	      		$("a#penales", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#penales_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			verificacionPenales();
	      		});
	      		$("a#llamada", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#llamada_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusLlamadas();
	      		});
	      		$("a#correoEnviado", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#email_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusEmails();
	      		});
	      		$("a#final", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			checkCandidato();
	      		});
	      		$('a[id^=pdfFinal]', row).bind('click', () => {
		            var id = data.id;
		            $('#pdf'+id).submit();
		        });
		        $('a#verCancelado', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('index.php/Candidato/getCancelacion'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	datos = res.split('##');
			            	var fecha = new Date(datos[0]);
							var options = { year: 'numeric', month: 'long', day: 'numeric' };
			            	$("#titulo_accion").text("Cancelado");
			            	$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue cancelado el '+fecha.toLocaleDateString("es-ES", options));
			            	$("#motivo").html("Comentario: <br>"+datos[1]);
			            	$("#verModal").modal('show');

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
		        $('a#verEliminado', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('index.php/Candidato/getEliminacion'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	datos = res.split('##');
			            	var fecha = new Date(datos[0]);
							var options = { year: 'numeric', month: 'long', day: 'numeric' };
			            	$("#titulo_accion").text("Eliminado");
			            	$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue eliminado el '+fecha.toLocaleDateString("es-ES", options));
			            	$("#motivo").html("Comentario: <br>"+datos[1]);
			            	$("#verModal").modal('show');

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
		        $("a#generar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$(".correo").val(data.correo);
	      			$("#titulo_accion").text("Generar password");
	      			$("#texto_confirmacion").html("Estás seguro de generar un nuevo password para <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','generate');
	      			$("#quitarModal").modal("show");
	      		});
	      		$('a#comentario', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('index.php/Candidato/viewComentario'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
			            		$("#titulo_accion").text("Comentario del candidato");
				            	//$("#nombre_candidato").html("El candidato <b>"+data.nombre+' '+data.paterno+' '+data.materno+'</b> fue cancelado el '+fecha.toLocaleDateString("es-ES", options));
				            	$("#motivo").html(res);
				            	$("#verModal").modal('show');
			            	}
			            	else{
			            		$("#titulo_accion").text("Comentarios del candidato");
				            	$("#motivo").html("No hay registro de comentarios");
				            	$("#verModal").modal('show');
			            	}
			            	

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
      		},
	      	"language": {
	        	"lengthMenu": "Mostrar _MENU_ registros por página",
	        	"zeroRecords": "No se encontraron registros",
	        	"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        	"infoEmpty": "Sin registros disponibles",
	        	"infoFiltered": "(Filtrado _MAX_ registros totales)",
	        	"sSearch": "Buscar:",
	        	"oPaginate": {
	          		"sLast": "Última página",
	          		"sFirst": "Primera",
	          		"sNext": "<i class='fa  fa-arrow-right'></i>",
	          		"sPrevious": "<i class='fa fa-arrow-left'></i>"
	        	}
	      	}
    	}); 
    	$('#tabla').DataTable().search(" ");
    	//Obtiene los municipios dependiendo del id estado
	  	$("#estado").change(function(){
	      	var id_estado = $(this).val();
	      	if(id_estado != -1){
	        	$.ajax({
	          		url: '<?php echo base_url('index.php/Candidato/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#municipio').prop('disabled', false);
	            		$('#municipio').html(res);
	          		},error:function(res)
	          		{
	            		//$('#errorModal').modal('show');
	          		}
	        	});
	      	}
	      	else{
        		$('#municipio').prop('disabled', true);
	        	$('#municipio').append($("<option selected></option>").attr("value",-1).text("Selecciona"));
	      	}
	    });
	    $("#fecha_nacimiento, #refLab1_entrada, #refLab1_salida, #refLab2_entrada, #refLab2_salida").datetimepicker({
	  		minView: 2,
	    	format: "mm/dd/yyyy",
	    	startView: 4,
	    	autoclose: true,
	    	todayHighlight: true,
	    	pickerPosition: "bottom-left",
	    	forceParse: false
  		});
  		$("#docsModal").on("hidden.bs.modal", function(){
	    	$("#documento").val("");
	    });
  	});
	//Sube un nuevo documento del candidato
	function subirDoc(){
        if($("#documento").val() == ""){
          $("#sin_documento").css("display","block");
          setTimeout(function(){
            $("#sin_documento").fadeOut();
          }, 4000);
        }
        else{
        	var id_candidato = $(".idCandidato").val();
		    var prefijo = $(".prefijo").val();
		    //var form = document.getElementById("formSubirDoc");
	        var data = new FormData();
	        var doc = $("#documento")[0].files[0];
	        data.append('id_candidato', id_candidato);
	        data.append('prefijo', prefijo);
	        data.append('documento', doc);
          	$.ajax({
             	url:"<?php echo base_url('index.php/Candidato/uploadDoc'); ?>",   
             	method:"POST",  
             	data:data,  
             	contentType: false,  
             	cache: false,  
             	processData:false,
             	
             	success:function(res){
         			$("#documento").val("");
         			
                	$("#subido").css('display','block');
            		setTimeout(function(){
                  		$('#subido').fadeOut();
                	},4000);
            		$("#tablaDocs").empty();
            		$("#tablaDocs").html(res);
            	}  
          	});
        }
    }  
	//Se actualizan los documentos referenciando por su tipo
	function actualizarDocs(){
		$("#sin_documento").css("display","none");
		var total = $("select[id^='tipo']").length;

		var totalVacios = $('.item').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".item").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#tipos_iguales").css('display','none');
					$("#campos_vacios").css('display','block');
					setTimeout(function(){
						$("#campos_vacios").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	var id_candidato = $(".idCandidato").val();
	    	var prefijo = $(".prefijo").val();
	    	var salida = "";
			var indicador = 0;
			var aux = new Array();
    		for(var i = 0; i < total; i++){
    			var arch = $("#arch"+i).text();
    			var tipo = $("#tipo"+i).val();
    			//console.log("idc "+id_candidato+" pre: "+prefijo+" arch: "+arch+" tipo: "+tipo);
    			/*if(aux.includes(tipo) && tipo != 13){
    				indicador = 1;
    			}
    			else{
    				salida += arch+","+tipo+"@@";
    			}*/
    			salida += arch+","+tipo+"@@";
    			aux[i] = tipo;
    		}
    		if(indicador == 1){
    			$("#tipos_iguales").css('display','block');
				setTimeout(function(){
					$("#tipos_iguales").fadeOut();
				},4000);
    		}
	    	else{
	    		console.log(salida);
	    		$.ajax({
	          		url: '<?php echo base_url('index.php/Candidato/saveDocs'); ?>',
	          		method: 'POST',
	          		data: {'docs':salida,'id_candidato':id_candidato,'prefijo':prefijo},
	          		dataType: "text",
	          		beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	          		success: function(res)
	          		{
	          			if(res == 1){
	          				setTimeout(function(){
		                  		$('.loader').fadeOut();
		                	},300);
		                	$("#exito").css('display','block');
		                	$("#docsModal").modal('hide');
	                		setTimeout(function(){
		                  		$('#exito').fadeOut();
		                	},5000);
	          			}
	          		},error:function(res)
	          		{
	            		//$('#errorModal').modal('show');
	          		}
	        	});
	    	}
	    }
		
	}
	//Modal para generar estatus de verificación de documentos
	function verificacionEstudios(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_estudio").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_estudio").css('display','none');
        				$("#btnTerminarVerificacionEstudio").css('display','none');
        			}
        			$("#div_crearEstatusEstudio").empty();
        			$("#idVerificacionEstudio").val(aux[1]);
        			$("#div_crearEstatusEstudio").append(aux[0]);
        		}
        		else{
        			$("#idVerificacionEstudio").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionEstudiosModal").modal("show");
	}
	//Generar estatus de verificacion de estudio
	function generarEstatusEstudio(){
		var id_candidato = $(".idCandidato").val();
		var id_verificacion = $("#idVerificacionEstudio").val();
		var comentario = $("#estudio_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_verificacion':id_verificacion,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionEstudio").val(aux[1]);
        		$("#estudio_estatus_comentario").val("");
        		$("#div_crearEstatusEstudio").empty();
        		$("#div_crearEstatusEstudio").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de estudios
	function terminarEstudios(){
		var id_verificacion = $("#idVerificacionEstudio").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/finishEstatusEstudios'); ?>',
      		method: 'POST',
      		data: {'id_verificacion':id_verificacion},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarEstudiosModal").modal('hide');
      			$("#verificacionEstudiosModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoEstudios").css('display','block');
            	setTimeout(function(){
              		$('#exitoEstudios').fadeOut();
            	},5000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar estatus de verificación de referencias laborales
	function verificacionLaborales(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_laboral").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_laboral").css('display','none');
        				$("#btnTerminarVerificacionLaboral").css('display','none');
        			}
        			$("#div_crearEstatusLaboral").empty();
        			$("#idVerificacionLaboral").val(aux[1]);
        			$("#div_crearEstatusLaboral").append(aux[0]);
        		}
        		else{
        			$("#idVerificacionLaboral").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionLaboralesModal").modal("show");
	}
	//Generar estatus de verificacion de referencia laboral
	function generarEstatusLaboral(){
		var id_candidato = $(".idCandidato").val();
		var id_status = $("#idVerificacionLaboral").val();
		var comentario = $("#laboral_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_status':id_status,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionLaboral").val(aux[1]);
        		$("#laboral_estatus_comentario").val("");
        		$("#div_crearEstatusLaboral").empty();
        		$("#div_crearEstatusLaboral").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de ref laborales
	function terminarLaborales(){
		var id_status = $("#idVerificacionLaboral").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/finishEstatusLaborales'); ?>',
      		method: 'POST',
      		data: {'id_status':id_status},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarLaboralesModal").modal('hide');
      			$("#verificacionLaboralesModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoLaborales").css('display','block');
            	setTimeout(function(){
              		$('#exitoLaborales').fadeOut();
            	},5000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar estatus de verificación de antecedentes no penales
	function verificacionPenales(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		$("#fecha_estatus_penales").text( dia + "/" + mes + "/" + f.getFullYear());
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_penales").css('display','none');
        				$("#btnTerminarVerificacionPenales").css('display','none');
        			}
        			$("#div_crearEstatusPenales").empty();
        			$("#idVerificacionPenales").val(aux[1]);
        			$("#div_crearEstatusPenales").append(aux[0]);
        		}
        		else{
        			$("#idVerificacionPenales").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#verificacionPenalesModal").modal("show");
	}
	//Generar estatus de verificacion de antecdentes no penales
	function generarEstatusPenales(){
		var id_candidato = $(".idCandidato").val();
		var id_status = $("#idVerificacionPenales").val();
		var comentario = $("#penales_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_status':id_status,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idVerificacionPenales").val(aux[1]);
        		$("#penales_estatus_comentario").val("");
        		$("#div_crearEstatusPenales").empty();
        		$("#div_crearEstatusPenales").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Termina con la verificación de antecedentes no penales
	function terminarPenales(){
		var id_status = $("#idVerificacionPenales").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/finishEstatusPenales'); ?>',
      		method: 'POST',
      		data: {'id_status':id_status},
      		dataType: "text",
      		beforeSend: function() {
            	$('.loader').css("display","block");
          	},
      		success: function(res)
      		{
      			$("#confirmarPenalesModal").modal('hide');
      			$("#verificacionPenalesModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoPenales").css('display','block');
            	setTimeout(function(){
              		$('#exitoPenales').fadeOut();
            	},5000);
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar registros de las llamadas hechas al candidato
	function estatusLlamadas(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_llamadas").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkLlamadas'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_llamadas").css('display','none');
        			}
        			$("#div_crearEstatusLlamadas").empty();
        			$("#idLlamadas").val(aux[1]);
        			$("#div_crearEstatusLlamadas").append(aux[0]);
        		}
        		else{
        			$("#idLlamadas").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#llamadasModal").modal("show");
	}
	//Generar estatus de llamada candidato
	function generarEstatusLlamada(){
		var id_candidato = $(".idCandidato").val();
		var id_llamada = $("#idLlamadas").val();
		var comentario = $("#llamadas_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusLlamada'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_llamada':id_llamada,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idLlamadas").val(aux[1]);
        		$("#llamadas_estatus_comentario").val("");
        		$("#div_crearEstatusLlamadas").empty();
        		$("#div_crearEstatusLlamadas").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Modal para generar registros de las llamadas hechas al candidato
	function estatusEmails(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$("#fecha_estatus_emails").text( dia + "/" + mes + "/" + f.getFullYear() +" "+h+":"+m);
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkEmails'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			if(res != 0){
      				var aux = res.split('@@');
      				var finalizado = aux[2];
        			if(finalizado == 1){
        				$("#div_estatus_emails").css('display','none');
        			}
        			$("#div_crearEstatusEmail").empty();
        			$("#idEmails").val(aux[1]);
        			$("#div_crearEstatusEmail").append(aux[0]);
        		}
        		else{
        			$("#idEmails").val(0);
        		}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#emailsModal").modal("show");
	}
	//Generar estatus de llamada candidato
	function generarEstatusEmail(){
		var id_candidato = $(".idCandidato").val();
		var id_email = $("#idEmails").val();
		var comentario = $("#emails_estatus_comentario").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/createEstatusEmail'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato,'id_email':id_email,'comentario':comentario},
      		dataType: "text",
      		success: function(res)
      		{
      			var aux = res.split('@@');
      			$("#idEmails").val(aux[1]);
        		$("#emails_estatus_comentario").val("");
        		$("#div_crearEstatusEmail").empty();
        		$("#div_crearEstatusEmail").append(aux[0]);
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Obtiene el municipio al verificar datos
	function getMunicipio(id_estado, id_municipio){
		$.ajax({
        	url: '<?php echo base_url('index.php/Candidato/getMunicipios'); ?>',
        	method: 'POST',
        	data: {'id_estado':id_estado},
        	dataType: "text",
        	success: function(res){
          		$('#municipio').prop('disabled', false);
          		$('#municipio').html(res);
          		$("#municipio").find('option').attr("selected",false) ;
          		$('#municipio option[value="'+id_municipio+'"]').attr('selected', 'selected');
        	},error:function(res){
          		$('#errorModal').modal('show');
        	}
      	});
	}
	//Actualiza los cambios a los datos del candidato
	function actualizarCandidato(){
		var id_candidato = $(".idCandidato").val();
		var d_personal = $("#d_personal").serialize();
		var d_documentos = $("#d_documentos").serialize();
		var d_estudios = $("#d_estudios").serialize();
		var d_refLaboral = $("#d_refLaboral").serialize();
		var d_refPersonal = $("#d_refPersonal").serialize();
		var d_images = $("#d_images").serialize();
		var idreflab1 = $("#idreflab1").val();
		var idreflab2 = $("#idreflab2").val();
		var refPer1_id = $("#refPer1_id").val();
		var refPer2_id = $("#refPer2_id").val();
		var refPer3_id = $("#refPer3_id").val();
		var idEstudios = $("#idEstudios").val();
		var trabajo_inactivo = $("#trabajo_inactivo").val();
		
        var d_familia = "";
        d_familia += $("#nombre_conyuge").val()+"@@";
        d_familia += $("#edad_conyuge").val()+"@@";
        d_familia += $("#puesto_conyuge").val()+"@@";
        d_familia += $("#ciudad_conyuge").val()+"@@"; 
        d_familia += $("#empresa_conyuge").val()+"@@"; 
        d_familia += $("#con_conyuge").val()+"@@"; 
        d_familia += $("#nombre_padre").val()+"@@"; 
        d_familia += $("#edad_padre").val()+"@@"; 
        d_familia += $("#puesto_padre").val()+"@@";
        d_familia += $("#ciudad_padre").val()+"@@";  
        d_familia += $("#empresa_padre").val()+"@@"; 
        d_familia += $("#con_padre").val()+"@@"; 
        d_familia += $("#nombre_madre").val()+"@@"; 
        d_familia += $("#edad_madre").val()+"@@"; 
        d_familia += $("#puesto_madre").val()+"@@";
        d_familia += $("#ciudad_madre").val()+"@@"; 
        d_familia += $("#empresa_madre").val()+"@@"; 
        d_familia += $("#con_madre").val(); 
        var hijos = "";
        var total_hijos = $(".es_hijo").length;
      	if(total_hijos > 0){
        	for(var i = 1; i <= total_hijos / 5; i++){
				hijos += $("#nombre_hijo"+i).val()+",,";
				hijos += $("#edad_hijo"+i).val()+",,";
				hijos += $("#puesto_hijo"+i).val()+",,";
				hijos += $("#ciudad_hijo"+i).val()+",,";
				hijos += $("#empresa_hijo"+i).val()+",,";
				hijos += $("#con_hijo"+i).val()+"@@";
			}
        }
        var hermanos = "";
        var total_hermanos = $(".es_hermano").length;
      	if(total_hermanos > 0){
        	for(var i = 1; i <= total_hermanos / 5; i++){
				hermanos += $("#nombre_hermano"+i).val()+",,";
				hermanos += $("#edad_hermano"+i).val()+",,";
				hermanos += $("#puesto_hermano"+i).val()+",,";
				hermanos += $("#ciudad_hermano"+i).val()+",,";
				hermanos += $("#empresa_hermano"+i).val()+",,";
				hermanos += $("#con_hermano"+i).val()+"@@";
			}
        }
		var form_data = new FormData();
		form_data.append('id_candidato',id_candidato);
		form_data.append('d_personal',d_personal);
		form_data.append('d_documentos',d_documentos);
		form_data.append('d_estudios',d_estudios);
		form_data.append('d_refLaboral',d_refLaboral);
		form_data.append('d_refPersonal',d_refPersonal);
		form_data.append('d_images',d_images);
		form_data.append('d_familia',d_familia);
		form_data.append('hijos',hijos);
		form_data.append('hermanos',hermanos);
		form_data.append('idreflab1',idreflab1);
		form_data.append('idreflab2',idreflab2);
		form_data.append('refPer1_id',refPer1_id);
		form_data.append('refPer2_id',refPer2_id);
		form_data.append('refPer3_id',refPer3_id);
		form_data.append('idEstudios',idEstudios);

		var totalPer = $('.personal_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	var totalFam = $('.familia_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	var totalEst = $('.estudios_obligado').filter(function(){
      		return !$(this).val();
    	}).length;
    	var totalRefPer = $('.refPer_obligado').filter(function(){
      		return !$(this).val();
    	}).length;


	    if(totalPer > 0){
	      	$(".personal_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "" || element.val() == -1) {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text("Hay campos vacíos en la sección de datos personales.");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},5000);
		          	$('html, body').animate({
					    scrollTop: $("#titulo_personal").offset().top
					}, 2000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	if(totalFam > 0){
		      	$(".familia_obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "" || element.val() == -1) {
			          	element.addClass("requerido");
			          	$("#txt_vacios").text(" Hay campos vacíos en la sección de familiares.");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},5000);
			          	$('html, body').animate({
						    scrollTop: $("#titulo_familia").offset().top
						}, 2000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	if(totalEst > 0){
			      	$(".estudios_obligado").each(function() {
				        var element = $(this);
				        if (element.val() == "" || element.val() == -1) {
				          	element.addClass("requerido");
				          	$("#txt_vacios").text(" Hay campos vacíos en la sección de estudios.");
				          	$('#vacios').css("display", "block");
				          	setTimeout(function(){
				            	$('#vacios').fadeOut();
				          	},5000);
				          	$('html, body').animate({
							    scrollTop: $("#titulo_estudios").offset().top
							}, 2000);
				        }
				        else{
				          	element.removeClass("requerido");
				        }
			      	});
			    }
			    else{
			    	if(trabajo_inactivo == ""){
			    		$("#trabajo_inactivo").addClass("requerido");
			          	$("#txt_vacios").text(" El campo <<Break(s) in Employment>> está vacío.");
			          	$('#vacios').css("display", "block");
			          	setTimeout(function(){
			            	$('#vacios').fadeOut();
			          	},5000);
			          	$('html, body').animate({
						    scrollTop: $("#titulo_break").offset().top
						}, 2000);
			    	}
			    	else{
			    		if(totalRefPer > 0){
				    		$(".refPer_obligado").each(function() {
						        var element = $(this);
						        if (element.val() == "" || element.val() == -1) {
						          	element.addClass("requerido");
						          	$("#txt_vacios").text(" Hay campos vacíos en la sección de referencias personales.");
						          	$('#vacios').css("display", "block");
						          	setTimeout(function(){
						            	$('#vacios').fadeOut();
						          	},5000);
						          	$('html, body').animate({
									    scrollTop: $("#titulo_refpersonales").offset().top
									}, 2000);
						        }
						        else{
						          	element.removeClass("requerido");
						        }
					      	});
				    	}
				    	
				    	else{
					    	//console.log(d_refLaboral);
					    	$.ajax({
					      		url: '<?php echo base_url('index.php/Candidato/updateCandidato'); ?>',
					      		method: "POST",  
					            data: form_data,  
					            contentType: false,  
					            cache: false,  
					            processData:false,  
					            dataType: "json",
					      		beforeSend: function() {
					            	$('.loader').css("display","block");
					          	},
					      		success: function(res)
					      		{
					      			if(res != 0){
					      				setTimeout(function(){
						              		$('.loader').fadeOut();
						            	},2000);
						            	localStorage.setItem("success", 1);
						            	location.reload();
						            	
					      			}
					      			
					      		},error:function(res)
					      		{
					        		//$('#errorModal').modal('show');
					      		}
					    	});
					    }

			    	}
			    }
		    }
	    	
	    }
    	// Display the key/value pairs
		/*for (var pair of form_data.entries()) {
		    console.log(pair[0]+ ', ' + pair[1]); 
		}*/
	}
	function ejecutarAccion(){
		var accion = $("#btnGuardar").val();
		var id_candidato = $(".idCandidato").val();
		var correo = $(".correo").val();
		if(accion == 'generate'){
			$.ajax({
              	url: '<?php echo base_url('index.php/Candidato/generate'); ?>',
              	type: 'post',
              	data: {'id_candidato':id_candidato,'correo':correo},
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		setTimeout(function(){
                  		$('.loader').fadeOut();
                	},300);
            		$("#quitarModal").modal('hide');
            		$("#user").text(correo);
            		$("#pass").text(res);
            		$("#respuesta_mail").text("* An email has been sent with this credentials to the candidate. This email could take a few minutes to be delivered.");
            		$("#passModal").modal('show');
            		recargarTable();
            		$("#texto_msj").text('The password has been created succesfully');
            		$("#mensaje").css('display','block');
            		setTimeout(function(){
                  		$('#mensaje').fadeOut();
                	},5000);
              	},error: function(res){
                	$('#errorModal').modal('show');
              	}
        	});
		}
	}
	//Verificamos si el candidato cuenta con los datos completos para poder generar el archivo final
	function checkCandidato(){
		var id_candidato = $(".idCandidato").val();
		$.ajax({
      		url: '<?php echo base_url('index.php/Candidato/checkCandidato'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		/*beforeSend: function() {
            	$('.loader').css("display","block");
          	},*/
      		success: function(res)
      		{
      			console.log(res)
      			if(res == "D1E1L1L1P1P1P1A1G1SL1SE1SP1"){//Indica todo completo
      				$("#div_incompleto").css('display','none');
  					$("#div_completo").css('display','block');
  					$("#btnTerminar").css('display','initial');
  					$("#completarModal").modal('show');
      			}
      			else{
      				if(res == "D0"){//Indica sin verificacion de documentos
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Faltan datos en la verificación de documentos");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E0"){//Indica sin verificacion de estudios
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Faltan datos en la verificación de estudios");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E1L0L0" || res == "D1E1L1L0" || res == "D1E1L0L1"){//Indica sin verificacion de referencias laborales
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Faltan datos en la verificación de referencias laborales");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E1L1L1P0P0P0" || res == "D1E1L1L1P1P0P0" || res == "D1E1L1L1P1P1P0" || res == "D1E1L1L1P1P0P1" || res == "D1E1L1L1P0P1P1" || res == "D1E1L1L1P0P0P1" || res == "D1E1L1L1P0P1P0" || res == "D1E1L1L1P0P0P1"){//Indica sin verificacion de referencias personales
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Faltan datos en la verificación de referencias personales");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E1L1L1P1P1P1A0"){//Indica sin verificacion de archivos completos del candidato
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Faltan documentos por subir del candidato");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E1L1L1P1P1P1A1G0"){//Indica sin archivos agregados para el documento final del candidato
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Falta agregar documentos para el archivo final del candidato");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E1L1L1P1P1P1A1G1SL0"){//Indica sin registros finalizados en estatus de referencia laboral para el documento final del candidato
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Sin registros de estatus en la verificación de referencias laborales");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E1L1L1P1P1P1A1G1SL1SE0"){//Indica sin registros finalizados en estatus de verificacion de documentos escolares para el documento final del candidato
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Sin registros de estatus en la verificación de estudios");
      					$("#completarModal").modal('show');
      				}
      				if(res == "D1E1L1L1P1P1P1A1G1SL1SE1SP0"){//Indica sin registros finalizados en estatus de verificacion de antecedentes no penales para el documento final del candidato
      					$("#div_completo").css('display','none');
      					$("#div_incompleto").css('display','block');
      					$("#btnTerminar").css('display','none');
      					$("#msjIncompleto").text("Sin registros de estatus en la verificación de antecedentes no penales");
      					$("#completarModal").modal('show');
      				}
      			}
      			
      			/*$("#confirmarPenalesModal").modal('hide');
      			$("#verificacionPenalesModal").modal('hide');
      			setTimeout(function(){
              		$('.loader').fadeOut();
            	},300);
            	$("#exitoPenales").css('display','block');
            	setTimeout(function(){
              		$('#exitoPenales').fadeOut();
            	},5000);*/
      			
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	//Registro de status bgc al candidato
	function finalizarProceso(){
		var id_candidato = $(".idCandidato").val();
		var checks = $("#formChecks").serialize();
		//console.log(checks)
		var totalVacios = $('.check_obligado').filter(function(){
      		return !$(this).val();
    	}).length;

    	if(totalVacios > 0){
	      	$(".check_obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "" || element.val() == -1) {
		          	element.addClass("requerido");
					$("#campos_vacios_check").css('display','block');
					setTimeout(function(){
						$("#campos_vacios_check").fadeOut();
					},4000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	$.ajax({
          		url: '<?php echo base_url('index.php/Candidato/finishCandidate'); ?>',
          		method: 'POST',
          		data: {'id_candidato':id_candidato,'checks':checks},
          		dataType: "text",
          		success: function(res)
          		{
            		setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("finished", 1);
	            	location.reload();
          		},error:function(res)
          		{
            		//$('#errorModal').modal('show');
          		}
        	});
	    }
	}
  	//Regresa del formulario al listado
  	function regresar(){
  		$("#listado").css('display','block');
  		$("#btn_regresar").css('display','none'); 
  		$("#formulario").css('display','none');
  	}
  	//Aplica el valor del option a todos los select correspondientes
  	$("#aplicar_todo").change(function(){
  		var valor = $(this).val();
  		switch(valor){
  			case "-1":
  				$(".performance").val(0);
  				break;
  			case "0":
  				$(".performance").val(0);
  				break;
  			case "1":
  				$(".performance").val(1);
  				break;
  			case "2":
  				$(".performance").val(2);
  				break;
  			case "3":
  				$(".performance").val(3);
  				break;
  			case "4":
  				$(".performance").val(4);
  				break;
  			case "5":
  				$(".performance").val(5);
  				break;
  		}
  	});
  	$("#aplicar_todo2").change(function(){
  		var valor = $(this).val();
  		switch(valor){
  			case "-1":
  				$(".performance2").val(0);
  				break;
  			case "0":
  				$(".performance2").val(0);
  				break;
  			case "1":
  				$(".performance2").val(1);
  				break;
  			case "2":
  				$(".performance2").val(2);
  				break;
  			case "3":
  				$(".performance2").val(3);
  				break;
  			case "4":
  				$(".performance2").val(4);
  				break;
  			case "5":
  				$(".performance2").val(5);
  				break;
  		}
  	});
  	//Habilita el textarea del motivo de recontratacion del candidato
  	$("#an_refLab1_recontratacion").change(function(){
  		var valor = $(this).val();
  		if(valor != -1){
  			$("#an_refLab1_motivo").addClass('obligado');
  			$("#an_refLab1_motivo").prop('disabled',false);
  			$("#an_refLab1_motivo").val("");
  		}
  		else{
  			$("#an_refLab1_motivo").removeClass('obligado');
  			$("#an_refLab1_motivo").prop('disabled',true);
  			$("#an_refLab1_motivo").val("");
  		}
  	});
  	//Obtiene la fecha de nacimiento
	$("#fecha_nacimiento").change(function(){
		var fecha = $(this).val();2
		var today = new Date();
	    var birthDate = new Date(fecha);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age = age - 1;
	    }
	    $("#edad").val(age);
	});
  	//Acepta solo numeros en los input
  	$(".solo_numeros").on("input", function(){
	    var valor = $(this).val();
	    $(this).val(valor.replace(/[^0-9]/g, ''));
	});
	//Limpiar inputs
	$(".personal_obligado, .check_obligado, .familia_obligado, .estudios_obligado, #trabajo_inactivo, .refPer_obligado").focus(function(){
		$(this).removeClass("requerido");
	});
	//Se cambia el idioma al español;
	$.fn.datetimepicker.dates['en'] = {
	    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
	    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
	    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
	    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
	    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
	    meridiem: '',
	    today: "Hoy"
	};

  	//Se crea la variable para establecer la fecha actual
  	var hoy = new Date();
  	var dd = hoy.getDate();
  	var mm = hoy.getMonth()+1;
  	var yyyy = hoy.getFullYear();
  	var hora = hoy.getHours()+":"+hoy.getMinutes();

  	if(dd<10) {
      	dd='0'+dd;
  	} 

  	if(mm<10) {
      	mm='0'+mm;
  	}

  	$("#an_refLab1_entrada, #an_refLab1_salida, #an_refLab2_entrada, #an_refLab2_salida").datetimepicker({
  		minView: 2,
    	format: "mm/dd/yyyy",
    	startView: 4,
    	autoclose: true,
    	todayHighlight: true,
    	pickerPosition: "bottom-left",
    	forceParse: false
  	});
  	//Se establece que el calendario comienzo en la fecha actual
  	//$('#fecha').datetimepicker('setStartDate', hoy);
</script>