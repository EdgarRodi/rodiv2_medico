<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Formularios RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/skins/all.css">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
</head>
<body>
	<div class="modal fade" id="formBase" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">Socioeconómico Base</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<form id="datosBasico">
		        		<p class="text-center p-3 mb-2 bg-danger text-white">DATOS GENERALES</p><br>
			        	<div class="row">
			        		<div class="col-md-3 offset-md-2">
			        			<label for="fecha">Fecha de visita *</label>
			        			<input type="text" name="fecha" id="fecha" class="form-control base_requerido" placeholder="dd/mm/aaaa" disabled>
			        			<br>
			        		</div>
			        		<div class="col-md-5">
			        			<label for="puesto">Puesto que solicita o desempeñado *</label>
			        			<input type="text" class="form-control base_requerido" name="puesto" id="puesto" placeholder="Puesto que solicita">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="nombre">Nombre(s) del aspirante *</label>
			        			<input type="text" class="form-control base_requerido" name="nombre" id="nombre" placeholder="Nombre(s) del aspirante">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="paterno">Apellido paterno *</label>
			        			<input type="text" class="form-control base_requerido" name="paterno" id="paterno" placeholder="Apellido paterno">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="materno">Apellido materno *</label>
			        			<input type="text" class="form-control base_requerido" name="materno" id="materno" placeholder="Apellido materno">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="fecha_nacimiento">Fecha de nacimiento *</label>
			        			<input type="text" class="form-control solo_lectura base_requerido" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="dd/mm/aaaa" readonly>
			        			<br>
			        		</div>
			        		<div class="col-md-2">
			        			<label for="edad">Edad *</label>
			        			<input type="text" class="form-control base_requerido" name="edad" id="edad" placeholder="Edad" readonly>
			        			<br>
			        		</div>
			        		<div class="col-md-6">
			        			<label for="lugar_nacimiento">Lugar de nacimiento *</label>
			        			<input type="text" class="form-control base_requerido" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Lugar de nacimiento">
			        			<br>
			        		</div>		
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="genero">Género *</label>
			        			<select name="genero" id="genero" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($generos as $g) {?>
						                <option value="<?php echo $g->id; ?>"><?php echo $g->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="civil">Estado civil *</label>
			        			<select name="civil" id="civil" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($estados_civiles as $civil) {?>
						                <option value="<?php echo $civil->id; ?>"><?php echo $civil->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="estudios">Grado máximo de estudios *</label>
			        			<select name="estudios" id="estudios" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($estudios as $est) {?>
						                <option value="<?php echo $est->id; ?>"><?php echo $est->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-3">
			        			<label for="telefono">Teléfono fijo o celular *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="telefono" id="telefono" placeholder="Teléfono" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-9">
			        			<label for="calle">Calle *</label>
			        			<input type="text" class="form-control base_requerido" name="calle" id="calle" placeholder="Calle">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-3">
			        			<label for="exterior">No. Exterior *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="exterior" id="exterior" placeholder="No. Ext." maxlength="6">
			        			<br>
			        		</div>
			        		<div class="col-md-3">
			        			<label for="interior">No. Interior </label>
			        			<input type="text" class="form-control" name="interior" id="interior" placeholder="No. Int." maxlength="5">
			        			<br>
			        		</div>
			        		<div class="col-md-2">
			        			<label for="cp">C.P. *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="cp" id="cp" placeholder="C.P." maxlength="5">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="colonia">Colonia *</label>
			        			<input type="text" class="form-control base_requerido" name="colonia" id="colonia" placeholder="Colonia">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="estado">Estado *</label>
			        			<select name="estado" id="estado" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($estados as $e) {?>
						                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="municipio">Municipio *</label>
			        			<select name="municipio" id="municipio" class="form-control base_requerido" disabled>
						            <option value="-1">Selecciona</option>
					          	</select>
					          	<br>
			        		</div>
			        	</div>
				        <p class="text-center p-3 mb-2 bg-danger text-white">DATOS COMPROBATORIOS</p><br>
				        <p class="subtitulo_form">Acta de Nacimiento</p>
				        <div class="row">
				        	<div class="col-md-4">
				        		<label for="fecha_acta">Fecha de registro *</label>
			        			<input type="text" class="form-control solo_lectura base_requerido" name="fecha_acta" id="fecha_acta" placeholder="dd/mm/aaaa" readonly>
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="acta">No. Acta *</label>
			        			<input type="text" class="form-control base_requerido" name="acta" id="acta" placeholder="No. Acta" maxlength="8">
			        			<br>
				        	</div>
				        	<div class="col-md-5">
				        		<label for="localidad">Localidad </label>
			        			<input type="text" class="form-control" name="localidad" id="localidad" placeholder="Localidad">
			        			<br>
				        	</div>
				        </div>
				        <p class="subtitulo_form">Comprobante de domicilio</p>
				        <div class="row">
				        	<div class="col-md-4">
				        		<label for="fecha_vencimiento">Fecha de vencimiento *</label>
			        			<input type="text" class="form-control solo_lectura base_requerido" name="fecha_vencimiento" id="fecha_vencimiento" placeholder="dd/mm/aaaa" readonly>
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="cuenta_domicilio">No. de Cuenta *</label>
			        			<input type="text" class="form-control base_requerido" name="cuenta_domicilio" id="cuenta_domicilio" placeholder="No. de Cuenta">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="comprobante">Tipo de comprobante *</label>
			        			<select name="comprobante" id="comprobante" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($comprobantes as $comp) {?>
						                <option value="<?php echo $comp->id; ?>"><?php echo $comp->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        </div>
				        <p class="subtitulo_form">Otros datos</p>
				        <div class="row">
				        	<div class="col-md-4">
				        		<label for="emision_ine">Año de emisión INE (IFE) *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="emision_ine" id="emision_ine" placeholder="Año" maxlength="4">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="ine">INE (IFE) *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="ine" id="ine" placeholder="INE (IFE)">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="curp">CURP *</label>
			        			<input type="text" class="form-control base_requerido" name="curp" id="curp" placeholder="CURP" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" maxlength="18">
			        			<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-4">
				        		<label for="nss">Afiliación al IMSS (NSS) *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="nss" id="nss" placeholder="Afiliación al IMSS (NSS)" maxlength="11">
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="fecha_licencia">Vigencia licencia para conducir  </label>
			        			<input type="text" class="form-control solo_lectura" name="fecha_licencia" id="fecha_licencia" placeholder="dd/mm/aaaa" readonly>
			        			<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="codigo_licencia">No. Licencia para conducir </label>
			        			<input type="text" class="form-control" name="codigo_licencia" id="codigo_licencia" placeholder="Código de licencia" maxlength="16" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
				        	</div>
				        </div>
				        <p class="text-center p-3 mb-2 bg-danger text-white">DATOS, BIENES Y ANTECEDENTES LABORALES DE LAS PERSONAS QUE HABITAN LA CASA</p><br>
				        <div class="row">
				        	<div class="col-md-4 offset-md-4">
				        		<label for="num_personas">No. de personas que habitan *</label>
			        			<input type="text" class="form-control solo_numeros" name="num_personas" id="num_personas" placeholder="No. de personas" maxlength="2" oninput="generarPersonas()">
			        			<br>
				        	</div>
				        </div>
				        <div id="personas">
				        	
				        </div>
				        <p class="text-center p-3 mb-2 bg-danger text-white">SITUACIÓN ECONÓMICA</p><br>
				        <div class="row">
				        	<div class="col-md-3">
				        		<label for="casa">La casa es *</label>
			        			<select name="casa" id="casa" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($casa as $ca) {?>
						                <option value="<?php echo $ca->id; ?>"><?php echo $ca->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="casa_pago">¿Cuánto paga mensualmente?</label>
			        			<input type="text" class="form-control" name="casa_pago" id="casa_pago" placeholder="Pago mensual" maxlength="8" onkeyup="checkDecimales(this);" disabled>
			        			<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-3">
				        		<label for="gas">Gas *</label>
			        			<input type="text" class="form-control base_requerido" name="gas" id="gas" placeholder="Gas" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="luz">Luz *</label>
			        			<input type="text" class="form-control base_requerido" name="luz" id="luz" placeholder="Luz" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="agua">Agua potable *</label>
			        			<input type="text" class="form-control base_requerido" name="agua" id="agua" placeholder="Agua potable" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="internet">Tel/Internet/Cable *</label>
			        			<input type="text" class="form-control base_requerido" name="internet" id="internet" placeholder="Tel/Internet/Cable" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-3">
				        		<label for="mantenimiento">Mantenimiento</label>
			        			<input type="text" class="form-control" name="mantenimiento" id="mantenimiento" placeholder="Mantenimiento" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="despensa">Despensa mensual *</label>
			        			<input type="text" class="form-control base_requerido" name="despensa" id="despensa" placeholder="Despensa mensual" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="transporte">Transporte mensual *</label>
			        			<input type="text" class="form-control base_requerido" name="transporte" id="transporte" placeholder="Transporte mensual" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="otros_pagos">Otros</label>
			        			<input type="text" class="form-control" name="otros_pagos" id="otros_pagos" placeholder="Otros" maxlength="8" onkeyup="checkDecimales(this);">
			        			<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-12">
				        		<label for="solucion_gastos">Cuando tus gastos son mayores a lo que ganas, ¿cómo los solucionas? *</label>
			        			<input type="text" class="form-control base_requerido" name="solucion_gastos" id="solucion_gastos" placeholder="Cuando tus gastos son mayores a lo que ganas, ¿cómo los solucionas?">
			        			<br>
				        	</div>
				        </div>
				        <p class="text-center p-3 mb-2 bg-danger text-white">HABITACIÓN Y MEDIO AMBIENTE</p><br>
				        <div class="row">
				        	<div class="col-md-3">
				        		<label for="anos_casa">Años *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="anos_casa" id="anos_casa" placeholder="Años" maxlength="2">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="meses_casa">Meses *</label>
			        			<select name="meses_casa" id="meses_casa" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php for ($i = 0; $i <= 11; $i++) {?>
						                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="nivel_zona">Nivel de zona *</label>
			        			<select name="nivel_zona" id="nivel_zona" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($zonas as $z) {?>
						                <option value="<?php echo $z->id; ?>"><?php echo $z->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="tipo_vivienda">Tipo de vivienda *</label>
			        			<select name="tipo_vivienda" id="tipo_vivienda" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($viviendas as $v) {?>
						                <option value="<?php echo $v->id; ?>"><?php echo $v->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-2">
				        		<label for="recamaras">Recámaras *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="recamaras" id="recamaras" placeholder="Recámaras" maxlength="2">
			        			<br>
				        	</div>
				        	<div class="col-md-2">
				        		<label for="banos">Baños *</label>
			        			<input type="text" class="form-control solo_numeros base_requerido" name="banos" id="banos" placeholder="Baños" maxlength="2">
			        			<br>
				        	</div>
				        	<div class="col-md-3">
				        		<div class="div_checkbox">
				        			<label>
				                      <input type="checkbox" class="minimal" id="cuarto" name="cuarto">   
				                      Cuarto de servicio
				                    </label>
				        		</div>
				        	</div>
				        	<div class="col-md-2">
				        		<div class="div_checkbox">
				        			<label>
				                      <input type="checkbox" class="minimal" id="cocina" name="cocina">   
				                      Cocina
				                    </label>
				        		</div>
				        	</div>
				        	<div class="col-md-2">
				        		<div class="div_checkbox">
				        			<label>
				                      <input type="checkbox" class="minimal" id="comedor" name="comedor">   
				                      Comedor
				                    </label>
				        		</div>
				        	</div>
				        </div>
				        <div class="row margen_fondo">
				        	<div class="col-md-3">
				        		<div class="div_checkbox">
				        			<label>
				                      <input type="checkbox" class="minimal" id="patio" name="patio">   
				                      Patio de servicio
				                    </label>
				        		</div>
				        	</div>
				        	<div class="col-md-2">
				        		<div class="div_checkbox">
				        			<label>
				                      <input type="checkbox" class="minimal" id="jardin" name="jardin">   
				                      Jardín
				                    </label>
				        		</div>
				        	</div>
				        	<div class="col-md-2">
				        		<div class="div_checkbox">
				        			<label>
				                      <input type="checkbox" class="minimal" id="garaje" name="garaje">   
				                      Garaje
				                    </label>
				        		</div>
				        	</div>
				        	<div class="col-md-2">
				        		<div class="div_checkbox">
				        			<label>
				                      <input type="checkbox" class="minimal" id="sala" name="sala">   
				                      Sala
				                    </label>
				        		</div>
				        	</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-5">
				        		<label for="mobiliario">Mobiliario *</label>
			        			<select name="mobiliario" id="mobiliario" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($muebles as $mue) {?>
						                <option value="<?php echo $mue->id; ?>"><?php echo $mue->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        	<div class="col-md-3">
				        		<label for="suficiencia">La vivienda es *</label>
			        			<select name="suficiencia" id="suficiencia" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <option value="1">Suficiente</option>
						            <option value="0">Insuficiente</option>
					          	</select>
					          	<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="condiciones">Condiciones *</label>
			        			<select name="condiciones" id="condiciones" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($condiciones as $cond) {?>
						                <option value="<?php echo $cond->id; ?>"><?php echo $cond->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        </div>
				        <p class="text-center p-3 mb-2 bg-danger text-white">REFERENCIAS VECINALES</p><br>
				        <p class='subtitulo_form'>Referencia 1</p>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="v1_nombre">Nombre *</label>
			        			<input type="text" class="form-control base_requerido" name="v1_nombre" id="v1_nombre" placeholder="Nombre">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v1_telefono">Teléfono fijo o celular</label>
			        			<input type="text" class="form-control solo_numeros" name="v1_telefono" id="v1_telefono" placeholder="Teléfono" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v1_calle">Calle *</label>
			        			<input type="text" class="form-control base_requerido" name="v1_calle" id="v1_calle" placeholder="Calle">
			        			<br>
			        		</div>
			        	</div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="v1_colonia">Colonia *</label>
			        			<input type="text" class="form-control base_requerido" name="v1_colonia" id="v1_colonia" placeholder="Colonia">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v1_estado">Estado *</label>
			        			<select name="v1_estado" id="v1_estado" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($estados as $e) {?>
						                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v1_municipio">Municipio *</label>
			        			<select name="v1_municipio" id="v1_municipio" class="form-control base_requerido" disabled>
						            <option value="-1">Selecciona</option>
					          	</select>
					          	<br>
			        		</div>
			        	</div>
			        	<p class='subtitulo_form'>Referencia 2</p>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="v2_nombre">Nombre *</label>
			        			<input type="text" class="form-control base_requerido" name="v2_nombre" id="v2_nombre" placeholder="Nombre">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v2_telefono">Teléfono fijo o celular </label>
			        			<input type="text" class="form-control solo_numeros" name="v2_telefono" id="v2_telefono" placeholder="Teléfono" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v2_calle">Calle *</label>
			        			<input type="text" class="form-control base_requerido" name="v2_calle" id="v2_calle" placeholder="Calle">
			        			<br>
			        		</div>
			        	</div>
				        <div class="row">
			        		<div class="col-md-4">
			        			<label for="v2_colonia">Colonia *</label>
			        			<input type="text" class="form-control base_requerido" name="v2_colonia" id="v2_colonia" placeholder="Colonia">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v2_estado">Estado *</label>
			        			<select name="v2_estado" id="v2_estado" class="form-control base_requerido">
						            <option value="-1">Selecciona</option>
						            <?php foreach ($estados as $e) {?>
						                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v2_municipio">Municipio *</label>
			        			<select name="v2_municipio" id="v2_municipio" class="form-control base_requerido" disabled>
						            <option value="-1">Selecciona</option>
					          	</select>
					          	<br>
			        		</div>
			        	</div>
			        </form>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" id="terminarFormBase">Terminar Formulario</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-rodi">
		  	<a class="navbar-brand" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		FORMULARIOS
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<?php
			    	foreach ($forms as $f): ?>
			    		<li class="nav-item">
				        	<a class="nav-link" href="javascript:void(0)" id="<?php echo $f->link; ?>"><?php echo $f->nombre; ?></a>
				      	</li>
			    	<?php endforeach;
			    	?>
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $nombre." ".$paterno; ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="<?php echo base_url(); ?>index.php/Login/cerrarSesion">Cerrar Sesión</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<section>
		
	</section>
	<script src="<?php echo base_url() ?>js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/formularios.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>js/icheck.js"></script>
	<script>
	//Obtiene los municipios dependiendo del id estado
  	$("#estado").change(function(){
      var id_estado = $(this).val();
      if(id_estado != -1){
        $.ajax({
          url: '<?php echo base_url('index.php/Formulario/getMunicipios'); ?>',
          method: 'POST',
          data: {'id_estado':id_estado},
          dataType: "text",
          success: function(res)
          {
            $('#municipio').prop('disabled', false);
            $('#municipio').html(res);
          },error:function(res)
          {
            //$('#errorModal').modal('show');
          }
        });
      }
      else{
        $('#municipio').prop('disabled', true);
        $('#municipio').append($("<option selected></option>").attr("value",-1).text("Selecciona"));
      }
    });
    $("#v1_estado").change(function(){
      var id_estado = $(this).val();
      if(id_estado != -1){
        $.ajax({
          url: '<?php echo base_url('index.php/Formulario/getMunicipios'); ?>',
          method: 'POST',
          data: {'id_estado':id_estado},
          dataType: "text",
          success: function(res)
          {
            $('#v1_municipio').prop('disabled', false);
            $('#v1_municipio').html(res);
          },error:function(res)
          {
            //$('#errorModal').modal('show');
          }
        });
      }
      else{
        $('#v1_municipio').prop('disabled', true);
        $('#v1_municipio').append($("<option selected></option>").attr("value",-1).text("Selecciona"));
      }
    });
    $("#v2_estado").change(function(){
      var id_estado = $(this).val();
      if(id_estado != -1){
        $.ajax({
          url: '<?php echo base_url('index.php/Formulario/getMunicipios'); ?>',
          method: 'POST',
          data: {'id_estado':id_estado},
          dataType: "text",
          success: function(res)
          {
            $('#v2_municipio').prop('disabled', false);
            $('#v2_municipio').html(res);
          },error:function(res)
          {
            //$('#errorModal').modal('show');
          }
        });
      }
      else{
        $('#v2_municipio').prop('disabled', true);
        $('#v2_municipio').append($("<option selected></option>").attr("value",-1).text("Selecciona"));
      }
    });
    $("#casa").change(function(){
    	var opcion = $(this).val();
    	if(opcion > 1){
    		$("#casa_pago").prop('disabled', false);
    		$("#casa_pago").addClass('base_requerido');
    	}
    	else{
    		$("#casa_pago").val("");
    		$("#casa_pago").prop('disabled', true);
    		$("#casa_pago").removeClass('base_requerido');
    	}
    });
    //Genera los campos dependiendo del número que se escriba
	function generarPersonas(){
		var num = $("#num_personas").val();
		var bloque = "";
		if(num >= 1){
			for(i = 1; i <= num; i++){
				bloque += "<p class='subtitulo_form'>Persona "+i+"</p><div class='row'><div class='col-md-7'><label>Nombre completo *</label><input type='text' class='form-control base_requerido' name='p_nombre"+i+"' id='p_nombre"+i+"' placeholder='Nombre completo'><br></div><div class='col-md-3'><label>Parentesco *</label><select name='p_parentesco"+i+"' id='p_parentesco"+i+"' class='form-control base_requerido'><option value='-1'>Selecciona</option><option value='1'>Padre</option><option value='2'>Madre</option><option value='3'>Hijo(a)</option><option value='4'>Cónyuge</option><option value='5'>Abuelo(a)</option><option value='6'>Hermano(a)</option><option value='7'>Nieto(a)</option><option value='8'>Suegro(a)</option><option value='9'>Yerno/Nuera</option><option value='10'>Cuñado(a)</option><option value='11'>Pareja</option><option value='12'>Roomie</option></select><br></div></div><div class='row'><div class='col-md-3'><label>Edad *</label><input type='number' class='form-control base_requerido' name='p_edad"+i+"' id='p_edad"+i+"' placeholder='Edad' min='1' max='99'><br></div><div class='col-md-3'><label>Estado civil *</label><select name='p_civil"+i+"' id='p_civil"+i+"' class='form-control base_requerido'><option value='-1'>Selecciona</option><option value='1'>Casado(a)</option><option value='3'>Divorciado(a)</option><option value='6'>Separado(a)</option><option value='2'>Soltero(a)</option><option value='4'>Unión libre</option><option value='5'>Viudo(a)</option></select><br></div><div class='col-md-3'><label>Escolaridad *</label><select name='p_escolaridad"+i+"' id='p_escolaridad"+i+"' class='form-control base_requerido'><option value='-1'>Selecciona</option><option value='1'>Preescolar</option><option value='2'>Primaria</option><option value='3'>Secundaria</option><option value='4'>Preparatoria/Bachillerato</option><option value='5'>Profesional técnico</option><option value='6'>Técnico superior</option><option value='7'>Licenciatura</option><option value='8'>Maestría</option><option value='9'>Doctorado</option></select><br></div><div class='col-md-3'><label>¿Depende de usted? *</label><select name='p_depende"+i+"' id='p_depende"+i+"' class='form-control base_requerido'><option value='-1'>Selecciona</option><option value='0'>No</option><option value='2'>Sí</option></select><br></div></div><div class='row'>	<div class='col-md-8'><label>Empresa para la que trabaja *</label><input type='text' class='form-control base_requerido' name='p_empresa"+i+"' id='p_empresa"+i+"' placeholder='Empresa'><br></div><div class='col-md-4'><label>Puesto *</label><input type='text' class='form-control base_requerido' name='p_puesto"+i+"' id='p_puesto"+i+"' placeholder='Puesto'><br></div></div><div class='row'><div class='col-md-3'><label>Sueldo mensual *</label><input type='number' class='form-control base_requerido' name='p_sueldo"+i+"' id='p_sueldo"+i+"' placeholder='Sueldo mensual'><br></div><div class='col-md-3'><label>Antigûedad (años) *</label><input type='number' class='form-control base_requerido' name='p_antiguedad"+i+"' id='p_antiguedad"+i+"' placeholder='Antigûedad'><br></div><div class='col-md-4'><label>¿Cuánto aporta a esta casa? *</label><input type='number' class='form-control base_requerido' name='p_aporta"+i+"' id='p_aporta"+i+"' placeholder='¿Cuánto aporta a esta casa?'><br></div></div><div class='row'><div class='col-md-6'><label>Muebles y/o inmubles *</label><input type='text' class='form-control base_requerido' name='p_mueble"+i+"' id='p_mueble"+i+"' placeholder='Muebles y/o inmubles'><br></div><div class='col-md-3'>		<label>¿Tiene adeudo? *</label><select name='p_adeudo"+i+"' id='p_adeudo"+i+"' class='form-control base_requerido'><option value='-1'>Selecciona</option><option value='0'>No</option><option value='1'>Sí</option></select><br></div></div>";
			}
			$("#personas").html(bloque);
		}
	}
	</script>
</body>
</html>