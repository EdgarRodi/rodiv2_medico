<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>HCL | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/cliente/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-datetimepicker.min.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<!-- DataTable -->
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"-->
	<!--link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"-->

</head>
<body>
	<div id="exito" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong> The candidate has been add succesfully
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong><p id="texto_msj"></p>
  	</div>
	<div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">New Register</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<form id="datos">
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="nombre">Name *</label>
			        			<input type="text" class="form-control obligado" name="nombre" id="nombre" placeholder="Name" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="paterno">First lastname *</label>
			        			<input type="text" class="form-control obligado" name="paterno" id="paterno" placeholder="First lastname" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="materno">Second lastname </label>
			        			<input type="text" class="form-control" name="materno" id="materno" placeholder="Second lastname" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>			    
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="correo">Email *</label>
			        			<input type="email" class="form-control obligado" name="correo" id="correo" placeholder="Email" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="celular">Cell phone number *</label>
			        			<input type="text" class="form-control solo_numeros obligado" name="celular" id="celular" placeholder="Cell phone number" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="fijo">Home Number </label>
			        			<input type="text" class="form-control" name="fijo" id="fijo" placeholder="Home Number" maxlength="10">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="fecha_nacimiento">Birthdate</label>
			        			<input type="text" class="form-control solo_lectura" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="mm/dd/yyyy" readonly>
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="proyecto">Project *</label>
			        			<select name="proyecto" id="proyecto" class="form-control obligado">
						            <option value="">Select</option>
						            <?php 
						            	foreach ($proyectos as $pro) { ?>
						                	<option value="<?php echo $pro->id; ?>"><?php echo $pro->nombre; ?></option>
						            <?php 	
					        			}?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="examen">Drug test *</label>
			        			<select name="examen" id="examen" class="form-control obligado">
						            <option value="">Select</option>
					          	</select>
					          	<br>
			        		</div>			        		
			        	</div>
			        </form>
			        <div id="campos_vacios" class="alert alert-danger">
		      			<p class="msj_error">There are empty fields</p>
		      		</div>
		      		<div id="correo_invalido" class="alert alert-danger">
		      			<p class="msj_error">The email is not valid</p>
		      		</div>
		      		<div id="repetido">
		      			<p class="msj_error" class="alert alert-danger">The name and/or email of the candidate already exists</p>
		      		</div>
		      		<div id="alert-msg">
						
					</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-success" id="registrar">Create</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="passModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">Password Generated</h5>
			        
		      	</div>
		      	<div class="modal-body">
		        	<h4>Data login on form</h4><br>
		        	<p><b>User: </b><span id="user"></span></p>
		        	<p><b>Password: </b><span id="pass"></span></p><br>
		        	<p id="respuesta_mail"></p>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	        		<div class="row" id="div_commentario">
	        			<div class="col-md-12">
	        				<label for="motivo">Comment *</label>
	        				<textarea name="motivo" id="motivo" class="form-control" rows="3"></textarea>
	        				<br>
	        			</div>
	        		</div>
	        		<div class="msj_error">
	        			<p id="msg_accion"></p>
	        		</div>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      		<button type="button" class="btn btn-danger" id="btnGuardar" onclick="ejecutarAccion()">Accept</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="statusModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Candidate Status</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<p id="nombreCandidato" class="text-center"></p>
		      		<div id="div_status"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Follow up of the candidate's study </h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_avances"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="llamadasModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Phone calls to candidate</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_llamadas"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="emailsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Emails to candidate</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_emails"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Candidate comments</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="comentario_candidato"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="documentosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-xl modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Candidate documents</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p id="lista_documentos"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="ofacModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">OFAC and OIG Verifications</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div class="row">
		      			<div class="col-md-4 offset-md-1">
		      				<p class="text-center"><b>Candidate name</b></p>
		      				<p class="text-center" id="ofac_nombrecandidato"></p>
		      			</div>
            			<div class="col-md-4 offset-md-2">
                        	<p class="text-center" id="fecha_titulo_ofac"><b></b></p>
                            <p class="text-center" id="fecha_estatus_ofac"></p>
                        </div>
            		</div>
        			<div class="row">
                        <div class="col-md-6 borde_gris">
                            <p id="estatus_ofac"></p><br>
                            <span id="res_ofac"></span>
			          		<br><br>
                        </div>
                        <div class="col-md-6">
                            <p id="estatus_oig"></p><br>
                            <span id="res_oig"></span>
			          		<br><br>
                        </div>
                    </div>
                    
                    
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-rodi">
		  	<a class="navbar-brand" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		<?php echo strtoupper($this->session->userdata('subcliente')); ?>
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item">
			        	<a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#newModal"><i class="fas fa-user-plus icon"></i> Add Candidate</a>
			      	</li>
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno'); ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="<?php echo base_url(); ?>Login/logout">Sign out</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="div_titulo">
						<p class="titulo">Table of Candidate Records</p>
					</div>
				</div>
			</div>
			<div class="row tabla">
				<div class="col-lg-12">
					<input type="hidden" class="idCandidato">
					<input type="hidden" class="correo">
					<table id="tabla" class="table stripe hover row-border cell-border table-responsive"></table>
				</div>
			</div>
		</div>
	</section>
	<script src="<?php echo base_url() ?>js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/clients.js"></script>

	<script src="<?php echo base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>js/icheck.js"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<script>
    $(document).ready(function(){
    	<?php $id_subcliente = $this->session->userdata('idsubcliente'); ?>
    	var subcliente = '<?php echo $id_subcliente ?>';
    	var url = '<?php echo base_url('Candidato/getCandidatosSubcliente?id_subcliente='); ?>'+subcliente;
		changeDatatable(url);
		$("#proyecto").change(function(){
	      	var id_proyecto = $(this).val();
	      	if(id_proyecto != "" && id_proyecto != 0){
	        	$.ajax({
	          		url: '<?php echo base_url('Subcliente/getPaqueteSubclienteProyecto'); ?>',
	          		method: 'POST',
	          		data: {'id_proyecto':id_proyecto},
	          		dataType: "text",
	          		success: function(res)
	          		{
	          			if(res != ""){
	          				//$('#examen').prop('disabled', false);
	            			$('#examen').html(res);
	          			}
	          		}
	        	});
	      	}
	      	else{
	      		$('#examen').empty();
	      		$('#examen').append($("<option selected></option>").attr("value","").text("Select"));
	      	}	      	
	    });	
		//Crear registro
		$("#registrar").click(function(){
			var datos = new FormData();
			var correo = $("#correo").val();
			datos.append('correo', $("#correo").val());
			datos.append('nombre', $("#nombre").val());
			datos.append('paterno', $("#paterno").val());
			datos.append('materno', $("#materno").val());
			datos.append('celular', $("#celular").val());
			datos.append('fijo', $("#fijo").val());
			datos.append('fecha_nacimiento', $("#fecha_nacimiento").val());
			datos.append('proyecto', $("#proyecto").val());
			datos.append('examen', $("#examen").val());
			//console.log($("#ine")[0].files[0])
			var totalVacios = $('.obligado').filter(function(){
	        	return !$(this).val();
	      	}).length;
	      
	      	if(totalVacios > 0){
	        	$(".obligado").each(function() {
	          		var element = $(this);
	          		if (element.val() == "") {
	            		element.addClass("requerido");
	            		$("#newModal #campos_vacios").css('display','block');
			            setTimeout(function(){
			              $('#newModal #campos_vacios').fadeOut();
			            },4000);
	          		}
	          		else{
	            		element.removeClass("requerido");
	          		}
	        	});
	      	}
	      	else{
	      		if(!isEmail(correo) && correo != ""){
          			$('#campos_vacios').css("display", "none");
          			$('#correo_invalido').css("display", "block");
          			$("#correo").addClass("requerido");
          			setTimeout(function(){
            			$("#correo_invalido").fadeOut();
          			},4000);
        		}
        		else{
        			$.ajax({
		              	url: '<?php echo base_url('Subcliente/addCandidate'); ?>',
		              	type: 'POST',
		              	data: datos,
		              	contentType: false,  
		     			cache: false,  
		     			processData:false,
		              	beforeSend: function() {
		                	$('.loader').css("display","block");
		              	},
		              	success : function(res){ 
		              		if(res == 0){
			                  	$('.loader').fadeOut();
		              			$('#campos_vacios').css("display", "none");
			          			$('#correo_invalido').css("display", "none");
			          			$('#repetido').css("display", "block");
			          			setTimeout(function(){
			            			$("#repetido").fadeOut();
			          			},4000);
		              		}
		              		if(res.substring(0,4) == "Sent"){
		              			dato = res.split('@@');
	              				setTimeout(function(){
			                  		$('.loader').fadeOut();
			                	},300);
		                		$("#newModal").modal('hide');
		                		recargarTable();
		                		$("#exito").css('display','block');
		                		$("#user").text(correo);
		                		$("#pass").text(dato[1]);
		                		$("#respuesta_mail").text("* A email has been sent with this credentials to candidate. This email could take a few minutes to be delivered.");
		                		$("#passModal").modal('show');
		                		setTimeout(function(){
			                  		$('#exito').fadeOut();
			                	},3000);
		              		}
		              		if(res == "creado"){
	              				setTimeout(function(){
			                  		$('.loader').fadeOut();
			                	},300);
		                		$("#newModal").modal('hide');
		                		recargarTable();
		                		$("#exito").css('display','block');
		                		setTimeout(function(){
			                  		$('#exito').fadeOut();
			                	},3000);
		              		}
		              		if(res != 0 && res.substring(0,4) != "Sent" && res != "creado"){
		              			//var dato = res.split('@@');
		              			setTimeout(function(){
			                  		$('.loader').fadeOut();
			                  	},200);
			                  	recargarTable();
			                  	$("#alert-msg").css('display','block');
		              			$('#alert-msg').html('<div class="alert alert-danger">' + res + '</div>');
		              		}
		              	}
	            	});
        		}
	      	}
		});

    });
	function changeDatatable(url){
		$('#tabla').DataTable({
			"pageLength": 10,
	      	"pagingType": "simple",
	      	"order": [8, "asc"],
	      	//"stateSave": true,
	      	"serverSide": false,
	      	"ajax": url,
	      	"bDestroy": true,
	      	"columns":[ 
		        { 	title: 'Name', data: 'nombreCompleto',
		        	mRender: function(data, type, full){
		        		return full.nombre+"<br>"+full.paterno+" "+full.materno;
		        	}
		        },
		        { 	title: 'Project', data: 'proyecto' },
		        { 	title: 'Register Date', data: 'fecha_alta', "width": "10%",
			        mRender: function(data, type, full){
			            var aux = data.split(" ");
			            var f = aux[0].split("-");
			            var  fecha = f[1]+"/"+f[2]+"/"+f[0];
			            var h = aux[1].split(':');
			            var hora = h[0]+":"+h[1];
			            var tiempo = fecha+" "+hora;
			            return tiempo;
	          		}
	          	},
	          	{ title: 'Form Date', data: 'fecha_contestado', "width": "10%",
	        		mRender: function(data, type, full){
	        			if(full.id_proyecto != 25){
		        			if(data == "" || data == null){
		        				return "<i class='fas fa-circle estatus0'></i>Pending";
		        			}
		        			else{
		        				var f = data.split(' ');
	            				var h = f[1];
	            				var aux = h.split(':');
	            				var hora = aux[0]+':'+aux[1];
	            				var aux = f[0].split('-');
	            				var fecha = aux[1]+"/"+aux[2]+"/"+aux[0];
	            				var tiempo = fecha+' '+hora;
	        					return "<i class='fas fa-circle estatus1'></i>"+tiempo;
		        			}
		        		}
		        		else{
		        			return "<i class='fas fa-circle status_bgc0'></i> N/A";
		        		}
          			}
	        	},
	        	{ title: 'Documents Date', data: 'fecha_documentos', "width": "10%",
	        		mRender: function(data, type, full){
	        			if(full.id_proyecto != 25){
		        			if(data == "" || data == null){
		        				return "<i class='fas fa-circle estatus0'></i>Pending";
		        			}
		        			else{
		        				var f = data.split(' ');
	            				var h = f[1];
	            				var aux = h.split(':');
	            				var hora = aux[0]+':'+aux[1];
	            				var aux = f[0].split('-');
	            				var fecha = aux[1]+"/"+aux[2]+"/"+aux[0];
	            				var tiempo = fecha+' '+hora;
	        					return "<i class='fas fa-circle estatus1'></i>"+tiempo;
		        			}
		        		}
		        		else{
		        			return "<i class='fas fa-circle status_bgc0'></i> N/A";
		        		}
          			}
	        	},
	          	{
		          	title: 'Candidate verifications',
		          	data: 'id',
		          	"width": "8%",

		          	bSortable: false, 
		          	mRender: function(data, type, full) {
		          		if(full.id_tipo_proceso == 1){
			          		if(full.status == 0){
			        			return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Follow up of the candidate" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Phone calls to candidate" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Emails to candidate" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
			        		}
			        		else{
			        			return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Follow up of the candidate" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Phone calls to candidate" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Emails to candidate" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="View status" id="ver" class="fa-tooltip a-acciones"><i class="fas fa-eye"></i></a>';
			        		}
			        	}
			        	else{
			        		if(full.status == 0){
			        			return '<a href="javascript:void(0)" id="ofac" data-toggle="tooltip" title="OFAC and OIG verifications" class="fa-tooltip a-acciones"><i class="fas fa-eye"></i></a><a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Phone calls to candidate" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Emails to candidate" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
			        		}
			        		else{
			        			return '<a href="javascript:void(0)" id="ofac" data-toggle="tooltip" title="OFAC and OIG verifications" class="fa-tooltip a-acciones"><i class="fas fa-eye"></i></a><a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Phone calls to candidate" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Emails to candidate" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
			        		}
			        	}
		          	}
		        },
        		{ title: 'Antidoping', data: 'id', bSortable: false, "width": "10%",
         			mRender: function(data, type, full) {
         				if(full.id_proyecto != 21){
         					if(full.fecha_resultado != null && full.fecha_resultado != ""){
	         					if(full.resultado_doping == 1){
	         						return "<i class='fas fa-circle status_bgc2'></i>Not approved"; 
	         					}
	         					else{
	         						return "<i class='fas fa-circle status_bgc1'></i>Approved";
	         					}
	         					
	         				}
	         				else{
	         					return "<i class='fas fa-circle status_bgc0'></i>Pending";
	         				}
         				}
         				else{
         					return "<i class='fas fa-circle status_bgc0'></i>N/A";
         				}
          			}
        		},
		        { 	title: 'BGV Status', data: 'status_bgc',bSortable: false, "width": "8%",
		        	mRender: function(data, type, full){
	        			if(full.status == 0){
		        			return "<a href='javascript:void(0)' data-toggle='tooltip' title='Pending form'><i class='fas fa-circle status_bgc0'></i></a>";
		        		}
		        		if(full.status == 1){
		        			return "<a href='javascript:void(0)' data-toggle='tooltip' title='In process by analyst'><i class='fas fa-circle status_bgc3'></i></a>";
		        		}
		        		else{
		        			switch(data){
				            	case '0':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='Undefined'><i class='fas fa-circle status_bgc0'></i></a>";
				            	break;
				            	case '1':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='Positive'><i class='fas fa-circle status_bgc1'></i></a>";
				            	break;
				            	case '2':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='Negative'><i class='fas fa-circle status_bgc2'></i></a>";
				            	break;
				            	case '3':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='For Consideration'><i class='fas fa-circle status_bgc3'></i></a>";
				            	break;
				            }
		        		}
	          		}
		        },
		        {
		          	title: 'Actions',
		          	data: 'id',
		          	"width": "8%",
		          	bSortable: false, 
		          	mRender: function(data, type, full) {
		          		if(full.id_proyecto != 25){
		          			if(full.status_bgc != 0){
		          				if(full.id_proyecto == 20){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLBectonPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 21){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLExxonPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 23){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLNokiaPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 24){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLSempraPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 25){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLPGPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 26){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLPGPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 27){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLUSAAPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 28){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLStandardPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 35){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLCitiPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}

			          		}
			          		else{
			          			if(full.cancelado == 0){
			          				if(full.fecha_contestado != "" && full.fecha_contestado != null && full.fecha_documentos != "" && full.fecha_documentos != null){
			          					return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="2" title="Delete candidate" id="eliminar" class="fa-tooltip a-acciones"><i class="fas fa-trash"></i></a>';
			          				}
			          				else{
			          					return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Generate password" id="generar" class="fa-tooltip a-acciones"><i class="fas fa-key"></i></a><a href="javascript:void(0)" data-toggle="tooltip" data-accion="2" title="Delete candidate" id="eliminar" class="fa-tooltip a-acciones"><i class="fas fa-trash"></i></a>';
			          				}
				          			
				          		}
				          		if(full.cancelado == 1){
				          			return '<a href="javascript:void(0)" data-toggle="tooltip" title="Delete candidate" id="eliminar" class="fa-tooltip a-acciones"><i class="fas fa-trash"></i></a>';
				          		}
			          		}	  
		          		}
		          		else{
         					if(full.fecha_resultado != null && full.fecha_resultado != ""){
	         					return '<div style="display: inline-block;"><form id="pdf'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download test doping" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+full.idDoping+'" value="'+full.idDoping+'"></form></div>';
	         				}
	         				else{
	         					return "<i class='fas fa-circle status_bgc0'></i>Pending";
	         				}
         				}
		          	}
		        }          
      		],
      		
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$("a#ver", row).bind('click', () => {
      				var salida = "";
      				var estudios = "<tr><th>Education </th><th>N/A</th></tr>";
      				var identidad = "<tr><th>Identity </th><th>N/A</th></tr>";
      				var empleo = "<tr><th>Employment History </th><th>N/A</th></tr>";
      				var criminal = "<tr><th>Criminal Court Record Search </th><th>N/A</th></tr>";
      				var global_database = "<tr><th>Global Database Searches </th><th>N/A</th></tr>";
      				var domicilios = "<tr><th>Address History </th><th>N/A</th></tr>";
      				switch(data.id_proyecto){
      					case '20':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
  						case '21':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						break;
      					case '23':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      					case '26':
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						break;
      					case '27':
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						break;
      					case '28':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      					case '35':
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      				}

	      			salida += '<table class="table table-striped">';
			        salida += '<thead>';
			        salida += '<tr>';
			        salida += '<th scope="col">Description</th>';
			        salida += '<th scope="col">Status</th>';
			        salida += '</tr>';
			        salida += '</thead>';
			        salida += '<tbody>';
			        salida += identidad;
			        salida += estudios;
			        salida += empleo;
			        salida += criminal;
			        salida += global_database;
			       	salida += domicilios;
                	salida += '</tbody>';
        			salida += '</table>';

        			$("#nombreCandidato").text("Candidate: "+data.nombreCompleto);
        			$("#div_status").html(salida);
        			$("#statusModal").modal("show");
      			
	      		});
	      		$("a#ofac", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#idCliente").val(data.id_cliente);
	      			$("#ofac_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusOFAC();
	      		});
	      		$('a[id^=pdfOfac]', row).bind('click', () => {
		            var id = data.id;
		            $('#ofacpdf'+id).submit();
		        });
	      		$("a#cancelar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#titulo_accion").text("Cancel candidate");
	      			$("#texto_confirmacion").html("Are you sure you want to cancel <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','cancel');
	      			$("#div_commentario").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
	      		$("a#eliminar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#titulo_accion").text("Delete candidate");
	      			$("#texto_confirmacion").html("Are you sure you want to delete <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','delete');
	      			$("#div_commentario").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
	      		$("a#generar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$(".correo").val(data.correo);
	      			$("#titulo_accion").text("Generate password");
	      			$("#texto_confirmacion").html("Are you sure you want to generate other password for <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','generate');
	      			$("#div_commentario").css('display','none');
	      			$("#quitarModal").modal("show");
	      		});
	      		$('a[id^=pdfFinal]', row).bind('click', () => {
		            var id = data.id;
		            $('#pdf'+id).submit();
		        });
		        $("a#msj_avances", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewAvances'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_avances").html(res);

			            }
			        });
	      			$("#avancesModal").modal("show");
	      		});
		        $("a#llamada", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewLlamadas'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_llamadas").html(res);

			            },error: function(res)
			            {
			              
			            }
			        });
	      			$("#llamadasModal").modal("show");
	      		});
	      		$("a#correoEnviado", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewEmails'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_emails").html(res);

			            },error: function(res)
			            {
			              
			            }
			        });
	      			$("#emailsModal").modal("show");
	      		});
	      		$('a#comentario', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/viewComentario'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
				            	$("#comentario_candidato").html(res);
				            	$("#verModal").modal('show');
			            	}
			            	else{
				            	$("#comentario_candidato").html("No comments");
				            	$("#verModal").modal('show');
			            	}
			            	

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
		        $('a#documentos', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/viewDocumentos'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
			            		$("#lista_documentos").empty();
				            	$("#lista_documentos").html(res);
				            	$("#documentosModal").modal('show');
			            	}
			            	else{
			            		$("#lista_documentos").empty();
				            	$("#lista_documentos").html("<p class='text-center'><b>Documents under review</b></p>");
				            	$("#documentosModal").modal('show');
			            	}
			            	

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
      		},
			"language": {
		        /*"lengthMenu": "Mostrar _MENU_ registros por página",
		        "zeroRecords": "No se encontraron registros",
		        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		        "infoEmpty": "Sin registros disponibles",
		        "infoFiltered": "(Filtrado _MAX_ registros totales)",*/
		        //"sSearch": 'Search: <i class="fas fa-search"></i>',
		        /*"oPaginate": {
		          //"sLast": "Última página",
		          //"sFirst": "Primera",
		          "sNext": "<i class='fa  fa-arrow-right'></i>",
		          "sPrevious": "<i class='fa fa-arrow-left'></i>"
		        }*/
		    }
		});
		$("#tabla").DataTable().search(" ");
	}
	function estatusOFAC(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkOfac'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			$("#fecha_estatus_ofac").empty();
      			$("#estatus_ofac").empty();
      			$("#res_ofac").empty();
      			$("#estatus_oig").empty();
      			$("#res_oig").empty();
  				var datos = res.split('@@');
  				if(datos[0] == 0){
  					$("#fecha_titulo_ofac").html("<b>No date</b>");
  					$("#estatus_ofac").html("<b>OFAC Status: </b>Not defined yet");
	    			$("#res_ofac").html("<b>Result:</b> Not defined yet");
	    			$("#estatus_oig").html("<b>OIG Status: </b>Not defined yet");
	    			$("#res_oig").html("<b>Result:</b> Not defined yet");
  				}
  				else{
  					$("#fecha_titulo_ofac").html("<b>Last update</b>");
  					$("#fecha_estatus_ofac").text(datos[0]);
  					$("#estatus_ofac").html("<b>OFAC Status:</b> "+datos[1]);
	    			var res_ofac = (datos[2] == 1)? "Positive":"Negative";
	    			$("#res_ofac").html("<b>Result:</b> "+res_ofac);
	    			$("#estatus_oig").html("<b>OIG Status:</b> "+datos[3]);
	    			var res_oig = (datos[4] == 1)? "Positive":"Negative";
	    			$("#res_oig").html("<b>Result:</b> "+res_oig);
  				}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#ofacModal").modal("show");
	}
	function ejecutarAccion(){
		var accion = $("#btnGuardar").val();
		var id_candidato = $(".idCandidato").val();
		var id_cliente = '<?php echo $this->session->userdata('idcliente') ?>';
		var correo = $(".correo").val();
		var motivo = $("#motivo").val();
		var usuario = 3;
		if(accion == 'cancel'){
			if(motivo == ""){
				$("#msg_accion").text("The comment is required");
				$("#msg_accion").css('display','block');
				setTimeout(function(){
              		$('#msg_accion').fadeOut();
            	},5000);
			}
			else{
				$.ajax({
	              	url: '<?php echo base_url('Candidato/cancel'); ?>',
	              	type: 'post',
	              	data: {'id_candidato':id_candidato,'motivo':motivo},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
	              		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
	            		$("#quitarModal").modal('hide');
	            		recargarTable();
	            		$("#texto_msj").text('The candidate has been cancelled succesfully');
	            		$("#mensaje").css('display','block');
	            		setTimeout(function(){
	                  		$('#mensaje').fadeOut();
	                	},3000);
	              	},error: function(res){
	                	$('#errorModal').modal('show');
	              	}
	        	});
			}
		}
		if(accion == 'delete'){
			if(motivo == ""){
				$("#msg_accion").text("The reason is required");
				$("#msg_accion").css('display','block');
				setTimeout(function(){
              		$('#msg_accion').fadeOut();
            	},5000);
			}
			else{
				$.ajax({
	              	url: '<?php echo base_url('Candidato/accionCandidato'); ?>',
	              	type: 'post',
	              	data: {'id':id_candidato,'motivo':motivo,'usuario':usuario,'id_cliente':id_cliente},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
	              		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
	            		$("#quitarModal").modal('hide');
	            		recargarTable();
	            		$("#texto_msj").text('The candidate has been deleted succesfully');
	            		$("#mensaje").css('display','block');
	            		setTimeout(function(){
	                  		$('#mensaje').fadeOut();
	                	},3000);
	              	},error: function(res){
	                	$('#errorModal').modal('show');
	              	}
	        	});
			}
		}
		if(accion == 'generate'){
			$.ajax({
              	url: '<?php echo base_url('Candidato/generate'); ?>',
              	type: 'post',
              	data: {'id_candidato':id_candidato,'correo':correo},
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		setTimeout(function(){
                  		$('.loader').fadeOut();
                	},300);
            		$("#quitarModal").modal('hide');
            		$("#user").text(correo);
            		$("#pass").text(res);
            		$("#respuesta_mail").text("* An email has been sent with this credentials to the candidate. This email could take a few minutes to be delivered.");
            		$("#passModal").modal('show');
            		recargarTable();
            		$("#texto_msj").text('The password has been created succesfully');
            		$("#mensaje").css('display','block');
            		setTimeout(function(){
                  		$('#mensaje').fadeOut();
                	},3000);
              	},error: function(res){
                	$('#errorModal').modal('show');
              	}
        	});
		}
	}
    //Verificacion de correo
	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
  	$('#quitarModal').on('hidden.bs.modal', function (e) {
	  	$("#msg_accion").css('display','none');
		$(this)
		    .find("input,textarea")
		       .val('')
		       .end();
	});
	$('#newModal').on('hidden.bs.modal', function (e) {
		$("#alert-msg").empty();
	  	$("#alert-msg").css('display','none');
	  	$("#campos_vacios").css('display','none');
	  	$("#correo_invalido").css('display','none');
	  	$("#repetido").css('display','none');
		$(this)
		    .find("input,select")
		       .val('')
		       .end();
	});
	var hoy = new Date();
  	var dd = hoy.getDate();
  	var mm = hoy.getMonth()+1;
  	var yyyy = hoy.getFullYear();
  	var hora = hoy.getHours()+":"+hoy.getMinutes();

  	if(dd<10) {
      	dd='0'+dd;
  	} 

  	if(mm<10) {
      	mm='0'+mm;
  	}
	$("#fecha_nacimiento").datetimepicker({
  		minView: 2,
    	format: "mm/dd/yyyy",
    	startView: 4,
    	autoclose: true,
    	todayHighlight: true,
    	pickerPosition: "bottom-left",
    	forceParse: false
  	});
  	$('#fecha_nacimiento').datetimepicker('setEndDate', (yyyy - 18)+'-'+'01-01');
	</script>
</body>
</html>