<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>TATA | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/cliente/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-datetimepicker.min.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<!-- DataTable -->
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"-->
	<!--link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"-->

</head>
<body>
	<div id="exito" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong> The candidate has been add succesfully
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong><p id="texto_msj"></p>
  	</div>
	<div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">New Register</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<form id="datos">
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="nombre">Name *</label>
			        			<input type="text" class="form-control obligado" name="nombre" id="nombre" placeholder="Name" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="paterno">First lastname *</label>
			        			<input type="text" class="form-control obligado" name="paterno" id="paterno" placeholder="First lastname" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="materno">Second lastname </label>
			        			<input type="text" class="form-control" name="materno" id="materno" placeholder="Second lastname" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>			    
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="correo">Email *</label>
			        			<input type="email" class="form-control obligado" name="correo" id="correo" placeholder="Email" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="celular">Cell phone number *</label>
			        			<input type="text" class="form-control solo_numeros obligado" name="celular" id="celular" placeholder="Cell phone number" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="fijo">Home Number </label>
			        			<input type="text" class="form-control" name="fijo" id="fijo" placeholder="Home Number" maxlength="10">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="fecha_nacimiento">Birthdate</label>
			        			<input type="text" class="form-control solo_lectura" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="mm/dd/yyyy" readonly>
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="proyecto">Project *</label>
			        			<select name="proyecto" id="proyecto" class="form-control obligado">
						            <option value="">Select</option>
						            <?php 
						            	foreach ($proyectos as $pro) { ?>
						                	<option value="<?php echo $pro->id; ?>"><?php echo $pro->nombre; ?></option>
						            <?php 	
					        			}?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="examen">Drug test *</label>
			        			<select name="examen" id="examen" class="form-control obligado">
						            <option value="">Select</option>
					          	</select>
					          	<br>
			        		</div>			        		
			        	</div>
			        </form>
			        <div id="campos_vacios" class="alert alert-danger">
		      			<p class="msj_error">There are empty fields</p>
		      		</div>
		      		<div id="correo_invalido" class="alert alert-danger">
		      			<p class="msj_error">The email is not valid</p>
		      		</div>
		      		<div id="repetido">
		      			<p class="msj_error" class="alert alert-danger">The name and/or email of the candidate already exists</p>
		      		</div>
		      		<div id="alert-msg">
						
					</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-success" id="registrar">Create</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="passModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title">Password Generated</h5>
			        
		      	</div>
		      	<div class="modal-body">
		        	<h4>Data login on form</h4><br>
		        	<p><b>User: </b><span id="user"></span></p>
		        	<p><b>Password: </b><span id="pass"></span></p><br>
		        	<p id="respuesta_mail"></p>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	        		<div class="row" id="div_commentario">
	        			<div class="col-md-12">
	        				<label for="motivo">Type the reason *</label>
	        				<textarea name="motivo" id="motivo" class="form-control" rows="3"></textarea>
	        				<br>
	        			</div>
	        		</div>
	        		<div class="msj_error">
	        			<p id="msg_accion"></p>
	        		</div>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      		<button type="button" class="btn btn-danger" id="btnGuardar" onclick="ejecutarAccion()">Accept</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="statusModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Candidate Status</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<p id="nombreCandidato" class="text-center"></p>
		      		<div id="div_status"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Follow up of the candidate's study </h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_avances"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="llamadasModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Phone calls to candidate</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_llamadas"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="emailsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Emails to candidate</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_emails"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Candidate comments</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="comentario_candidato"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="documentosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-xl modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Candidate documents</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p id="lista_documentos"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="ofacModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">OFAC and OIG Verifications</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div class="row">
		      			<div class="col-md-4 offset-md-1">
		      				<p class="text-center"><b>Candidate name</b></p>
		      				<p class="text-center" id="ofac_nombrecandidato"></p>
		      			</div>
            			<div class="col-md-4 offset-md-2">
                        	<p class="text-center" id="fecha_titulo_ofac"><b></b></p>
                            <p class="text-center" id="fecha_estatus_ofac"></p>
                        </div>
            		</div>
        			<div class="row">
                        <div class="col-md-6 borde_gris">
                            <p id="estatus_ofac"></p><br>
                            <span id="res_ofac"></span>
			          		<br><br>
                        </div>
                        <div class="col-md-6">
                            <p id="estatus_oig"></p><br>
                            <span id="res_oig"></span>
			          		<br><br>
                        </div>
                    </div>
                    
                    
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-rodi">
		  	<a class="navbar-brand" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		<?php echo strtoupper($this->session->userdata('cliente')); ?>
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno'); ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="<?php echo base_url(); ?>Login/logout">Sign out</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="div_titulo">
						<p class="titulo">Table of Candidate Records</p>
					</div>
				</div>
			</div>
			<div class="row tabla">
				<div class="col-lg-12">
					<input type="hidden" class="idCandidato">
					<input type="hidden" class="correo">
					<table id="tabla" class="table stripe hover row-border cell-border table-responsive"></table>
				</div>
			</div>
		</div>
	</section>
	<script src="<?php echo base_url() ?>js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/clients.js"></script>

	<script src="<?php echo base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>js/icheck.js"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<script>
    $(document).ready(function(){
    	var url = '<?php echo base_url('Cliente/TataGet'); ?>';
		changeDatatable(url);
		//Crear registro
		$("#registrar").click(function(){
			var datos = new FormData();
			var correo = $("#correo").val();
			datos.append('correo', $("#correo").val());
			datos.append('nombre', $("#nombre").val());
			datos.append('paterno', $("#paterno").val());
			datos.append('materno', $("#materno").val());
			datos.append('celular', $("#celular").val());
			datos.append('fijo', $("#fijo").val());
			datos.append('fecha_nacimiento', $("#fecha_nacimiento").val());
			datos.append('proyecto', $("#proyecto").val());
			datos.append('examen', $("#examen").val());
			//console.log($("#ine")[0].files[0])
			var totalVacios = $('.obligado').filter(function(){
	        	return !$(this).val();
	      	}).length;
	      
	      	if(totalVacios > 0){
	        	$(".obligado").each(function() {
	          		var element = $(this);
	          		if (element.val() == "") {
	            		element.addClass("requerido");
	            		$("#newModal #campos_vacios").css('display','block');
			            setTimeout(function(){
			              $('#newModal #campos_vacios').fadeOut();
			            },4000);
	          		}
	          		else{
	            		element.removeClass("requerido");
	          		}
	        	});
	      	}
	      	else{
	      		if(!isEmail(correo) && correo != ""){
          			$('#campos_vacios').css("display", "none");
          			$('#correo_invalido').css("display", "block");
          			$("#correo").addClass("requerido");
          			setTimeout(function(){
            			$("#correo_invalido").fadeOut();
          			},4000);
        		}
        		else{
        			$.ajax({
		              	url: '<?php echo base_url('Cliente/addCandidate'); ?>',
		              	type: 'POST',
		              	data: datos,
		              	contentType: false,  
		     			cache: false,  
		     			processData:false,
		              	beforeSend: function() {
		                	$('.loader').css("display","block");
		              	},
		              	success : function(res){ 
		              		if(res == 0){
			                  	$('.loader').fadeOut();
		              			$('#campos_vacios').css("display", "none");
			          			$('#correo_invalido').css("display", "none");
			          			$('#repetido').css("display", "block");
			          			setTimeout(function(){
			            			$("#repetido").fadeOut();
			          			},4000);
		              		}
		              		if(res.substring(0,4) == "Sent"){
		              			dato = res.split('@@');
	              				setTimeout(function(){
			                  		$('.loader').fadeOut();
			                	},300);
		                		$("#newModal").modal('hide');
		                		recargarTable();
		                		$("#exito").css('display','block');
		                		$("#user").text(correo);
		                		$("#pass").text(dato[1]);
		                		$("#respuesta_mail").text("* A email has been sent with this credentials to candidate. This email could take a few minutes to be delivered.");
		                		$("#passModal").modal('show');
		                		setTimeout(function(){
			                  		$('#exito').fadeOut();
			                	},3000);
		              		}
		              		if(res == "creado"){
	              				setTimeout(function(){
			                  		$('.loader').fadeOut();
			                	},300);
		                		$("#newModal").modal('hide');
		                		recargarTable();
		                		$("#exito").css('display','block');
		                		setTimeout(function(){
			                  		$('#exito').fadeOut();
			                	},3000);
		              		}
		              		if(res != 0 && res.substring(0,4) != "Sent" && res != "creado"){
		              			//var dato = res.split('@@');
		              			setTimeout(function(){
			                  		$('.loader').fadeOut();
			                  	},200);
			                  	recargarTable();
			                  	$("#alert-msg").css('display','block');
		              			$('#alert-msg').html('<div class="alert alert-danger">' + res + '</div>');
		              		}
		              	}
	            	});
        		}
	      	}
		});

    });
	function changeDatatable(url){
		$('#tabla').DataTable({
      		"pageLength": 25,
	      	"pagingType": "simple",
	      	"order": [0, "desc"],
	      	//"stateSave": true,
	      	"serverSide": false,
	      	"ajax": url,
	      	"columns":[ 
	      		{ 	title: 'id', data: 'id', visible: false },
	        	{ 	title: 'Candidate', data: 'nombre', "width": "20%" },
	        	{ 	title: 'Project', data: 'proyecto', "width": "12%" },
	        	{ 	title: 'Analist', data: 'analista', "width": "12%" },
	        	{ 	title: 'Register date', data: 'fecha_alta', "width": "11%",
	        		mRender: function(data, type, full){
            			var f = data.split(' ');
            			var h = f[1];
            			var aux = h.split(':');
            			var hora = aux[0]+':'+aux[1];
            			var aux = f[0].split('-');
            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
            			var tiempo = fecha+' '+hora;
        				return tiempo;
          			}
	        	},
	        	{ 	title: 'Actions', data: 'id', bSortable: false, "width": "8%",
         			mRender: function(data, type, full) {
         				return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Follow up of the candidate" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a>';
          			}
        		},
        		{ 	title: 'BGC Status', data: 'id', bSortable: false, "width": "15%",
         			mRender: function(data, type, full) {
 						if(full.status_bgc == 0){
         					return "<i class='fas fa-circle status_bgc0'></i>Pending"; 
         				}
         				if(full.status_bgc == 1){
         					return "<i class='fas fa-circle status_bgc1'></i>Positive";
         				}
         				if(full.status_bgc == 2){
         					return "<i class='fas fa-circle status_bgc2'></i>Negative";
         				}
         				if(full.status_bgc == 3){
         					return "<i class='fas fa-circle status_bgc3'></i>For consideration";
         				}
          			}
        		} 
	      	],
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$("a#msj_avances", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewAvances'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_avances").html(res);

			            }
			        });
	      			$("#avancesModal").modal("show");
	      		});
		        $("a#llamada", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewLlamadas'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_llamadas").html(res);

			            },error: function(res)
			            {
			              
			            }
			        });
	      			$("#llamadasModal").modal("show");
	      		});
	      		$("a#correoEnviado", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewEmails'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_emails").html(res);

			            },error: function(res)
			            {
			              
			            }
			        });
	      			$("#emailsModal").modal("show");
	      		});
	      		$("a#eliminar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$(".idCliente").val(data.id_cliente);
	      			$("#titulo_accion").text("Eliminar candidato");
	      			//$("#texto_confirmacion").html("¿Estás seguro de desactivar el puesto <b>"+data.nombre+"</b>?");
	      			$("#div_commentario").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
      		},
	      	"language": {
	        	"lengthMenu": "Mostrar _MENU_ registros por página",
	        	"zeroRecords": "No se encontraron registros",
	        	"info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        	"infoEmpty": "Sin registros disponibles",
	        	"infoFiltered": "(Filtrado _MAX_ registros totales)",
	        	"sSearch": "Search:",
	        	"oPaginate": {
	          		"sLast": "Última página",
	          		"sFirst": "Primera",
	          		"sNext": "<i class='fa  fa-arrow-right'></i>",
	          		"sPrevious": "<i class='fa fa-arrow-left'></i>"
	        	}
	      	}
    	}); 
		$("#tabla").DataTable().search(" ");
	}
	function estatusOFAC(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkOfac'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			$("#fecha_estatus_ofac").empty();
      			$("#estatus_ofac").empty();
      			$("#res_ofac").empty();
      			$("#estatus_oig").empty();
      			$("#res_oig").empty();
  				var datos = res.split('@@');
  				if(datos[0] == 0){
  					$("#fecha_titulo_ofac").html("<b>No date</b>");
  					$("#estatus_ofac").html("<b>OFAC Status: </b>Not defined yet");
	    			$("#res_ofac").html("<b>Result:</b> Not defined yet");
	    			$("#estatus_oig").html("<b>OIG Status: </b>Not defined yet");
	    			$("#res_oig").html("<b>Result:</b> Not defined yet");
  				}
  				else{
  					$("#fecha_titulo_ofac").html("<b>Last update</b>");
  					$("#fecha_estatus_ofac").text(datos[0]);
  					$("#estatus_ofac").html("<b>OFAC Status:</b> "+datos[1]);
	    			var res_ofac = (datos[2] == 1)? "Positive":"Negative";
	    			$("#res_ofac").html("<b>Result:</b> "+res_ofac);
	    			$("#estatus_oig").html("<b>OIG Status:</b> "+datos[3]);
	    			var res_oig = (datos[4] == 1)? "Positive":"Negative";
	    			$("#res_oig").html("<b>Result:</b> "+res_oig);
  				}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#ofacModal").modal("show");
	}
	function ejecutarAccion(){
		var accion = $("#btnGuardar").val();
		var id_candidato = $(".idCandidato").val();
		var id_cliente = '<?php echo $this->session->userdata('id') ?>';
		var correo = $(".correo").val();
		var motivo = $("#motivo").val();
		var usuario = 2;
		if(accion == 'cancel'){
			if(motivo == ""){
				$("#msg_accion").text("The comment is required");
				$("#msg_accion").css('display','block');
				setTimeout(function(){
              		$('#msg_accion').fadeOut();
            	},5000);
			}
			else{
				$.ajax({
	              	url: '<?php echo base_url('Candidato/cancel'); ?>',
	              	type: 'post',
	              	data: {'id_candidato':id_candidato,'motivo':motivo},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
	              		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
	            		$("#quitarModal").modal('hide');
	            		recargarTable();
	            		$("#texto_msj").text('The candidate has been cancelled succesfully');
	            		$("#mensaje").css('display','block');
	            		setTimeout(function(){
	                  		$('#mensaje').fadeOut();
	                	},3000);
	              	},error: function(res){
	                	$('#errorModal').modal('show');
	              	}
	        	});
			}
		}
		if(accion == 'delete'){
			if(motivo == ""){
				$("#msg_accion").text("The reason is required");
				$("#msg_accion").css('display','block');
				setTimeout(function(){
              		$('#msg_accion').fadeOut();
            	},5000);
			}
			else{
				$.ajax({
	              	url: '<?php echo base_url('Candidato/accionCandidato'); ?>',
	              	type: 'post',
	              	data: {'id':id_candidato,'motivo':motivo,'usuario':usuario,'id_cliente':id_cliente},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
	              		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
	            		$("#quitarModal").modal('hide');
	            		recargarTable();
	            		$("#texto_msj").text('The candidate has been deleted succesfully');
	            		$("#mensaje").css('display','block');
	            		setTimeout(function(){
	                  		$('#mensaje').fadeOut();
	                	},3000);
	              	},error: function(res){
	                	$('#errorModal').modal('show');
	              	}
	        	});
			}
		}
		if(accion == 'generate'){
			$.ajax({
              	url: '<?php echo base_url('Candidato/generate'); ?>',
              	type: 'post',
              	data: {'id_candidato':id_candidato,'correo':correo},
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		setTimeout(function(){
                  		$('.loader').fadeOut();
                	},300);
            		$("#quitarModal").modal('hide');
            		$("#user").text(correo);
            		$("#pass").text(res);
            		$("#respuesta_mail").text("* An email has been sent with this credentials to the candidate. This email could take a few minutes to be delivered.");
            		$("#passModal").modal('show');
            		recargarTable();
            		$("#texto_msj").text('The password has been created succesfully');
            		$("#mensaje").css('display','block');
            		setTimeout(function(){
                  		$('#mensaje').fadeOut();
                	},3000);
              	},error: function(res){
                	$('#errorModal').modal('show');
              	}
        	});
		}
	}
    //Verificacion de correo
	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
  	$('#quitarModal').on('hidden.bs.modal', function (e) {
	  	$("#msg_accion").css('display','none');
		$(this)
		    .find("input,textarea")
		       .val('')
		       .end();
	});
	$('#newModal').on('hidden.bs.modal', function (e) {
		$("#alert-msg").empty();
	  	$("#alert-msg").css('display','none');
	  	$("#campos_vacios").css('display','none');
	  	$("#correo_invalido").css('display','none');
	  	$("#repetido").css('display','none');
		$(this)
		    .find("input,select")
		       .val('')
		       .end();
	});
	var hoy = new Date();
  	var dd = hoy.getDate();
  	var mm = hoy.getMonth()+1;
  	var yyyy = hoy.getFullYear();
  	var hora = hoy.getHours()+":"+hoy.getMinutes();

  	if(dd<10) {
      	dd='0'+dd;
  	} 

  	if(mm<10) {
      	mm='0'+mm;
  	}
	$("#fecha_nacimiento").datetimepicker({
  		minView: 2,
    	format: "mm/dd/yyyy",
    	startView: 4,
    	autoclose: true,
    	todayHighlight: true,
    	pickerPosition: "bottom-left",
    	forceParse: false
  	});
  	$('#fecha_nacimiento').datetimepicker('setEndDate', (yyyy - 18)+'-'+'01-01');
	</script>
</body>
</html>