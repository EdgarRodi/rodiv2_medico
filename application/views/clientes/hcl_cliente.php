<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>HCL | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/subcliente.css">
  <!-- DataTables -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.25/datatables.min.css" />
  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <!-- Sweetalert 2 -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.12.7/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
  <link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"-->
	<!--link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"-->

</head>
<body>
	<div id="exito" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong> The candidate has been add succesfully
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong><p id="texto_msj"></p>
  	</div>
	<div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Register New Candidate</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form id="datos">
	          <div class="row">
	            <div class="col-md-4">
	              <label>Names(s) *</label>
	              <input type="text" class="form-control registro_obligado" name="nombre_registro" id="nombre_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
	              <br>
	            </div>
	            <div class="col-md-4">
	              <label>Firs lastname *</label>
	              <input type="text" class="form-control registro_obligado" name="paterno_registro" id="paterno_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
	              <br>
	            </div>
	            <div class="col-md-4">
	              <label>Second lastname </label>
	              <input type="text" class="form-control" name="materno_registro" id="materno_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
	              <br>
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-4">
	              <label>Email *</label>
	              <input type="email" class="form-control registro_obligado" name="correo_registro" id="correo_registro" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
	              <br>
	            </div>
	            <div class="col-md-4">
	              <label>Cell phone number</label>
	              <input type="text" class="form-control" name="celular_registro" id="celular_registro" maxlength="16">
	              <br>
	            </div>
	            <div class="col-md-4">
	              <label>Home phone number </label>
	              <input type="text" class="form-control" name="fijo_registro" id="fijo_registro" maxlength="16">
	              <br>
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-4">
	              <label>Birthdate</label>
	              <input type="text" class="form-control" name="fecha_nacimiento_registro" id="fecha_nacimiento_registro" placeholder="mm/dd/yyyy">
	              <br>
	            </div>
	            <div class="col-md-4">
	              <label>Proyect *</label>
	              <select name="proyecto_registro" id="proyecto_registro" class="form-control registro_obligado">
	                <option value="">Select</option>
	                <?php
	                foreach ($proyectos as $pro) {
	                  $id_compuesto = $pro->id . '@@' . $pro->internacional; ?>
	                  <option value="<?php echo $id_compuesto; ?>"><?php echo $pro->nombre; ?></option>
	                <?php
	                } ?>
	              </select>
	              <br>
	            </div>
	            <div class="col-md-4">
	              <label>Drug test *</label>
	              <select name="examen_registro" id="examen_registro" class="form-control registro_obligado">
	                <option value="">Select</option>
	              </select>
	              <br>
	            </div>
	          </div>
	          <div class="row">
	            <div class="col-md-4">
	              <label>Medical test *</label>
	              <select name="examen_medico" id="examen_medico" class="form-control registro_obligado">
	                <option value="0">N/A</option>
	                <option value="1">Apply</option>
	              </select>
	              <br>
	            </div>
	            <div class="col-md-4">
	              <label>Country *</label>
	              <select name="pais_registro" id="pais_registro" class="form-control registro_obligado" disabled>
	                <option value="-1">Select</option>
	                <option value="Argentina">Argentina</option>
	                <option value="Colombia">Colombia</option>
	                <option value="Costa Rica">Costa Rica</option>
	                <option value="Panama">Panama</option>
	              </select>
	              <br>
	            </div>
	          </div>
	        </form>
	        <div id="msj_error" class="alert alert-danger hidden"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	        <button type="button" class="btn btn-success" onclick="registrar()">Save</button>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="modal fade" id="statusModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Candidate Status</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<p id="nombreCandidato" class="text-center"></p>
		      		<div id="div_status"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Follow up of the candidate's study </h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_avances"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Candidate comments</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="comentario_candidato"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="documentosModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-xl modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Candidate documents</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p id="lista_documentos"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="ofacModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">OFAC and OIG Verifications</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div class="row">
		      			<div class="col-md-4 offset-md-1">
		      				<p class="text-center"><b>Candidate name</b></p>
		      				<p class="text-center" id="ofac_nombrecandidato"></p>
		      			</div>
            			<div class="col-md-4 offset-md-2">
                        	<p class="text-center" id="fecha_titulo_ofac"><b></b></p>
                            <p class="text-center" id="fecha_estatus_ofac"></p>
                        </div>
            		</div>
        			<div class="row">
                        <div class="col-md-6 borde_gris">
                            <p id="estatus_ofac"></p><br>
                            <span id="res_ofac"></span>
			          		<br><br>
                        </div>
                        <div class="col-md-6">
                            <p id="estatus_oig"></p><br>
                            <span id="res_oig"></span>
			          		<br><br>
                        </div>
                    </div>
                    
                    
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light" id="menu">
		  	<a class="navbar-brand text-light" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" class="space">
		  		<?php echo $this->session->userdata('nombre') . " " . $this->session->userdata('paterno'); ?>
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item">
			        	<a class="nav-link text-light font-weight-bold" href="javascript:void(0)" data-toggle="modal" data-target="#newModal"><i class="fas fas fa-plus-circle"></i> Add Candidate</a>
			      	</li>
					<li class="nav-item">
			            <a class="nav-link text-light font-weight-bold" href="<?php echo base_url(); ?>Login/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
			        </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
	<section>
		<div class="contenedor mt-5 my-5">
	      <div class="card shadow mb-4">
	        <div class="card-header py-3">
	          <h4 class="m-0 font-weight-bold text-primary  text-center">List of Candidates</h4>
	        </div>
	        <div class="card-body">
	          <div class="table-responsive">
	            <table id="tabla" class="table table-bordered" width="100%" cellspacing="0">
	            </table>
	          </div>
	        </div>
	      </div>
	    </div>
	</section>
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.25/datatables.min.js"></script>
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  	<!-- Sweetalert 2 -->
  	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.12.7/dist/sweetalert2.js"></script>
	<script>
    $(document).ready(function(){
    	var url = '<?php echo base_url('Candidato/getCandidatos'); ?>';
		changeDatatable(url);
		$("#subcliente").change(function(){
			var subcliente = $(this).val();
			if(subcliente == 0){
				var url = '<?php echo base_url('Candidato/getCandidatos'); ?>';
				changeDatatable(url);
			}
			else{
				var url = '<?php echo base_url('Candidato/getCandidatosSubcliente?id_subcliente='); ?>'+subcliente;
				changeDatatable(url);
			}
		});
		$("#proyecto").change(function(){
	      	var id_proyecto = $(this).val();
	      	if(id_proyecto != "" && id_proyecto != 0){
	        	$.ajax({
	          		url: '<?php echo base_url('Cliente/getPaqueteSubclienteProyecto'); ?>',
	          		method: 'POST',
	          		data: {'id_proyecto':id_proyecto},
	          		dataType: "text",
	          		success: function(res)
	          		{
	          			if(res != ""){
	          				//$('#examen').prop('disabled', false);
	            			$('#examen').html(res);
	          			}
	          		}
	        	});
	      	}
	      	else{
	      		$('#examen').empty();
	      		$('#examen').append($("<option selected></option>").attr("value","").text("Select"));
	      	}	      	
	    });	
		//Crear registro
		function registrar(){
			var datos = new FormData();
			var aux = $("#proyecto_registro").val().split('@@');
			var proyecto = aux[0];
			var pais = $("#pais_registro").val();
			var correo = $("#correo_registro").val();
			datos.append('correo', $("#correo_registro").val());
			datos.append('nombre', $("#nombre_registro").val());
			datos.append('paterno', $("#paterno_registro").val());
			datos.append('materno', $("#materno_registro").val());
			datos.append('celular', $("#celular_registro").val());
			datos.append('fijo', $("#fijo_registro").val());
			datos.append('fecha_nacimiento', $("#fecha_nacimiento_registro").val());
			datos.append('proyecto', proyecto);
			datos.append('id_cliente', id_cliente);
			datos.append('examen', $("#examen_registro").val());
			datos.append('medico', $("#examen_medico").val());
			datos.append('pais', pais);
			datos.append('usuario', 1);
			
			$.ajax({
				url: '<?php echo base_url('Cliente_HCL/registrar'); ?>',
				type: 'POST',
				data: datos,
				contentType: false,  
				cache: false,  
				processData:false,
				beforeSend: function() {
					$('.loader').css("display","block");
				},
				success : function(res){ 
					setTimeout(function() {
						$('.loader').fadeOut();
					}, 200);
					var data = JSON.parse(res);
					if (data.codigo === 1) {
						$("#newModal").modal('hide')
						recargarTable()
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: 'Se ha guardado correctamente',
							showConfirmButton: false,
							timer: 2500
						})
					}
					if(data.codigo === 3){
						$("#newModal").modal('hide');
						recargarTable();
						Swal.fire({
							position: 'center',
							icon: 'warning',
							title: 'Se ha guardado correctamente pero hubo un problema al enviar el correo',
							showConfirmButton: false,
							timer: 2500
						})
					}
					if(data.codigo === 4){
						$("#newModal").modal('hide');
						recargarTable();
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: 'Se ha guardado correctamente',
							showConfirmButton: false,
							timer: 2500
						})
						$("#user").text(correo);
						$("#respuesta_mail").text("* Un correo ha sido enviado al candidato con sus nuevas credenciales. Este correo puede demorar algunos minutos.");
						$("#passModal").modal('show');
					}
					if(data.codigo === 0 || data.codigo === 2){
						$("#newModal #msj_error").css('display', 'block').html(data.msg);
					}
				}
			});	
		}

    });
	function changeDatatable(url){
		$('#tabla').DataTable({
			"pageLength": 25,
	      	//"pagingType": "simple",
	      	"order": [0, "desc"],
	      	"stateSave": true,
	      	"serverSide": false,
	      	"ajax": url,
	      	"columns":[ 
		        { 	title: 'Name', data: 'nombreCompleto',
		        	mRender: function(data, type, full){
		        		return full.nombre+" "+full.paterno+" "+full.materno;
		        	}
		        },
		        { 	title: 'Project', data: 'proyecto' },
		        { 	title: 'Register Date', data: 'fecha_alta', "width": "10%",
			        mRender: function(data, type, full){
			            var aux = data.split(" ");
			            var f = aux[0].split("-");
			            var  fecha = f[1]+"/"+f[2]+"/"+f[0];
			            var h = aux[1].split(':');
			            var hora = h[0]+":"+h[1];
			            var tiempo = fecha+" "+hora;
			            return tiempo;
	          		}
	          	},
	          	{ title: 'Form Date', data: 'fecha_contestado', "width": "10%",
	        		mRender: function(data, type, full){
	        			if(full.id_proyecto != 25 && full.id_proyecto != 135 && full.id_proyecto != 136 && full.id_proyecto != 137 && full.id_proyecto != 138 && full.id_proyecto != 140 && full.id_proyecto != 147 && full.id_proyecto != 148){
		        			if(data == "" || data == null){
		        				return "<i class='fas fa-circle estatus0'></i>Pending";
		        			}
		        			else{
		        				var f = data.split(' ');
	            				var h = f[1];
	            				var aux = h.split(':');
	            				var hora = aux[0]+':'+aux[1];
	            				var aux = f[0].split('-');
	            				var fecha = aux[1]+"/"+aux[2]+"/"+aux[0];
	            				var tiempo = fecha+' '+hora;
	        					return "<i class='fas fa-circle estatus1'></i>"+tiempo;
		        			}
		        		}
		        		else{
		        			return "<i class='fas fa-circle status_bgc0'></i> N/A";
		        		}
          			}
	        	},
	        	{ title: 'Documents Date', data: 'fecha_documentos', "width": "10%",
	        		mRender: function(data, type, full){
	        			if(full.id_proyecto != 25 && full.id_proyecto != 135 && full.id_proyecto != 136 && full.id_proyecto != 137 && full.id_proyecto != 138 && full.id_proyecto != 140 && full.id_proyecto != 147 && full.id_proyecto != 148){
		        			if(data == "" || data == null){
		        				return "<i class='fas fa-circle estatus0'></i>Pending";
		        			}
		        			else{
		        				var f = data.split(' ');
	            				var h = f[1];
	            				var aux = h.split(':');
	            				var hora = aux[0]+':'+aux[1];
	            				var aux = f[0].split('-');
	            				var fecha = aux[1]+"/"+aux[2]+"/"+aux[0];
	            				var tiempo = fecha+' '+hora;
	        					return "<i class='fas fa-circle estatus1'></i>"+tiempo;
		        			}
		        		}
		        		else{
		        			return "<i class='fas fa-circle status_bgc0'></i> N/A";
		        		}
          			}
	        	},
	          	{
		          	title: 'Candidate verifications',
		          	data: 'id',
		          	"width": "8%",

		          	bSortable: false, 
		          	mRender: function(data, type, full) {
		          		if(full.id_tipo_proceso == 1){
		          			if(full.id_proyecto != 128){
		          				if(full.status == 0){
				        			return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Follow up of the candidate" id="msj_avances" class="fa-tooltip icono_datatable"><i class="fas fa-comment-dots"></i></a>';
				        		}
				        		else{
				        			return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Follow up of the candidate" id="msj_avances" class="fa-tooltip icono_datatable"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="View status" id="ver" class="fa-tooltip icono_datatable"><i class="fas fa-eye"></i></a>';
				        		}
		          			}
		          			if(full.id_proyecto == 135 || full.id_proyecto == 136 || full.id_proyecto == 137 || full.id_proyecto == 138 || full.id_proyecto == 140 || full.id_proyecto == 147 || full.id_proyecto == 148){
		          				if(full.status == 0){
				        			return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Follow up of the candidate" id="msj_avances" class="fa-tooltip icono_datatable"><i class="fas fa-comment-dots"></i></a>';
				        		}
				        		else{
				        			return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Follow up of the candidate" id="msj_avances" class="fa-tooltip icono_datatable"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="View status" id="ver" class="fa-tooltip icono_datatable"><i class="fas fa-eye"></i></a>';
				        		}
		          			}
		          			if(full.id_proyecto == 128){
		          				if(full.status == 0){
				        			return 'Global database seaches in process';
				        		}
				        		else{
				        			return 'Global database seaches completed';
				        		}
		          			}
			          		
			        	}
			        	else{
			        		if(full.status == 0){
			        			return '<a href="javascript:void(0)" id="ofac" data-toggle="tooltip" title="OFAC and OIG verifications" class="fa-tooltip icono_datatable"><i class="fas fa-eye"></i></a>';
			        		}
			        		else{
			        			return '<a href="javascript:void(0)" id="ofac" data-toggle="tooltip" title="OFAC and OIG verifications" class="fa-tooltip icono_datatable"><i class="fas fa-eye"></i></a>';
			        		}
			        	}
		          	}
		        },
        		{ title: 'Drug test', data: 'id', bSortable: false, "width": "10%",
         			mRender: function(data, type, full) {
         				if(full.id_proyecto != 21 && full.id_proyecto != 135 && full.id_proyecto != 136 && full.id_proyecto != 138 && full.id_proyecto != 140){
         					if(full.id_proyecto == 25){
         						if(full.fecha_resultado != null && full.fecha_resultado != ""){
		         					if(full.resultado_doping == 1){
		         						return '<i class="fas fa-circle status_bgc2"></i> Not approved'; 
		         					}
		         					else{
		         						return '<i class="fas fa-circle status_bgc1"></i> Approved';
		         					}
		         					
		         				}
		         				else{
		         					return "<i class='fas fa-circle status_bgc0'></i>Pending";
		         				}
         					}
         					else{
         						if(full.fecha_resultado != null && full.fecha_resultado != ""){
		         					if(full.resultado_doping == 1){
		         						return '<i class="fas fa-circle status_bgc2"></i> <div style="display: inline-block;"><form id="pdf'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download test doping" id="pdfDoping" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop'+full.idDoping+'" value="'+full.idDoping+'"></form></div>'; 
		         					}
		         					else{
		         						return '<i class="fas fa-circle status_bgc1"></i> <div style="display: inline-block;"><form id="pdf'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download test doping" id="pdfDoping" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop'+full.idDoping+'" value="'+full.idDoping+'"></form></div>';
		         					}
		         					
		         				}
		         				else{
		         					return "<i class='fas fa-circle status_bgc0'></i>Pending";
		         				}
         					}
         				}
         				else{
         					return "<i class='fas fa-circle status_bgc0'></i>N/A";
         				}
          			}
        		},
		        { 	title: 'BGV Status', data: 'status_bgc',bSortable: false, "width": "8%",
		        	mRender: function(data, type, full){
	        			if(full.status == 0){
		        			return "<a href='javascript:void(0)' data-toggle='tooltip' title='Pending form'><i class='fas fa-circle status_bgc0'></i></a>";
		        		}
		        		if(full.status == 1){
		        			return "<a href='javascript:void(0)' data-toggle='tooltip' title='In process by analyst'><i class='fas fa-circle status_bgc3'></i></a>";
		        		}
		        		else{
		        			switch(data){
				            	case '0':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='Undefined'><i class='fas fa-circle status_bgc0'></i></a>";
				            	break;
				            	case '1':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='Positive'><i class='fas fa-circle status_bgc1'></i></a>";
				            	break;
				            	case '2':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='Negative'><i class='fas fa-circle status_bgc2'></i></a>";
				            	break;
				            	case '3':
				            		return "<a href='javascript:void(0)' data-toggle='tooltip' title='For Consideration'><i class='fas fa-circle status_bgc3'></i></a>";
				            	break;
				            }
		        		}
	          		}
		        },
		        {
		          	title: 'Actions',
		          	data: 'id',
		          	"width": "8%",
		          	bSortable: false, 
		          	mRender: function(data, type, full) {
		          		if(full.id_proyecto != 25){
		          			if(full.status_bgc != 0){
		          				if(full.id_proyecto == 20){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLBectonPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 21){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLExxonPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 23){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLNokiaPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 24){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLSempraPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 26){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLPGPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 27){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLGeneral'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 28){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLGeneral'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 35){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLCitiPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 128){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLInternalPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}
			          			if(full.id_proyecto == 135 || full.id_proyecto == 136 || full.id_proyecto == 137 || full.id_proyecto == 138 || full.id_proyecto == 140 || full.id_proyecto == 147 || full.id_proyecto == 148){
			          				return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/createHCLCustom'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download BGV" id="pdfFinal" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
			          			}

			          		}
			          		else{
			          			return ''
			          		}	  
		          		}
		          		else{
         					if(full.fecha_resultado != null && full.fecha_resultado != ""){
	         					return '<div style="display: inline-block;"><form id="pdf'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Download test doping" id="pdfDoping" class="fa-tooltip icono_datatable"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop'+full.idDoping+'" value="'+full.idDoping+'"></form></div>';
	         				}
	         				else{
	         					return "<i class='fas fa-circle status_bgc0'></i>Pending";
	         				}
         				}
		          	}
		        }          
      		],
      		
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$("a#ver", row).bind('click', () => {
      				var salida = "";
      				var estudios = "<tr><th>Education </th><th>N/A</th></tr>";
      				var identidad = "<tr><th>Identity </th><th>N/A</th></tr>";
      				var empleo = "<tr><th>Employment History </th><th>N/A</th></tr>";
      				var criminal = "<tr><th>Criminal Court Record Search </th><th>N/A</th></tr>";
      				var global_database = "<tr><th>Global Database Searches </th><th>N/A</th></tr>";
      				var domicilios = "<tr><th>Address History </th><th>N/A</th></tr>";
      				var ref_profesionales = "<tr><th>Professional references </th><th>N/A</th></tr>";
      				switch(data.id_proyecto){
      					case '20':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
  						case '21':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Completed</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						
      						break;
      					case '23':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      					case '24':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Completed</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						break;
      					case '26':
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						break;
      					case '27':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Completed</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						else{
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						break;
      					case '28':
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Court Record Search </th><th>Completed</th></tr>":"<tr><th>Criminal Court Record Search </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      					case '35':
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      						}
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      					case '135':
      					case '137':
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      					case '136':
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Search </th><th>Completed</th></tr>":"<tr><th>Criminal Search </th><th>In process</th></tr>";
      						break;
      					case '138':
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      					case '140':
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						break;
      					case '147':
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal Search </th><th>Completed</th></tr>":"<tr><th>Criminal Search </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Completed</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						else{
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						break;
      					case '148':
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Completed</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						else{
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						break;
      					case '150':
      					case '151':
      					case '152':
      					case '153':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Completed</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal check </th><th>Completed</th></tr>":"<tr><th>Criminal check </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						ref_profesionales = (data.idRefPro != "" && data.idRefPro != null)? "<tr><th>Professional references </th><th>Completed</th></tr>":"<tr><th>Professional references </th><th>In process</th></tr>";
      						break;
      					case '154':
      						estudios = (data.id_grado_estudio != 0 && data.estudios_periodo != "" && data.estudios_escuela != "" && data.estudios_ciudad != "" && data.estudios_certificado != "")? "<tr><th>Education </th><th>Completed</th></tr>":"<tr><th>Education </th><th>In process</th></tr>";
      						identidad = (data.ine != "" && data.ine_institucion != "")? "<tr><th>Identity </th><th>Completed</th></tr>":"<tr><th>Identity </th><th>In process</th></tr>";
      						if(data.status_bgc > 0){
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Completed</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Completed</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						else{
      							empleo = (data.idVerificacionLaboral != "" && data.idVerificacionLaboral != null)? "<tr><th>Employment History </th><th>Registered</th></tr>":"<tr><th>Employment History </th><th>In process</th></tr>";
      							domicilios = (data.comentario_domicilios != "" && data.comentario_domicilios != null)? "<tr><th>Address History </th><th>Registered</th></tr>":"<tr><th>Address History </th><th>In process</th></tr>";
      						}
      						criminal = (data.penales_finalizado == 1)? "<tr><th>Criminal check </th><th>Completed</th></tr>":"<tr><th>Criminal check </th><th>In process</th></tr>";
      						global_database = (data.idGlobal != "" && data.idGlobal != null)? "<tr><th>Global Database Searches </th><th>Completed</th></tr>":"<tr><th>Global Database Searches </th><th>In process</th></tr>";
      						break;
      				}

	      			salida += '<table class="table table-striped">';
			        salida += '<thead>';
			        salida += '<tr>';
			        salida += '<th scope="col">Description</th>';
			        salida += '<th scope="col">Status</th>';
			        salida += '</tr>';
			        salida += '</thead>';
			        salida += '<tbody>';
			        salida += estudios;
			        salida += identidad;
			        salida += empleo;
			        salida += criminal;
			        salida += global_database;
			        salida += domicilios;
			        salida += ref_profesionales;
                	salida += '</tbody>';
        			salida += '</table>';

        			$("#nombreCandidato").text("Candidate: "+data.nombreCompleto);
        			$("#div_status").html(salida);
        			$("#statusModal").modal("show");
      			
	      		});
	      		$("a#ofac", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#idCliente").val(data.id_cliente);
	      			$("#ofac_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusOFAC();
	      		});
	      		$('a[id^=pdfOfac]', row).bind('click', () => {
		            var id = data.id;
		            $('#ofacpdf'+id).submit();
		        });
	      		$("a#cancelar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#titulo_accion").text("Cancel candidate");
	      			$("#texto_confirmacion").html("Are you sure you want to cancel <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','cancel');
	      			$("#div_commentario").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
	      		$('a[id^=pdfDoping]', row).bind('click', () => {
		            var id = data.idDoping;
		            $('#pdf'+id).submit();
		        });
	      		$('a[id^=pdfFinal]', row).bind('click', () => {
		            var id = data.id;
		            $('#pdf'+id).submit();
		        });
		        $("a#msj_avances", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewAvances'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_avances").html(res);

			            }
			        });
	      			$("#avancesModal").modal("show");
	      		});
	      		$('a#comentario', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/viewComentario'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
				            	$("#comentario_candidato").html(res);
				            	$("#verModal").modal('show');
			            	}
			            	else{
				            	$("#comentario_candidato").html("No comments");
				            	$("#verModal").modal('show');
			            	}
			            	

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
		        $('a#documentos', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/viewDocumentos'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
			            		$("#lista_documentos").empty();
				            	$("#lista_documentos").html(res);
				            	$("#documentosModal").modal('show');
			            	}
			            	else{
			            		$("#lista_documentos").empty();
				            	$("#lista_documentos").html("<p class='text-center'><b>Documents under review</b></p>");
				            	$("#documentosModal").modal('show');
			            	}
			            	

			            },error: function(res)
			            {
			              
			            }
			        });
		        });
      		},
			"language": {
		        /*"lengthMenu": "Mostrar _MENU_ registros por página",
		        "zeroRecords": "No se encontraron registros",
		        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		        "infoEmpty": "Sin registros disponibles",
		        "infoFiltered": "(Filtrado _MAX_ registros totales)",*/
		        //"sSearch": 'Search: <i class="fas fa-search"></i>',
		        /*"oPaginate": {
		          //"sLast": "Última página",
		          //"sFirst": "Primera",
		          "sNext": "<i class='fa  fa-arrow-right'></i>",
		          "sPrevious": "<i class='fa fa-arrow-left'></i>"
		        }*/
		    }
		});
		$("#tabla").DataTable().search(" ");
	}
	function estatusOFAC(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkOfac'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			$("#fecha_estatus_ofac").empty();
      			$("#estatus_ofac").empty();
      			$("#res_ofac").empty();
      			$("#estatus_oig").empty();
      			$("#res_oig").empty();
  				var datos = res.split('@@');
  				if(datos[0] == 0){
  					$("#fecha_titulo_ofac").html("<b>No date</b>");
  					$("#estatus_ofac").html("<b>OFAC Status: </b>Not defined yet");
	    			$("#res_ofac").html("<b>Result:</b> Not defined yet");
	    			$("#estatus_oig").html("<b>OIG Status: </b>Not defined yet");
	    			$("#res_oig").html("<b>Result:</b> Not defined yet");
  				}
  				else{
  					$("#fecha_titulo_ofac").html("<b>Last update</b>");
  					$("#fecha_estatus_ofac").text(datos[0]);
  					$("#estatus_ofac").html("<b>OFAC Status:</b> "+datos[1]);
	    			var res_ofac = (datos[2] == 1)? "Positive":"Negative";
	    			$("#res_ofac").html("<b>Result:</b> "+res_ofac);
	    			$("#estatus_oig").html("<b>OIG Status:</b> "+datos[3]);
	    			var res_oig = (datos[4] == 1)? "Positive":"Negative";
	    			$("#res_oig").html("<b>Result:</b> "+res_oig);
  				}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#ofacModal").modal("show");
	}
	function ejecutarAccion(){
		var accion = $("#btnGuardar").val();
		var id_candidato = $(".idCandidato").val();
		var id_cliente = '<?php echo $this->session->userdata('id') ?>';
		var correo = $(".correo").val();
		var motivo = $("#motivo").val();
		var usuario = 2;
		if(accion == 'cancel'){
			if(motivo == ""){
				$("#msg_accion").text("The comment is required");
				$("#msg_accion").css('display','block');
				setTimeout(function(){
              		$('#msg_accion').fadeOut();
            	},5000);
			}
			else{
				$.ajax({
	              	url: '<?php echo base_url('Candidato/cancel'); ?>',
	              	type: 'post',
	              	data: {'id_candidato':id_candidato,'motivo':motivo},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
	              		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
	            		$("#quitarModal").modal('hide');
	            		recargarTable();
	            		$("#texto_msj").text('The candidate has been cancelled succesfully');
	            		$("#mensaje").css('display','block');
	            		setTimeout(function(){
	                  		$('#mensaje').fadeOut();
	                	},3000);
	              	},error: function(res){
	                	$('#errorModal').modal('show');
	              	}
	        	});
			}
		}
	}
    //Verificacion de correo
	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
  	$('#quitarModal').on('hidden.bs.modal', function (e) {
	  	$("#msg_accion").css('display','none');
		$(this)
		    .find("input,textarea")
		       .val('')
		       .end();
	});
	$('#newModal').on('hidden.bs.modal', function (e) {
		$("#alert-msg").empty();
	  	$("#alert-msg").css('display','none');
	  	$("#campos_vacios").css('display','none');
	  	$("#correo_invalido").css('display','none');
	  	$("#repetido").css('display','none');
		$(this)
		    .find("input,select")
		       .val('')
		       .end();
	});
	var hoy = new Date();
  	var dd = hoy.getDate();
  	var mm = hoy.getMonth()+1;
  	var yyyy = hoy.getFullYear();
  	var hora = hoy.getHours()+":"+hoy.getMinutes();

  	if(dd<10) {
      	dd='0'+dd;
  	} 

  	if(mm<10) {
      	mm='0'+mm;
  	}
	$("#fecha_nacimiento").datetimepicker({
  		minView: 2,
    	format: "mm/dd/yyyy",
    	startView: 4,
    	autoclose: true,
    	todayHighlight: true,
    	pickerPosition: "bottom-left",
    	forceParse: false
  	});
  	$('#fecha_nacimiento').datetimepicker('setEndDate', (yyyy - 18)+'-'+'01-01');
	</script>
</body>
</html>