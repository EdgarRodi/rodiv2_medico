<!DOCTYPE html>
<html>

<head>
	<title>ERP LAR Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- CSS -->
	<?php echo link_tag("css/login.css"); ?>
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<!--- JQuery -->
	<script type="text/javascript" src="<?php echo base_url();?>jquery/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	<!-- Favicon -->
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>img/mini-logo.png" />
</head>

<body>
	<!-- Alerts -->
	<div id="incompleto" class="alert alert-warning fade in mensaje" style='display:none;'>
		<!--a href="#" class="close" data-dismiss="alert" aria-label="close">×</a-->
	  	<strong>Atención:</strong> Escriba su correo electrónico
	</div>
	<div id="recupera_correo_invalido" class="alert alert-warning fade in mensaje" style='display:none;'>
		<!--a href="#" class="close" data-dismiss="alert" aria-label="close">×</a-->
	  	<strong>Atención:</strong> Correo inválido
	</div>
	<?php if($this->session->flashdata('error')):  ?>
		<div style="opacity: 1;" id="error" class="alert alert-danger fade in mensaje hide-it">
			<strong>Atención:</strong> <?php echo $this->session->flashdata('error'); ?>	
		</div>
	<?php endif; ?>

	<div class="flex-container">
		<div class="flex-item">
			<div class="logo">
				<img src="<?php echo base_url();?>img/lar-logo.png"">
			</div>
			<form action="<?php echo site_url('Login/generarLink');?>" method="POST">
			 	<div class="form-group">
			    	<input type="email" class="form-control" name="correo" id="correo"  placeholder="Ingrese su correo electrónico" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
			  	</div>
			  	<input type="submit" name="submit" class="btn btn-primary btn-md submit" onclick="return validar();" value="Enviar">
			  	<a href="<?php echo base_url("index.php/Login/index");?>">Iniciar sesión</a>
			</form>
		</div>	
	</div>

	<!--- Check Form -->
	<script >
		function validar(){
			var email = $('#correo').val();
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (email == "") {
				$('#recupera_correo_invalido').css("display", "none");
				$('#correo').focus();
				$('#incompleto').css("display", "block");
				$('#incompleto').css("opacity", 1);
				setTimeout(function(){
					$('#incompleto').css("display", "none");
				},4000);
	      return false;
			}
			else
				if(regex.test(email)){
					return true;
				}
				else{
					$('#incompleto').css("display", "none");
					$('#recupera_correo_invalido').css("display", "block");
					$('#recupera_correo_invalido').css("opacity", 1);
					setTimeout(function(){
						$('#recupera_correo_invalido').css("display", "none");
					},4000);
					return false;
				}
				
		}		
	</script>
	<!--- JQuery -->
	<script type="text/javascript" src="<?php echo base_url();?>jquery/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	
	<script>
		$(document).ready(function(){
			$(".hide-it").fadeOut(10000);
		});
	</script>
	
</body>

</html>