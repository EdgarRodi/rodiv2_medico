<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cat_puestos_model extends CI_Model{
  
  function getTotal(){
    $this->db
    ->select("id")
    ->from("puesto")
    ->where("eliminado", 0);

    $query = $this->db->get();
    return $query->num_rows();
  }
  function getPuestosActivos(){
    $this->db
    ->select("p.*,CONCAT(u.nombre,' ',u.paterno) as usuario")
    ->from("puesto as p")
    ->join("usuario as u","u.id = p.id_usuario")
    ->where("p.eliminado", 0)
    ->order_by("p.nombre", 'ASC');

    $query = $this->db->get();
    if($query->num_rows() > 0){
        return $query->result();
    }else{
        return FALSE;
    }
  }
  function registrar($puesto){
    $this->db->insert("puesto", $puesto);
  }
  function editar($puesto, $idPuesto){
    $this->db
    ->where('id', $idPuesto)
    ->update('puesto', $puesto);
  }   
}