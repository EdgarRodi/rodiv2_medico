<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadistica_model extends CI_Model{

  function countCandidatos(){
    $this->db
    ->select("COUNT(bgc.id) as total")
    ->from('candidato as c')
    ->join('candidato_bgc as bgc','bgc.id_candidato = c.id')
    ->where('c.cancelado', 0)
    ->where('c.eliminado', 0);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function countCandidatosCancelados(){
    $this->db
    ->select("COUNT(c.id) as total")
    ->from('candidato as c')
    ->where('c.cancelado', 1)
    ->where('c.eliminado', 0);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function countCandidatosEliminados(){
    $this->db
    ->select("COUNT(c.id) as total")
    ->from('candidato as c')
    ->where('c.eliminado', 1);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function countCandidatosAnalista($id_candidato){
    $this->db
    ->select("COUNT(bgc.id) as total")
    ->from('candidato as c')
    ->join('candidato_bgc as bgc','bgc.id_candidato = c.id')
    ->where('c.id_usuario', $id_candidato)
    ->where('c.cancelado', 0)
    ->where('c.eliminado', 0);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function countCandidatosSinFormulario($id_usuario){
    $this->db
    ->select("COUNT(c.id) as total")
    ->from('candidato as c')
    //->where('c.id_usuario', $id_usuario)
    ->where('c.id_cliente', 1)
    ->where('c.fecha_contestado', NULL)
    ->where('c.cancelado', 0)
    ->where('c.eliminado', 0);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function countCandidatosSinDocumentos($id_usuario){
    $this->db
    ->select("COUNT(c.id) as total")
    ->from('candidato as c')
    //->where('c.id_usuario', $id_usuario)
    ->where('c.id_cliente', 1)
    ->where('c.fecha_documentos', NULL)
    ->where('c.cancelado', 0)
    ->where('c.eliminado', 0);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
}