<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_general_model extends CI_Model{

  function getSubclientes($id_cliente){
    $data['subclientes'] = $this->getSubclientesOmitidos($id_cliente);
    $subclientes[] = -1;
    if($data['subclientes']){
      foreach($data['subclientes'] as $sub){
        $subclientes[] = $sub->id_subcliente;
      }
    }
    $this->db
    ->select('id, nombre, empresa, razon_social')
    ->from('subcliente')
    ->where('id_cliente', $id_cliente)
    ->where('status', 1)
    ->where('eliminado', 0)
    ->where_not_in('id', $subclientes)
    ->order_by('nombre','ASC');

    $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
    }else{
      return FALSE;
    }
  }
  function getDatosCliente($id_cliente){
    $this->db
    ->select('cl.*')
    ->from('cliente as cl')
    ->where('cl.id',$id_cliente);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function checkCandidatoRepetidoSubcliente($nombre, $paterno, $materno, $id_cliente, $id_subcliente){
    $this->db
   ->select('c.id')
   ->from('candidato as c')
   ->join("candidato_pruebas as pr","pr.id_candidato = c.id")
   ->where('c.nombre', $nombre)
   ->where('c.paterno', $paterno)
   ->where('c.materno', $materno)
   ->where('c.id_cliente', $id_cliente)
   ->where('c.id_subcliente', $id_subcliente)
   ->where('pr.socioeconomico', 1)
   ->where('c.eliminado', 0);

   $query = $this->db->get();
   return $query->num_rows();
  }
  function getCandidatosCliente($id_cliente){
    $this->db
    ->select("c.id, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato")
    ->from('candidato as c')
    ->join("cliente as cl","cl.id = c.id_cliente")
    ->join('candidato_pruebas as pru','pru.id_candidato = c.id')
    ->join('puesto as p','p.id = c.id_puesto')
    ->where('c.id_cliente', $id_cliente)
    ->where('c.eliminado', 0)
    ->where('pru.socioeconomico', 1)
    ->order_by('c.id','DESC');

    $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
    }else{
      return FALSE;
    }
  }
  function getTotal($id_cliente, $id_rol, $id_usuario){
    $usuario = ($id_rol == 2)? array('c.id_usuario =' => $id_usuario):array('c.id_usuario >' => 0);
    $this->db
    ->select("c.id")
    ->from("candidato as c")
    ->join('candidato_pruebas as pru','pru.id_candidato = c.id')
    ->where("c.id_cliente", $id_cliente)
    ->where("c.eliminado", 0)
    ->where('pru.socioeconomico', 1)
    ->where($usuario);

    $query = $this->db->get();
    return $query->num_rows();
  }
  function getCandidatos($id_cliente, $id_rol, $id_usuario){
    $usuario = ($id_rol == 2)? array('c.id_usuario =' => $id_usuario):array('c.id_usuario >' => 0);
    $data['subclientes'] = $this->getSubclientesOmitidos($id_cliente);
    $subclientes[] = -1;
    if($data['subclientes']){
      foreach($data['subclientes'] as $sub){
        $subclientes[] = $sub->id_subcliente;
      }
    }
    $this->db
    ->select("c.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, sub.nombre as subcliente, dop.id as idDoping, dop.fecha_resultado, dop.resultado as resultado_doping, pru.tipo_antidoping, pru.medico, pru.psicometrico, p.nombre as puesto,estado.nombre as estado, mun.nombre as municipio, g.nombre as grado, vivienda.nombre as vivienda, hab.tiempo_residencia, hab.id_tipo_nivel_zona, hab.id_tipo_vivienda, hab.recamaras, hab.banios, hab.distribucion, hab.id_tipo_condiciones, hab.mobiliario, zona.nombre as zona, hab.calidad_mobiliario, cond.nombre as condiciones, hab.tamanio_vivienda, m.id as idMedico, m.imagen_historia_clinica as imagen, m.conclusion, psi.id as idPsicometrico, psi.archivo, c.religion as personales_religion, verdoc.licencia as ver_licencia, verdoc.licencia_institucion, verdoc.ine as ver_ine, verdoc.ine_ano, verdoc.ine_vertical, verdoc.ine_institucion, verdoc.domicilio, verdoc.fecha_domicilio, verdoc.imss as ver_imss, verdoc.imss_institucion, verdoc.rfc as ver_rfc, verdoc.rfc_institucion, verdoc.curp as ver_curp, verdoc.curp_institucion, verdoc.carta_recomendacion, verdoc.carta_recomendacion_institucion, verdoc.comentarios as ver_comentarios, civil.nombre as civil,vis.comentarios as visita_comentarios, f.creacion as fecha_final, f.tiempo, f.descripcion_personal1, f.descripcion_personal2, f.descripcion_personal3, f.descripcion_personal4, f.descripcion_laboral1, f.descripcion_laboral2, f.descripcion_socio1, f.descripcion_socio2, f.descripcion_visita1, f.descripcion_visita2, f.recomendable, f.id as idFinalizado, c.puesto as puesto_ingles, verdoc.penales as ver_penales, verdoc.penales_institucion, verdoc.pasaporte, verdoc.pasaporte_fecha, verdoc.forma_migratoria, verdoc.forma_migratoria_fecha, bgc.id as idBGC, bgc.identidad_check, bgc.global_searches_check, bgc.comentario_final, bgc.penales_check, bgc.ofac_check, bgc.empleo_check, bgc.estudios_check, bgc.creacion as fecha_final_ingles, bgc.tiempo as tiempo_ingles, CONCAT(us.nombre,' ',us.paterno) as usuario, pru.status_doping as doping_hecho, c.fecha_domicilio as fecha_doc_domicilio ")
    ->from('candidato as c')
    ->join("cliente as cl","cl.id = c.id_cliente")
    ->join("subcliente as sub","sub.id = c.id_subcliente","left")
    ->join("grado_estudio as g","g.id = c.id_grado_estudio","left")
    ->join('doping as dop','dop.id_candidato = c.id AND dop.status = 0','left')
    ->join('candidato_pruebas as pru','pru.id_candidato = c.id')
    ->join('puesto as p','p.id = c.id_puesto','left')
    ->join('estado','estado.id = c.id_estado','left')
    ->join('municipio as mun','mun.id = c.id_municipio','left')
    ->join('candidato_habitacion as hab','hab.id_candidato = c.id','left')
    ->join('tipo_vivienda as vivienda','vivienda.id = hab.id_tipo_vivienda','left')
    ->join('tipo_nivel_zona as zona','zona.id = hab.id_tipo_nivel_zona','left')
    ->join('tipo_condiciones as cond','cond.id = hab.id_tipo_condiciones','left')
    ->join('medico as m','c.id = m.id_candidato','left')
    ->join('psicometrico as psi','c.id = psi.id_candidato','left')
    ->join('verificacion_documento as verdoc','c.id = verdoc.id_candidato','left')
    ->join('visita as vis','c.id = vis.id_candidato','left')
    ->join('estado_civil as civil','c.id_estado_civil = civil.id','left')
    ->join('candidato_finalizado as f','c.id = f.id_candidato','left')
    ->join('candidato_bgc as bgc','c.id = bgc.id_candidato','left')
    ->join('usuario as us','us.id = c.id_usuario',"left")
    ->where('c.id_cliente', $id_cliente)
    ->where_not_in('c.id_subcliente', $subclientes)
    ->where('c.eliminado', 0)
    ->where('pru.socioeconomico', 1)
    ->where($usuario);

    $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
    }else{
      return FALSE;
    }
  }
  function getSubclientesOmitidos($id_cliente){
    $this->db
    ->select("id_subcliente")
    ->from('subclientes_omitidos')
    ->where('id_cliente', $id_cliente);

    $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
    }else{
      return FALSE;
    }
  }
}