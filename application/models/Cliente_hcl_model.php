<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_hcl_model extends CI_Model{

  function getTotal(){
    $this->db
    ->select("*")
    ->from("candidato")
    ->where("id_cliente", 2)
    ->where("eliminado", 0);

    $query = $this->db->get();
    return $query->num_rows();
  }
  function getCandidatos(){
      $this->db
      ->select("c.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, est.id as idEstudios,est.primaria_periodo, est.primaria_escuela, est.primaria_ciudad, est.primaria_certificado, est.primaria_validada, est.secundaria_periodo, est.secundaria_escuela, est.secundaria_ciudad, est.secundaria_certificado, est.secundaria_validada, est.preparatoria_periodo, est.preparatoria_escuela, est.preparatoria_ciudad, est.preparatoria_certificado, est.preparatoria_validada, est.licenciatura_periodo, est.licenciatura_escuela, est.licenciatura_ciudad, est.licenciatura_certificado, est.licenciatura_validada, est.otros_certificados, est.comentarios, est.carrera_inactivo, global.law_enforcement, global.regulatory, global.sanctions, global.other_bodies, global.media_searches, global.usa_sanctions, global.oig, global.interpol, global.facis, global.bureau, global.european_financial, global.fda, global.sdn, global.ofac, global.sam, global.global_comentarios, pro.nombre as proyecto,st.nombre as estudios, dop.id as idDoping, dop.fecha_resultado, dop.resultado as resultado_doping, may.id as idMayores, may.id_tipo_studies, may.periodo, may.escuela, may.ciudad, may.certificado, may.comentarios as estudios_comentarios, check.id as idCheck, check.education, check.employment, check.address, check.criminal, check.global_database, check.identity, check.military, check.other, bgc.identidad_check, bgc.empleo_check, bgc.estudios_check, bgc.visita_check, bgc.penales_check, bgc.ofac_check, bgc.laboratorio_check, bgc.medico_check, bgc.oig_check, bgc.global_searches_check, bgc.domicilios_check, bgc.comentario_final, bgc.id as idBGC, verdoc.ine as custom_ine, verdoc.ine_ano as custom_ine_ano, verdoc.ine_vertical as custom_ine_vertical, verdoc.ine_institucion as custom_ine_institucion, verdoc.pasaporte as custom_pasaporte, verdoc.pasaporte_fecha as custom_pasaporte_fecha, verdoc.militar as custom_militar, verdoc.militar_fecha as custom_militar_fecha, verdoc.comentarios as custom_comentarios, bgc.creacion as fecha_final, bgc.tiempo, bgc.credito_check, pru.antidoping, pru.medico, med.archivo_examen_medico, CONCAT(us.nombre,' ',us.paterno) as usuario,c.puesto as puesto_ingles")
      ->from('candidato as c')
      ->join('candidato_estudios as est','est.id_candidato = c.id',"left")
      ->join('candidato_global_searches as global','global.id_candidato = c.id',"left")
      ->join('tipo_studies as st','st.id = c.id_grado_estudio',"left")
      ->join('proyecto as pro','pro.id = c.id_proyecto')
      ->join('doping as dop','dop.id_candidato = c.id','left')
      ->join('verificacion_mayores_estudios as may','may.id_candidato = c.id',"left")
      ->join('verificacion_checklist as check','check.id_candidato = c.id',"left")
      ->join('candidato_bgc as bgc','bgc.id_candidato = c.id',"left")
      ->join('verificacion_documento as verdoc','verdoc.id_candidato = c.id',"left")
      ->join('candidato_pruebas as pru','pru.id_candidato = c.id',"left")
      ->join('medico as med','med.id_candidato = c.id',"left")
      ->join('usuario as us','us.id = c.id_usuario',"left")
      ->where('c.id_cliente',2)
      ->where('c.eliminado', 0);

      $query = $this->db->get();
      if($query->num_rows() > 0){
        return $query->result();
      }else{
        return FALSE;
      }
  }

}