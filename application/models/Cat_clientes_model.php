<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cat_clientes_model extends CI_Model{

  function getTotal(){
    $this->db
    ->select("c.id")
    ->from('cliente as c')
    ->where('c.ingles', 0)
    ->where('c.eliminado', 0);

    $query = $this->db->get();
    return $query->num_rows();
  }
  function getClientes(){
    $this->db
    ->select("c.*, CONCAT(uc.nombre,' ',uc.paterno) as referente, uc.correo as ref_correo, uc.nombre as nombreCliente, uc.paterno as paternoCliente, uc.id as idUsuarioCliente, uc.status as statusUsuario, COUNT(uc.id) as numero_accesos")
    ->from('cliente as c')
    ->join('usuario as u','u.id = c.id_usuario',"left")
    ->join('usuario_cliente as uc','uc.id_cliente = c.id',"left")
    ->where('c.eliminado', 0)
    ->order_by('c.creacion','ASC')
    ->group_by('c.id');

    $query = $this->db->get();
    if($query->num_rows() > 0){
        return $query->result();
    }else{
        return FALSE;
    }
  }
  function getUltimoCliente(){
    $this->db
    ->select('id')
    ->from('cliente')
    ->order_by('id','DESC')
    ->limit(1);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function registrar($cliente){
    $this->db->insert("cliente", $cliente);
  }
  function registrarPermiso($permiso){
    $this->db->insert("permiso", $permiso);
  }
  function editar($cliente, $idCliente){
    $this->db
    ->where('id', $idCliente)
    ->update('cliente', $cliente);
  }
  function editarAcceso($usuario, $idUsuarioCliente){
    $this->db
    ->where('id', $idUsuarioCliente)
    ->update('usuario_cliente', $usuario);
  }
  function registrarUsuario($usuario){
    $this->db->insert("usuario_cliente", $usuario);
  }
  function getCliente($idCliente){
    $this->db
    ->select('*')
    ->from('cliente')
    ->where('id',$idCliente);

    $consulta = $this->db->get();
    $resultado = $consulta->row();
    return $resultado;
  }
  function actuaizarUrl($idCliente, $url){
    $this->db
    ->set('url', $url)
    ->where('id', $idCliente)
    ->update('cliente');
  }
  function getAccesos($id_cliente){
    $this->db
    ->select("c.*,CONCAT(u.nombre,' ',u.paterno) as usuario, CONCAT(uc.nombre,' ',uc.paterno) as usuario_cliente, uc.correo as correo_usuario, uc.creacion as alta, uc.id as idUsuarioCliente")
    ->from("cliente as c")
    ->join("usuario_cliente as uc","uc.id_cliente = c.id")
    ->join("usuario as u","u.id = uc.id_usuario")
    ->where("c.id", $id_cliente)
    ->order_by("uc.id", 'desc');

    $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
    }else{
      return FALSE;
    }
  }
  function deleteAcceso($idUsuarioCliente){
    $this->db
    ->where('id', $idUsuarioCliente)
    ->delete('usuario_cliente');
  }
}