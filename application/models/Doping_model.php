<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doping_model extends CI_Model{

    function getDopingsTotal(){
        $this->db
        ->select("dop.*")
        ->from("doping as dop")
        ->join("candidato as c","dop.id_candidato = c.id")
        ->join('candidato_pruebas as pr','pr.id_candidato = c.id')
        ->where('pr.tipo_antidoping !=', 0)
        ->where('dop.fecha_resultado', NULL)
        ->where("c.eliminado", 0);

        $query = $this->db->get();
        return $query->num_rows();
    }
    function getDopings(){
        $this->db
        ->select("dop.*, c.nombre, c.paterno, c.materno, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as nombreCompleto, cl.nombre as cliente, sub.nombre as subcliente, pr.antidoping, c.fecha_nacimiento, CONCAT(u.nombre,' ',u.paterno) as usuario, pro.nombre as proyecto, paq.nombre as paquete, pr.socioeconomico, per.id as idPermiso")
        ->from('doping as dop')
        ->join('candidato as c','c.id = dop.id_candidato')
        ->join('candidato_pruebas as pr','pr.id_candidato = c.id')
        ->join('cliente as cl','cl.id = dop.id_cliente')
        ->join('subcliente as sub','sub.id = dop.id_subcliente',"left")
        ->join('usuario as u','u.id = dop.id_usuario')
        ->join('proyecto as pro','pro.id = dop.id_proyecto',"left")
        ->join('antidoping_paquete as paq','paq.id = dop.id_antidoping_paquete',"left")
        ->join('permiso as per','per.id_cliente = dop.id_cliente',"left")
        ->where('c.eliminado', 0)
        ->where('pr.tipo_antidoping !=', 0)
        ->where('dop.fecha_resultado', NULL)
        ->order_by('dop.id','DESC')
        ->group_by('dop.id');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getCandidatosSinDoping(){
        $this->db
        ->select('p.*,c.id as idCandidato, c.nombre, c.paterno, c.materno, cl.nombre as cliente, sub.nombre as subcliente, p.antidoping, pro.nombre as proyecto')
        ->from('candidato_pruebas as p')
        ->join('candidato as c','c.id = p.id_candidato')
        ->join('cliente as cl','cl.id = c.id_cliente')
        ->join('subcliente as sub','sub.id = c.id_subcliente','left')
        ->join('proyecto as pro','pro.id = c.id_proyecto','left')
        ->where_in('p.tipo_antidoping', [1,2])
        ->where('p.status_doping', 0)
        ->where('c.eliminado', 0)
        ->order_by('c.paterno','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getPaquetesAntidoping(){
        $this->db
        ->select('*')
        ->from('antidoping_paquete')
        ->where('status', 1)
        ->where('eliminado', 0)
        ->order_by('id','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    
    function getSubclientes($id_cliente){
        $this->db
        ->select('*')
        ->from('subcliente')
        ->where('id_cliente', $id_cliente)
        ->where('status', 1)
        ->where('eliminado', 0)
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getProyectos($id_cliente){
        $this->db
        ->select('*')
        ->from('proyecto')
        ->where('id_cliente', $id_cliente)
        ->where('status', 1)
        ->where('eliminado', 0)
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getProyectosSubcliente($id_subcliente){
        $this->db
        ->select('*')
        ->from('proyecto')
        ->where('id_subcliente', $id_subcliente)
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getPaqueteCliente($id_cliente, $id_subcliente, $id_proyecto){
        $this->db
        ->select('cl.id_antidoping_paquete, paq.nombre')
        ->from('cliente_doping as cl')
        ->join('antidoping_paquete as paq','paq.id = cl.id_antidoping_paquete')
        ->where('cl.id_cliente', $id_cliente)
        ->where('cl.id_subcliente', $id_subcliente)
        ->where('cl.id_proyecto', $id_proyecto);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
        
    }
    function getPaqueteSubcliente($id_cliente, $id_subcliente){
        $this->db
        ->select('id_antidoping_paquete')
        ->from('cliente_doping')
        ->where('id_cliente', $id_cliente)
        ->where('id_subcliente', $id_subcliente)
        ->where('id_proyecto', 0);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $resultado = $query->row();
            return $resultado;
        }else{
            return FALSE;
        }
    }
    function getPaqueteSubclienteProyecto($id_cliente, $id_proyecto, $id_subcliente){
        $this->db
        ->select('id_antidoping_paquete')
        ->from('cliente_doping')
        ->where('id_cliente', $id_cliente)
        ->where('id_subcliente', $id_subcliente)
        ->where('id_proyecto', $id_proyecto);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $resultado = $query->row();
            return $resultado;
        }else{
            return FALSE;
        }
    }
    function countDopings(){
        $this->db
        ->select("dop.id")
        ->from("doping as dop")
        ->order_by("dop.id","DESC")
        ->limit(1);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function getDatosCandidato($id_candidato){
        $this->db
        ->select('c.id, c.nombre, c.paterno, c.materno, c.id_cliente, c.id_subcliente, cl.clave as claveCliente, sub.clave as claveSubcliente, cl.nombre as cliente, sub.nombre as subcliente, pro.nombre as proyecto, c.fecha_nacimiento, c.id_proyecto, prueba.antidoping')
        ->from('candidato as c')
        ->join('cliente as cl','cl.id = c.id_cliente')
        ->join('subcliente as sub','sub.id = c.id_subcliente','left')
        ->join('doping as dop','dop.id_candidato = c.id','left')
        ->join('proyecto as pro','pro.id = dop.id_proyecto','left')
        ->join('candidato_pruebas as prueba','prueba.id_candidato = c.id','left')
        ->where('c.id', $id_candidato);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getParametrosCandidato($id_candidato){
        $this->db
        ->select('tipo_antidoping, antidoping')
        ->from('candidato_pruebas')
        ->where('id_candidato', $id_candidato);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getPaqueteCandidato($parametros){
        $this->db
        ->select('*')
        ->from('antidoping_paquete')
        ->where('id', $parametros);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function getSustanciaCandidato($id_sustancia){
        $this->db
        ->select('*')
        ->from('antidoping_sustancia')
        ->where('id', $id_sustancia);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function insertDoping($doping){
        $this->db->insert('doping', $doping);
        $id = $this->db->insert_id();
        return  $id;
    }
    function updateDoping($doping, $id_doping){
        $this->db
        ->where('id', $id_doping)
        ->update('doping', $doping);
    }
    function insertDetalleDoping($detalle){
        $this->db->insert('doping_detalle', $detalle);
    }
    
    function updatePruebaCandidato($id_candidato, $prueba){
        $this->db
        ->where('id_candidato', $id_candidato)
        ->update('candidato_pruebas', $prueba);
    }
    function checkPendienteDoping($nombre, $paterno, $materno){
        $this->db
        ->select('p.*,c.id as idCandidato, c.nombre, c.paterno, c.materno')
        ->from('candidato_pruebas as p')
        ->join('candidato as c','c.id = p.id_candidato')
        ->where("c.nombre", $nombre)
        ->where("c.paterno", $paterno)
        ->where("c.materno", $materno)
        ->where('p.tipo_antidoping', 1)
        ->where('p.status_doping', 0);

        $query = $this->db->get();
        return $query->num_rows();
    }
    function insertCandidato($datos_candidato){
        $this->db->insert('candidato', $datos_candidato);
        $id = $this->db->insert_id();
        return  $id;
    }
    function updateCandidato($datos_candidato, $id_candidato){
        $this->db
        ->where('id', $id_candidato)
        ->update('candidato', $datos_candidato);        
    }
    function insertCandidatoPruebas($pruebas){
        $this->db->insert('candidato_pruebas', $pruebas);
    }
    function updateCandidatoPruebas($pruebas, $id_candidato){
        $this->db
        ->where('id_candidato', $id_candidato)
        ->update('candidato_pruebas', $pruebas);
    }
    function getDopingCandidato($id_doping){
        $this->db
        ->select("dop.*, tipo.nombre as identificacion")
        ->from("doping as dop")
        ->join("tipo_identificacion as tipo","tipo.id = dop.id_tipo_identificacion")
        ->where("dop.id", $id_doping);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function insertFacturaCandidato($datos_factura, $id_doping){
        $this->db
        ->where('id', $id_doping)
        ->update('doping', $datos_factura);
    }
    function getSustanciasDoping($id_doping){
        $this->db
        ->select('*')
        ->from('doping_detalle')
        ->where('id_doping', $id_doping);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function updateResultadoDoping($id_doping, $id_sustancia, $res){
        $this->db
        ->where('id_doping', $id_doping)
        ->where('id_sustancia', $id_sustancia)
        ->update('doping_detalle', $res);
    }
    function deleteDoping($id_doping, $borrado){
        $this->db
        ->where('id', $id_doping)
        ->update('doping', $borrado);
    }
    function deleteDopingDetalle($id_doping){
        $this->db
        ->where('id_doping', $id_doping)
        ->delete('doping_detalle');
    }
    function deleteCandidato($id_candidato){
        $this->db
        ->where('id', $id_candidato)
        ->delete('candidato');
    }
    function deleteDocumentoFactura($id_candidato){
        $this->db
        ->where('id_candidato', $id_candidato)
        ->delete('documento_factura');
    }
    function motivoEliminar($motivo){
        $this->db->insert('doping_eliminado', $motivo);
    }
    function getDatosDoping($id_doping){
        $this->db
        ->select('dop.*, c.nombre, c.paterno, c.materno, paq.nombre as paquete, paq.sustancias, cl.nombre as cliente, sub.nombre as subcliente, det.id_sustancia, pro.nombre as proyecto, ide.nombre as identificacion, c.fecha_nacimiento, paq.nombre as drogas')
        ->from('doping as dop')
        ->join('doping_detalle as det','det.id_doping = dop.id')
        ->join('candidato as c','c.id = dop.id_candidato')
        ->join('antidoping_paquete as paq','paq.id = dop.id_antidoping_paquete')
        ->join('cliente as cl','cl.id = dop.id_cliente')
        ->join('subcliente as sub','sub.id = dop.id_subcliente','left')
        ->join('proyecto as pro','pro.id = dop.id_proyecto','left')
        ->join('tipo_identificacion as ide','ide.id = dop.id_tipo_identificacion','left')
        ->where('dop.id', $id_doping);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function getLastDoping(){
        $this->db
        ->select('id')
        ->from('doping')
        ->order_by('id', 'DESC')
        ->limit(1);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function getDopingsEliminados(){
        $this->db
        ->select("elim.*, dop.fecha_doping, dop.codigo_prueba, CONCAT(us.nombre,' ',us.paterno) as usuario ")
        ->from('doping_eliminado as elim')
        ->join('doping as dop','dop.id = elim.id_doping')
        ->join('usuario as us','us.id = elim.id_usuario')
        ->order_by('elim.id','DESC')
        ->limit(15);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getDopingsFinalizadosTotal(){
        $this->db
        ->select("dop.id")
        ->from('doping as dop')
        ->join('candidato as c','c.id = dop.id_candidato')
        ->join('candidato_pruebas as pr','pr.id_candidato = c.id')
        ->join('cliente as cl','cl.id = dop.id_cliente')
        ->join('usuario as u','u.id = dop.id_usuario')
        ->where('c.eliminado', 0)
        ->where('pr.tipo_antidoping !=', 0)
        ->where('pr.status_doping', 1)
        ->where('dop.fecha_resultado !=', null);

        $query = $this->db->get();
        return $query->num_rows();
    }
    function getDopingsFinalizados(){
        $this->db
        ->select("dop.id, c.nombre, c.paterno, c.materno, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, sub.nombre as subcliente, pr.antidoping, pro.nombre as proyecto, paq.nombre as paquete")
        ->from('doping as dop')
        ->join('candidato as c','c.id = dop.id_candidato')
        ->join('candidato_pruebas as pr','pr.id_candidato = c.id')
        ->join('cliente as cl','cl.id = dop.id_cliente')
        ->join('subcliente as sub','sub.id = dop.id_subcliente',"left")
        //->join('usuario as u','u.id = dop.id_usuario')
        ->join('proyecto as pro','pro.id = dop.id_proyecto',"left")
        ->join('antidoping_paquete as paq','paq.id = dop.id_antidoping_paquete',"left")
        ->where('c.eliminado', 0)
        ->where('pr.tipo_antidoping !=', 0)
        ->where('pr.status_doping', 1)
        ->where('dop.fecha_resultado !=', null)
        ->order_by('dop.id','DESC')
        ->limit(300);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getDetalleDoping($idDoping){
        $this->db
        ->select("dop.*, c.nombre, c.paterno, c.materno, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, sub.nombre as subcliente, pr.antidoping, c.fecha_nacimiento, CONCAT(u.nombre,' ',u.paterno) as usuario, pro.nombre as proyecto, paq.nombre as paquete")
        ->from('doping as dop')
        ->join('candidato as c','c.id = dop.id_candidato')
        ->join('candidato_pruebas as pr','pr.id_candidato = c.id')
        ->join('cliente as cl','cl.id = dop.id_cliente')
        ->join('subcliente as sub','sub.id = dop.id_subcliente',"left")
        ->join('usuario as u','u.id = dop.id_usuario')
        ->join('proyecto as pro','pro.id = dop.id_proyecto',"left")
        ->join('antidoping_paquete as paq','paq.id = dop.id_antidoping_paquete',"left")
        ->where('c.eliminado', 0)
        ->where('pr.tipo_antidoping !=', 0)
        ->where('pr.status_doping', 1)
        ->where('dop.fecha_resultado !=', null)
        ->where('dop.id', $idDoping);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function cambiarEstatusDoping($id_candidato){
        $this->db
        ->set('status', 1)
        ->where('id_candidato', $id_candidato)
        ->update('doping');
    }
}