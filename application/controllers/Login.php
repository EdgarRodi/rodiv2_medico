<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	function __construct(){
		parent::__construct();
	}

	//Formulario de Login establecido por default
	function index(){
		$this->load->view('login/login_view');
	}
	//Vista del Dashboard SI hay o NO session; redireciconamiento a inicio desde menú
	function veryfing_account(){
		$this->form_validation->set_rules('correo', 'Email', 'required|trim');
		$this->form_validation->set_rules('pwd', 'Estatus final del BGC', 'required|trim');
      
		$this->form_validation->set_message('required','El campo {field} es obligatorio');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('not-found', 'Ingresa tu correo y contraseña');
			redirect('Login/index');
		} 
		else{
			$base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
			$pass = md5($base . $this->input->post('pwd'));
			$usuario = $this->usuario_model->existeUsuario($this->input->post('correo'), $pass);
			if ($usuario) {
				$usuario_data = array(
					"id" => $usuario->id,
					"nombre" => $usuario->nombre,
					"paterno" => $usuario->paterno,
					"rol" => $usuario->rol,
					"idrol" => $usuario->id_rol,
					"tipo" => 1,
					"loginBD" => $usuario->loginBD,
					"logueado" => TRUE
				);
				if ($usuario->id_rol != 3) {
					$this->session->set_userdata($usuario_data);
					redirect('Dashboard/index');
				} 
				else {
					$this->session->set_userdata($usuario_data);
					redirect('Dashboard/visitador_panel');
				}
			}
			else {
				$cliente = $this->usuario_model->existeUsuarioCliente($this->input->post('correo'), $pass);
				if ($cliente) {
					$cliente_data = array(
						"id" => $cliente->id,
						"correo" => $cliente->correo,
						"nombre" => $cliente->nombre,
						"paterno" => $cliente->paterno,
						"nuevopassword" => $cliente->nuevo_password,
						"idcliente" => $cliente->id_cliente,
						"cliente" => $cliente->cliente,
						"tipo" => 2,
						"loginBD" => $cliente->loginBD,
						"logueado" => TRUE
					);
	
					$this->session->set_userdata($cliente_data);
					if ($cliente->id_cliente == 1) {
						redirect('Dashboard/ustglobal_panel');
					}
					if ($cliente->id_cliente == 2) {
						redirect('Dashboard/hcl_panel');
					}
					if ($cliente->id_cliente == 3) {
						redirect('Dashboard/tata_panel');
					}
					if ($cliente->id_cliente != 1 && $cliente->id_cliente != 2) {
						redirect('Dashboard/clientes_panel');
					}
				} 
				else {
					$pass = md5($this->input->post('pwd') . $base);
					$candidato = $this->candidato_model->existeCandidato($this->input->post('correo'), $pass);
					if ($candidato) {
						if ($candidato->fecha_nacimiento != "" && $candidato->fecha_nacimiento != null) {
							$aux = explode('-', $candidato->fecha_nacimiento);
							$fnacimiento = $aux[1] . '/' . $aux[2] . '/' . $aux[0];
						} else {
							$fnacimiento = "";
						}
						$candidato_data = array(
							"id" => $candidato->id,
							"correo" => $candidato->correo,
							"nombre" => $candidato->nombre,
							"paterno" => $candidato->paterno,
							"materno" => $candidato->materno,
							"celular" => $candidato->celular,
							"fecha" => $fnacimiento,
							"status" => $candidato->status,
							"proceso" => $candidato->id_tipo_proceso,
							"proyecto" => $candidato->id_proyecto,
							"tipo" => 3,
							"logueado" => TRUE
						);
						$this->session->set_userdata($candidato_data);
						if ($candidato->id_tipo_proceso == 1) {
							if (
								$candidato->id_proyecto == 28 && $candidato->status == 0 ||
								$candidato->id_proyecto == 25 && $candidato->status == 0 ||
								$candidato->id_proyecto == 27 && $candidato->status == 0 ||
								$candidato->id_proyecto == 35 && $candidato->status == 0 ||
								$candidato->id_proyecto == 26 && $candidato->status == 0 ||
								$candidato->id_proyecto == 20 && $candidato->status == 0 ||
								$candidato->id_proyecto == 23 && $candidato->status == 0 ||
								$candidato->id_proyecto == 21 && $candidato->status == 0 ||
								$candidato->id_proyecto == 24 && $candidato->status == 0 ||
								$candidato->id_proyecto == 150 && $candidato->status == 0 ||
								$candidato->id_proyecto == 151 && $candidato->status == 0 ||
								$candidato->id_proyecto == 152 && $candidato->status == 0 ||
								$candidato->id_proyecto == 153 && $candidato->status == 0 ||
								$candidato->id_proyecto == 154 && $candidato->status == 0 ||
								$candidato->id_proyecto == 155 && $candidato->status == 0 ||
								$candidato->id_proyecto == 156 && $candidato->status == 0 ||
								$candidato->id_proyecto == 157 && $candidato->status == 0
							) {
								redirect('Dashboard/project_candidate');
							}
							if (
								$candidato->id_proyecto == 28 && $candidato->status == 1 ||
								$candidato->id_proyecto == 25 && $candidato->status == 1 ||
								$candidato->id_proyecto == 27 && $candidato->status == 1 ||
								$candidato->id_proyecto == 35 && $candidato->status == 1 ||
								$candidato->id_proyecto == 26 && $candidato->status == 1 ||
								$candidato->id_proyecto == 20 && $candidato->status == 1 ||
								$candidato->id_proyecto == 23 && $candidato->status == 1 ||
								$candidato->id_proyecto == 21 && $candidato->status == 1 ||
								$candidato->id_proyecto == 24 && $candidato->status == 1 ||
								$candidato->id_proyecto == 150 && $candidato->status == 1 ||
								$candidato->id_proyecto == 151 && $candidato->status == 1 ||
								$candidato->id_proyecto == 152 && $candidato->status == 1 ||
								$candidato->id_proyecto == 153 && $candidato->status == 1 ||
								$candidato->id_proyecto == 154 && $candidato->status == 1 ||
								$candidato->id_proyecto == 155 && $candidato->status == 1 ||
								$candidato->id_proyecto == 156 && $candidato->status == 1 ||
								$candidato->id_proyecto == 157 && $candidato->status == 1
							) {
								redirect('Dashboard/candidate_documents');
							} else {
								if ($candidato->status == 0) {
									redirect('Dashboard/candidate_form');
								}
								if ($candidato->status == 1) {
									redirect('Dashboard/candidate_documents');
								}
							}
						}
						if($candidato->id_tipo_proceso == 3 || $candidato->id_tipo_proceso == 4){
							if($candidato->formulario_contestado == null && $candidato->documentos_cargados == null){
								redirect('Dashboard/subcliente_candidate_form');
							}
							if($candidato->formulario_contestado != null && $candidato->documentos_cargados == null){
								redirect('Dashboard/candidate_documents');
							}
							if($candidato->formulario_contestado != null && $candidato->documentos_cargados != null){
								$this->session->set_flashdata('error', 'El usuario o la contraseña son incorrectos.');
								redirect('Login/index');
							}
						}
					} 
					else {
						$pass = md5($base . $this->input->post('pwd'));
						$subcliente = $this->usuario_model->existeUsuarioSubcliente($this->input->post('correo'), $pass);
						if ($subcliente) {
							$subcliente_data = array(
								"id" => $subcliente->id,
								"correo" => $subcliente->correo,
								"nombre" => $subcliente->nombre,
								"paterno" => $subcliente->paterno,
								"nuevopassword" => $subcliente->nuevo_password,
								"idcliente" => $subcliente->id_cliente,
								"idsubcliente" => $subcliente->id_subcliente,
								"cliente" => $subcliente->cliente,
								"subcliente" => $subcliente->subcliente,
								"tipo" => 4,
								"loginBD" => $subcliente->loginBD,
								"logueado" => TRUE
							);
	
							$this->session->set_userdata($subcliente_data);
							switch ($subcliente->tipo_acceso) {
								case 1:
									redirect('Dashboard/subclientes_general_panel');
									break;
								case 2:
									redirect('Dashboard/subclientes_ingles_panel');
									break;
							}
						} 
						else {
							$this->session->set_flashdata('not-found', 'El usuario o la contraseña son incorrectos');
							redirect('Login/index');
						}
					}
				}
			}
		}
	}
	//Funcion para salir del sistema y presentar el formulario del login
	function logout(){
		$usuario_data = array(
			'logueado' => FALSE
		);
		$this->session->sess_destroy();
		redirect('Login/index');
	}
}
