<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Psicometrico extends CI_Controller{

	function __construct(){
		parent::__construct();
    }
    function subirPsicometria(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_usuario = $this->session->userdata('id');
        $id_psicometrico = $_POST['id_psicometrico'];
        $id_candidato = $_POST['id_candidato'];
        $extension = pathinfo($_FILES['archivo']['name'], PATHINFO_EXTENSION);
        $nombre_archivo = $id_candidato."_psicometrico.".$extension;
        $config['upload_path'] = './_psicometria/';  
        $config['allowed_types'] = 'pdf';
        $config['overwrite'] = TRUE;
        /*$config['max_size']      = 1048; 
        $config['max_width']     = 1024; 
        $config['max_height']    = 768;*/
        $config['file_name'] = $nombre_archivo;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // File upload
        if($this->upload->do_upload('archivo')){
            $data = $this->upload->data();
        }
        if($id_psicometrico != ""){
            $doc = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'archivo' => $nombre_archivo
            );
            $this->psicometrico_model->updatePsicometrico($doc, $id_psicometrico);
        }
        else{
            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'archivo' => $nombre_archivo
            );
            $this->psicometrico_model->insertPsicometrico($doc);
        }
        
        echo $salida = 1;
    }
}