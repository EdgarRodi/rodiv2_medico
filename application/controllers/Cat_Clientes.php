<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cat_clientes extends CI_Controller{

	function __construct(){
		parent::__construct();
	}
  
  function index(){
    $data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
    $data['permisos_subclientes'] = $this->usuario_model->getPermisosSubclientes($this->session->userdata('id'));
    $data['accesos'] = $this->usuario_model->getModulos($this->session->userdata('idrol'));
    foreach($data['accesos'] as $acceso) {
        $items[] = $acceso->id_operaciones;
    }
    $data['acceso'] = $items;
    $datos['estados'] = $this->funciones_model->getEstados();
    $info['clientes'] = $this->funciones_model->getClientesActivos();
    $datos['modals'] = $this->load->view('modals/mdl_clientes',$info, TRUE);
    
    $this->load
    ->view('adminpanel/header',$data)
    ->view('adminpanel/scripts')
    ->view('catalogos/cliente',$datos)
    ->view('adminpanel/footer');
  }
  function getClientes(){
		$cliente['recordsTotal'] = $this->cat_clientes_model->getTotal();
    $cliente['recordsFiltered'] = $this->cat_clientes_model->getTotal();
    $cliente['data'] = $this->cat_clientes_model->getClientes();
    $this->output->set_output( json_encode( $cliente ) );
	}
  function registrar(){
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|is_unique[cliente.nombre]');
    $this->form_validation->set_rules('clave', 'Clave', 'required|trim|max_length[3]|is_unique[cliente.clave]');

    $this->form_validation->set_message('required','El campo %s es obligatorio');
    $this->form_validation->set_message('is_unique','El campo %s debe ser único');
    $this->form_validation->set_message('max_length','El campo %s debe tener máximo 3 carácteres');

    $msj = array();
    if ($this->form_validation->run() == FALSE) {
      $msj = array(
          'codigo' => 0,
          'msg' => validation_errors()
      );
    } 
    else {
      $id_usuario = $this->session->userdata('id');
      date_default_timezone_set('America/Mexico_City');
      $date = date('Y-m-d H:i:s');
      $nuevo_id = $this->cat_clientes_model->getUltimoCliente();
      $url = "Cliente/control/".($nuevo_id->id + 1);
      $cliente = array(
          'creacion' => $date,
          'edicion' => $date,
          'id_usuario' => $id_usuario,
          'nombre' => mb_strtoupper($this->input->post('nombre')),
          'clave' => mb_strtoupper($this->input->post('clave')),
          'icono' => '<i class="fas fa-user-tie"></i>',
          'url' => $url
      );
      $this->cat_clientes_model->registrar($cliente);
      $permiso = array(
          'id_usuario' => 1,
          'id_cliente' => ($nuevo_id->id + 1),
          'cliente' => mb_strtoupper($this->input->post('nombre'))
      );
      $this->cat_clientes_model->registrarPermiso($permiso);
      $msj = array(
          'codigo' => 1,
          'msg' => 'success'
      );
    }
    echo json_encode($msj);
  }
  function editar(){
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|is_unique[cliente.nombre]');
    $this->form_validation->set_rules('clave', 'Clave', 'required|trim|max_length[3]|is_unique[cliente.clave]');

    $this->form_validation->set_message('required','El campo %s es obligatorio');
    $this->form_validation->set_message('is_unique','El campo %s debe ser único');
    $this->form_validation->set_message('max_length','El campo %s debe tener máximo 3 carácteres');

    $msj = array();
    if ($this->form_validation->run() == FALSE) {
      $msj = array(
          'codigo' => 0,
          'msg' => validation_errors()
      );
    } 
    else {
      $id_usuario = $this->session->userdata('id');
      date_default_timezone_set('America/Mexico_City');
      $date = date('Y-m-d H:i:s');
      $cliente = array(
          'edicion' => $date,
          'id_usuario' => $id_usuario,
          'nombre' => mb_strtoupper($this->input->post('nombre')),
          'clave' => mb_strtoupper($this->input->post('clave')),
      );
      $this->cat_clientes_model->editar($cliente, $this->input->post('id'));
      $msj = array(
          'codigo' => 1,
          'msg' => 'success'
      );
    }
    echo json_encode($msj);
  }
  function accion(){
    $id_usuario = $this->session->userdata('id');
    date_default_timezone_set('America/Mexico_City');
    $date = date('Y-m-d H:i:s');
    $idCliente = $this->input->post('id');
    $idUsuarioCliente = $this->input->post('id_usuario_cliente');
    $accion = $this->input->post('accion');
    if($accion == "desactivar"){
        $cliente = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'status' => 0
        );
        $this->cat_clientes_model->editar($cliente, $idCliente);
        if($idUsuarioCliente != ""){
          $usuario = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'status' => 0
          );
          $this->cat_clientes_model->editarAcceso($usuario, $idUsuarioCliente);
        }
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    if($accion == "activar"){
        $cliente = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'status' => 1
        );
        $this->cat_clientes_model->editar($cliente, $idCliente);
        if($idUsuarioCliente != ""){
          $usuario = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'status' => 1
          );
          $this->cat_clientes_model->editarAcceso($usuario, $idUsuarioCliente);
        }
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    if($accion == "eliminar"){
        $cliente = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'eliminado' => 1
        );
        $this->cat_clientes_model->editar($cliente, $idCliente);
        if($idUsuarioCliente != ""){
          $usuario = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'eliminado' => 1
          );
          $this->cat_clientes_model->editarAcceso($usuario, $idUsuarioCliente);
        }
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    echo json_encode($msj);
  }
  function registrarUsuario(){
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
    $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim');
    $this->form_validation->set_rules('correo', 'Correo', 'required|trim|valid_email|is_unique[usuario_cliente.correo]');
    $this->form_validation->set_rules('password', 'Contraseña', 'required|trim');

    $this->form_validation->set_message('required','El campo %s es obligatorio');
    $this->form_validation->set_message('is_unique','El %s ya esta registrado');
    $this->form_validation->set_message('valid_email','El campo %s debe ser un correo válido');

    $msj = array();
    if ($this->form_validation->run() == FALSE) {
        $msj = array(
            'codigo' => 0,
            'msg' => validation_errors()
        );
    } 
    else {
        $id_usuario = $this->session->userdata('id');
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $nombre = $this->input->post('nombre');
        $paterno = $this->input->post('paterno');
        $correo = $this->input->post('correo');
        $uncode_password = $this->input->post('password');
        $base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
        $password = md5($base.$uncode_password);
        $idCliente = $this->input->post('id');
        $usuario = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'id_cliente' => $idCliente,
            'nombre' => $nombre,
            'paterno' => $paterno,
            'correo' => $correo,
            'password' => $password,
            'uncode_password' => $uncode_password
        );
        $this->cat_clientes_model->registrarUsuario($usuario);

        $dataCliente = $this->cat_clientes_model->getCliente($idCliente);
        if($dataCliente->ingles == 0){
            $url = "Cliente/control/".$idCliente;
            $this->cat_clientes_model->actuaizarUrl($idCliente, $url);
            $permiso = array(
                'id_usuario' => $id_usuario,
                'cliente' => $dataCliente->nombre,
                'id_cliente' => $idCliente
            );
            $this->cat_clientes_model->registrarPermiso($permiso);
        }
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    echo json_encode($msj);
  }
  function getClientesAccesos(){
    $id_cliente = $this->input->post('id_cliente');
    $salida = "";
    $data['usuarios'] = $this->cat_clientes_model->getAccesos($id_cliente);
    if($data['usuarios']){
      $salida .= '<table class="table table-striped">';
      $salida .= '<thead>';
      $salida .= '<tr>';
      $salida .= '<th scope="col">Nombre</th>';
      $salida .= '<th scope="col">Correo</th>';
      $salida .= '<th scope="col">Alta</th>';
      $salida .= '<th scope="col">Usuario</th>';
      $salida .= '<th scope="col">Eliminar</th>';
      $salida .= '</tr>';
      $salida .= '</thead>';
      $salida .= '<tbody>';
      foreach($data['usuarios'] as $u){
          $fecha = fecha_sinhora_espanol_bd($u->alta);
          $salida .= "<tr id='".$u->idUsuarioCliente."'><th>".$u->usuario_cliente."</th><th>".$u->correo_usuario."</th><th>".$fecha."</th><th>".$u->usuario."</th><th><a href='javascript:void(0)' class='fa-tooltip a-acciones' onclick='eliminarAcceso(".$u->idUsuarioCliente.")'><i class='fas fa-trash'></i></a></th></tr>";
      }
      $salida .= '</tbody>';
      $salida .= '</table>';
      echo $salida;
    }
    else{
      echo $salida .= '<p style="text-align:center; font-size: 20px;">No hay registro de accesos</p>';
    }
  }
  function controlAcceso(){
    $id_usuario = $this->session->userdata('id');
    date_default_timezone_set('America/Mexico_City');
    $date = date('Y-m-d H:i:s');
    $idUsuarioCliente = $this->input->post('idUsuarioCliente');
    $activo = $this->input->post('activo');
    if($idUsuarioCliente != ""){
      if($activo != -1){
        $usuario = array(
          'edicion' => $date,
          'id_usuario' => $id_usuario,
          'status' => $activo
        );
        $this->cat_clientes_model->editarAcceso($usuario, $idUsuarioCliente);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      else{
        $this->cat_clientes_model->deleteAcceso($idUsuarioCliente);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
  }
  
}