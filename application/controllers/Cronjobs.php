<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function generarTiempoEspanol(){
		date_default_timezone_set('America/Mexico_City');
        
        $data['candidatos'] = $this->cronjobs_model->getCandidatos();
        foreach($data['candidatos'] as $candidato){
        	$dias = 0; //dias transcurridos
        	$acum = 0;
        	$fecha_registro = $candidato->fecha_alta; //alta del candidato
        	$f_alta = explode(' ', $fecha_registro);
        	$fecha_fija = $f_alta[0].' 16:00:00';//hora limite para iniciar el contador de dias en 1
        	$fr = strtotime($fecha_registro);
        	$ff = strtotime($fecha_fija);
        	if($fr < $ff){
			   $dias = 1;//Si la fecha de registro es menor a la hora limite se inicia el dia en 1
        	}

        	//Verificacion del contador de dias con la fecha de regitro
	        $num_dia = date('N', $fr);
	        if($num_dia != 6 && $num_dia != 7){//Se evalua si el registro no fue hecho un sabado o domingo
	          	$f_aux = strtotime($f_alta[0]);
	          	$data['festivas'] = $this->cronjobs_model->getFechasFestivas();
	          	foreach($data['festivas'] as $festiva){
	          		$aux = explode(' ', $festiva->fecha);
	          		$fecha_festiva = strtotime($aux[0]);//Se extraen o definen los dias festivos
		          	if($f_aux == $fecha_festiva){//Se evalua si cada fecha festiva es diferente a la fecha de regitro
			            $dias = 0;
			            break;
		          	}
	          	}
	        }
	        $fecha_final = $candidato->fecha_final;//la fecha final es la fecha de creacion de la tabla candidato_finalizado
    	 	//Se consulta si existe registro del candidato en la tabla candidato_finalizado
	        if($fecha_final != "" && $fecha_final != null){
	        	$fin = explode(' ', $fecha_final);
	            $date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
	            $date2 = new DateTime($fin[0]);//fecha final
	            $diff = $date1->diff($date2);
	            if($diff->days != 0){
	              	for($i = 1; $i <= $diff->days; $i++){
	              		$acum = 0;
		                $siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
		                $sig = strtotime($siguiente);
		                $num_sig = date('N', $sig);
		                if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
		                  	foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
		                }
	              	}
	              	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizado($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	            else{
	            	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizado($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	        }
	        else{//Sin fecha de finalizacion de estudio
      			$date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
          		$date2 = new DateTime();//fecha actual
          		$date2->format('d/m/Y');
          		$diff = $date1->diff($date2);
          		if($diff->days != 0){
            		for($i = 1; $i <= $diff->days; $i++){
            			$acum = 0;
              			$siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
              			$sig = strtotime($siguiente);
              			$num_sig = date('N', $sig);
              			if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
                			foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
              			}
            		}
            		$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
          		else{
          			$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
        	}
        }
        echo "Calculo de dias en los procesos de clientes en espanol terminado";
	}
	function generarTiempoIngles(){
		date_default_timezone_set('America/Mexico_City');
        
        $data['candidatos'] = $this->cronjobs_model->getCandidatosIngles();
        foreach($data['candidatos'] as $candidato){
        	$dias = 0; //dias transcurridos
        	$acum = 0;
        	$fecha_registro = $candidato->fecha_alta; //alta del candidato
        	$f_alta = explode(' ', $fecha_registro);
        	$fecha_fija = $f_alta[0].' 16:00:00';//hora limite para iniciar el contador de dias en 1
        	$fr = strtotime($fecha_registro);
        	$ff = strtotime($fecha_fija);
        	if($fr < $ff){
			   $dias = 1;//Si la fecha de registro es menor a la hora limite se inicia el dia en 1
        	}

        	//Verificacion del contador de dias con la fecha de regitro
	        $num_dia = date('N', $fr);
	        if($num_dia != 6 && $num_dia != 7){//Se evalua si el registro no fue hecho un sabado o domingo
	          	$f_aux = strtotime($f_alta[0]);
	          	$data['festivas'] = $this->cronjobs_model->getFechasFestivas();
	          	foreach($data['festivas'] as $festiva){
	          		$aux = explode(' ', $festiva->fecha);
	          		$fecha_festiva = strtotime($aux[0]);//Se extraen o definen los dias festivos
		          	if($f_aux == $fecha_festiva){//Se evalua si cada fecha festiva es diferente a la fecha de regitro
			            $dias = 0;
			            break;
		          	}
	          	}
	        }
	        $fecha_final = $candidato->fecha_final;//la fecha final es la fecha de creacion de la tabla candidato_finalizado
    	 	//Se consulta si existe registro del candidato en la tabla candidato_finalizado
	        if($fecha_final != "" && $fecha_final != null){
	        	$fin = explode(' ', $fecha_final);
	            $date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
	            $date2 = new DateTime($fin[0]);//fecha final
	            $diff = $date1->diff($date2);
	            if($diff->days != 0){
	              	for($i = 1; $i <= $diff->days; $i++){
	              		$acum = 0;
		                $siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
		                $sig = strtotime($siguiente);
		                $num_sig = date('N', $sig);
		                if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
		                  	foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
		                }
	              	}
	              	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizadoIngles($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	            else{
	            	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizadoIngles($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	        }
	        else{//Sin fecha de finalizacion de estudio
      			$date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
          		$date2 = new DateTime();//fecha actual
          		$date2->format('d/m/Y');
          		$diff = $date1->diff($date2);
          		if($diff->days != 0){
            		for($i = 1; $i <= $diff->days; $i++){
            			$acum = 0;
              			$siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
              			$sig = strtotime($siguiente);
              			$num_sig = date('N', $sig);
              			if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
                			foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
              			}
            		}
            		$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
          		else{
          			$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
        	}
        }
        echo "Calculo de dias en los procesos de clientes en ingles terminado";
	}
	function generarTiempoInglesTipo2(){ //Para TATA
		date_default_timezone_set('America/Mexico_City');
        
        $data['candidatos'] = $this->cronjobs_model->getCandidatosInglesTipo2();
        foreach($data['candidatos'] as $candidato){
        	$dias = 0; //dias transcurridos
        	$acum = 0;
        	$fecha_registro = $candidato->fecha_alta; //alta del candidato
        	$f_alta = explode(' ', $fecha_registro);
        	$fecha_fija = $f_alta[0].' 17:00:00';//hora limite para iniciar el contador de dias en 1
        	$fr = strtotime($fecha_registro);
        	$ff = strtotime($fecha_fija);
        	if($fr < $ff){
			   $dias = 1;//Si la fecha de registro es menor a la hora limite se inicia el dia en 1
        	}

        	//Verificacion del contador de dias con la fecha de regitro
	        $num_dia = date('N', $fr);
	        if($num_dia != 6 && $num_dia != 7){//Se evalua si el registro no fue hecho un sabado o domingo
	          	$f_aux = strtotime($f_alta[0]);
	          	$data['festivas'] = $this->cronjobs_model->getFechasFestivas();
	          	foreach($data['festivas'] as $festiva){
	          		$aux = explode(' ', $festiva->fecha);
	          		$fecha_festiva = strtotime($aux[0]);//Se extraen o definen los dias festivos
		          	if($f_aux == $fecha_festiva){//Se evalua si cada fecha festiva es diferente a la fecha de regitro
			            $dias = 0;
			            break;
		          	}
	          	}
	        }
	        $fecha_final = $candidato->fecha_final;//la fecha final es la fecha de creacion de la tabla candidato_finalizado
    	 	//Se consulta si existe registro del candidato en la tabla candidato_finalizado
	        if($fecha_final != "" && $fecha_final != null){
	        	$fin = explode(' ', $fecha_final);
	            $date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
	            $date2 = new DateTime($fin[0]);//fecha final
	            $diff = $date1->diff($date2);
	            if($diff->days != 0){
	              	for($i = 1; $i <= $diff->days; $i++){
	              		$acum = 0;
		                $siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
		                $sig = strtotime($siguiente);
		                $num_sig = date('N', $sig);
		                if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
		                  	foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
		                }
	              	}
	              	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizadoIngles($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	            else{
	            	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizadoIngles($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	        }
	        else{//Sin fecha de finalizacion de estudio
      			$date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
          		$date2 = new DateTime();//fecha actual
          		$date2->format('d/m/Y');
          		$diff = $date1->diff($date2);
          		if($diff->days != 0){
            		for($i = 1; $i <= $diff->days; $i++){
            			$acum = 0;
              			$siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
              			$sig = strtotime($siguiente);
              			$num_sig = date('N', $sig);
              			if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
                			foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
              			}
            		}
            		$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
          		else{
          			$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
        	}
        }
        echo "Calculo de dias en los procesos de clientes en ingles tipo 2 terminado";
	}
	function generarTiempoEspanolProcesosEspecificos(){
		date_default_timezone_set('America/Mexico_City');
        
        $data['candidatos'] = $this->cronjobs_model->getCandidatosProcesosEspecificos();
        foreach($data['candidatos'] as $candidato){
        	$dias = 0; //dias transcurridos
        	$acum = 0;
        	$fecha_registro = $candidato->fecha_alta; //alta del candidato
        	$f_alta = explode(' ', $fecha_registro);
        	$fecha_fija = $f_alta[0].' 16:00:00';//hora limite para iniciar el contador de dias en 1
        	$fr = strtotime($fecha_registro);
        	$ff = strtotime($fecha_fija);
        	if($fr < $ff){
			   $dias = 1;//Si la fecha de registro es menor a la hora limite se inicia el dia en 1
        	}

        	//Verificacion del contador de dias con la fecha de regitro
	        $num_dia = date('N', $fr);
	        if($num_dia != 6 && $num_dia != 7){//Se evalua si el registro no fue hecho un sabado o domingo
	          	$f_aux = strtotime($f_alta[0]);
	          	$data['festivas'] = $this->cronjobs_model->getFechasFestivas();
	          	foreach($data['festivas'] as $festiva){
	          		$aux = explode(' ', $festiva->fecha);
	          		$fecha_festiva = strtotime($aux[0]);//Se extraen o definen los dias festivos
		          	if($f_aux == $fecha_festiva){//Se evalua si cada fecha festiva es diferente a la fecha de regitro
			            $dias = 0;
			            break;
		          	}
	          	}
	        }
	        $fecha_final = $candidato->fecha_final;//la fecha final es la fecha de creacion de la tabla candidato_finalizado
    	 	//Se consulta si existe registro del candidato en la tabla candidato_finalizado
	        if($fecha_final != "" && $fecha_final != null){
	        	$fin = explode(' ', $fecha_final);
	            $date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
	            $date2 = new DateTime($fin[0]);//fecha final
	            $diff = $date1->diff($date2);
	            if($diff->days != 0){
	              	for($i = 1; $i <= $diff->days; $i++){
	              		$acum = 0;
		                $siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
		                $sig = strtotime($siguiente);
		                $num_sig = date('N', $sig);
		                if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
		                  	foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
		                }
	              	}
	              	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizadoIngles($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	            else{
	            	$datos = array(
	            		'tiempo' => $dias
	            	);
	            	$this->cronjobs_model->registroDiasFinalizadoIngles($datos, $candidato->idFin);
	            	$datos2 = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos2, $candidato->idCandidato);
	            }
	        }
	        else{//Sin fecha de finalizacion de estudio
      			$date1 = new DateTime($f_alta[0]);//Se toma la fecha solamente de registro, la hora no importa porque se calcula al principio y despues de ello se omite para contabilizar los dias entre fechas
          		$date2 = new DateTime();//fecha actual
          		$date2->format('d/m/Y');
          		$diff = $date1->diff($date2);
          		if($diff->days != 0){
            		for($i = 1; $i <= $diff->days; $i++){
            			$acum = 0;
              			$siguiente = date("Y-m-d",strtotime(date($f_alta[0])."+ ".$i." days")); //dia siguiente suponiendo que sea el actual en ese momento
              			$sig = strtotime($siguiente);
              			$num_sig = date('N', $sig);
              			if($num_sig != 6 && $num_sig != 7){//Se evalua si el registro no fue hecho un sabado o domingo
                			foreach($data['festivas'] as $festiva){//Se extraen o definen los dias festivos
		                  		$aux = explode(' ', $festiva->fecha);
		                  		$fecha_festiva = strtotime($aux[0]);
			                  	if($sig == $fecha_festiva){
			                    	$acum++; //Si la fecha siguiente al dia de registro es igual a una fecha festiva se incrementa el acumulador funcionando como indicador
			                  	}
			                }
			                if($acum == 0){
			                	$dias++;//SI la fecha festiva no es igual (es decir $acum = 0) a la fecha siguiente de la fecha registro se incrementa el dia
			                }
              			}
            		}
            		$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
          		else{
          			$datos = array(
                		'tiempo_parcial' => $dias
                	);
                	$this->cronjobs_model->registroDiasEnProceso($datos, $candidato->idCandidato);
          		}
        	}
        }
        echo "Calculo de dias en los procesos de clientes en espanol terminado";
	}
	function generarPDFClientesEspanol(){
		$mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = 5163;
        $data['datos'] = $this->candidato_model->getInfoCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $ffin = $row->fecha_fin;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $subcliente = $row->subcliente;
            $id_doping = $row->idDoping;
            $id_cliente = $row->id_cliente;
        }
        $fecha_fin = $this->formatoFechaEspanol($ffin);
        $f_alta = $this->formatoFechaEspanol($f);
        $hoy = $this->formatoFecha($hoy);
        $data['finalizado'] = $this->candidato_model->getDatosFinalizadosCandidato($id_candidato);
        $data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['pruebas'] = $this->candidato_model->getPruebasCandidato($id_candidato);
        $data['docs'] = $this->candidato_model->getDocsCandidato($id_candidato);
        $data['academico'] = $this->candidato_model->getHistorialAcademicoCandidato($id_candidato);
        $data['sociales'] = $this->candidato_model->getAntecedentesSocialesCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliares($id_candidato);
        $data['egresos'] = $this->candidato_model->getEgresosFamiliares($id_candidato);
        $data['vivienda'] = $this->candidato_model->getDatosVivienda($id_candidato);
        $data['ref_personal'] = $this->candidato_model->getReferenciasPersonales($id_candidato);
        $data['ref_vecinal'] = $this->candidato_model->getReferenciasVecinales($id_candidato);
        $data['legal'] = $this->candidato_model->getVerificacionLegal($id_candidato);
        $data['nom'] = $this->candidato_model->getTrabajosNoMencionados($id_candidato);
        $data['finalizado'] = $this->candidato_model->getEstudioFinalizado($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getLaborales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['cliente'] = $cliente;
        $data['subcliente'] = $subcliente;
        $data['fecha_fin'] = $ffin;

        $html = $this->load->view('pdfs/candidato_general_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        if($id_cliente == 39){
            $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 100px;" src="'.base_url().'img/logo_talink.png"></div><div style="width: 33%; float: right;text-align: right;">Fecha de Registro: '.$f_alta.'<br>Fecha de Elaboración: '.$fecha_fin.'</div>');
            $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">4-EST-001.Rev. 01 <br>Fecha de Rev. 05/06/2020</p></div>');
            
        }
        else{
            $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Fecha de Registro: '.$f_alta.'<br>Fecha de Elaboración: '.$fecha_fin.'</div>');
            $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599<br><br>4-EST-001.Rev. 01 <br>Fecha de Rev. 05/06/2020</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        }        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Estudio_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
	}
	function generarAvancesUST(){
		$data['candidatos'] = $this->cronjobs_model->getCandidatosUST();
		foreach($data['candidatos'] as $c){
			$porcentaje = 0;
			if($c->estudios_comentarios != '' && $c->estudios_comentarios != null){//Comentarios historial academico
				$porcentaje += 10;
			}
			if($c->docs_comentarios != '' && $c->docs_comentarios != null){//Comentarios verificacion documentos
				$porcentaje += 10;
			}
			if($c->idRefPer != null){//minimo un id de candidato_ref_personal
				$porcentaje += 20;
			}
			if($c->idVerLaboral != null){//minimo un id de verificacion_ref_laboral
				$porcentaje += 20;
			}
			if($c->idVerEstudios != null){//minimo un id de verificacion de estudios
				$porcentaje += 10;
			}
			if($c->idEstatusLaboral != null){//minimo un id de verificacion laboral
				$porcentaje += 10;
			}
			if($c->idVerPenal != null){//minimo un id de verificacion penal
				$porcentaje += 10;
			}
			//Se checa el porcentaje en caso de que tenga los documentos obligatorios, los demas puede que no se tengan por alguna razon
			$data['docs'] = $this->cronjobs_model->getDocumentosObligatoriosUST($c->idCandidato);
			if($data['docs']){
				$aviso = 0;
				$ofac = 0;
				foreach ($data['docs'] as $doc) {
					if ($doc->id_tipo_documento == 8) { // Si tiene cargado el aviso de privacidad
						$aviso = 1;
					}
					if ($doc->id_tipo_documento == 11) { //Si tiene cargado el OFAC
						$ofac = 1;
					}
				}
				$p = ($aviso == 1) ? 5 : 0;
				$porcentaje += $p;
				$p2 = ($ofac == 1) ? 5 : 0;
				$porcentaje += $p2;
			}
			$this->cronjobs_model->cleanAvance($c->idCandidato);
			$this->cronjobs_model->actualizarAvance($porcentaje, $c->idCandidato);
		}
		echo "Calculo de porcentaje de avances en los procesos UST GLOBAL terminado";
	}
	/*function generarAvancesClientesGeneral(){
		$data['candidatos'] = $this->cronjobs_model->getCandidatosClientesGeneral();
		foreach($data['candidatos'] as $c){
			$porcentaje = 0;
			if($c->estudios_comentarios != '' && $c->estudios_comentarios != null){//Comentarios historial academico
				$porcentaje += 10;
			}
			if($c->docs_comentarios != '' && $c->docs_comentarios != null){//Comentarios verificacion documentos
				$porcentaje += 10;
			}
			if($c->idRefPer != null){//minimo un id de candidato_ref_personal
				$porcentaje += 20;
			}
			if($c->idVerLaboral != null){//minimo un id de verificacion_ref_laboral
				$porcentaje += 20;
			}
			if($c->idVerEstudios != null){//minimo un id de verificacion de estudios
				$porcentaje += 10;
			}
			if($c->idEstatusLaboral != null){//minimo un id de verificacion laboral
				$porcentaje += 10;
			}
			if($c->idVerPenal != null){//minimo un id de verificacion penal
				$porcentaje += 10;
			}
			//Se checa el porcentaje en caso de que tenga los documentos obligatorios, los demas puede que no se tengan por alguna razon
			$data['docs'] = $this->cronjobs_model->getDocumentosObligatoriosUST($c->idCandidato);
			if($data['docs']){
				$aviso = 0;
				$ofac = 0;
				foreach ($data['docs'] as $doc) {
					if ($doc->id_tipo_documento == 8) { // Si tiene cargado el aviso de privacidad
						$aviso = 1;
					}
					if ($doc->id_tipo_documento == 11) { //Si tiene cargado el OFAC
						$ofac = 1;
					}
				}
				$p = ($aviso == 1) ? 5 : 0;
				$porcentaje += $p;
				$p2 = ($ofac == 1) ? 5 : 0;
				$porcentaje += $p2;
			}
			$this->cronjobs_model->cleanAvance($c->idCandidato);
			$this->cronjobs_model->actualizarAvance($porcentaje, $c->idCandidato);
		}
		echo "Calculo de porcentaje de avances en los procesos UST GLOBAL terminado";
	}*/
	function formatoFecha($f){
        date_default_timezone_set('America/Mexico_City');
        $numeroDia = date('d', strtotime($f));
        $mes = date('F', strtotime($f));
        $anio = date('Y', strtotime($f));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_ES, $meses_EN, $mes);

        return $nombreMes." ".$numeroDia.", ".$anio;
    }
    function formatoFechaEspanol($f){
        date_default_timezone_set('America/Mexico_City');
        $numeroDia = date('d', strtotime($f));
        $mes = date('F', strtotime($f));
        $anio = date('Y', strtotime($f));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);

        return $nombreMes." ".$numeroDia.", ".$anio;
    }
}