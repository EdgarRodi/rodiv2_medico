<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_HCL extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function index(){
		if($this->session->userdata('logueado') && 
			$this->session->userdata('tipo') == 1 && 
			$this->session->userdata('idrol') == 1 || $this->session->userdata('idrol') == 2 || $this->session->userdata('idrol') == 6){
			$data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
			$data['permisos_subclientes'] = $this->usuario_model->getPermisosSubclientes($this->session->userdata('id'));
			$data['accesos'] = $this->usuario_model->getModulos($this->session->userdata('idrol'));
			foreach($data['accesos'] as $acceso) {
				$items[] = $acceso->id_operaciones;
			}
			$data['acceso'] = $items;
			$info['estados'] = $this->funciones_model->getEstados();
			$info['civiles'] = $this->funciones_model->getEstadosCiviles();
			$info['estudios'] = $this->funciones_model->getTiposEstudios();
			$info['proyectos'] = $this->candidato_model->getProyectosCliente(2);
      $info['tipos_docs'] = $this->funciones_model->getTiposDocumentos();
      $info['studies'] = $this->funciones_model->getTiposEstudios();
			$info['paises'] = $this->funciones_model->getPaises();

			$vista['modals'] = $this->load->view('modals/mdl_cliente_hcl', $info, TRUE);

			$this->load
			->view('adminpanel/header',$data)
			->view('adminpanel/scripts')
			->view('analista/hcl_index', $vista)
			->view('adminpanel/footer');
		}
		else{
			redirect('Login/index');
		}
	}
	/*----------------------------------------*/
  /*  Consultas 
  /*----------------------------------------*/
		function getCandidatos(){
			$cand['recordsTotal'] = $this->cliente_hcl_model->getTotal();
			$cand['recordsFiltered'] = $this->cliente_hcl_model->getTotal();
			$cand['data'] = $this->cliente_hcl_model->getCandidatos();
			$this->output->set_output( json_encode( $cand ) );
		}
		function getVerificacionDocumentos(){
			$id_candidato = $_POST['id_candidato'];
			$salida = "";
			$data['docs'] = $this->candidato_model->checkVerificacionDocumentos($id_candidato);
			if($data['docs']){
				foreach($data['docs'] as $doc){
						$salida .= $doc->licencia.'@@';
						$salida .= $doc->licencia_institucion.'@@';
						$salida .= $doc->ine.'@@';
						$salida .= $doc->ine_ano.'@@';
						$salida .= $doc->ine_vertical.'@@';
						$salida .= $doc->ine_institucion.'@@';
						$salida .= $doc->penales.'@@';
						$salida .= $doc->penales_institucion.'@@';
						$salida .= $doc->domicilio.'@@';
						$salida .= $doc->fecha_domicilio.'@@';
						$salida .= $doc->militar.'@@';
						$salida .= $doc->militar_fecha.'@@';
						$salida .= $doc->pasaporte.'@@';
						$salida .= $doc->pasaporte_fecha.'@@';
						$salida .= $doc->forma_migratoria.'@@';
						$salida .= $doc->forma_migratoria_fecha.'@@';
						$salida .= $doc->comentarios;
				}
				echo $salida;
			}
			else{
					echo $salida = 0;
			}
		}
		function getReferenciasLaborales(){
      $id_candidato = $this->input->post('id_candidato');
      $salida = "";
      $data['referencias'] = $this->candidato_model->getReferenciasLaborales($id_candidato);
      if($data['referencias']){
          foreach($data['referencias'] as $ref){
              $salida .= $ref->empresa."@@";
              $salida .= $ref->direccion."@@";
              $salida .= $ref->fecha_entrada."@@";
              $salida .= $ref->fecha_salida."@@";
              $salida .= $ref->telefono."@@";
              $salida .= $ref->puesto1."@@";
              $salida .= $ref->puesto2."@@";
              $salida .= $ref->salario1."@@";
              $salida .= $ref->salario2."@@";
              $salida .= $ref->jefe_nombre."@@";
              $salida .= $ref->jefe_correo."@@";
              $salida .= $ref->jefe_puesto."@@";
              $salida .= $ref->causa_separacion."@@";
              $salida .= $ref->id."###";
          }
          
      }
      echo $salida;
    }
		function getVerificacionesLaborales(){
      $id_candidato = $this->input->post('id_candidato');
      $salida = "";
      $data['referencias'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
      if($data['referencias']){
          foreach($data['referencias'] as $ref){
              $salida .= $ref->empresa."@@";
              $salida .= $ref->direccion."@@";
              $salida .= $ref->fecha_entrada."@@";
              $salida .= $ref->fecha_salida."@@";
              $salida .= $ref->telefono."@@";
              $salida .= $ref->puesto1."@@";
              $salida .= $ref->puesto2."@@";
              $salida .= $ref->salario1."@@";
              $salida .= $ref->salario2."@@";
              $salida .= $ref->jefe_nombre."@@";
              $salida .= $ref->jefe_correo."@@";
              $salida .= $ref->jefe_puesto."@@";
              $salida .= $ref->causa_separacion."@@";
              $salida .= $ref->notas."@@";
              $salida .= $ref->responsabilidad."@@";
              $salida .= $ref->iniciativa."@@";
              $salida .= $ref->eficiencia."@@";
              $salida .= $ref->disciplina."@@";
              $salida .= $ref->puntualidad."@@";
              $salida .= $ref->limpieza."@@";
              $salida .= $ref->estabilidad."@@";
              $salida .= $ref->emocional."@@";
              $salida .= $ref->honestidad."@@";
              $salida .= $ref->rendimiento."@@";
              $salida .= $ref->actitud."@@";
              $salida .= $ref->recontratacion."@@";
              $salida .= $ref->motivo_recontratacion."@@";
              $salida .= $ref->id."@@";
              $salida .= $ref->numero_referencia."###";
          }
          
      }
      echo $salida;
    }
		function getHistorialDomicilios(){
			$id_candidato = $this->input->post('id_candidato');
			$data['doms'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
			if($data['doms']){
				$salida = '';
				foreach($data['doms'] as $dom){
						$salida .= $dom->periodo.'@@';
						$salida .= $dom->causa.'@@';
						$salida .= $dom->calle.'@@';
						$salida .= $dom->exterior.'@@';
						$salida .= $dom->interior.'@@';
						$salida .= $dom->colonia.'@@';
						$salida .= $dom->id_estado.'@@';
						$salida .= $dom->id_municipio.'@@';
						$salida .= $dom->cp.'@@';
						$salida .= $dom->id.'@@';
						$salida .= $dom->domicilio_internacional.'@@';
						$salida .= $dom->pais.'###';
				}
				echo $salida;
			}
			else{
					$salida = 0;
			}
		}
		function getVerificacionHistorialDomicilios(){
			$id_candidato = $this->input->post('id_candidato');
			$salida = '';
			$verificacion = $this->candidato_model->getVerificacionHistorialDomicilios($id_candidato);
			if($verificacion != null){
					echo $salida = $verificacion->comentario;
			}
			else{
					echo $salida;
			}
		}
		function getReferenciasProfesionales(){
			$id_candidato = $this->input->post('id_candidato');
			$data['refs'] = $this->candidato_model->getReferenciasProfesionales($id_candidato);
			if($data['refs']){
					$salida = '';
					foreach($data['refs'] as $ref){
							$salida .= $ref->id.'@@';
							$salida .= $ref->nombre.'@@';
							$salida .= $ref->telefono.'@@';
							$salida .= $ref->tiempo_conocerlo.'@@';
							$salida .= $ref->donde_conocerlo.'@@';
							$salida .= $ref->puesto.'@@';
							$salida .= $ref->verificacion_tiempo.'@@';
							$salida .= $ref->verificacion_conocerlo.'@@';
							$salida .= $ref->verificacion_puesto.'@@';
							$salida .= $ref->cualidades.'@@';
							$salida .= $ref->desempeno.'@@';
							$salida .= $ref->recomienda.'@@';
							$salida .= $ref->comentarios.'###';
					}
					echo $salida;
			}
			else{
					$salida = 0;
			}
		}
		function checkCredito(){
			$id_candidato = $this->input->post('id_candidato');
			$salida = "";
			$data['historial'] = $this->candidato_model->checkCredito($id_candidato);
			if($data['historial']){
				foreach($data['historial'] as $h){
					$salida .= '<div class="col-md-3">
												<p class="text-center"><b>Del</b></p>
												<p class="text-center">'.$h->fecha_inicio.'</p>
												</div>
											<div class="col-md-3">
													<p class="text-center"><b>Al</b></p>
													<p class="text-center">'.$h->fecha_fin.'</p>
											</div>
											<div class="col-md-6">
													<label>Comentario</label>
													<p>'.$h->comentario.'</p>
											</div>';
				}
				echo $salida;
			}
			else{
				echo $salida = 0;
			}
	}
	/*----------------------------------------*/
  /*  Proceso 
  /*----------------------------------------*/
		function registrar(){
			$this->form_validation->set_rules('nombre', 'Nombre(s)', 'required|trim');
			$this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim');
			$this->form_validation->set_rules('materno', 'Segundo apellido', 'trim');
			$this->form_validation->set_rules('correo', 'Correo', 'required|trim|valid_email');
			$this->form_validation->set_rules('celular', 'Tel. Celular', 'trim|max_length[16]');
			$this->form_validation->set_rules('fijo', 'Tel. Casa', 'trim|max_length[16]');
			$this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'trim');
			$this->form_validation->set_rules('proyecto', 'Proyecto', 'required');
			$this->form_validation->set_rules('examen', 'Examen antidoping', 'required|trim');
			$this->form_validation->set_rules('medico', 'Examen Médico', 'required');
			$this->form_validation->set_rules('pais', 'País', 'trim');

			$this->form_validation->set_message('required', 'El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length', 'El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('valid_email', 'El campo {field} debe ser un correo válido');

			$msj = array();
      if ($this->form_validation->run() == FALSE) {
        $msj = array(
          'codigo' => 0,
          'msg' => validation_errors()
        );
      }
			else{
				if($this->session->userdata('idcliente') != null){
					$id_cliente = $this->session->userdata('idcliente');
				}
				else{
					$id_cliente = $this->input->post('id_cliente');
				}
				$nombre = strtoupper($this->input->post('nombre'));
				$paterno = strtoupper($this->input->post('paterno'));
				$materno = strtoupper($this->input->post('materno'));
				$cel = $this->input->post('celular');
				$tel = $this->input->post('fijo');
				$correo = strtolower($this->input->post('correo'));
				$fecha_nacimiento = $this->input->post('fecha_nacimiento');
				$proyecto = $this->input->post('proyecto');
				$pais = ($this->input->post('pais') == -1)? '' : $this->input->post('pais');
				$examen = $this->input->post('examen');
				$medico = $this->input->post('medico');
				$existeCandidato = $this->candidato_model->repetidoCandidato($nombre, $paterno, $materno, $correo, $id_cliente);
				if($existeCandidato > 0){
					$msj = array(
						'codigo' => 2,
						'msg' => 'El candidato ya existe'
					);
				}
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$usuario = $this->input->post('usuario');
          switch ($usuario) {
            case 1:
              $tipo_usuario = "id_usuario";
              break;
            case 2:
              $tipo_usuario = "id_usuario_cliente";
              break;
            case 3:
              $tipo_usuario = "id_usuario_subcliente";
              break;
          }
          $id_usuario = $this->session->userdata('id');
					$last = $this->candidato_model->lastIdCandidato();
					$last = ($last == null || $last == "")? 0 : $last;
					if($fecha_nacimiento != "" && $fecha_nacimiento != null){
							$fnacimiento = fecha_ingles_bd($fecha_nacimiento);
					}
					else{
							$fnacimiento = "";
					}
					if($proyecto != 25 && $proyecto != 128 && $proyecto != 135 && $proyecto != 136 && $proyecto != 137 && $proyecto != 138 && $proyecto != 140 && $proyecto != 147 && $proyecto != 148){
							$base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
							$aux = substr( md5(microtime()), 1, 8);
							$token = md5($aux.$base);
							$socioeconomico = 1;
					}
					if($proyecto == 25 || $proyecto == 128 || $proyecto == 135 || $proyecto == 136 || $proyecto == 137 || $proyecto == 138 || $proyecto == 140 || $proyecto == 147 || $proyecto == 148){
							$token = "completo";
							$socioeconomico = 0;
					}
					$tipo_antidoping = ($examen == 0)? 0:1;
					$antidoping = ($examen == 0)? 0:$examen;
					$data = array(
							'creacion' => $date,
							'edicion' => $date,
							$tipo_usuario => $id_usuario,
							'fecha_alta' => $date,
							'nombre' => $nombre,
							'paterno' => $paterno,
							'materno' => $materno,
							'correo' => $correo,
							'fecha_nacimiento' => $fnacimiento,
							'token' => $token,
							'id_cliente' => $id_cliente,
							'id_subcliente' => 0,
							'celular' => $cel,
							'telefono_casa' => $tel,
							'id_proyecto' => $proyecto,
							'subproyecto' => $pais
					);
					$this->candidato_model->guardarCandidato($data);
					$pruebas = array(
							'creacion' => $date,
							'edicion' => $date,
							$tipo_usuario => $id_usuario,
							'id_candidato' => ($last->id + 1),
							'id_cliente' => $id_cliente,
							'socioeconomico' => $socioeconomico,
							'tipo_antidoping' => $tipo_antidoping,
							'antidoping' => $antidoping,
							'medico' => $medico
					);
					$this->candidato_model->crearPruebas($pruebas);
					if($proyecto != 25 && $proyecto != 128 && $proyecto != 135 && $proyecto != 136 && $proyecto != 137 && $proyecto != 138 && $proyecto != 140 && 		$proyecto != 147 && $proyecto != 148){
						$from = $this->config->item('smtp_user');
						$to = $correo;
						$subject = strtolower($this->session->userdata('cliente'))." - credentials for register form";
						$datos['password'] = $aux;
						$datos['cliente'] = strtoupper($this->session->userdata('cliente'));
						$datos['email'] = $correo;
						$message = $this->load->view('login/mail_view',$datos,TRUE);
						$this->load->library('phpmailer_lib');
						$mail = $this->phpmailer_lib->load();
						$mail->isSMTP();
						$mail->Host     = 'rodi.com.mx';
						$mail->SMTPAuth = true;
						$mail->Username = 'rodicontrol@rodi.com.mx';
						$mail->Password = 'RRodi#2019@';
						$mail->SMTPSecure = 'ssl';
						$mail->Port     = 465;
						
						$mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
						$mail->addAddress($to);
						$mail->Subject = $subject;
						$mail->isHTML(true);
						$mailContent = $message;
						$mail->Body = $mailContent;

						if(!$mail->send()){
							$msj = array(
								'codigo' => 3,
								'msg' => $aux
							);
						}else{
							$msj = array(
								'codigo' => 4,
								'msg' => $aux
							);
						}
					}
					else{
						$msj = array(
							'codigo' => 1,
							'msg' => 'Success'
						);
					}
				}
			} 
			echo json_encode($msj);
		}
		function regenerarPassword(){
      $this->load->config('email');
      date_default_timezone_set('America/Mexico_City');
      $date = date('Y-m-d H:i:s');
      $id_candidato = $_POST['id_candidato'];
      $correo = $_POST['correo'];
      $base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
      $aux = substr( md5(microtime()), 1, 8);
      $token = md5($aux.$base);
      $this->candidato_model->regenerarPassword($id_candidato, $date, $token);
      $from = $this->config->item('smtp_user');
      $to = $correo;
      $subject = strtolower($this->session->userdata('cliente'))." - credentials for register form";
      $datos['password'] = $aux;
      $datos['cliente'] = strtoupper($this->session->userdata('cliente'));
      $datos['email'] = $correo;
      $message = $this->load->view('mails/mail_view',$datos,TRUE);
      $this->load->library('phpmailer_lib');
      $mail = $this->phpmailer_lib->load();
      $mail->isSMTP();
      $mail->Host     = 'rodi.com.mx';
      $mail->SMTPAuth = true;
      $mail->Username = 'rodicontrol@rodi.com.mx';
      $mail->Password = 'RRodi#2019@';
      $mail->SMTPSecure = 'ssl';
      $mail->Port     = 465;
      
      $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
      $mail->addAddress($to);
      $mail->Subject = $subject;
      $mail->isHTML(true);
      $mailContent = $message;
      $mail->Body = $mailContent;
      if(!$mail->send()){
          $msj = array(
              'codigo' => 3,
              'msg' => $aux
          );
      }else{
          $msj = array(
              'codigo' => 1,
              'msg' => $aux
          );
      }
      echo json_encode($msj);
    }
		function guardarDatosGenerales(){
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim|alpha');
      $this->form_validation->set_rules('materno', 'Segundo apellido', 'trim|alpha');
      $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required|trim');
      $this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'required|trim');
      $this->form_validation->set_rules('puesto', 'Puesto', 'required|trim');
      $this->form_validation->set_rules('genero', 'Género', 'required|trim');
      $this->form_validation->set_rules('calle', 'Calle', 'required|trim');
      $this->form_validation->set_rules('exterior', 'No. Exterior', 'required|trim|max_length[8]');
      $this->form_validation->set_rules('interior', 'No. Interior', 'trim|max_length[8]');
      $this->form_validation->set_rules('colonia', 'Colonia', 'required|trim');
      $this->form_validation->set_rules('estado', 'Estado', 'required|trim|numeric');
      $this->form_validation->set_rules('municipio', 'Municipio', 'required|trim|numeric');
      $this->form_validation->set_rules('cp', 'Código postal', 'required|trim|numeric|max_length[5]');
      $this->form_validation->set_rules('civil', 'Estado civil', 'required|trim');
      $this->form_validation->set_rules('celular', 'Tel. Celular', 'required|trim|max_length[16]');
      $this->form_validation->set_rules('tel_casa', 'Tel. Casa', 'trim|max_length[16]');
      $this->form_validation->set_rules('correo', 'Correo', 'required|trim|valid_email');

      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('valid_email','El campo {field} debe ser un correo válido');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('alpha','El campo {field} debe contener solo carácteres alfabéticos y sin acentos');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
          date_default_timezone_set('America/Mexico_City');
          $date = date('Y-m-d H:i:s');
          $id_candidato = $this->input->post('id_candidato');
          $id_usuario = $this->session->userdata('id');
          $fecha = fecha_espanol_bd($this->input->post('fecha_nacimiento'));
          $edad = calculaEdad($fecha);

          $candidato = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'nombre' => $this->input->post('nombre'),
              'paterno' => $this->input->post('paterno'),
              'materno' => $this->input->post('materno'),
              'fecha_nacimiento' => $fecha,
              'edad' => $edad,
              'puesto' => $this->input->post('puesto'),
              'nacionalidad' => $this->input->post('nacionalidad'),
              'genero' => $this->input->post('genero'),
              'calle' => $this->input->post('calle'),
              'exterior' => $this->input->post('exterior'),
              'interior' => $this->input->post('interior'),
              'entre_calles' => $this->input->post('entre_calles'),
              'colonia' => $this->input->post('colonia'),
              'id_estado' => $this->input->post('estado'),
              'id_municipio' => $this->input->post('municipio'),
              'cp' => $this->input->post('cp'),
              'id_estado_civil' => $this->input->post('civil'),
              'celular' => $this->input->post('celular'),
              'telefono_casa' => $this->input->post('tel_casa'),
              'correo' => $this->input->post('correo')
          );
          $this->candidato_model->editarCandidato($candidato, $id_candidato);
          $msj = array(
              'codigo' => 1,
              'msg' => 'success'
          );
      }
      echo json_encode($msj);
    }
		function guardarMayoresEstudios(){
      $this->form_validation->set_rules('mayor_estudios_candidato', 'Nivel escolar del Candidato', 'required|trim');
      $this->form_validation->set_rules('periodo_candidato', 'Periodo del Candidato', 'required|trim');
      $this->form_validation->set_rules('escuela_candidato', 'Escuela del Candidato', 'required|trim');
      $this->form_validation->set_rules('ciudad_candidato', 'Ciudad del Candidato', 'required|trim');
      $this->form_validation->set_rules('certificado_candidato', 'Certificado del Candidato', 'required|trim');
      $this->form_validation->set_rules('mayor_estudios_analista', 'Nivel escolar revisado por Analista', 'required|trim');
      $this->form_validation->set_rules('periodo_analista', 'Periodo revisado por Analista', 'required|trim');
      $this->form_validation->set_rules('escuela_analista', 'Escuela revisado por Analista', 'required|trim');
      $this->form_validation->set_rules('ciudad_analista', 'Ciudad revisado por Analista', 'required|trim');
      $this->form_validation->set_rules('certificado_analista', 'Certificado obtenido revisado por Analista', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios de la analista', 'required|trim');

      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('alpha','El campo {field} debe contener solo carácteres alfabéticos y sin acentos');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');


      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
						'codigo' => 0,
						'msg' => validation_errors()
				);
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $data['estudios'] = $this->candidato_model->revisionMayoresEstudios($id_candidato);
        if($data['estudios']){
          $candidato = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'id_grado_estudio' => $this->input->post('mayor_estudios_candidato'),
              'estudios_periodo' => $this->input->post('periodo_candidato'),
              'estudios_escuela' => $this->input->post('escuela_candidato'),
              'estudios_ciudad' => $this->input->post('ciudad_candidato'),
              'estudios_certificado' => $this->input->post('certificado_candidato')
          );
          $this->candidato_model->editarCandidato($candidato, $id_candidato);
          $verificacion = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'id_tipo_studies' => $this->input->post('mayor_estudios_analista'),
              'periodo' => $this->input->post('periodo_analista'),
              'escuela' => $this->input->post('escuela_analista'),
              'ciudad' => $this->input->post('ciudad_analista'),
              'certificado' => $this->input->post('certificado_analista'),
              'comentarios' => $this->input->post('comentarios')
          );
          $this->candidato_model->editarMayoresEstudios($verificacion, $id_candidato);
          $msj = array(
            'codigo' => 1,
            'msg' => 'success'
          );
        }
        else{
          $candidato = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'id_grado_estudio' => $this->input->post('mayor_estudios_candidato'),
              'estudios_periodo' => $this->input->post('periodo_candidato'),
              'estudios_escuela' => $this->input->post('escuela_candidato'),
              'estudios_ciudad' => $this->input->post('ciudad_candidato'),
              'estudios_certificado' => $this->input->post('certificado_candidato')
          );
          $this->candidato_model->editarCandidato($candidato, $id_candidato);
          $verificacion = array(
              'creacion' => $date,
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'id_candidato' => $id_candidato,
              'id_tipo_studies' => $this->input->post('mayor_estudios_analista'),
              'periodo' => $this->input->post('periodo_analista'),
              'escuela' => $this->input->post('escuela_analista'),
              'ciudad' => $this->input->post('ciudad_analista'),
              'certificado' => $this->input->post('certificado_analista'),
              'comentarios' => $this->input->post('comentarios')
          );
          $id_ver_estudios = $this->candidato_model->guardarMayoresEstudios($verificacion);
          $msj = array(
            'codigo' => 1,
            'msg' => 'success'
          );
        }
      }
      echo json_encode($msj);
    }
		function guardarGlobalesEspecial(){
      $this->form_validation->set_rules('sanctions', 'Global compliance & Sanctions database', 'required|trim');
      $this->form_validation->set_rules('media_searches', 'Global media searches', 'required|trim');
      $this->form_validation->set_rules('oig', 'Office of the Inspector General', 'required|trim');
      $this->form_validation->set_rules('interpol', 'Interpol check', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
          'id_usuario' => $id_usuario
        );
        $this->candidato_model->editarCandidato($candidato, $id_candidato);
        $global = array(
          'creacion' => $date,
          'id_usuario' => $id_usuario,
          'id_candidato' => $id_candidato,
          'sanctions' => $this->input->post('sanctions'),
          'media_searches' => $this->input->post('media_searches'),
          'oig' => $this->input->post('oig'),
          'interpol' => $this->input->post('interpol'),
          'global_comentarios' => $this->input->post('comentarios')
        );
        $this->candidato_model->eliminarGlobalSearches($id_candidato);
        $this->candidato_model->guardarGlobalSearches($global);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
		function guardarTrabajoGobierno(){
      if($this->input->post('caso') == 2){//Para HCL
          $this->form_validation->set_rules('inactivo', 'Break(s) in Employment', 'required|trim');
          
          $this->form_validation->set_message('required','El campo {field} es obligatorio');

          $msj = array();
          if ($this->form_validation->run() == FALSE) {
              $msj = array(
                  'codigo' => 0,
                  'msg' => validation_errors()
              );
          }
          else{
              date_default_timezone_set('America/Mexico_City');
              $date = date('Y-m-d H:i:s');
              $id_candidato = $this->input->post('id_candidato');
              $trabajo = ($this->input->post('trabajo')  !== null)? $this->input->post('trabajo'):'';
              $enterado = ($this->input->post('enterado') !== null)? $this->input->post('enterado'):'';
              $inactivo = ($this->input->post('inactivo')  !== null)? $this->input->post('inactivo'):'';
              $id_usuario = $this->session->userdata('id');
              $candidato = array(
                  'edicion' => $date,
                  'id_usuario' => $id_usuario,
                  'trabajo_gobierno' => $trabajo,
                  'trabajo_enterado' => $enterado,
                  'trabajo_inactivo' => $inactivo
              );
              $this->candidato_model->editarCandidato($candidato, $id_candidato);
              $msj = array(
                  'codigo' => 1,
                  'msg' => 'success'
              );
          }
          echo json_encode($msj);
      }
    }
		function guardarReferenciaLaboral(){
      $this->form_validation->set_rules('empresa', 'Empresa', 'required|trim');
      $this->form_validation->set_rules('direccion', 'Direccion', 'required|trim');
      $this->form_validation->set_rules('entrada', 'Fecha de entrada', 'required|trim');
      $this->form_validation->set_rules('salida', 'Fecha de salida', 'required|trim');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'required|trim|max_length[16]');
      $this->form_validation->set_rules('puesto1', 'Puesto inicial', 'required|trim');
      $this->form_validation->set_rules('puesto2', 'Puesto final', 'required|trim');
      $this->form_validation->set_rules('salario1', 'Salario inicial', 'required|trim|numeric');
      $this->form_validation->set_rules('salario2', 'Salario final', 'required|trim|numeric');
      $this->form_validation->set_rules('jefenombre', 'Jefe inmediato', 'required|trim');
      $this->form_validation->set_rules('jefecorreo', 'Correo del jefe inmediato', 'required|trim');
      $this->form_validation->set_rules('jefepuesto', 'Puesto del jefe inmediato', 'required|trim');
      $this->form_validation->set_rules('separacion', 'Causa de separación', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
          date_default_timezone_set('America/Mexico_City');
          $date = date('Y-m-d H:i:s');
          $id_candidato = $this->input->post('id_candidato');
          $num = $this->input->post('num');
          $idref = $this->input->post('idref');
          $id_usuario = $this->session->userdata('id');

          $data['refs'] = $this->candidato_model->revisionReferenciaLaboral($idref);
          if($data['refs']){
              $entrada = fecha_ingles_bd($this->input->post('entrada'));
              $salida = fecha_ingles_bd($this->input->post('salida'));
              $datos = array(
								'edicion' => $date,
								'empresa' => ucwords(mb_strtolower( $this->input->post('empresa'))),
								'direccion' => $this->input->post('direccion'),
								'fecha_entrada' => $entrada,
								'fecha_salida' => $salida,
								'telefono' => $this->input->post('telefono'),
								'puesto1' => $this->input->post('puesto1'),
								'puesto2' => $this->input->post('puesto2'),
								'salario1' => $this->input->post('salario1'),
								'salario2' => $this->input->post('salario2'),
								'jefe_nombre' => $this->input->post('jefenombre'),
								'jefe_correo' => mb_strtolower($this->input->post('jefecorreo')),
								'jefe_puesto' => $this->input->post('jefepuesto'),
								'causa_separacion' => $this->input->post('separacion')
              );
              $this->candidato_model->editarReferenciaLaboral($datos, $idref);
              $msj = array(
								'codigo' => 1,
								'msg' => 'success'
              );
          }
          else{
              $entrada = fecha_ingles_bd($this->input->post('entrada'));
              $salida = fecha_ingles_bd($this->input->post('salida'));
              $datos = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  'id_candidato' => $id_candidato,
                  'empresa' => ucwords(mb_strtolower( $this->input->post('empresa'))),
                  'direccion' => $this->input->post('direccion'),
                  'fecha_entrada' => $entrada,
                  'fecha_salida' => $salida,
                  'telefono' => $this->input->post('telefono'),
                  'puesto1' => $this->input->post('puesto1'),
                  'puesto2' => $this->input->post('puesto2'),
                  'salario1' => $this->input->post('salario1'),
                  'salario2' => $this->input->post('salario2'),
                  'jefe_nombre' => $this->input->post('jefenombre'),
                  'jefe_correo' => mb_strtolower($this->input->post('jefecorreo')),
                  'jefe_puesto' => $this->input->post('jefepuesto'),
                  'causa_separacion' => $this->input->post('separacion')
              );
              $id_nuevo = $this->candidato_model->guardarReferenciaLaboral($datos);
              $msj = array(
								'codigo' => 2,
								'msg' => $id_nuevo
              );
          }
      }
      echo json_encode($msj);
    }
		function guardarVerificacionLaboral(){
      $this->form_validation->set_rules('empresa', 'Empresa', 'required|trim');
      $this->form_validation->set_rules('direccion', 'Direccion', 'required|trim');
      $this->form_validation->set_rules('entrada', 'Fecha de entrada', 'required|trim');
      $this->form_validation->set_rules('salida', 'Fecha de salida', 'required|trim');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'required|trim|max_length[16]');
      $this->form_validation->set_rules('puesto1', 'Puesto inicial', 'required|trim');
      $this->form_validation->set_rules('puesto2', 'Puesto final', 'required|trim');
      $this->form_validation->set_rules('salario1', 'Salario inicial', 'required|trim|numeric');
      $this->form_validation->set_rules('salario2', 'Salario final', 'required|trim|numeric');
      $this->form_validation->set_rules('jefenombre', 'Jefe inmediato', 'required|trim');
      $this->form_validation->set_rules('jefecorreo', 'Correo del jefe inmediato', 'required|trim');
      $this->form_validation->set_rules('jefepuesto', 'Puesto del jefe inmediato', 'required|trim');
      $this->form_validation->set_rules('separacion', 'Causa de separación', 'required|trim');
      $this->form_validation->set_rules('notas', 'Notas', 'required|trim');
      $this->form_validation->set_rules('responsabilidad', 'Responsabilidad', 'required|trim');
      $this->form_validation->set_rules('iniciativa', 'Iniciativa', 'required|trim');
      $this->form_validation->set_rules('eficiencia', 'Eficiencia', 'required|trim');
      $this->form_validation->set_rules('disciplina', 'Disciplina', 'required|trim');
      $this->form_validation->set_rules('puntualidad', 'Puntualidad y asistencia', 'required|trim');
      $this->form_validation->set_rules('limpieza', 'Limpieza y orden', 'required|trim');
      $this->form_validation->set_rules('estabilidad', 'Estabilidad laboral', 'required|trim');
      $this->form_validation->set_rules('emocional', 'Estabilidad emocional', 'required|trim');
      $this->form_validation->set_rules('honesto', 'Honesto', 'required|trim');
      $this->form_validation->set_rules('rendimiento', 'Rendimiento', 'required|trim');
      $this->form_validation->set_rules('actitud', 'Actitud', 'required|trim');
      $this->form_validation->set_rules('recontratacion', '¿Lo(a) contrataría de nuevo?', 'required|trim');
      $this->form_validation->set_rules('motivo', '¿Por qué?', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
          date_default_timezone_set('America/Mexico_City');
          $date = date('Y-m-d H:i:s');
          $id_candidato = $this->input->post('id_candidato');
          $num = $this->input->post('num');
          $idverlab = $this->input->post('idverlab');
          $id_usuario = $this->session->userdata('id');

          $this->candidato_model->eliminarVerificacionLaboral($id_candidato, $num);
          $fentrada = fecha_ingles_bd($this->input->post('entrada'));
          $fsalida = fecha_ingles_bd($this->input->post('salida'));
          $verificacion_reflab = array(
              'creacion' => $date,
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'id_candidato' => $id_candidato,
              'numero_referencia' => $num,
              'empresa' => $this->input->post('empresa'), 
              'direccion' => $this->input->post('direccion'),
              'fecha_entrada' => $fentrada, 
              'fecha_salida' => $fsalida,
              'telefono' => $this->input->post('telefono'),
              'puesto1' => $this->input->post('puesto1'), 
              'puesto2' => $this->input->post('puesto2'),
              'salario1' => $this->input->post('salario1'), 
              'salario2' => $this->input->post('salario2'), 
              'jefe_nombre' => $this->input->post('jefenombre'), 
              'jefe_correo' => $this->input->post('jefecorreo'),
              'jefe_puesto' => $this->input->post('jefepuesto'), 
              'causa_separacion' => $this->input->post('separacion'), 
              'notas' => $this->input->post('notas'), 
              'responsabilidad' => $this->input->post('responsabilidad'),
              'iniciativa' => $this->input->post('iniciativa'), 
              'eficiencia' => $this->input->post('eficiencia'), 
              'disciplina' => $this->input->post('disciplina'), 
              'puntualidad' => $this->input->post('puntualidad'),
              'limpieza' => $this->input->post('limpieza'), 
              'estabilidad' => $this->input->post('estabilidad'),
              'emocional' => $this->input->post('emocional'),
              'honestidad' => $this->input->post('honesto'),
              'rendimiento' => $this->input->post('rendimiento'),
              'actitud' => $this->input->post('actitud'),
              'recontratacion' => $this->input->post('recontratacion'),
              'motivo_recontratacion' => $this->input->post('motivo')
          );
          $this->candidato_model->guardarVerificacionLaboral($verificacion_reflab);
          //$this->generarAvancesUST($id_candidato);
          $msj = array(
              'codigo' => 1,
              'msg' => 'success'
          );
      }
      echo json_encode($msj);
    }
		function guardarDocumentacion(){
			if($this->input->post('tipoProyecto') == 1){
				$this->form_validation->set_rules('lic_profesional', 'Número de documento del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('lic_institucion', 'Fecha / Institución del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'required|trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'required|trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
			if($this->input->post('tipoProyecto') == 2){
				$this->form_validation->set_rules('lic_profesional', 'Número de documento del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('lic_institucion', 'Fecha / Institución del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'required|trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'required|trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('penales_numero', 'Número del documento de Antecedentes Penales', 'trim');
				$this->form_validation->set_rules('penales_institucion', 'Fecha / Institución Antecedentes Penales', 'trim');
				$this->form_validation->set_rules('domicilio_numero', 'Número de documento del Comprobante de domicilio', 'required|trim');
				$this->form_validation->set_rules('domicilio_fecha', 'Fecha / Institución del Comprobante de domicilio', 'required|trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
			if($this->input->post('tipoProyecto') == 3){
				$this->form_validation->set_rules('lic_profesional', 'Número de documento del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('lic_institucion', 'Fecha / Institución del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'required|trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'required|trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('penales_numero', 'Número del documento de Antecedentes Penales', 'required|trim');
				$this->form_validation->set_rules('penales_institucion', 'Fecha / Institución Antecedentes Penales', 'required|trim');
				$this->form_validation->set_rules('domicilio_numero', 'Número de documento del Comprobante de domicilio', 'trim');
				$this->form_validation->set_rules('domicilio_fecha', 'Fecha / Institución del Comprobante de domicilio', 'trim');
				$this->form_validation->set_rules('militar_numero', 'Número de documento de la Carta o Cartilla Militar', 'required|trim');
				$this->form_validation->set_rules('militar_fecha', 'Fecha / Institución de la Carta o Cartilla Militar', 'required|trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
			if($this->input->post('tipoProyecto') == 4){
				$this->form_validation->set_rules('lic_profesional', 'Número de documento del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('lic_institucion', 'Fecha / Institución del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('penales_numero', 'Número del documento de Antecedentes Penales', 'required|trim');
				$this->form_validation->set_rules('penales_institucion', 'Fecha / Institución Antecedentes Penales', 'required|trim');
				$this->form_validation->set_rules('domicilio_numero', 'Número de documento del Comprobante de domicilio', 'trim');
				$this->form_validation->set_rules('domicilio_fecha', 'Fecha / Institución del Comprobante de domicilio', 'trim');
				$this->form_validation->set_rules('militar_numero', 'Número de documento de la Carta o Cartilla Militar', 'trim');
				$this->form_validation->set_rules('militar_fecha', 'Fecha / Institución de la Carta o Cartilla Militar', 'trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
			if($this->input->post('tipoProyecto') == 5){
				$this->form_validation->set_rules('lic_profesional', 'Número de documento del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('lic_institucion', 'Fecha / Institución del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'required|trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'required|trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('penales_numero', 'Número del documento de Antecedentes Penales', 'required|trim');
				$this->form_validation->set_rules('penales_institucion', 'Fecha / Institución Antecedentes Penales', 'required|trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
			if($this->input->post('tipoProyecto') == 6){
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'required|trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'required|trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('militar_numero', 'Número de documento de la Carta o Cartilla Militar', 'trim');
				$this->form_validation->set_rules('militar_fecha', 'Fecha / Institución de la Carta o Cartilla Militar', 'trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
			if($this->input->post('tipoProyecto') == 7){
				$this->form_validation->set_rules('lic_profesional', 'Número de documento del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('lic_institucion', 'Fecha / Institución del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('penales_numero', 'Número del documento de Antecedentes Penales', 'trim');
				$this->form_validation->set_rules('penales_institucion', 'Fecha / Institución Antecedentes Penales', 'trim');
				$this->form_validation->set_rules('domicilio_numero', 'Número de documento del Comprobante de domicilio', 'required|trim');
				$this->form_validation->set_rules('domicilio_fecha', 'Fecha / Institución del Comprobante de domicilio', 'required|trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
			if($this->input->post('tipoProyecto') == 8){
				$this->form_validation->set_rules('lic_profesional', 'Número de documento del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('lic_institucion', 'Fecha / Institución del Comprobante de Estudios', 'required|trim');
				$this->form_validation->set_rules('ine_clave', 'ID o clave de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('ine_registro', 'Año de registro de la INE (ID)', 'trim|numeric|max_length[4]');
				$this->form_validation->set_rules('ine_vertical', 'Número vertical de la INE (ID)', 'trim|numeric|max_length[13]');
				$this->form_validation->set_rules('ine_institucion', 'Fecha / Institución de la INE (ID)', 'required|trim');
				$this->form_validation->set_rules('pasaporte_numero', 'Número de documento del Pasaporte', 'trim');
				$this->form_validation->set_rules('pasaporte_institucion', 'Fecha / Institución del Pasaporte', 'trim');
				$this->form_validation->set_rules('penales_numero', 'Número del documento de Antecedentes Penales', 'trim');
				$this->form_validation->set_rules('penales_institucion', 'Fecha / Institución Antecedentes Penales', 'trim');
				$this->form_validation->set_rules('domicilio_numero', 'Número de documento del Comprobante de domicilio', 'trim');
				$this->form_validation->set_rules('domicilio_fecha', 'Fecha / Institución del Comprobante de domicilio', 'trim');
				$this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');

				$this->form_validation->set_message('required','El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
				$this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
						$msj = array(
								'codigo' => 0,
								'msg' => validation_errors()
						);
				} 
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$id_candidato = $this->input->post('id_candidato');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);
					$verificacion_documento = array(
						'creacion' => $date,
						'id_usuario' => $id_usuario,
						'id_candidato' => $id_candidato,
						'licencia' => $this->input->post('lic_profesional'),
						'licencia_institucion' => $this->input->post('lic_institucion'),
						'ine' => $this->input->post('ine_clave'),
						'ine_ano' => $this->input->post('ine_registro'),
						'ine_vertical' => $this->input->post('ine_vertical'),
						'ine_institucion' => $this->input->post('ine_institucion'),
						'penales' => $this->input->post('penales_numero'),
						'penales_institucion' => $this->input->post('penales_institucion'),
						'domicilio' => $this->input->post('domicilio_numero'),
						'fecha_domicilio' => $this->input->post('domicilio_fecha'),
						'militar' => $this->input->post('militar_numero'),
						'militar_fecha' => $this->input->post('militar_fecha'),
						'pasaporte' => $this->input->post('pasaporte_numero'),
						'pasaporte_fecha' => $this->input->post('pasaporte_institucion'),
						'comentarios' => $this->input->post('comentarios')
					);      
					$this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
					$this->candidato_model->guardarVerificacionDocumento($verificacion_documento);
					$msj = array(
						'codigo' => 1,
						'msg' => 'success'
					);
				}
			}
      echo json_encode($msj);
    }
		function finalizarProceso(){
			$this->form_validation->set_rules('comentario_final', 'Declaración final', 'required|trim');
      $this->form_validation->set_rules('bgc_status', 'Estatus final del BGC', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
			else{
				date_default_timezone_set('America/Mexico_City');
				$date = date('Y-m-d H:i:s');
				$id_candidato = $_POST['id_candidato'];
				$id_usuario = $this->session->userdata('id');
				//Checks
				$check_identidad = ($this->input->post('check_identidad') != -1)? $this->input->post('check_identidad'):3;
				$check_estudios = ($this->input->post('check_estudios') != -1)? $this->input->post('check_estudios'):3;
				$check_global = ($this->input->post('check_global') != -1)? $this->input->post('check_global'):3;
				$check_laboral = ($this->input->post('check_laboral') != -1)? $this->input->post('check_laboral'):3;
				$check_visita = ($this->input->post('check_visita') != -1)? $this->input->post('check_visita'):3;
				$check_laboratorio = ($this->input->post('check_laboratorio') != -1)? $this->input->post('check_laboratorio'):3;
				$check_domicilio = ($this->input->post('check_domicilio') != -1)? $this->input->post('check_domicilio'):3;
				$check_penales = ($this->input->post('check_penales') != -1)? $this->input->post('check_penales'):3;
				$check_ofac = ($this->input->post('check_ofac') != -1)? $this->input->post('check_ofac'):3;
				$check_credito = ($this->input->post('check_credito') != -1)? $this->input->post('check_credito'):3;
				$check_medico = ($this->input->post('check_medico') != -1)? $this->input->post('check_medico'):3;
			
				$bgc = array(
					'creacion' => $date,
					'edicion' => $date,
					'id_usuario' => $id_usuario,
					'id_candidato' => $id_candidato,
					'identidad_check' => $check_identidad,
					'empleo_check' => $check_laboral,
					'estudios_check' => $check_estudios,
					'visita_check' => $check_visita,
					'penales_check' => $check_penales,
					'ofac_check' => $check_ofac,
					'medico_check' => $check_medico,
					'laboratorio_check' => $check_laboratorio,
					'medico_check' => $check_medico,
					'global_searches_check' => $check_global,
					'domicilios_check' => $check_domicilio,
					'credito_check' => $check_credito,
					'comentario_final' => $this->input->post('comentario_final')
				);
				$this->candidato_model->eliminarBGC($id_candidato);
				$this->candidato_model->guardarBGC($bgc, $id_candidato);
				$this->candidato_model->statusBGCCandidato($this->input->post('bgc_status'), $id_candidato);
				$msj = array(
					'codigo' => 1,
					'msg' => 'success'
				);
			}
      echo json_encode($msj);
		}
		function guardarHistorialDomicilios(){
			if($this->input->post('tipo') == 'general'){
				$this->form_validation->set_rules('address_periodo', 'Periodo', 'required|trim');
				$this->form_validation->set_rules('address_causa', 'Causa de salida', 'required|trim');
				$this->form_validation->set_rules('address_calle', 'Calle', 'required|trim');
				$this->form_validation->set_rules('address_exterior', 'No. Exterior', 'required|trim');
				$this->form_validation->set_rules('address_interior', 'No. Interior', 'trim');
				$this->form_validation->set_rules('address_colonia', 'Colonia', 'required|trim');
				$this->form_validation->set_rules('address_estado', 'Estado', 'required|trim');
				$this->form_validation->set_rules('address_municipio', 'Municipio', 'required|trim');
				$this->form_validation->set_rules('address_cp', 'CP', 'required|trim');

				$this->form_validation->set_message('required', 'El campo {field} es obligatorio');
				$this->form_validation->set_message('max_length', 'El campo {field} debe tener máximo {param} carácteres');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
					$msj = array(
						'codigo' => 0,
						'msg' => validation_errors()
					);
				}
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$num =  $this->input->post('num');
					$id_candidato =  $this->input->post('id_candidato');
					$idDomicilio =  $this->input->post('id_domicilio');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);

					if($idDomicilio != ''){
						$dom = array(
							'edicion' => $date,
							'id_candidato' => $id_candidato,
							'periodo' => $this->input->post('address_periodo'),
							'causa' => $this->input->post('address_causa'),
							'calle' => $this->input->post('address_calle'),
							'exterior' => $this->input->post('address_exterior'),
							'interior' => $this->input->post('address_interior'),
							'colonia' => $this->input->post('address_colonia'),
							'id_estado' => $this->input->post('address_estado'),
							'id_municipio' => $this->input->post('address_municipio'),
							'cp' => $this->input->post('address_cp')
						);      
						$this->candidato_model->editarDomicilio($dom, $idDomicilio);
						$msj = array(
							'codigo' => 2,
							'msg' => 'success'
						);
					}
					else{
						$dom = array(
							'creacion' => $date,
							'edicion' => $date,
							'id_candidato' => $id_candidato,
							'periodo' => $this->input->post('address_periodo'),
							'causa' => $this->input->post('address_causa'),
							'calle' => $this->input->post('address_calle'),
							'exterior' => $this->input->post('address_exterior'),
							'interior' => $this->input->post('address_interior'),
							'colonia' => $this->input->post('address_colonia'),
							'id_estado' => $this->input->post('address_estado'),
							'id_municipio' => $this->input->post('address_municipio'),
							'cp' => $this->input->post('address_cp')
						);      
						$idDomicilio = $this->candidato_model->guardarDomicilio($dom);
						$msj = array(
							'codigo' => 1,
							'msg' => $idDomicilio
						);
					}
				}
				echo json_encode($msj);
			}
			if($this->input->post('tipo') == 'internacional'){
				$this->form_validation->set_rules('address_periodo', 'Periodo', 'required|trim');
				$this->form_validation->set_rules('address_causa', 'Causa de salida', 'required|trim');
				$this->form_validation->set_rules('address_domicilio', 'Calle', 'required|trim');
				$this->form_validation->set_rules('address_pais', 'No. Exterior', 'required|trim');

				$this->form_validation->set_message('required', 'El campo {field} es obligatorio');

				$msj = array();
				if ($this->form_validation->run() == FALSE) {
					$msj = array(
						'codigo' => 0,
						'msg' => validation_errors()
					);
				}
				else{
					date_default_timezone_set('America/Mexico_City');
					$date = date('Y-m-d H:i:s');
					$num =  $this->input->post('num');
					$id_candidato =  $this->input->post('id_candidato');
					$idDomicilio =  $this->input->post('id_domicilio');
					$id_usuario = $this->session->userdata('id');

					$candidato = array(
						'id_usuario' => $id_usuario
					);
					$this->candidato_model->editarCandidato($candidato, $id_candidato);

					if($idDomicilio != ''){
						$dom = array(
							'edicion' => $date,
							'id_candidato' => $id_candidato,
							'periodo' => $this->input->post('address_periodo'),
							'causa' => $this->input->post('address_causa'),
							'domicilio_internacional' => $this->input->post('address_domicilio'),
							'pais' => $this->input->post('address_pais')
						);      
						$this->candidato_model->editarDomicilio($dom, $idDomicilio);
						$msj = array(
							'codigo' => 2,
							'msg' => 'success'
						);
					}
					else{
						$dom = array(
							'creacion' => $date,
							'edicion' => $date,
							'id_candidato' => $id_candidato,
							'periodo' => $this->input->post('address_periodo'),
							'causa' => $this->input->post('address_causa'),
							'domicilio_internacional' => $this->input->post('address_domicilio'),
							'pais' => $this->input->post('address_pais')
						);      
						$idDomicilio = $this->candidato_model->guardarDomicilio($dom);
						$msj = array(
							'codigo' => 1,
							'msg' => $idDomicilio
						);
					}
				}
				echo json_encode($msj);
			}
		}
		function guardarComentarioVerificarDomicilios(){
			$this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');

			$this->form_validation->set_message('required', 'El campo {field} es obligatorio');

			$msj = array();
			if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
			}
			else{
				date_default_timezone_set('America/Mexico_City');
				$date = date('Y-m-d H:i:s');
				$id_candidato = $this->input->post('id_candidato');
				$comentario = $this->input->post('comentario');
				$id_usuario = $this->session->userdata('id');
				$this->candidato_model->eliminarVerificacionDomicilios($id_candidato);
				$domicilio = array(
					'creacion' => $date,
					'edicion' => $date,
					'id_usuario' => $id_usuario,
					'id_candidato' => $id_candidato,
					'comentario' => $comentario
				);
				$this->candidato_model->guardarVerificacionDomicilio($domicilio);
				$msj = array(
					'codigo' => 1,
					'msg' => 'Success'
				);
			}
			echo json_encode($msj);
		}
		function guardarGlobalesEspecifico(){
      $this->form_validation->set_rules('sanctions', 'General Services Administration Sanction', 'required|trim');
      $this->form_validation->set_rules('facis', 'FACIS Sanction Search', 'required|trim');
      $this->form_validation->set_rules('oig', 'Office of the Inspector General', 'required|trim');
      $this->form_validation->set_rules('bureau', 'Bureau of Industry and Security List of Denied Persons', 'required|trim');
      $this->form_validation->set_rules('european_financial', 'EU Freeze List Maintained by the European Union (financial sanctions)', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
          'id_usuario' => $id_usuario
        );
        $this->candidato_model->editarCandidato($candidato, $id_candidato);
        $global = array(
          'creacion' => $date,
          'id_usuario' => $id_usuario,
          'id_candidato' => $id_candidato,
          'sanctions' => $this->input->post('sanctions'),
          'facis' => $this->input->post('facis'),
          'oig' => $this->input->post('oig'),
          'bureau' => $this->input->post('bureau'),
          'european_financial' => $this->input->post('european_financial'),
          'global_comentarios' => $this->input->post('comentarios')
        );
        $this->candidato_model->eliminarGlobalSearches($id_candidato);
        $this->candidato_model->guardarGlobalSearches($global);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
		function guardarGlobalesGeneral(){
      $this->form_validation->set_rules('sanctions', 'Sanctions', 'required|trim');
      $this->form_validation->set_rules('regulatory', 'Regulatory', 'required|trim');
      $this->form_validation->set_rules('law_enforcement', 'Law enforcement', 'required|trim');
      $this->form_validation->set_rules('other_bodies', 'Other bodies', 'required|trim');
      $this->form_validation->set_rules('media_searches', 'Web and media searches', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
          'id_usuario' => $id_usuario
        );
        $this->candidato_model->editarCandidato($candidato, $id_candidato);
        $global = array(
          'creacion' => $date,
          'id_usuario' => $id_usuario,
          'id_candidato' => $id_candidato,
          'sanctions' => $this->input->post('sanctions'),
          'regulatory' => $this->input->post('regulatory'),
          'law_enforcement' => $this->input->post('law_enforcement'),
          'other_bodies' => $this->input->post('other_bodies'),
          'media_searches' => $this->input->post('media_searches'),
          'global_comentarios' => $this->input->post('comentarios')
        );
        $this->candidato_model->eliminarGlobalSearches($id_candidato);
        $this->candidato_model->guardarGlobalSearches($global);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
		function guardarGlobalesSanctions(){
      $this->form_validation->set_rules('sanctions', 'Global sanctions', 'required|trim');
      $this->form_validation->set_rules('usa_sanctions', 'USA sanctions', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
          'id_usuario' => $id_usuario
        );
        $this->candidato_model->editarCandidato($candidato, $id_candidato);
        $global = array(
          'creacion' => $date,
          'id_usuario' => $id_usuario,
          'id_candidato' => $id_candidato,
          'sanctions' => $this->input->post('sanctions'),
          'usa_sanctions' => $this->input->post('usa_sanctions'),
          'global_comentarios' => $this->input->post('comentarios')
        );
        $this->candidato_model->eliminarGlobalSearches($id_candidato);
        $this->candidato_model->guardarGlobalSearches($global);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
		function guardarCustomDatabase(){
      $this->form_validation->set_rules('sanctions', 'Sanctions', 'required|trim');
      $this->form_validation->set_rules('regulatory', 'Regulatory', 'required|trim');
      $this->form_validation->set_rules('law_enforcement', 'Law enforcement', 'required|trim');
      $this->form_validation->set_rules('other_bodies', 'Other bodies', 'required|trim');
      $this->form_validation->set_rules('media_searches', 'Web and media searches', 'required|trim');
      $this->form_validation->set_rules('sdn', 'SDN', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
          'id_usuario' => $id_usuario
        );
        $this->candidato_model->editarCandidato($candidato, $id_candidato);
        $global = array(
          'creacion' => $date,
          'id_usuario' => $id_usuario,
          'id_candidato' => $id_candidato,
          'sanctions' => $this->input->post('sanctions'),
          'regulatory' => $this->input->post('regulatory'),
          'law_enforcement' => $this->input->post('law_enforcement'),
          'other_bodies' => $this->input->post('other_bodies'),
          'media_searches' => $this->input->post('media_searches'),
          'sdn' => $this->input->post('sdn'),
          'global_comentarios' => $this->input->post('comentarios')
        );
        $this->candidato_model->eliminarGlobalSearches($id_candidato);
        $this->candidato_model->guardarGlobalSearches($global);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
		function guardarDatosGeneralesInternacionales(){
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim|alpha');
      $this->form_validation->set_rules('materno', 'Segundo apellido', 'trim|alpha');
      $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required|trim');
      $this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'required|trim');
      $this->form_validation->set_rules('puesto', 'Puesto', 'required|trim');
      $this->form_validation->set_rules('genero', 'Género', 'required|trim');
      $this->form_validation->set_rules('domicilio', 'Domicilio', 'required|trim');
      $this->form_validation->set_rules('pais', 'País', 'required|trim');
      $this->form_validation->set_rules('civil', 'Estado civil', 'required|trim');
      $this->form_validation->set_rules('celular', 'Tel. Celular', 'required|trim|max_length[16]');
      $this->form_validation->set_rules('tel_casa', 'Tel. Casa', 'trim|max_length[16]');
      $this->form_validation->set_rules('otro_telefono', 'Otro teléfono', 'trim|max_length[16]');
      $this->form_validation->set_rules('correo', 'Correo', 'required|trim|valid_email');

      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('valid_email','El campo {field} debe ser un correo válido');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('alpha','El campo {field} debe contener solo carácteres alfabéticos y sin acentos');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
      else{
				date_default_timezone_set('America/Mexico_City');
				$date = date('Y-m-d H:i:s');
				$id_candidato = $this->input->post('id_candidato');
				$id_usuario = $this->session->userdata('id');
				$fecha = fecha_espanol_bd($this->input->post('fecha_nacimiento'));
				$edad = calculaEdad($fecha);

				$candidato = array(
					'edicion' => $date,
					'id_usuario' => $id_usuario,
					'nombre' => $this->input->post('nombre'),
					'paterno' => $this->input->post('paterno'),
					'materno' => $this->input->post('materno'),
					'fecha_nacimiento' => $fecha,
					'edad' => $edad,
					'puesto' => $this->input->post('puesto'),
					'nacionalidad' => $this->input->post('nacionalidad'),
					'genero' => $this->input->post('genero'),
					'domicilio_internacional' => $this->input->post('domicilio'),
					'pais' => $this->input->post('pais'),
					'id_estado_civil' => $this->input->post('civil'),
					'celular' => $this->input->post('celular'),
					'telefono_casa' => $this->input->post('tel_casa'),
					'telefono_otro' => $this->input->post('otro_telefono'),
					'correo' => $this->input->post('correo')
				);
				$this->candidato_model->editarCandidato($candidato, $id_candidato);
				$msj = array(
					'codigo' => 1,
					'msg' => 'success'
				);
      }
      echo json_encode($msj);
    }
		function guardarReferenciaProfesional(){
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'required|trim');
      $this->form_validation->set_rules('tiempo', 'Tiempo de conocerlo(a)', 'required|trim');
      $this->form_validation->set_rules('conocido', 'Dónde lo(a) conoció', 'required|trim');
      $this->form_validation->set_rules('puesto', 'Qué puesto desempeñaba', 'required|trim');
      $this->form_validation->set_rules('tiempo2', 'Tiempo de conocer al candidato', 'required|trim');
      $this->form_validation->set_rules('conocido2', 'Dónde conoció al candidato', 'required|trim');
      $this->form_validation->set_rules('puesto2', 'Qué puesto desempeñaba', 'required|trim');
      $this->form_validation->set_rules('cualidades', 'Cualidades del candidato', 'required|trim');
      $this->form_validation->set_rules('desempeno', '¿Cómo fue el desempeño del candidato?', 'required|trim');
      $this->form_validation->set_rules('recomienda', '¿Recomienda al candidato?', 'required|trim');
      $this->form_validation->set_rules('comentario', 'Comentarios de la referencia', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
			else{
				date_default_timezone_set('America/Mexico_City');
				$date = date('Y-m-d H:i:s');
				$num = $this->input->post('num');
				$id_candidato = $this->input->post('id_candidato');

				$this->candidato_model->eliminarReferenciaProfesional($id_candidato, $num);
				$refpro = array(
					'creacion' => $date,
					'id_candidato' => $id_candidato,
					'numero' => $num,
					'nombre' => $this->input->post('nombre'),
					'telefono' => $this->input->post('telefono'),
					'tiempo_conocerlo' => $this->input->post('tiempo'),
					'donde_conocerlo' => $this->input->post('conocido'),
					'puesto' => $this->input->post('puesto'),
					'verificacion_tiempo' => $this->input->post('tiempo2'),
					'verificacion_conocerlo' => $this->input->post('conocido2'),
					'verificacion_puesto' => $this->input->post('puesto2'),
					'cualidades' => $this->input->post('cualidades'),
					'desempeno' => $this->input->post('desempeno'),
					'recomienda' => $this->input->post('recomienda'),
					'comentarios' => $this->input->post('comentario'),
				);
				$this->candidato_model->guardarReferenciaProfesional($refpro);
				$msj = array(
					'codigo' => 1,
					'msg' => 'success'
				);
			}
			echo json_encode($msj);
		}
		function createHistorialCrediticio(){
			$this->form_validation->set_rules('fi', 'Fecha inicio', 'required|trim');
      $this->form_validation->set_rules('ff', 'Fecha fin', 'required|trim');
      $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
			else{
				$id_candidato = $this->input->post('id_candidato');
				$fi = $this->input->post('fi');
				$ff = $this->input->post('ff');
				$comentario = $this->input->post('comentario');
				$id_usuario = $this->session->userdata('id');
				date_default_timezone_set('America/Mexico_City');
				$date = date('Y-m-d H:i:s');
				$salida = "";
				$credito = array(
					'creacion' => $date,
					'edicion' => $date,
					'id_usuario' => $id_usuario,
					'id_candidato' => $id_candidato,
					'fecha_inicio' => $fi,
					'fecha_fin' => $ff,
					'comentario' => $comentario
				);
				$this->candidato_model->guardarHistorialCrediticio($credito);
				$data['historial'] = $this->candidato_model->checkCredito($id_candidato);
				foreach($data['historial'] as $h){
					$salida .= '<div class="col-md-3">
												<p class="text-center"><b>Del</b></p>
												<p class="text-center">'.$h->fecha_inicio.'</p>
												</div>
											<div class="col-md-3">
													<p class="text-center"><b>Al</b></p>
													<p class="text-center">'.$h->fecha_fin.'</p>
											</div>
											<div class="col-md-6">
													<label>Comentario</label>
													<p>'.$h->comentario.'</p>
											</div>';
				}
				$msj = array(
					'codigo' => 1,
					'msg' => $salida
				);
			}
			echo json_encode($msj);
		}
		function guardarGlobalesInternacionalBasico(){
      $this->form_validation->set_rules('ofac', 'OFAC', 'required|trim');
      $this->form_validation->set_rules('sam', 'GSA/SAM', 'required|trim');
      $this->form_validation->set_rules('fda', 'FDA department search', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
				$msj = array(
					'codigo' => 0,
					'msg' => validation_errors()
				);
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
          'id_usuario' => $id_usuario
        );
        $this->candidato_model->editarCandidato($candidato, $id_candidato);
        $global = array(
          'creacion' => $date,
          'id_usuario' => $id_usuario,
          'id_candidato' => $id_candidato,
          'ofac' => $this->input->post('ofac'),
          'sam' => $this->input->post('sam'),
          'fda' => $this->input->post('fda'),
          'global_comentarios' => $this->input->post('comentarios')
        );
        $this->candidato_model->eliminarGlobalSearches($id_candidato);
        $this->candidato_model->guardarGlobalSearches($global);
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
		function createGeneral(){
			$mpdf = new \Mpdf\Mpdf();
			date_default_timezone_set('America/Mexico_City');
			$data['hoy'] = date("d-m-Y");
			$hoy = date("d-m-Y");
			$id_candidato = $_POST['idPDF'];
			$data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
			foreach($data['datos'] as $row){
					$f = $row->fecha_alta;
					$fform = $row->fecha_contestado;
					$fdocs = $row->fecha_documentos;
					$fbgc = $row->fecha_bgc;
					$nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
					$cliente = $row->cliente;
					$proyecto = $row->proyecto;
					$id_doping = $row->idDoping;
			}
			$fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
			$fecha_bgc = $fecha_bgc->format('F d, Y');
			$f_alta = formatoFecha($f);
			$fform = formatoFecha($fform);
			$fdocs = formatoFecha($fdocs);
			$fbgc = formatoFecha($fbgc);
			$hoy = formatoFecha($hoy);
			$data['bgc'] = $this->candidato_model->getBGC($id_candidato);
			$data['global_searches'] = $this->candidato_model->getGlobalSearches($id_candidato);
			$data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
			$data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
			$data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
			$data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
			$data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
			$data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
			$data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
			$data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
			$data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
			$data['ver_mayor_estudio'] = $this->candidato_model->getVerificacionMayoresEstudios($id_candidato);
			$data['ref_laboral'] = $this->candidato_model->getReferenciasLaborales($id_candidato);
			$data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
			$data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
			$data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
			$data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
			$data['analista'] = $this->candidato_model->getAnalista($id_candidato);
			$data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
			$data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
			$data['domicilios'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
			$data['ver_domicilios'] = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
			$data['cliente'] = $cliente;
			$data['proyecto'] = $proyecto;
			$data['fecha_bgc'] = $fecha_bgc;
			$data['pruebas'] = $this->candidato_model->getPruebasCandidato($id_candidato);
			$doping = $this->doping_model->getDatosDoping($id_doping);
			$data['doping'] = $doping;
			//$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
			$html = $this->load->view('pdfs/hcl_general_pdf',$data,TRUE);
			$mpdf->setAutoTopMargin = 'stretch';
			$mpdf->AddPage();
			$mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
			$mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
			
			$mpdf->WriteHTML($html);

			$mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
		}
		function createCustom(){
			$mpdf = new \Mpdf\Mpdf();
			date_default_timezone_set('America/Mexico_City');
			$data['hoy'] = date("d-m-Y");
			$hoy = date("d-m-Y");
			$id_candidato = $_POST['idPDF'];
			$data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
			foreach($data['datos'] as $row){
				$f = $row->fecha_alta;
				$fform = $row->fecha_contestado;
				$fdocs = $row->fecha_documentos;
				$fbgc = $row->fecha_bgc;
				$nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
				$cliente = $row->cliente;
				$proyecto = $row->proyecto;
				$id_doping = $row->idDoping;
			}
			$fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
			$fecha_bgc = $fecha_bgc->format('F d, Y');
			$f_alta = formatoFecha($f);
			$fform = formatoFecha($fform);
			$fdocs = formatoFecha($fdocs);
			$fbgc = formatoFecha($fbgc);
			$hoy = formatoFecha($hoy);
			$data['bgc'] = $this->candidato_model->getBGC($id_candidato);
			$data['global_searches'] = $this->candidato_model->getGlobalSearches($id_candidato);
			$data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
			$data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
			$data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
			$data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
			$data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
			$data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
			$data['domicilios'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
			$data['ver_domicilios'] = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
			$data['analista'] = $this->candidato_model->getAnalista($id_candidato);
			$data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
			$data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
			$data['cliente'] = $cliente;
			$data['proyecto'] = $proyecto;
			$data['fecha_bgc'] = $fecha_bgc;
			$data['pruebas'] = $this->candidato_model->getPruebasCandidato($id_candidato);
			$doping = $this->doping_model->getDatosDoping($id_doping);
			$data['doping'] = $doping;

			$html = $this->load->view('pdfs/hcl_custom_pdf',$data,TRUE);
			$mpdf->setAutoTopMargin = 'stretch';
			$mpdf->AddPage();
			$mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
			$mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599<br><br>4-EST-001.Rev. 01 <br>Fecha de Rev. 05/06/2020</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
			
			$mpdf->WriteHTML($html);

			$mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
		}
		function createInternacional(){
			$mpdf = new \Mpdf\Mpdf();
			date_default_timezone_set('America/Mexico_City');
			$data['hoy'] = date("d-m-Y");
			$hoy = date("d-m-Y");
			$id_candidato = $_POST['idPDF'];
			$data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
			foreach($data['datos'] as $row){
				$f = $row->fecha_alta;
				$fform = $row->fecha_contestado;
				$fdocs = $row->fecha_documentos;
				$fbgc = $row->fecha_bgc;
				$nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
				$cliente = $row->cliente;
				$proyecto = $row->proyecto;
				$id_doping = $row->idDoping;
			}
			$fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
			$fecha_bgc = $fecha_bgc->format('F d, Y');
			$f_alta = formatoFecha($f);
			$fform = formatoFecha($fform);
			$fdocs = formatoFecha($fdocs);
			$fbgc = formatoFecha($fbgc);
			$hoy = formatoFecha($hoy);
			$data['bgc'] = $this->candidato_model->getBGC($id_candidato);
			$data['global_searches'] = $this->candidato_model->getGlobalSearches($id_candidato);
			$data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
			$data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
			$data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
			$data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
			$data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
			$data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
			$data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
			$data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
			$data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
			$data['ver_mayor_estudio'] = $this->candidato_model->getVerificacionMayoresEstudios($id_candidato);
			$data['ref_laboral'] = $this->candidato_model->getReferenciasLaborales($id_candidato);
			$data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
			$data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
			$data['credito'] = $this->candidato_model->checkCredito($id_candidato);
			$data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
			$data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
			$data['analista'] = $this->candidato_model->getAnalista($id_candidato);
			$data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
			$data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
			$data['domicilios'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
			$data['ver_domicilios'] = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
			$data['ref_profesional'] = $this->candidato_model->getReferenciasProfesionales($id_candidato);
			$data['cliente'] = $cliente;
			$data['proyecto'] = $proyecto;
			$data['fecha_bgc'] = $fecha_bgc;
			$data['pruebas'] = $this->candidato_model->getPruebasCandidato($id_candidato);
			$doping = $this->doping_model->getDatosDoping($id_doping);
			$data['doping'] = $doping;
			//$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
			$html = $this->load->view('pdfs/hcl_internacional_pdf',$data,TRUE);
			$mpdf->setAutoTopMargin = 'stretch';
			$mpdf->AddPage();
			$mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
			$mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
			
			$mpdf->WriteHTML($html);

			$mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
		}
}