<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidato extends CI_Controller{

	function __construct(){
		parent::__construct();
	}
    /*----------------------------------------*/
    /*  Funciones Comunes para Candidatos
    /*----------------------------------------*/
        function checkEstatusEstudios(){
            $id_candidato = $this->input->post('id_candidato');
            $salida = "";
            $data['estatus'] = $this->candidato_model->checkEstatusEstudios($id_candidato);
            if($data['estatus']){
                foreach($data['estatus'] as $estudio){
                    $aux = explode('-', $estudio->fecha);
                    $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                    $salida .= '<div class="row">
                                    <div class="col-md-3">
                                        <p class="text-center"><b>Fecha</b></p>
                                        <p class="text-center">'.$fecha_estatus.'</p>
                                    </div>
                                    <div class="col-md-9">
                                        <label>Comentario / Estatus</label>
                                        <p>'.$estudio->comentarios.'</p>
                                    </div>
                                </div>';
                    $id_verificacion = $estudio->idVerificacion;
                    $terminado = $estudio->finalizado;
                }
                echo $salida.'@@'.$id_verificacion.'@@'.$terminado;
            }
            else{
                echo $salida = 0;
            }
        }
        function createEstatusEstudios(){
            $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');
            $this->form_validation->set_message('required','El campo {field} es obligatorio');
        
            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            } 
            else{
                $id_candidato = $this->input->post('id_candidato');
                $id_verificacion = $this->input->post('id_verificacion');
                $comentario = $this->input->post('comentario');
                $id_usuario = $this->session->userdata('id');
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $salida = "";
                if($id_verificacion == 0){
                    $nueva_verificacion = $this->candidato_model->createEstatusEstudios($id_candidato, $id_usuario, $date);
                    $this->candidato_model->createDetalleEstatusEstudio($nueva_verificacion, $date, $comentario);
                    $data['estatus'] = $this->candidato_model->checkEstatusEstudios($id_candidato);
                    foreach($data['estatus'] as $estudio){
                        $aux = explode('-', $estudio->fecha);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                        $salida .= '<div class="row">
                                        <div class="col-md-3">
                                            <p class="text-center"><b>Fecha</b></p>
                                            <p class="text-center">'.$fecha_estatus.'</p>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Comentario / Estatus</label>
                                            <p>'.$estudio->comentarios.'</p>
                                        </div>
                                    </div>';
                        $id_verificacion = $estudio->idVerificacion;
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_verificacion
                    );
                }
                else{
                    $this->candidato_model->createDetalleEstatusEstudio($id_verificacion, $date, $comentario);
                    $data['estatus'] = $this->candidato_model->checkEstatusEstudios($id_candidato);
                    foreach($data['estatus'] as $estudio){
                        $aux = explode('-', $estudio->fecha);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                        $salida .= '<div class="row">
                                        <div class="col-md-3">
                                            <p class="text-center"><b>Fecha</b></p>
                                            <p class="text-center">'.$fecha_estatus.'</p>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Comentario / Estatus</label>
                                            <p>'.$estudio->comentarios.'</p>
                                        </div>
                                    </div>';
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_verificacion
                    );
                }
            }
            echo json_encode($msj);
        }
        function finishEstatusEstudios(){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_verificacion = $this->input->post('id_verificacion');
            $id_usuario = $this->session->userdata('id');
            $this->candidato_model->finishEstatusEstudios($id_verificacion, $date, $id_usuario);
            //Calcula el porcentaje de avance
            /*$c = $this->candidato_model->getClienteCandidato($this->input->post('id_candidato'));
            if($c->id_cliente == 1)
                $this->generarAvancesUST($this->input->post('id_candidato'));*/
        }
        function checkEstatusLaborales(){
            $id_candidato = $this->input->post('id_candidato');
            $salida = "";
            $data['estatus'] = $this->candidato_model->checkEstatusLaborales($id_candidato);
            if($data['estatus']){
                foreach($data['estatus'] as $lab){
                    $aux = explode('-', $lab->fecha);
                    $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                    $salida .= '<div class="row">
                                    <div class="col-md-3">
                                        <p class="text-center"><b>Fecha</b></p>
                                        <p class="text-center">'.$fecha_estatus.'</p>
                                    </div>
                                    <div class="col-md-9">
                                        <label>Comentario / Estatus</label>
                                        <p>'.$lab->comentarios.'</p>
                                    </div>
                                </div>';
                    $id_status = $lab->idStatus;
                    $terminado = $lab->finalizado;
                }
                echo $salida.'@@'.$id_status.'@@'.$terminado;
            }
            else{
                echo $salida = 0;
            }
        }
        function createEstatusLaborales(){
            $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');
            $this->form_validation->set_message('required','El campo {field} es obligatorio');

            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            } 
            else{
                $id_candidato = $this->input->post('id_candidato');
                $id_verificacion = $this->input->post('id_verificacion');
                $comentario = $this->input->post('comentario');
                $id_usuario = $this->session->userdata('id');
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $salida = "";
                if($id_verificacion == 0){
                    $nuevo_status = $this->candidato_model->createEstatusLaborales($id_candidato, $id_usuario, $date);
                    $this->candidato_model->createDetalleEstatusLaboral($nuevo_status, $date, $comentario);
                    $data['estatus'] = $this->candidato_model->checkEstatusLaborales($id_candidato);
                    foreach($data['estatus'] as $ref){
                        $aux = explode('-', $ref->fecha);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                        $salida .= '<div class="row">
                                        <div class="col-md-3">
                                            <p class="text-center"><b>Fecha</b></p>
                                            <p class="text-center">'.$fecha_estatus.'</p>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Comentario / Estatus</label>
                                            <p>'.$ref->comentarios.'</p>
                                        </div>
                                    </div>';
                        $id_verificacion = $ref->idStatus;
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_verificacion
                    );
                }
                else{
                    $this->candidato_model->createDetalleEstatusLaboral($id_verificacion, $date, $comentario);
                    $data['estatus'] = $this->candidato_model->checkEstatusLaborales($id_candidato);
                    foreach($data['estatus'] as $ref){
                        $aux = explode('-', $ref->fecha);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                        $salida .= '<div class="row">
                                        <div class="col-md-3">
                                            <p class="text-center"><b>Fecha</b></p>
                                            <p class="text-center">'.$fecha_estatus.'</p>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Comentario / Estatus</label>
                                            <p>'.$ref->comentarios.'</p>
                                        </div>
                                    </div>';
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_verificacion
                    );
                }
            }
            echo json_encode($msj);
        }
        function finishEstatusLaborales(){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_verificacion = $this->input->post('id_verificacion');
            $id_usuario = $this->session->userdata('id');
            $this->candidato_model->finishEstatusLaborales($id_verificacion, $date, $id_usuario);
            //Calcula el porcentaje de avance
            /*$c = $this->candidato_model->getClienteCandidato($this->input->post('id_candidato'));
             if($c->id_cliente == 1)
                 $this->generarAvancesUST($this->input->post('id_candidato'));*/
        }
        function checkEstatusPenales(){
            $id_candidato = $this->input->post('id_candidato');
            $salida = "";
            $data['estatus'] = $this->candidato_model->checkEstatusPenales($id_candidato);
            if($data['estatus']){
                foreach($data['estatus'] as $p){
                    $aux = explode('-', $p->fecha);
                    $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                    $salida .= '<div class="row">
                                    <div class="col-md-3">
                                        <p class="text-center"><b>Fecha</b></p>
                                        <p class="text-center">'.$fecha_estatus.'</p>
                                    </div>
                                    <div class="col-md-9">
                                        <label>Comentario / Estatus</label>
                                        <p>'.$p->comentarios.'</p>
                                    </div>
                                </div>';
                    $id_status = $p->idStatus;
                    $terminado = $p->finalizado;
                }
                echo $salida.'@@'.$id_status.'@@'.$terminado;
            }
            else{
                echo $salida = 0;
            }
        }
        function createEstatusPenales(){
            $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');
            $this->form_validation->set_message('required','El campo {field} es obligatorio');

            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            } 
            else{
                $id_candidato = $this->input->post('id_candidato');
                $id_verificacion = $this->input->post('id_verificacion');
                $comentario = $this->input->post('comentario');
                $id_usuario = $this->session->userdata('id');
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $salida = "";
                if($id_verificacion == 0){
                    $nuevo_status = $this->candidato_model->createEstatusPenales($id_candidato, $id_usuario, $date);
                    $this->candidato_model->createDetalleEstatusPenales($nuevo_status, $date, $comentario);
                    $data['estatus'] = $this->candidato_model->checkEstatusPenales($id_candidato);
                    foreach($data['estatus'] as $ref){
                        $aux = explode('-', $ref->fecha);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                        $salida .= '<div class="row">
                                        <div class="col-md-3">
                                            <p class="text-center"><b>Fecha</b></p>
                                            <p class="text-center">'.$fecha_estatus.'</p>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Comentario / Estatus</label>
                                            <p>'.$ref->comentarios.'</p>
                                        </div>
                                    </div>';
                        $id_verificacion = $ref->idStatus;
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_verificacion
                    );
                }
                else{
                    $this->candidato_model->createDetalleEstatusPenales($id_verificacion, $date, $comentario);
                    $data['estatus'] = $this->candidato_model->checkEstatusPenales($id_candidato);
                    foreach($data['estatus'] as $ref){
                        $aux = explode('-', $ref->fecha);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0];
                        $salida .= '<div class="row">
                                        <div class="col-md-3">
                                            <p class="text-center"><b>Fecha</b></p>
                                            <p class="text-center">'.$fecha_estatus.'</p>
                                        </div>
                                        <div class="col-md-9">
                                            <label>Comentario / Estatus</label>
                                            <p>'.$ref->comentarios.'</p>
                                        </div>
                                    </div>';
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_verificacion
                    );
                }
            }
            echo json_encode($msj);
        }
        function finishEstatusPenales(){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_verificacion = $_POST['id_verificacion'];
            $id_usuario = $this->session->userdata('id');
            $this->candidato_model->finishEstatusPenales($id_verificacion, $date, $id_usuario);
            //Calcula el porcentaje de avance
            /*$c = $this->candidato_model->getClienteCandidato($this->input->post('id_candidato'));
             if($c->id_cliente == 1)
                $this->generarAvancesUST($this->input->post('id_candidato'));*/
        }
        function getDocumentos(){
            $id_candidato = $this->input->post('id_candidato');
            $data['docs_candidato'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
            $salida = '';
            if($data['docs_candidato']){
                $salida .= '<table class="table table-striped">';
                $salida .= '<thead>';
                $salida .= '<tr>';
                $salida .= '<th width="40%" scope="col">Nombre archivo</th>';
                $salida .= '<th scope="col">Tipo archivo</th>';
                $salida .= '<th scope="col">Acción</th>';
                $salida .= '</tr>';
                $salida .= '</thead>';
                $salida .= '<tbody>';
                foreach($data['docs_candidato'] as $doc){
                    $salida .= '<tr id="fila'.$doc->id.'">';
                    $salida .= '<th><a href="'.base_url().'_docs/'.$doc->archivo.'" target="_blank" style="word-break: break-word;">'.$doc->archivo.'</a></th>';
                    $salida .= '<th>'.$doc->tipo.'</th>';
                    $salida .= '<th><a href="javascript:void(0);"  data-toggle="tooltip" title="Eliminar documento" class="fa-tooltip icono_datatable" onclick="eliminarArchivo('.$doc->id.',\''.$doc->archivo.'\','.$id_candidato.')"><i class="fas fa-trash"></i></a></th>';
                    
                }
                $salida .= '</tr></tbody></table>';
            }
            else{
                $salida = '<table class="table table-striped">';
                $salida .= '<thead>';
                $salida .= '<tr>';
                $salida .= '<th scope="col">Nombre archivo</th>';
                $salida .= '<th scope="col">Tipo archivo</th>';
                $salida .= '</tr>';
                $salida .= '</thead>';
                $salida .= '<tbody>';
                $salida .= '<tr>';
                $salida .= '<th class="text-center" colspan="2">Sin documentos subidos</th>';
                $salida .= '</tr>';
                $salida .= '</tbody>';
                $salida .= '</table>';
            }
            
            echo $salida;
        }
        function cargarDocumento(){
            if (isset($_FILES["documento"]["name"])) {
                $this->form_validation->set_rules('tipo_doc', 'Tipo de archivo', 'required');
                $this->form_validation->set_message('required','El campo {field} es obligatorio');
                if ($this->form_validation->run() == FALSE) {
                    $msj = array(
                        'codigo' => 0,
                        'msg' => validation_errors()
                    );
                    echo json_encode($msj);
                }
                else{
                    $this->form_validation->set_rules('documento', 'Documento', 'callback_file_check');
                    if ($this->form_validation->run() == true) {
                        date_default_timezone_set('America/Mexico_City');
                        $date = date('Y-m-d H:i:s');
                        $id_candidato = $this->input->post('id_candidato');
                        $tipo_doc = $this->input->post('tipo_doc');
                        $prefijo = $this->input->post('prefijo');
                        $prefijo = str_replace(' ','',$prefijo);
                        $salida = "";
                        $archivos = array();
                        $config['upload_path'] = './_docs/';  
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['overwrite'] = TRUE;
                        //$aux2 = str_replace(' ', '', $_FILES['documento']['name']);
                        //$nombre_archivo = str_replace('_', '', $aux2);
                        //Checa si hay mas archivos dle mismo tipo para nombrarlos diferente
                        $cadena = substr(md5(time()), 0, 16);
                        $tipo = $this->candidato_model->getTipoDoc($tipo_doc);
                        $tipoArchivo = str_replace(' ','',$tipo->nombre);
                        $extension = pathinfo($_FILES['documento']['name'], PATHINFO_EXTENSION);
                        $config['file_name'] = $prefijo."_".$tipoArchivo.'_'.$cadena.'.'.$extension;
                        $nombre_archivo = $prefijo."_".$tipoArchivo.'_'.$cadena.'.'.$extension;
                        $documento = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'id_tipo_documento' => $tipo_doc,
                            'archivo' => $nombre_archivo
                        );
                        $this->candidato_model->registrarDocumento($documento);
                        
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('documento')){
                            //$data = $this->upload->data();
                            $salida .= '<table class="table table-striped">';
                            $salida .= '<thead>';
                            $salida .= '<tr>';
                            $salida .= '<th width="40%" scope="col">Nombre archivo</th>';
                            $salida .= '<th scope="col">Tipo archivo</th>';
                            $salida .= '<th scope="col">Acción</th>';
                            $salida .= '</tr>';
                            $salida .= '</thead>';
                            $salida .= '<tbody>';
                            $data['docs_candidato'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
                            foreach($data['docs_candidato'] as $doc){
                                $salida .= '<tr id="fila'.$doc->id.'">';
                                $salida .= '<th><a href="'.base_url().'_docs/'.$doc->archivo.'" target="_blank" style="word-break: break-word;">'.$doc->archivo.'</a></th>';
                                $salida .= '<th>'.$doc->tipo.'</th>';
                                $salida .= '<th><a href="javascript:void(0);"  data-toggle="tooltip" title="Eliminar documento" class="fa-tooltip icono_datatable" onclick="eliminarArchivo('.$doc->id.',\''.$doc->archivo.'\','.$id_candidato.')"><i class="fas fa-trash"></i></a></th>';
                                
                            }
                            $salida .= '</tr></tbody></table>';
                            $msj = array(
                                'codigo' => 1,
                                'msg' => $salida
                            );
                            //Checar porcentaje avance
                            /*$c = $this->candidato_model->getClienteCandidato($id_candidato);
                            if($c->id_cliente == 1){
                                if($tipo_doc == 8 || $tipo_doc == 11){//Si los archivos son aviso de privacidad u OFAC
                                    $this->generarAvancesUST($id_candidato);
                                }
                            }*/
                        }
                        else{
                            $msj = array(
                                'codigo' => 0,
                                'msg' => 'Error al subir el documento'
                            );
                        }
                        echo json_encode($msj);
                    }
                    else{
                        $msj = array(
                            'codigo' => 0,
                            'msg' => validation_errors()
                        );
                        echo json_encode($msj);
                    }
                }
            }
            else{
                $msj = array(
                    'codigo' => 0,
                    'msg' => 'Elija una imagen del documento a subir'
                );
                echo json_encode($msj);
            }
        }
        function eliminarDocumento(){
            $id = $this->input->post('idDoc');
            $archivo = $this->input->post('archivo');
            $id_candidato = $this->input->post('id_candidato');
            $existe = 0;
            if($id !== null && $archivo !== null){
                $aux = directory_map('./_docs/');
                for($i = 0; $i < count($aux); $i++){
                    $indice = explode('_', $aux[$i]);
                    if($indice[0] === $id_candidato){
                        $existe++; 
                        break;
                    }
                }
                if($existe == 1){
                    unlink('./_docs/'.$archivo);
                    $this->candidato_model->eliminarDocCandidato($id);
                    $msj = array(
                        'codigo' => 1,
                        'msg' => 'success'
                    );
                }
                else{
                    $this->candidato_model->eliminarDocCandidato($id);
                    $msj = array(
                        'codigo' => 1,
                        'msg' => 'success'
                    );
                }
            }
            else{
                $msj = array(
                    'codigo' => 0,
                    'msg' => 'error'
                );
            }
            echo json_encode($msj);
        }
        function cancelarCandidato(){
            $id_candidato = $this->input->post('id_candidato');
            $candidato = array(
                'cancelado' => 1
            );
            $this->candidato_model->editarCandidato($candidato, $id_candidato);
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
            echo json_encode($msj);
        }
        function checkAvances(){
            $id_candidato = $_POST['id_candidato'];
            $salida = "";
            $data['avances'] = $this->candidato_model->checkAvances($id_candidato);
            if($data['avances']){
                foreach($data['avances'] as $l){
                    $parte = explode(' ', $l->fecha);
                    $aux = explode('-', $parte[0]);
                    $h = explode(':', $parte[1]);
                    $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                    $imagen = ($l->adjunto != "")? "<a href='".base_url()."_adjuntos/".$l->adjunto."' target='_blank'>Ver imagen</a>" : " Sin adjunto ";
                    $salida .= '<div class="col-md-3">
                                    <p class="text-center"><b>Fecha</b></p>
                                    <p class="text-center">'.$fecha_estatus.'</p>
                                </div>
                                <div class="col-md-5">
                                    <label>Comentario / Estatus</label>
                                    <p>'.$l->comentarios.'</p>
                                </div>
                                <div class="col-md-4">
                                    <label>Imagen adjunto</label>
                                    <p>'.$imagen.'</p>
                                </div>';
                    $id_status = $l->idAvance;
                    $terminado = $l->finalizado;
                }
                echo $salida.'@@'.$id_status.'@@'.$terminado;
            }
            else{
                echo $salida = 0;
            }
        }
        function createEstatusAvance(){
            $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim');
            $this->form_validation->set_message('required','El campo {field} es obligatorio');

            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            } 
            else{
                $id_candidato = $this->input->post('id_candidato');
                $id_avance = $this->input->post('id_avance');
                $comentario = $this->input->post('comentario');
                $id_usuario = $this->session->userdata('id');
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $time = str_replace(':','',$date);
                $time = str_replace(' ','',$time);
                $salida = "";
                $nombre_archivo = "";
                if($id_avance == 0){            
                    $nuevo_avance = $this->candidato_model->createEstatusAvance($id_candidato, $id_usuario, $date);
                    if(isset($_FILES['adjunto']['name'])){
                        $extension = pathinfo($_FILES['adjunto']['name'], PATHINFO_EXTENSION);
                        $nombre_archivo = $nuevo_avance."_".$time."_adjunto.".$extension;
                        $config['upload_path'] = './_adjuntos/';  
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['overwrite'] = TRUE;
                        $config['file_name'] = $nombre_archivo;
        
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        // File upload
                        if($this->upload->do_upload('adjunto')){
                            $data = $this->upload->data();
                        }
                    }
                    $this->candidato_model->createDetalleEstatusAvance($nuevo_avance, $date, $comentario, $nombre_archivo);
                    $data['avances'] = $this->candidato_model->checkAvances($id_candidato);
                    foreach($data['avances'] as $av){
                        $parte = explode(' ', $av->fecha);
                        $aux = explode('-', $parte[0]);
                        $h = explode(':', $parte[1]);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                        $imagen = ($av->adjunto != "")? "<a href='".base_url()."_adjuntos/".$av->adjunto."' target='_blank'>Ver imagen</a>" : " Sin adjunto ";
                        $salida .= '<div class="col-md-3">
                                        <p class="text-center"><b>Fecha</b></p>
                                        <p class="text-center">'.$fecha_estatus.'</p>
                                    </div>
                                    <div class="col-md-5">
                                        <label>Comentario / Estatus</label>
                                        <p>'.$av->comentarios.'</p>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Imagen adjunto</label>
                                        <p>'.$imagen.'</p>
                                    </div>';
                        $id_avance = $av->idAvance;
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_avance
                    );
                }
                else{
                    if(isset($_FILES['adjunto']['name'])){
                        $extension = pathinfo($_FILES['adjunto']['name'], PATHINFO_EXTENSION);
                        $nombre_archivo = $id_avance."_".$time."_adjunto.".$extension;
                        $config['upload_path'] = './_adjuntos/';  
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['overwrite'] = TRUE;
                        $config['file_name'] = $nombre_archivo;
        
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        // File upload
                        if($this->upload->do_upload('adjunto')){
                            $data = $this->upload->data();
                        }
                    }
                    $this->candidato_model->createDetalleEstatusAvance($id_avance, $date, $comentario, $nombre_archivo);
                    $data['avances'] = $this->candidato_model->checkAvances($id_candidato);
                    foreach($data['avances'] as $av){
                        $parte = explode(' ', $av->fecha);
                        $aux = explode('-', $parte[0]);
                        $h = explode(':', $parte[1]);
                        $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                        $imagen = ($av->adjunto != "")?  "<a href='".base_url()."_adjuntos/".$av->adjunto."' target='_blank'>Ver imagen</a>" : " Sin adjunto ";
                        $salida .= '<div class="col-md-3">
                                        <p class="text-center"><b>Fecha</b></p>
                                        <p class="text-center">'.$fecha_estatus.'</p>
                                    </div>
                                    <div class="col-md-5">
                                        <label>Comentario / Estatus</label>
                                        <p>'.$av->comentarios.'</p>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Imagen adjunto</label>
                                        <p>'.$imagen.'</p>
                                    </div>';
                    }
                    $msj = array(
                        'codigo' => 1,
                        'msg' => $salida.'@@'.$id_avance
                    );
                }
            }
            echo json_encode($msj);
        }
        function verComentarioCandidato(){
            $id_candidato = $_POST['id_candidato'];
            $res = $this->candidato_model->getComentario($id_candidato);
            if($res != "" && $res != null){
                echo $res->comentario;
            }
            else{
                echo $res = 0;
            }        
        }
        function checkDocumentos(){
            $id_candidato = $_POST['id_candidato'];
            $salida = "";
            $data['documents'] = $this->candidato_model->checkDocumentos($id_candidato);
            if(isset($data['documents'])){
                foreach($data['documents'] as $d){
                    $salida .= $d->id_tipo_documento.",".$d->archivo."@@";
                }
                echo $salida;
            }
            else{
                $salida = 0;
            }
        }
        function reasignarCandidatoAnalista(){
            $id_candidato = $this->input->post('id_candidato');
            $id_usuario = $this->input->post('id_usuario');
    
            $datos = array(
                'id_usuario' => $id_usuario
            );
            $this->candidato_model->editarCandidato($datos, $id_candidato);
            $msj = array(
                'codigo' => 1,
                'msg' => 'Success'
            );
            echo json_encode($msj);
        }
        function registrarInfoVisitador(){
            $this->form_validation->set_rules('fecha_acta', 'Fecha de expedición del acta de nacimiento', 'required|trim');
            $this->form_validation->set_rules('numero_acta', 'Número y/o vigencia del acta de nacimiento', 'required|trim');
            $this->form_validation->set_rules('fecha_domicilio', 'Fecha de expedición del comprobante del domicilio', 'required|trim');
            $this->form_validation->set_rules('numero_domicilio', 'Número y/o vigencia del comprobante del domicilio', 'required|trim');
            $this->form_validation->set_rules('fecha_curp', 'Fecha de expedición del CURP', 'required|trim');
            $this->form_validation->set_rules('numero_curp', 'Número y/o vigencia del CURP', 'required|trim');
            $this->form_validation->set_rules('fecha_rfc', 'Fecha de expedición del RFC', 'required|trim');
            $this->form_validation->set_rules('numero_rfc', 'Número y/o vigencia del RFC', 'required|trim');
            $this->form_validation->set_rules('candidato_ingresos', 'Ingresos del candidato', 'required|trim');
            $this->form_validation->set_rules('candidato_muebles', 'Muebles e inmuebles del candidato', 'required|trim');
            $this->form_validation->set_rules('candidato_adeudo', 'Adeudo', 'required|trim');
            $this->form_validation->set_rules('notas', 'Notas', 'required|trim');
            $this->form_validation->set_rules('renta', 'Renta', 'required|trim|numeric');
            $this->form_validation->set_rules('alimentos', 'Alimentos', 'required|trim|numeric');
            $this->form_validation->set_rules('servicios', 'Servicios', 'required|trim|numeric');
            $this->form_validation->set_rules('transportes', 'Transportes', 'required|trim|numeric');
            $this->form_validation->set_rules('otros_gastos', 'Otros gastos', 'required|trim|numeric');
            $this->form_validation->set_rules('solvencia', '¿Cuando los egresos son mayores a los ingresos, ¿cómo los solventa?', 'required|trim');
            $this->form_validation->set_rules('tiempo_residencia', 'Tiempo de residencia en el domicilio actual', 'required|trim');
            $this->form_validation->set_rules('nivel_zona', 'Nivel de la zona', 'required|trim');
            $this->form_validation->set_rules('tipo_vivienda', 'Tipo de vivienda', 'required|trim');
            $this->form_validation->set_rules('recamaras', 'Recámaras', 'required|trim|numeric');
            $this->form_validation->set_rules('banios', 'Baños', 'required|trim');
            $this->form_validation->set_rules('distribucion', 'Distribución', 'required|trim');
            $this->form_validation->set_rules('calidad_mobiliario', 'Calidad mobiliario', 'required|trim');
            $this->form_validation->set_rules('mobiliario', 'Mobiliario', 'required|trim');
            $this->form_validation->set_rules('tamanio_vivienda', 'Tamaño vivienda', 'required|trim');
            $this->form_validation->set_rules('condiciones_vivienda', 'Condiciones de la vivienda', 'required|trim');
            
            $this->form_validation->set_message('required','El campo {field} es obligatorio');
            $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
            $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');
            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            }
            else{
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $id_candidato = $this->input->post('id_candidato');
                $id_usuario = $this->session->userdata('id');
                if($this->input->post('personas') != "" && $this->input->post('personas') != null){
                    $data_personas = "";
                    $h = explode("@@", $this->input->post('personas'));
                    for($i = 0; $i < (count($h) - 1); $i++){
                        $aux = explode(",,", $h[$i]);
                        $data_personas = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'nombre' => urldecode($aux[0]),
                            'id_tipo_parentesco' => $aux[1],
                            'edad' => $aux[2],
                            'id_grado_estudio' => $aux[3],
                            'misma_vivienda' => $aux[4],
                            'id_estado_civil' => $aux[5],
                            'empresa' => urldecode($aux[6]),
                            'puesto' => urldecode($aux[7]),
                            'antiguedad' => urldecode(($aux[8])),
                            'sueldo' => $aux[9],
                            'monto_aporta' => $aux[10],
                            'muebles' => urldecode($aux[11]),
                            'adeudo' => $aux[12]
                        );
                        $this->candidato_model->guardarFamiliar($data_personas); 
                    }
                }
                $candidato = array(
                    'edicion' => $date,
                    'muebles' => $this->input->post('candidato_muebles'),
                    'adeudo_muebles' => $this->input->post('candidato_adeudo'),
                    'ingresos' => $this->input->post('candidato_ingresos'),
                    'comentario' => $this->input->post('notas'),
                    'visitador' => 1
                );
                $this->candidato_model->editarCandidato($candidato, $id_candidato);

                $egresos = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_candidato' => $id_candidato,
                    'renta' => $this->input->post('renta'),
                    'alimentos' => $this->input->post('alimentos'),
                    'servicios' => $this->input->post('servicios'),
                    'transporte' => $this->input->post('transportes'),
                    'otros' => $this->input->post('otros_gastos'),
                    'solvencia' => $this->input->post('solvencia')
                );
                $this->candidato_model->guardarEgresos($egresos);

                $habitacion = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_candidato' => $id_candidato,
                    'tiempo_residencia' => $this->input->post('tiempo_residencia'),
                    'id_tipo_nivel_zona' => $this->input->post('nivel_zona'),
                    'id_tipo_vivienda' => $this->input->post('tipo_vivienda'),
                    'recamaras' => $this->input->post('recamaras'),
                    'banios' => $this->input->post('banios'),
                    'distribucion' => $this->input->post('distribucion'),
                    'calidad_mobiliario' => $this->input->post('calidad_mobiliario'),
                    'mobiliario' => $this->input->post('mobiliario'),
                    'tamanio_vivienda' => $this->input->post('tamanio_vivienda'),
                    'id_tipo_condiciones' => $this->input->post('condiciones_vivienda')
                );
                $this->candidato_model->guardarHabitacion($habitacion);

                for($i = 1; $i <= 3; $i++){
                    if($this->input->post('vecino'.$i.'_nombre') != ''){
                        $vecino = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_usuario' => $id_usuario,
                            'id_candidato' => $id_candidato,
                            'nombre' => $this->input->post('vecino'.$i.'_nombre'),
                            'telefono' => $this->input->post('vecino'.$i.'_tel'),
                            'domicilio' => $this->input->post('vecino'.$i.'_domicilio'),
                            'concepto_candidato' => $this->input->post('vecino'.$i.'_concepto'),
                            'concepto_familia' => $this->input->post('vecino'.$i.'_familia'),
                            'civil_candidato' => $this->input->post('vecino'.$i.'_civil'),
                            'hijos_candidato' => $this->input->post('vecino'.$i.'_hijos'),
                            'sabe_trabaja' => $this->input->post('vecino'.$i.'_sabetrabaja'),
                            'notas' => $this->input->post('vecino'.$i.'_notas')
                        );
                        $this->candidato_model->guardarReferenciaVecinal($vecino);
                    }
                }

                $f_acta = ($this->input->post('fecha_acta') !== null)? fecha_espanol_bd($this->input->post('fecha_acta')) : "";
                $f_domicilio = ($this->input->post('fecha_domicilio') !== null)? fecha_espanol_bd($this->input->post('fecha_domicilio')) : "";
                $f_curp = ($this->input->post('fecha_curp') !== null)? fecha_espanol_bd($this->input->post('fecha_curp'))  :"";
                $f_imss = ($this->input->post('fecha_imss') !== null)? fecha_espanol_bd($this->input->post('fecha_imss')) : "";
                $f_retencion = ($this->input->post('fecha_retencion') !== null)? fecha_espanol_bd($this->input->post('fecha_retencion')) : "";
                $f_rfc = ($this->input->post('fecha_rfc') !== null)? fecha_espanol_bd($this->input->post('fecha_rfc')) : "";
                $f_licencia = ($this->input->post('fecha_licencia') !== null)? fecha_espanol_bd($this->input->post('fecha_licencia')) : "";
                $f_migra = ($this->input->post('fecha_migra') !== null)? fecha_espanol_bd($this->input->post('fecha_migra')) : "";
                $f_visa = ($this->input->post('fecha_visa') !== null)? fecha_espanol_bd($this->input->post('fecha_visa')) : "";
                $documentacion = array(
                    'edicion' => $date,
                    'fecha_acta' => $f_acta,
                    'acta' => $this->input->post('numero_acta'),
                    'fecha_domicilio' => $f_domicilio,
                    'cuenta_domicilio' => $this->input->post('numero_domicilio'),
                    'emision_ine' => $this->input->post('fecha_ine'),
                    'ine' => $this->input->post('numero_ine'),
                    'emision_curp' => $f_curp,
                    'curp' => $this->input->post('numero_curp'),
                    'emision_nss' => $f_imss,
                    'nss' => $this->input->post('numero_imss'),
                    'fecha_retencion_impuestos' => $f_retencion,
                    'retencion_impuestos' => $this->input->post('numero_retencion'),
                    'emision_rfc' => $f_rfc,
                    'rfc' => $this->input->post('numero_rfc'),
                    'fecha_licencia' => $f_licencia,
                    'licencia' => $this->input->post('numero_licencia'),
                    'vigencia_migratoria' => $f_migra,
                    'numero_migratorio' => $this->input->post('numero_migra'),
                    'fecha_visa' => $f_visa,
                    'visa' => $this->input->post('numero_visa')
                );
                $this->candidato_model->editarCandidato($documentacion, $id_candidato);

                $msj = array(
                    'codigo' => 1,
                    'msg' => 'success'
                );
            } 
            echo json_encode($msj);
        }
        function registrarInfoVisitadorAlterno(){
            $this->form_validation->set_rules('imss1', 'Número de documento del Número de Seguridad Social', 'required|trim');
            $this->form_validation->set_rules('imss2', 'Dato / Institución del Número de Seguridad Social', 'required|trim');
            $this->form_validation->set_rules('dom1', 'Número de documento del comprobante del domicilio', 'required|trim');
            $this->form_validation->set_rules('dom2', 'Dato / Institución del comprobante del domicilio', 'required|trim');
            $this->form_validation->set_rules('ine1', 'Número de documento del INE (IFE o pasaporte)', 'required|trim');
            $this->form_validation->set_rules('ine2', 'Dato / Institución del INE (IFE o pasaporte)', 'required|trim');
            $this->form_validation->set_rules('lic1', 'Número de documento de la Licencia para conducir', 'required|trim');
            $this->form_validation->set_rules('lic2', 'Dato / Institución de la Licencia para conducir', 'required|trim');
            $this->form_validation->set_rules('carta1', 'Número de documento de las Cartas de recomendación', 'required|trim');
            $this->form_validation->set_rules('carta2', 'Dato / Institución de las Cartas de recomendación', 'required|trim');
            $this->form_validation->set_rules('curp1', 'Número de documento del CURP', 'required|trim');
            $this->form_validation->set_rules('curp2', 'Dato / Institución del CURP', 'required|trim');
            $this->form_validation->set_rules('rfc1', 'Número de documento del RFC', 'required|trim');
            $this->form_validation->set_rules('rfc2', 'Dato / Institución del RFC', 'required|trim');
            $this->form_validation->set_rules('candidato_ingresos', 'Ingresos del candidato', 'required|trim');
            $this->form_validation->set_rules('candidato_muebles', 'Muebles e inmuebles del candidato', 'required|trim');
            $this->form_validation->set_rules('candidato_adeudo', 'Adeudo', 'required|trim');
            $this->form_validation->set_rules('candidato_aporte', 'Aporte del candidato', 'required|trim');
            $this->form_validation->set_rules('notas', 'Notas', 'required|trim');
            $this->form_validation->set_rules('renta', 'Renta', 'required|trim|numeric');
            $this->form_validation->set_rules('alimentos', 'Alimentos', 'required|trim|numeric');
            $this->form_validation->set_rules('servicios', 'Servicios', 'required|trim|numeric');
            $this->form_validation->set_rules('transportes', 'Transportes', 'required|trim|numeric');
            $this->form_validation->set_rules('otros_gastos', 'Otros gastos', 'required|trim|numeric');
            $this->form_validation->set_rules('solvencia', '¿Cuando los egresos son mayores a los ingresos, ¿cómo los solventa?', 'required|trim');
            $this->form_validation->set_rules('tiempo_residencia', 'Tiempo de residencia en el domicilio actual', 'required|trim');
            $this->form_validation->set_rules('nivel_zona', 'Nivel de la zona', 'required|trim');
            $this->form_validation->set_rules('tipo_vivienda', 'Tipo de vivienda', 'required|trim');
            $this->form_validation->set_rules('recamaras', 'Recámaras', 'required|trim|numeric');
            $this->form_validation->set_rules('banios', 'Baños', 'required|trim');
            $this->form_validation->set_rules('distribucion', 'Distribución', 'required|trim');
            $this->form_validation->set_rules('calidad_mobiliario', 'Calidad mobiliario', 'required|trim');
            $this->form_validation->set_rules('mobiliario', 'Mobiliario', 'required|trim');
            $this->form_validation->set_rules('tamanio_vivienda', 'Tamaño vivienda', 'required|trim');
            $this->form_validation->set_rules('condiciones_vivienda', 'Condiciones de la vivienda', 'required|trim');
            $this->form_validation->set_rules('comentario_visitador', 'Comentarios del visitador', 'required|trim');
            
            $this->form_validation->set_message('required','El campo {field} es obligatorio');
            $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
            $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');
            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            }
            else{
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $id_candidato = $this->input->post('id_candidato');
                $id_usuario = $this->session->userdata('id');

                if($this->input->post('personas') != "" && $this->input->post('personas') != null){
                    $data_personas = "";
                    $h = explode("@@", $this->input->post('personas'));
                    for($i = 0; $i < (count($h) - 1); $i++){
                        $aux = explode(",,", $h[$i]);
                        $data_personas = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'nombre' => urldecode($aux[0]),
                            'id_tipo_parentesco' => $aux[1],
                            'edad' => $aux[2],
                            'id_grado_estudio' => $aux[3],
                            'misma_vivienda' => $aux[4],
                            'id_estado_civil' => $aux[5],
                            'empresa' => urldecode($aux[6]),
                            'puesto' => urldecode($aux[7]),
                            'antiguedad' => urldecode(($aux[8])),
                            'sueldo' => $aux[9],
                            'monto_aporta' => $aux[10],
                            'muebles' => urldecode($aux[11]),
                            'adeudo' => $aux[12]
                        );
                        $this->candidato_model->guardarFamiliar($data_personas); 
                    }
                }
                $candidato = array(
                    'edicion' => $date,
                    'muebles' => $this->input->post('candidato_muebles'),
                    'adeudo_muebles' => $this->input->post('candidato_adeudo'),
                    'ingresos' => $this->input->post('candidato_ingresos'),
                    'aporte' => $this->input->post('candidato_aporte'),
                    'comentario' => $this->input->post('notas'),
                    'visitador' => 1
                );
                $this->candidato_model->editarCandidato($candidato, $id_candidato);

                $egresos = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_candidato' => $id_candidato,
                    'renta' => $this->input->post('renta'),
                    'alimentos' => $this->input->post('alimentos'),
                    'servicios' => $this->input->post('servicios'),
                    'transporte' => $this->input->post('transportes'),
                    'otros' => $this->input->post('otros_gastos'),
                    'solvencia' => $this->input->post('solvencia')
                );
                $this->candidato_model->guardarEgresos($egresos);

                $habitacion = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_candidato' => $id_candidato,
                    'tiempo_residencia' => $this->input->post('tiempo_residencia'),
                    'id_tipo_nivel_zona' => $this->input->post('nivel_zona'),
                    'id_tipo_vivienda' => $this->input->post('tipo_vivienda'),
                    'recamaras' => $this->input->post('recamaras'),
                    'banios' => $this->input->post('banios'),
                    'distribucion' => $this->input->post('distribucion'),
                    'calidad_mobiliario' => $this->input->post('calidad_mobiliario'),
                    'mobiliario' => $this->input->post('mobiliario'),
                    'tamanio_vivienda' => $this->input->post('tamanio_vivienda'),
                    'id_tipo_condiciones' => $this->input->post('condiciones_vivienda')
                );
                $this->candidato_model->guardarHabitacion($habitacion);

                if($this->input->post('mismo_trabajo_nombre1') != "" && $this->input->post('mismo_trabajo_puesto1') != ""){
                    $persona1 = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario' => $id_usuario,
                        'id_candidato' => $id_candidato,
                        'nombre' => $this->input->post('mismo_trabajo_nombre1'),
                        'puesto' => $this->input->post('mismo_trabajo_puesto1')
                    );
                    $this->candidato_model->guardarPersonaMismoTrabajo($persona1);
                }
                if($this->input->post('mismo_trabajo_nombre2') != "" && $this->input->post('mismo_trabajo_puesto2') != ""){
                    $persona2 = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario' => $id_usuario,
                        'id_candidato' => $id_candidato,
                        'nombre' => $this->input->post('mismo_trabajo_nombre2'),
                        'puesto' => $this->input->post('mismo_trabajo_puesto2')
                    );
                    $this->candidato_model->guardarPersonaMismoTrabajo($persona2);
                }

                if($this->input->post('vecinal_nombre') != '' && $this->input->post('vecinal_domicilio') != ''){
                    $vecino = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario' => $id_usuario,
                        'id_candidato' => $id_candidato,
                        'nombre' => $this->input->post('vecinal_nombre'),
                        'telefono' => $this->input->post('vecinal_tel'),
                        'domicilio' => $this->input->post('vecinal_domicilio'),
                        'concepto_candidato' => $this->input->post('vecinal_concepto'),
                        'concepto_familia' => $this->input->post('vecinal_familia'),
                        'civil_candidato' => $this->input->post('vecinal_civil'),
                        'hijos_candidato' => $this->input->post('vecinal_hijos'),
                        'sabe_trabaja' => $this->input->post('vecinal_sabetrabaja'),
                        'notas' => $this->input->post('vecinal_notas')
                    );
                    $this->candidato_model->guardarReferenciaVecinal($vecino);
                }

                $documentacion = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_candidato' => $id_candidato,
                    'licencia' => $this->input->post('lic1'),
                    'licencia_institucion' => $this->input->post('lic2'),
                    'ine' => $this->input->post('ine1'),
                    'ine_institucion' => $this->input->post('ine2'),
                    'domicilio' => $this->input->post('dom1'),
                    'fecha_domicilio' => $this->input->post('dom2'),
                    'imss' => $this->input->post('imss1'),
                    'imss_institucion' => $this->input->post('imss2'),
                    'rfc' => $this->input->post('rfc1'),
                    'rfc_institucion' => $this->input->post('rfc2'),
                    'curp' => $this->input->post('curp1'),
                    'curp_institucion' => $this->input->post('curp2'),
                    'carta_recomendacion' => $this->input->post('carta1'),
                    'carta_recomendacion_institucion' => $this->input->post('carta2'),
                    'comentarios' => $this->input->post('comentarios_documentos')
                );
                $this->candidato_model->eliminarVerificacionDocumentacion($id_candidato);
                $this->candidato_model->guardarVerificacionDocumento($documentacion);

                $visita = array(
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'comentarios' => $this->input->post('comentario_visitador')
                );
                $this->candidato_model->editarDatosVisita($visita, $id_candidato);

                $msj = array(
                    'codigo' => 1,
                    'msg' => 'success'
                );
            }
            echo json_encode($msj);
        }
        function subirPsicometrico(){
            if (isset($_FILES["archivo"]["name"])) {
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $id_candidato = $this->input->post('id_candidato');
                $id_psicometrico = $this->input->post('id_psicometrico');
                $id_usuario = $this->session->userdata('id');
                $extension = pathinfo($_FILES['archivo']['name'], PATHINFO_EXTENSION);
                $nombre_archivo = $id_candidato."_psicometrico.".$extension;

                $config['upload_path'] = './_psicometria/';  
                $config['allowed_types'] = 'pdf';
                $config['overwrite'] = TRUE;
                $config['file_name'] = $nombre_archivo;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('archivo')){
                    $data = $this->upload->data();
                    $this->candidato_model->eliminarPsicometrico($id_candidato);
                    $doc = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario' => $id_usuario,
                        'id_candidato' => $id_candidato,
                        'archivo' => $nombre_archivo
                    );
                    $this->candidato_model->guardarPsicometrico($doc);
                    $msj = array(
                        'codigo' => 1,
                        'msg' => 'Success'
                    );
                }
                else{
                    $msj = array(
                        'codigo' => 2,
                        'msg' => 'Error al subir el archivo'
                    );
                }
                echo json_encode($msj);
            }
            else{
                $msj = array(
                    'codigo' => 0,
                    'msg' => 'Elija el archivo a subir'
                );
                echo json_encode($msj);
            }
        }
        function getDocumentacionCandidato(){
            $opcion = $this->input->post('opcion');
            $id_candidato = $this->input->post('id_candidato');
            $salida = '';
            if($opcion == 1){
                $doc = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
                if($doc != null){
                    $f_acta = ($doc->fecha_acta != "0000-00-00" && $doc->fecha_acta != null)? fecha_espanol_frontend($doc->fecha_acta):'';
                    $f_domicilio = ($doc->fecha_domicilio != "0000-00-00" && $doc->fecha_domicilio != null)? fecha_espanol_frontend($doc->fecha_domicilio):'';
                    $f_curp = ($doc->emision_curp != "0000-00-00" && $doc->emision_curp != null)? fecha_espanol_frontend($doc->emision_curp):'';
                    $f_nss = ($doc->emision_nss != '0000-00-00' && $doc->emision_nss != null)? fecha_espanol_frontend($doc->emision_nss):'';
                    $f_retencion = ($doc->fecha_retencion_impuestos != '0000-00-00' && $doc->fecha_retencion_impuestos != null)? fecha_espanol_frontend($doc->fecha_retencion_impuestos):'';
                    $f_rfc = ($doc->emision_rfc != '0000-00-00' && $doc->emision_rfc != null)? fecha_espanol_frontend($doc->emision_rfc):'';
                    $f_licencia = ($doc->fecha_licencia != '0000-00-00' && $doc->fecha_licencia != null)? fecha_espanol_frontend($doc->fecha_licencia):'';
                    $f_migra = ($doc->vigencia_migratoria != '0000-00-00' && $doc->vigencia_migratoria != null)? fecha_espanol_frontend($doc->vigencia_migratoria):'';
                    $f_visa = ($doc->fecha_visa != '0000-00-00' && $doc->fecha_visa != null)? fecha_espanol_frontend($doc->fecha_visa):'';
                    $salida .= $f_acta.'@@';
                    $salida .= $doc->acta.'@@';
                    $salida .= $f_domicilio.'@@';
                    $salida .= $doc->cuenta_domicilio.'@@';
                    $salida .= $doc->emision_ine.'@@';
                    $salida .= $doc->ine.'@@';
                    $salida .= $f_curp.'@@';
                    $salida .= $doc->curp.'@@';
                    $salida .= $f_nss.'@@';
                    $salida .= $doc->nss.'@@';
                    $salida .= $f_retencion.'@@';
                    $salida .= $doc->retencion_impuestos.'@@';
                    $salida .= $f_rfc.'@@';
                    $salida .= $doc->rfc.'@@';
                    $salida .= $f_licencia.'@@';
                    $salida .= $doc->licencia.'@@';
                    $salida .= $f_migra.'@@';
                    $salida .= $doc->numero_migratorio.'@@';
                    $salida .= $f_visa.'@@';
                    $salida .= $doc->visa;
    
                    echo $salida;
                }
                else{
                    echo $salida = 0;
                }
            }
            if($opcion == 2){
                $data['docs'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
                if($data['docs']){
                    foreach($data['docs'] as $doc){
                        $salida .= $doc->licencia.'@@';
                        $salida .= $doc->licencia_institucion.'@@';
                        $salida .= $doc->ine.'@@';
                        $salida .= $doc->ine_institucion.'@@';
                        $salida .= $doc->domicilio.'@@';
                        $salida .= $doc->fecha_domicilio.'@@';
                        $salida .= $doc->imss.'@@';
                        $salida .= $doc->imss_institucion.'@@';
                        $salida .= $doc->rfc.'@@';
                        $salida .= $doc->rfc_institucion.'@@';
                        $salida .= $doc->curp.'@@';
                        $salida .= $doc->curp_institucion.'@@';
                        $salida .= $doc->carta_recomendacion.'@@';
                        $salida .= $doc->carta_recomendacion_institucion.'@@';
                        $salida .= $doc->comentarios;
                    }
                    echo $salida;
                }
                else{
                    echo $salida = 0;
                }
            }
        }
        function liberarProceso(){
            $id_candidato = $this->input->post('id_candidato');
            $accion = $this->input->post('accion');
            if($accion == 1){
                $datos = array(
                    'liberado' => 1
                );
                $this->candidato_model->editarCandidato($datos, $id_candidato);
                $msj = array(
                'codigo' => 1,
                'msg' => 'Se ha liberado correctamente'
                );
            }
            if($accion == 0){
                $datos = array(
                    'liberado' => 0
                );
                $this->candidato_model->editarCandidato($datos, $id_candidato);
                $msj = array(
                'codigo' => 1,
                'msg' => 'Se ha detenido correctamente'
                );
            }
            echo json_encode($msj);
        }
        function getPaqueteSubclienteProyecto(){
            $id_cliente = 2;
            $id_proyecto = $_POST['id_proyecto'];
            $data['paquetes'] = $this->cliente_model->getPaqueteSubclienteProyecto($id_cliente, $id_proyecto);
            $salida = "<option value=''>Select</option>";
            if($data['paquetes']){
                foreach ($data['paquetes'] as $row){
                    $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
                } 
                $salida .= "<option value='0'>N/A</option>";
                echo $salida;
            }
            else{
                $salida .= " <option value='0'>N/A</option>";
                echo $salida;
            }
        }
        function getPaqueteProyecto(){
            $id_cliente = $_POST['id_cliente'];
            $id_proyecto = $_POST['id_proyecto'];
            $data['paquetes'] = $this->cliente_model->getPaqueteSubclienteProyecto($id_cliente, $id_proyecto);
            $salida = "<option value=''>Select</option>";
            if($data['paquetes']){
                foreach ($data['paquetes'] as $row){
                    $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
                } 
                $salida .= " <option value='0'>N/A</option>";
                echo $salida;
            }
            else{
                $salida .= " <option value='0'>N/A</option>";
                echo $salida;
            }
        }
        function checkGaps(){
            $id_candidato = $this->input->post('id_candidato');
            $salida = "";
            $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
            if($data['gaps']){
                foreach($data['gaps'] as $gap){
                    $salida .= '<div class="col-3">
                                    <p class="text-center"><b>Del</b></p>
                                    <p class="text-center">'.$gap->fecha_inicio.'</p>
                                </div>
                                <div class="col-3">
                                    <p class="text-center"><b>Al</b></p>
                                    <p class="text-center">'.$gap->fecha_fin.'</p>
                                </div>
                                <div class="col-6">
                                    <label>Razón</label>
                                    <p>'.$gap->razon.'</p>
                                </div>';
                }
                echo $salida;
            }
            else{
                echo $salida = 0;
            }
        }
        function createGap(){
            $this->form_validation->set_rules('fi', 'Fecha inicio', 'required|trim');
            $this->form_validation->set_rules('ff', 'Fecha fin', 'required|trim');
            $this->form_validation->set_rules('razon', 'Razón', 'required|trim');
            $this->form_validation->set_message('required','El campo {field} es obligatorio');

            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            } 
            else{
                $id_candidato = $this->input->post('id_candidato');
                $fi = $this->input->post('fi');
                $ff = $this->input->post('ff');
                $razon = $this->input->post('razon');
                $id_usuario = $this->session->userdata('id');
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $salida = "";
                $this->candidato_model->createGap($id_candidato, $id_usuario, $date, $fi, $ff, $razon);
                $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
                foreach($data['gaps'] as $gap){
                    $salida .= '<div class="col-3">
                                    <p class="text-center"><b>Del</b></p>
                                    <p class="text-center">'.$gap->fecha_inicio.'</p>
                                </div>
                                <div class="col-3">
                                    <p class="text-center"><b>Al</b></p>
                                    <p class="text-center">'.$gap->fecha_fin.'</p>
                                </div>
                                <div class="col-6">
                                    <label>Razón</label>
                                    <p>'.$gap->razon.'</p>
                                </div>';
                }
                $msj = array(
                    'codigo' => 1,
                    'msg' => $salida
                );
            }
            echo json_encode($msj);
        }
        function guardarVerificacionChecklist(){
            $this->form_validation->set_rules('check_education', 'Educación', 'required|trim');
            $this->form_validation->set_rules('check_employment', 'Empleos', 'required|trim');
            $this->form_validation->set_rules('check_address', 'Domicilios', 'required|trim');
            $this->form_validation->set_rules('check_criminal', 'Criminal', 'required|trim');
            $this->form_validation->set_rules('check_database', 'Global database', 'required|trim');
            $this->form_validation->set_rules('check_identity', 'Identidad', 'required|trim');
            $this->form_validation->set_rules('check_military', 'Servicio militar', 'required|trim');
            $this->form_validation->set_rules('check_other', 'Otras verificaciones', 'required|trim');

            $this->form_validation->set_message('required','El campo {field} es obligatorio');

            $msj = array();
            if ($this->form_validation->run() == FALSE) {
                $msj = array(
                    'codigo' => 0,
                    'msg' => validation_errors()
                );
            } 
            else{
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $id_candidato = $this->input->post('id_candidato');
                $id_usuario = $this->session->userdata('id');
                $verificacion = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_candidato' => $id_candidato,
                    'education' => $this->input->post('check_education'),
                    'employment' => $this->input->post('check_employment'),
                    'address' => $this->input->post('check_address'),
                    'criminal' => $this->input->post('check_criminal'),
                    'global_database' => $this->input->post('check_database'),
                    'identity' => $this->input->post('check_identity'),
                    'military' => $this->input->post('check_military'),
                    'other' => $this->input->post('check_other')
                );
                $this->candidato_model->eliminarChecklist($id_candidato);
                $this->candidato_model->guardarChecklist($verificacion);
                $msj = array(
                    'codigo' => 1,
                    'msg' => 'success'
                );
            }
            echo json_encode($msj);
        }
    /*----------------------------------------*/
	/*  Creacion de Porcentaje de Avances
	/*----------------------------------------*/
        function generarAvancesUST($id_candidato){
            $c = $this->cliente_ust_model->getSeccionesRequeridas($id_candidato);
            $porcentaje = 0;
            if($c->estudios_comentarios != '' && $c->estudios_comentarios != null){//Comentarios historial academico
            $porcentaje += 10;
            }
            if($c->docs_comentarios != '' && $c->docs_comentarios != null){//Comentarios verificacion documentos
            $porcentaje += 10;
            }
            if($c->idRefPer != null){//minimo un id de candidato_ref_personal
            $porcentaje += 20;
            }
            if($c->idVerLaboral != null){//minimo un id de verificacion_ref_laboral
            $porcentaje += 20;
            }
            if($c->idVerEstudios != null){//minimo un id de verificacion de estudios
            $porcentaje += 10;
            }
            if($c->idEstatusLaboral != null){//minimo un id de verificacion laboral
            $porcentaje += 10;
            }
            if($c->idVerPenal != null){//minimo un id de verificacion penal
            $porcentaje += 10;
            }
            //Se checa el porcentaje en caso de que tenga los documentos obligatorios, los demas puede que no se tengan por alguna razon
            $data['docs'] = $this->cronjobs_model->getDocumentosObligatoriosUST($id_candidato);
            if($data['docs']){
                $aviso = 0; $ofac = 0;
                foreach($data['docs'] as $doc){
                    if($doc->id_tipo_documento == 8){ // Si tiene cargado el aviso de privacidad
                        $aviso = 1;
                    }
                    if($doc->id_tipo_documento == 11){ //Si tiene cargado el OFAC
                        $ofac = 1;
                    }
                }
                $p = ($aviso == 1)? 5:0;
                $porcentaje += $p;
                $p2 = ($ofac == 1)? 5:0;
                $porcentaje += $p2;
            }
            $this->cronjobs_model->cleanAvance($id_candidato);
            $this->cronjobs_model->actualizarAvance($porcentaje, $id_candidato);
        }
    
    
    /*----------------------------------------*/
    /* Panel Subclientes
    /*----------------------------------------*/
        function viewAvances(){
            $id_candidato = $this->input->post('id_candidato');
            $txt_fecha = ($this->input->post('espanol') == 1)? 'Fecha: ':'Date: ';
            $txt_comentario = ($this->input->post('espanol') == 1)? 'Comentario: ':'Comment: ';
            $txt_imagen = ($this->input->post('espanol') == 1)? 'Ver imagen: ':'View file';
            $txt_sin_registros = ($this->input->post('espanol') == 1)? 'Sin registro de avances: ':'No registers';
            $salida = '<div class="row">';
            $salida .= '<div class="col-md-12">';
            $data['avances'] = $this->candidato_model->checkAvances($id_candidato);
            if($data['avances']){
                foreach($data['avances'] as $row){
                    $parte = explode(' ', $row->fecha);
                    $aux = explode('-', $parte[0]);
                    $h = explode(':', $parte[1]);
                    $fecha_espanol = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                    $salida .= '<p style="padding-right: 5px;"><b>'.$txt_fecha.'</b> '.$fecha_espanol.'</p><p><b>'.$txt_comentario.'</b> '.$row->comentarios.'</p>';
                    $salida .= ($row->adjunto != "")? "<a href='".base_url()."_adjuntos/".$row->adjunto."' target='_blank' style='margin-bottom: 10px;text-align:center;'>".$txt_imagen."</a><hr>" : "<hr>";
                }
                $salida .= '</div>';
            }
            else{
                $salida .= '<p class="text-center"><b>'.$txt_sin_registros.'</b></p><br>';
                $salida .= '</div>';
            }
            $salida .= '</div>';
            echo $salida;
        }
    
    /*----------------------------------------*/
    /* Validaciones Extras
    /*----------------------------------------*/
        function file_check($str){
            $allowed_mime_type_arr = array('image/jpeg', 'image/png', 'image/jpg');
            $mime = get_mime_by_extension($_FILES['documento']['name']);
            if (isset($_FILES['documento']['name']) && $_FILES['documento']['name'] != "") {
                if (in_array($mime, $allowed_mime_type_arr)) {
                return true;
                } else {
                $this->form_validation->set_message('file_check', 'Solo se permiten archivos jpeg, jpg o png.');
                return false;
                }
            } else {
                $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
                return false;
            }
        }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*----------------------------------------*/
    /* 
    /*----------------------------------------*/
    function getCandidatos(){
        $cand['recordsTotal'] = $this->candidato_model->getTotalCandidatos($this->session->userdata('idcliente'));
        $cand['recordsFiltered'] = $this->candidato_model->getTotalCandidatos($this->session->userdata('idcliente'));
        $cand['data'] = $this->candidato_model->getCandidatos($this->session->userdata('idcliente'));
        $this->output->set_output( json_encode( $cand ) );
    }
    function getCandidatosSubcliente(){
        $id_subcliente = $_GET['id_subcliente'];
        $sub['recordsTotal'] = $this->candidato_model->getCandidatosSubclienteTotal($this->session->userdata('idcliente'), $id_subcliente);
        $sub['recordsFiltered'] = $this->candidato_model->getCandidatosSubclienteTotal($this->session->userdata('idcliente'), $id_subcliente);
        $sub['data'] = $this->candidato_model->getCandidatosSubcliente($this->session->userdata('idcliente'), $id_subcliente);
        $this->output->set_output( json_encode( $sub ) );
    }
	
    
	function storeCandidato(){
		date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->session->userdata('id');
        /*$candidato_nombre = $this->session->userdata('nombre');
        $candidato_paterno = $this->session->userdata('paterno');
        $candidato_materno = $this->session->userdata('materno');*/
        $cadena = $this->input->post('datos');
        parse_str($cadena, $personal);
        $cadena2 = $this->input->post('padres');
        parse_str($cadena2, $padres);
        $cadena3 = $this->input->post('complementos');
        parse_str($cadena3, $dato);
        //echo $personal['fecha_nacimiento'];
        //var_dump($padres);
        $fecha = fecha_ingles_bd($personal['fecha_nacimiento']);
        $edad = $this->calculaEdad($fecha);
        $candidato = array(
            'fecha_contestado' => $date,
            //'token' => 'completo',
            'edicion' => $date,
            'fecha_nacimiento' => $fecha,
            'edad' => $edad,
            'puesto' => $personal['puesto'],
            'nacionalidad' => $personal['nacionalidad'],
            'genero' => $personal['genero'],
            'id_grado_estudio' => 11,
            'calle' => $personal['calle'],
            'exterior' => $personal['exterior'],
            'interior' => $personal['interior'],
            'colonia' => $personal['colonia'],
            'id_estado' => $personal['estado'],
            'id_municipio' => $personal['municipio'],
            'cp' => $personal['cp'],
            'id_estado_civil' => $personal['civil'],
            'celular' => $personal['telefono'],
            'telefono_casa' => $personal['tel_casa'],
            'telefono_otro' => $personal['tel_otro'],
            'trabajo_inactivo' => $dato['trabajo_inactivo'],
            'trabajo_gobierno' => $dato['trabajo_gobierno'],
            'comentario' => $dato['obs'],
            'status' => 1
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        //var_dump($_POST['hijos']);
        if($personal['nombre_conyuge'] != ""){
            $data_conyuge = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'nombre' => $personal['nombre_conyuge'],
                'id_tipo_parentesco' => 4,
                'edad' => $personal['edad_conyuge'],
                'id_estado_civil' => 1,
                'id_grado_estudio' => 11,
                'misma_vivienda' => $personal['con_conyuge'],
                'ciudad' => $personal['ciudad_conyuge'],
                'empresa' => $personal['empresa_conyuge'],
                'puesto' => $personal['puesto_conyuge']
            );
            $this->candidato_model->savePersona($data_conyuge);
        }
        if($_POST['hijos'] != ""){
            $data_hijos = "";
            $h = explode("@@", $_POST['hijos']);
            for($i = 0; $i < (count($h) - 1); $i++){
                $aux = explode(",", $h[$i]);
                $data_hijos = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_candidato' => $id_candidato,
                    'nombre' => urldecode($aux[0]),
                    'id_tipo_parentesco' => 3,
                    'edad' => $aux[1],
                    'id_estado_civil' => 7,
                    'id_grado_estudio' => 11,
                    'misma_vivienda' => $aux[5],
                    'ciudad' => urldecode($aux[3]),
                    'empresa' => urldecode($aux[4]),
                    'puesto' => urldecode($aux[2])
                );
                $this->candidato_model->savePersona($data_hijos); 
            }
        }
        $data_padre = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_candidato' => $id_candidato,
            'nombre' => $padres['nombre_padre'],
            'id_tipo_parentesco' => 1,
            'edad' => $padres['edad_padre'],
            'id_estado_civil' => 7,
            'id_grado_estudio' => 11,
            'misma_vivienda' => $padres['con_padre'],
            'ciudad' => $padres['ciudad_padre'],
            'empresa' => $padres['empresa_padre'],
            'puesto' => $padres['puesto_padre']
        );
        $this->candidato_model->savePersona($data_padre);
        $data_madre = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_candidato' => $id_candidato,
            'nombre' => $padres['nombre_madre'],
            'id_tipo_parentesco' => 2,
            'edad' => $padres['edad_madre'],
            'id_estado_civil' => 7,
            'id_grado_estudio' => 11,
            'misma_vivienda' => $padres['con_madre'],
            'ciudad' => $padres['ciudad_madre'],
            'empresa' => $padres['empresa_madre'],
            'puesto' => $padres['puesto_madre']
        );
        $this->candidato_model->savePersona($data_madre);
        if($_POST['hermanos'] != ""){
            $data_hermanos = "";
            $h = explode("@@", $_POST['hermanos']);
            for($i = 0; $i < (count($h) - 1); $i++){
                $aux = explode(",", $h[$i]);
                $data_hermanos = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_candidato' => $id_candidato,
                    'nombre' => urldecode($aux[0]),
                    'id_tipo_parentesco' => 6,
                    'edad' => $aux[1],
                    'id_estado_civil' => 7,
                    'id_grado_estudio' => 11,
                    'misma_vivienda' => $aux[5],
                    'ciudad' => urldecode($aux[3]),
                    'empresa' => urldecode($aux[4]),
                    'puesto' => urldecode($aux[2])
                );
                $this->candidato_model->savePersona($data_hermanos); 
            }
        }
        $data_estudios = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_candidato' => $id_candidato,
            'primaria_periodo' => $dato['prim_periodo'],
            'primaria_escuela' => $dato['prim_escuela'],
            'primaria_ciudad' => $dato['prim_ciudad'],
            'primaria_certificado' => $dato['prim_certificado'],
            'secundaria_periodo' => $dato['sec_periodo'],
            'secundaria_escuela' => $dato['sec_escuela'],
            'secundaria_ciudad' => $dato['sec_ciudad'],
            'secundaria_certificado' => $dato['sec_certificado'],
            'preparatoria_periodo' => $dato['prep_periodo'],
            'preparatoria_escuela' => $dato['prep_escuela'],
            'preparatoria_ciudad' => $dato['prep_ciudad'],
            'preparatoria_certificado' => $dato['prep_certificado'],
            'licenciatura_periodo' => $dato['lic_periodo'],
            'licenciatura_escuela' => $dato['lic_escuela'],
            'licenciatura_ciudad' => $dato['lic_ciudad'],
            'licenciatura_certificado' => $dato['lic_certificado'],
            'otros_certificados' => $dato['otro_certificado'],
            'carrera_inactivo' => $dato['carrera_inactivo']
        );
        $this->candidato_model->saveEstudios($data_estudios);
        if($dato['refLab1_empresa'] != "" && $dato['refLab1_direccion'] != ""){
            $fentrada = fecha_ingles_bd($dato['refLab1_entrada']);
            $fsalida = fecha_ingles_bd($dato['refLab1_salida']);
            $data_reflab1 = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'empresa' => ucwords(strtolower($dato['refLab1_empresa'])),
                'direccion' => ucwords(strtolower($dato['refLab1_direccion'])),
                'fecha_entrada' => $fentrada,
                'fecha_salida' => $fsalida,
                'telefono' => $dato['refLab1_telefono'],
                'puesto1' => ucwords(strtolower($dato['refLab1_puesto1'])),
                'puesto2' => ucwords(strtolower($dato['refLab1_puesto2'])),
                'salario1' => $dato['refLab1_salario1'],
                'salario2' => $dato['refLab1_salario2'],
                'jefe_nombre' => ucwords(strtolower($dato['refLab1_bossnombre'])),
                'jefe_correo' => strtolower($dato['refLab1_bosscorreo']),
                'jefe_puesto' => ucwords(strtolower($dato['refLab1_bosspuesto'])),
                'causa_separacion' => $dato['refLab1_separacion']
            );
            $this->candidato_model->saveRefLab($data_reflab1);
        }
        if($dato['refLab2_empresa'] != "" && $dato['refLab2_direccion'] != ""){
            $fentrada = fecha_ingles_bd($dato['refLab2_entrada']);
            $fsalida = fecha_ingles_bd($dato['refLab2_salida']);
            $data_reflab2 = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'empresa' => ucwords(strtolower($dato['refLab2_empresa'])),
                'direccion' => ucwords(strtolower($dato['refLab2_direccion'])),
                'fecha_entrada' => $fentrada,
                'fecha_salida' => $fsalida,
                'telefono' => $dato['refLab2_telefono'],
                'puesto1' => ucwords(strtolower($dato['refLab2_puesto1'])),
                'puesto2' => ucwords(strtolower($dato['refLab2_puesto2'])),
                'salario1' => $dato['refLab2_salario1'],
                'salario2' => $dato['refLab2_salario2'],
                'jefe_nombre' => ucwords(strtolower($dato['refLab2_bossnombre'])),
                'jefe_correo' => strtolower($dato['refLab2_bosscorreo']),
                'jefe_puesto' => ucwords(strtolower($dato['refLab2_bosspuesto'])),
                'causa_separacion' => $dato['refLab2_separacion']
            );
            $this->candidato_model->saveRefLab($data_reflab2);
        }
        $data_refper1 = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_candidato' => $id_candidato,
            'nombre' => $dato['refPer1_nombre'],
            'telefono' => $dato['refPer1_telefono'],
            'tiempo_conocerlo' => $dato['refPer1_tiempo'],
            'donde_conocerlo' => $dato['refPer1_conocido'],
            'sabe_trabajo' => $dato['refPer1_sabetrabajo'],
            'sabe_vive' => $dato['refPer1_sabevive']
        );
        $this->candidato_model->saveRefPer($data_refper1);
        $data_refper2 = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_candidato' => $id_candidato,
            'nombre' => $dato['refPer2_nombre'],
            'telefono' => $dato['refPer2_telefono'],
            'tiempo_conocerlo' => $dato['refPer2_tiempo'],
            'donde_conocerlo' => $dato['refPer2_conocido'],
            'sabe_trabajo' => $dato['refPer2_sabetrabajo'],
            'sabe_vive' => $dato['refPer2_sabevive']
        );
        $this->candidato_model->saveRefPer($data_refper2);
        $data_refper3 = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_candidato' => $id_candidato,
            'nombre' => $dato['refPer3_nombre'],
            'telefono' => $dato['refPer3_telefono'],
            'tiempo_conocerlo' => $dato['refPer3_tiempo'],
            'donde_conocerlo' => $dato['refPer3_conocido'],
            'sabe_trabajo' => $dato['refPer3_sabetrabajo'],
            'sabe_vive' => $dato['refPer3_sabevive']
        );
        $this->candidato_model->saveRefPer($data_refper3);
        echo $hecho = 1;
	}
    function subirEstudiosCandidato(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $_POST['id_candidato'];
        $prefijo = str_replace(' ', '', $_POST['prefijo']);
        $countfiles = count($_FILES['estudios']['name']);
  
        for($i = 0; $i < $countfiles; $i++){
            if(!empty($_FILES['estudios']['name'][$i])){
                // Define new $_FILES array - $_FILES['file']
                $_FILES['file']['name'] = $_FILES['estudios']['name'][$i];
                $_FILES['file']['type'] = $_FILES['estudios']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['estudios']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['estudios']['error'][$i];
                $_FILES['file']['size'] = $_FILES['estudios']['size'][$i];
                $aux2 = str_replace(' ', '', $_FILES['estudios']['name'][$i]);
                $nombre_archivo = str_replace('_', '', $aux2);
                // Set preference
                $config['upload_path'] = './_docs/'; 
                $config['allowed_types'] = 'pdf|jpeg|jpg|png';
                $config['max_size'] = '15000'; // max_size in kb
                $config['file_name'] = $prefijo."_".$nombre_archivo;
                //Load upload library
                $this->load->library('upload',$config); 
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('file')){
                   // $data = $this->upload->data(); 
                    echo $salida = 1; 
                }
                $doc = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_candidato' => $id_candidato,
                    'id_tipo_documento' => 7,
                    'archivo' => $prefijo."_".$nombre_archivo
                );
                $this->candidato_model->insertDocCandidato($doc);
            }
        }
    }
    function subirAntecedenteCandidato(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['criminal']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('criminal')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 12,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
    function subirIneCandidato(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['ine']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('ine')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 3,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
    function subirAvisoCandidato(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['aviso']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('aviso')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 8,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
    function subirPasaporteCandidato(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['pasaporte']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('pasaporte')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 14,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
    function subirReporteIMSSCandidato(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $_POST['id_candidato'];
        $prefijo = str_replace(' ', '', $_POST['prefijo']);
        $countfiles = count($_FILES['semanas']['name']);
  
        for($i = 0; $i < $countfiles; $i++){
            if(!empty($_FILES['semanas']['name'][$i])){
                // Define new $_FILES array - $_FILES['file']
                $_FILES['file']['name'] = $_FILES['semanas']['name'][$i];
                $_FILES['file']['type'] = $_FILES['semanas']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['semanas']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['semanas']['error'][$i];
                $_FILES['file']['size'] = $_FILES['semanas']['size'][$i];
                $aux2 = str_replace(' ', '', $_FILES['semanas']['name'][$i]);
                $nombre_archivo = str_replace('_', '', $aux2);
                // Set preference
                $config['upload_path'] = './_docs/'; 
                $config['allowed_types'] = 'pdf|jpeg|jpg|png';
                $config['max_size'] = '15000'; // max_size in kb
                $config['file_name'] = $prefijo."_".$nombre_archivo;
                //Load upload library
                $this->load->library('upload',$config); 
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('file')){
                   // $data = $this->upload->data(); 
                    echo $salida = 1; 
                }
                $doc = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_candidato' => $id_candidato,
                    'id_tipo_documento' => 9,
                    'archivo' => $prefijo."_".$nombre_archivo
                );
                $this->candidato_model->insertDocCandidato($doc);
            }
        }
    }
    function subirComprobanteDomicilio(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['domicilio']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('domicilio')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 2,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
    function subirPasaporte(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['pasaporte']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('pasaporte')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 14,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
    function subirFormaMigratoria(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['forma']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('forma')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 20,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
    function subirMilitar(){
        //if(isset($_FILES["documento"]["name"])){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_candidato = $_POST['id_candidato'];
            $prefijo = str_replace(' ', '', $_POST['prefijo']);
            $archivos = array();
            $aux2 = str_replace(' ', '', $_FILES['militar']['name']);
            $nombre_archivo = str_replace('_', '', $aux2);
            $config['upload_path'] = './_docs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            /*$config['max_size']      = 1048; 
            $config['max_width']     = 1024; 
            $config['max_height']    = 768;*/
            $config['file_name'] = $prefijo."_".$nombre_archivo;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('militar')){
                $data = $this->upload->data();
                echo $salida = 1; 
            }

            $doc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 15,
                'archivo' => $prefijo."_".$nombre_archivo
            );
            $this->candidato_model->insertDocCandidato($doc);
        //}
    }
	
    
    
    
    
    
    
    function checkFamiliares(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $h1 = 1;
        $h2 = 1;
        $data['tipos_parentesco'] = $this->candidato_model->getParentescos();
        $data['familia'] = $this->candidato_model->checkFamiliares($id_candidato);
        if(isset($data['familia'])){
            foreach($data['familia'] as $f){
                if($f->id_tipo_parentesco == 4 && $f->nombre != "" && $f->nombre != "undefined"){
                    $salida .= '<p class="tituloSubseccion">Wife/Husband</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Name</label>
                                        <input type="text" class="form-control familia_obligado" name="nombre_conyuge" id="4_nombre_conyuge" value="'.$f->nombre.'">
                                        <br>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Age</label>
                                        <input type="text" class="form-control familia_obligado solo_numeros" name="edad_conyuge" id="4_edad_conyuge" maxlength="2" value="'.$f->edad.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Ocupation </label>
                                        <input type="text" class="form-control familia_obligado" name="puesto_conyuge" id="4_puesto_conyuge" value="'.$f->puesto.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>City</label>
                                        <input type="text" class="form-control familia_obligado" name="ciudad_conyuge" id="4_ciudad_conyuge" value="'.$f->ciudad.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Company</label>
                                        <input type="text" class="form-control familia_obligado" name="empresa_conyuge" id="4_empresa_conyuge" value="'.$f->empresa.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Live with her/him? </label>';
                                        $salida .= ($f->misma_vivienda == 1) ? '<input type="text" class="form-control" name="con_conyuge" id="4_con_conyuge" value="Sí" readonly>' : '<input type="text" class="form-control" name="con_conyuge" id="4_con_conyuge" value="No" readonly>';
                        $salida .= '<br>
                                    </div>
                                </div>';
                }
                if($f->id_tipo_parentesco == 3){
                    $salida .= "<p class='tituloSubseccion'>Child ".$h1."</p>
                                <div class='row'>
                                    <div class='col-md-3'>
                                        <label>Name *</label>
                                        <input type='text' class='form-control familia_obligado es_hijo' name='nombre_hijo".$h1."' id='3_nombre_hijo".$h1."' value='".$f->nombre."' >
                                        <br>
                                    </div>
                                    <div class='col-md-1'>
                                        <label>Age *</label>
                                        <input type='text' class='form-control solo_numeros familia_obligado es_hijo' name='edad_hijo".$h1."' id='3_edad_hijo".$h1."' value='".$f->edad."' maxlength='2' >
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>Ocupation *</label>
                                        <input type='text' class='form-control familia_obligado es_hijo' name='puesto_hijo".$h1."' id='3_puesto_hijo".$h1."' value='".$f->puesto."' >
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>City *</label>
                                        <input type='text' class='form-control familia_obligado es_hijo' name='ciudad_hijo".$h1."' id='3_ciudad_hijo".$h1."' value='".$f->ciudad."'>
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>School/Company *</label>
                                        <input type='text' class='form-control familia_obligado es_hijo' name='empresa_hijo".$h1."' id='3_empresa_hijo".$h1."' value='".$f->empresa."'>
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>Live with her/him? *</label>";
                                        $salida .= ($f->misma_vivienda == 1) ? '<input type="text" class="form-control  es_hijo" name="con_hijo'.$h1.'" id="3_con_hijo'.$h1.'" value="Sí" readonly>' : '<input type="text" class="form-control  es_hijo" name="con_hijo'.$h1.'" id="3_con_hijo'.$h1.'" value="No" readonly>';
    
                        $salida .= '<br>
                                    </div>
                                </div>';
                                $h1++;   
                }
                if($f->id_tipo_parentesco == 1){
                    $salida .= '<p class="tituloSubseccion">Father</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Name *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="nombre_padre" id="1_nombre_padre" value="'.$f->nombre.'" >
                                        <br>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Age *</label>
                                        <input type="text" class="form-control solo_numeros familia_obligado parte_familia" name="edad_padre" id="1_edad_padre" value="'.$f->edad.'" maxlength="2" >
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Ocupation *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="puesto_padre" id="1_puesto_padre" value="'.$f->puesto.'" >
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>City *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="ciudad_padre" id="1_ciudad_padre" value="'.$f->ciudad.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Company *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="empresa_padre" id="1_empresa_padre" value="'.$f->empresa.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Live with her/him? *</label>';
                                        $salida .= ($f->misma_vivienda == 1) ? '<input type="text" class="form-control parte_familia" name="con_padre" id="1_con_padre" value="Sí" readonly>' : '<input type="text" class="form-control parte_familia" name="con_padre" id="1_con_padre" value="No" readonly>';
                            $salida .= '<br>
                                    </div>
                                </div>';
                }
                if($f->id_tipo_parentesco == 2){
                    $salida .= '<p class="tituloSubseccion">Mother</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Name *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="nombre_madre" id="2_nombre_madre" value="'.$f->nombre.'" >
                                        <br>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Age *</label>
                                        <input type="text" class="form-control solo_numeros familia_obligado parte_familia" name="edad_madre" id="2_edad_madre" value="'.$f->edad.'" maxlength="2" >
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Ocupation *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="puesto_madre" id="2_puesto_madre" value="'.$f->puesto.'" >
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>City *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="ciudad_madre" id="2_ciudad_madre" value="'.$f->ciudad.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Company *</label>
                                        <input type="text" class="form-control familia_obligado parte_familia" name="empresa_madre" id="2_empresa_madre" value="'.$f->empresa.'">
                                        <br>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Live with her/him? *</label>';
                                        $salida .= ($f->misma_vivienda == 1) ? '<input type="text" class="form-control parte_familia" name="con_madre" id="2_con_madre" value="Sí" readonly>' : '<input type="text" class="form-control parte_familia" name="con_madre" id="2_con_madre" value="No" readonly>';
                        $salida .= '    <br>
                                    </div>
                                </div>';
                }
                if($f->id_tipo_parentesco == 6){
                    $salida .= "<p class='tituloSubseccion'>Sibling ".$h2."</p>
                                <div class='row'>
                                    <div class='col-md-3'>
                                        <label>Name *</label>
                                        <input type='text' class='form-control familia_obligado es_hermano' name='nombre_hermano".$h2."' id='6_nombre_hermano".$h2."' value='".$f->nombre."' >
                                        <br>
                                    </div>
                                    <div class='col-md-1'>
                                        <label>Age *</label>
                                        <input type='text' class='form-control solo_numeros familia_obligado es_hermano' name='edad_hermano".$h2."' id='6_edad_hermano".$h2."' value='".$f->edad."' maxlength='2' >
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>Ocupation *</label>
                                        <input type='text' class='form-control familia_obligado es_hermano' name='puesto_hermano".$h2."' id='6_puesto_hermano".$h2."' value='".$f->puesto."' >
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>City *</label>
                                        <input type='text' class='form-control familia_obligado es_hermano' name='ciudad_hermano".$h2."' id='6_ciudad_hermano".$h2."' value='".$f->ciudad."' >
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>School/Company *</label>
                                        <input type='text' class='form-control familia_obligado es_hermano' name='empresa_hermano".$h2."' id='6_empresa_hermano".$h2."' value='".$f->empresa."' >
                                        <br>
                                    </div>
                                    <div class='col-md-2'>
                                        <label>Live with her/him? *</label>";
                                        $salida .= ($f->misma_vivienda == 1) ? '<input type="text" class="form-control es_hermano" name="con_hermano'.$h2.'" id="6_con_hermano'.$h2.'" value="Sí" readonly>' : '<input type="text" class="form-control es_hermano" name="con_hermano'.$h2.'" id="6_con_hermano'.$h2.'" value="No" readonly>';
    
                        $salida .= '<br>
                                    </div>
                                </div>'; 
                        $h2++; 
                }
            }
            echo $salida;
        }
        else{
            $salida = 0;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    function getVerificacionReferencias(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['referencias'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        if($data['referencias']){
            foreach($data['referencias'] as $ref){
                $salida .= $ref->empresa."@@";
                $salida .= $ref->direccion."@@";
                $salida .= $ref->fecha_entrada."@@";
                $salida .= $ref->fecha_salida."@@";
                $salida .= $ref->telefono."@@";
                $salida .= $ref->puesto1."@@";
                $salida .= $ref->puesto2."@@";
                $salida .= $ref->salario1."@@";
                $salida .= $ref->salario2."@@";
                $salida .= $ref->jefe_nombre."@@";
                $salida .= $ref->jefe_correo."@@";
                $salida .= $ref->jefe_puesto."@@";
                $salida .= $ref->causa_separacion."@@";
                $salida .= $ref->id."@@";
                $salida .= $ref->numero_referencia."@@";
                $salida .= $ref->notas."@@";
                $salida .= $ref->demanda."@@";
                $salida .= $ref->responsabilidad."@@";
                $salida .= $ref->iniciativa."@@";
                $salida .= $ref->eficiencia."@@";
                $salida .= $ref->disciplina."@@";
                $salida .= $ref->puntualidad."@@";
                $salida .= $ref->limpieza."@@";
                $salida .= $ref->estabilidad."@@";
                $salida .= $ref->emocional."@@";
                $salida .= $ref->honestidad."@@";
                $salida .= $ref->rendimiento."@@";
                $salida .= $ref->actitud."@@";
                $salida .= $ref->recontratacion."@@";
                $salida .= $ref->motivo_recontratacion."@@";
                $salida .= $ref->numero_referencia."###";
            }
            
        }
        echo $salida;
    }
    
    
    
    function getTiposDocumentos(){
        $id_candidato = $_POST['id_candidato'];
        $salida = ""; $i = 0; $acum = array();
        $data['tipos'] = $this->candidato_model->getTiposDocs();
        $data['docs'] = $this->candidato_model->getDocs($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        if($data['agregados']){
            foreach($data['agregados'] as $item){
                $acum[] = $item->id_tipo_documento;
            }
        }
        if($data['docs']){
            foreach($data['docs'] as $doc){
                if($doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 9 || $doc->id_tipo_documento == 10 ||$doc->id_tipo_documento == 11 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 13 || $doc->id_tipo_documento == 18 || $doc->id_tipo_documento == 7){
                    if(substr($doc->archivo, -4) == '.jpg' || substr($doc->archivo, -4) == '.png' || substr($doc->archivo, -5) == '.jpeg' ||
                        substr($doc->archivo, -4) == '.JPG' || substr($doc->archivo, -4) == '.PNG' || substr($doc->archivo, -5) == '.JPEG'){
                        if(in_array($doc->id_tipo_documento, $acum)){
                            foreach($data['tipos'] as $tipo){
                                if($tipo->id == $doc->id_tipo_documento){
                                    $salida .= '
                                            <div class="col-md-4">
                                                <label class="contenedor_check">'.$doc->archivo.' ('.$tipo->nombre.')
                                                    <input type="checkbox" name="imagen'.$i.'" id="imagen'.$i.'" value="'.$tipo->id."@@".$doc->archivo.'" checked>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                    ';
                                    $i++;
                                    break;
                                }
                            } 
                        }
                        else{
                            foreach($data['tipos'] as $tipo){
                                if($tipo->id == $doc->id_tipo_documento){
                                    $salida .= '
                                            <div class="col-md-4">
                                                <label class="contenedor_check">'.$doc->archivo.' ('.$tipo->nombre.')
                                                    <input type="checkbox" name="imagen'.$i.'" id="imagen'.$i.'" value="'.$tipo->id."@@".$doc->archivo.'">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                    ';
                                    $i++;
                                    break;
                                }
                            } 
                        }                                           
                    }
                }
                else{
                    $salida .= "";
                }
            }
            unset($acum);
            echo $salida;
        }
        else{
            echo $salida .= '<p class="text-center">No hay documentos válidos (.jpg, .png) para añadir</p>';
        }       
    }
    function updateDatos1(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_personal');
        parse_str($cadena, $personal);
        $id_candidato = $personal['id_candidato'];
        $id_usuario = $this->session->userdata('id');

        $fecha = fecha_ingles_bd($personal['fecha_nacimiento']);
        $edad = $this->calculaEdad($fecha);
        $candidato = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'nombre' => $personal['nombre'],
            'paterno' => $personal['paterno'],
            'materno' => $personal['materno'],
            'fecha_nacimiento' => $fecha,
            'edad' => $edad,
            'puesto' => $personal['puesto'],
            'nacionalidad' => $personal['nacionalidad'],
            'genero' => $personal['genero'],
            'calle' => $personal['calle'],
            'exterior' => $personal['exterior'],
            'interior' => $personal['interior'],
            'colonia' => $personal['colonia'],
            'id_estado' => $personal['estado'],
            'id_municipio' => $personal['municipio'],
            'cp' => $personal['cp'],
            'id_estado_civil' => $personal['civil'],
            'celular' => $personal['telefono'],
            'telefono_casa' => $personal['tel_casa'],
            'telefono_otro' => $personal['tel_otro'],
            'correo' => strtolower($personal['correo'])
            //'trabajo_gobierno' => $dato['trabajo_gobierno']
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        echo $salida = 1;
    }
    
    function verificacionDocumentosCandidatoHCL(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_documentos');
        parse_str($cadena, $doc);
        $id_candidato = $doc['id_candidato'];
        $id_usuario = $this->session->userdata('id');
        $candidato = array(
            'id_usuario' => $id_usuario
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
       
        $verificacion_documento = array(
            'creacion' => $date,
            'id_usuario' => $id_usuario,
            'id_candidato' => $id_candidato,
            'licencia' => $doc['lic_profesional'],
            'licencia_institucion' => $doc['lic_institucion'],
            'ine' => $doc['ine_clave'],
            'ine_ano' => $doc['ine_registro'],
            'ine_vertical' => $doc['ine_vertical'],
            'ine_institucion' => $doc['ine_institucion'],
            'penales' => $doc['penales_numero'],
            'penales_institucion' => $doc['penales_institucion'],
            'domicilio' => $doc['direccion_numero'],
            'fecha_domicilio' => $doc['direccion_fecha'],
            'militar' => $doc['militar_numero'],
            'militar_fecha' => $doc['militar_fecha'],
            'pasaporte' => $doc['pasaporte_numero'],
            'pasaporte_fecha' => $doc['pasaporte_fecha'],
            'forma_migratoria' => $doc['forma_numero'],
            'forma_migratoria_fecha' => $doc['forma_fecha'],
            'comentarios' => $doc['doc_comentarios']
        );
       
        $this->candidato_model->cleanVerificacionDocumentos($id_candidato);
        $this->candidato_model->saveVerificacionDocumento($verificacion_documento);
        echo $salida = 1;
    }
    
    function updateDatabaseCheck(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('database');
        parse_str($cadena, $dato);
        $id_candidato = $dato['id_candidato'];
        $id_usuario = $this->session->userdata('id');

        $law_enforcement = (isset($dato['custom_law_enforcement']))? $dato['custom_law_enforcement']:"";
        $regulatory = (isset($dato['custom_regulatory']))? $dato['custom_regulatory']:"";
        $other_bodies = (isset($dato['custom_other_bodies']))? $dato['custom_other_bodies']:"";
        $sanctions = (isset($dato['custom_sanctions']))? $dato['custom_sanctions']:"";
        $media_searches = (isset($dato['custom_media_searches']))? $dato['custom_media_searches']:"";
        $sdn = (isset($dato['custom_sdn']))? $dato['custom_sdn']:"";
        /*$facis = (isset($dato['x_facis']))? $dato['x_facis']:"";
        $interpol = (isset($dato['e_interpol']))? $dato['e_interpol']:"";
        $bureau = (isset($dato['x_bureau']))? $dato['x_bureau']:"";
        $eur = (isset($dato['x_european_financial']))? $dato['x_european_financial']:"";
        $fda = (isset($dato['global_fda']))? $dato['global_fda']:"";
        */
        $candidato = array(
            'id_usuario' => $id_usuario
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        $database = array(
            'creacion' => $date,
            'id_usuario' => $id_usuario,
            'id_candidato' => $id_candidato,
            'law_enforcement' => $law_enforcement,
            'regulatory' => $regulatory,
            'sanctions' => $sanctions,
            'other_bodies' => $other_bodies,
            'media_searches' => $media_searches,
            'sdn' => $sdn,
            'global_comentarios' => $dato['custom_comentarios']
        );
        $this->candidato_model->cleanGlobalSearches($id_candidato);
        $this->candidato_model->saveGlobalSearches($database);
        echo $salida = 1;
    }
    function updateFamiliares1(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('padre');
        parse_str($cadena, $padre);
        $cadena2 = $this->input->post('madre');
        parse_str($cadena2, $madre);
        $cadena3 = $this->input->post('conyuge');
        parse_str($cadena3, $conyuge);
        
        $id_candidato = $_POST['id_candidato'];
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
            'id_usuario' => $id_usuario
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        if(isset($conyuge)){
            $data_conyuge = array(
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'nombre' => $conyuge['nombre_conyuge'],
                'id_tipo_parentesco' => 4,
                'edad' => $conyuge['edad_conyuge'],
                'id_estado_civil' => 1,
                'id_grado_estudio' => 11,
                'ciudad' => $conyuge['ciudad_conyuge'],
                'empresa' => $conyuge['empresa_conyuge'],
                'puesto' => $conyuge['puesto_conyuge']
            );
            $this->candidato_model->updatePersona($data_conyuge, $id_candidato, 4);
        }
        if(isset($padre)){
            $data_padre = array(
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'nombre' => $padre['nombre_padre'],
                'id_tipo_parentesco' => 1,
                'edad' => $padre['edad_padre'],
                'id_estado_civil' => 7,
                'id_grado_estudio' => 11,
                'ciudad' => $padre['ciudad_padre'],
                'empresa' => $padre['empresa_padre'],
                'puesto' => $padre['puesto_padre']
            );
            $this->candidato_model->updatePersona($data_padre, $id_candidato, 1);
        }
        if(isset($madre)){
            $data_madre = array(
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'nombre' => $madre['nombre_madre'],
                'id_tipo_parentesco' => 2,
                'edad' => $madre['edad_madre'],
                'id_estado_civil' => 7,
                'id_grado_estudio' => 11,
                'ciudad' => $madre['ciudad_madre'],
                'empresa' => $madre['empresa_madre'],
                'puesto' => $madre['puesto_madre']
            );
            $this->candidato_model->updatePersona($data_madre, $id_candidato, 2);
        }
        if($_POST['hijos'] != ""){
            $this->candidato_model->cleanFamiliares($id_candidato, 3);
            $data_hijos = "";
            $h = explode("@@", $_POST['hijos']);
            for($i = 0; $i < count($h); $i++){
                $aux = explode("__", $h[$i]);
                if($h[$i] != ""){
                    $data_hijos = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_candidato' => $id_candidato,
                        'nombre' => urldecode($aux[0]),
                        'id_tipo_parentesco' => 3,
                        'edad' => $aux[1],
                        'id_estado_civil' => 7,
                        'id_grado_estudio' => 11,
                        'misma_vivienda' => $misma = ($aux[5] == 'No') ? 0 : 1,
                        'ciudad' => urldecode($aux[3]),
                        'empresa' => urldecode($aux[4]),
                        'puesto' => urldecode($aux[2])
                    );
                    $this->candidato_model->savePersona($data_hijos);
                }
            }
        }

        if($_POST['hermanos'] != ""){
            $this->candidato_model->cleanFamiliares($id_candidato, 6);
            $data_hermanos = "";
            $h = explode("@@", $_POST['hermanos']);
            for($i = 0; $i < count($h); $i++){
                $aux = explode("__", $h[$i]);
                if($h[$i] != ""){
                    $data_hermanos = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_candidato' => $id_candidato,
                        'nombre' => urldecode($aux[0]),
                        'id_tipo_parentesco' => 6,
                        'edad' => $aux[1],
                        'id_estado_civil' => 7,
                        'id_grado_estudio' => 11,
                        'misma_vivienda' => $misma = ($aux[5] == 'No') ? 0 : 1,
                        'ciudad' => urldecode($aux[3]),
                        'empresa' => urldecode($aux[4]),
                        'puesto' => urldecode($aux[2])
                    );
                    $this->candidato_model->savePersona($data_hermanos);
                }
            }
        }
        echo $salida = 1;
    }
    function updateEstudios1(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_estudios');
        parse_str($cadena, $estudio);
        $id_candidato = $estudio['id_candidato'];
        $id_usuario = $this->session->userdata('id');
        $candidato = array(
            'id_usuario' => $id_usuario
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        $verificacion_estudios = array(
            'edicion' => $date,
            'primaria_periodo' => $estudio['prim_periodo'],
            'primaria_escuela' => $estudio['prim_escuela'],
            'primaria_ciudad' => $estudio['prim_ciudad'],
            'primaria_certificado' => $estudio['prim_certificado'],
            'primaria_validada' => $estudio['prim_validado'],
            'secundaria_periodo' => $estudio['sec_periodo'],
            'secundaria_escuela' => $estudio['sec_escuela'],
            'secundaria_ciudad' => $estudio['sec_ciudad'],
            'secundaria_certificado' => $estudio['sec_certificado'],
            'secundaria_validada' => $estudio['sec_validado'],
            'preparatoria_periodo' => $estudio['prep_periodo'],
            'preparatoria_escuela' => $estudio['prep_escuela'],
            'preparatoria_ciudad' => $estudio['prep_ciudad'],
            'preparatoria_certificado' => $estudio['prep_certificado'],
            'preparatoria_validada' => $estudio['prep_validado'],
            'licenciatura_periodo' => $estudio['lic_periodo'],
            'licenciatura_escuela' => $estudio['lic_escuela'],
            'licenciatura_ciudad' => $estudio['lic_ciudad'],
            'licenciatura_certificado' => $estudio['lic_certificado'],
            'licenciatura_validada' => $estudio['lic_validado'],
            'otros_certificados' => $estudio['otro_certificado'],
            'comentarios' => $estudio['estudios_comentarios'],
            'carrera_inactivo' => $estudio['carrera_inactivo']
        );
        $this->candidato_model->updateVerificacionEstudios($verificacion_estudios, $id_candidato);
        echo $salida = 1;
    }
    
    
    
    function actualizarReferenciaLaboral(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('reflaboral');
        $num = $this->input->post('num');
        parse_str($cadena, $ref);
        $id_candidato = $ref['id_candidato'];
        $id_usuario = $this->session->userdata('id');

        $data['refs'] = $this->candidato_model->revisionReferenciaLaboral($ref['idref']);
        if($data['refs']){
            $datos = array(
                'edicion' => $date,
                'empresa' => ucwords(strtolower($ref['reflab'.$num.'_empresa'])),
                'direccion' => ucwords(strtolower($ref['reflab'.$num.'_direccion'])),
                'fecha_entrada_txt' => $ref['reflab'.$num.'_entrada'],
                'fecha_salida_txt' => $ref['reflab'.$num.'_salida'],
                'telefono' => $ref['reflab'.$num.'_telefono'],
                'puesto1' => ucwords(strtolower($ref['reflab'.$num.'_puesto1'])),
                'puesto2' => ucwords(strtolower($ref['reflab'.$num.'_puesto2'])),
                'salario1_txt' => $ref['reflab'.$num.'_salario1'],
                'salario2_txt' => $ref['reflab'.$num.'_salario2'],
                'jefe_nombre' => ucwords(strtolower($ref['reflab'.$num.'_jefenombre'])),
                'jefe_correo' => strtolower($ref['reflab'.$num.'_jefecorreo']),
                'jefe_puesto' => ucwords(strtolower($ref['reflab'.$num.'_jefepuesto'])),
                'causa_separacion' => $ref['reflab'.$num.'_separacion']
            );
            $this->candidato_model->updateReferenciaLaboral($datos, $ref['idref']);
            $salida = $ref['idref'];
        }
        else{
            $datos = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'empresa' => ucwords(strtolower($ref['reflab'.$num.'_empresa'])),
                'direccion' => ucwords(strtolower($ref['reflab'.$num.'_direccion'])),
                'fecha_entrada_txt' => $ref['reflab'.$num.'_entrada'],
                'fecha_salida_txt' => $ref['reflab'.$num.'_salida'],
                'telefono' => $ref['reflab'.$num.'_telefono'],
                'puesto1' => ucwords(strtolower($ref['reflab'.$num.'_puesto1'])),
                'puesto2' => ucwords(strtolower($ref['reflab'.$num.'_puesto2'])),
                'salario1_txt' => $ref['reflab'.$num.'_salario1'],
                'salario2_txt' => $ref['reflab'.$num.'_salario2'],
                'jefe_nombre' => ucwords(strtolower($ref['reflab'.$num.'_jefenombre'])),
                'jefe_correo' => strtolower($ref['reflab'.$num.'_jefecorreo']),
                'jefe_puesto' => ucwords(strtolower($ref['reflab'.$num.'_jefepuesto'])),
                'causa_separacion' => $ref['reflab'.$num.'_separacion']
            );
            $salida = $this->candidato_model->saveReferenciaLaboral($datos);
        }
        echo $salida;
    }
    function actualizarReferenciaLaboralIngles(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('reflaboral');
        $num = $this->input->post('num');
        parse_str($cadena, $ref);
        $id_candidato = $ref['id_candidato'];
        $id_usuario = $this->session->userdata('id');

        $data['refs'] = $this->candidato_model->revisionReferenciaLaboral($ref['idref']);
        if($data['refs']){
            $datos = array(
                'edicion' => $date,
                'empresa' => ucwords(strtolower($ref['reflab'.$num.'_empresa_ingles'])),
                'direccion' => ucwords(strtolower($ref['reflab'.$num.'_direccion_ingles'])),
                'fecha_entrada_txt' => $ref['reflab'.$num.'_entrada_ingles'],
                'fecha_salida_txt' => $ref['reflab'.$num.'_salida_ingles'],
                'telefono' => $ref['reflab'.$num.'_telefono_ingles'],
                'puesto1' => ucwords(strtolower($ref['reflab'.$num.'_puesto1_ingles'])),
                'puesto2' => ucwords(strtolower($ref['reflab'.$num.'_puesto2_ingles'])),
                'salario1_txt' => $ref['reflab'.$num.'_salario1_ingles'],
                'salario2_txt' => $ref['reflab'.$num.'_salario2_ingles'],
                'jefe_nombre' => ucwords(strtolower($ref['reflab'.$num.'_jefenombre_ingles'])),
                'jefe_correo' => strtolower($ref['reflab'.$num.'_jefecorreo_ingles']),
                'jefe_puesto' => ucwords(strtolower($ref['reflab'.$num.'_jefepuesto_ingles'])),
                'causa_separacion' => $ref['reflab'.$num.'_separacion_ingles']
            );
            $this->candidato_model->updateReferenciaLaboral($datos, $ref['idref']);
            $salida = $ref['idref'];
        }
        else{
            $datos = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'empresa' => ucwords(strtolower($ref['reflab'.$num.'_empresa_ingles'])),
                'direccion' => ucwords(strtolower($ref['reflab'.$num.'_direccion_ingles'])),
                'fecha_entrada_txt' => $ref['reflab'.$num.'_entrada_ingles'],
                'fecha_salida_txt' => $ref['reflab'.$num.'_salida_ingles'],
                'telefono' => $ref['reflab'.$num.'_telefono_ingles'],
                'puesto1' => ucwords(strtolower($ref['reflab'.$num.'_puesto1_ingles'])),
                'puesto2' => ucwords(strtolower($ref['reflab'.$num.'_puesto2_ingles'])),
                'salario1_txt' => $ref['reflab'.$num.'_salario1_ingles'],
                'salario2_txt' => $ref['reflab'.$num.'_salario2_ingles'],
                'jefe_nombre' => ucwords(strtolower($ref['reflab'.$num.'_jefenombre_ingles'])),
                'jefe_correo' => strtolower($ref['reflab'.$num.'_jefecorreo_ingles']),
                'jefe_puesto' => ucwords(strtolower($ref['reflab'.$num.'_jefepuesto_ingles'])),
                'causa_separacion' => $ref['reflab'.$num.'_separacion_ingles']
            );
            $salida = $this->candidato_model->saveReferenciaLaboral($datos);
        }
        echo $salida;
    }
    
    
    
    function updatePersonales1(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_refPersonal');
        parse_str($cadena, $ref);
        $id_candidato = $ref['id_candidato'];
        $id_refper1 = $ref['id_refper1'];
        $id_refper2 = $ref['id_refper2'];
        $id_refper3 = $ref['id_refper3'];
        $id_usuario = $this->session->userdata('id');

        $candidato = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'trabajo_gobierno' => $ref['trabajo_gobierno']
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);

        $data_refper1 = array(
            'nombre' => $ref['refPer1_nombre'],
            'telefono' => $ref['refPer1_telefono'],
            'donde_conocerlo' => $ref['refPer1_conocido'],
            'comentario' => $ref['refPer1_comentario']
        );
        $data_refper2 = array(
            'nombre' => $ref['refPer2_nombre'],
            'telefono' => $ref['refPer2_telefono'],
            'donde_conocerlo' => $ref['refPer2_conocido'],
            'comentario' => $ref['refPer2_comentario']
        );
        $data_refper3 = array(
            'nombre' => $ref['refPer3_nombre'],
            'telefono' => $ref['refPer3_telefono'],
            'donde_conocerlo' => $ref['refPer3_conocido'],
            'comentario' => $ref['refPer3_comentario']
        );
        $this->candidato_model->updateReferenciaPersonal($id_refper1, $data_refper1);
        $this->candidato_model->updateReferenciaPersonal($id_refper2, $data_refper2);
        $this->candidato_model->updateReferenciaPersonal($id_refper3, $data_refper3);
        echo $salida = 1;
    }
    function updateArchivos1(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_images');
        parse_str($cadena, $arch);
        $id_candidato = $_POST['id_candidato'];
        $id_usuario = $this->session->userdata('id');
        //var_dump($arch);
        //Imagenes (archivos) para documento final
        $candidato = array(
            'id_usuario' => $id_usuario
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        
        if(isset($arch)){
            $this->candidato_model->cleanDocsAgregados($id_candidato);
            $cant = count($arch);
            for($i = 0;$i <= $cant;$i++){
                if(isset($arch['imagen'.$i])){
                    $img = explode('@@', $arch['imagen'.$i]);
                    $this->candidato_model->agregarDocumento($img[0], $img[1], $date, $id_usuario, $id_candidato);
                }
                else{
                    $i++;
                    if(isset($arch['imagen'.$i])){
                        $img = explode('@@', $arch['imagen'.$i]);
                        $this->candidato_model->agregarDocumento($img[0], $img[1], $date, $id_usuario, $id_candidato);
                    }
                }
            }
        }
        else{
            $this->candidato_model->cleanDocsAgregados($id_candidato);
        }
        
        echo $salida = 1;
    }
    
    
    
    
    
    function addCandidate(){
        $id_cliente = $this->session->userdata('idcliente');
        switch($id_cliente){
            case 1:
                $nombre = ucwords(strtolower($_POST['nombre']));
                $paterno = ucwords(strtolower($_POST['paterno']));
                $materno = ucwords(strtolower($_POST['materno']));
                
                $cel = $_POST['celular'];
                $tel = $_POST['fijo'];
                $correo = strtolower($_POST['correo']);
                $fecha_nacimiento = $_POST['fecha_nacimiento'];
                $proceso = $_POST['proceso'];
                $existeCandidato = $this->candidato_model->repetidoCandidato($nombre, $paterno, $materno, $correo, $id_cliente);
                if($existeCandidato > 0){
                    echo $res = 0;
                }
                else{
                    date_default_timezone_set('America/Mexico_City');
                    $date = date('Y-m-d H:i:s');
                    $id_usuario = $this->session->userdata('id');
                    $id_cliente = $this->session->userdata('idcliente');
                    if($fecha_nacimiento != "" && $fecha_nacimiento != null){
                        $fnacimiento = fecha_ingles_bd($fecha_nacimiento);
                    }
                    else{
                        $fnacimiento = "";
                    }
                    $base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
                    $aux = substr( md5(microtime()), 1, 8);
                    $token = md5($aux.$base);
                    $data = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario_cliente' => $id_usuario,
                        'fecha_alta' => $date,
                        'nombre' => $nombre,
                        'paterno' => $paterno,
                        'materno' => $materno,
                        'correo' => $correo,
                        'fecha_nacimiento' => $fnacimiento,
                        'token' => $token,
                        'id_cliente' => $id_cliente,
                        'celular' => $cel,
                        'telefono_casa' => $tel,
                        'id_tipo_proceso' => $proceso
                    );
                    $this->candidato_model->nuevoCandidato($data);
                    $from = $this->config->item('smtp_user');
                    $to = $correo;
                    $subject = strtolower($this->session->userdata('cliente'))." - credentials for register form";
                    $datos['password'] = $aux;
                    $datos['cliente'] = strtoupper($this->session->userdata('cliente'));
                    $datos['email'] = $correo;
                    $message = $this->load->view('login/mail_view',$datos,TRUE);
                    /*$this->email->set_newline("\r\n");
                    $this->email->from($from);
                    $this->email->to($to);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //$this->email->send();

                    if ($this->email->send()) {
                        echo $aux;
                    } else {
                        //show_error($this->email->print_debugger());
                        echo "No sent";
                    }*/
                    $this->load->library('phpmailer_lib');
                    $mail = $this->phpmailer_lib->load();
                    $mail->isSMTP();
                    $mail->Host     = 'rodi.com.mx';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'rodicontrol@rodi.com.mx';
                    $mail->Password = 'RRodi#2019@';
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port     = 465;
                    
                    $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                    $mail->addAddress($to);
                    $mail->Subject = $subject;
                    $mail->isHTML(true);
                    $mailContent = $message;
                    $mail->Body = $mailContent;

                    if(!$mail->send()){
                        //echo 'Message could not be sent.';
                        //echo 'Mailer Error: ' . $mail->ErrorInfo;
                        echo "No sent";
                    }else{
                        //echo 'Message has been sent';
                        echo $aux;
                    }
                }
            break;
            case 2:
                $val_antidoping = "";$val_psicometrico = "";$val_buro = "";
                if($this->input->post('socio') != 'on' && $this->input->post('antidoping') != 'on' && $this->input->post('psicometrico') != 'on' && $this->input->post('medico') != 'on' && $this->input->post('buro') != 'on'){
                    echo $val_antidoping = "<p>Se requiere confirmar al menos un estudio para el candidato</p>";
                }
                else{  
                    if($this->input->post('antidoping') == 'on'){
                        if($this->input->post('ant_paquete') == 'undefined' && ($this->input->post('ant_sustancia') == 'undefined' || $this->input->post('ant_sustancia') == '')){
                            echo $val_antidoping = "<p>Se requiere confirmar un paquete o parámetros para el antidoping</p>";
                        }
                        else{
                            if($this->input->post('ant_paquete') != 'undefined'){
                                $num_paq = $this->configuracion_model->countPaquetesAntidoping();
                                //$this->form_validation->set_rules('ant_paquete', 'antidoping por paquete', 'greater_than[0]|less_than['.($num_paq+1).']');
                                $val_antidoping = ($this->input->post('ant_paquete') > 0 && $this->input->post('ant_paquete') <= $num_paq)? 1 : "<p>La opción de antidoping por paquete no es válida</p>";
                                if($val_antidoping != 1){
                                    echo $val_antidoping;
                                }
                            }
                            else{
                                if($this->input->post('ant_sustancia') != 'undefined'){
                                    $num_sust = $this->configuracion_model->countSustanciasAntidoping();
                                    //$this->form_validation->set_rules('ant_sustancia', 'antidoping por parámetro', 'callback_string_values');
                                    if($this->string_values($this->input->post('ant_sustancia'))){
                                        $cont = 0;
                                        $aux = explode(',', $this->input->post('ant_sustancia'));
                                        for($i = 0; $i < count($aux); $i++){
                                            if(!($aux[$i] > 0 && $aux[$i] <= $num_sust)){
                                                $cont++;
                                            }
                                        }
                                        $val_antidoping = ($cont > 0)? "<p>La opción de antidoping por parámetros no es válida</p>" : 1;
                                        if($val_antidoping != 1){
                                            echo $val_antidoping;
                                        }
                                    }
                                    else{
                                        echo $val_antidoping = "<p>La opción de antidoping por parámetros no es válida</p>";
                                    }
                                }
                                
                            }
                        }
                    }
                    if($this->input->post('psicometrico') == 'on'){
                        if($this->input->post('psi_bateria') == 'undefined' && ($this->input->post('psi_prueba') == 'undefined' || $this->input->post('psi_prueba') == '')){
                            echo $val_psicometrico = "<p>Se requiere confirmar una batería o pruebas para el estudio psicométrico</p>";
                        }
                        else{
                            if($this->input->post('psi_bateria') != 'undefined'){
                                $num_bat = $this->configuracion_model->countBateriasPsicometrico();
                                //$this->form_validation->set_rules('psi_bateria', 'psicométrico por bateria', 'greater_than[0]|less_than['.($num_bat+1).']');
                                $val_psicometrico = ($this->input->post('psi_bateria') > 0 && $this->input->post('psi_bateria') <= $num_bat)? 1 : "<p>La opción de psicometría por batería no es válida</p>";
                                if($val_psicometrico != 1){
                                    echo $val_psicometrico;
                                }
                            }
                            else{
                                if($this->input->post('psi_prueba') != 'undefined'){
                                    $num_pru = $this->configuracion_model->countPruebasPsicometrico();
                                    //$this->form_validation->set_rules('psi_prueba', 'psicométrico por prueba', 'callback_string_values');
                                    if($this->string_values($this->input->post('psi_prueba'))){
                                        $cont = 0;
                                        $aux = explode(',', $this->input->post('psi_prueba'));
                                        for($i = 0; $i < count($aux); $i++){
                                            if(!($aux[$i] > 0 && $aux[$i] <= $num_pru)){
                                                $cont++;
                                            }
                                        }
                                        $val_psicometrico = ($cont > 0)? "<p>La opción de psicometría por pruebas no es válida</p>" : 1;
                                        if($val_psicometrico != 1){
                                            echo $val_psicometrico;
                                        }
                                    }
                                    else{
                                        echo $val_psicometrico = "<p>La opción de psicometría por pruebas no es válida</p>";
                                    }
                                }
                            }
                        }
                    }
                    if($this->input->post('buro') == 'on'){
                        if($this->input->post('buro_bu') == 'undefined'){
                            echo $val_buro = "<p>Se requiere confirmar una opción para el buró de crédito</p>";
                        }
                        else{
                            $num_buro = $this->configuracion_model->countBuroCredito();
                            //$this->form_validation->set_rules('buro_bu', 'opción del buró de crédito', 'greater_than[0]|less_than['.($num_buro+1).']');
                            $val_buro = ($this->input->post('buro_bu') > 0 && $this->input->post('buro_bu') <= $num_buro)? 1 : "<p>La opción de buró de crédito no es válida</p>";
                            if($val_buro != 1){
                                echo $val_buro;
                            }
                        }
                    }
                }
                $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|callback_alpha_space_only');
                $this->form_validation->set_rules('paterno', 'Apellido paterno', 'required|trim|callback_alpha_space_only');
                $this->form_validation->set_rules('materno', 'Apellido materno', 'required|trim|callback_alpha_space_only');
                $this->form_validation->set_rules('correo', 'Correo', 'required|valid_email');
                $this->form_validation->set_rules('celular', 'Tel. Celular', 'required|numeric|max_length[10]|min_length[10]');
                $this->form_validation->set_rules('fijo', 'Tel. Casa', 'numeric|max_length[10]|min_length[10]');
                $this->form_validation->set_rules('puesto', 'Puesto', 'required|numeric');
                
                if(empty($this->input->post('cv')) || $this->input->post('cv') == 'undefined'){

                    $this->form_validation->set_rules('cv', 'cv o solicitud de empleo', 'callback_required_file');
                }

                $this->form_validation->set_message('required','El campo {field} es obligatorio');
                //$this->form_validation->set_message('alpha','El campo {field} debe estar compuesto solo por letras');
                $this->form_validation->set_message('valid_email','El campo {field} debe ser un email válido');
                $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');
                $this->form_validation->set_message('min_length','El campo {field} no es válido');
                $this->form_validation->set_message('max_length','El campo {field} no es válido');
                $this->form_validation->set_message('less_than','El campo {field} no es válido');
                $this->form_validation->set_message('greater_than','El campo {field} no es válido');

                if($this->form_validation->run() != TRUE && ($val_antidoping != 1 || $val_psicometrico != 1 || $val_buro != 1 || $this->input->post('socio') != 'on' || $this->input->post('medico') != 'on')){ //Si la validación es incorrecta
                    echo validation_errors();
                }
                if($this->form_validation->run() == TRUE && ($val_antidoping == 1 || $val_psicometrico == 1 || $val_buro == 1 || $this->input->post('socio') == 'on' || $this->input->post('medico') == 'on')){
                    date_default_timezone_set('America/Mexico_City');
                    $date = date('Y-m-d H:i:s');
                    $id_usuario = $this->session->userdata('id');
                    $id_cliente = $this->session->userdata('idcliente');
                    $last = $this->candidato_model->lastIdCandidato();
                    $nombre_cv = ($last->id + 1)."_".$this->input->post('nombre')."".$this->input->post('paterno')."_".$_FILES['cv']['name'];

                    $config['upload_path'] = './_cvs/';  
                    $config['allowed_types'] = 'pdf|jpg|jpeg|png';
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $nombre_cv;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    // File upload
                    if($this->upload->do_upload('cv')){
                        $data = $this->upload->data();
                    }
                    $puesto = $this->candidato_model->getPuesto($this->input->post('puesto'));
                    $data = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario_cliente' => $id_usuario,
                        'fecha_alta' => $date,
                        'nombre' => $this->input->post('nombre'),
                        'paterno' => $this->input->post('paterno'),
                        'materno' => $this->input->post('materno'),
                        'puesto' => $puesto->nombre,
                        'correo' => $this->input->post('correo'),
                        'id_cliente' => $this->input->post('id_cliente'),
                        'celular' => $this->input->post('celular'),
                        'telefono_casa' => $this->input->post('fijo')
                    );
                    $nuevo_candidato = $this->candidato_model->addCandidato($data);
                    $documento = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_candidato' => $nuevo_candidato,
                        'id_tipo_documento' => 16,
                        'archivo' => $nombre_cv
                    );
                    $this->candidato_model->insertCVCandidato($documento);

                    $socio = ($this->input->post('socio') == 'on')? 1 : 0;
                    $medico = ($this->input->post('medico') == 'on')? 1 : 0;
                    $sociolaboral = ($this->input->post('laboral') == 'on')? 1 : 0;
                    if($this->input->post('antidoping') == 'on'){
                        if($this->input->post('ant_paquete') != 'undefined'){
                            $antidoping = $this->input->post('ant_paquete');
                            $tipo_antidoping = 1;
                        }
                        else{
                            if($this->input->post('ant_sustancia') != 'undefined'){
                                $antidoping = $this->input->post('ant_sustancia');
                                $tipo_antidoping = 2;
                            }
                        }
                    }
                    else{
                        $antidoping = 0;
                    }
                    if($this->input->post('psicometrico') == 'on'){
                        if($this->input->post('psi_bateria') != 'undefined'){
                            $psicometrico = $this->input->post('psi_bateria');
                            $tipo_psicometrico = 1;
                        }
                        else{
                            if($this->input->post('psi_prueba') != 'undefined'){
                                $psicometrico = $this->input->post('psi_prueba');
                                $tipo_psicometrico = 2;
                            }
                        }
                    }
                    else{
                        $psicometrico = 0;
                    }
                    if($this->input->post('buro') == 'on'){
                        if($this->input->post('buro_bu') != 'undefined'){
                            $buro = $this->input->post('buro_bu');
                        }
                        else{
                            $buro = 0;
                        }
                    }
                    else{
                        $buro = 0;
                    }
                    //var_dump($buro);
                    $pruebas = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario_cliente' => $id_usuario,
                        'id_candidato' => $nuevo_candidato,
                        'id_cliente' => $id_cliente,
                        'socioeconomico' => $socio,
                        'tipo_antidoping' => $tipo_antidoping,
                        'antidoping' => $antidoping,
                        'tipo_psicometrico' => $tipo_psicometrico,
                        'psicometrico' => $psicometrico,
                        'medico' => $medico,
                        'buro_credito' => $buro,
                        'sociolaboral' => $sociolaboral,
                        'otro_requerimiento' => $this->input->post('otro')
                    );
                    $this->candidato_model->insertPruebasCandidato($pruebas);

                    echo $salida = 1;
                    
                }
            break;
            case 3:
                /*$this->form_validation->set_rules('nombre', 'Name', 'required|trim|callback_alpha_space_only_english');
                $this->form_validation->set_rules('paterno', 'First lastname', 'required|trim|callback_alpha_space_only_english');
                //$this->form_validation->set_rules('materno', 'Second lastname', 'required|trim|callback_alpha_space_only_english');
                $this->form_validation->set_rules('correo', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('celular', 'Cell phone number', 'required|numeric|max_length[10]|min_length[10]');
                $this->form_validation->set_rules('fijo', 'Home number', 'numeric|max_length[10]|min_length[10]');
                $this->form_validation->set_rules('proyecto', 'Project', 'required|numeric');
                $this->form_validation->set_rules('examen', 'Drug test', 'required|numeric');

                $this->form_validation->set_message('required','The field {field} is required');
                $this->form_validation->set_message('valid_email','The field {field} must be an valid email');
                $this->form_validation->set_message('numeric','The field {field} must be a number');
                $this->form_validation->set_message('min_length','The field {field} is not valid');
                $this->form_validation->set_message('max_length','The field {field} is not valid');
                $this->form_validation->set_message('less_than','The field {field} is not valid');
                $this->form_validation->set_message('greater_than','The field {field} is not valid');
                if($this->form_validation->run() != TRUE){ 
                    echo validation_errors();
                }
                if($this->form_validation->run() == TRUE){*/
                $id_cliente = $this->session->userdata('idcliente');
                //$id_subcliente = $this->session->userdata('idsubcliente');
                $nombre = strtoupper($this->input->post('nombre'));
                $paterno = strtoupper($this->input->post('paterno'));
                $materno = strtoupper($this->input->post('materno'));
                $cel = $this->input->post('celular');
                $tel = $this->input->post('fijo');
                $correo = strtolower($this->input->post('correo'));
                $fecha_nacimiento = $this->input->post('fecha_nacimiento');
                $proyecto = $this->input->post('proyecto');
                $examen = $this->input->post('examen');
                $existeCandidato = $this->candidato_model->repetidoCandidato($nombre, $paterno, $materno, $correo, $id_cliente);
                if($existeCandidato > 0){
                    echo $res = 0;
                }
                else{
                    date_default_timezone_set('America/Mexico_City');
                    $date = date('Y-m-d H:i:s');
                    $id_usuario = $this->session->userdata('id');
                    $last = $this->candidato_model->lastIdCandidato();
                    $last = ($last == null || $last == "")? 0 : $last;
                    if($fecha_nacimiento != "" && $fecha_nacimiento != null){
                        $fnacimiento = fecha_ingles_bd($fecha_nacimiento);
                    }
                    else{
                        $fnacimiento = "";
                    }

                    $token = "completo";
                    $socioeconomico = 1;

                    $tipo_antidoping = ($examen == 0)? 0:1;
                    $antidoping = ($examen == 0)? 0:$examen;
                    $data = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario' => $id_usuario,
                        'fecha_alta' => $date,
                        'nombre' => $nombre,
                        'paterno' => $paterno,
                        'materno' => $materno,
                        'correo' => $correo,
                        'fecha_nacimiento' => $fnacimiento,
                        'token' => $token,
                        'id_cliente' => $id_cliente,
                        'id_subcliente' => 0,
                        'celular' => $cel,
                        'telefono_casa' => $tel,
                        'id_proyecto' => $proyecto
                    );
                    $this->candidato_model->nuevoCandidato($data);

                    //$doping = $this->candidato_model->getPaqueteAntidopingCandidato($id_cliente, $proyecto);
                    $pruebas = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario_cliente' => $id_usuario,
                        'id_candidato' => ($last->id + 1),
                        'id_cliente' => $id_cliente,
                        'socioeconomico' => $socioeconomico,
                        'tipo_antidoping' => $tipo_antidoping,
                        'antidoping' => $antidoping
                        
                    );
                    $this->candidato_model->insertPruebasCandidato($pruebas);
                    echo "creado";
                }
            break;
        }
    }
    function nuevoCandidato(){
        $this->form_validation->set_rules('nombre', 'Name', 'required|trim|callback_alpha_space_only_english');
        $this->form_validation->set_rules('paterno', 'First lastname', 'required|trim|callback_alpha_space_only_english');
        $this->form_validation->set_rules('materno', 'Second lastname', 'required|trim|callback_alpha_space_only_english');
        $this->form_validation->set_rules('correo', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('celular', 'Cell phone number', 'required|numeric|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('fijo', 'Home number', 'numeric|max_length[10]|min_length[10]');
        /*if(empty($this->input->post('ine')) || $this->input->post('ine') == 'undefined'){

            $this->form_validation->set_rules('ine', 'ine o solicitud de empleo', 'callback_required_file');
        }*/
        /*$this->form_validation->set_message('required','El campo {field} es obligatorio');
        $this->form_validation->set_message('valid_email','El campo {field} debe ser un email válido');
        $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');
        $this->form_validation->set_message('min_length','El campo {field} no es válido');
        $this->form_validation->set_message('max_length','El campo {field} no es válido');
        $this->form_validation->set_message('less_than','El campo {field} no es válido');
        $this->form_validation->set_message('greater_than','El campo {field} no es válido');*/
        if($this->form_validation->run() != TRUE){ 
            echo validation_errors();
        }
        if($this->form_validation->run() == TRUE){
            $id_cliente = $this->session->userdata('idcliente');
            $nombre = ucwords(strtolower($this->input->post('nombre')));
            $paterno = ucwords(strtolower($this->input->post('paterno')));
            $materno = ucwords(strtolower($this->input->post('materno')));
            $cel = $this->input->post('celular');
            $tel = $this->input->post('fijo');
            $correo = strtolower($this->input->post('correo'));
            $fecha_nacimiento = $this->input->post('fecha_nacimiento');
            $proceso = $this->input->post('proceso');
            $existeCandidato = $this->candidato_model->repetidoCandidato($nombre, $paterno, $materno, $correo, $id_cliente);
            if($existeCandidato > 0){
                echo $res = 0;
            }
            else{
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $id_usuario = $this->session->userdata('id');
                $last = $this->candidato_model->lastIdCandidato();
                if(isset($_FILES['ine'])){
                    $nombre_ine = ($last->id + 1)."_".$this->input->post('nombre')."".$this->input->post('paterno')."_".$_FILES['ine']['name'];
                    $config['upload_path'] = './_docs/';  
                    $config['allowed_types'] = 'pdf|jpg|jpeg|png';
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $nombre_ine;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    // File upload
                    if($this->upload->do_upload('ine')){
                        $documento = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => ($last->id + 1),
                            'id_tipo_documento' => 3,
                            'archivo' => $nombre_ine
                        );
                        $this->candidato_model->insertCVCandidato($documento);
                        $data = $this->upload->data();
                    }
                }
                if($fecha_nacimiento != "" && $fecha_nacimiento != null){
                    $fnacimiento = fecha_ingles_bd($fecha_nacimiento);
                }
                else{
                    $fnacimiento = "";
                }
                if($proceso == 1){
                    $base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
                    $aux = substr( md5(microtime()), 1, 8);
                    $token = md5($aux.$base);
                    $data = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario_cliente' => $id_usuario,
                        'fecha_alta' => $date,
                        'nombre' => $nombre,
                        'paterno' => $paterno,
                        'materno' => $materno,
                        'correo' => $correo,
                        'fecha_nacimiento' => $fnacimiento,
                        'token' => $token,
                        'id_cliente' => $id_cliente,
                        'celular' => $cel,
                        'telefono_casa' => $tel,
                        'id_tipo_proceso' => $proceso
                    );
                    $this->candidato_model->nuevoCandidato($data);
                    $from = $this->config->item('smtp_user');
                    $to = $correo;
                    $subject = strtolower($this->session->userdata('cliente'))." - credentials for register form";
                    $datos['password'] = $aux;
                    $datos['cliente'] = strtoupper($this->session->userdata('cliente'));
                    $datos['email'] = $correo;
                    $message = $this->load->view('login/mail_view',$datos,TRUE);
                    $this->load->library('phpmailer_lib');
                    $mail = $this->phpmailer_lib->load();
                    $mail->isSMTP();
                    $mail->Host     = 'rodi.com.mx';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'rodicontrol@rodi.com.mx';
                    $mail->Password = 'RRodi#2019@';
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port     = 465;
                    
                    $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                    $mail->addAddress($to);
                    $mail->Subject = $subject;
                    $mail->isHTML(true);
                    $mailContent = $message;
                    $mail->Body = $mailContent;

                    if(!$mail->send()){
                        //echo 'Message could not be sent.';
                        //echo 'Mailer Error: ' . $mail->ErrorInfo;
                        echo "No sent@@".$aux;
                    }else{
                        //echo 'Message has been sent';
                        echo "Sent@@".$aux;
                    }
                }
                if($proceso == 2){
                    $data = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario_cliente' => $id_usuario,
                        'fecha_alta' => $date,
                        'nombre' => $nombre,
                        'paterno' => $paterno,
                        'materno' => $materno,
                        'correo' => $correo,
                        'fecha_nacimiento' => $fnacimiento,
                        'id_cliente' => $id_cliente,
                        'celular' => $cel,
                        'telefono_casa' => $tel,
                        'id_tipo_proceso' => $proceso
                    );
                    $this->candidato_model->nuevoCandidato($data);
                    echo $aux = 1;
                }
            }
        }
    }
    
    function registrarCandidatosIngles(){
        $id_cliente = $this->input->post('id_cliente');
        $id_subcliente = $this->input->post('subcliente');
        $nombre = strtoupper($this->input->post('nombre'));
        $paterno = strtoupper($this->input->post('paterno'));
        $materno = strtoupper($this->input->post('materno'));
        $cel = $this->input->post('celular');
        $tel = $this->input->post('fijo');
        $correo = strtolower($this->input->post('correo'));
        $examen = $this->input->post('examen');
        $proceso = $this->input->post('proceso');
        $puesto = $this->input->post('puesto');
        $otro = $this->input->post('otro');
        $existeCandidato = $this->candidato_model->repetidoCandidato($nombre, $paterno, $materno, $correo, $id_cliente);
        if($existeCandidato > 0){
            echo $res = 0;
        }
        else{
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $last = $this->candidato_model->lastIdCandidato();
            $base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
            $aux = substr( md5(microtime()), 1, 8);
            $token = ($proceso == 3 || $proceso == 4)? md5($aux.$base):'';
            $data = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_cliente' => $id_cliente,
                'id_subcliente' => $id_subcliente,
                'id_puesto' => $puesto,
                'token' => $token,
                'id_tipo_proceso' => $proceso,
                'fecha_alta' => $date,
                'nombre' => $nombre,
                'paterno' => $paterno,
                'materno' => $materno,
                'correo' => $correo,
                'celular' => $cel,
                'telefono_casa' => $tel
            );
            $id_candidato = $this->candidato_model->registrarCandidatoEspanol($data);
            //Subida y Registro de CV
            if($this->input->post('hay_cvs') == 1){
                $countfiles = count($_FILES['cvs']['name']);
                $nombreCandidato = str_replace(' ', '', $this->input->post('nombre'));
                $paternoCandidato = str_replace(' ', '', $this->input->post('paterno'));

                for($i = 0; $i < $countfiles; $i++){
                    if(!empty($_FILES['cvs']['name'][$i])){
                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_FILES['cvs']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['cvs']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['cvs']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['cvs']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['cvs']['size'][$i];
                        $temp = str_replace(' ', '', $_FILES['cvs']['name'][$i]);
                        $nombre_cv = $id_candidato."_".$nombreCandidato."".$paternoCandidato."_".$temp;
                        // Set preference
                        $config['upload_path'] = './_docs/'; 
                        $config['allowed_types'] = 'pdf|jpeg|jpg|png';
                        $config['max_size'] = '15000'; // max_size in kb
                        $config['file_name'] = $nombre_cv;
                        //Load upload library
                        $this->load->library('upload',$config); 
                        $this->upload->initialize($config);
                        // File upload
                        if($this->upload->do_upload('file')){
                            $data = $this->upload->data(); 
                            //$salida = 1; 
                        }
                        $documento = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'id_tipo_documento' => 16,
                            'archivo' => $nombre_cv
                        );
                        $this->candidato_model->insertCVCandidato($documento);
                    }
                }
            }
            //$socio = ($this->input->post('socio') == 'on')? 1 : 0;
            $medico = ($this->input->post('medico') == 'on')? 1 : 0;
            $sociolaboral = ($this->input->post('laboral') == 'on')? 1 : 0;
            $antidoping = ($this->input->post('antidoping') == 'on')? 1 : 0;
            $psicometrico = ($this->input->post('psicometrico') == 'on')? 1 : 0;
            $buro = ($this->input->post('buro') == 'on')? 1 : 0;

            if($antidoping == 1){
                $drogas = ($examen != "")? $examen:0;
                $tipo_antidoping = 1;
            }
            else{
                $drogas = 0;
                $tipo_antidoping = 0;
            }
            $pruebas = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'id_cliente' => $id_cliente,
                'socioeconomico' => 1,
                'tipo_antidoping' => $tipo_antidoping,
                'antidoping' => $drogas,
                'tipo_psicometrico' => $psicometrico,
                'psicometrico' => $psicometrico,
                'medico' => $medico,
                'buro_credito' => $buro,
                'sociolaboral' => $sociolaboral,
                'otro_requerimiento' => $otro
            );
            $this->candidato_model->insertPruebasCandidato($pruebas);
            if($proceso == 3 || $proceso == 4){
                $data_candidato = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
                $from = $this->config->item('smtp_user');
                $to = $correo;
                $subject = $data_candidato->subcliente." - credentials for register form";
                $datos['password'] = $aux;
                $datos['cliente'] = strtoupper($this->session->userdata('cliente'));
                $datos['email'] = $correo;
                $message = $this->load->view('login/mail_view',$datos,TRUE);
                $this->load->library('phpmailer_lib');
                $mail = $this->phpmailer_lib->load();
                $mail->isSMTP();
                $mail->Host     = 'rodi.com.mx';
                $mail->SMTPAuth = true;
                $mail->Username = 'rodicontrol@rodi.com.mx';
                $mail->Password = 'RRodi#2019@';
                $mail->SMTPSecure = 'ssl';
                $mail->Port     = 465;
                
                $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                $mail->addAddress($to);
                $mail->Subject = $subject;
                $mail->isHTML(true);
                $mailContent = $message;
                $mail->Body = $mailContent;

                if(!$mail->send()){
                    echo "No sent@@".$aux;
                }else{
                    echo "Sent@@".$aux;
                }
            }
            else{
                echo $res = 1;
            }
        }
    }
    function registrarCandidato(){
        $val_antidoping = "";$val_psicometrico = "";$val_buro = "";
        if($this->input->post('socio') != 'on' && $this->input->post('antidoping') != 'on' && $this->input->post('psicometrico') != 'on' && $this->input->post('medico') != 'on' && $this->input->post('buro') != 'on'){
            echo $val_antidoping = "<p>Se requiere confirmar al menos un estudio para el candidato</p>";
        }
        else{  
            if($this->input->post('antidoping') == 'on'){
                if($this->input->post('ant_paquete') == 'undefined' && ($this->input->post('ant_sustancia') == 'undefined' || $this->input->post('ant_sustancia') == '')){
                    echo $val_antidoping = "<p>Se requiere confirmar un paquete o parámetros para el antidoping</p>";
                }
                else{
                    if($this->input->post('ant_paquete') != 'undefined'){
                        $num_paq = $this->configuracion_model->countPaquetesAntidoping();
                        //$this->form_validation->set_rules('ant_paquete', 'antidoping por paquete', 'greater_than[0]|less_than['.($num_paq+1).']');
                        $val_antidoping = ($this->input->post('ant_paquete') > 0 && $this->input->post('ant_paquete') <= $num_paq)? 1 : "<p>La opción de antidoping por paquete no es válida</p>";
                        if($val_antidoping != 1){
                            echo $val_antidoping;
                        }
                    }
                    else{
                        if($this->input->post('ant_sustancia') != 'undefined'){
                            $num_sust = $this->configuracion_model->countSustanciasAntidoping();
                            //$this->form_validation->set_rules('ant_sustancia', 'antidoping por parámetro', 'callback_string_values');
                            if($this->string_values($this->input->post('ant_sustancia'))){
                                $cont = 0;
                                $aux = explode(',', $this->input->post('ant_sustancia'));
                                for($i = 0; $i < count($aux); $i++){
                                    if(!($aux[$i] > 0 && $aux[$i] <= $num_sust)){
                                        $cont++;
                                    }
                                }
                                $val_antidoping = ($cont > 0)? "<p>La opción de antidoping por parámetros no es válida</p>" : 1;
                                if($val_antidoping != 1){
                                    echo $val_antidoping;
                                }
                            }
                            else{
                                echo $val_antidoping = "<p>La opción de antidoping por parámetros no es válida</p>";
                            }
                        }
                        
                    }
                }
            }
            if($this->input->post('psicometrico') == 'on'){
                if($this->input->post('psi_bateria') == 'undefined' && ($this->input->post('psi_prueba') == 'undefined' || $this->input->post('psi_prueba') == '')){
                    echo $val_psicometrico = "<p>Se requiere confirmar una batería o pruebas para el estudio psicométrico</p>";
                }
                else{
                    if($this->input->post('psi_bateria') != 'undefined'){
                        $num_bat = $this->configuracion_model->countBateriasPsicometrico();
                        //$this->form_validation->set_rules('psi_bateria', 'psicométrico por bateria', 'greater_than[0]|less_than['.($num_bat+1).']');
                        $val_psicometrico = ($this->input->post('psi_bateria') > 0 && $this->input->post('psi_bateria') <= $num_bat)? 1 : "<p>La opción de psicometría por batería no es válida</p>";
                        if($val_psicometrico != 1){
                            echo $val_psicometrico;
                        }
                    }
                    else{
                        if($this->input->post('psi_prueba') != 'undefined'){
                            $num_pru = $this->configuracion_model->countPruebasPsicometrico();
                            //$this->form_validation->set_rules('psi_prueba', 'psicométrico por prueba', 'callback_string_values');
                            if($this->string_values($this->input->post('psi_prueba'))){
                                $cont = 0;
                                $aux = explode(',', $this->input->post('psi_prueba'));
                                for($i = 0; $i < count($aux); $i++){
                                    if(!($aux[$i] > 0 && $aux[$i] <= $num_pru)){
                                        $cont++;
                                    }
                                }
                                $val_psicometrico = ($cont > 0)? "<p>La opción de psicometría por pruebas no es válida</p>" : 1;
                                if($val_psicometrico != 1){
                                    echo $val_psicometrico;
                                }
                            }
                            else{
                                echo $val_psicometrico = "<p>La opción de psicometría por pruebas no es válida</p>";
                            }
                        }
                    }
                }
            }
            if($this->input->post('buro') == 'on'){
                if($this->input->post('buro_bu') == 'undefined'){
                    echo $val_buro = "<p>Se requiere confirmar una opción para el buró de crédito</p>";
                }
                else{
                    $num_buro = $this->configuracion_model->countBuroCredito();
                    //$this->form_validation->set_rules('buro_bu', 'opción del buró de crédito', 'greater_than[0]|less_than['.($num_buro+1).']');
                    $val_buro = ($this->input->post('buro_bu') > 0 && $this->input->post('buro_bu') <= $num_buro)? 1 : "<p>La opción de buró de crédito no es válida</p>";
                    if($val_buro != 1){
                        echo $val_buro;
                    }
                }
            }
        }
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|callback_alpha_space_only');
        $this->form_validation->set_rules('paterno', 'Apellido paterno', 'required|trim|callback_alpha_space_only');
        $this->form_validation->set_rules('materno', 'Apellido materno', 'required|trim|callback_alpha_space_only');
        $this->form_validation->set_rules('correo', 'Correo', 'required|valid_email');
        $this->form_validation->set_rules('celular', 'Tel. Celular', 'required|numeric|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('fijo', 'Tel. Casa', 'numeric|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('puesto', 'Puesto', 'required|numeric');
        
        if(empty($this->input->post('cv')) || $this->input->post('cv') == 'undefined'){

            $this->form_validation->set_rules('cv', 'cv o solicitud de empleo', 'callback_required_file');
        }

        $this->form_validation->set_message('required','El campo {field} es obligatorio');
        //$this->form_validation->set_message('alpha','El campo {field} debe estar compuesto solo por letras');
        $this->form_validation->set_message('valid_email','El campo {field} debe ser un email válido');
        $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');
        $this->form_validation->set_message('min_length','El campo {field} no es válido');
        $this->form_validation->set_message('max_length','El campo {field} no es válido');
        $this->form_validation->set_message('less_than','El campo {field} no es válido');
        $this->form_validation->set_message('greater_than','El campo {field} no es válido');

        if($this->form_validation->run() != TRUE && ($val_antidoping != 1 || $val_psicometrico != 1 || $val_buro != 1 || $this->input->post('socio') != 'on' || $this->input->post('medico') != 'on')){ //Si la validación es incorrecta
            echo validation_errors();
        }
        if($this->form_validation->run() == TRUE && ($val_antidoping == 1 || $val_psicometrico == 1 || $val_buro == 1 || $this->input->post('socio') == 'on' || $this->input->post('medico') == 'on')){
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $id_cliente = $this->session->userdata('idcliente');
            $last = $this->candidato_model->lastIdCandidato();
            $nombre_cv = ($last->id + 1)."_".$this->input->post('nombre')."".$this->input->post('paterno')."_".$_FILES['cv']['name'];

            $config['upload_path'] = './_cvs/';  
            $config['allowed_types'] = 'pdf|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            $config['file_name'] = $nombre_cv;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // File upload
            if($this->upload->do_upload('cv')){
                $data = $this->upload->data();
            }
            $puesto = $this->candidato_model->getPuesto($this->input->post('puesto'));
            $data = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'fecha_alta' => $date,
                'nombre' => $this->input->post('nombre'),
                'paterno' => $this->input->post('paterno'),
                'materno' => $this->input->post('materno'),
                'puesto' => $puesto->nombre,
                'correo' => $this->input->post('correo'),
                'id_cliente' => $this->input->post('id_cliente'),
                'id_subcliente' => $this->input->post('id_subcliente'),
                'celular' => $this->input->post('celular'),
                'telefono_casa' => $this->input->post('fijo')
            );
            $nuevo_candidato = $this->candidato_model->addCandidato($data);
            $documento = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $nuevo_candidato,
                'id_tipo_documento' => 16,
                'archivo' => $nombre_cv
            );
            $this->candidato_model->insertCVCandidato($documento);

            $socio = ($this->input->post('socio') == 'on')? 1 : 0;
            $medico = ($this->input->post('medico') == 'on')? 1 : 0;
            $sociolaboral = ($this->input->post('laboral') == 'on')? 1 : 0;
            if($this->input->post('antidoping') == 'on'){
                if($this->input->post('ant_paquete') != 'undefined'){
                    $antidoping = $this->input->post('ant_paquete');
                    $tipo_antidoping = 1;
                }
                else{
                    if($this->input->post('ant_sustancia') != 'undefined'){
                        $antidoping = $this->input->post('ant_sustancia');
                        $tipo_antidoping = 2;
                    }
                }
            }
            else{
                $tipo_antidoping = 0;
                $antidoping = 0;
            }
            if($this->input->post('psicometrico') == 'on'){
                if($this->input->post('psi_bateria') != 'undefined'){
                    $psicometrico = $this->input->post('psi_bateria');
                    $tipo_psicometrico = 1;
                }
                else{
                    if($this->input->post('psi_prueba') != 'undefined'){
                        $psicometrico = $this->input->post('psi_prueba');
                        $tipo_psicometrico = 2;
                    }
                }
            }
            else{
                $tipo_psicometrico = 0;
                $psicometrico = 0;
            }
            if($this->input->post('buro') == 'on'){
                if($this->input->post('buro_bu') != 'undefined'){
                    $buro = $this->input->post('buro_bu');
                }
                else{
                    $buro = 0;
                }
            }
            else{
                $buro = 0;
            }
            //var_dump($buro);
            $pruebas = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $nuevo_candidato,
                'id_cliente' => $this->input->post('id_cliente'),
                'socioeconomico' => $socio,
                'tipo_antidoping' => $tipo_antidoping,
                'antidoping' => $antidoping,
                'tipo_psicometrico' => $tipo_psicometrico,
                'psicometrico' => $psicometrico,
                'medico' => $medico,
                'buro_credito' => $buro,
                'sociolaboral' => $sociolaboral,
                'otro_requerimiento' => $this->input->post('otro')
            );
            $this->candidato_model->insertPruebasCandidato($pruebas);

            echo $salida = 1;
            
        }
    }
    function viewSolicitudesCandidato(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $pruebas = $this->candidato_model->getPruebasCandidato($id_candidato);
        if(isset($pruebas)){
            $salida .= ($pruebas->socioeconomico == 1)? "- <b>Socioeconomico</b><br>":"";
            if($pruebas->tipo_antidoping == 1){
                $paq = $this->candidato_model->getPaqueteAntidoping($pruebas->antidoping);
                $salida .= '- <b>Antidoping:</b> '.$paq->nombre.'<br>';
            }
            if($pruebas->tipo_antidoping == 2){
                $salida .= '- <b>Antidoping:</b> <br>';
                $aux = explode(',', $pruebas->antidoping);
                for($i = 0; $i < count($aux); $i++){
                    $sust = $this->candidato_model->getSustanciaAntidoping($aux[$i]);
                    $salida .= $sust->abreviatura."<br>";
                }
            }
            if($pruebas->tipo_psicometrico == 1){
                $psi = $this->candidato_model->getBateria($pruebas->psicometrico);
                $salida .= '- <b>Psicométrico:</b> '.$psi->nombre.'<br>';
            }
            if($pruebas->tipo_psicometrico == 2){
                $salida .= '- <b>Psicométrico:</b> <br>';
                $aux = explode(',', $pruebas->antidoping);
                for($i = 0; $i < count($aux); $i++){
                    $pru = $this->candidato_model->getPruebasPsicometrico($aux[$i]);
                    $salida .= $pru->abreviatura."<br>";
                }
            }
            $salida .= ($pruebas->medico == 1)? "- <b>Médico</b><br>":"";
            if($pruebas->buro_credito != 0){
                $buro = $this->candidato_model->getBuroCredito($pruebas->buro_credito);
                $salida .= '- <b>Buró de crédito:</b> '.$buro->nombre.'<br>';
            }
            $salida .= ($pruebas->sociolaboral == 1)? "- <b>Sociolaboral</b><br>":"";
            echo $salida;
        }
        else{
            echo $salida;
        }
    }
    function registrarVisita(){     
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_usuario = $this->session->userdata('id');
        $cadena = $this->input->post('data');
        parse_str($cadena, $dato);   
        $aux = explode('/', $dato['v_fecha_visita']);
        $fecha_visita = $aux[2].'-'.$aux[1].'-'.$aux[0];
        $existe = $this->candidato_model->existeVisita($dato['id_candidato']);
        if($existe > 0){
            $visita = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'fecha_visita' => $fecha_visita,
                'hora_inicio' => $dato['v_hora_inicial'],
                'hora_fin' => $dato['v_hora_final'],
                'calle' => $dato['v_calle'],
                'exterior' => $dato['v_exterior'],
                'interior' => $dato['v_interior'],
                'colonia' => $dato['v_colonia'],
                'id_estado' => $dato['v_estado'],
                'id_municipio' => $dato['v_municipio'],
                'cp' => $dato['v_cp'],
                'celular' => $dato['v_celular'],
                'telefono_casa' => $dato['v_tel_casa'],
                'telefono_otro' => $dato['v_tel_otro']
            );
            $this->candidato_model->updateVisita($visita, $dato['id_candidato']);
        }
        else{
            $visita = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_cliente' => $dato['id_cliente'],
                'id_candidato' => $dato['id_candidato'],
                'fecha_visita' => $fecha_visita,
                'hora_inicio' => $dato['v_hora_inicial'],
                'hora_fin' => $dato['v_hora_final'],
                'calle' => $dato['v_calle'],
                'exterior' => $dato['v_exterior'],
                'interior' => $dato['v_interior'],
                'colonia' => $dato['v_colonia'],
                'id_estado' => $dato['v_estado'],
                'id_municipio' => $dato['v_municipio'],
                'cp' => $dato['v_cp'],
                'celular' => $dato['v_celular'],
                'telefono_casa' => $dato['v_tel_casa'],
                'telefono_otro' => $dato['v_tel_otro']
            );
            $this->candidato_model->addVisita($visita);
        }
        $candidato = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'calle' => $dato['v_calle'],
            'exterior' => $dato['v_exterior'],
            'interior' => $dato['v_interior'],
            'colonia' => $dato['v_colonia'],
            'id_estado' => $dato['v_estado'],
            'id_municipio' => $dato['v_municipio'],
            'cp' => $dato['v_cp'],
            'celular' => $dato['v_celular'],
            'telefono_casa' => $dato['v_tel_casa'],
            'telefono_otro' => $dato['v_tel_otro']
        );
        $this->candidato_model->saveCandidato($candidato, $dato['id_candidato']);
        echo $salida = 1;
    }
    
    function cancel(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_usuario_cliente = $this->session->userdata('id');
        $id_candidato = $_POST['id_candidato'];
        $motivo = $_POST['motivo'];
        $this->candidato_model->cancel($id_candidato, $date, $id_usuario_cliente);
        $this->candidato_model->motivoCancelar($id_candidato, $motivo, $date);
    }
    function delete(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_usuario_cliente = $this->session->userdata('id');
        $id_candidato = $_POST['id_candidato'];
        $motivo = $_POST['motivo'];
        $this->candidato_model->delete($id_candidato, $date, $id_usuario_cliente);
        $this->candidato_model->motivoEliminar($id_candidato, $motivo, $date);
    }
    //Obtiene los status penales, laborales y de estaudios del candidato para vista del cliente
    function viewStatus(){
        $id_candidato = $_POST['id_candidato'];
        $salida = '<div class="row">';
        $salida = '<p class="text-center "><strong>Criminal Records</strong></p>';
        $data['s_penales'] = $this->candidato_model->checkEstatusPenales($id_candidato);
        if($data['s_penales']){
            foreach($data['s_penales'] as $p){
                $aux = explode('-', $p->fecha);
                $f1 = $aux[1].'/'.$aux[2].'/'.$aux[0];
                $salida .= '<div class="col-md-12">';
                $salida .= '<p>Date: '.$f1.'</p><p>Comment: '.$p->comentarios.'</p></div>';
                $p_terminado = $p->finalizado;
            }
            $salida .= ($p_terminado == 1)? '<p class="text-center"><b>Completed</b></p><hr>' : '<p class="text-center"><b>In process by analyst</b></p><hr>';
            $salida .= '</div>';
            $salida .= '</div>';
        }
        else{
            $salida .= '<p class="text-center"><b>In process by analyst</b></p><br><hr>';
            $salida .= '</div>';
        }

        $salida .= '<p class="text-center "><strong>Laboral References</strong></p>';
        $data['s_laborales'] = $this->candidato_model->checkEstatusLaborales($id_candidato);
        if($data['s_laborales']){
            foreach($data['s_laborales'] as $l){
                $aux = explode('-', $l->fecha);
                $f2 = $aux[1].'/'.$aux[2].'/'.$aux[0];
                $salida .= '<div class="col-md-12">';
                $salida .= '<p>Date: '.$f2.'</p><p>Comment: '.$l->comentarios.'</p></div>';
                $l_terminado = $l->finalizado;
            }
            $salida .= ($l_terminado == 1)? '<p class="text-center"><b>Completed</b></p><hr>' : '<p class="text-center"><b>In process by analyst</b></p><hr>';
            $salida .= '</div>';
        }
        else{
            $salida .= '<p class="text-center"><b>In process by analyst</b></p><br><hr>';
            $salida .= '</div>';
        }

        $salida .= '<p class="text-center "><strong>Studies Record</strong></p>';
        $data['s_estudios'] = $this->candidato_model->checkEstatusEstudios($id_candidato);
        if($data['s_estudios']){
            foreach($data['s_estudios'] as $e){
                $aux = explode('-', $e->fecha);
                $f3 = $aux[1].'/'.$aux[2].'/'.$aux[0];
                $salida .= '<div class="col-md-12">';
                $salida .= '<p>Date: '.$f3.'</p><p>Comment: '.$e->comentarios.'</p></div>';
                $e_terminado = $e->finalizado;
            }
            $salida .= ($e_terminado == 1)? '<p class="text-center"><b>Completed</b></p>' : '<p class="text-center"><b>In process by analyst</b></p>';
            $salida .= '</div>';
        }
        else{
            $salida .= '<p class="text-center"><b>In process by analyst</b></p><br>';
            $salida .= '</div>';
        }
        $salida .= '</div>';

        echo $salida;
    }
    function getCancelacion(){
        $id_candidato = $_POST['id_candidato'];
        $candidato = $this->candidato_model->getCancelacion($id_candidato);
        $salida = $candidato->creacion.'##'.$candidato->motivo;
        echo $salida;
    }
    function getEliminacion(){
        $id_candidato = $_POST['id_candidato'];
        $candidato = $this->candidato_model->getEliminacion($id_candidato);
        $salida = $candidato->creacion.'##'.$candidato->motivo;
        echo $salida;
    }
    
    
    
    function checkLlamadas(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['estatus'] = $this->candidato_model->checkLlamadas($id_candidato);
        if($data['estatus']){
            foreach($data['estatus'] as $l){
                $parte = explode(' ', $l->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= '<div class="row">
                                <div class="col-md-3">
                                    <p class="text-center"><b>Fecha</b></p>
                                    <p class="text-center">'.$fecha_estatus.'</p>
                                </div>
                                <div class="col-md-9">
                                    <label>Comentario / Estatus</label>
                                    <p>'.$l->comentarios.'</p>
                                </div>
                            </div>';
                $id_status = $l->idLlamada;
                $terminado = $l->finalizado;
            }
            echo $salida.'@@'.$id_status.'@@'.$terminado;
        }
        else{
            echo $salida = 0;
        }
    }
    function createEstatusLlamada(){
        $id_candidato = $_POST['id_candidato'];
        $id_llamada = $_POST['id_llamada'];
        $comentario = $_POST['comentario'];
        $id_usuario = $this->session->userdata('id');
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $salida = "";
        if($id_llamada == 0){
            $nueva_llamada = $this->candidato_model->createEstatusLlamada($id_candidato, $id_usuario, $date);
            $this->candidato_model->createDetalleEstatusLlamada($nueva_llamada, $date, $comentario);
            $data['estatus'] = $this->candidato_model->checkLlamadas($id_candidato);
            foreach($data['estatus'] as $ref){
                $parte = explode(' ', $ref->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= '<div class="row">
                                <div class="col-md-3">
                                    <p class="text-center"><b>Fecha</b></p>
                                    <p class="text-center">'.$fecha_estatus.'</p>
                                </div>
                                <div class="col-md-9">
                                    <label>Comentario / Estatus</label>
                                    <p>'.$ref->comentarios.'</p>
                                </div>
                            </div>';
                $id_llamada = $ref->idLlamada;
            }
            echo $salida.'@@'.$id_llamada;
        }
        else{
            $this->candidato_model->createDetalleEstatusLlamada($id_llamada, $date, $comentario);
            $data['estatus'] = $this->candidato_model->checkLlamadas($id_candidato);
            foreach($data['estatus'] as $ref){
                $parte = explode(' ', $ref->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= '<div class="row">
                                <div class="col-md-3">
                                    <p class="text-center"><b>Fecha</b></p>
                                    <p class="text-center">'.$fecha_estatus.'</p>
                                </div>
                                <div class="col-md-9">
                                    <label>Comentario / Estatus</label>
                                    <p>'.$ref->comentarios.'</p>
                                </div>
                            </div>';
            }
            echo $salida.'@@'.$id_llamada;
        }
    }
    function viewLlamadas(){
        $id_candidato = $_POST['id_candidato'];
        $id_cliente = $_POST['id_cliente'];
        $res = $this->cliente_model->checkIngles($id_cliente);
        $salida = '<div class="row">';
        $salida .= '<div class="col-md-12">';
        $data['llamadas'] = $this->candidato_model->checkLlamadas($id_candidato);
        if($data['llamadas']){
            foreach($data['llamadas'] as $row){
                $parte = explode(' ', $row->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_ingles = $aux[1].'/'.$aux[2].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $fecha_espanol = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= ($res->ingles == 1)?  '<p style="padding-right: 5px;"><b>Date: </b>'.$fecha_ingles.'</p><p><b>Comment: </b>'.$row->comentarios.'</p><br>' : '<p style="padding-right: 5px;"><b>Fecha: </b>'.$fecha_espanol.'</p><p><b>Comentario:</b> '.$row->comentarios.'</p><br>';
                $row_terminado = $row->status;
            }
            /*$txt1 = ($res->ingles == 1)? 'Completed':'Completado';
            $txt2 = ($res->ingles == 1)? 'In process by analyst':'En proceso por el analista';
            $salida .= ($row_terminado > 0)? '<p class="text-center"><b>'.$txt1.'</b></p>' : '<p class="text-center"><b>'.$txt2.'</b></p>';*/
            $salida .= '</div>';
        }
        else{
            $salida .= ($res->ingles == 1)? '<p class="text-center"><b>There are not calls phone to candidate</b></p><br>' : '<p class="text-center"><b>No hay llamadas al candidato</b></p><br>';
            $salida .= '</div>';
        }
        $salida .= '</div>';

        echo $salida;
    }
    
    
    function checkEmails(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['estatus'] = $this->candidato_model->checkEmails($id_candidato);
        if($data['estatus']){
            foreach($data['estatus'] as $l){
                $parte = explode(' ', $l->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= '<div class="row">
                                <div class="col-md-3">
                                    <p class="text-center"><b>Fecha</b></p>
                                    <p class="text-center">'.$fecha_estatus.'</p>
                                </div>
                                <div class="col-md-9">
                                    <label>Comentario / Estatus</label>
                                    <p>'.$l->comentarios.'</p>
                                </div>
                            </div>';
                $id_status = $l->idEmail;
                $terminado = $l->finalizado;
            }
            echo $salida.'@@'.$id_status.'@@'.$terminado;
        }
        else{
            echo $salida = 0;
        }
    }
    
    
    function createEstatusEmail(){
        $id_candidato = $_POST['id_candidato'];
        $id_email = $_POST['id_email'];
        $comentario = $_POST['comentario'];
        $id_usuario = $this->session->userdata('id');
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $salida = "";
        if($id_email == 0){
            $nuevo_email = $this->candidato_model->createEstatusEmail($id_candidato, $id_usuario, $date);
            $this->candidato_model->createDetalleEstatusEmail($nuevo_email, $date, $comentario);
            $data['estatus'] = $this->candidato_model->checkEmails($id_candidato);
            foreach($data['estatus'] as $ref){
                $parte = explode(' ', $ref->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= '<div class="row">
                                <div class="col-md-3">
                                    <p class="text-center"><b>Fecha</b></p>
                                    <p class="text-center">'.$fecha_estatus.'</p>
                                </div>
                                <div class="col-md-9">
                                    <label>Comentario / Estatus</label>
                                    <p>'.$ref->comentarios.'</p>
                                </div>
                            </div>';
                $id_email = $ref->idEmail;
            }
            echo $salida.'@@'.$id_email;
        }
        else{
            $this->candidato_model->createDetalleEstatusEmail($id_email, $date, $comentario);
            $data['estatus'] = $this->candidato_model->checkEmails($id_candidato);
            foreach($data['estatus'] as $ref){
                $parte = explode(' ', $ref->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_estatus = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= '<div class="row">
                                <div class="col-md-3">
                                    <p class="text-center"><b>Fecha</b></p>
                                    <p class="text-center">'.$fecha_estatus.'</p>
                                </div>
                                <div class="col-md-9">
                                    <label>Comentario / Estatus</label>
                                    <p>'.$ref->comentarios.'</p>
                                </div>
                            </div>';
            }
            echo $salida.'@@'.$id_email;
        }
    }
    function viewEmails(){
        $id_candidato = $_POST['id_candidato'];
        $id_cliente = $_POST['id_cliente'];
        $res = $this->cliente_model->checkIngles($id_cliente);
        $salida = '<div class="row">';
        $salida .= '<div class="col-md-12">';
        $data['emails'] = $this->candidato_model->checkEmails($id_candidato);
        if($data['emails']){
            foreach($data['emails'] as $row){
                $parte = explode(' ', $row->fecha);
                $aux = explode('-', $parte[0]);
                $h = explode(':', $parte[1]);
                $fecha_ingles = $aux[1].'/'.$aux[2].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $fecha_espanol = $aux[2].'/'.$aux[1].'/'.$aux[0].' '.$h[0].':'.$h[1];
                $salida .= ($res->ingles == 1)? '<p style="padding-right: 5px;"><b>Date:</b> '.$fecha_ingles.'</p><p><b>Comment: </b>'.$row->comentarios.'</p><br>' : '<p style="padding-right: 5px;"><b>Fecha:</b> '.$fecha_espanol.'</p><p><b>Comentario: </b>'.$row->comentarios.'</p><br>';
                $row_terminado = $row->status;
            }
            /*$txt1 = ($res->ingles == 1)? 'Completed':'Completado';
            $txt2 = ($res->ingles == 1)? 'In process by analyst':'En proceso por el analista';
            $salida .= ($row_terminado > 0)? '<p class="text-center"><b>'.$txt1.'</b></p>' : '<p class="text-center"><b>'.$txt2.'</b></p>';*/
            $salida .= '</div>';
        }
        else{
            $salida .= ($res->ingles == 1)? '<p class="text-center"><b>There are not emails to candidate</b></p><br>' : '<p class="text-center"><b>No hay emails al candidato</b></p><br>';
            $salida .= '</div>';
        }
        $salida .= '</div>';

        echo $salida;
    }
    
    
    function viewDocumentos(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['docs'] = $this->candidato_model->getDocumentos($id_candidato);
        if($data['docs']){
            $salida .= "<ul>";
            foreach($data['docs'] as $doc){
                $salida .= '<li><b><a href="'.base_url().'_docs/'.$doc->archivo.'" target="_blank">'.$doc->archivo.'</a></b> ('.$doc->tipo.')</li>';
            }
            $salida .= "</ul>";
            echo $salida;
        }
        else{
            echo $salida = 0;
        }        
    }
    
    function getPaqueteAntidoping(){
        $id_paq_antidoping = $_POST['antidoping'];
        $paq = $this->candidato_model->getPaqueteAntidoping($id_paq_antidoping);
        echo $paq->nombre;
    }
    function getSustanciasAntidoping(){
        $salida = "";
        $sustancias = $_POST['antidoping'];
        $aux = explode(',', $sustancias);
        for($i = 0; $i < count($aux); $i++){
            $sust = $this->candidato_model->getSustanciaAntidoping($aux[$i]);
            $salida .= $sust->abreviatura."<br>";
        }
        
        echo $salida;
    }
    function getBateria(){
        $id_bateria = $_POST['psicometrico'];
        $psi = $this->candidato_model->getBateria($id_bateria);
        echo $psi->nombre;
    }
    function getPruebasPsicometrico(){
        $salida = "";
        $pruebas = $_POST['psicometrico'];
        $aux = explode(',', $pruebas);
        for($i = 0; $i < count($aux); $i++){
            $pru = $this->candidato_model->getPruebasPsicometrico($aux[$i]);
            $salida .= $pru->abreviatura."<br>";
        }
        echo $salida;
    }
    function getBuroCredito(){
        $id_buro = $_POST['buro'];
        $buro = $this->candidato_model->getBuroCredito($id_buro);
        echo $buro->nombre;
    }
    function finalizarDocumentos(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $_POST['id_candidato'];
        $data['docs'] = $this->candidato_model->checkDocsCandidato($id_candidato);
        if($data['docs']){
            foreach($data['docs'] as $doc){
                $docs[] = $doc->id_tipo_documento;
            }
            if($this->session->userdata('proyecto') == 21){
                $ine = (in_array(3, $docs))? 1:"Upload your ID (IFE, INE or Passport)<br>";
                $penales = (in_array(12, $docs))? 1:"Upload your non-criminal background letter<br>";
                $estudios = (in_array(7, $docs) || in_array(10, $docs))? 1:"Upload your professional licence or studies certificate<br>";
                $aviso = (in_array(8, $docs))? 1:"Upload your non-disclosure agreement<br>";
                $semanas = (in_array(9, $docs))? 1:"Upload your IMSS report<br>";
                $dom = (in_array(2, $docs))? 1:"your current proof of address<br>";

                if($ine == 1 && $penales == 1 && $estudios == 1 && $aviso == 1 && $dom == 1){
                    $candidato = array(
                        "token" => "completo",
                        "fecha_documentos" => $date
                    );
                    $this->candidato_model->saveCandidato($candidato, $id_candidato);
                    echo $salida = 1;
                }
                else{
                    echo $ine."".$penales."".$estudios."".$aviso."".$semanas."".$dom;
                }
            }
            if($this->session->userdata('proyecto') == 26){
                $ine = (in_array(3, $docs))? 1:"Upload your ID (IFE, INE or Passport)<br>";
                $penales = (in_array(12, $docs))? 1:"Upload your non-criminal background letter<br>";
                //$estudios = (in_array(7, $docs) || in_array(10, $docs))? 1:"Upload your professional licence or studies certificate<br>";
                $aviso = (in_array(8, $docs))? 1:"Upload your non-disclosure agreement<br>";
                //$semanas = (in_array(9, $docs))? 1:"Upload your IMSS report<br>";
                
                if($ine == 1 && $penales == 1 && $aviso == 1){
                    $candidato = array(
                        "token" => "completo",
                        "fecha_documentos" => $date
                    );
                    $this->candidato_model->saveCandidato($candidato, $id_candidato);
                    echo $salida = 1;
                }
                else{
                    echo $ine."".$penales."".$aviso;
                }
                
            }
            if($this->session->userdata('proyecto') == 27){
                $ine = (in_array(3, $docs))? 1:"Upload your ID (IFE, INE or Passport)<br>";
                $penales = (in_array(12, $docs))? 1:"Upload your non-criminal background letter<br>";
                //$estudios = (in_array(7, $docs) || in_array(10, $docs))? 1:"Upload your professional licence or studies certificate<br>";
                $aviso = (in_array(8, $docs))? 1:"Upload your non-disclosure agreement<br>";
                $dom = (in_array(2, $docs))? 1:"your current proof of address<br>";
                
                if($ine == 1 && $penales == 1 && $aviso == 1 && $dom == 1){
                    $candidato = array(
                        "token" => "completo",
                        "fecha_documentos" => $date
                    );
                    $this->candidato_model->saveCandidato($candidato, $id_candidato);
                    echo $salida = 1;
                }
                else{
                    echo $ine."".$penales."".$aviso."".$dom;
                }
                
            }
            if($this->session->userdata('proyecto') == 35){
                $ine = (in_array(3, $docs))? 1:"Upload your ID (IFE, INE, Passport or Immigration ID)<br>";
                //$penales = (in_array(12, $docs))? 1:"Upload your non-criminal background letter<br>";
                $estudios = (in_array(7, $docs) || in_array(10, $docs))? 1:"Upload your professional licence or studies certificate<br>";
                $aviso = (in_array(8, $docs))? 1:"Upload your non-disclosure agreement<br>";
                //$semanas = (in_array(9, $docs))? 1:"Upload your IMSS report<br>";

                if($ine == 1 && $estudios == 1 && $aviso == 1){
                    $candidato = array(
                        "token" => "completo",
                        "fecha_documentos" => $date
                    );
                    $this->candidato_model->saveCandidato($candidato, $id_candidato);
                    echo $salida = 1;
                }
                else{
                    echo $ine."".$estudios."".$aviso;
                }
            }
            if($this->session->userdata('proyecto') != 26 && $this->session->userdata('proyecto') != 27 && $this->session->userdata('proyecto') != 21 && $this->session->userdata('proyecto') != 35 && $this->session->userdata('proyecto') != 150){
                $ine = (in_array(3, $docs))? 1:"Upload your ID (IFE, INE or Passport)<br>";
                $penales = (in_array(12, $docs))? 1:"Upload your non-criminal background letter<br>";
                $estudios = (in_array(7, $docs) || in_array(10, $docs))? 1:"Upload your professional licence or studies certificate<br>";
                $aviso = (in_array(8, $docs))? 1:"Upload your non-disclosure agreement<br>";
                $semanas = (in_array(9, $docs))? 1:"Upload your IMSS report<br>";

                if($ine == 1 && $penales == 1 && $estudios == 1 && $aviso == 1){
                    $candidato = array(
                        "token" => "completo",
                        "fecha_documentos" => $date
                    );
                    $this->candidato_model->saveCandidato($candidato, $id_candidato);
                    echo $salida = 1;
                }
                else{
                    echo $ine."".$penales."".$estudios."".$aviso."".$semanas;
                }
            }
            if($this->session->userdata('proyecto') == 150){
                $ine = (in_array(3, $docs))? 1:"Upload your ID<br>";
                $estudios = (in_array(7, $docs) || in_array(10, $docs))? 1:"Upload your professional licence or studies certificate<br>";
                $aviso = (in_array(8, $docs))? 1:"Upload the signed non-disclosure agreement<br>";
                $dom = (in_array(2, $docs))? 1:"your current proof of address<br>";

                if($ine == 1 && $estudios == 1 && $aviso == 1 && $dom == 1){
                    $candidato = array(
                        "token" => "completo",
                        "fecha_documentos" => $date
                    );
                    $this->candidato_model->saveCandidato($candidato, $id_candidato);
                    echo $salida = 1;
                }
                else{
                    echo $ine."".$estudios."".$aviso."".$dom;
                }
            }
        }
        else{
            echo $salida = 0;
        }

    }
    function getSubclientes(){
        $id_cliente = $_POST['id_cliente'];
        $data['subclientes'] = $this->candidato_model->getSublientes($id_cliente);
        $salida = "<option value=''>Selecciona</option>";
        if($data['subclientes']){
            foreach ($data['subclientes'] as $row){
                $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            $salida .= "<option value='0'>N/A</option>";
            echo $salida;
        }
    }
    /*----------------------------------------*/
    /*  Visitador
    /*----------------------------------------*/
    function getCandidatosVisitador(){
        $cand['recordsTotal'] = $this->candidato_model->getTotalCandidatosVisitador();
        $cand['recordsFiltered'] = $this->candidato_model->getTotalCandidatosVisitador();
        $cand['data'] = $this->candidato_model->getCandidatosVisitador();
        $this->output->set_output( json_encode( $cand ) );
    }
    /*----------------------------------------*/
    /*  TATA
    /*----------------------------------------*/
    function getDetalleCandidatoTata(){
        $idCandidato = $this->input->post('idCandidato');
        $salida = "";
        $dato = $this->candidato_model->getDetalleCandidatoTata($idCandidato);
        $salida .= '<table class="table table-striped">';
        $salida .= '<thead>';
        $salida .= '<tr>';
        $salida .= '<th scope="col">Candidato</th>';
        $salida .= '<th scope="col">Proyecto</th>';
        $salida .= '<th scope="col">Fecha de alta</th>';
        $salida .= '<th scope="col">Fecha final</th>';
        $salida .= '<th scope="col">SLA</th>';
        $salida .= '<th scope="col">Acciones</th>';
        $salida .= '</tr>';
        $salida .= '</thead>';
        $salida .= '<tbody>';
        $fecha_alta = fecha_sinhora_espanol_bd($dato->fecha_alta);
        $fecha_final = fecha_sinhora_espanol_bd($dato->fecha_final);
        if($dato->status_bgc == 1){
            $color = '<i class="fas fa-circle status_bgc1"></i> ';
        }
        if($dato->status_bgc == 2){
            $color = '<i class="fas fa-circle status_bgc2"></i> ';
        }
        if($dato->status_bgc == 3){
            $color = '<i class="fas fa-circle status_bgc3"></i> ';
        }
        $salida .= "<tr><th>".$color.$dato->candidato."</th><th>".$dato->proyecto."</th><th>".$fecha_alta."</th><th>".$fecha_final."</th><th>".$dato->tiempo." dia(s)</th><th><button class='btn btn-primary btn-sm' onclick='eliminarEstudio(".$dato->id.",".$dato->id_cliente.",\"".$dato->candidato."\")'>Eliminar</button></th>";
        $salida .= '</tbody>';
        $salida .= '</table>';
        echo $salida;
    }
    /*----------------------------------------*/
    /*  HCL
    /*----------------------------------------*/
    function addCandidatoIngles(){
        $this->form_validation->set_rules('nombre', 'Name', 'required|trim|callback_alpha_space_only_english');
        $this->form_validation->set_rules('paterno', 'First lastname', 'required|trim|callback_alpha_space_only_english');
        $this->form_validation->set_rules('materno', 'Second lastname', 'required|trim|callback_alpha_space_only_english');
        $this->form_validation->set_rules('correo', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('celular', 'Cell phone number', 'required|numeric|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('fijo', 'Home number', 'numeric|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('proyecto', 'Proyecto', 'required|numeric');
        /*if(empty($this->input->post('ine')) || $this->input->post('ine') == 'undefined'){

            $this->form_validation->set_rules('ine', 'ine o solicitud de empleo', 'callback_required_file');
        }*/
        $this->form_validation->set_message('required','El campo {field} es obligatorio');
        $this->form_validation->set_message('valid_email','El campo {field} debe ser un email válido');
        $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');
        $this->form_validation->set_message('min_length','El campo {field} no es válido');
        $this->form_validation->set_message('max_length','El campo {field} no es válido');
        $this->form_validation->set_message('less_than','El campo {field} no es válido');
        $this->form_validation->set_message('greater_than','El campo {field} no es válido');
        if($this->form_validation->run() != TRUE){ 
            echo validation_errors();
        }
        if($this->form_validation->run() == TRUE){
            $id_cliente = $this->session->userdata('idcliente');
            $nombre = strtoupper($this->input->post('nombre'));
            $paterno = strtoupper($this->input->post('paterno'));
            $materno = strtoupper($this->input->post('materno'));
            $cel = $this->input->post('celular');
            $tel = $this->input->post('fijo');
            $correo = strtolower($this->input->post('correo'));
            $fecha_nacimiento = $this->input->post('fecha_nacimiento');
            $proyecto = $this->input->post('proyecto');
            $existeCandidato = $this->candidato_model->repetidoCandidato($nombre, $paterno, $materno, $correo, $id_cliente);
            if($existeCandidato > 0){
                echo $res = 0;
            }
            else{
                date_default_timezone_set('America/Mexico_City');
                $date = date('Y-m-d H:i:s');
                $id_usuario = $this->session->userdata('id');
                $last = $this->candidato_model->lastIdCandidato();
                if(isset($_FILES['ine'])){
                    $nombre_ine = ($last->id + 1)."_".$this->input->post('nombre')."".$this->input->post('paterno')."_".$_FILES['ine']['name'];
                    $config['upload_path'] = './_docs/';  
                    $config['allowed_types'] = 'pdf|jpg|jpeg|png';
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $nombre_ine;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    // File upload
                    if($this->upload->do_upload('ine')){
                        $documento = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => ($last->id + 1),
                            'id_tipo_documento' => 3,
                            'archivo' => $nombre_ine
                        );
                        $this->candidato_model->insertCVCandidato($documento);
                        $data = $this->upload->data();
                    }
                }
                if($fecha_nacimiento != "" && $fecha_nacimiento != null){
                    $fnacimiento = fecha_ingles_bd($fecha_nacimiento);
                }
                else{
                    $fnacimiento = "";
                }
                
                $base = 'k*jJlrsH:cY]O^Z^/J2)Pz{)qz:+yCa]^+V0S98Zf$sV[c@hKKG07Q{utg%OlODS';
                $aux = substr( md5(microtime()), 1, 8);
                $token = md5($aux.$base);
                $data = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario_cliente' => $id_usuario,
                    'fecha_alta' => $date,
                    'nombre' => $nombre,
                    'paterno' => $paterno,
                    'materno' => $materno,
                    'correo' => $correo,
                    'fecha_nacimiento' => $fnacimiento,
                    'token' => $token,
                    'id_cliente' => $id_cliente,
                    'celular' => $cel,
                    'telefono_casa' => $tel,
                    'id_proyecto' => $proyecto
                );
                $this->candidato_model->nuevoCandidato($data);

                $doping = $this->candidato_model->getPaqueteAntidopingCandidato($id_cliente, $proyecto);
                $pruebas = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario_cliente' => $id_usuario,
                    'id_candidato' => ($last->id + 1),
                    'id_cliente' => $id_cliente,
                    'socioeconomico' => 1,
                    'tipo_antidoping' => 1,
                    'antidoping' => $doping->id_antidoping_paquete
                    
                );
                $this->candidato_model->insertPruebasCandidato($pruebas);

                $from = $this->config->item('smtp_user');
                $to = $correo;
                $subject = strtolower($this->session->userdata('cliente'))." - credentials for register form";
                $datos['password'] = $aux;
                $datos['cliente'] = strtoupper($this->session->userdata('cliente'));
                $datos['email'] = $correo;
                $message = $this->load->view('login/mail_view',$datos,TRUE);
                $this->load->library('phpmailer_lib');
                $mail = $this->phpmailer_lib->load();
                $mail->isSMTP();
                $mail->Host     = 'rodi.com.mx';
                $mail->SMTPAuth = true;
                $mail->Username = 'rodicontrol@rodi.com.mx';
                $mail->Password = 'RRodi#2019@';
                $mail->SMTPSecure = 'ssl';
                $mail->Port     = 465;
                
                $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                $mail->addAddress($to);
                $mail->Subject = $subject;
                $mail->isHTML(true);
                $mailContent = $message;
                $mail->Body = $mailContent;

                if(!$mail->send()){
                    //echo 'Message could not be sent.';
                    //echo 'Mailer Error: ' . $mail->ErrorInfo;
                    echo "No sent@@".$aux;
                }else{
                    //echo 'Message has been sent';
                    echo "Sent@@".$aux;
                }
                
            }
        }
    }
    function candidateHCLStandard(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->session->userdata('id');
       
        $cadena = $this->input->post('datos');
        parse_str($cadena, $personal);
        $cadena3 = $this->input->post('complementos');
        parse_str($cadena3, $dato);
        
        $fecha = fecha_ingles_bd($personal['fecha_nacimiento']);
        $edad = $this->calculaEdad($fecha);
        $candidato = array(
            'fecha_contestado' => $date,
            //'token' => 'completo',
            'edicion' => $date,
            'fecha_nacimiento' => $fecha,
            'edad' => $edad,
            'puesto' => $personal['puesto'],
            'nacionalidad' => $personal['nacionalidad'],
            'genero' => $personal['genero'],
            'id_grado_estudio' => $dato['estudios'],
            'estudios_periodo' => $dato['estudios_periodo'],
            'estudios_escuela' => $dato['estudios_escuela'],
            'estudios_ciudad' => $dato['estudios_ciudad'],
            'estudios_certificado' => $dato['estudios_certificado'],
            'calle' => $personal['calle'],
            'exterior' => $personal['exterior'],
            'interior' => $personal['interior'],
            'colonia' => $personal['colonia'],
            'id_estado' => $personal['estado'],
            'id_municipio' => $personal['municipio'],
            'cp' => $personal['cp'],
            'id_estado_civil' => $personal['civil'],
            'celular' => $personal['telefono'],
            'telefono_casa' => $personal['tel_casa'],
            'telefono_otro' => $personal['tel_otro'],
            'comentario' => $dato['obs'],
            'trabajo_inactivo' => $dato['trabajo_inactivo'],
            'status' => 1
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        
        if($_POST['trabajos'] != ""){
            $data_trabajo = "";
            $trab = explode("@@", $_POST['trabajos']);
            for($i = 0; $i < count($trab); $i++){
                $aux = explode("__", $trab[$i]);
                if($trab[$i] != ""){
                    $fentrada = fecha_ingles_bd($aux[2]);
                    $fsalida = fecha_ingles_bd($aux[3]);
                    $data_trabajo = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_candidato' => $id_candidato,
                        'empresa' => ucwords(strtolower($aux[0])),
                        'direccion' => ucwords(strtolower($aux[1])),
                        'fecha_entrada' => $fentrada,
                        'fecha_salida' => $fsalida,
                        'telefono' => $aux[4],
                        'puesto1' => ucwords(strtolower($aux[5])),
                        'puesto2' => ucwords(strtolower($aux[6])),
                        'salario1' => $aux[7],
                        'salario2' => $aux[8],
                        'jefe_nombre' => ucwords(strtolower($aux[9])),
                        'jefe_correo' => strtolower($aux[10]),
                        'jefe_puesto' => ucwords(strtolower($aux[11])),
                        'causa_separacion' => $aux[12]
                    );
                    $this->candidato_model->saveRefLab($data_trabajo);
                }
            }
        }
        if(isset($_POST['doms'])){
            $data_dom = "";
            $dom = explode("@@", $_POST['doms']);
            for($i = 0; $i < count($dom); $i++){
                $aux = explode("__", $dom[$i]);
                if($dom[$i] != ""){
                    if($i == 0){
                        $data_dom = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'periodo' => $aux[0],
                            'causa' => $aux[1],
                            'calle' => $personal['calle'],
                            'exterior' => $personal['exterior'],
                            'interior' => $personal['interior'],
                            'colonia' => $personal['colonia'],
                            'id_estado' => $personal['estado'],
                            'id_municipio' => $personal['municipio'],
                            'cp' => $personal['cp']
                        );
                        $this->candidato_model->saveDomicilio($data_dom);
                    }
                    else{
                        $data_dom = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'periodo' => $aux[0],
                            'causa' => $aux[1],
                            'calle' => $aux[2],
                            'exterior' => $aux[3],
                            'interior' => $aux[4],
                            'colonia' => $aux[5],
                            'id_estado' => $aux[6],
                            'id_municipio' => $aux[7],
                            'cp' => $aux[8]
                        );
                        $this->candidato_model->saveDomicilio($data_dom);
                    }
                }
            }
        }
        echo $hecho = 1;
    }
    function formHCLInternational(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->session->userdata('id');
       
        $cadena = $this->input->post('datos');
        parse_str($cadena, $personal);
        $cadena3 = $this->input->post('complementos');
        parse_str($cadena3, $dato);
        
        $fecha = fecha_espanol_bd($personal['fecha_nacimiento']);
        $edad = $this->calculaEdad($fecha);
        $candidato = array(
            'fecha_contestado' => $date,
            'edicion' => $date,
            'fecha_nacimiento' => $fecha,
            'edad' => $edad,
            'puesto' => $personal['puesto'],
            'nacionalidad' => $personal['nacionalidad'],
            'genero' => $personal['genero'],
            'id_grado_estudio' => $dato['estudios'],
            'estudios_periodo' => $dato['estudios_periodo'],
            'estudios_escuela' => $dato['estudios_escuela'],
            'estudios_ciudad' => $dato['estudios_ciudad'],
            'estudios_certificado' => $dato['estudios_certificado'],
            'domicilio_internacional' => $personal['domicilio'],
            'pais' => $personal['pais'],
            'id_estado_civil' => $personal['civil'],
            'celular' => $personal['telefono'],
            'telefono_casa' => $personal['tel_casa'],
            'telefono_otro' => $personal['tel_otro'],
            'comentario' => $dato['obs'],
            'trabajo_inactivo' => $dato['trabajo_inactivo'],
            'status' => 1
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);

        $refpro1 = array(
            'creacion' => $date,
            'id_candidato' => $id_candidato,
            'numero' => 1,
            'nombre' => $dato['refpro1_nombre'],
            'telefono' => $dato['refpro1_telefono'],
            'tiempo_conocerlo' => $dato['refpro1_tiempo'],
            'donde_conocerlo' => $dato['refpro1_conocido'],
            'puesto' => $dato['refpro1_puesto']
        );
        $this->candidato_model->saveRefProfesional($refpro1);
        $refpro2 = array(
            'creacion' => $date,
            'id_candidato' => $id_candidato,
            'numero' => 2,
            'nombre' => $dato['refpro2_nombre'],
            'telefono' => $dato['refpro2_telefono'],
            'tiempo_conocerlo' => $dato['refpro2_tiempo'],
            'donde_conocerlo' => $dato['refpro2_conocido'],
            'puesto' => $dato['refpro2_puesto']
        );
        $this->candidato_model->saveRefProfesional($refpro2);
        
        if($_POST['trabajos'] != ""){
            $data_trabajo = "";
            $trab = explode("@@", $_POST['trabajos']);
            for($i = 0; $i < count($trab); $i++){
                $aux = explode("__", $trab[$i]);
                if($trab[$i] != ""){
                    $fentrada = fecha_espanol_bd($aux[2]);
                    $fsalida = fecha_espanol_bd($aux[3]);
                    $data_trabajo = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_candidato' => $id_candidato,
                        'empresa' => ucwords(strtolower($aux[0])),
                        'direccion' => ucwords(strtolower($aux[1])),
                        'fecha_entrada' => $fentrada,
                        'fecha_salida' => $fsalida,
                        'telefono' => $aux[4],
                        'puesto1' => ucwords(strtolower($aux[5])),
                        'puesto2' => ucwords(strtolower($aux[6])),
                        'salario1' => $aux[7],
                        'salario2' => $aux[8],
                        'jefe_nombre' => ucwords(strtolower($aux[9])),
                        'jefe_correo' => strtolower($aux[10]),
                        'jefe_puesto' => ucwords(strtolower($aux[11])),
                        'causa_separacion' => $aux[12]
                    );
                    $this->candidato_model->saveRefLab($data_trabajo);
                }
            }
        }
        if(isset($_POST['doms'])){
            $data_dom = "";
            $dom = explode("@@", $_POST['doms']);
            for($i = 0; $i < count($dom); $i++){
                $aux = explode("__", $dom[$i]);
                if($dom[$i] != ""){
                    if($i == 0){
                        $data_dom = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'periodo' => $aux[0],
                            'causa' => $aux[1],
                            'domicilio_internacional' => $personal['domicilio'],
                            'pais' => $personal['pais']
                        );
                        $this->candidato_model->saveDomicilio($data_dom);
                    }
                    else{
                        $data_dom = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'periodo' => $aux[0],
                            'causa' => $aux[1],
                            'domicilio_internacional' => $aux[2],
                            'pais' => $aux[3]
                        );
                        $this->candidato_model->saveDomicilio($data_dom);
                    }
                }
            }
        }
        echo $hecho = 1;
    }
    
    function createHCLInternalPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_internal_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599<br><br>4-EST-001.Rev. 01 <br>Fecha de Rev. 05/06/2020</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    
    function createHCLStandardPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_standard_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599<br><br>4-EST-001.Rev. 01 <br>Fecha de Rev. 05/06/2020</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    function createHCLPGPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        //$data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_pg_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    function createHCLUSAAPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['domicilios'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
        $data['ver_domicilios'] = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_usaa_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    
    
    function createHCLExxonPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            //$id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['domicilios'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
        $data['ver_domicilios'] = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_exxon_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    function createHCLCitiPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['domicilios'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
        $data['ver_domicilios'] = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_citi_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    function createHCLBectonPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_becton_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    function createHCLNokiaPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_nokia_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    function createHCLSempraPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPDF'];
        $data['datos'] = $this->candidato_model->getDatosCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            $fform = $row->fecha_contestado;
            $fdocs = $row->fecha_documentos;
            $fbgc = $row->fecha_bgc;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $proyecto = $row->proyecto;
            $id_doping = $row->idDoping;
        }
        $fecha_bgc = DateTime::createFromFormat('Y-m-d H:i:s', $fbgc);
        $fecha_bgc = $fecha_bgc->format('F d, Y');
        $f_alta = $this->formatoFecha($f);
        $fform = $this->formatoFecha($fform);
        $fdocs = $this->formatoFecha($fdocs);
        $fbgc = $this->formatoFecha($fbgc);
        $hoy = $this->formatoFecha($hoy);
        $data['bgc'] = $this->candidato_model->getBGCCandidato($id_candidato);
        //$data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['global_searches'] = $this->candidato_model->getGlobalSearchesCandidato($id_candidato);
        $data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        $data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        $data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        $data['fecha_ver_documentos'] = $this->candidato_model->getFechaVerificacionDocumentos($id_candidato);
        $data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        $data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliaresCandidato($id_candidato);
        $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ver_mayor_estudio'] = $this->candidato_model->getMayorEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getReferencias($id_candidato);
        $data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        $data['gaps'] = $this->candidato_model->checkGaps($id_candidato);
        $data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        $data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        $data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['checklist'] = $this->candidato_model->getVerificacionChecklist($id_candidato);
        $data['domicilios'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
        $data['ver_domicilios'] = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
        $data['cliente'] = $cliente;
        $data['proyecto'] = $proyecto;
        $data['fecha_bgc'] = $fecha_bgc;

        $doping = $this->doping_model->getDatosDoping($id_doping);
        $data['doping'] = $doping;
        //$data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        $html = $this->load->view('pdfs/hcl_sempra_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Request Date: '.$f_alta.'<br>Release Date: '.$fbgc.'</div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Background_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    function getReferenciasLaborales(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $cont = 1;
        $data['referencias'] = $this->candidato_model->getReferencias($id_candidato);
        if($data['referencias']){
            foreach($data['referencias'] as $ref){
                $fi = fecha_sinhora_ingles_front($ref->fecha_entrada);
                $ff = fecha_sinhora_ingles_front($ref->fecha_salida);
                $salida .= '<div class="box-header text-center tituloSubseccion">
                                <p class="box-title " id="titulo_reflab1"><strong>  Reference #'.$cont.' </strong><hr></p>
                                <input type="hidden" id="idreflab'.$cont.'" value="'.$ref->id.'">
                            </div>
                            <p class="tituloSubseccion text-center"></p>
                            <div class="col-md-6">
                                <p class="tituloSubseccion">Candidate</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Company: </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_empresa">'.$ref->empresa.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Address: </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_direccion">'.$ref->direccion.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Entry Date: </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_entrada">'.$fi.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Exit Date: </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_salida">'.$ff.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Phone: </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_telefono">'.$ref->telefono.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Initial Job Position </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_puesto1">'.$ref->puesto1.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Last Job Position </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_puesto2">'.$ref->puesto2.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Initial Salary </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_salario1">$'.$ref->salario1.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Last Salary </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_salario2">$'.$ref->salario2.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Immediate Boss Name </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_bossnombre">'.$ref->jefe_nombre.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Immediate Boss Email </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_bosscorreo">'.$ref->jefe_correo.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Immediate Boss Position </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_bosspuesto">'.$ref->jefe_puesto.'</p>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Cause of Separation </label>
                                        <br>
                                    </div>
                                    <div class="col-md-9">
                                        <p id="reflab'.$cont.'_separacion">'.$ref->causa_separacion.'</p>
                                        <br>
                                    </div>
                                </div>
                            </div>';
                            
                            $vlaboral = $this->candidato_model->getVerificacionLaboral($cont, $id_candidato);
                            if(isset($vlaboral)){
                                            $vfi = fecha_sinhora_ingles_front($vlaboral->fecha_entrada);
                                            $vff = fecha_sinhora_ingles_front($vlaboral->fecha_salida);
                                $salida .= '<form id=update_vlaboral'.$cont.'>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <p class="tituloSubseccion text-center">Analist</p>
                                                    <div class="col-md-3">
                                                        <label>Company: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_empresa" id="verlab'.$cont.'_empresa" value="'.$vlaboral->empresa.'">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Address: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control reflab1_obligado" name="verlab'.$cont.'_direccion" id="verlab'.$cont.'_direccion" value="'.$vlaboral->direccion.'">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Entry Date: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_lectura verlab'.$cont.'_obligado fecha_reflab" name="verlab'.$cont.'_entrada" id="verlab'.$cont.'_entrada" value="'.$vfi.'" readonly>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Exit Date: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_lectura verlab'.$cont.'_obligado fecha_reflab" name="verlab'.$cont.'_salida" id="verlab'.$cont.'_salida" value="'.$vff.'" readonly>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Phone: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_numeros verlab'.$cont.'_obligado" name="verlab'.$cont.'_telefono" id="verlab'.$cont.'_telefono" value="'.$vlaboral->telefono.'" maxlength="10">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Initial Job Position </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_puesto1" id="verlab'.$cont.'_puesto1" value="'.$vlaboral->puesto1.'">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Last Job Position </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_puesto2" id="verlab'.$cont.'_puesto2" value="'.$vlaboral->puesto2.'">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Initial Salary </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_numeros verlab'.$cont.'_obligado" name="verlab'.$cont.'_salario1" id="verlab'.$cont.'_salario1" value="'.$vlaboral->salario1.'" maxlength="8">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Last Salary </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_numeros verlab'.$cont.'_obligado" name="verlab'.$cont.'_salario2" id="verlab'.$cont.'_salario2" value="'.$vlaboral->salario2.'" maxlength="8">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Immediate Boss Name </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_bossnombre" id="verlab'.$cont.'_bossnombre" value="'.$vlaboral->jefe_nombre.'">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Immediate Boss Email </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_bosscorreo" id="verlab'.$cont.'_bosscorreo" value="'.$vlaboral->jefe_correo.'">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Immediate Boss Position </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_bosspuesto" id="verlab'.$cont.'_bosspuesto" value="'.$vlaboral->jefe_puesto.'">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Cause of Separation </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_separacion" id="verlab'.$cont.'_separacion" value="'.$vlaboral->causa_separacion.'">
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <label>Notes *</label>
                                                    <textarea class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_notas" id="verlab'.$cont.'_notas" rows="3">'.$vlaboral->notas.'</textarea>
                                                </div>
                                            </div>
                                            <br><p class="tituloSubseccion text-center">Candidate Performance'.$cont.'</p><br>
                                            <div class="row">
                                                <div class="col-md-4 col-md-offset-4">
                                                    <label for="aplicar_todo'.$cont.'">Apply to all</label>
                                                    <select id="aplicar_todo'.$cont.'" class="form-control">
                                                        <option value="-1">Select</option>
                                                        <option value="0">Not provided</option>
                                                        <option value="1">Excellent</option>
                                                        <option value="2">Good</option>
                                                        <option value="3">Regular</option>
                                                        <option value="4">Bad</option>
                                                        <option value="5">Very Bad</option>
                                                    </select>
                                                    <br><br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_responsabilidad">Responsability *</label>
                                                    <select name="verlab'.$cont.'_responsabilidad" id="verlab'.$cont.'_responsabilidad" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                        <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_iniciativa">Initiative *</label>
                                                    <select name="verlab'.$cont.'_iniciativa" id="verlab'.$cont.'_iniciativa" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_eficiencia">Work efficiency *</label>
                                                    <select name="verlab'.$cont.'_eficiencia" id="verlab'.$cont.'_eficiencia" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_disciplina">Discipline *</label>
                                                    <select name="verlab'.$cont.'_disciplina" id="verlab'.$cont.'_disciplina" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_puntualidad">Punctuality and assistance *</label>
                                                    <select name="verlab'.$cont.'_puntualidad" id="verlab'.$cont.'_puntualidad" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_limpieza">Cleanliness and order *</label>
                                                    <select name="verlab'.$cont.'_limpieza" id="verlab'.$cont.'_limpieza" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_estabilidad">Stability *</label>
                                                    <select name="verlab'.$cont.'_estabilidad" id="verlab'.$cont.'_estabilidad" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_emocional">Emotional Stability *</label>
                                                    <select name="verlab'.$cont.'_emocional" id="verlab'.$cont.'_emocional" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_honesto">Honesty *</label>
                                                    <select name="verlab'.$cont.'_honesto" id="verlab'.$cont.'_honesto" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_rendimiento">Performance *</label>
                                                    <select name="verlab'.$cont.'_rendimiento" id="verlab'.$cont.'_rendimiento" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="verlab'.$cont.'_actitud">Attitude with coworkers, bosses and subordinates *</label>
                                                    <select name="verlab'.$cont.'_actitud" id="verlab'.$cont.'_actitud" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="verlab'.$cont.'_recontratacion">In case of vacancy would you hire her/him again? *</label>
                                                    <select name="verlab'.$cont.'_recontratacion" id="verlab'.$cont.'_recontratacion" class="form-control verlab'.$cont.'_obligado">
                                                        <option value="">Select</option>
                                                        <option value="0">No</option>
                                                        <option value="1">Yes</option>
                                                    </select>
                                                    <br><br><br>
                                                </div>
                                                <div class="col-md-8">
                                                    <label>Why? *</label>
                                                    <textarea class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_motivo" id="verlab'.$cont.'_motivo" rows="3">'.$vlaboral->motivo_recontratacion.'</textarea>
                                                    <br><br><br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-md-offset-5">
                                                    <button type="button" class="btn btn-primary" onclick="updateVerificacionLaboral('.$vlaboral->id.','.$cont.')">Actualizar Verificación Laboral #'.$cont.'</button>
                                                    <br><br>
                                                </div>
                                            </div>
                                            </form>
                                            <script>
                                                $("#aplicar_todo'.$cont.'").change(function(){
                                                    var valor = $(this).val();
                                                    switch(valor){
                                                        case "-1":
                                                            $(".performance'.$cont.'").val("Not provided");
                                                            break;
                                                        case "0":
                                                            $(".performance'.$cont.'").val("Not provided");
                                                            break;
                                                        case "1":
                                                            $(".performance'.$cont.'").val("Excellent");
                                                            break;
                                                        case "2":
                                                            $(".performance'.$cont.'").val("Good");
                                                            break;
                                                        case "3":
                                                            $(".performance'.$cont.'").val("Regular");
                                                            break;
                                                        case "4":
                                                            $(".performance'.$cont.'").val("Bad");
                                                            break;
                                                        case "5":
                                                            $(".performance'.$cont.'").val("Very Bad");
                                                            break;
                                                    }
                                                });
                                                $("#verlab'.$cont.'_responsabilidad").val("'.$vlaboral->responsabilidad.'");
                                                $("#verlab'.$cont.'_iniciativa").val("'.$vlaboral->iniciativa.'");
                                                $("#verlab'.$cont.'_eficiencia").val("'.$vlaboral->eficiencia.'");
                                                $("#verlab'.$cont.'_disciplina").val("'.$vlaboral->disciplina.'");
                                                $("#verlab'.$cont.'_puntualidad").val("'.$vlaboral->puntualidad.'");
                                                $("#verlab'.$cont.'_limpieza").val("'.$vlaboral->limpieza.'");
                                                $("#verlab'.$cont.'_estabilidad").val("'.$vlaboral->estabilidad.'");
                                                $("#verlab'.$cont.'_emocional").val("'.$vlaboral->emocional.'");
                                                $("#verlab'.$cont.'_honesto").val("'.$vlaboral->honestidad.'");
                                                $("#verlab'.$cont.'_rendimiento").val("'.$vlaboral->rendimiento.'");
                                                $("#verlab'.$cont.'_actitud").val("'.$vlaboral->actitud.'");
                                                $("#verlab'.$cont.'_recontratacion").val("'.$vlaboral->recontratacion.'");
                                                $(".fecha_reflab").datetimepicker({minView: 2,format: "mm/dd/yyyy",startView: 4,autoclose: true,todayHighlight: true,forceParse: false});
                                            </script>';
                                            $cont++;
                            }
                            else{
                                
                                $salida .= '<form id=data_vlaboral'.$cont.'>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <p class="tituloSubseccion text-center">Analist</p>
                                                    <div class="col-md-3">
                                                        <label>Company: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_empresa" id="verlab'.$cont.'_empresa">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Address: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_direccion" id="verlab'.$cont.'_direccion">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Entry Date: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_lectura verlab'.$cont.'_obligado fecha_reflab" name="verlab'.$cont.'_entrada" id="verlab'.$cont.'_entrada" readonly>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Exit Date: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_lectura verlab'.$cont.'_obligado fecha_reflab" name="verlab'.$cont.'_salida" id="verlab'.$cont.'_salida" readonly>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Phone: </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_numeros verlab'.$cont.'_obligado" name="verlab'.$cont.'_telefono" id="verlab'.$cont.'_telefono" maxlength="10">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Initial Job Position </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_puesto1" id="verlab'.$cont.'_puesto1">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Last Job Position </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_puesto2" id="verlab'.$cont.'_puesto2">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Initial Salary </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_numeros verlab'.$cont.'_obligado" name="verlab'.$cont.'_salario1" id="verlab'.$cont.'_salario1" maxlength="8">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Last Salary </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control solo_numeros verlab'.$cont.'_obligado" name="verlab'.$cont.'_salario2" id="verlab'.$cont.'_salario2" maxlength="8">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Immediate Boss Name </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_bossnombre" id="verlab'.$cont.'_bossnombre">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Immediate Boss Email </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_bosscorreo" id="verlab'.$cont.'_bosscorreo">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Immediate Boss Position </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_bosspuesto" id="verlab'.$cont.'_bosspuesto">
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label>Cause of Separation </label>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_separacion" id="verlab'.$cont.'_separacion">
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                    <label>Notes *</label>
                                                    <textarea class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_notas" id="verlab'.$cont.'_notas" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <br><p class="tituloSubseccion text-center">Candidate Performance</p><br>
                                            <div class="row">
                                                <div class="col-md-4 col-md-offset-4">
                                                    <label for="aplicar_todo'.$cont.'">Apply to all</label>
                                                    <select id="aplicar_todo'.$cont.'" class="form-control">
                                                        <option value="-1">Select</option>
                                                        <option value="0">Not provided</option>
                                                        <option value="1">Excellent</option>
                                                        <option value="2">Good</option>
                                                        <option value="3">Regular</option>
                                                        <option value="4">Bad</option>
                                                        <option value="5">Very Bad</option>
                                                    </select>
                                                    <br><br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_responsabilidad">Responsability *</label>
                                                    <select name="verlab'.$cont.'_responsabilidad" id="verlab'.$cont.'_responsabilidad" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_iniciativa">Initiative *</label>
                                                    <select name="verlab'.$cont.'_iniciativa" id="verlab'.$cont.'_iniciativa" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_eficiencia">Work efficiency *</label>
                                                    <select name="verlab'.$cont.'_eficiencia" id="verlab'.$cont.'_eficiencia" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_disciplina">Discipline *</label>
                                                    <select name="verlab'.$cont.'_disciplina" id="verlab'.$cont.'_disciplina" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_puntualidad">Punctuality and assistance *</label>
                                                    <select name="verlab'.$cont.'_puntualidad" id="verlab'.$cont.'_puntualidad" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_limpieza">Cleanliness and order *</label>
                                                    <select name="verlab'.$cont.'_limpieza" id="verlab'.$cont.'_limpieza" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_estabilidad">Stability *</label>
                                                    <select name="verlab'.$cont.'_estabilidad" id="verlab'.$cont.'_estabilidad" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_emocional">Emotional Stability *</label>
                                                    <select name="verlab'.$cont.'_emocional" id="verlab'.$cont.'_emocional" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_honesto">Honesty *</label>
                                                    <select name="verlab'.$cont.'_honesto" id="verlab'.$cont.'_honesto" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="verlab'.$cont.'_rendimiento">Performance *</label>
                                                    <select name="verlab'.$cont.'_rendimiento" id="verlab'.$cont.'_rendimiento" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="verlab'.$cont.'_actitud">Attitude with coworkers, bosses and subordinates *</label>
                                                    <select name="verlab'.$cont.'_actitud" id="verlab'.$cont.'_actitud" class="form-control performance'.$cont.' verlab'.$cont.'_obligado">
                                                         <option value="Not provided">Not provided</option>
                                                        <option value="Excellent">Excellent</option>
                                                        <option value="Good">Good</option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Bad">Bad</option>
                                                        <option value="Very Bad">Very Bad</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="verlab'.$cont.'_recontratacion">In case of vacancy would you hire her/him again? *</label>
                                                    <select name="verlab'.$cont.'_recontratacion" id="verlab'.$cont.'_recontratacion" class="form-control verlab'.$cont.'_obligado">
                                                        <option value="">Select</option>
                                                        <option value="0">No</option>
                                                        <option value="1">Yes</option>
                                                    </select>
                                                    <br><br><br>
                                                </div>
                                                <div class="col-md-8">
                                                    <label>Why? *</label>
                                                    <textarea class="form-control verlab'.$cont.'_obligado" name="verlab'.$cont.'_motivo" id="verlab'.$cont.'_motivo" rows="3"></textarea>
                                                    <br><br><br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-md-offset-5">
                                                    <button type="button" class="btn btn-primary" onclick="saveVerificacionLaboral('.$cont.')">Guardar Verificación Laboral #'.$cont.'</button>
                                                    <br><br>
                                                </div>
                                            </div>
                                            </form>
                                            <script>
                                                $("#aplicar_todo'.$cont.'").change(function(){
                                                    var valor = $(this).val();
                                                    switch(valor){
                                                        case "-1":
                                                            $(".performance'.$cont.'").val("Not provided");
                                                            break;
                                                        case "0":
                                                            $(".performance'.$cont.'").val("Not provided");
                                                            break;
                                                        case "1":
                                                            $(".performance'.$cont.'").val("Excellent");
                                                            break;
                                                        case "2":
                                                            $(".performance'.$cont.'").val("Good");
                                                            break;
                                                        case "3":
                                                            $(".performance'.$cont.'").val("Regular");
                                                            break;
                                                        case "4":
                                                            $(".performance'.$cont.'").val("Bad");
                                                            break;
                                                        case "5":
                                                            $(".performance'.$cont.'").val("Very Bad");
                                                            break;
                                                    }
                                                });
                                                $(".fecha_reflab").datetimepicker({minView: 2,format: "mm/dd/yyyy",startView: 4,autoclose: true,todayHighlight: true,forceParse: false});
                                            </script>';
                                            $cont++;
                                
                            }
            }
            echo $salida;
        }
    }
    function insertVerificacionLaboral(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cont = $this->input->post('cont');
        $cadena = $this->input->post('verlab');
        parse_str($cadena, $ver);
        $id_candidato = $ver['id_candidato'];
        //$id_reflaboral = $ver['id_reflaboral'];
        $id_usuario = $this->session->userdata('id');

        //$this->candidato_model->cleanVerificacionLaboral($id_candidato, 1);
        $fentrada = fecha_ingles_bd($ver['verlab'.$cont.'_entrada']);
        $fsalida = fecha_ingles_bd($ver['verlab'.$cont.'_salida']);
        $verificacion_reflab = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'id_candidato' => $id_candidato,
            'numero_referencia' => $cont,
            'empresa' => $ver['verlab'.$cont.'_empresa'], 
            'direccion' => $ver['verlab'.$cont.'_direccion'],
            'fecha_entrada' => $fentrada, 
            'fecha_salida' => $fsalida,
            'telefono' => $ver['verlab'.$cont.'_telefono'],
            'puesto1' => $ver['verlab'.$cont.'_puesto1'], 
            'puesto2' => $ver['verlab'.$cont.'_puesto2'],
            'salario1' => $ver['verlab'.$cont.'_salario1'], 
            'salario2' => $ver['verlab'.$cont.'_salario2'], 
            'jefe_nombre' => $ver['verlab'.$cont.'_bossnombre'], 
            'jefe_correo' => $ver['verlab'.$cont.'_bosscorreo'],
            'jefe_puesto' => $ver['verlab'.$cont.'_bosspuesto'], 
            'causa_separacion' => $ver['verlab'.$cont.'_separacion'], 
            'notas' => $ver['verlab'.$cont.'_notas'], 
            'demanda' => 0, 
            'responsabilidad' => $ver['verlab'.$cont.'_responsabilidad'],
            'iniciativa' => $ver['verlab'.$cont.'_iniciativa'], 
            'eficiencia' => $ver['verlab'.$cont.'_eficiencia'], 
            'disciplina' => $ver['verlab'.$cont.'_disciplina'], 
            'puntualidad' => $ver['verlab'.$cont.'_puntualidad'],
            'limpieza' => $ver['verlab'.$cont.'_limpieza'], 
            'estabilidad' => $ver['verlab'.$cont.'_estabilidad'],
            'emocional' => $ver['verlab'.$cont.'_emocional'],
            'honestidad' => $ver['verlab'.$cont.'_honesto'],
            'rendimiento' => $ver['verlab'.$cont.'_rendimiento'],
            'actitud' => $ver['verlab'.$cont.'_actitud'],
            'recontratacion' => $ver['verlab'.$cont.'_recontratacion'],
            'motivo_recontratacion' => $ver['verlab'.$cont.'_motivo']
        );
        $this->candidato_model->saveVerificacionLaboral($verificacion_reflab);
        echo $salida = 1;
    }
    function updateVerificacionLaboral(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_verificacion = $this->input->post('id');
        $cont = $this->input->post('cont');
        $cadena = $this->input->post('verlab');
        parse_str($cadena, $ver);
        $id_candidato = $ver['id_candidato'];
        //$id_reflaboral = $ver['id_reflaboral'];
        $id_usuario = $this->session->userdata('id');

        //$this->candidato_model->cleanVerificacionLaboral($id_candidato, 1);
        $fentrada = fecha_ingles_bd($ver['verlab'.$cont.'_entrada']);
        $fsalida = fecha_ingles_bd($ver['verlab'.$cont.'_salida']);
        $verificacion_reflab = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'id_candidato' => $id_candidato,
            'empresa' => $ver['verlab'.$cont.'_empresa'], 
            'direccion' => $ver['verlab'.$cont.'_direccion'],
            'fecha_entrada' => $fentrada, 
            'fecha_salida' => $fsalida,
            'telefono' => $ver['verlab'.$cont.'_telefono'],
            'puesto1' => $ver['verlab'.$cont.'_puesto1'], 
            'puesto2' => $ver['verlab'.$cont.'_puesto2'],
            'salario1' => $ver['verlab'.$cont.'_salario1'], 
            'salario2' => $ver['verlab'.$cont.'_salario2'], 
            'jefe_nombre' => $ver['verlab'.$cont.'_bossnombre'], 
            'jefe_correo' => $ver['verlab'.$cont.'_bosscorreo'],
            'jefe_puesto' => $ver['verlab'.$cont.'_bosspuesto'], 
            'causa_separacion' => $ver['verlab'.$cont.'_separacion'], 
            'notas' => $ver['verlab'.$cont.'_notas'], 
            'responsabilidad' => $ver['verlab'.$cont.'_responsabilidad'],
            'iniciativa' => $ver['verlab'.$cont.'_iniciativa'], 
            'eficiencia' => $ver['verlab'.$cont.'_eficiencia'], 
            'disciplina' => $ver['verlab'.$cont.'_disciplina'], 
            'puntualidad' => $ver['verlab'.$cont.'_puntualidad'],
            'limpieza' => $ver['verlab'.$cont.'_limpieza'], 
            'estabilidad' => $ver['verlab'.$cont.'_estabilidad'],
            'emocional' => $ver['verlab'.$cont.'_emocional'],
            'honestidad' => $ver['verlab'.$cont.'_honesto'],
            'rendimiento' => $ver['verlab'.$cont.'_rendimiento'],
            'actitud' => $ver['verlab'.$cont.'_actitud'],
            'recontratacion' => $ver['verlab'.$cont.'_recontratacion'],
            'motivo_recontratacion' => $ver['verlab'.$cont.'_motivo']
        );
        $this->candidato_model->actualizaVerificacionLaboral($verificacion_reflab, $id_verificacion);
        echo $salida = 1;
    }
    function updateEstudiosHCL(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_estudios');
        parse_str($cadena, $estudio);
        $id_ver_estudios = $estudio['id_ver_estudios'];
        $id_candidato = $estudio['id_candidato'];
        $id_usuario = $this->session->userdata('id');
        $data['estudios'] = $this->candidato_model->revisionMayoresEstudios($id_candidato);
        if($data['estudios']){
            $candidato = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_grado_estudio' => $estudio['mayor_estudios'],
                'estudios_periodo' => $estudio['estudios_periodo'],
                'estudios_escuela' => $estudio['estudios_escuela'],
                'estudios_ciudad' => $estudio['estudios_ciudad'],
                'estudios_certificado' => $estudio['estudios_certificado'],
            );
            $this->candidato_model->editarCandidato($candidato, $id_candidato);
            $verificacion = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_tipo_studies' => $estudio['mayor_estudios2'],
                'periodo' => $estudio['estudios_periodo2'],
                'escuela' => $estudio['estudios_escuela2'],
                'ciudad' => $estudio['estudios_ciudad2'],
                'certificado' => $estudio['estudios_certificado2'],
                'comentarios' => $estudio['estudios_comentarios']
            );
            $this->candidato_model->updateMayoresEstudios($verificacion, $id_ver_estudios);
            echo $id_ver_estudios; 
        }
        else{
            $candidato = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_grado_estudio' => $estudio['mayor_estudios'],
                'estudios_periodo' => $estudio['estudios_periodo'],
                'estudios_escuela' => $estudio['estudios_escuela'],
                'estudios_ciudad' => $estudio['estudios_ciudad'],
                'estudios_certificado' => $estudio['estudios_certificado'],
            );
            $this->candidato_model->editarCandidato($candidato, $id_candidato);
            $verificacion = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'id_tipo_studies' => $estudio['mayor_estudios2'],
                'periodo' => $estudio['estudios_periodo2'],
                'escuela' => $estudio['estudios_escuela2'],
                'ciudad' => $estudio['estudios_ciudad2'],
                'certificado' => $estudio['estudios_certificado2'],
                'comentarios' => $estudio['estudios_comentarios']
            );
            $id_ver_estudios = $this->candidato_model->saveMayoresEstudios($verificacion);
            echo $id_ver_estudios;
        }
    }
    
    function viewStatusVerificaciones(){
        $id_candidato = $_POST['id_candidato'];
        $salida = '<div class="row">';
        $salida = '<p class="text-center "><strong>Criminal Records</strong></p>';
        $data['s_penales'] = $this->candidato_model->checkEstatusPenales($id_candidato);
        if($data['s_penales']){
            foreach($data['s_penales'] as $p){
                $aux = explode('-', $p->fecha);
                $f1 = $aux[1].'/'.$aux[2].'/'.$aux[0];
                $salida .= '<div class="col-md-12">';
                $salida .= '<p>Date: '.$f1.'</p><p>Comment: '.$p->comentarios.'</p></div>';
                $p_terminado = $p->finalizado;
            }
            $salida .= ($p_terminado == 1)? '<p class="text-center"><b>Completed</b></p><hr>' : '<p class="text-center"><b>In process by analyst</b></p><hr>';
            $salida .= '</div>';
            $salida .= '</div>';
        }
        else{
            $salida .= '<p class="text-center"><b>In process by analyst</b></p><br><hr>';
            $salida .= '</div>';
        }



        $salida .= '<p class="text-center "><strong>Laboral References</strong></p>';
        $data['s_laborales'] = $this->candidato_model->checkEstatusLaborales($id_candidato);
        if($data['s_laborales']){
            foreach($data['s_laborales'] as $l){
                $aux = explode('-', $l->fecha);
                $f2 = $aux[1].'/'.$aux[2].'/'.$aux[0];
                $salida .= '<div class="col-md-12">';
                $salida .= '<p>Date: '.$f2.'</p><p>Comment: '.$l->comentarios.'</p></div>';
                $l_terminado = $l->finalizado;
            }
            $salida .= ($l_terminado == 1)? '<p class="text-center"><b>Completed</b></p><hr>' : '<p class="text-center"><b>In process by analyst</b></p><hr>';
            $salida .= '</div>';
        }
        else{
            $salida .= '<p class="text-center"><b>In process by analyst</b></p><br><hr>';
            $salida .= '</div>';
        }


        $salida .= '<p class="text-center "><strong>Studies Record</strong></p>';
        $data['s_estudios'] = $this->candidato_model->checkEstatusEstudios($id_candidato);
        if($data['s_estudios']){
            foreach($data['s_estudios'] as $e){
                $aux = explode('-', $e->fecha);
                $f3 = $aux[1].'/'.$aux[2].'/'.$aux[0];
                $salida .= '<div class="col-md-12">';
                $salida .= '<p>Date: '.$f3.'</p><p>Comment: '.$e->comentarios.'</p></div>';
                $e_terminado = $e->finalizado;
            }
            $salida .= ($e_terminado == 1)? '<p class="text-center"><b>Completed</b></p>' : '<p class="text-center"><b>In process by analyst</b></p>';
            $salida .= '</div>';
        }
        else{
            $salida .= '<p class="text-center"><b>In process by analyst</b></p><br>';
            $salida .= '</div>';
        }
        $salida .= '</div>';

        $global = $this->candidato_model->checkGlobalSearches($id_candidato);
        if(isset($global)){
            $salida .= '<p class="text-center "><strong>Database searches</strong></p>';
            $dia = explode(' ', $global->creacion);
            $aux = explode('-', $dia[0]);
            $fglobal = $aux[1].'/'.$aux[2].'/'.$aux[0];
            $salida .= '<div class="col-md-12">';
            $salida .= '<p>Date: '.$fglobal.'</p><p>- Law enforcement: Verified</p><p>- Regulatory: Verified</p><p>- Sanctions: Verified</p><p>- Other bodies: Verified</p><p>- Media searches: Verified</p></div>';
            
            $salida .= '<p class="text-center"><b>Completed</b></p>';
            $salida .= '</div>';
        }
        else{
            $salida .= '<p class="text-center "><strong>Database searches</strong></p>';
            $fglobal = "In progress";
            $salida .= '<div class="col-md-12">';
            $salida .= '<p>Date: '.$fglobal.'</p><p>- Law enforcement: In progress</p><p>- Regulatory: In progress</p><p>- Sanctions: In progress</p><p>- Other bodies: In progress</p><p>- Media searches: In progress</p></div>';
            $salida .= '<p class="text-center"><b>In process by analyst</b></p>';
            $salida .= '</div>';
        }
        $salida .= '</div>';


        echo $salida;
    }
    function getHistorialDomicilios(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $cont = 1;
        $data['doms'] = $this->candidato_model->getHistorialDomicilios($id_candidato);
        if($data['doms']){
            foreach($data['doms'] as $d){
                $actual = ($cont == 1)? "Current address":"Previous address #".$cont."";
                $salida .= '
                <p class="tituloSubseccion">'.$actual.'</p>
                <div class="row">
                    <div class="col-md-4">
                        <label for="h'.$cont.'_periodo">Period *</label>
                        <input type="text" class="form-control obligado" name="h'.$cont.'_periodo" id="h'.$cont.'_periodo" value="'.$d->periodo.'" disabled><br>
                    </div>
                    <div class="col-md-8">
                        <label for="h'.$cont.'_causa">Cause of departure *</label>
                        <input type="text" class="form-control obligado" name="h'.$cont.'_causa" id="h'.$cont.'_causa" value="'.$d->causa.'" disabled><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="h'.$cont.'_calle">Address *</label>
                        <input type="text" class="form-control obligado" name="h'.$cont.'_calle" id="h'.$cont.'_calle" value="'.$d->calle.'" disabled><br>
                    </div>
                    <div class="col-md-2">
                        <label for="h'.$cont.'_exterior">Ext. Num. *</label>
                        <input type="text" class="form-control obligado" name="h'.$cont.'_exterior" id="h'.$cont.'_exterior"  maxlength="6" value="'.$d->exterior.'" disabled><br>
                    </div>
                    <div class="col-md-2">
                        <label for="h'.$cont.'_interior">Int. Num. </label>
                        <input type="text" class="form-control" name="h'.$cont.'_interior" id="h'.$cont.'_interior"  maxlength="5" value="'.$d->interior.'" disabled><br>
                    </div>
                    <div class="col-md-4">
                        <label for="h'.$cont.'_colonia">Neighborhood *</label>
                        <input type="text" class="form-control obligado" name="h'.$cont.'_colonia" id="h'.$cont.'_colonia" value="'.$d->colonia.'" disabled><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="h'.$cont.'_estado">State *</label>
                        <input type="text" class="form-control obligado" name="h'.$cont.'_estado" id="h'.$cont.'_estado" value="'.$d->estado.'" disabled><br>
                    </div>
                    <div class="col-md-4">
                        <label for="h'.$cont.'_municipio">City *</label>
                        <input type="text" class="form-control obligado" name="h'.$cont.'_municipio" id="h'.$cont.'_municipio" value="'.$d->municipio.'" disabled><br>
                    </div>
                    <div class="col-md-2">
                        <label for="h'.$cont.'_cp">Zip Code *</label>
                        <input type="text" class="form-control solo_numeros obligado" name="h'.$cont.'_cp" id="h'.$cont.'_cp"  maxlength="5" value="'.$d->cp.'" disabled><br>
                    </div>
                </div>';
                $cont++;
            }
            echo $salida;
        }
        else{
            echo $salida;
        }
    }
    function verificarDomicilios(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_doms');
        parse_str($cadena, $dom);
        $id_candidato = $dom['id_candidato'];
        $id_usuario = $this->session->userdata('id');
        $this->candidato_model->cleanVerificacionDomicilios($id_candidato);
        $domicilio = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'id_candidato' => $id_candidato,
            'comentario' => $dom['domicilios_comentarios']
        );
        $this->candidato_model->saveVerificacionDomicilio($domicilio);
        echo $salida = 1;
    }
    
    
    
    
    function checkVerificacionDomicilios(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $dom = $this->candidato_model->checkVerificacionDomicilios($id_candidato);
        if(isset($dom)){
            echo $dom->comentario;
        }
        else{
            echo $salida;
        }
        
    }
    function getProyectosSubcliente(){
        $id_cliente = $_POST['id_cliente'];
        $id_subcliente = $_POST['id_subcliente'];
        $data['proyectos'] = $this->candidato_model->getProyectosSubcliente($id_cliente, $id_subcliente);
        $salida = "<option value=''>Selecciona</option>";
        //$salida .= "<option value='0'>N/A</option>";
        if($data['proyectos']){
            foreach ($data['proyectos'] as $row){
                $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            echo $salida;
        }
    }
    function xx(){
        $basic  = new \Nexmo\Client\Credentials\Basic('5aa133d2', 'mvVfoFEKFZsHDH8c');
        $client = new \Nexmo\Client($basic);

        $message = $client->message()->send([
            'to' => '523331493010',
            'from' => 'Nexmo',
            'text' => 'Hello from NexmoRODI'
        ]);
    }

    /************************************************ Candidatos *********************************************************/
    function registrarCandidatoEspanol(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $existeCandidato = $this->candidato_model->checkCandidatoRepetido(strtoupper($this->input->post('nombre')), strtoupper($this->input->post('paterno')), strtoupper($this->input->post('materno')), $this->input->post('id_cliente'));
        if($existeCandidato > 0){
            echo $res = 0;
        }
        else{
            $usuario = $this->input->post('usuario');
            switch ($usuario) {
                case 1:
                    $tipo_usuario = "id_usuario";
                    break;
                case 2:
                    $tipo_usuario = "id_usuario_cliente";
                    break;
                case 3:
                    $tipo_usuario = "id_usuario_subcliente";
                    break;
            }
            $id_usuario = $this->session->userdata('id');

            $data['candidato'] = $this->candidato_model->vieneDoping(strtoupper($this->input->post('nombre')), strtoupper($this->input->post('paterno')), strtoupper($this->input->post('materno')), $this->input->post('id_cliente'));
            if($data['candidato']){
                foreach ($data['candidato'] as $dato) {
                    $idCandidato = $dato->idCandidato;
                    $idPrueba = $dato->idPrueba;
                }
                $data = array(
                    'edicion' => $date,
                    $tipo_usuario => $id_usuario,
                    'id_subcliente' => $this->input->post('subcliente'),
                    'id_puesto' => $this->input->post('puesto'),
                    'correo' => $this->input->post('correo'),
                    'celular' => $this->input->post('celular'),
                    'telefono_casa' => $this->input->post('fijo')
                );
                $id_candidato = $this->candidato_model->editarCandidato($data, $idCandidato);
                //Subida y Registro de CV
                if($this->input->post('hay_cvs') == 1){
                    $countfiles = count($_FILES['cvs']['name']);
                    $nombreCandidato = str_replace(' ', '', $this->input->post('nombre'));
                    $paternoCandidato = str_replace(' ', '', $this->input->post('paterno'));
  
                    for($i = 0; $i < $countfiles; $i++){
                        if(!empty($_FILES['cvs']['name'][$i])){
                            // Define new $_FILES array - $_FILES['file']
                            $_FILES['file']['name'] = $_FILES['cvs']['name'][$i];
                            $_FILES['file']['type'] = $_FILES['cvs']['type'][$i];
                            $_FILES['file']['tmp_name'] = $_FILES['cvs']['tmp_name'][$i];
                            $_FILES['file']['error'] = $_FILES['cvs']['error'][$i];
                            $_FILES['file']['size'] = $_FILES['cvs']['size'][$i];
                            $temp = str_replace(' ', '', $_FILES['cvs']['name'][$i]);
                            $nombre_cv = $id_candidato."_".$nombreCandidato."".$paternoCandidato."_".$temp;
                            // Set preference
                            $config['upload_path'] = './_docs/'; 
                            $config['allowed_types'] = 'pdf|jpeg|jpg|png';
                            $config['max_size'] = '15000'; // max_size in kb
                            $config['file_name'] = $nombre_cv;
                            //Load upload library
                            $this->load->library('upload',$config); 
                            $this->upload->initialize($config);
                            // File upload
                            if($this->upload->do_upload('file')){
                                $data = $this->upload->data(); 
                                //$salida = 1; 
                            }
                            $documento = array(
                                'creacion' => $date,
                                'edicion' => $date,
                                'id_candidato' => $id_candidato,
                                'id_tipo_documento' => 16,
                                'archivo' => $nombre_cv
                            );
                            $this->candidato_model->insertCVCandidato($documento);
                        }
                    }
                }
                //$socio = ($this->input->post('socio') == 'on')? 1 : 0;
                $medico = ($this->input->post('medico') == 'on')? 1 : 0;
                $sociolaboral = ($this->input->post('laboral') == 'on')? 1 : 0;
                $antidoping = ($this->input->post('antidoping') == 'on')? 1 : 0;
                $psicometrico = ($this->input->post('psicometrico') == 'on')? 1 : 0;
                $buro = ($this->input->post('buro') == 'on')? 1 : 0;

                if($antidoping == 1){
                    $drogas = ($this->input->post('examen') != "")? $this->input->post('examen'):0;
                    $tipo_antidoping = 1;
                }
                else{
                    $drogas = 0;
                    $tipo_antidoping = 0;
                }
                $pruebas = array(
                    'edicion' => $date,
                    $tipo_usuario => $id_usuario,
                    'socioeconomico' => 1,
                    'tipo_psicometrico' => $psicometrico,
                    'psicometrico' => $psicometrico,
                    'medico' => $medico,
                    'buro_credito' => $buro,
                    'sociolaboral' => $sociolaboral,
                    'otro_requerimiento' => $this->input->post('otro')
                );
                $this->candidato_model->updatePruebasCandidato($pruebas, $idPrueba);

                $visita = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    $tipo_usuario => $id_usuario,
                    'id_cliente' => $this->input->post('id_cliente'),
                    'id_subcliente' => $this->input->post('subcliente'),
                    'id_candidato' => $idCandidato,
                    'id_tipo_formulario' => 4
                    
                );
                $this->candidato_model->llevaVisitaOperador($visita);

                if($usuario == 2 || $usuario == 3){
                    $from = $this->config->item('smtp_user');
                    $info_cliente = $this->cliente_model->getDatosCliente($this->input->post('id_cliente'));
                    $to = "bjimenez@rodi.com.mx";
                    $subject = " Nuevo candidato en la plataforma del cliente ".$info_cliente->nombre;
                    $message = "Se ha agregado a ".strtoupper($this->input->post('nombre'))." ".strtoupper($this->input->post('paterno'))." ".strtoupper($this->input->post('materno'))." del cliente ".$info_cliente->nombre." en la plataforma";
                    $this->load->library('phpmailer_lib');
                    $mail = $this->phpmailer_lib->load();
                    $mail->isSMTP();
                    $mail->Host     = 'rodi.com.mx';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'rodicontrol@rodi.com.mx';
                    $mail->Password = 'RRodi#2019@';
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port     = 465;
                    $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                    $mail->addAddress($to);
                    $mail->Subject = $subject;
                    $mail->isHTML(true);
                    $mailContent = $message;
                    $mail->Body = $mailContent;

                    if(!$mail->send()){
                        $enviado = 1;
                    }else{
                       $enviado = 0;
                    }
                } 
                echo $salida = 1;
            }
            else{
                if($usuario == 2 || $usuario == 3){
                    if($this->input->post('subcliente') != 180){
                        $config = $this->configuracion_model->getConfiguraciones();
                        $data = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            $tipo_usuario => $id_usuario,
                            'id_usuario' => $config->usuario_lider_espanol,
                            'id_cliente' => $this->input->post('id_cliente'),
                            'id_subcliente' => $this->input->post('subcliente'),
                            'id_puesto' => $this->input->post('puesto'),
                            'fecha_alta' => $date,
                            'nombre' => strtoupper($this->input->post('nombre')),
                            'paterno' => strtoupper($this->input->post('paterno')),
                            'materno' => strtoupper($this->input->post('materno')),
                            'correo' => $this->input->post('correo'),
                            'celular' => $this->input->post('celular'),
                            'telefono_casa' => $this->input->post('fijo')
                        );
                    }
                    if($this->input->post('subcliente') == 180){
                        $config = $this->configuracion_model->getConfiguraciones();
                        $data = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            $tipo_usuario => $id_usuario,
                            'id_usuario' => $config->usuario_lider_ingles,
                            'id_cliente' => $this->input->post('id_cliente'),
                            'id_subcliente' => $this->input->post('subcliente'),
                            'id_puesto' => $this->input->post('puesto'),
                            'fecha_alta' => $date,
                            'nombre' => strtoupper($this->input->post('nombre')),
                            'paterno' => strtoupper($this->input->post('paterno')),
                            'materno' => strtoupper($this->input->post('materno')),
                            'correo' => $this->input->post('correo'),
                            'celular' => $this->input->post('celular'),
                            'telefono_casa' => $this->input->post('fijo')
                        );
                    }
                }
                else{
                    $data = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        $tipo_usuario => $id_usuario,
                        'id_cliente' => $this->input->post('id_cliente'),
                        'id_subcliente' => $this->input->post('subcliente'),
                        'id_puesto' => $this->input->post('puesto'),
                        'fecha_alta' => $date,
                        'nombre' => strtoupper($this->input->post('nombre')),
                        'paterno' => strtoupper($this->input->post('paterno')),
                        'materno' => strtoupper($this->input->post('materno')),
                        'correo' => $this->input->post('correo'),
                        'celular' => $this->input->post('celular'),
                        'telefono_casa' => $this->input->post('fijo')
                    );
                }
                $id_candidato = $this->candidato_model->registrarCandidatoEspanol($data);
                //Subida y Registro de CV
                if($this->input->post('hay_cvs') == 1){
                    $countfiles = count($_FILES['cvs']['name']);
                    $nombreCandidato = str_replace(' ', '', $this->input->post('nombre'));
                    $paternoCandidato = str_replace(' ', '', $this->input->post('paterno'));
  
                    for($i = 0; $i < $countfiles; $i++){
                        if(!empty($_FILES['cvs']['name'][$i])){
                            // Define new $_FILES array - $_FILES['file']
                            $_FILES['file']['name'] = $_FILES['cvs']['name'][$i];
                            $_FILES['file']['type'] = $_FILES['cvs']['type'][$i];
                            $_FILES['file']['tmp_name'] = $_FILES['cvs']['tmp_name'][$i];
                            $_FILES['file']['error'] = $_FILES['cvs']['error'][$i];
                            $_FILES['file']['size'] = $_FILES['cvs']['size'][$i];
                            $temp = str_replace(' ', '', $_FILES['cvs']['name'][$i]);
                            $nombre_cv = $id_candidato."_".$nombreCandidato."".$paternoCandidato."_".$temp;
                            // Set preference
                            $config['upload_path'] = './_docs/'; 
                            $config['allowed_types'] = 'pdf|jpeg|jpg|png';
                            $config['max_size'] = '15000'; // max_size in kb
                            $config['file_name'] = $nombre_cv;
                            //Load upload library
                            $this->load->library('upload',$config); 
                            $this->upload->initialize($config);
                            // File upload
                            if($this->upload->do_upload('file')){
                                $data = $this->upload->data(); 
                                //$salida = 1; 
                            }
                            $documento = array(
                                'creacion' => $date,
                                'edicion' => $date,
                                'id_candidato' => $id_candidato,
                                'id_tipo_documento' => 16,
                                'archivo' => $nombre_cv
                            );
                            $this->candidato_model->insertCVCandidato($documento);
                        }
                    }
                }
                //$socio = ($this->input->post('socio') == 'on')? 1 : 0;
                $medico = ($this->input->post('medico') == 'on')? 1 : 0;
                $sociolaboral = ($this->input->post('laboral') == 'on')? 1 : 0;
                $antidoping = ($this->input->post('antidoping') == 'on')? 1 : 0;
                $psicometrico = ($this->input->post('psicometrico') == 'on')? 1 : 0;
                $buro = ($this->input->post('buro') == 'on')? 1 : 0;

                if($antidoping == 1){
                    $drogas = ($this->input->post('examen') != "")? $this->input->post('examen'):0;
                    $tipo_antidoping = 1;
                }
                else{
                    $drogas = 0;
                    $tipo_antidoping = 0;
                }
                $pruebas = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    $tipo_usuario => $id_usuario,
                    'id_candidato' => $id_candidato,
                    'id_cliente' => $this->input->post('id_cliente'),
                    'socioeconomico' => 1,
                    'tipo_antidoping' => $tipo_antidoping,
                    'antidoping' => $drogas,
                    'tipo_psicometrico' => $psicometrico,
                    'psicometrico' => $psicometrico,
                    'medico' => $medico,
                    'buro_credito' => $buro,
                    'sociolaboral' => $sociolaboral,
                    'otro_requerimiento' => $this->input->post('otro')
                    
                );
                $this->candidato_model->insertPruebasCandidato($pruebas);

                if($this->input->post('subcliente') != 180){
                    $visita = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        $tipo_usuario => $id_usuario,
                        'id_cliente' => $this->input->post('id_cliente'),
                        'id_subcliente' => $this->input->post('subcliente'),
                        'id_candidato' => $id_candidato,
                        'id_tipo_formulario' => 4
                        
                    );
                    $this->candidato_model->llevaVisitaOperador($visita);
                }
                
                if($usuario == 2 || $usuario == 3){
                    $from = $this->config->item('smtp_user');
                    $info_cliente = $this->cliente_model->getDatosCliente($this->input->post('id_cliente'));
                    $to = "bjimenez@rodi.com.mx";
                    $subject = " Nuevo candidato en la plataforma del cliente ".$info_cliente->nombre;
                    $message = "Se ha agregado a ".strtoupper($this->input->post('nombre'))." ".strtoupper($this->input->post('paterno'))." ".strtoupper($this->input->post('materno'))." del cliente ".$info_cliente->nombre." en la plataforma";
                    $this->load->library('phpmailer_lib');
                    $mail = $this->phpmailer_lib->load();
                    $mail->isSMTP();
                    $mail->Host     = 'rodi.com.mx';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'rodicontrol@rodi.com.mx';
                    $mail->Password = 'RRodi#2019@';
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port     = 465;
                    $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                    $mail->addAddress($to);
                    $mail->Subject = $subject;
                    $mail->isHTML(true);
                    $mailContent = $message;
                    $mail->Body = $mailContent;

                    if(!$mail->send()){
                        $enviado = 1;
                    }else{
                       $enviado = 0;
                    }
                } 
                echo $salida = 1;
            }
        }
    }
    
    
    function actualizarDatosGeneralesIngles(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $cadena = $this->input->post('d_generales');
        parse_str($cadena, $personal);
        $id_candidato = $personal['id_candidato'];
        $id_usuario = $this->session->userdata('id');
        $lugar_nacimiento = (isset($personal['lugar_ingles']))? $personal['lugar_ingles']:'';
        $tel_oficina = (isset($personal['tel_oficina_ingles']))? $personal['tel_oficina_ingles']:'';
        $nacionalidad = (isset($personal['nacionalidad_ingles']))? $personal['nacionalidad_ingles']:'';
        $correo = (isset($personal['personales_correo_ingles']))? $personal['personales_correo_ingles']:'';
        $tiempo_dom_actual = (isset($personal['tiempo_dom_actual']))? $personal['tiempo_dom_actual']:'';
        $tiempo_traslado = (isset($personal['tiempo_traslado']))? $personal['tiempo_traslado']:'';
        $medio_transporte = (isset($personal['medio_transporte']))? $personal['medio_transporte']:'';
        $grado = (isset($personal['grado']))? $personal['grado']:0;
        $calles = (isset($personal['calles_ingles']))? $personal['calles_ingles']:'';

        $fecha = fecha_espanol_bd($personal['fecha_nacimiento_ingles']);
        $edad = $this->calculaEdad($fecha);
        $candidato = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'nombre' => $personal['nombre_ingles'],
            'paterno' => $personal['paterno_ingles'],
            'materno' => $personal['materno_ingles'],
            'fecha_nacimiento' => $fecha,
            'edad' => $edad,
            'puesto' => $personal['puesto_ingles'],
            'lugar_nacimiento' => $lugar_nacimiento,
            'nacionalidad' => $nacionalidad,
            'genero' => $personal['genero_ingles'],
            'id_grado_estudio' => $grado,
            'calle' => $personal['calle_ingles'],
            'exterior' => $personal['exterior_ingles'],
            'interior' => $personal['interior_ingles'],
            'entre_calles' => $calles,
            'colonia' => $personal['colonia_ingles'],
            'id_estado' => $personal['estado_ingles'],
            'id_municipio' => $personal['municipio_ingles'],
            'cp' => $personal['cp_ingles'],
            'id_estado_civil' => $personal['civil_ingles'],
            'celular' => $personal['celular_general_ingles'],
            'telefono_casa' => $personal['tel_casa_ingles'],
            'telefono_otro' => $tel_oficina,
            'correo' => $correo,
            'tiempo_dom_actual' => $tiempo_dom_actual,
            'tiempo_traslado' => $tiempo_traslado,
            'tipo_transporte' => $medio_transporte
        );
        $this->candidato_model->editarCandidato($candidato, $id_candidato);

        /*$res = $this->candidato_model->checkTipoClienteCandidato($id_candidato);
        if($res->id_cliente != 1){
            $visita = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'calle' => $personal['calle_ingles'],
                'exterior' => $personal['exterior_ingles'],
                'interior' => $personal['interior_ingles'],
                'colonia' => $personal['colonia_ingles'],
                'id_estado' => $personal['estado_ingles'],
                'id_municipio' => $personal['municipio_ingles'],
                'cp' => $personal['cp_ingles'],
                'celular' => $personal['celular_general_ingles'],
                'telefono_casa' => $personal['tel_casa_ingles']
                
            );
            $this->candidato_model->updateVisitaOperador($visita, $id_candidato);
        }*/
        echo $salida = 1;
    }
    
    
    
    
    
    
    function candidatoResponseForm(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->session->userdata('id');
       
        $cadena = $this->input->post('datos');
        parse_str($cadena, $personal);
        $cadena3 = $this->input->post('complementos');
        parse_str($cadena3, $dato);
        
        $fecha = fecha_ingles_bd($personal['fecha_nacimiento']);
        $edad = $this->calculaEdad($fecha);
        $candidato = array(
            'fecha_contestado' => $date,
            //'token' => 'completo',
            'edicion' => $date,
            'fecha_nacimiento' => $fecha,
            'edad' => $edad,
            'puesto' => $personal['puesto'],
            'nacionalidad' => $personal['nacionalidad'],
            'genero' => $personal['genero'],
            'id_grado_estudio' => $dato['estudios'],
            'estudios_periodo' => $dato['estudios_periodo'],
            'estudios_escuela' => $dato['estudios_escuela'],
            'estudios_ciudad' => $dato['estudios_ciudad'],
            'estudios_certificado' => $dato['estudios_certificado'],
            'calle' => $personal['calle'],
            'exterior' => $personal['exterior'],
            'interior' => $personal['interior'],
            'colonia' => $personal['colonia'],
            'id_estado' => $personal['estado'],
            'id_municipio' => $personal['municipio'],
            'cp' => $personal['cp'],
            'id_estado_civil' => $personal['civil'],
            'celular' => $personal['telefono'],
            'telefono_casa' => $personal['tel_casa'],
            'telefono_otro' => $personal['tel_otro'],
            'comentario' => $dato['obs'],
            'trabajo_inactivo' => $dato['trabajo_inactivo'],
            'status' => 1
        );
        $this->candidato_model->saveCandidato($candidato, $id_candidato);
        
        if($_POST['trabajos'] != ""){
            $data_trabajo = "";
            $trab = explode("@@", $_POST['trabajos']);
            for($i = 0; $i < count($trab); $i++){
                $aux = explode("__", $trab[$i]);
                if($trab[$i] != ""){
                    $data_trabajo = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_candidato' => $id_candidato,
                        'empresa' => ucwords(strtolower($aux[0])),
                        'direccion' => ucwords(strtolower($aux[1])),
                        'fecha_entrada_txt' => $aux[2],
                        'fecha_salida_txt' => $aux[3],
                        'telefono' => $aux[4],
                        'puesto1' => ucwords(strtolower($aux[5])),
                        'puesto2' => ucwords(strtolower($aux[6])),
                        'salario1_txt' => $aux[7],
                        'salario2_txt' => $aux[8],
                        'jefe_nombre' => ucwords(strtolower($aux[9])),
                        'jefe_correo' => strtolower($aux[10]),
                        'jefe_puesto' => ucwords(strtolower($aux[11])),
                        'causa_separacion' => $aux[12]
                    );
                    $this->candidato_model->saveRefLab($data_trabajo);
                }
            }
        }
        if(isset($_POST['doms'])){
            $data_dom = "";
            $dom = explode("@@", $_POST['doms']);
            for($i = 0; $i < count($dom); $i++){
                $aux = explode("__", $dom[$i]);
                if($dom[$i] != ""){
                    if($i == 0){
                        $data_dom = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'periodo' => $aux[0],
                            'causa' => $aux[1],
                            'calle' => $personal['calle'],
                            'exterior' => $personal['exterior'],
                            'interior' => $personal['interior'],
                            'colonia' => $personal['colonia'],
                            'id_estado' => $personal['estado'],
                            'id_municipio' => $personal['municipio'],
                            'cp' => $personal['cp']
                        );
                        $this->candidato_model->saveDomicilio($data_dom);
                    }
                    else{
                        $data_dom = array(
                            'creacion' => $date,
                            'edicion' => $date,
                            'id_candidato' => $id_candidato,
                            'periodo' => $aux[0],
                            'causa' => $aux[1],
                            'calle' => $aux[2],
                            'exterior' => $aux[3],
                            'interior' => $aux[4],
                            'colonia' => $aux[5],
                            'id_estado' => $aux[6],
                            'id_municipio' => $aux[7],
                            'cp' => $aux[8]
                        );
                        $this->candidato_model->saveDomicilio($data_dom);
                    }
                }
            }
        }
        echo $hecho = 1;
    }
    
    
    
    
    
    
    
    
    
    
    function getRefPersonales(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['refs'] = $this->candidato_model->getPersonales($id_candidato);
        if($data['refs']){
            foreach($data['refs'] as $ref){
                $salida .= $ref->nombre."@@";
                $salida .= $ref->telefono."@@";
                $salida .= $ref->tiempo_conocerlo."@@";
                $salida .= $ref->donde_conocerlo."@@";
                $salida .= $ref->sabe_trabajo."@@";
                $salida .= $ref->sabe_vive."@@";
                $salida .= $ref->recomienda."@@";
                $salida .= $ref->comentario."@@";
                $salida .= $ref->id."###";
            }
            echo $salida;
        }
        else{
            echo $salida = 0;
        }
    }
    function getVecinales(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['refs'] = $this->candidato_model->getVecinales($id_candidato);
        if($data['refs']){
            foreach($data['refs'] as $ref){
                $salida .= $ref->nombre."@@";
                $salida .= $ref->telefono."@@";
                $salida .= $ref->domicilio."@@";
                $salida .= $ref->concepto_candidato."@@";
                $salida .= $ref->concepto_familia."@@";
                $salida .= $ref->civil_candidato."@@";
                $salida .= $ref->hijos_candidato."@@";
                $salida .= $ref->sabe_trabaja."@@";
                $salida .= $ref->notas."@@";
                $salida .= $ref->id."###";
            }
            echo $salida;
        }
        else{
            echo $salida = 0;
        }
    }
    
    
    
    
    
    function revisionEstudioCandidato(){
        $id_candidato = $_POST['id_candidato'];
        $completo = "";
        $docs = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
        if(isset($docs)){
            if($docs->acta == "" || $docs->acta == null ||
                $docs->fecha_acta == ""   || $docs->fecha_acta == null ||
                $docs->cuenta_domicilio == ""    || $docs->cuenta_domicilio == null ||
                $docs->fecha_domicilio == ""  || $docs->fecha_domicilio == null ||
                $docs->curp == ""    || $docs->curp == null ||
                $docs->emision_curp == ""    || $docs->emision_curp == null ||
                $docs->emision_rfc == ""    || $docs->emision_rfc == null ||
                $docs->rfc == "" || $docs->rfc == null){
                $completo .= "D0";
            }
            else{
                $completo .= "D1";
            }
            if($completo == "D1"){
                $data['estudios'] = $this->candidato_model->revisionEstudios($id_candidato);
                if($data['estudios']){
                    $completo .= "SE1";

                }
                else{
                    $completo .= "SE0";
                }
                if($completo == "D1SE1"){
                    $data['ant_sociales'] = $this->candidato_model->revisionAntecedentesSociales($id_candidato);
                    if($data['ant_sociales']){
                        $completo .= "AS1";

                    }
                    else{
                        $completo .= "AS0";
                    }
                    if($completo == "D1SE1AS1"){
                        $data['personas'] = $this->candidato_model->revisionFamiliares($id_candidato);
                        if($data['personas']){
                            $completo .= "F1";

                        }
                        else{
                            $completo .= "F0";
                        }
                        if($completo == "D1SE1AS1F1"){
                            $data['ref_personal'] = $this->candidato_model->revisionVerificacionPersonal($id_candidato);
                            if($data['ref_personal']){
                                $completo .= "P1";

                            }
                            else{
                                $completo .= "P0";
                            }
                            if($completo == "D1SE1AS1F1P1"){
                                $data['ant_laborales'] = $this->candidato_model->revisionAntecedentesLaborales($id_candidato);
                                if($data['ant_laborales']){
                                    $completo .= "AL1";
                                }
                                else{
                                    $completo .= "AL0";
                                }
                                if($completo == "D1SE1AS1F1P1AL1"){
                                    $data['legal'] = $this->candidato_model->revisionInvestigacionLegal($id_candidato);
                                    if($data['legal']){
                                        $completo .= "IL1";

                                    }
                                    else{
                                        $completo .= "IL0";
                                    }
                                    if($completo == "D1SE1AS1F1P1AL1IL1"){
                                        $data['no_mencionados'] = $this->candidato_model->revisionTrabajosNoMencionados($id_candidato);
                                        if($data['no_mencionados']){
                                            echo $completo .= "N1";

                                        }
                                        else{
                                            echo $completo .= "N0";
                                        }
                                    }
                                    else{
                                        echo $completo;
                                    }
                                }
                                else{
                                    echo $completo; 
                                }
                            }
                            else{
                                echo $completo; 
                            }
                        }
                        else{
                            echo $completo;
                        }
                    }
                    else{
                        echo $completo;
                    }
                }
                else{
                   echo $completo; 
                }
            }
            else{
                echo $completo;
            }
            
        }
        else{
            echo $completo .= "D0";
        }
    }
    
    
    
    
    
    function crearPrevioPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_candidato = $_POST['idPrevio'];
        $data['datos'] = $this->candidato_model->getInfoCandidato($id_candidato);
        foreach($data['datos'] as $row){
            $f = $row->fecha_alta;
            //$fform = $row->fecha_contestado;
            //$fdocs = $row->fecha_documentos;
            $ffin = $row->fecha_fin;
            $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
            $cliente = $row->cliente;
            $subcliente = $row->subcliente;
            $id_doping = $row->idDoping;
            $id_cliente = $row->id_cliente;
        }
        //$fecha_fin = DateTime::createFromFormat('Y-m-d H:i:s', $ffin);
        //$fecha_fin = $fecha_fin->format('F d, Y');
        $fecha_fin = $this->formatoFechaEspanol($ffin);
        $f_alta = $this->formatoFechaEspanol($f);
        //$fform = $this->formatoFecha($fform);
        //$fdocs = $this->formatoFecha($fdocs);
        //$ffin = $this->formatoFecha($ffin);
        $hoy = $this->formatoFecha($hoy);
        $data['finalizado'] = $this->candidato_model->getDatosFinalizadosCandidato($id_candidato);
        $data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
        $data['pruebas'] = $this->candidato_model->getPruebasCandidato($id_candidato);
        //$data['fecha_ver_laboral'] = $this->candidato_model->getFechaVerificacionLaboral($id_candidato);
        //$data['fecha_ver_estudios'] = $this->candidato_model->getFechaVerificacionEstudios($id_candidato);
        //$data['fecha_ver_penales'] = $this->candidato_model->getFechaVerificacionPenales($id_candidato);
        //$data['fecha_ver_ofac'] = $this->candidato_model->getFechaVerificacionOfac($id_candidato);
        $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
        //$data['ver_documento'] = $this->candidato_model->getVerificacionDocumentosCandidato($id_candidato);
        $data['academico'] = $this->candidato_model->getHistorialAcademicoCandidato($id_candidato);
        $data['sociales'] = $this->candidato_model->getAntecedentesSocialesCandidato($id_candidato);
        $data['familia'] = $this->candidato_model->getFamiliares($id_candidato);
        $data['egresos'] = $this->candidato_model->getEgresosFamiliares($id_candidato);
        $data['vivienda'] = $this->candidato_model->getDatosVivienda($id_candidato);
        $data['ref_personal'] = $this->candidato_model->getReferenciasPersonales($id_candidato);
        $data['ref_vecinal'] = $this->candidato_model->getReferenciasVecinales($id_candidato);
        $data['legal'] = $this->candidato_model->getVerificacionLegal($id_candidato);
        $data['nom'] = $this->candidato_model->getTrabajosNoMencionados($id_candidato);
        $data['finalizado'] = $this->candidato_model->getEstudioFinalizado($id_candidato);
        //$data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
        $data['ref_laboral'] = $this->candidato_model->getLaborales($id_candidato);
        //$data['ver_laboral'] = $this->candidato_model->getVerificacionReferencias($id_candidato);
        //$data['det_empleo'] = $this->candidato_model->getStatusVerificacionEmpleo($id_candidato);
        //$data['agregados'] = $this->candidato_model->getDocsAgregados($id_candidato);
        //$data['det_penales'] = $this->candidato_model->getStatusVerificacionPenales($id_candidato);
        $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
        $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
        $data['cliente'] = $cliente;
        $data['subcliente'] = $subcliente;
        $data['fecha_fin'] = $ffin;

        $checkDoping = $this->candidato_model->confirmarAntidoping($id_candidato);
        if($checkDoping->tipo_antidoping == 1 && $checkDoping->status_doping == 1){
            $doping = $this->doping_model->getDatosDoping($id_doping);
            $dop['doping'] = $doping;
            $data['doc_doping'] = $this->load->view('pdfs/doping_pdf',$dop,TRUE);
        }
        else{
            $data['doc_doping'] = "";
        }
        $html = $this->load->view('pdfs/previo_espanol_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->AddPage();
        if($id_cliente == 39){
            $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 100px;" src="'.base_url().'img/logo_talink.png"></div><div style="width: 33%; float: right;text-align: right;">Fecha de Registro: '.$f_alta.'<br>Fecha de Elaboración: '.$fecha_fin.'</div>');
            
        }
        else{
            $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Fecha de Registro: '.$f_alta.'<br>Fecha de Elaboración: '.$fecha_fin.'</div>');
            $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
        }        
        $mpdf->WriteHTML($html);

        $mpdf->Output('Estudio_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
    
    
    function getEgresos(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['egresos'] = $this->candidato_model->getEgresos($id_candidato);
        if($data['egresos']){
            foreach($data['egresos'] as $e){
                //$salida .= $e->id."@@";
                $salida .= $e->renta."@@";
                $salida .= $e->alimentos."@@";
                $salida .= $e->servicios."@@";
                $salida .= $e->transporte."@@";
                $salida .= $e->otros."@@";
                $salida .= $e->solvencia;
            }
            echo $salida;
        }
        else{
            echo $salida = 0;
        }
    }
    function getHabitacion(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['datos'] = $this->candidato_model->getHabitacion($id_candidato);
        if($data['datos']){
            foreach($data['datos'] as $hab){
                //$salida .= $hab->id."@@";
                $salida .= $hab->tiempo_residencia."@@";
                $salida .= $hab->zona."@@";
                $salida .= $hab->vivienda."@@";
                $salida .= $hab->recamaras."@@";
                $salida .= $hab->banios."@@";
                $salida .= $hab->distribucion."@@";
                $salida .= $hab->calidad_mobiliario."@@";
                $salida .= $hab->mobiliario."@@";
                $salida .= $hab->tamanio_vivienda."@@";
                $salida .= $hab->condiciones;
            }
            echo $salida;
        }
        else{
            echo $salida = 0;
        }
    }
    
    
    
    
    
    function verificarVisitayDoping(){
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $doping = $this->candidato_model->verificarAntidoping($id_candidato);
        if($doping->tipo_antidoping != 0){
            if($doping->status_doping == 0){
                $salida .= "<p>- No se ha registrado el resultado del examen antidoping del candidato</p><br>";
            }
            $visita = $this->candidato_model->verificarVisita($id_candidato);
            if($visita != "" && $visita != null){
                $visitado = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
                if($visitado->visitador == 0){
                    $salida .= "<p>- No se ha registrado la visita al candidato</p>";
                    echo $salida;
                }
                else{
                    echo $salida;
                }
            }
            else{
                echo $salida;
            }            
        }
        else{
            $visita = $this->candidato_model->verificarVisita($id_candidato);
            if($visita != "" && $visita != null){
                $visitado = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
                if($visitado->visitador == 0){
                    $salida .= "<p>- No se ha registrado la visita al candidato</p>";
                    echo $salida;
                }
                else{
                    echo $salida;
                }
            }
            else{
                echo $salida;
            }
        }
    }
    function visitaConfirmada(){
        date_default_timezone_set('America/Mexico_City');
        $id_candidato = $_POST['id_candidato'];
        $date = date('Y-m-d H:i:s');
        $id_usuario = $this->session->userdata('id');
        $visita = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'visitador' => 1
        );
        $this->candidato_model->saveCandidato($visita, $id_candidato);
    }
    function finalizarProcesoCandidato(){
        date_default_timezone_set('America/Mexico_City');
        $id_candidato = $_POST['id_candidato'];
        $estatus = $_POST['estatus'];
        $date = date('Y-m-d H:i:s');
        $id_usuario = $this->session->userdata('id');
        $comentario = ($_POST['comentario'] !== null)? $_POST['comentario']:'';

        $num = $this->candidato_model->checkBGC($id_candidato);
        if($num > 0){
            $bgc = array(
                'id_usuario' => $id_usuario,
                'comentario_final' => $comentario
            );
            $this->candidato_model->updateBGC($bgc, $id_candidato);
            $this->candidato_model->statusBGCCandidato($estatus, $id_candidato);
        }
        else{
            $bgc = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'comentario_final' => $comentario
            );
            $this->candidato_model->saveBGC($bgc);
            $this->candidato_model->statusBGCCandidato($estatus, $id_candidato);
        }
        
    }
    function accionCandidato(){
        $id_usuario = $this->session->userdata('id');
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $motivo = $_POST['motivo'];
        $id_candidato = $_POST['id'];
        $id_cliente = $_POST['id_cliente'];
        $usuario = $_POST['usuario'];
        switch ($usuario) {
            case 1:
                $tipo_usuario = "id_usuario";
                break;
            case 2:
                $tipo_usuario = "id_usuario_cliente";
                break;
            case 3:
                $tipo_usuario = "id_usuario_subcliente";
                break;
        }
        $eliminacion = array(
            'creacion' => $date,
            $tipo_usuario => $id_usuario,
            'id_cliente' => $id_cliente,
            'id_candidato' => $id_candidato,
            'motivo' => $motivo
        );
        $this->candidato_model->accionCandidato($eliminacion);
        $cand = array(
            'edicion' => $date,
            'eliminado' => 1
        );
        $this->candidato_model->saveCandidato($cand, $id_candidato);
        echo $res = 1;
    }
    function getCandidatosEliminados(){
        $id_cliente = $_POST['id_cliente'];
        $salida = "";
        $data['eliminados'] = $this->candidato_model->getCandidatosEliminados($id_cliente);
        if($data['eliminados']){
            $salida .= '<table class="table table-striped">';
            $salida .= '<thead>';
            $salida .= '<tr>';
            $salida .= '<th scope="col">Candidato</th>';
            $salida .= '<th scope="col">Fecha</th>';
            $salida .= '<th scope="col" width="40%">Motivo</th>';
            $salida .= '<th scope="col">Usuario</th>';
            $salida .= '</tr>';
            $salida .= '</thead>';
            $salida .= '<tbody>';
            foreach($data['eliminados'] as $e){
                $fecha = fecha_sinhora_espanol_bd($e->fecha_eliminado);
                $salida .= "<tr><th>".$e->candidato."</th><th>".$fecha."</th><th>".$e->motivo."</th><th>".$e->usuario."</th></tr>";
            }
            $salida .= '</tbody>';
            $salida .= '</table>';
            echo $salida;
        }
        else{
            echo $salida .= '<p style="text-align:center">No hay registros eliminados</p>';
        }
    } 
    function getCandidatosEliminadosTATA(){
        $id_cliente = $_POST['id_cliente'];
        $salida = "";
        $data['eliminados'] = $this->candidato_model->getCandidatosEliminadosTATA($id_cliente);
        if($data['eliminados']){
            $salida .= '<table class="table table-striped">';
            $salida .= '<thead>';
            $salida .= '<tr>';
            $salida .= '<th scope="col">Candidato</th>';
            $salida .= '<th scope="col">Fecha</th>';
            $salida .= '<th scope="col" width="40%">Motivo</th>';
            $salida .= '<th scope="col">Usuario</th>';
            $salida .= '</tr>';
            $salida .= '</thead>';
            $salida .= '<tbody>';
            foreach($data['eliminados'] as $e){
                $fecha = fecha_sinhora_espanol_bd($e->fecha_eliminado);
                $salida .= "<tr><th>".$e->candidato."</th><th>".$fecha."</th><th>".$e->motivo."</th><th>".$e->usuario."</th></tr>";
            }
            $salida .= '</tbody>';
            $salida .= '</table>';
            echo $salida;
        }
        else{
            echo $salida .= '<p style="text-align:center">No hay registros eliminados</p>';
        }
    } 
    
    
    
    

    

    
    /************************************************ Rules Validate Form ************************************************/

    //Regla para nombres con espacios
    function alpha_space_only($str){
        if (!preg_match("/^[a-zA-Z ]+$/",$str)){
            $this->form_validation->set_message('alpha_space_only', 'El campo {field} debe estar compuesto solo por letras y espacios y no debe estar vacío');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    function alpha_space_only_english($str){
        if (!preg_match("/^[a-zA-Z ]+$/",$str)){
            $this->form_validation->set_message('alpha_space_only_english', '{field} does not must be alfanumeric');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    function required_file(){
        $this->form_validation->set_message('required_file', 'Carga el CV o solicitud de empleo del candidato');
        if (empty($_FILES['cv']['name'])) {
                return FALSE;
            }else{
                return TRUE;
            }
    }
    function string_values($str){
        if (!preg_match("/^\d+$|^[\d,\d]+$/",$str)){
            //$this->form_validation->set_message('string_values', 'El campo {field} no es válido');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    function date_format_es($str){
        if (!preg_match("/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/i",$str)){
            $this->form_validation->set_message('date_format_es', 'El campo {field} no es una fecha válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }    
    function subirNonDisclosure(){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $_POST['id_candidato'];
        $prefijo = str_replace(' ', '', $_POST['prefijo']);
        $countfiles = count($_FILES['avisos']['name']);
  
        for($i = 0; $i < $countfiles; $i++){
            if(!empty($_FILES['avisos']['name'][$i])){
                // Define new $_FILES array - $_FILES['file']
                $_FILES['file']['name'] = $_FILES['avisos']['name'][$i];
                $_FILES['file']['type'] = $_FILES['avisos']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['avisos']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['avisos']['error'][$i];
                $_FILES['file']['size'] = $_FILES['avisos']['size'][$i];
                $aux2 = str_replace(' ', '', $_FILES['avisos']['name'][$i]);
                $extension = pathinfo($_FILES['avisos']['name'][$i], PATHINFO_EXTENSION);
                $nombre_archivo = $prefijo."_".$i."_non_disclosure.".$extension;
                // Set preference
                $config['upload_path'] = './_docs/'; 
                $config['allowed_types'] = 'pdf|jpeg|jpg|png';
                //$config['max_size'] = '15000'; // max_size in kb
                $config['file_name'] = $nombre_archivo;
                //Load upload library
                $this->load->library('upload',$config); 
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('file')){
                   // $data = $this->upload->data(); 
                    echo $salida = 1; 
                }
                $doc = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_candidato' => $id_candidato,
                    'id_tipo_documento' => 8,
                    'archivo' => $nombre_archivo
                );
                $this->candidato_model->insertDocCandidato($doc);
            }
        }
    }
}