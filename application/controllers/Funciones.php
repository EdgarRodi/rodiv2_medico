<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funciones extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

  function generarPassword(){
    //Se define una cadena de caractares.
    //Os recomiendo desordenar las minúsculas, mayúsculas y números para mejorar la probabilidad.
    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    //Obtenemos la longitud de la cadena de caracteres
    $longitudCadena=strlen($cadena);
  
    //Definimos la variable que va a contener la contraseña
    $pass = "";
    //Se define la longitud de la contraseña, puedes poner la longitud que necesites
    //Se debe tener en cuenta que cuanto más larga sea más segura será.
    $longitudPass=12;
  
    //Creamos la contraseña recorriendo la cadena tantas veces como hayamos indicado
    for($i=1 ; $i<=$longitudPass ; $i++){
        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
        $pos=rand(0,$longitudCadena-1);
  
        //Vamos formando la contraseña con cada carácter aleatorio.
        $pass .= substr($cadena,$pos,1);
    }
    
    echo $pass;
  }
  function getMunicipios(){
		$id_estado = $this->input->post('id_estado');
		$data['municipios'] = $this->funciones_model->getMunicipios($id_estado);
		$salida = "<option value=''>Selecciona</option>";
		if($data['municipios']){
			foreach ($data['municipios'] as $row){
				$salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
			} 
	        echo $salida;
	    }
	    else{
	    	echo $salida;
	    }
  }
  
}