<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cat_Puestos extends CI_Controller{

	function __construct(){
		parent::__construct();
	}
  function index(){
    $data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
    $data['permisos_subclientes'] = $this->usuario_model->getPermisosSubclientes($this->session->userdata('id'));
    $data['accesos'] = $this->usuario_model->getModulos($this->session->userdata('idrol'));
    foreach($data['accesos'] as $acceso) {
        $items[] = $acceso->id_operaciones;
    }
    $data['acceso'] = $items;
    $vista['modals'] = $this->load->view('modals/mdl_puestos','', TRUE);
    $this->load
    ->view('adminpanel/header',$data)
    ->view('adminpanel/scripts')
    ->view('catalogos/puestos', $vista)
    ->view('adminpanel/footer');
  }
  function getPuestos(){
    $p['recordsTotal'] = $this->cat_puestos_model->getTotal();
    $p['recordsFiltered'] = $this->cat_puestos_model->getTotal();
    $p['data'] = $this->cat_puestos_model->getPuestosActivos();
    $this->output->set_output( json_encode( $p ) );
  } 
  function registrar(){
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|is_unique[puesto.nombre]');

    $this->form_validation->set_message('required', 'El campo %s es obligatorio');
    $this->form_validation->set_message('is_unique', 'El campo %s debe ser único');

    $msj = array();
    if ($this->form_validation->run() == FALSE) {
      $msj = array(
        'codigo' => 0,
        'msg' => validation_errors()
      );
    } else {
      $id_usuario = $this->session->userdata('id');
      date_default_timezone_set('America/Mexico_City');
      $date = date('Y-m-d H:i:s');
      $puesto = array(
        'creacion' => $date,
        'edicion' => $date,
        'id_usuario' => $id_usuario,
        'nombre' => $this->input->post('nombre')
      );
      $this->cat_puestos_model->registrar($puesto);
      $msj = array(
        'codigo' => 1,
        'msg' => 'success'
      );
    }
    echo json_encode($msj);
  }
  function editar(){
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|is_unique[puesto.nombre]');

    $this->form_validation->set_message('required','El campo %s es obligatorio');
    $this->form_validation->set_message('is_unique','El campo %s está repetido o es el mismo a ingresar');

    $msj = array();
    if ($this->form_validation->run() == FALSE) {
        $msj = array(
            'codigo' => 0,
            'msg' => validation_errors()
        );
    } 
    else {
        $id_usuario = $this->session->userdata('id');
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $puesto = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'nombre' => $this->input->post('nombre')
        );
        $this->cat_puestos_model->editar($puesto, $this->input->post('id'));
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    echo json_encode($msj);
  }
  function accion(){
    $id_usuario = $this->session->userdata('id');
    date_default_timezone_set('America/Mexico_City');
    $date = date('Y-m-d H:i:s');
    $accion = $this->input->post('accion');
    if($accion == "desactivar"){
        $puesto = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'status' => 0
        );
        $this->cat_puestos_model->editar($puesto, $this->input->post('id'));
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    if($accion == "activar"){
        $puesto = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'status' => 1
        );
        $this->cat_puestos_model->editar($puesto, $this->input->post('id'));
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    if($accion == "eliminar"){
        $puesto = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'eliminado' => 1
        );
        $this->cat_puestos_model->editar($puesto, $this->input->post('id'));
        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
    }
    echo json_encode($msj);
  }
}