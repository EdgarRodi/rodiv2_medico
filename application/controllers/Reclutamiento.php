<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reclutamiento extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function index(){
		$data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
    	if($data['permisos']){
    		foreach($data['permisos'] as $p){
    			if($p->nombreCliente == "UST Global"){
    				$data['cliente'] = $p->nombreCliente;
    			}
    		}
    	}
		$this->load
		->view('adminpanel/header',$data)
		->view('adminpanel/scripts')
		->view('reclutamiento/recluta_index')
		->view('adminpanel/footer');
	}
	function requisicion(){
		$data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
    	if($data['permisos']){
    		foreach($data['permisos'] as $p){
    			if($p->nombreCliente == "UST Global"){
    				$data['cliente'] = $p->nombreCliente;
    			}
    		}
    	}
    	$data['estados'] = $this->cliente_model->getEstados();
    	$data['pagos'] = $this->reclutamiento_model->getTiposPago();
    	$data['escolaridades'] = $this->reclutamiento_model->getEscolaridades();
    	$data['niveles'] = $this->reclutamiento_model->getNivelesEscolares();
    	$data['generos'] = $this->reclutamiento_model->getGeneros();
    	$data['civiles'] = $this->reclutamiento_model->getCiviles();
    	$data['licencias'] = $this->reclutamiento_model->getLicencias();
    	$data['discapacidades'] = $this->reclutamiento_model->getDiscapacidades();
    	$data['causas'] = $this->reclutamiento_model->getCausas();
    	$data['jornadas'] = $this->reclutamiento_model->getJornadas();
    	$data['sueldos'] = $this->reclutamiento_model->getSueldos();
    	$data['prestaciones'] = $this->reclutamiento_model->getPrestaciones();
    	$data['tipos_competencias'] = $this->reclutamiento_model->getCompetencias();
    	$salida = "";
    	if($data['tipos_competencias']){
    		foreach($data['tipos_competencias'] as $c){
                $salida .= '
                        <div class="col-md-4">
                            <label class="contenedor_check">'.$c->nombre.'
                                <input type="checkbox" name="competencia'.$c->id.'" id="competencia'.$c->id.'" value="'.$c->id.'">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                ';
            }
            $data['competencias'] = $salida;
    	}
    	else{
    		$data['competencias'] = $salida;
    	}
		$this->load
		->view('adminpanel/header',$data)
		->view('adminpanel/scripts')
		->view('reclutamiento/requisicion')
		->view('adminpanel/footer');
	}
	function getMunicipios(){
		$id_estado = $_POST['id_estado'];
		$data['municipios'] = $this->candidato_model->getMunicipios($id_estado);
		$salida = "<option value=''>Selecciona</option>";
		if($data['municipios']){
			foreach ($data['municipios'] as $row){
				$salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
			} 
	        echo $salida;
	    }
	    else{
	    	echo $salida;
	    }
	}
	function getRequisiciones(){
		$req['recordsTotal'] = $this->reclutamiento_model->getRequisicionesTotal();
		$req['recordsFiltered'] = $this->reclutamiento_model->getRequisicionesTotal();
		$req['data'] = $this->reclutamiento_model->getRequisiciones();
		$this->output->set_output( json_encode( $req ) );
	}
	function getSueldoVariables(){
		$sueldo = $_POST['sueldo'];
		$data['variables'] = $this->reclutamiento_model->getSueldoVariables($sueldo);
		$salida = "<option value=''>Selecciona</option>";
		if($data['variables']){
			foreach ($data['variables'] as $row){
				$salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
			} 
	        echo $salida;
	    }
	    else{
	    	echo $salida;
	    }
	}
	function matchCliente(){
		if(isset($_GET['term'])){
	      	//$term = strtoupper($_GET['term']);
	      	$term = $_GET['term'];
	      	//echo $id;
	      	echo json_encode($this->reclutamiento_model->matchCliente($term));
	    }
	}
	function getDatosCliente(){
		$id_cliente = $_POST['id'];
		$data['datos'] = $this->reclutamiento_model->getDatosCliente($id_cliente);
		$salida = "";
		if($data['datos']){
			foreach ($data['datos'] as $row){
				$salida .= $row->calle.",".$row->exterior.",".$row->interior.",".$row->colonia.",".$row->id_estado.",".$row->id_municipio.",".$row->cp.",".$row->telefono.",".$row->correo.",".$row->contacto.",".$row->rfc.",".$row->razon_social;
			} 
	        echo $salida;
	    }
	    else{
	    	echo $salida;
	    }
	}
	function crearRequisicion(){
		date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_usuario = $this->session->userdata('id');
        $cadena = $this->input->post('datos');
        parse_str($cadena, $dato);
        $habilidad = $this->input->post('habilidades');
        $idioma = $this->input->post('idiomas');
        //var_dump($this->input->post('habilidades'));

        if($dato['id_cliente'] == ""){

        }
        else{
        	$cliente = array(
        		'edicion' => $date,
                'id_usuario' => $id_usuario,
                'nombre' => $dato['nombre'],
        		'empresa' => $dato['nombre'],
        		'razon_social' => $dato['razon'],
        		'calle' => $dato['calle'],
        		'exterior' => $dato['exterior'],
        		'interior' => $dato['interior'],
        		'colonia' => $dato['colonia'],
        		'id_estado' => $dato['estado'],
        		'id_municipio' => $dato['municipio'],
        		'cp' => $dato['cp'],
        		'telefono' => $dato['telefono'],
        		'correo' => $dato['correo'],
        		'contacto' => $dato['contacto'],
        		'rfc' => $dato['rfc']
        	);
        	$this->reclutamiento_model->updateCliente($cliente, $dato['id_cliente']);

        	$requisicion = array(
        		'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_cliente' => $dato['id_cliente'],
        		'id_tipo_pago' => $dato['pago'],
        		'uso_cfdi' => $dato['cfdi'],
        		'puesto' => $dato['puesto'],
        		'numero_vacantes' => $dato['vacantes'],
        		'id_tipo_escolaridad' => $dato['escolaridad'],
        		'id_tipo_nivel_escolar' => $dato['nivel'],
        		'otro_nivel_escolar' => $dato['otro_nivel_escolar'],
        		'carrera' => $dato['carrera'],
        		'otros_estudios' => $dato['otros_estudios'],
        		'id_genero' => $dato['genero'],
        		'id_estado_civil' => $dato['civil'],
        		'edad_minima' => $dato['minima'],
        		'edad_maxima' => $dato['maxima'],
        		'id_tipo_licencia' => $dato['licencia'],
        		'id_tipo_discapacidad' => $dato['discapacidad'],
        		'id_tipo_causa_vacante' => $dato['causa'],
        		'residencia' => $dato['residencia'],
        		'id_tipo_jornada' => $dato['jornada'],
        		'desde_hora' => $dato['hora_inicial'],
        		'hasta_hora' => $dato['hora_final'],
        		'desde_dia' => $dato['dia_inicial'],
        		'hasta_dia' => $dato['dia_final'],
        		'dias_descanso' => $dato['descanso'],
        		'viajar' => $dato['viajar'],
        		'rolar_turnos' => $dato['rolar'],
        		'zona_trabajo' => $dato['zona'],
        		'id_tipo_sueldo' => $dato['sueldo'],
        		'sueldo_variable' => $dato['sueldo_variable'],
        		'variable_cantidad' => $dato['variable_monto'],
        		'sueldo_minimo' => $dato['sueldo_min'],
        		'sueldo_maximo' => $dato['sueldo_max'],
        		'id_tipo_prestaciones' => $dato['prestaciones'],
        		'otras_prestaciones' => $dato['otras_prestaciones'],
        		'experiencia' => $dato['experiencia'],
        		'actividades' => $dato['actividades'],
        		'observaciones' => $dato['observaciones']
        	);
        	$id_requisicion = $this->reclutamiento_model->crearRequisicion($requisicion);

        	$requisito = array(
        		'creacion' => $date,
                'edicion' => $date,
                'id_cliente' => $dato['id_cliente'],
                'id_requisicion' => $id_requisicion,
        		'requisito' => $dato['requisito']
        	);
        	$this->reclutamiento_model->insertRequisito($requisito);

        	$hab = explode('@@', $habilidad);
        	for($i = 0; $i < count($hab); $i++){
        		if($hab[$i] != ""){
        			$h = explode(',', $hab[$i]);
        			$item = array(
        				'id_requisicion' => $id_requisicion,
        				'nombre' => $h[0],
        				'porcentaje' => $h[1]
        			);
        			$this->reclutamiento_model->insertHabilidades($item);
        		}
        	}
        	$idi = explode('@@', $idioma);
        	for($i = 0; $i < count($idi); $i++){
        		if($idi[$i] != ""){
        			$aux = explode(',', $idi[$i]);
        			$item = array(
        				'id_requisicion' => $id_requisicion,
        				'nombre' => $aux[0],
        				'porcentaje' => $aux[1]
        			);
        			$this->reclutamiento_model->insertIdiomas($item);
        		}
        	}
        	for($i = 1; $i <= 20; $i++){
        		if(isset($dato['competencia'.$i.'']))
        			$this->reclutamiento_model->insertCompetencia($i, $id_requisicion);
        	}
        }
	}
}