<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doping extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function index(){
		$data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
        $data['permisos_subclientes'] = $this->usuario_model->getPermisosSubclientes($this->session->userdata('id'));
        $data['accesos'] = $this->usuario_model->getModulos($this->session->userdata('idrol'));
        foreach($data['accesos'] as $acceso) {
            $items[] = $acceso->id_operaciones;
        }
        $data['acceso'] = $items;
        $info['candidatos'] = $this->doping_model->getCandidatosSinDoping();
        $info['paquetes'] = $this->doping_model->getPaquetesAntidoping();
        $info['clientes'] = $this->funciones_model->getClientesActivos();
        $info['identificaciones'] = $this->funciones_model->getTiposIdentificaciones();
        $info['finalizados'] = $this->doping_model->getDopingsFinalizados();
        $datos['modals'] = $this->load->view('modals/mdl_doping',$info, TRUE);

        
		$this->load
		->view('adminpanel/header',$data)
		->view('adminpanel/scripts')
		->view('doping/doping',$datos)
		->view('adminpanel/footer');
	}
	function getDopings(){
		$dop['recordsTotal'] = $this->doping_model->getDopingsTotal();
        $dop['recordsFiltered'] = $this->doping_model->getDopingsTotal();
        $dop['data'] = $this->doping_model->getDopings();
        $this->output->set_output( json_encode( $dop ) );
	}
    function getAntidopingCandidato(){
        $id_candidato = $_POST['id_candidato'];
        $data['parametros'] = $this->doping_model->getParametrosCandidato($id_candidato);
        foreach($data['parametros'] as $p){
            $tipo = $p->tipo_antidoping;
            $parametros = $p->antidoping;
        }
        if($tipo == 1){
            $paquete = $this->doping_model->getPaqueteCandidato($parametros);
            $salida = '<span><b>Doping por paquete: </b></span><br>';
            $salida .= '<span><b>'.$paquete->nombre.'</b></span><br>';
            $aux = explode(',', $paquete->sustancias);
            $salida .= '<small>(';
            for($i = 0; $i < count($aux); $i++){
                $sustancia = $this->doping_model->getSustanciaCandidato($aux[$i]);
                $salida .= $sustancia->abreviatura.' ';
            }
            $salida .= ')</small><br><br>';
            echo $salida;

        }
        if($tipo == 2){
            $aux = explode(',', $parametros);
            $salida = '<span><b>Doping por parámetro: </b></span><br>';
            $salida .= '<span><b>';
            $extra = '(';
            for($i = 0; $i < count($aux); $i++){
                $sustancia = $this->doping_model->getSustanciaCandidato($aux[$i]);
                $salida .= $sustancia->abreviatura.' ';
                $extra .= ' '.$sustancia->descripcion;
            }
            $extra .= ' )';
            $salida .= '</b></span><br>'.$extra.'<br><br>';
            echo $salida;
        }
    }
    function getSubclientes(){
        $id_cliente = $_POST['id_cliente'];
        $data['subclientes'] = $this->doping_model->getSubclientes($id_cliente);
        $salida = "<option value='0' selected>Selecciona</option>";
        $salida .= "<option value='0'>N/A</option>";
        if($data['subclientes']){
            foreach ($data['subclientes'] as $row){
                $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            echo $salida;
        }
    }
    function getProyectos(){
        $id_cliente = $_POST['id_cliente'];
        $data['proyectos'] = $this->doping_model->getProyectos($id_cliente);
        $salida = "<option value='0' selected>Selecciona</option>";
        $salida .= "<option value='0'>N/A</option>";
        if($data['proyectos']){
            foreach ($data['proyectos'] as $row){
                $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            echo $salida;
        }
    }
    function getProyectosSubcliente(){
        $id_subcliente = $_POST['id_subcliente'];
        $data['proyectos'] = $this->doping_model->getProyectosSubcliente($id_subcliente);
        $salida = "<option value=''>Selecciona</option>";
        $salida .= "<option value='0'>N/A</option>";
        if($data['proyectos']){
            foreach ($data['proyectos'] as $row){
                $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            echo $salida;
        }
    }
    function getPaqueteCliente(){
        $id_cliente = $_POST['id_cliente'];
        $id_subcliente = $_POST['id_subcliente'];
        $id_proyecto = $_POST['id_proyecto'];
        $data['examenes'] = $this->doping_model->getPaqueteCliente($id_cliente, $id_subcliente, $id_proyecto);
        $salida = "";
        if($data['examenes']){
            foreach ($data['examenes'] as $row){
                $salida .= "<option value='".$row->id_antidoping_paquete."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            echo $salida;
        }
    }
    function getPaqueteSubcliente(){
        $id_cliente = $_POST['id_cliente'];
        $id_subcliente = $_POST['id_subcliente'];
        $id_proyecto = $_POST['id_proyecto'];
        $paq = $this->doping_model->getPaqueteSubcliente($id_cliente, $id_subcliente);
        $salida = "";
        if($paq){
            echo $paq->id_antidoping_paquete;
        }
        else{
            echo $salida;
        }
    }
    function getPaqueteSubclienteProyecto(){
        $id_cliente = $_POST['id_cliente'];
        $id_subcliente = $_POST['id_subcliente'];
        $id_proyecto = $_POST['id_proyecto'];
        $paq = $this->doping_model->getPaqueteSubclienteProyecto($id_cliente, $id_proyecto, $id_subcliente);
        $salida = "";
        //$salida .= "<option value='0'>N/A</option>";
        if($paq){
            echo $paq->id_antidoping_paquete;
        }
        else{
            echo $salida;
        }
    }
    function registrarNuevo(){        
        $this->form_validation->set_rules('nombre', 'Nombre(s)', 'required|trim');
        $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim');
        $this->form_validation->set_rules('materno', 'Segundo apellido', 'trim');
        $this->form_validation->set_rules('paquete', 'Parametros', 'required');
        $this->form_validation->set_rules('cliente', 'Cliente', 'required');
        $this->form_validation->set_rules('subcliente', 'Subcliente', 'required');
        $this->form_validation->set_rules('proyecto', 'Proyecto', 'required');
        $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required');
        $this->form_validation->set_rules('ine', 'Numero, licencia o código', 'required');
        $this->form_validation->set_rules('identificacion', 'Tipo de identificacion', 'required');
        $this->form_validation->set_rules('razon', 'Razon', 'required');
        $this->form_validation->set_rules('medicamentos', 'Medicamentos', 'required');
        $this->form_validation->set_rules('fecha_doping', 'Fecha de doping', 'required');

        $this->form_validation->set_message('required','El campo %s es obligatorio');

        if ($this->form_validation->run() == FALSE) {
            $msj = array(
                'codigo' => 0,
                'msg' => validation_errors()
            );
        } 
        else{
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $num = $this->doping_model->countDopings();
            $foto = '';
            if($this->input->post('fecha_nacimiento') != "" && $this->input->post('fecha_nacimiento') != null){
                $f_nacimiento = fecha_espanol_bd($this->input->post('fecha_nacimiento'));
                $edad = $this->calculaEdad($f_nacimiento);
            }
            else{
                $f_nacimiento = "";
                $edad = 0;
            }
            $conf = $this->funciones_model->getConfiguraciones();
            $f_doping = fecha_hora_espanol_bd($this->input->post('fecha_doping'));
            $datos_candidato = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_usuario_cliente' => 0,
                'id_cliente' => $this->input->post('cliente'),
                'id_subcliente' => $this->input->post('subcliente'),
                'id_proyecto' => $this->input->post('proyecto'),
                'fecha_alta' => $date,
                'nombre' => $this->input->post('nombre'),
                'paterno' => $this->input->post('paterno'),
                'materno' => $this->input->post('materno'),
                'correo' => 'Sin correo',
                'fecha_nacimiento' => $f_nacimiento,
                'edad' => $edad
            );
            $id_candidato = $this->doping_model->insertCandidato($datos_candidato);
            $data['candidato'] = $this->doping_model->getDatosCandidato($id_candidato);
            foreach($data['candidato'] as $row){
                $clave_cliente = $row->claveCliente;
                $clave_subcliente = $row->claveSubcliente;
            }
            if(isset($_FILES['foto'])){
                $file_ext = pathinfo($_FILES["foto"]["name"], PATHINFO_EXTENSION);
                $nombre = str_replace(' ', '', $this->input->post('nombre'));
                $paterno = str_replace(' ', '', $this->input->post('paterno'));
                $nombre_archivo = $id_candidato."_".$nombre."_".$paterno."_Foto.".$file_ext;

                $config['upload_path'] = './_doping/';  
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = $nombre_archivo;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('foto')){
                    $data = $this->upload->data();
                }
                $foto = $nombre_archivo;
            }
            $doping = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'id_cliente' => $this->input->post('cliente'),
                'id_subcliente' => $this->input->post('subcliente'),
                'id_proyecto' => $this->input->post('proyecto'),
                'id_antidoping_paquete' => $this->input->post('paquete'),
                'fecha_doping' => $f_doping,
                'id_tipo_identificacion' => $this->input->post('identificacion'),
                'ine' => $this->input->post('ine'),
                'foto' => $foto,
                'razon' => $this->input->post('razon'),
                'medicamentos' => $this->input->post('medicamentos'),
                'folio' => ($num->id + 1),
                'codigo_prueba' => 'AD-'.$clave_cliente.'-'.$clave_subcliente.'-'.($num->id + 1).'-'.substr(date('Y'), -2),
                'comentarios' => $this->input->post('comentarios'),
                'firma_responsable' => $conf->firma_responsable_doping,
                'cedula_responsable' => $conf->cedula_responsable_doping
            );
            $id_doping = $this->doping_model->insertDoping($doping);
            $pruebas = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_usuario_cliente' => 0,
                'id_candidato' => $id_candidato,
                'id_cliente' => $this->input->post('cliente'),
                'tipo_antidoping' => 1,
                'antidoping' => $this->input->post('paquete'),
                'status_doping' => 1,
                'psicometrico' => 0,
                'socioeconomico' => 0
            );
            $this->doping_model->insertCandidatoPruebas($pruebas);
            
            $paquete = $this->doping_model->getPaqueteCandidato($this->input->post('paquete'));
            $aux = explode(',', $paquete->sustancias);
            for($i = 0; $i < count($aux); $i++){
                $sustancia = $this->doping_model->getSustanciaCandidato($aux[$i]);
                $detalle = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_doping' => $id_doping,
                    'id_candidato' => $id_candidato,
                    'id_sustancia' => $sustancia->id
                );
                $this->doping_model->insertDetalleDoping($detalle);
            }               
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
        }
        echo json_encode($msj);
    }
    function editarRegistro(){
        $this->form_validation->set_rules('nombre', 'Nombre(s)', 'required|trim');
        $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim');
        $this->form_validation->set_rules('materno', 'Segundo apellido', 'trim');
        $this->form_validation->set_rules('paquete', 'Parametros', 'required');
        $this->form_validation->set_rules('cliente', 'Cliente', 'required');
        $this->form_validation->set_rules('subcliente', 'Subcliente', 'required');
        $this->form_validation->set_rules('proyecto', 'Proyecto', 'required');
        $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required');
        $this->form_validation->set_rules('ine', 'Numero, licencia o código', 'required');
        $this->form_validation->set_rules('identificacion', 'Tipo de identificacion', 'required');
        $this->form_validation->set_rules('razon', 'Razon', 'required');
        $this->form_validation->set_rules('medicamentos', 'Medicamentos', 'required');
        $this->form_validation->set_rules('fecha_doping', 'Fecha de doping', 'required');
        $this->form_validation->set_rules('id_doping', 'ID del doping', 'required');

        $this->form_validation->set_message('required','El campo %s es obligatorio');

        if ($this->form_validation->run() == FALSE) {
            $msj = array(
                'codigo' => 0,
                'msg' => validation_errors()
            );
        } 
        else{
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $id_candidato = $this->input->post('id_candidato');
            $id_doping = $this->input->post('id_doping');
            if($this->input->post('fecha_nacimiento') != "" && $this->input->post('fecha_nacimiento') != null){
                $f_nacimiento = fecha_espanol_bd($this->input->post('fecha_nacimiento'));
                $edad = $this->calculaEdad($f_nacimiento);
            }
            else{
                $f_nacimiento = "";
                $edad = "";
            }
            $f_doping = fecha_hora_espanol_bd($this->input->post('fecha_doping'));
            $datos_candidato = array(
                'edicion' => $date,
                'id_cliente' => $this->input->post('cliente'),
                'id_subcliente' => $this->input->post('subcliente'),
                'fecha_alta' => $date,
                'nombre' => $this->input->post('nombre'),
                'paterno' => $this->input->post('paterno'),
                'materno' => $this->input->post('materno'),
                'fecha_nacimiento' => $f_nacimiento,
                'edad' => $edad
            );
            $this->doping_model->updateCandidato($datos_candidato, $id_candidato);

            $data['candidato'] = $this->doping_model->getDatosCandidato($id_candidato);
            foreach($data['candidato'] as $row){
                $clave_cliente = $row->claveCliente;
                $clave_subcliente = $row->claveSubcliente;
            }
            if(isset($_FILES['foto'])){
                $file_ext = pathinfo($_FILES["foto"]["name"], PATHINFO_EXTENSION);
                $nombre = str_replace(' ', '', $this->input->post('nombre'));
                $paterno = str_replace(' ', '', $this->input->post('paterno'));
                $nombre_archivo = $id_candidato."_".$nombre."_".$paterno."_Foto.".$file_ext;
            
                $config['upload_path'] = './_doping/';  
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = $nombre_archivo;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('foto')){
                    $data = $this->upload->data();
                }
                $foto = array(
                    'foto' => $nombre_archivo
                );
                $this->doping_model->updateDoping($foto, $id_doping);
            }
            $doping = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'id_cliente' => $this->input->post('cliente'),
                'id_subcliente' => $this->input->post('subcliente'),
                'id_proyecto' => $this->input->post('proyecto'),
                'id_antidoping_paquete' => $this->input->post('paquete'),
                'fecha_doping' => $f_doping,
                'id_tipo_identificacion' => $this->input->post('identificacion'),
                'ine' => $this->input->post('ine'),
                'razon' => $this->input->post('razon'),
                'medicamentos' => $this->input->post('medicamentos'),
                'codigo_prueba' => 'AD-'.$clave_cliente.'-'.$clave_subcliente.'-'.$id_doping.'-'.substr(date('Y'), -2),
                'comentarios' => $this->input->post('comentarios')
            );
            $this->doping_model->updateDoping($doping, $id_doping);

            $pruebas = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_cliente' => $this->input->post('cliente'),
                'antidoping' => $this->input->post('paquete'),
            );
            $this->doping_model->updateCandidatoPruebas($pruebas, $id_candidato);

            $this->doping_model->deleteDopingDetalle($id_doping);
            $paquete = $this->doping_model->getPaqueteCandidato($this->input->post('paquete'));
            $aux = explode(',', $paquete->sustancias);
            for($i = 0; $i < count($aux); $i++){
                $sustancia = $this->doping_model->getSustanciaCandidato($aux[$i]);
                $detalle = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'id_doping' => $id_doping,
                    'id_candidato' => $id_candidato,
                    'id_sustancia' => $sustancia->id,
                    'resultado' => 0
                );
                $this->doping_model->insertDetalleDoping($detalle);
            }               
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
        }
        echo json_encode($msj);
    }
    function registrarPendiente(){
        $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required');
        $this->form_validation->set_rules('ine', 'Numero, licencia o código', 'required');
        $this->form_validation->set_rules('identificacion', 'Tipo de identificacion', 'required');
        $this->form_validation->set_rules('razon', 'Razon', 'required');
        $this->form_validation->set_rules('medicamentos', 'Medicamentos', 'required');
        $this->form_validation->set_rules('fecha_doping', 'Fecha de doping', 'required');

        $this->form_validation->set_message('required','El campo %s es obligatorio');

        if ($this->form_validation->run() == FALSE) {
            $msj = array(
                'codigo' => 0,
                'msg' => validation_errors()
            );
        }
        else{
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $num = $this->doping_model->countDopings();
            $foto = '';
            if($this->input->post('fecha_nacimiento') != "" && $this->input->post('fecha_nacimiento') != null){
                $f_nacimiento = fecha_espanol_bd($this->input->post('fecha_nacimiento'));
                $edad = $this->calculaEdad($f_nacimiento);
                $candidato_fnacimiento = array(
                    'fecha_nacimiento' => $f_nacimiento,
                    'edad' => $edad
                );
                $this->candidato_model->editarCandidato($candidato_fnacimiento, $this->input->post('id_candidato'));
            }
            else{
                $f_nacimiento = "";
                $edad = 0;
            }
            $f_doping = fecha_hora_espanol_bd($this->input->post('fecha_doping'));
            $conf = $this->funciones_model->getConfiguraciones();

            $data['candidato'] = $this->doping_model->getDatosCandidato($this->input->post('id_candidato'));
            foreach($data['candidato'] as $row){
                $nombre = $row->nombre;
                $paterno = $row->paterno;
                $cliente = $row->id_cliente;
                $subcliente = $row->id_subcliente;
                $proyecto = $row->id_proyecto;
                $clave_cliente = $row->claveCliente;
                $clave_subcliente = $row->claveSubcliente;
                $paquete = $row->antidoping;
            }
            if(isset($_FILES["foto"])){
                $file_ext = pathinfo($_FILES["foto"]["name"], PATHINFO_EXTENSION);
                $nombre = str_replace(' ', '', $nombre);
                $paterno = str_replace(' ', '', $paterno);
                $nombre_archivo = $this->input->post('id_candidato')."_".$nombre."".$paterno."_Foto.".$file_ext;

                $config['upload_path'] = './_doping/';  
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = $nombre_archivo;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('foto')){
                    $data = $this->upload->data();
                }
                $foto = $nombre_archivo;
            }
            $doping = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $this->input->post('id_candidato'),
                'id_cliente' => $cliente,
                'id_subcliente' => $subcliente,
                'id_proyecto' => $proyecto,
                'id_antidoping_paquete' => $paquete,
                'fecha_doping' => $f_doping,
                'id_tipo_identificacion' => $this->input->post('identificacion'),
                'ine' => $this->input->post('ine'),
                'foto' => $foto,
                'razon' => $this->input->post('razon'),
                'medicamentos' => $this->input->post('medicamentos'),
                'folio' => ($num->id + 1),
                'codigo_prueba' => 'AD-'.$clave_cliente.'-'.$clave_subcliente.'-'.($num->id + 1).'-'.substr(date('Y'), -2),
                'comentarios' => $this->input->post('comentarios'),
                'firma_responsable' => $conf->firma_responsable_doping,
                'cedula_responsable' => $conf->cedula_responsable_doping
            );
            $id_doping = $this->doping_model->insertDoping($doping);
            $prueba = array(
                'status_doping' => 1
            );
            $this->doping_model->updatePruebaCandidato($this->input->post('id_candidato'), $prueba);        

            $data['parametros'] = $this->doping_model->getParametrosCandidato($this->input->post('id_candidato'));
            foreach($data['parametros'] as $p){
                $tipo = $p->tipo_antidoping;
                $parametros = $p->antidoping;
            }
            if($tipo == 1){
                $paquete = $this->doping_model->getPaqueteCandidato($parametros);
                $aux = explode(',', $paquete->sustancias);
                for($i = 0; $i < count($aux); $i++){
                    $sustancia = $this->doping_model->getSustanciaCandidato($aux[$i]);
                    $detalle = array(
                        'creacion' => $date,
                        'edicion' => $date,
                        'id_usuario' => $id_usuario,
                        'id_doping' => $id_doping,
                        'id_candidato' => $this->input->post('id_candidato'),
                        'id_sustancia' => $sustancia->id
                    );
                    $this->doping_model->insertDetalleDoping($detalle);
                }
            }
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
        }
        echo json_encode($msj);
    }
    function editarPendiente(){
        $this->form_validation->set_rules('ine', 'Numero, licencia o código', 'required');
        $this->form_validation->set_rules('identificacion', 'Tipo de identificacion', 'required');
        $this->form_validation->set_rules('razon', 'Razon', 'required');
        $this->form_validation->set_rules('medicamentos', 'Medicamentos', 'required');
        $this->form_validation->set_rules('fecha_doping', 'Fecha de doping', 'required');

        $this->form_validation->set_message('required','El campo %s es obligatorio');

        if ($this->form_validation->run() == FALSE) {
            $msj = array(
                'codigo' => 0,
                'msg' => validation_errors()
            );
        }
        else{
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $id_doping = $this->input->post('id_doping');
            $id_candidato = $this->input->post('id_candidato');
            /*if($this->input->post('fecha_nacimiento') != "" && $this->input->post('fecha_nacimiento') != null){
                $f_nacimiento = fecha_espanol_bd($this->input->post('fecha_nacimiento'));
                $edad = $this->calculaEdad($f_nacimiento);
                $candidato_fnacimiento = array(
                    'fecha_nacimiento' => $f_nacimiento,
                    'edad' => $edad
                );
                $this->candidato_model->editarCandidato($candidato_fnacimiento, $this->input->post('id_candidato'));
            }*/
            $f_doping = fecha_hora_espanol_bd($this->input->post('fecha_doping'));

            $data['candidato'] = $this->doping_model->getDatosCandidato($id_candidato);
            foreach($data['candidato'] as $row){
                $nombre = $row->nombre;
                $paterno = $row->paterno;
            }
            if(isset($_FILES["foto"])){
                $file_ext = pathinfo($_FILES["foto"]["name"], PATHINFO_EXTENSION);
                $nombre = str_replace(' ', '', $nombre);
                $paterno = str_replace(' ', '', $paterno);
                $nombre_archivo = $this->input->post('id_candidato')."_".$nombre."".$paterno."_Foto.".$file_ext;

                $config['upload_path'] = './_doping/';  
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['overwrite'] = TRUE;
                $config['file_name'] = $nombre_archivo;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // File upload
                if($this->upload->do_upload('foto')){
                    $data = $this->upload->data();
                }
                $foto = array(
                    'foto' => $nombre_archivo
                );
                $this->doping_model->updateDoping($foto, $id_doping);
            }
            $doping = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'fecha_doping' => $f_doping,
                'id_tipo_identificacion' => $this->input->post('identificacion'),
                'ine' => $this->input->post('ine'),
                'razon' => $this->input->post('razon'),
                'medicamentos' => $this->input->post('medicamentos'),
                'comentarios' => $this->input->post('comentarios')
            );
            $this->doping_model->updateDoping($doping, $id_doping);
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
        }
        echo json_encode($msj);
    }
    function getClaveCliente(){
        $id_cliente = $_POST['id_cliente'];
        $id_subcliente = $_POST['id_subcliente'];
        $id_proyecto = $_POST['id_proyecto'];
        if($id_subcliente == 0){
            if($id_proyecto == 0){
                $cliente = $this->funciones_model->getClaveCliente($id_cliente);
                $doping = $this->doping_model->getLastDoping();
                $doping = ($doping == "" || $doping == null)? 0:$doping->id;
                $salida = "AD-".$cliente->clave."--".($doping + 1)."-".substr(date('Y'), -2);
            }
            else{
                $cliente = $this->funciones_model->getClaveProyecto($id_cliente, $id_proyecto);
                $doping = $this->doping_model->getLastDoping();
                $doping = ($doping == "" || $doping == null)? 0:$doping->id;
                $salida = "AD-".$cliente->claveCliente."--".($doping + 1)."-".substr(date('Y'), -2);
            }
        }
        else{
            if($id_proyecto == 0){
                $cliente = $this->funciones_model->getClaveSubcliente($id_cliente, $id_subcliente);
                $doping = $this->doping_model->getLastDoping();
                $doping = ($doping == "" || $doping == null)? 0:$doping->id;
                $salida = "AD-".$cliente->claveCliente."-".$cliente->claveSubcliente."-".($doping + 1)."-".substr(date('Y'), -2);
            }
            else{
                $subcliente = $this->funciones_model->getClaveSubclienteProyecto($id_cliente, $id_proyecto, $id_subcliente);
                $doping = $this->doping_model->getLastDoping();
                $doping = ($doping == "" || $doping == null)? 0:$doping->id;
                $salida = "AD-".$subcliente->claveCliente."-".$subcliente->claveSubcliente."-".($doping + 1)."-".substr(date('Y'), -2);
            }
        }
        echo $salida;
    }
    function checkPendienteDoping(){
        $es_pendiente = $this->doping_model->checkPendienteDoping($this->input->post('nombre'), $this->input->post('paterno'), $this->input->post('materno'));
        if($es_pendiente > 0){
            $msj = array(
                'codigo' => 1,
                'msg' => 'El candidato esta disponible en Pendientes'
            ); 
        }
        else{
            $msj = array(
                'codigo' => 0,
                'msg' => ''
            );
        }
        echo json_encode($msj);
    }
	function getFechaNacimiento(){
        $id_candidato = $_POST['id_candidato'];
        $f = $this->candidato_model->getFechaNacimiento($id_candidato);
        if($f->fecha_nacimiento != "" && $f->fecha_nacimiento != "0000-00-00"){
            $aux = explode('-', $f->fecha_nacimiento);
            $fnacimiento = $aux[2].'/'.$aux[1].'/'.$aux[0];
            echo $fnacimiento;
        }
        else{
            echo $fnacimiento = "";
        }
    }
    function calculaEdad($fechanacimiento){
        list($ano,$mes,$dia) = explode("-",$fechanacimiento);
        $ano_diferencia  = date("Y") - $ano;
        $mes_diferencia = date("m") - $mes;
        $dia_diferencia   = date("d") - $dia;
        if ($mes_diferencia < 0 || ($mes_diferencia == 0 && $dia_diferencia < 0 || $mes_diferencia < 0))
            $ano_diferencia--;
        return $ano_diferencia;
    }
    function getDopingCandidato(){
        $id_doping = $_POST['id_doping'];
        $id_candidato = $_POST['id_candidato'];
        $candidato = $this->doping_model->getDopingCandidato($id_doping);
        $data['parametros'] = $this->doping_model->getParametrosCandidato($id_candidato);
        foreach($data['parametros'] as $p){
            $tipo = $p->tipo_antidoping;
            $parametros = $p->antidoping;
        }
        if($tipo == 1){
            $paquete = $this->doping_model->getPaqueteCandidato($parametros);
            $doping = '<span><b>Doping: </b></span><br>';
            $doping .= '<span><b>'.$paquete->nombre.'</b></span><br>';
            $aux = explode(',', $paquete->sustancias);
            $doping .= '<span>(';
            for($i = 0; $i < count($aux); $i++){
                $sustancia = $this->doping_model->getSustanciaCandidato($aux[$i]);
                $doping .= $sustancia->abreviatura.' ';
            }
            $doping .= ')</span><br>';
        }
        $salida = $candidato->ine.'##'.$candidato->medicamentos.'##'.$candidato->folio.'##'.$candidato->comentarios.'##'.$doping.'##'.$candidato->identificacion;
        echo $salida;
    }
    function getSustanciasDoping(){
        $id_doping = $_POST['id_doping'];
        $id_candidato = $_POST['id_candidato'];
        $salida = "";
        $data['sustancias'] = $this->doping_model->getSustanciasDoping($id_doping);
        foreach($data['sustancias'] as $s){
            $sustancia = $this->doping_model->getSustanciaCandidato($s->id_sustancia);
            $salida .= '<div class="row">';
            $salida .= '<div class="col-md-6 col-md-offset-1 text-center">';
            $salida .= '<b>'.$sustancia->abreviatura.'</b><br><small>'.$sustancia->descripcion.'</small>';
            $salida .= '</div>';
            $salida .= '<div class="col-md-3">';
            $salida .= '<select id="sust'.$sustancia->id.'" name="sust'.$sustancia->id.'" class="form-control es_sustancia sust_obligado">';

            if($s->resultado != -1){
                if($s->resultado == 0){
                    $salida .= '<option value="'.$sustancia->id.':0" selected>Negativo</option>';
                    $salida .= '<option value="'.$sustancia->id.':1">Positivo</option>';
                    $salida .= '<option value="'.$sustancia->id.':2">Inválido</option>';
                }
                if($s->resultado == 1){
                    $salida .= '<option value="'.$sustancia->id.':1" selected>Positivo</option>';
                    $salida .= '<option value="'.$sustancia->id.':0">Negativo</option>';
                    $salida .= '<option value="'.$sustancia->id.':2">Inválido</option>';
                }
                if($s->resultado == 2){
                    $salida .= '<option value="'.$sustancia->id.':1">Positivo</option>';
                    $salida .= '<option value="'.$sustancia->id.':0">Negativo</option>';
                    $salida .= '<option value="'.$sustancia->id.':2" selected>Inválido</option>';
                }
            }
            else{
                $salida .= '<option value="">Selecciona resultado</option>';
                $salida .= '<option value="'.$sustancia->id.':0" selected>Negativo</option>';
                $salida .= '<option value="'.$sustancia->id.':1">Positivo</option>';
                $salida .= '<option value="'.$sustancia->id.':2">Inválido</option>';
            }
            $salida .= '</select><br>';
            $salida .= '</div>';
            $salida .= '</div>';
        }
        echo $salida;        
    }
    function registrarResultadosDoping(){
        $this->form_validation->set_rules('fecha_resultados', 'Fecha de resultados', 'required');
        $this->form_validation->set_rules('valores', 'Parámetros', 'required');
        $this->form_validation->set_message('required','El campo %s es obligatorio');

        if ($this->form_validation->run() == FALSE) {
            $msj = array(
                'codigo' => 0,
                'msg' => validation_errors()
            );
        }
        else{
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $id_doping = $this->input->post('id_doping');
            $cadena = $this->input->post('valores');
            $f_resultados = fecha_hora_espanol_bd($_POST['fecha_resultados']);
            $valores = explode(',', $cadena);
            $cont = 0;
            $invalido = 0;
            for($i = 0; $i < count($valores); $i++){
                $valor = explode(':', $valores[$i]);
                $res = array(
                    'edicion' => $date,
                    'id_usuario' => $id_usuario,
                    'resultado' => $valor[1]
                );
                $this->doping_model->updateResultadoDoping($id_doping, $valor[0], $res);
                if($valor[1] == 1){
                    $cont++;
                }
                if($valor[1] == 2){
                    $invalido++;
                }
            }
            if($cont > 0 && $invalido == 0){
                $doping = array(
                    'edicion' => $date,
                    //'id_usuario' => $id_usuario,
                    'fecha_resultado' => $f_resultados,
                    'resultado' => 1
                );
                $this->doping_model->updateDoping($doping, $id_doping);
            }
            if($cont == 0 && $invalido == 0){
                $doping = array(
                    'edicion' => $date,
                    //'id_usuario' => $id_usuario,
                    'fecha_resultado' => $f_resultados,
                    'resultado' => 0
                );
                $this->doping_model->updateDoping($doping, $id_doping);
            }
            if($invalido > 0){
                $doping = array(
                    'edicion' => $date,
                    //'id_usuario' => $id_usuario,
                    'fecha_resultado' => $f_resultados,
                    'resultado' => 2
                );
                $this->doping_model->updateDoping($doping, $id_doping);
            }
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
        }
        echo json_encode($msj);
    }
    function delete(){
        $this->form_validation->set_rules('motivo', 'Motivo', 'required');
        $this->form_validation->set_message('required','El campo %s es obligatorio');

        if ($this->form_validation->run() == FALSE) {
            $msj = array(
                'codigo' => 0,
                'msg' => validation_errors()
            );
        }
        else{
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $id_candidato = $this->input->post('id_candidato');
            $motivo = $this->input->post('motivo');
            $id_doping = $this->input->post('id_doping');
            $data['candidato'] = $this->doping_model->getDatosCandidato($id_candidato);
            foreach($data['candidato'] as $c){
                $nombre = $c->nombre;
                $paterno = $c->paterno;
                $materno = $c->materno;
                $cliente = $c->cliente;
                $subcliente = $c->subcliente;
                $proyecto = $c->proyecto;
            }
            $borrado = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'eliminado' => 1
            );
            $this->doping_model->deleteDoping($id_doping, $borrado);
            $this->doping_model->deleteDopingDetalle($id_doping);
            $this->doping_model->deleteCandidato($id_candidato);
            $motivo = array(
                'creacion' => $date,
                'id_usuario' => $id_usuario,
                'id_doping' => $id_doping,
                'motivo' => $motivo,
                'nombre' => $nombre,
                'paterno' => $paterno,
                'materno' => $materno,
                'cliente' => $cliente,
                'subcliente' => $subcliente,
                'proyecto' => $proyecto
            );
            $this->doping_model->motivoEliminar($motivo);
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
        }
        echo json_encode($msj);
    }
    function getDopingsEliminados(){
        $data['eliminados'] = $this->doping_model->getDopingsEliminados();
        if($data['eliminados']){
            $salida = '<table class="table table-striped" style="font-size: 13px">';
            $salida .= '<tr style="background: gray;color:white;">';
            $salida .= '<th>Fecha doping</th>';
            $salida .= '<th>Nombre</th>';
            //$salida .= '<th>Cliente</th>';
            $salida .= '<th>Código</th>';
            $salida .= '<th>Motivo</th>';
            $salida .= '<th>Usuario</th>';
            $salida .= '</tr>';
            foreach($data['eliminados'] as $c){
                $subcliente = ($c->subcliente != "" && $c->subcliente != null)? $c->subcliente:"";
                $proyecto = ($c->proyecto != "" && $c->proyecto != null)? $c->proyecto:"";
                $salida .= "<tr>";
                $salida .= '<td>'.$c->fecha_doping.'</td>';
                $salida .= '<td>'.$c->nombre." ".$c->paterno." ".$c->materno.'</td>';
                //$salida .= '<td>'.$c->cliente.'-'.$subcliente.'-'.$proyecto.'</td>';
                $salida .= '<td>'.$c->codigo_prueba.'</td>';
                $salida .= '<td width="30%">'.$c->motivo.'</td>';
                $salida .= '<td width="10%">'.$c->usuario.'</td>';
                $salida .= "</tr>";
            }
            $salida .= "</table>";
        }
        else{
            $salida = '<p style="text-align:center;font-size:18px;font-weight:bold;">Sin registros</p>';
        }
        
        echo $salida;
    }
    function createPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        if($this->uri->segment(3) != null){
            $id_doping = $this->uri->segment(3);
        }
        else{
            $id_doping = $_POST['idDop'];
        }
        $doping = $this->doping_model->getDatosDoping($id_doping);
        
        $data['doping'] = $doping;
        $html = $this->load->view('pdfs/doping_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->SetHTMLHeader('<div style="width: 100%; float: left;"><img style="height: 150px;" src="'.base_url().'img/Encabezado.png"></div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 12px;"><div style="border-bottom:1px solid gray;"><b>Teléfono:</b> (33) 2301-8599 | <b>Correo:</b> hola@rodi.com.mx | <b>Sitio web:</b> rodi.com.mx</div><br>Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco, México. C.P. 45018 <br><br>4-DOP-003.Rev. 00 <br>Fecha de Rev. 14/10/2019</p></div><div style="position: absolute; right: 10px;  bottom: 13px;"><img width="" src="'.base_url().'img/logo2.png"></div>');
        $mpdf->WriteHTML($html);

        $mpdf->Output('doping_'.$doping->codigo_prueba.'_'.$doping->nombre.'_'.$doping->paterno.'.pdf','D'); // opens in browser
    }
    function createPDF2(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_doping = $_POST['idPDF2'];
        $doping = $this->doping_model->getDatosDoping($id_doping);
        
        $data['doping'] = $doping;
        $html = $this->load->view('pdfs/doping_adul_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->SetHTMLHeader('<div style="width: 100%; float: left;"><img style="height: 150px;" src="'.base_url().'img/Encabezado.png"></div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 12px;"><div style="border-bottom:1px solid gray;"><b>Teléfono:</b> (33) 2301-8599 | <b>Correo:</b> hola@rodi.com.mx | <b>Sitio web:</b> rodi.com.mx</div><br>Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco, México. C.P. 45018 <br></p></div><div style="position: absolute; right: 10px;  bottom: 13px;"><img width="" src="'.base_url().'img/logo2.png"></div>');
        $mpdf->WriteHTML($html);

        $mpdf->Output('doping_adul_'.$doping->codigo_prueba.'_'.$doping->nombre.'_'.$doping->paterno.'.pdf','D'); // opens in browser
    }
    function createCadenaPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_doping = $_POST['idCadena'];
        $doping = $this->doping_model->getDatosDoping($id_doping);
        
        $data['doping'] = $doping;
        $html = $this->load->view('pdfs/doping_cadena_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->SetHTMLHeader('<div style="width: 100%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px;"><p style="font-size: 12px;"><div style="width: 95%;margin-left: 20px;">AVISO DE PRIVACIDAD RO & DI GLOBAL S DE RL DE CV HACE DE SU CONOCIMIENTO QUE LA INFORMACIÓN PROPORCIONADA POR USTED EN EL PRESENTE DOCUMENTO ES PROPIEDAD DE LA EMPRESA QUIEN CONTRATÓ ESTE SERVICIO Y NOS DESLINDAMOS DEL USO QUE SE LE DÉ A LA MISMA.</p><br>4-DOP-001.Rev. 00 <br>Fecha de Rev. 14/10/2019</div></div>');
        $mpdf->WriteHTML($html);

        $mpdf->Output('cadena_'.$doping->codigo_prueba.'_'.$doping->nombre.'_'.$doping->paterno.'.pdf','D'); // opens in browser
    }
    function createMembretadoPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_doping = $_POST['idDopMembretado'];
        $doping = $this->doping_model->getDatosDoping($id_doping);
        
        $data['doping'] = $doping;
        $html = $this->load->view('pdfs/doping_membretado_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->WriteHTML($html);

        $mpdf->Output('doping_membretado_'.$doping->codigo_prueba.'_'.$doping->nombre.'_'.$doping->paterno.'.pdf','D'); // opens in browser
    }
    function changeLaboratorio(){
        $this->form_validation->set_rules('lab', 'Laboratorio', 'required|trim');

        $this->form_validation->set_message('required','El campo %s es obligatorio');

        $msj = array();
        if ($this->form_validation->run() == FALSE) {
            $msj = array(
                'codigo' => 0,
                'msg' => validation_errors()
            );
        } 
        else {
            $dop = array(
                'laboratorio' => $this->input->post('lab')
            ); 
            $this->doping_model->updateDoping($dop, $this->input->post('id_doping'));
            $msj = array(
                'codigo' => 1,
                'msg' => 'success'
            );
        }
        echo json_encode($msj);
    }
    function getDetalleDoping(){
        $idDoping = $this->input->post('idDoping');
        $salida = "";
        $dato = $this->doping_model->getDetalleDoping($idDoping);
        $salida .= '<table class="table table-striped" style="font-size: 13px">';
        $salida .= '<thead>';
        $salida .= '<tr  style="background: gray;color:white;">';
        $salida .= '<th scope="col">Candidato</th>';
        $salida .= '<th scope="col">Codigo</th>';
        $salida .= '<th scope="col">Fecha resultado</th>';
        $salida .= '<th scope="col">Acciones</th>';
        $salida .= '</tr>';
        $salida .= '</thead>';
        $salida .= '<tbody>';
        $fecha = fecha_sinhora_espanol_bd($dato->fecha_resultado);
        if($dato->resultado == 0){
            $color = '<i class="fas fa-circle status_bgc1"></i> ';
        }
        elseif ($dato->resultado == 1) {
            $color = '<i class="fas fa-circle status_bgc2"></i> ';
        }
        else{
            $color = '<i class="fas fa-circle status_bgc3"></i> ';
        }
        if($dato->id_cliente != 3 && $dato->id_cliente != 71){
            $salida .= "<tr><th>".$color.$dato->candidato."</th><th>".$dato->codigo_prueba."</th><th>".$fecha."</th><th><div style='margin-right:5px;'><form id='descform".$dato->id."' action='".base_url('Doping/createPDF')."' method='POST'><input type='hidden' name='idDop' id='idDop".$dato->id."' value='".$dato->id."'><button class='btn btn-primary btn-sm' type='submit'>Descargar Resultado</button></form></div> <div style='margin-right:5px;'><form id='descformmembretado".$dato->id."' action='".base_url('Doping/createMembretadoPDF')."' method='POST'><input type='hidden' name='idDopMembretado' id='idDopMembretado".$dato->id."' value='".$dato->id."'><button class='btn btn-info btn-sm' type='submit'>Descargar para membretado</button></form></div> <button class='btn btn-danger btn-sm' onclick='eliminarDoping(".$dato->id.",".$dato->id_candidato.",\"".$dato->codigo_prueba."\")'>Eliminar</button><button class='btn btn-warning btn-sm' onclick='editarDoping(".$dato->id_candidato.",".$dato->id.",\"".$dato->nombre."\",\"".$dato->paterno."\",\"".$dato->materno."\",".$dato->antidoping.",".$dato->id_cliente.",".$dato->id_subcliente.",".$dato->id_proyecto.",\"".$dato->fecha_nacimiento."\",".$dato->id_tipo_identificacion.",\"".$dato->ine."\",".$dato->razon.",\"".$dato->medicamentos."\",\"".$dato->fecha_doping."\",\"".$dato->foto."\",\"".$dato->comentarios."\")'>Editar</button><button class='btn btn-primary btn-sm' onclick='resultadosDoping(".$dato->id.",\"".$dato->codigo_prueba."\",\"".$dato->candidato."\",\"".$dato->fecha_resultado."\",".$dato->id_candidato.")'>Ver Resultados</button></th></tr>";
        }
        if($dato->id_cliente == 3 || $dato->id_cliente == 71){
            $salida .= "<tr><th>".$color.$dato->candidato."</th><th>".$dato->codigo_prueba."</th><th>".$fecha."</th><th><div style='margin-right:5px;'><form id='descform".$dato->id."' action='".base_url('Doping/createPDF')."' method='POST'><input type='hidden' name='idDop' id='idDop".$dato->id."' value='".$dato->id."'><button class='btn btn-primary btn-sm' type='submit'>Descargar Resultado</button></form></div><div style='margin-right:5px;'><form id='otropdf".$dato->id."' action='".base_url('Doping/createPDF2')."' method='POST'><input type='hidden' name='idPDF2' id='idPDF2".$dato->id."' value='".$dato->id."'><button class='btn btn-primary btn-sm' type='submit'>Descargar Resultado Adulterantes</button></form></div><button class='btn btn-danger btn-sm' onclick='eliminarDoping(".$dato->id.",".$dato->id_candidato.",\"".$dato->codigo_prueba."\")'>Eliminar</button><button class='btn btn-warning btn-sm' onclick='editarDoping(".$dato->id_candidato.",".$dato->id.",\"".$dato->nombre."\",\"".$dato->paterno."\",\"".$dato->materno."\",".$dato->antidoping.",".$dato->id_cliente.",".$dato->id_subcliente.",".$dato->id_proyecto.",\"".$dato->fecha_nacimiento."\",".$dato->id_tipo_identificacion.",\"".$dato->ine."\",".$dato->razon.",\"".$dato->medicamentos."\",\"".$dato->fecha_doping."\",\"".$dato->foto."\",\"".$dato->comentarios."\")'>Editar</button><button class='btn btn-primary btn-sm' onclick='resultadosDoping(".$dato->id.",\"".$dato->codigo_prueba."\",\"".$dato->candidato."\",\"".$dato->fecha_resultado."\",".$dato->id_candidato.")'>Ver Resultados</button></th></tr>";
        }
        
        $salida .= '</tbody>';
        $salida .= '</table>';
        echo $salida;
    }

    /************************************************ Rules Validate Form ************************************************/

    //Regla para nombres con espacios
    function alpha_space_only($str){
        if (!preg_match("/^[a-zA-Z ]+$/",$str)){
            $this->form_validation->set_message('alpha_space_only', 'El campo %s debe estar compuesto solo por letras y espacios y no debe estar vacío');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    function required_file(){
        $this->form_validation->set_message('required_file', 'Carga el archivo de factura');
        if (empty($_FILES['archivo']['name'])) {
            return FALSE;
        }else{
            return TRUE;
        }
    }
}